# WifiBotLib
Collection de bibliothèques et de logiciels pour faciliter l'utilisation des Wifibots. Le but est de doter les Wifibots de comportements "intelligents".

## Ancienne pages Wiki de la gforge INRIA

### WifiBotPpc
#### Connexion par cable série

* connecter l'ordinateur au wifibot avec un cable série standard (possible parce que nous avons des wifibots PPC). Il faut utiliser le port série du même côté que l'interrupteur du wifibot.
* lancer le minicom avec comme configuration (115200 8N1) 

### URBI et Webots
#### URBI

* Documentation écrite sur URBI : <lien qui n'existe plus>

On y trouve en particulier un tutorial, les références du langage URBI et des explications sur comment s'interfacer avec URBI en C++, voire même comment ajouter des fonctionnalité (plugin) à URBI.

* Pour tester URBI 

Le plus simple est sans doute de télécharger la version gratuite (mais limitée) de Webots (le simulateur fait par la K-Team). Outre une démo, des "challenges", on y trouve aussi de quoi utiliser URBI. Y'a des info sur <lien qui n'existe plus> et en particulier un lien sur une page de documentation (tutorial) sur URBI+Webots : <lien qui n'existe plus>

moi (Alain) j'ai trouvé que c'était pas la peine de télécharger "Urbi For Webots" qui ne s'installe pas si facilement (sous debian, il faut forcer l'install avec un "--force-overwrite") et en plus c'est une version d'évaluation qui ne marche que 5 min. Mais à mieux tester

* URBI Remote 

Pour interagir avec un server URBI, il y a telnet mais c'est pas super pratique. J'ai téléchargé urbiremote sur leur sourceforge <lien qui n'existe plus>

* Programme partenaire

Chez Gostai, ils font des partenariats avec des Académiciens <lien qui n'existe plus> 

#### KheperaTrois

* kb_khepera3.h/c : This module is layer for communication with various devices which are part of the khepera3 robot. It offers simple interface to the user.

    * ```int kh3init( void )``` kh3init initializes some things like the GPIO40 pin. This function needs to be called BEFORE any other functions. 

    * ```int kh3proximity_ir(char 'outbuf, knet_devt hDev)``` => as a buffer of unsigned int (2 char)
 
    * ```int kh3ambiant_ir(char 'outbuf, knet_devt hDev)``` => as a buffer of unsigned int (2char).
 
    * ```int kh3battery_voltage(char 'outbuf, unsigned char argument, knet_devt hDev)``` , argument = {0:Voltage, 1:Current, 2:!AvgCurrent, 3:!AbsRemainCapcity, 4:Temp} => asa a buffer of (sometimes) unsigned int.
 
    * ```int kh3measure_us(char 'outbuf, unsigned char usnbr, knet_devt hDev)```, usnbr={1 to 5} => complex buffer with, for each echo, its distance and amplitude

* kmot.h/c : This module provides useful basic facilities to use a !KoreMotor. 

    * ```void kmot!SetPoint( knet_devt dev , int regtype , long setPoint )```, regtype={kmot::kMotRegType} => set motors speed, position, etc (see also kmot::kMotRegType)
    * ```void kmot!SetMode( knet_devt dev , int mode )```, mode={kmot::kMotMode} => used for stopping the motor
    * ```long kmot!GetMeasure( knet_devt dev , int regtype )```, regtype={kmot::kMotRegType}

* kb_error.h/c : This module provides basic error and message handling.

    * ```void kbset_debuglevel( unsigned int level)``` 

* kmot_ipserver.c : exemple de server TCP/IP a faire tourner sur le robot 

* kmot_test.c : exemple d'interaction avec un moteur, et aussi pour des test/status. 

##### Questions

volatilité de /media/ram ? 
