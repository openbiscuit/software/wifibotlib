/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**                         Maia, Loria.
**
** Original authors: C�dric Bernier, Julien Le Guen.
**  
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/* Fonctions I2C "bas niveau" pour le wifibot 4G
 * 
 * Ces fonctions permettent de contr�ler et de r�cup�rer
 * les information du robot via le bus I2C de la carte
 * mere, qui joue le r�le du maitre du bus.
 * Plusieurs esclaves sont pr�sents sur le bus :
 *  - une carte qui contr�le le niveau de la batterie
 *    que l'on peut interroger
 *  - une carte par moiti� du robot (gauche et droite)
 *    qui contr�le les deux moteurs, et r�cup�re les
 *    informations de vitesse des moteurs ainsi que la
 *    valeur renvoy�e par le capteur de distance IR.
 *
 * Il faut initialiser le bus I2C au d�but du programme,
 * avec la fonction initI2C. Le num�ro du bus est d�fini
 * dans le header.
 *
 * Ensuite on peut modifier la vitesse de chaque cot�
 * avec la fonction setI2CSpeed(adresse, vitesse).
 * L'adresse doit �tre une adresse de carte valide
 * (ADR_LEFT ou ADR_RIGHT) et vitesse doit �tre construit
 * de cette fa�on :
 * - bit 7:	Speed Motor Control
 *  		Permet d'activer le PID des roues
 *  		Asservissement de la vitesse des roues
 *  		par rapport � la consigne.
 *  		1: ON
 *  		0: OFF (par d�faut)
 * - bit 6: Sens des roues
 *   		0: Backward
 *   		1: Forward
 * - bits 5 � 0: Vitesse des roues
 *   		De 0 � 60 en mode "libre"
 *   		De 0 � 40 en mode PID
 *
 * Pour contr�ler le robot, on peut lire la vitesse de
 * chaque roue. Pour cela il faut utiliser la fonction
 * getI2CInfo(adresse). Adresse doit �tre une adresse
 * valide. La fonction met � jour les champs
 * correspondant au c�t� du robot dans la structure
 * winfo de type WifibotInfo. Elle r�cup�re :
 *  - vitesse de la roue avant
 *  - vitesse de la roue arri�re
 *  - valeur du capteur de distance associ� au cot�
 *
 * On peut �galement contr�ler le niveau de la batterie
 * du robot grace � la fonction getI2CBattery().
 * Elle met � jour le champ battery de la structure
 * winfo.
 *
 * Il est recommand� d'utiliser le mode PID pour le
 * controle de la vitesse, car le robot v�rifie en
 * permanence la vitesse des roues et r�agit bien mieux
 * aux changements de consigne.
 */


#include <stdio.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>


/*
 * Defines 
 */

/* Num�ro du bus I2C */
#define NUM_BUS 0

/* Adresses des diff�rents �l�ments */
#define ADR_LEFT 0x51	/* cot� gauche du robot	*/
#define ADR_RIGHT 0x52	/* cot� droit du robot	*/
#define ADR_BAT 0x4e	/* la batterie	*/

/* Valeurs retourn�es par les fonctions */
#define I2C_OK	0
#define I2C_OPEN_ERROR	1
#define I2C_IO_ERROR	2
#define I2C_WRITE_ERROR	3
#define I2C_READ_ERROR	4
#define I2C_ADR_ERROR	5

/* Flags de controle des moteurs */
#define MOT_CTRL_ON		0x80
#define MOT_CTRL_OFF	0x00
#define MOT_FORWARD		0x40
#define MOT_BACKWARD	0x00


/* 
 * Structure d�finissant les commandes � envoyer
 */
typedef struct cmd {
	unsigned char left;	/* vitesse cot� gauche */
	unsigned char right;/* vitesse cot� droit */
}WifibotCmd;

/* 
 * Structure d�finissant les informations re�ues
 */
typedef struct info {
	unsigned char battery;
	unsigned char frontLeft;
	unsigned char rearLeft;
	unsigned char frontRight;
	unsigned char rearRight;
	unsigned char irLeft;
	unsigned char irRight;
}WifibotInfo;


/* 
 * Encore un peu crade ces globales...
 * TODO modifier la lib pour renvoyer des fp et tout...
 */
WifibotInfo winfo;
int file;	/* "pointeur" vers le bus I2C */


/*
 * Initialise le bus I2C num_bus en lecture/ecriture
 */
int initI2C(int num_bus)
{
	char buf[255];
	 /*
	  * Version powerpc : i2c-%d
	  * Version MIPS : i2c/%d
	  */
	sprintf(buf, "/dev/i2c-%d", num_bus);
	/* Impossible d'ouvrir le bus I2C */
	if((file = open(buf, O_RDWR)) < 0) 
		return I2C_OPEN_ERROR;
	
	/* Tout va bien */
	return I2C_OK;
}


/*
 * Envoie la commande de vitesse au robot.
 * adr est ADR_LEFT ou ADR_RIGHT
 */
int setI2CSpeed(unsigned char adr, unsigned char speed)
{
	char buf[2];
	buf[0] = speed;
	buf[1] = speed;

	/* On teste ocazou */
	if(adr != ADR_LEFT && adr != ADR_RIGHT)
		return I2C_ADR_ERROR;
	
	/* On ouvre la connexion vers l'esclave */
	if(ioctl(file, I2C_SLAVE, adr) < 0)
		return I2C_IO_ERROR;

	/* On �crit la vitesse */
	if(write(file, buf, 2) != 2)
		return I2C_WRITE_ERROR;

	return I2C_OK;
}


/*
 * R�cup�re les informations de vitesse et distance
 * du robot. Les informations se retrouvent dans la
 * structure globale winfo.
 */
int getI2CInfo(unsigned char adr)
{
	char buf[3];
	
	/* on teste ocazou */
	if(adr != ADR_LEFT && adr != ADR_RIGHT)
		return I2C_ADR_ERROR;

	/* On ouvre la connexion vers l'esclave */
	if(ioctl(file, I2C_SLAVE, adr) < 0)
		return I2C_IO_ERROR;

	/* on lit les informations de vitesse */
	if(read(file, buf, 3) != 3)
		return I2C_READ_ERROR;

	/* On met � jour les donn�es */
	if(adr == ADR_LEFT)
	{
		winfo.frontLeft = buf[0];
		winfo.rearLeft = buf[1];
		winfo.irLeft = buf[2];
	} else
	{
		winfo.frontRight = buf[1];
		winfo.rearRight = buf[0];
		winfo.irRight = buf[2];
	}

	return I2C_OK;
}


/*
 * R�cup�re l'information de la batterie
 * Met � jour la structure globale winfo
 */
int getI2CBattery()
{
	char buf[2];
	char buf2;

	/* On ouvre la connexion vers l'esclave */
	if(ioctl(file, I2C_SLAVE, ADR_BAT) < 0)
		return I2C_IO_ERROR;

	/* On lui dit qu'on veut les infos de la batterie */
	buf[0] = 0x40;
	buf[1] = 250;	/* DAC - cf source d'exemple */
	if(write(file, buf, 2) != 2)
		return I2C_WRITE_ERROR;

	/* On r�cup�re l'information de niveau */
	if(read(file, &buf2, 1) != 1)
		return I2C_READ_ERROR;

	winfo.battery = buf2;

	return I2C_OK;
}


/*
 * Fonction de niveau sup�rieur qui r�cup�re toutes les infos disponibles
 */
int getInfos()
{
	
}
