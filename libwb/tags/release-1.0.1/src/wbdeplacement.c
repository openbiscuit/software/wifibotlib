/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

#include "libwb/wbi2c.h"

unsigned char mode = MOT_CTRL_ON;

/*
 * Permet de modifier le mode de fonctionnement du cont�le moteur
 * m doit etre MOT_CTRL_ON ou MOT_CTRL_OFF suivant que l'on veut
 * ou non que l'asservissement soit activ� (ON par d�faut)
 */
void setMode(unsigned char m)
{
	mode = m;
}


/*
 * Permet de modifier la vitesse de chaque c�t� du robot.
 * vg et vd doivent �tre inf�rieurs � 63, et sens doit �tre
 * MOT_FORWARD ou MOT_BACKWARD.
 */
int setSpeed(unsigned char vg, unsigned char vd, unsigned char sens)
{
	int error;
	/* On seuille la vitesse pour �viter d'�craser les flags */
	if(vg > 63) vg = 63;
	if(vd > 63) vd = 63;
	error = setI2CSpeed(ADR_LEFT, mode | sens | vg);
	if(error) return error;
	
	error = setI2CSpeed(ADR_RIGHT, mode | sens | vd);
	return error;
}


/*
 * Permet d'avancer en faisant une courbe, c'est � dire que les 
 * moteurs ne tournent pas � la m�me vitesse.
 */
int avancerCourbe(unsigned char vg, unsigned char vd)
{
	return setSpeed(vg, vd, MOT_FORWARD);
}


/*
 * Permet d'avancer th�oriquement tout droit. Les essais montrent
 * que le robot ne va pas droit lorsqu'on lui donne la m�me vitesse
 * de chaque c�t�.
 */
int avancer(unsigned char vitesse)
{
	return avancerCourbe(vitesse, vitesse);
}


/* 
 * Permet de reculer en faisant une courbe
 */
int reculerCourbe(unsigned char vg, unsigned char vd)
{
	return setSpeed(vg, vd, MOT_BACKWARD);
}


/*
 * Permet de reculer tout droit. Les m�me limitations qu'avec
 * avancer() sont pr�sentes.
 */
int reculer(unsigned char vitesse)
{
	return reculerCourbe(vitesse, vitesse);
}


/*
 * Stoppe compl�tement et imm�diatement le robot. Pr�voir un temps
 * de latence entre envoi de la commande et l'arret effectif du robot,
 * notamment du fait de l'inertie du robot et des moteurs.
 */
int stop(void)
{
	return setSpeed(0, 0, MOT_FORWARD);
}


/* Permet de tourner sur place (sens horaire) */
int tournerHoraire(unsigned char vitesse)
{
	int error;
	/* On seuille la vitesse pour �viter d'�craser les flags */
	if(vitesse > 63) vitesse = 63;
	error = setI2CSpeed(ADR_LEFT, mode | MOT_FORWARD | vitesse);
	if(error) return error;
	
	error = setI2CSpeed(ADR_RIGHT, mode | MOT_BACKWARD | vitesse);
	return error;
}


/* Permet de tourner sur place (sens antihoraire) */
int tournerAntiHoraire(unsigned char vitesse)
{
	int error;
	/* On seuille la vitesse pour �viter d'�craser les flags */
	if(vitesse > 63) vitesse = 63;
	error = setI2CSpeed(ADR_LEFT, mode | MOT_BACKWARD | vitesse);
	if(error) return error;
	
	error = setI2CSpeed(ADR_RIGHT, mode | MOT_FORWARD | vitesse);
	return error;
}

