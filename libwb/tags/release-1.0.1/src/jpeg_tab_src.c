/*
 * jpeg_tab_src.c
 * Fournit un source manager pour le d�compresseur de la libjpeg
 * comme expliqu� dans libjpeg.doc
 */

#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"
#include <stdlib.h>
#include <stdio.h>

/* Expanded data source object for tab input */
typedef struct {
	struct jpeg_source_mgr pub; /* public fields */
	JOCTET * buffer;	/* Start of buffer */
	int nb_bytes;
	boolean start_of_file;
} tab_source_mgr;

typedef tab_source_mgr * tab_src_ptr;


/*
 * Initialise source --- called by jpeg_read_header
 * before any data is actually read
 */
METHODDEF(void) init_source(j_decompress_ptr cinfo)
{
	tab_src_ptr src = (tab_src_ptr) cinfo->src; 
  	/* We reset the empty-input-file flag for each image,
   	 * but we don't clear the input buffer.
   	 * This is correct behavior for reading a series of images from one source.
   	 */
	src->start_of_file = TRUE;
}




METHODDEF(boolean) fill_input_buffer(j_decompress_ptr cinfo)
{
	tab_src_ptr src = (tab_src_ptr) cinfo->src;
	src->pub.next_input_byte = src->buffer;
	src->pub.bytes_in_buffer = src->nb_bytes;
	src->start_of_file = FALSE;

	return TRUE;
}



METHODDEF(void) skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
	tab_src_ptr src = (tab_src_ptr) cinfo->src;
	if(num_bytes > 0)
	{
		src->pub.next_input_byte += (size_t) num_bytes;
		src->pub.bytes_in_buffer -= (size_t) num_bytes;
	}
}


METHODDEF(void) term_source(j_decompress_ptr cinfo)
{
	/* void */
}


/*
 * Source manager � partir d'un tableau d�j� charg� en m�moire.
 * Il suffit d'indiquer l'adresse du tableau, ainsi que sa taille,
 * pour ensuite pouvoir d�compresser l'image stock�e dans ce tableau.
 * Une fois l'image d�compress�e on peut travailler dessus.
 */
GLOBAL(void) jpeg_tab_src(j_decompress_ptr cinfo, JOCTET *buffer, int nbytes)
{
	tab_src_ptr src;
	if(cinfo->src == NULL)
	{
		cinfo->src = (struct jpeg_source_mgr *)
			(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
										SIZEOF(tab_source_mgr));
		src = (tab_src_ptr) cinfo->src;
	}

	src = (tab_src_ptr) cinfo->src;
	src->pub.init_source = init_source;
	src->pub.fill_input_buffer = fill_input_buffer;
	src->pub.skip_input_data = skip_input_data;
	src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
	src->pub.term_source = term_source;
	src->nb_bytes = nbytes;
	src->buffer = buffer;
	src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
	src->pub.next_input_byte = NULL; /* until buffer loaded */

}









