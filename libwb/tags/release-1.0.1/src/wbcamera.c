/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/
/*
 * Routines pour controler la cam�ra du wifibot
 *
 * La cam�ra du robot embarque un serveur Web ex�cutant des scripts Java et CGI,
 * mettant � disposition un flux vid�o et Jpeg.
 * Pour piloter la cam�ra, on dispose d'un applet java. On peut aussi utiliser les routines
 * ci-dessous pour la contr�ler en embarqu�, depuis le robot.
 *
 * Pour la contr�ler il faut envoyer une requete POST au script /PANTILTCONTROL.CGI
 * de ce type: 
 *	POST /PANTILTCONTROL.CGI HTTP/1.0
 *	User-Agent: user
 *	Authorization: Basic
 *	Content-Type: application/x-www-form-urlencoded
 *	Content-Length: 66
 *
 *	PanSingleMoveDegree=10&TiltSingleMoveDegree=10&PanTiltSingleMove=6
 *
 * Cela demande � la cam�ra de bouger de 10 degr�s en PAN et TILT, selon la direction 6.
 * Tableau des directions :
 * 0 1 2
 * 3 4 5
 * 6 7 8
 * La direction 4 repositionne la cam�ra en position initiale.
 *
 * Pour faire bouger la cam�ra il suffit alors d'ouvrir une socket sur le port 80 de la cam�ra,
 * de construire une requete POST correcte et d'envoyer tout �a au script CGI.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "libwb/wbsocket.h"

#define BUFFERSIZE 512

/* Enumeration des directions du mouvement de la camera*/
typedef enum {
  TopLeft, Top, TopRight, Left, Home, Right, BottomLeft, Bottom, BottomRight, SENS_Unknown
}SENS;



/* Proc�dure permettant de contr�ler la cam�ra.
 * pan: le mouvement vertical 
 * tilt: le mouvement horizontal
 * sens: la direction du mouvement 
 */
void PanTiltMoveCamera(int pan, int tilt, SENS sens)
{
	char buffer[BUFFERSIZE];
	char buf[100];
	int taille, nb, sk;
	
	taille = sprintf(buf, "PanSingleMoveDegree=%d&TiltSingleMoveDegree=%d&PanTiltSingleMove=%d", pan, tilt, sens);

	/* on construit la requete POST */
	nb = sprintf(buffer, "POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n", taille, buf);

	sk = initialiseSocket();
	/* on envoie notre requete */
	send(sk, buffer, nb, 0);
	/* ON ATTEND QUE LE MESSAGE SOIT ENVOYE
	 * ET QUE LA CAM AIT REAGI */
	usleep(500000);
	close(sk);
}



/* Proc�dure permettant de contr�ler la cam�ra.
 * pan: le mouvement vertical 
 * tilt: le mouvement horizontal
 * sens: la direction du mouvement 
 * N'EFFECTUE PAS L'ATTENTE APRES LE SEND
 * A N'UTILISER QUE SI LES TRAITEMENTS ENTRE DEUX APPELS
 * SONT SUFFISAMMENT LONGS
 * Retourne le numero de socket a fermer dans la routine appelante
 */
int PanTiltMoveCameraNoSleep(int pan, int tilt, SENS sens)
{
	char buffer[BUFFERSIZE];
	char buf[100];
	int taille, nb, sk;
	
	taille = sprintf(buf, "PanSingleMoveDegree=%d&TiltSingleMoveDegree=%d&PanTiltSingleMove=%d", pan, tilt, sens);

	/* on construit la requete POST */
	nb = sprintf(buffer, "POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n", taille, buf);

	sk = initialiseSocket();
	/* on envoie notre requete */
	send(sk, buffer, nb, 0);
	/* ON ATTEND QUE LE MESSAGE SOIT ENVOYE
	 * ET QUE LA CAM AIT REAGI */
	//usleep(500000);
	//close(sk);
	return sk;
}

