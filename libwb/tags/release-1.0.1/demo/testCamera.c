/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 *  Fichier testant la chaine de compilation et la camera.
 *  Sur le Wifibot.
 */

#include "libwb/wb.h"
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#define SIZE 150000


int main(void)
{
	int taille, width, height;
	unsigned char *image;
	//unsigned char image[480][640][3];
	unsigned char buffer[SIZE];


	// on aura jamais plus grand
	image = (unsigned char*) malloc(640*480*3*sizeof(unsigned char));

	// on r�cup�re l'image et on l'enregistre
	printf("getImage\n");
	taille = getImage(buffer);

	printf("decompresseImage\n");
	taille = decompresseImage((unsigned char*)image, buffer, taille, &width, &height);

	printf("ecrireImage\n");
	ecrireImage("out.ppm", (unsigned char*)image, taille, width, height);

	// goto home
	printf("goto home\n");
	PanTiltMoveCamera(10, 10, Home);
	usleep(500000);
	
	printf("goto top right\n");
	PanTiltMoveCamera(10, 10, TopRight);
	
	printf("goto bottom right\n");
	PanTiltMoveCamera(10, 10, BottomRight);
	PanTiltMoveCamera(0,0,Home);

	free(image);
	return 0;
}
