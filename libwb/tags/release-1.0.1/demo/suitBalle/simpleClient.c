/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * A simpleClient to test data transmission with the simpleServer.
 * On client computers.
 */


#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "suitBalle.h"

#define NB_THREADS	2
void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);

void dieWithErrorNb(int ret);
void dieWithErrorMsg(char *msg);
void gestionnaire (int numero);

int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole);
void printTarget( Tobjet *target );


pthread_t thread [NB_THREADS];

int dog=0;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;

#define MAXPENDING 5    /* Maximum outstanding connection requests */
int                sock;
struct sockaddr_in    addr;
int recvMsgSize=0; 
unsigned char buffso_rcv[32]; 
//static unsigned char buffso_send[7];
char sendbuf[10];
char rcvbuf[7],rcvbuftemp[7];

Tobjet myObjet;

int first=1;
int connected=0;

struct SensorData
{
  int BatVoltage;
  int SpeedFrontLeft;
  int SpeedRearLeft;
  int SpeedFrontRight;
  int SpeedRearRight;
  int IRLeft;
  int IRRight;	
};

int main (int argc, char *argv[])
{
  int ret;

   if (lecture_arguments (argc, argv, & addr, "tcp") < 0) {
     dieWithErrorMsg( "Check argument\n");
   }
   addr.sin_family = AF_INET;

  // Alain : thread[0] est un chien de garde pour fermer le thread de connection
  // si ouvert trop longtemps...
  if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
    dieWithErrorNb( ret );
  }
  // Alain : thread[1] est le thread de connection
  if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
    dieWithErrorNb( ret );
  }

  while ( 1 ){
    //usleep (1 * 100000);
    sleep(5);
  }

  pthread_join (thread [0], NULL);//blocking
  pthread_join (thread [1], NULL);//blocking

   return 0;
 }

/**
 * Function used to receive DATA over the socket.
 */
ssize_t recvData( int socket, Tobjet *data )
{
  Tobjet bufTobjet;
  int ret;

  ret = recv( socket, (void *) &bufTobjet, sizeof(Tobjet), 0);

  // perform data conversion (compatible high/low endian)

  data->haut = ntohl(bufTobjet.haut);
  data->bas = ntohl(bufTobjet.bas);
  data->gauche = ntohl(bufTobjet.gauche);
  data->droite = ntohl(bufTobjet.droite);
  data->taille = ntohl(bufTobjet.taille);
  data->x = ntohl(bufTobjet.x);
  data->y = ntohl(bufTobjet.y);

  return ret;
}

/**
 * Wait for some time before reseting the client.
 */
void * fn_thread_dog (void * num)
{
  int numero = (int) num;
  while (1) {
    // wait for 0.06s
    usleep (60000);

    /* TODO send message */
    strcpy( sendbuf, "c->s");
    send(sock, sendbuf, 5, 0);
    printf("client send: [%s]\n",sendbuf);

    pthread_mutex_lock (& mutex_dog);
    dog ++;
    pthread_mutex_unlock (& mutex_dog);

    if (dog>100) {//If no reception during 6 second : reset the client
      dog=0;
      connected=0;
      int ret=pthread_cancel (thread[1]);
      close(sock);
      sleep(2);
      if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
	dieWithErrorNb( ret );
      }			
    }
  }//end while
  pthread_exit (NULL);
}

/**
 * Thread for com. Open a socket, listen for msg.
 */
void * fn_thread_com (void * num) 
{
  int numero = (int) num;
    
  if ((sock = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
    dieWithErrorMsg( "socket() fail");
  }
  if (connect (sock, (struct sockaddr *) &addr, sizeof (struct sockaddr)) < 0) {
    dieWithErrorMsg( "connect() fail"); 
  }
  else {
    printf("Connected to the WIFIBOT\n");
    connected=1;        
    do{
      // wait for a message in the socket
      //if ((recvMsgSize = recv(sock, rcvbuftemp, 5, 0)) < 1) {
      if( (recvMsgSize = recvData( sock, &myObjet )) < 1) {
	printf("rcv error\n");shutdown(sock,1);
      }
      else {
	// when message received, 'dog' is set to 0
	pthread_mutex_lock (& mutex_dog);
	connected=1;	
	dog =0;
	pthread_mutex_unlock (& mutex_dog);

	/* TODO : decode message */
        //struct SensorData RobotSensors;
	//RobotSensors.BatVoltage=(int)rcvbuftemp[0];
	//RobotSensors.SpeedFrontLeft=(int)rcvbuftemp[1];
	//RobotSensors.SpeedRearLeft=(int)rcvbuftemp[2];
	//RobotSensors.SpeedFrontRight=(int)rcvbuftemp[3];
	//RobotSensors.SpeedRearRight=(int)rcvbuftemp[4];
	//RobotSensors.IRLeft=(int)rcvbuftemp[5];
	//RobotSensors.IRRight=(int)rcvbuftemp[6];
	//printf("bat %d\n",RobotSensors.BatVoltage);
	//printf("client rcvd: [%s]\n",rcvbuftemp);
	printf("client rcvd:\n");
	printTarget( &myObjet );
      }
    }while(recvMsgSize>0);
    connected=0;
  }//end else
  pthread_exit (NULL);
}//end thread

//pthread_mutex_lock (& mutex_joy_usb);
//XjoyTmp=Xjoy;
//YjoyTmp=Yjoy;
//pthread_mutex_unlock (& mutex_joy_usb);

/**
 * Interprets the arguments.
 *
 * @return -1 if error
 */
int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole)
{
  char * liste_options = "a:p:h";
  int    option;
	
  char * hote  = "152.81.10.250";
  char * port = "16000";

  struct hostent * hostent;
  struct servent * servent;

  int    numero;

  while ((option = getopt (argc, argv, liste_options)) != -1) {
    switch (option) {
    case 'a' :
      hote  = optarg;
      break;
    case 'p' :
      port = optarg;
      break;
    case 'h' :
      fprintf (stderr, "Syntaxe : %s [-a adresse] [-p port] [-j] j for usb joystick\n",
	       argv [0]);
      return (-1);
    default	: 
      break;
    }
  }
  memset (adresse, 0, sizeof (struct sockaddr_in));
  if (inet_aton (hote, & (adresse -> sin_addr)) == 0) {
    if ((hostent = gethostbyname (hote)) == NULL) {
      fprintf (stderr, "hote %s inconnu \n", hote);
      return (-1);
    }
    adresse -> sin_addr . s_addr =
      ((struct in_addr *) (hostent -> h_addr)) -> s_addr; 
  }
  if (sscanf (port, "%d", & numero) == 1) {
    adresse -> sin_port = htons (numero);
    return (0);
  }
  if ((servent = getservbyname (port, protocole)) == NULL) {
    fprintf (stderr, "Service %s inconnu \n", port);
    return (-1);
  }
  adresse -> sin_port = servent -> s_port;
  return (0);
}

/**
 * Connect to the server.
 */
void connection()
{
  int ret;

  if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
    dieWithErrorNb( ret );
  }
  
  if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
    dieWithErrorNb( ret );
    
  }
}

/**
 * Die while emiting an error msg.
 */
void dieWithErrorMsg(char *msg)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }
  close(sock);

  fprintf (stderr, "Program exit with error: %s\n", msg);
  exit (1);
}
/**
 * Die with an error msg number.
 */
void dieWithErrorNb(int ret)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }
  close(sock);

  fprintf (stderr, "Program exits with error: %s", strerror (ret));
  exit (1);
}
/**
 * Print info about target.
 */
void printTarget( Tobjet *target )
{
  printf( "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", target->haut, target->bas, target->gauche, target->droite);
  printf( "       x=%d, y=%d, taille=%d\n", target->x, target->y, target->taille);
}
