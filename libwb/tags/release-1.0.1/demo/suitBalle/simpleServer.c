/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * A simpleServer running on the wifibot to test data exchange with simpleClient.
 */

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "suitBalle.h"

#define NB_THREADS	2
void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);

ssize_t sendMsg(int socket, char *msg);
ssize_t sendData(int socket, Tobjet *data);

void printTarget( Tobjet *target );

void dieWithErrorMsg( char *msg);
void dieWithErrorNb(int ret);

void gestionnaire (int numero);

pthread_t thread [NB_THREADS];

int dog=0;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;

#define MAXPENDING 5    /* Maximum outstanding connection requests */
static int servSock;                    /* Socket descriptor for server */
static int clntSock;                    /* Socket descriptor for client */
static struct sockaddr_in echoServAddr; /* Local address */
static struct sockaddr_in echoClntAddr; /* Client address */
static unsigned short echoServPort=16000;     /* Server port */
static unsigned int clntLen;            /* Length of client address data structure */
static int recvMsgSize=0; 
static unsigned char buffso_rcv[32]; 
//static unsigned char buffso_send[7];

static Tobjet myObjet;

static int connected=0;

int main(int argc, char *argv[])
{
  int ret;
  myObjet.haut = 100;
  myObjet.bas = 170;
  myObjet.gauche = 200;
  myObjet.droite = 400;
  myObjet.taille = 123456;
  myObjet.x = 150;
  myObjet.y = 300;
  
  // Alain : pour des actions differentes
  struct sigaction action;

  //buffso_send[0]=0;
  //buffso_send[1]=0;
  //buffso_send[2]=0;	
  //buffso_send[3]=0;
  //buffso_send[4]=0;
  //buffso_send[5]=0;
  //buffso_send[6]=0;

  printf("running....\n");

  // Alain : thread[0] est un chien de garde pour fermer le thread de connection
  // si ouvert trop longtemps...
  if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
    dieWithErrorNb( ret );
  }
  // Alain : thread[1] est le thread de connection
  if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
    dieWithErrorNb( ret );
  }

  action.sa_handler = gestionnaire;
  sigemptyset (& (action.sa_mask));
  action.sa_flags = 0;

  if (sigaction (SIGQUIT, & action, NULL) != 0) {
    dieWithErrorNb( errno );
  }

  action . sa_handler = gestionnaire;
  sigemptyset (& (action . sa_mask));
  action . sa_flags = SA_RESTART | SA_RESETHAND;

  if (sigaction (SIGINT, & action, NULL) != 0) {
    dieWithErrorNb( errno );
  }

  while ( 1 ){
    //usleep (1 * 100000);
    sleep(5);
  }

  pthread_join (thread [0], NULL);//blocking
  pthread_join (thread [1], NULL);//blocking
  return 0;
}

/**
 * WatchDog for the Com Thread.
 * Every 3 seconds, variable 'dog' is incremented. If 'dog' reaches 5, it ends
 * everything.
 */
void * fn_thread_dog (void * num)
{
  int numero = (int) num;
	
  while (1) {
    usleep (3 * 100000);
    pthread_mutex_lock (& mutex_dog);
    dog ++;
    pthread_mutex_unlock (& mutex_dog);
    if (dog>5) {
      dog=0;
      if (connected) {
	connected=0;
        int ret=pthread_cancel (thread[1]);
       	close(clntSock);
	close(servSock);
	sleep(2);
	if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
	  dieWithErrorNb( ret );
	}
      }
    }
  }//end while
  pthread_exit (NULL);
}
/**
 * Thread for com. Open a socket, wait for connection, create socket with 
 * client and then listen and answers commands.
 */
void * fn_thread_com (void * num) 
{
  int numero = (int) num;

  /* Create socket for incoming connections (ip(7)*/
  if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    fprintf( stderr, "socket failed()\n");
    //dieWithErrorMsg("socket failed()");
  }

  /* Construct local address structure */
  memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
  echoServAddr.sin_family = AF_INET;                /* Internet address family */
  echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
  echoServAddr.sin_port = htons(echoServPort);      /* Local port */

  /* Bind to the local address */
  int autorisation=1;
  setsockopt(servSock,SOL_SOCKET,SO_REUSEADDR,&autorisation,sizeof(int));

  if (bind(servSock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) {
    fprintf(stderr, "bind()failed\n");
    //dieWithErrorMsg("bind() failed");
  }

  for (;;) /* Run forever */
    {       
      /* Mark the socket so it will listen for incoming connections */
      if (listen(servSock, MAXPENDING) < 0) {
        printf("listen() failed\n");
	//dieWithErrorMsg("listen() failed");
      }

      /* Set the size of the in-out parameter */
      clntLen = sizeof(echoClntAddr);
      /* Wait for a client to connect */
      if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0) {
	fprintf( stderr, "accept() failed\n");
	dieWithErrorMsg("accept() failed");
      }

      /* clntSock is connected to a client! */
      do{
	/* Wait to receive adequate msg */
        if ((recvMsgSize = recv(clntSock, buffso_rcv, 5, 0)) < 1) {
	  shutdown(clntSock,1);
	}
	else {
	  pthread_mutex_lock (& mutex_dog);
	  connected=1;	
	  dog =0;
	  pthread_mutex_unlock (& mutex_dog);	       	          
	  /* Answer the request */
	  printf("server rcvd: [%s]\n",buffso_rcv);
	  /* TODO */
	  //sendMsg( clntSock, "s->c");
	  sendData( clntSock, &myObjet );
	  //printf("server send: [%s]\n",buffMsg);
	  printf("server send:\n");
	  printTarget( &myObjet );
	}
      }
      while(recvMsgSize>0);
      
      connected=0;
      sleep(1);
    }//end for(;;)
  pthread_exit (NULL);
}//end thread

/**
 * Function used to send a string over the socket.
 * 
 * @return length of data send
 */
ssize_t sendMsg(int socket, char *msg)
{
  return send( socket, msg, strlen(msg), 0);
}
/**
 * Function used to send DATA over the socket.
 * 
 * @return length of data send
 */
ssize_t sendData(int socket, Tobjet *data)
{
  return send( socket, (void *) data, sizeof(Tobjet), 0);
}

/**
 * Print info about target Tobjet.
 */
void printTarget( Tobjet *target )
{
  printf( "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", target->haut, target->bas, target->gauche, target->droite);
  printf( "       x=%d, y=%d, taille=%d\n", target->x, target->y, target->taille);
}

/**
 * Die while emiting an error msg.
 */
void dieWithErrorMsg( char *msg)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }

  close(clntSock);
  close(servSock);
  fprintf (stderr, "Program exit with error: %s\n", msg);
  exit (1);
}
/**
 * Die with an error msg number.
 */
void dieWithErrorNb(int ret)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }
  close(clntSock);
  close(servSock);
  fprintf (stderr, "Program exits with error: %s", strerror (ret));
  exit (1);
}


/**
 * Deal with SIGNALs.
 * @comment Alain
 */
void gestionnaire (int signal)
{
  switch (signal) {
  case SIGQUIT :
    fprintf (stdout, "\nSIGQUIT recu\n"); fflush (stdout);		        	
    break;
  case SIGINT :
    fprintf (stdout, "\nSIGINT recu\n"); fflush (stdout);
    break;
  }
}
