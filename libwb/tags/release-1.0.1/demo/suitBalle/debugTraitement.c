/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * Debug program
 * Used to test image processing and target detection.
 */


#include <stdlib.h>
#include "SDL.h"
#include "SDL_gfxPrimitives.h"
#include "suitBalle.h"

unsigned char image[480*640*3+1];
int height, width, taille, depth;

unsigned char pixels[480*640*4+1];

Tpx myteinte[480][640];
unsigned char hsv[480*640];
unsigned char filtre[480*640*4];


SDL_Surface *screen;
SDL_Surface *imageSDL;
#define SDL_GFX_GREEN 0x00FF00FF
#define CROSS_RADIUS 10
#define max(a,b)   ((a) < (b) ? (b) : (a))
#define min(a,b)   ((a) > (b) ? (b) : (a))
#define INC 1

Uint32 rmask, gmask, bmask, amask;

/*variables globales*/
Tobjet objets[500]; //les objets r�cup�r�s apr�s traitement
int nbelt;
/* variables de param�trage*/
int tailleObjet=500; //taille minimum pour prendre en compte un objet trouv�
char prec=2; // nb de pixels analys�s pouvant ne pas etre de la couleur demand�e, donc marge d'erreur

void printTarget( Tobjet *target );

/*
 * 'img' must be large enough.
 */
int chargeImage( char *filename, unsigned char *img, int *taille, int *width, int *height)
{
  FILE *fp;
  int i;
  
  fp = fopen(filename, "r");
  if(fp == NULL) {
    fprintf(stderr, "Erreur : impossible d'ouvrir le fichier %s\n", filename);
    return(1);
  }
  // on lit les entetes
  // on �crit les entetes ppm (24 bits RVB)
  fscanf(fp, "P6 %d %d 255 ", width, height);
  *taille = 3 * (*width) * (*height); // 24 bits RVB
  // on lit le fichier
  for(i=0; i<(*taille); i++)
    {
      fscanf(fp, "%c", &(img[i]));
    }
  
  return fclose(fp);
} /* chargeImage() */
/* 
 * Enregistre une image RAW au format PPM
 *  - fichier 	: chaine de caractere contenant le nom du fichier
 *  - img		: pointeur vers le buffer contenant l'image d�compress�e
 *  - taille	: taille de l'image en octets
 *  - width, height : dimensions de l'image
 * Renvoie 0 en cas de succes.
 */
int ecrireImage(char *fichier, unsigned char *img, int taille, int width, int height)
{
	FILE *fp;
	int i;

	fp = fopen(fichier, "w");
	if(fp == NULL)
	{
		fprintf(stderr, "Erreur : impossible d'ouvrir le fichier %s\n", fichier);
		return(1);
	}
	// on �crit les entetes ppm (24 bits RVB)
	fprintf(fp, "P6 %d %d 255 ", width, height);
	// on copie le tableau dans le fichier
	for(i=0; i<taille; i++)
	{
		fprintf(fp, "%c", img[i]);
		fprintf(fp, "%c", img[i]);
		fprintf(fp, "%c", img[i]);
	}
	
	return fclose(fp);
} /* ecrireImage() */

int HtoPixels( unsigned char *img, int taille )
{
  int i;
  int index = 0;
  for( i=0; i< taille; i++ ) {
    pixels[index++] = img[i];
    pixels[index++] = img[i];
    pixels[index++] = img[i];
    pixels[index++] = 0xFF;
  }

  if( imageSDL != NULL ) {
    SDL_FreeSurface( imageSDL );
  }
  imageSDL = SDL_CreateRGBSurfaceFrom( pixels, width, height, 32, width*4,
				       rmask, gmask, bmask, amask);
  if(imageSDL == NULL) {
    fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
    exit(1);
  }
  printf( "SDLImage Created width=%d, heigth=%d\n", imageSDL->w, imageSDL->h);
  // Draws the image on the screen:
  /* Blit onto the screen surface */
  if(SDL_BlitSurface(imageSDL, NULL, screen, NULL) < 0)
    fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
  SDL_UpdateRect(screen, 0, 0, imageSDL->w, imageSDL->h);

  return 0;
}
int RGBtoPixels( unsigned char *img, int taille )
{
  int i;
  int index = 0;
  for( i=0; i< taille; i++ ) {
    pixels[index++] = img[i];
    if( ((i+1) % 3) == 0) {
      pixels[index++] = 0xFF;
    }
  }

  if( imageSDL != NULL ) {
    SDL_FreeSurface( imageSDL );
  }
  imageSDL = SDL_CreateRGBSurfaceFrom( pixels, width, height, 32, width*4,
				       rmask, gmask, bmask, amask);
  if(imageSDL == NULL) {
    fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
    exit(1);
  }
  printf( "SDLImage Created width=%d, heigth=%d\n", imageSDL->w, imageSDL->h);
  // Draws the image on the screen:
  /* Blit onto the screen surface */
  if(SDL_BlitSurface(imageSDL, NULL, screen, NULL) < 0)
    fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
  SDL_UpdateRect(screen, 0, 0, imageSDL->w, imageSDL->h);
  return 0;
}
/**
 * Draw a rectangle around target and a cross at its center.
 */
int drawTarget( Tobjet *target )
{
  Sint16 xmin = max( 0, target->x - CROSS_RADIUS );
  Sint16 xmax = min( imageSDL->w, target->x + CROSS_RADIUS );

  Sint16 ymin = max( 0, target->y - CROSS_RADIUS );
  Sint16 ymax = min( imageSDL->w, target->y + CROSS_RADIUS );

  hlineColor( screen, xmin, xmax, target->y, SDL_GFX_GREEN);
  vlineColor( screen, target->x, ymin, ymax, SDL_GFX_GREEN);

  rectangleColor( screen, target->gauche, target->haut, 
		  target->droite, target->bas, SDL_GFX_GREEN );
  
  SDL_UpdateRect(screen, 0, 0, imageSDL->w, imageSDL->h);
  
  return 1;
}


/*
 * Transforme une image RVB stock�e dans un tableau 3D en coordonn�es HSL (teinte seulement)
 * H : Hue (coloration, teinte)
 * S : Saturation
 * L : Luminance
 *
 * L'algo de transformation a �t� trouv� sur wikipedia
 * http://fr.wikipedia.org/wiki/Codage_informatique_des_couleurs#Formules_de_changement_de_syst�me_de_codage
 *
 * image : image RVB d'entree
 * teinte : buffer contenant les infos sur la teinte
 */
void rvb2hsl(unsigned char *image, Tpx teinte[480][640], int width, int height){

	//pour le cacul de la teinte
	int T0, delta; // delta=max(r,v,b)-min(r,v,b)
	unsigned char cst=TMAX; // pour la formule du calcul de la teinte

	unsigned char r, v, b; // couleurs
	int i=0, j; //parcours du tableau hsl
	int temp=0; //parcours du tableau image

	//on traite l'image pixel par pixel
	while(i<height){
		j=0;
		//if(i%10 == 0) printf("ligne %d\n", i);
		while(j<width){
			//recuperation des composantes rvb
			r=image[temp++];
			v=image[temp++];
			b=image[temp++];
			temp += 3*(INC-1);

			if( r == v && v == b ) {
			  // cas special
			  T0 = 0;
			}
			else if(r >= v && r >= b) {
				delta = (v > b)? r-b : r-v;
				T0 = cst*(v-b)/ (6*delta);
			} else if(v >= b && v >= r) {
				delta = (b > r)? v-r : v-b;
				T0 = cst*((b-r)+2*delta)/ (6*delta);
			} else {
				delta = (r > v)? b-v : b-r;
				T0 = cst*((r-v)+4*delta)/ (6*delta);
			}
			   
			//T = ((T0)*cst); // cst = TMAX/6 avec TMAX = 255
			//T = (unsigned char)(T%TMAX);
			
			//remplir le tableau hsl avec seulement la composante h
			teinte[i][j].valeur=T0;
			teinte[i][j].traite=0;
			// on avance dans la ligne
			j++;  
		} // while(width)
		// on change de ligne
		i += INC;
	}// while(height)
}// rvb2hsl()

int HfromRGB( unsigned char r, unsigned char v, unsigned char b) 
{
  int T0, delta; // delta=max(r,v,b)-min(r,v,b)
  unsigned char cst=TMAX; // pour la formule du calcul de la teinte

  if( r == v && v == b ) {
    // cas special
    T0 = 0;
  }
  else if(r >= v && r >= b) {
    delta = (v > b)? r-b : r-v;
    T0 = cst*(v-b)/ (6*delta);
  } else if(v >= b && v >= r) {
    delta = (b > r)? v-r : v-b;
    T0 = cst*((b-r)+2*delta)/ (6*delta);
  } else {
    delta = (r > v)? b-v : b-r;
    T0 = cst*((r-v)+4*delta)/ (6*delta);
  }

  return T0;
}

/*
 * Recherche des voisins : on cherche ligne par ligne, les pixels � gauche puis ceux � droite du pixel courant
 * x et y sont les coordonn�es de d�part pour la recherche
 * inf et sup sont les bornes de la zone de couleur recherch�e
 */
void voisins(Tpx teinte[480][640], int x, int y, int inf, int sup, int width, int height){

	int i=x,j;  // pour le parcours du tableau hsl
	int b; // booleen disant s'il faut continuer dans la direction en cours
	int nbpx=0, nb=-1; //nbpx : nb de pixels trouv�s dans la zone, nb : nb de pixels trouv�s dans la boucle precedente, comme �a, on peut sortitr d�s qu'il n'y a plus d'�volution
	int haut, bas, droite, gauche;

	char answer[15]; // DEBUG

	haut=x;
	gauche=y;
	droite=y;

	while(i<height && nbpx>nb){
		j=y;
		nb=nbpx;
		/*on regarde les pixels pr�c�dents du pixel courant sur la meme ligne*/
		b=1;
		while(j>0 && b==1){
			if(teinte[i][j].traite==0){
				if(teinte[i][j].valeur>=inf && teinte[i][j].valeur<=sup){ //c'est la couleur recherch�e
					prec=1;
					nbpx+=sautVoisin;
				}else{
					if(prec==0)
						b=0;
					else prec--;
				} 
				teinte[i][j].traite=1; //le pixel a �t� analys�
			}
			//regarder gauche
			if(j<gauche)
				gauche=j;
			j-=sautVoisin;
		}
		/*on regarde les pixels successeurs du pixel courant sur la meme ligne*/
		j=y;
		b=1;
		while(j<width && b==1){
			if(teinte[i][j].traite==0){
				if(teinte[i][j].valeur>=inf && teinte[i][j].valeur<=sup){ //c'est la couleur recherch�e
					prec=1;
					nbpx+=sautVoisin;
				}else{
					if(prec==0)
						b=0;
					else prec--;
				}
				teinte[i][j].traite=1; //le pixel a �t� analys�
			} 
			//regarder droite
			if(j>droite)
				droite=j;
			j+=sautVoisin;
		}
		i+=sautVoisin;
	}
	bas=i-1;

	//	printf("nbpx=%d h=%d\n",nbpx, haut);

	/* on regarde si c'est un objet assez grand, dans ce cas on recupere les coordonn�es du centre*/ 
	if(nbpx>tailleObjet){  // si le nombre de pixels de cet objet est suffisant
	  //int coul=200;
	  //int l=0;
		objets[nbelt].taille=nbpx;
		objets[nbelt].x=(gauche+droite)/2;
		objets[nbelt].y=(haut+bas)/2;
		objets[nbelt].haut = bas;
		objets[nbelt].bas = haut;
		objets[nbelt].gauche = gauche;
		objets[nbelt].droite = droite;
		//printf("nb pixels=%d\ncentre : x=%d y=%d\nhaut=%d bas=%d gauche=%d droite=%d\n\n",nbpx,objets[nbelt].x,objets[nbelt].y,haut,bas,gauche,droite);

		//TEST
		//HtoPixels( hsv, width*height );
		printTarget( &(objets[nbelt]) );
		// try to infer a SDL_surface from JPEG in Memory
		//displayImage( rw );
		drawTarget( &(objets[nbelt]) );

		printf( "Next...[return]\n");
		scanf( "%s", answer );

		//dessiner une croix sur le centre 
		/* x=objets[nbelt].x; */
/* 		y=objets[nbelt].y; */
/* 		for(l=x-2;l<x+3;l++) */
/* 			for(j=y-2;j<y+3;j++){ */
/* 				teinte[l][j].valeur=coul; */
/* 			}  */
/* 		teinte[x][y].valeur=TMAX;  */
		//FTEST

		nbelt++;
	}
	
}

/**
 * Print info about target Tobjet.
 */
void printTarget( Tobjet *target )
{
  printf( "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", target->haut, target->bas, target->gauche, target->droite);
  printf( "       x=%d, y=%d, taille=%d\n", target->x, target->y, target->taille);
}


int main( int argc, char *argv[] )
{
  int cmin, cmax;
  int x,y,i,j,index;
  int xm, ym;

  SDL_Event event;

  cmin = 213;
  cmax = 239;

/* SDL interprets each pixel as a 32-bit number, so our masks must depend
     on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif
  
  // load file
  chargeImage( argv[1], image, &taille, &width, &height );
  printf( "charg� image, taille=%d, width=%d, height=%d\n", taille, width, height);
 

  /*transformation en h de hsl*/
  printf("rvb2hsl\n");
  rvb2hsl(image, myteinte, width, height);

  // into ppm format
  printf( "// into ppm format\n");
  for( y=0; y<height; y++) {
    for( x=0; x < width; x++ ) {
      hsv[y*width+x] = myteinte[y][x].valeur;
      filtre[y*width+x] = myteinte[y][x].valeur;
    }
  }
  
  printf( "// write teinte.ppm \n");
  ecrireImage( "teinte.ppm", hsv, width*height, width, height );

  if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  }
  /* Initialize the best video mode */
  /* Have a preference for 24-bit, but accept any depth */
  screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE|SDL_ANYFORMAT);
  if ( screen == NULL ) {
    fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
	    SDL_GetError());
    exit(1);
  }
  printf("Set 640x480 at %d bits-per-pixel mode\n",screen->format->BitsPerPixel);
  depth = screen->format->BitsPerPixel;
  

  HtoPixels( hsv, width*height );
  
  while(1) {
    SDL_WaitEvent(&event);
    
    switch (event.type) {
    case SDL_QUIT:
      printf("Quitting...\n");
      SDL_FreeSurface ( imageSDL );
      exit(0);
    case SDL_KEYDOWN:
      printf("The %s key was pressed!\n",
	     SDL_GetKeyName(event.key.keysym.sym));
      if(event.key.keysym.sym==SDLK_ESCAPE) {
	printf("Quitting...\n");
	SDL_FreeSurface ( imageSDL );
	exit(0);
      }
      if(event.key.keysym.sym==SDLK_o) {
	//printf( "Original image\n");
	RGBtoPixels( image, taille );
      }
      else if(event.key.keysym.sym==SDLK_h) {
	//printf( "H(sv) image\n");
	HtoPixels( hsv, width*height );
      }
      else if(event.key.keysym.sym==SDLK_f) {
	printf( "couleur min max : "); 
	scanf( "%d %d", &cmin, &cmax );
	for( i=0; i<width*height; i++) {
	  if( (hsv[i] >= cmin) && (hsv[i] <= cmax)) {
	    filtre[i] = 0xff;
	  }
	  else {
	    filtre[i] = 0;
	  }
	}
	HtoPixels( filtre,  width*height );
      }
      else if(event.key.keysym.sym==SDLK_t) {
	HtoPixels( hsv, width*height );
	i = 0;
	j = 0;
	while(i<height){
	  j=0;
	  while(j<width){
	    if(myteinte[i][j].traite==0){
	      if(myteinte[i][j].valeur>=cmin && myteinte[i][j].valeur<=cmax){  //c est la couleur recherch�e
		voisins(myteinte,i,j,cmin,cmax, width, height);
	      } 
	    }
	    j+=saut;  
	  }
	  i+=saut;
	}


	int objTaille = 0;
	int index = 0;
	for(i=0; i<nbelt; i++)
	  {
	    if(objets[i].taille > objTaille)
	      {
		index = i;
		objTaille = objets[i].taille;
	      }
	  }
	printf( "Plus grande target\n");
	printTarget( &(objets[index]) );
	drawTarget( &(objets[index]) );
	
      }
      break;
      
    case SDL_MOUSEBUTTONDOWN:
      xm = event.button.x;
      ym = event.button.y;
      printf( "x=%d, y=%d ---> teinte=%d\n", xm, ym, hsv[ym*width+xm]);
    }    
  }

  return 0;
}
