/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * Programme de demonstration de la libwb.
 * Reconnaissance d'une balle de couleur et suivi par la camera du robot.
 * Server pour envoyer les donnees de la cible sur port 16000.
 *  when "save" is send to the server, the current image and info are saved
 *  (debugImage.ppm and debugInfo.txt)
 *  otherwise info about the actual position of the target can be asked
 *  through a socket (see viewer.c).
 * Debug level
 *  0 : no debug
 *  1 : with all msg between server and client
 *  2 : just print info about target position and camera movement, but the
 *      camera does not move.
 *
 * Parametres a considerer:
 *  - char prec : marge d'erreur
 *  - bornes pour la teinte (H) : 213, 229
 *
 *	/!\ --- LES FLOATS, C'EST MAAAAAL --- /!\
 *	Un calcul en float (tout ptit le calcul) dans rvb2hsl
 *	entraine un facteur x20 dans le temps d'ex�cution /!\
 */


#include "suitBalle.h"
#include "libwb/wb.h"

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>


#define INC 1


/*variables globales*/
Tobjet objets[500]; //les objets r�cup�r�s apr�s traitement
int nbelt;
Tobjet obj; // will be shared by main thread and fn_thread_com
pthread_mutex_t	mutex_obj = PTHREAD_MUTEX_INITIALIZER;

/* variables de param�trage*/
int tailleObjet=500; //taille minimum pour prendre en compte un objet trouv�
char prec=2; // nb de pixels analys�s pouvant ne pas etre de la couleur demand�e, donc marge d'erreur

#define NB_THREADS	2
void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);

ssize_t sendMsg(int socket, char *msg);
ssize_t sendData(int socket, Tobjet *data);

void printTarget( Tobjet *target );

void dieWithErrorMsg( char *msg);
void dieWithErrorNb(int ret);

static int connected=0;
pthread_t thread [NB_THREADS];
int dog=0;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;

#define MAXPENDING 5    /* Maximum outstanding connection requests */
static int servSock;                    /* Socket descriptor for server */
static int clntSock;                    /* Socket descriptor for client */
static struct sockaddr_in echoServAddr; /* Local address */
static struct sockaddr_in echoClntAddr; /* Client address */
static unsigned short echoServPort=16000;     /* Server port */
static unsigned int clntLen;            /* Length of client address data structure */

static int recvMsgSize=0; 
static unsigned char buffso_rcv[32];

int debug;
int debug_exit;

int taille, width, height;


/*
 * Transforme une image RVB stock�e dans un tableau 3D en coordonn�es HSL (teinte seulement)
 * H : Hue (coloration, teinte)
 * S : Saturation
 * L : Luminance
 *
 * L'algo de transformation a �t� trouv� sur wikipedia
 * http://fr.wikipedia.org/wiki/Codage_informatique_des_couleurs#Formules_de_changement_de_syst�me_de_codage
 *
 * image : image RVB d'entree
 * teinte : buffer contenant les infos sur la teinte
 */
void rvb2hsl(unsigned char *image, Tpx teinte[480][640], int width, int height){

	//pour le cacul de la teinte
	int T0, delta; // delta=max(r,v,b)-min(r,v,b)
	unsigned char cst=TMAX; // pour la formule du calcul de la teinte

	unsigned char r, v, b; // couleurs
	int i=0, j; //parcours du tableau hsl
	int temp=0; //parcours du tableau image

	//on traite l'image pixel par pixel
	while(i<height){
		j=0;
		//if(i%10 == 0) printf("ligne %d\n", i);
		while(j<width){
			//recuperation des composantes rvb
			r=image[temp++];
			v=image[temp++];
			b=image[temp++];
			temp += 3*(INC-1);

			if( r == v && v == b ) {
			  // cas special
			  T0 = 0;
			}
			else if(r >= v && r >= b) {
				delta = (v > b)? r-b : r-v;
				T0 = cst*(v-b)/ (6*delta);
			} else if(v >= b && v >= r) {
				delta = (b > r)? v-r : v-b;
				T0 = cst*((b-r)+2*delta)/ (6*delta);
			} else {
				delta = (r > v)? b-v : b-r;
				T0 = cst*((r-v)+4*delta)/ (6*delta);
			}
			   
			//T = ((T0)*cst); // cst = TMAX/6 avec TMAX = 255
			//T = (unsigned char)(T%TMAX);
			
			//remplir le tableau hsl avec seulement la composante h
			teinte[i][j].valeur=T0;
			teinte[i][j].traite=0;
			// on avance dans la ligne
			j++;  
		} // while(width)
		// on change de ligne
		i += INC;
	}// while(height)
}// rvb2hsl()

/*
 * Recherche des voisins : on cherche ligne par ligne, les pixels � gauche puis ceux � droite du pixel courant
 * x et y sont les coordonn�es de d�part pour la recherche
 * inf et sup sont les bornes de la zone de couleur recherch�e
 */
void voisins(Tpx teinte[480][640], int x, int y, int inf, int sup, int width, int height){

	int i=x,j;  // pour le parcours du tableau hsl
	int b; // booleen disant s'il faut continuer dans la direction en cours
	int nbpx=0, nb=-1; //nbpx : nb de pixels trouv�s dans la zone, nb : nb de pixels trouv�s dans la boucle precedente, comme �a, on peut sortitr d�s qu'il n'y a plus d'�volution
	int haut, bas, droite, gauche;

	haut=x;
	gauche=y;
	droite=y;

	while(i<height && nbpx>nb){
		j=y;
		nb=nbpx;
		/*on regarde les pixels pr�c�dents du pixel courant sur la meme ligne*/
		b=1;
		while(j>0 && b==1){
			if(teinte[i][j].traite==0){
				if(teinte[i][j].valeur>=inf && teinte[i][j].valeur<=sup){ //c'est la couleur recherch�e
					prec=1;
					nbpx+=sautVoisin;
				}else{
					if(prec==0)
						b=0;
					else prec--;
				} 
				teinte[i][j].traite=1; //le pixel a �t� analys�
			}
			//regarder gauche
			if(j<gauche)
				gauche=j;
			j-=sautVoisin;
		}
		/*on regarde les pixels successeurs du pixel courant sur la meme ligne*/
		j=y;
		b=1;
		while(j<width && b==1){
			if(teinte[i][j].traite==0){
				if(teinte[i][j].valeur>=inf && teinte[i][j].valeur<=sup){ //c'est la couleur recherch�e
					prec=1;
					nbpx+=sautVoisin;
				}else{
					if(prec==0)
						b=0;
					else prec--;
				}
				teinte[i][j].traite=1; //le pixel a �t� analys�
			} 
			//regarder droite
			if(j>droite)
				droite=j;
			j+=sautVoisin;
		}
		i+=sautVoisin;
	}
	bas=i-1;

	//	printf("nbpx=%d h=%d\n",nbpx, haut);

	/* on regarde si c'est un objet assez grand, dans ce cas on recupere les coordonn�es du centre*/ 
	if(nbpx>tailleObjet){  // si le nombre de pixels de cet objet est suffisant
	  //int coul=200;
	  //int l=0;
		objets[nbelt].taille=nbpx;
		objets[nbelt].x=(gauche+droite)/2;
		objets[nbelt].y=(haut+bas)/2;
		objets[nbelt].haut = bas;
		objets[nbelt].bas = haut;
		objets[nbelt].gauche = gauche;
		objets[nbelt].droite = droite;
		//printf("nb pixels=%d\ncentre : x=%d y=%d\nhaut=%d bas=%d gauche=%d droite=%d\n\n",nbpx,objets[nbelt].x,objets[nbelt].y,haut,bas,gauche,droite);

		//TEST

		//dessiner une croix sur le centre 
		/* x=objets[nbelt].x; */
/* 		y=objets[nbelt].y; */
/* 		for(l=x-2;l<x+3;l++) */
/* 			for(j=y-2;j<y+3;j++){ */
/* 				teinte[l][j].valeur=coul; */
/* 			}  */
/* 		teinte[x][y].valeur=TMAX;  */
		//FTEST

		nbelt++;
	}
}


/* 
 * Traitement de l'image
 * retourne le num de l'objet s�lectionn�, c'est-�-dire celui qui est le plus proche
 */
int traitementImage(unsigned char *image, Tpx teinte[480][640], int inf, int sup){

	int i=0, j;
	int index;
	/* 100ko l'image compress�e c'est d�j� pas mal */
	unsigned char buffer[100000];
	nbelt = 0;

	/*recup�ration de l'image rvb*/

	//printf("getImage\n");
	taille = getImage(buffer);

	//printf("decompresseImage\n");
	taille = decompresseImage(image, buffer, taille, &width, &height);
	//width = 640;
	//height = 480;
	if (debug_exit == 1 ) {
	  printf( "sauvegarde de l'image\n" );
	  ecrireImage( "debugImage.ppm", image, taille, width, height);
	}
	
	/*transformation en h de hsl*/
	//printf("rvb2hsl\n");
	rvb2hsl(image, teinte, width, height);

	/* on defini la taille des objets cherch�s A REVOIR */
	//tailleObjet = (height+width)/2-120;

	/*recherche des objet de la couleur demand�e*/
	/*traitement de l'image dans le tableau hsl*/
	//printf("recherche des objets\n");
	while(i<height){
		j=0;
		while(j<width){
			if(teinte[i][j].traite==0){
				if(teinte[i][j].valeur>=inf && teinte[i][j].valeur<=sup){  //c est la couleur recherch�e
					voisins(teinte,i,j,inf,sup, width, height);
				} 
			}
			j+=saut;  
		}
		i+=saut;
	}


	taille = 0;
	index = 0;
	for(i=0; i<nbelt; i++)
	{
		if(objets[i].taille > taille)
		{
			index = i;
			taille = objets[i].taille;
		}
	}
	return index;
}


/*
 * Here starts the fun !
 */
int main(int argc, char *argv[])
{

  Tpx teinte[480][640];
  unsigned char image[480][640][3];
  int index_objet;
  SENS sens;
  int pan, tilt;
  // defined for 480 x 640 image
  // must be redifined after the size of the image is known.
  // pthaut -------------> x
  //   |
  // y |
  //   |
  //   V               pt bas
  //  
  int xbas = 210;
  int xhaut = 430;
  int ybas = 320;
  int yhaut = 320;
  int socket = -1;

  int ret;
  FILE *fp;

  debug_exit = 0;
  debug = 0;
  if( argc > 1 ) {
      debug = atoi(argv[1]);
  } 

  printf("%s running with debug=%d\n", argv[0],debug);


  // Alain : thread[0] est un chien de garde pour fermer le thread de connection
  // si ouvert trop longtemps...
  if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
    dieWithErrorNb( ret );
  }
  // Alain : thread[1] est le thread de connection
  if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
    dieWithErrorNb( ret );
  }

  while(1)
    {
      /* On r�cup�re la teinte pr�dominante dans l'image pour ensuite
       * suivre l'objet le plus gros ayant cette teinte */
      // en fait pour l'instant on se limite au rose (213 -> 229)
      index_objet = traitementImage((unsigned char*)image, teinte, 213, 229);
      // to ensure mutex
      pthread_mutex_lock (& mutex_obj);
      obj = objets[index_objet];
      pthread_mutex_unlock (& mutex_obj);

      // on a l'index de l'objet le plus proche, maintenant faut il suivre
      /*	printf("Objet %d [%d %d %d %d] %d pixels (%d,%d)\n", index_objet,
		obj.haut, obj.gauche, obj.bas, obj.droite,
		obj.taille, obj.x, obj.y);
      */
      // recherche d'un objet, choix nous donne celui qui a ete s�lectionn�
      //		printf("nb elt : %d\n",nbelt);

      /* Ici on a un objet, on peut se caler dessus */
      /* Attention, le "haut" de l'image est vers les x d�croissants */
      if(nbelt > 0)
	{
	  //redefinition des coordonn�es image
	  xhaut = width/3; yhaut = height/3;
	  xbas = 2*width/3; ybas = 2*height/3;
	  // Diff�rents cas � analyser
	  pan = 1;
	  tilt = 1;
	  sens = SENS_Unknown;
	  if(obj.y < yhaut && obj.x < xhaut) sens = TopLeft;
	  else if(obj.y > ybas && obj.x < xhaut) sens = BottomLeft;
	  else if(obj.x < xhaut) sens = Left;
	  else if(obj.y < yhaut && obj.x > xbas) sens = TopRight;
	  else if(obj.y > ybas && obj.x > xbas) sens = BottomRight;
	  else if(obj.x > xbas) sens = Right;
	  else if(obj.y < yhaut) sens = Top;
	  else if(obj.y > ybas) sens = Bottom;
	  else sens = SENS_Unknown;
	  if( debug != 0 ) {
		  printf("Target x=%d, y=%d\n", obj.x, obj.y );
		  printf( "camera xhaut=%d, yhaut=%d, xbas=%d, ybas=%d\n", xhaut, yhaut, xbas, ybas);
	  }

	  /* IMPORTANT si on utilise PanTiltMoveCameraNoSleep, il faut refermer la socket � la main IMPORTANT */
	  if(socket != -1) { close(socket); socket = -1; }
	  if(sens != SENS_Unknown)
	    {
	      if( debug != 0 ) {
		  printf("Move Camera sens %d pan %d tilt %d\n", sens, pan, tilt);
		}
		else if ( debug == 0 ){
		  PanTiltMoveCameraNoSleep(pan,tilt,sens);
		}
	    }

	  // debug
	  if( debug_exit == 1 ) {
	    // write info to file
	    printf( "write info to file\n");
	    fp = fopen( "debugInfo.txt", "w");
	    if( fp == NULL ) {
	      dieWithErrorNb( errno );
	    }
	    fprintf( fp, "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", obj.haut, obj.bas, obj.gauche, obj.droite);
	    fprintf( fp, "       x=%d, y=%d, taille=%d\n", obj.x, obj.y, obj.taille);
	    fprintf( fp, "camera xhaut=%d, yhaut=%d, xbas=%d, ybas=%d\n", xhaut, yhaut, xbas, ybas);
	    fprintf( fp, "       sens=%d, pan=%d, tilt=%d\n", sens, pan, tilt);
	    fclose( fp );

	    dieWithErrorMsg( "On a eu un 'debug_exit'");
	  }
	  
	    
	}
    }

  pthread_join (thread [0], NULL);//blocking
  pthread_join (thread [1], NULL);//blocking

  return 0;
}


/**
 * WatchDog for the Com Thread.
 * Every 3 seconds, variable 'dog' is incremented. If 'dog' reaches 5, it ends
 * everything.
 */
void * fn_thread_dog (void * num)
{
  int numero = (int) num;
	
  while (1) {
    usleep (3 * 100000);
    pthread_mutex_lock (& mutex_dog);
    dog ++;
    pthread_mutex_unlock (& mutex_dog);
    if (dog>5) {
      dog=0;
      if (connected) {
	connected=0;
        int ret=pthread_cancel (thread[1]);
       	close(clntSock);
	close(servSock);
	sleep(2);
	if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
	  dieWithErrorNb( ret );
	}
      }
    }
  }//end while
  pthread_exit (NULL);
}
/**
 * Thread for com. Open a socket, wait for connection, create socket with 
 * client and then listen and answers commands.
 */
void * fn_thread_com (void * num) 
{
  int numero = (int) num;

  /* Create socket for incoming connections (ip(7)*/
  if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    fprintf( stderr, "socket failed()\n");
    //dieWithErrorMsg("socket failed()");
  }

  /* Construct local address structure */
  memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
  echoServAddr.sin_family = AF_INET;                /* Internet address family */
  echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
  echoServAddr.sin_port = htons(echoServPort);      /* Local port */

  /* Bind to the local address */
  int autorisation=1;
  setsockopt(servSock,SOL_SOCKET,SO_REUSEADDR,&autorisation,sizeof(int));

  if (bind(servSock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) {
    fprintf(stderr, "bind()failed\n");
    //dieWithErrorMsg("bind() failed");
  }

  for (;;) /* Run forever */
    {       
      /* Mark the socket so it will listen for incoming connections */
      if (listen(servSock, MAXPENDING) < 0) {
        printf("listen() failed\n");
	//dieWithErrorMsg("listen() failed");
      }

      /* Set the size of the in-out parameter */
      clntLen = sizeof(echoClntAddr);
      /* Wait for a client to connect */
      if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0) {
	fprintf( stderr, "accept() failed\n");
	dieWithErrorMsg("accept() failed");
      }

      /* clntSock is connected to a client! */
      do{
	/* Wait to receive adequate msg ("target")*/
        if ((recvMsgSize = recv(clntSock, buffso_rcv, 5, 0)) < 1) {
	  shutdown(clntSock,1);
	}
	else {
	  pthread_mutex_lock (& mutex_dog);
	  connected=1;	
	  dog =0;
	  pthread_mutex_unlock (& mutex_dog);
	  if( strcmp( (char *)buffso_rcv, "save" ) == 0 ) {
	    if( debug > 0 ) {
	      printf( "recognised : -%s-\n", buffso_rcv);
	    }
	    debug_exit = 1;
	  }
	  /* Answer the request */
	  if( debug == 1 ) {
	    printf("server rcvd: [%s]\n",buffso_rcv);
	  }
	  /* TODO */
	  //sendMsg( clntSock, "s->c");
	  sendData( clntSock, &obj );
	  //printf("server send: [%s]\n",buffMsg);
	  if( debug == 1 ) {
	    printf("server send:\n");
	    printTarget( &obj );
	  }
	}
      }
      while(recvMsgSize>0);
      
      connected=0;
      sleep(1);
    }//end for(;;)
  pthread_exit (NULL);
}//end thread


/**
 * Function used to send DATA over the socket.
 * 
 * @return length of data send
 */
ssize_t sendData(int socket, Tobjet *data)
{
  Tobjet bufTobjet;

  // perform data conversion (compatible high/low endian)

  bufTobjet.haut = htonl(data->haut);
  bufTobjet.bas = htonl(data->bas);
  bufTobjet.gauche = htonl(data->gauche);
  bufTobjet.droite = htonl(data->droite);
  bufTobjet.taille = htonl(data->taille);
  bufTobjet.x = htonl(data->x);
  bufTobjet.y = htonl(data->y);
  
  return send( socket, (void *) &bufTobjet, sizeof(Tobjet), 0);
}

/**
 * Print info about target Tobjet.
 */
void printTarget( Tobjet *target )
{
  printf( "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", target->haut, target->bas, target->gauche, target->droite);
  printf( "       x=%d, y=%d, taille=%d\n", target->x, target->y, target->taille);
}

/**
 * Die while emiting an error msg.
 */
void dieWithErrorMsg( char *msg)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }

  close(clntSock);
  close(servSock);
  fprintf (stderr, "Program exit with error: %s\n", msg);
  exit (1);
}
/**
 * Die with an error msg number.
 */
void dieWithErrorNb(int ret)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }
  close(clntSock);
  close(servSock);
  fprintf (stderr, "Program exits with error: %s\n", strerror (ret));
  exit (1);
}
