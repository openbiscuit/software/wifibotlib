** libwb **
***********

'libwb' is a low level library for using and controling a set of WifiBot.

30/01/2007

* Layout
--------

README.txt			this file
TODO.txt			what seems important to do, add.

+ doc				documentation
|  |
|  + tex			Latex source files
|
+ src				source file for libwb
|  Makefile			to compile libwb
|
|
+ include			include files for local library 
|  |				(like libjpeg)
|  |
|  + libwb			include files for libwb library
|
+ lib				library files
|				(external like libjpeg) and created
|
+ dist				needed to set up released packages
|
|
+ demo				various small demo programs
  testCamera			a simple test of the camera;
   braitenberg			the wifibot moves like a Braitenberg Bot.
   stop				the wifibot stops.
   |
   + suitBalle			the robot camera follows a pink ball.
      simpleServer		example of a server on the robot side
      simpleClient		example of associated client
      suitBalleServer		onboard demo of ball following + server
      viewer			associated viewer
      debugSize			debug software for data compatibility test
      debugTraitement		debug software for testing 'suitBalleServer'


* Original authors
------------------
Julien Le Guen and C�dric Bernier, summer 2006 in the MAIA team of Loria. 

