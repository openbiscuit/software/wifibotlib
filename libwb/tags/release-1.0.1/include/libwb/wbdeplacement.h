/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**                         Maia, Loria.
**
** Original authors: C�dric Bernier, Julien Le Guen.
**  
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/
/* Fichier ent�te pour le wifibot 4G */

/*
 * ajouter des fonctions en m/s, acceleration, etc...
 */

#ifndef H_WB_DEPLACEMENT
#define H_WB_DEPLACEMENT

#include "wbi2c.h"

/* D�claration des fonctions moyen-bas niveau */
void setMode(unsigned char m);
int setSpeed(unsigned char vg, unsigned char vd, unsigned char sens);
int avancerCourbe(unsigned char vg, unsigned char vd);
int avancer(unsigned char vitesse);
int reculerCourbe(unsigned char vg, unsigned char vd);
int reculer(unsigned char vitesse);
int stop(void);
int tournerHoraire(unsigned char vitesse);
int tournerAntiHoraire(unsigned char vitesse);

#endif
