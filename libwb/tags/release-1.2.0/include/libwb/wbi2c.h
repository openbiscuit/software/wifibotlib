/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**                         Maia, Loria.
**
** Original authors: C�dric Bernier, Julien Le Guen.
**  
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/
/* Fonctions I2C "bas niveau" pour le wifibot 4G
 * 
 * Ces fonctions permettent de contr�ler et de r�cup�rer
 * les information du robot via le bus I2C de la carte
 * mere, qui joue le r�le du maitre du bus.
 * Plusieurs esclaves sont pr�sents sur le bus :
 *  - une carte qui contr�le le niveau de la batterie
 *    que l'on peut interroger
 *  - une carte par moiti� du robot (gauche et droite)
 *    qui contr�le les deux moteurs, et r�cup�re les
 *    informations de vitesse des moteurs ainsi que la
 *    valeur renvoy�e par le capteur de distance IR.
 *
 * Il faut initialiser le bus I2C au d�but du programme,
 * avec la fonction initI2C. Le num�ro du bus est d�fini
 * dans le header.
 *
 * Ensuite on peut modifier la vitesse de chaque cot�
 * avec la fonction setI2CSpeed(adresse, vitesse).
 * L'adresse doit �tre une adresse de carte valide
 * (ADR_LEFT ou ADR_RIGHT) et vitesse doit �tre construit
 * de cette fa�on :
 * - bit 7:	Speed Motor Control
 *  		Permet d'activer le PID des roues
 *  		Asservissement de la vitesse des roues
 *  		par rapport � la consigne.
 *  		1: ON
 *  		0: OFF (par d�faut)
 * - bit 6: Sens des roues
 *   		0: Backward
 *   		1: Forward
 * - bits 5 � 0: Vitesse des roues
 *   		De 0 � 60 en mode "libre"
 *   		De 0 � 40 en mode PID
 *
 * Pour contr�ler le robot, on peut lire la vitesse de
 * chaque roue. Pour cela il faut utiliser la fonction
 * getI2CInfo(adresse). Adresse doit �tre une adresse
 * valide. La fonction met � jour les champs
 * correspondant au c�t� du robot dans la structure
 * winfo de type WifibotInfo. Elle r�cup�re :
 *  - vitesse de la roue avant
 *  - vitesse de la roue arri�re
 *  - valeur du capteur de distance associ� au cot�
 *
 * On peut �galement contr�ler le niveau de la batterie
 * du robot grace � la fonction getI2CBattery().
 * Elle met � jour le champ battery de la structure
 * winfo.
 *  
 * Il est recommand� d'utiliser le mode PID pour le
 * controle de la vitesse, car le robot v�rifie en
 * permanence la vitesse des roues et r�agit bien mieux
 * aux changements de consigne.
 */

#ifndef H_WB_I2C
#define H_WB_I2C

/*
 * Defines 
 */

/* Num�ro du bus I2C */
#define WB_NUM_BUS 0

/* Adresses des diff�rents �l�ments */
/**
 * @name WB_ADRR
 * 
 * Addresses of various elements on the I2C bus.
 */
//@{
#define WB_ADR_LEFT 0x51	/* cot� gauche du robot */
#define WB_ADR_RIGHT 0x52	/* cot� droit du robot */
#define WB_ADR_BAT 0x4e	/* la batterie */
//@}

/* Valeurs retourn�es par les fonctions */
/**
 * @name WB_I2C
 *
 * Return values of wbi2c functions.
 */
//@{
#define WB_I2C_OK	0
#define WB_I2C_OPEN_ERROR	1
#define WB_I2C_IO_ERROR	2
#define WB_I2C_WRITE_ERROR	3
#define WB_I2C_READ_ERROR	4
#define WB_I2C_ADR_ERROR	5
//@}

/* Flags de controle des moteurs */
//#define MOT_CTRL_ON		0x80
//#define MOT_CTRL_OFF	0x00
//#define MOT_FORWARD		0x40
//#define MOT_BACKWARD	0x00


/* 
 * Structure d�finissant les commandes � envoyer
 */
//typedef struct cmd {
//	unsigned char left;	/* vitesse cot� gauche */
//	unsigned char right;/* vitesse cot� droit */
//}WifibotCmd;


/* 
 * Structure d�finissant les informations re�ues
 */
/**
 * Structure for information gathered on the wifibot
 */
typedef struct info {
	unsigned char battery;	/* charge de la batterie */
	unsigned char frontLeft; /* vitesse roue avant gauche */
	unsigned char rearLeft; /* vitesse roue arri�re gauche */
	unsigned char frontRight; /* vitesse roue avant droite */
	unsigned char rearRight; /* vitesse roue arri�re droite */
	unsigned char irLeft; /* valeur t�l�m�tre gauche */
	unsigned char irRight; /* valeur t�l�m�tre droit */
}WBInfo;


/* 
 * Variables globales
 */
/** Info about the wifibot (wheel speed, sensor value, battery level) */
extern WBInfo winfo;	/* Structure de controle */

/* 
 * D�claration des fonctions I2C 
 */
int wbInitI2C(int num_bus);
int wbSetI2CSpeed(unsigned char adr, unsigned char speed);
int wbGetI2CInfo(unsigned char adr);
int wbGetI2CBattery(void);

#endif
