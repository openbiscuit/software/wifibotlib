/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   wbdeplacement.c
 * 
 * @brief Functions to control the robot movements.
 *
 * @author C�dric Barnier
 * @author Alain Dutech
 * @author Julien LeGuen
 * @date   Fri Apr 20 15:11:50 2007
 *
 * <b>French comments can be found in the source file</b>.<br>
 */

#include "libwb/wbi2c.h"
#include "libwb/wbdeplacement.h"

unsigned char mode = WB_MOTOR_CTRL_ON;

/*
 * Permet de modifier le mode de fonctionnement du cont�le moteur
 * m doit etre MOT_CTRL_ON ou MOT_CTRL_OFF suivant que l'on veut
 * ou non que l'asservissement soit activ� (ON par d�faut)
 */
/**
 * Set the regulation mode for motor control.
 *
 * Default is set to WB_MOTOR_CTRL_ON.
 *
 * @param m is eihther WB_MOTOR_CTRL_ON or WB_MOTOR_CTRL_OFF
 */
void wbSetMode(unsigned char m)
{
	mode = m;
}


/*
 * Permet de modifier la vitesse de chaque c�t� du robot.
 * vg et vd doivent �tre inf�rieurs � 63, et sens doit �tre
 * MOT_FORWARD ou MOT_BACKWARD.
 */
/**
 * Set the speed of each side of the robot.
 *
 * @param vg left speed (0-63)
 * @param vd right speed (0-63)
 * @param sens either WB_MOTOR_FORWARD or WB_MOTOR_BACKWARD
 *
 * @return WB_I2C_xxx error message
 */
int wbSetSpeed(unsigned char vg, unsigned char vd, unsigned char sens)
{
	int error;
	/* On seuille la vitesse pour �viter d'�craser les flags */
	if(vg > 63) vg = 63;
	if(vd > 63) vd = 63;
	error = wbSetI2CSpeed(WB_ADR_LEFT, mode | sens | vg);
	if(error) return error;
	
	error = wbSetI2CSpeed(WB_ADR_RIGHT, mode | sens | vd);
	return error;
}


/*
 * Permet d'avancer en faisant une courbe, c'est � dire que les 
 * moteurs ne tournent pas � la m�me vitesse.
 */
/**
 * Moves the robot forward, allowing it to turn.
 *
 * @param vg left speed (0-63)
 * @param vd right speed (0-63)
 *
 * @return WB_I2C_xxx error message
 */
int wbAvancerCourbe(unsigned char vg, unsigned char vd)
{
	return wbSetSpeed(vg, vd, WB_MOTOR_FORWARD);
}


/*
 * Permet d'avancer th�oriquement tout droit. Les essais montrent
 * que le robot ne va pas droit lorsqu'on lui donne la m�me vitesse
 * de chaque c�t�.
 */
/**
 * In therory, move straight ahead...
 *
 * @param vitesse speed (0-63)
 *
 * @return WB_I2C_xxx error message
 */
int wbAvancer(unsigned char vitesse)
{
	return wbAvancerCourbe(vitesse, vitesse);
}


/* 
 * Permet de reculer en faisant une courbe
 */
/**
 * Moves the robot backward, allowing it to turn.
 *
 * @param vg left speed (0-63)
 * @param vd right speed (0-63)
 *
 * @return WB_I2C_xxx error message
 */
int wbReculerCourbe(unsigned char vg, unsigned char vd)
{
	return wbSetSpeed(vg, vd, WB_MOTOR_BACKWARD);
}


/*
 * Permet de reculer tout droit. Les m�me limitations qu'avec
 * avancer() sont pr�sentes.
 */
/**
 * In therory, back up straight.
 *
 * @param vitesse speed (0-63)
 *
 * @return WB_I2C_xxx error message
 */
int wbReculer(unsigned char vitesse)
{
	return wbReculerCourbe(vitesse, vitesse);
}


/*
 * Stoppe compl�tement et imm�diatement le robot. Pr�voir un temps
 * de latence entre envoi de la commande et l'arret effectif du robot,
 * notamment du fait de l'inertie du robot et des moteurs.
 */
/**
 * Immediately stops the robot.
 *
 * @return WB_I2C_xxx error message
 */
int wbStop(void)
{
	return wbSetSpeed(0, 0, WB_MOTOR_FORWARD);
}


/* Permet de tourner sur place (sens horaire) */
/**
 * Turn on the spot, clockwise.
 *
 * @param vitesse speed (0-63)
 *
 * @return WB_I2C_xxx error message
 */
int wbTournerHoraire(unsigned char vitesse)
{
	int error;
	/* On seuille la vitesse pour �viter d'�craser les flags */
	if(vitesse > 63) vitesse = 63;
	error = wbSetI2CSpeed(WB_ADR_LEFT, mode | WB_MOTOR_FORWARD | vitesse);
	if(error) return error;
	
	error = wbSetI2CSpeed(WB_ADR_RIGHT, mode | WB_MOTOR_BACKWARD | vitesse);
	return error;
}


/* Permet de tourner sur place (sens antihoraire) */
/**
 * Turn on the spot, conterclockwise.
 *
 * @param vitesse speed (0-63)
 *
 * @return WB_I2C_xxx error message
 */int wbTournerAntiHoraire(unsigned char vitesse)
{
	int error;
	/* On seuille la vitesse pour �viter d'�craser les flags */
	if(vitesse > 63) vitesse = 63;
	error = wbSetI2CSpeed(WB_ADR_LEFT, mode | WB_MOTOR_BACKWARD | vitesse);
	if(error) return error;
	
	error = wbSetI2CSpeed(WB_ADR_RIGHT, mode | WB_MOTOR_FORWARD | vitesse);
	return error;
}

