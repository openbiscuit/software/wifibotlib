#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>


static char filename3[20];
static int i2cbus=0,file;

static unsigned char bufset[10];
static unsigned char bufget[3];
static unsigned char bufad[1];

#define NB_THREADS	1
void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);

static void initI2c();
static void setI2cL();
static void getI2cL();
static void setI2cR();
static void getI2cR();
static void getAD();
static void stop();
void gestionnaire (int numero);


static int compteur = 0;
pthread_t thread [NB_THREADS];
int       i;
int       ret;

int dog=0;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;

#define MAXPENDING 5    /* Maximum outstanding connection requests */
static int servSock;                    /* Socket descriptor for server */
static int clntSock;                    /* Socket descriptor for client */
static struct sockaddr_in echoServAddr; /* Local address */
static struct sockaddr_in echoClntAddr; /* Client address */
static unsigned short echoServPort=15000;     /* Server port */
static unsigned int clntLen;            /* Length of client address data structure */
static int recvMsgSize=0; 
static unsigned char buffso_rcv[32]; 
static unsigned char buffso_send[7];

int first=1;
static int connected=0;

int debug=0;
