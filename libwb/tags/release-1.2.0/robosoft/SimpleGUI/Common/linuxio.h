/* linuxio.h (c)2003 Tony.Noel@inria.fr
 * WARNING: If you modify this file, it may loose its consistency!
 */

#define LINUXIO_SHM_SEG_NAME 0xAAAA
typedef struct {
  int fromLinux_mode[1]; unsigned char fromLinux_mode_sem;
  int fromLinux_rjtk_x[1]; unsigned char fromLinux_rjtk_x_sem;
  int fromLinux_rjtk_y[1]; unsigned char fromLinux_rjtk_y_sem;
  int switch_curr[1]; unsigned char switch_curr_sem;
  int switch_x[1]; unsigned char switch_x_sem;
  int switch_y[1]; unsigned char switch_y_sem;
  int speed_s[1]; unsigned char speed_s_sem;
  int FSAngle_a[1]; unsigned char FSAngle_a_sem;
} linuxio_shm_seg_def;
