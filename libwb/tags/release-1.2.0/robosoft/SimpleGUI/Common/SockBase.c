#include "SockBase.h"



int CreateSock(int SockType)
{
	return socket(AF_INET,SockType,0);

}

int BindSock(int Sock,int Port)
{
	struct sockaddr_in serv;

  	serv.sin_family=AF_INET;
  	serv.sin_addr.s_addr=htonl(INADDR_ANY);
  	serv.sin_port=htons(Port);

    return bind(Sock,(struct sockaddr *)&serv,sizeof(serv)) ;

}


int HostNameToAddr(char * HostName,struct sockaddr_in * Addr)
{
	struct hostent *hp;

  	hp=gethostbyname(HostName);
  	if(hp==(struct hostent *)0)
  		return -1;

  	memcpy((char *) &Addr->sin_addr,(char *)hp->h_addr,hp->h_length);


  	return 0;
}



void CloseSock(int Sock)
{
	if ( close(Sock) == -1 )
		perror("CloseSock");
}
