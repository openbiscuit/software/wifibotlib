#ifndef UDPSock_h
#define UDPSock_h

#include "SockBase.h"

int CreateUDPServer(int Port);

/* Creation d'un Client UDP
- Type =0 -> HostName : nom de la machine distante / Type = 1 -> HostName = Adress IP de la machine */
int CreateUDPClient(char * HostName,int Port,int Type,struct sockaddr_in * Serv);

int UDPRecv(int Sock,char * Buf,int Len);

int UDPSend(int Sock,char * Buf, int Len,struct sockaddr_in * Dest);

#endif
