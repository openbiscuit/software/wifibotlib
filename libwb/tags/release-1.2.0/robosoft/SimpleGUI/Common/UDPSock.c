

#include "UDPSock.h"

#include <stdio.h>

int CreateUDPServer(int Port)
{
	int s;

	s = CreateSock(SOCK_DGRAM);

	if ( s == -1 )
		return -1;

	if (BindSock(s,Port) == -1 )
		return -1;


	return s;
}





int CreateUDPClient(char * HostName,int Port,int Type,struct sockaddr_in * Serv)
{
	int s;

	printf("%s %d %d\n",HostName,Port,Type);
	s = CreateSock(SOCK_DGRAM);

	if ( s == -1 )
	{   printf("CreateSock \n");
			return -1;
	}

	Serv->sin_family = AF_INET;
	Serv->sin_port = htons(Port);

	if (Type == 0)
	{
		if ( HostNameToAddr(HostName,Serv) == -1)
			return -1;
	}
	else /* HostName = IP address */
	{
		if ( inet_aton(HostName,&Serv->sin_addr) == 0)
					return -1;
	}

	return s;
}


int UDPRecv(int Sock,char * Buf,int Len)
{

	struct sockaddr_in client;
	int client_addr_len;

	client_addr_len=sizeof(client);

	return recvfrom(Sock,Buf,Len,0,(struct sockaddr *)&client,&client_addr_len);

}

int UDPSend(int Sock,char * Buf, int Len,struct sockaddr_in * Dest)
{

	return sendto(Sock,Buf,Len,0,(struct sockaddr *)Dest,sizeof(struct sockaddr_in));
}

