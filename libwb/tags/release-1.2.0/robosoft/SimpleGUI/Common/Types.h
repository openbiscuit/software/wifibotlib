#ifndef Types_h
#define Types_h


#define IMAGE_MAXSIZE 200000

// Network Period in usec
#define PERIOD 100000




/* Consignes de teleoperation */
struct s_comm {
  int Mod;           // 0:manual / 1:teleoperated
  int Joy_X;
  int Joy_Y;
};

/* Retour d'infos du Cycab */
struct s_infos {
  int mode;
  int speed;
  int angle;
  int Jtk_X;
  int Jtk_Y;
 // int img_size;
 // unsigned char img[IMAGE_MAXSIZE];
};


#endif

