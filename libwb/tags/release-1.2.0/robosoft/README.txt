** robosoft **
**************

In this section, simple programs written by the wifibot's creators
(don't know the exact name) are given.

simple_server: a very simple command server that runs on the wifibot
	      under the name /usr/sbin/robot. It takes one parameters
	      which is the debug level of the program. (see
	      Utilisateur_wifibot.pdf)
   simple_server <debug>
   WIFIBOT port 15000

robot-client: a simple client that emulate a joystick and sends
              commands to the simple_server.
   robot-client [-a adresse] [-p port] [-j] j for usb joystick
   WIFIBOT port 15000

video-client: a simple client that displays images from the wifibot
              camera.
   video-client [-a adresse] [-p port]
   WIFIBOT port 80

19/04/2007

* Layout
--------

README.txt			this file

+ SimpleServer			simple_server
|
|
|
+ SimpleGUI			some client examples
   |
   + Common			common TCPSocket functions
   |
   + robot-client		see above
   |
   + video-client		see above



