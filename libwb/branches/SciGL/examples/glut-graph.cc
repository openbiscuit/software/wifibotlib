/*
 * Copyright (C) 2008 Nicolas P. Rougier, Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include <cstdlib>
#include "graph.h" 
#include "terminal.h" 
#include "colorbar.h" 
#include "smooth-surface.h"
#include "frame.h"
#include "data.h"
#include "convert.h"
#include "basis-cube.h"
#include "fparser.h"

static Graph scene;
static Terminal *terminal = 0;
static unsigned int button = 0;
static unsigned int fps = 40;

void on_key_press (unsigned char key, int x, int y) {
  //printf( "key_press : %d\n", key);
  if (key == 3) // Ctrl-C
    exit(0);
  if (key == 27) // ESC
        if (terminal->get_visible())
            terminal->hide (250);
        else
            terminal->show (250);
    else
        scene.key_press (key);
}
void on_special_key_press (int key, int x, int y) {
    unsigned char k = 0;
    switch (key) {
    case GLUT_KEY_F1:
      if (scene.get_basis_cube()->get_visible())
	scene.get_basis_cube()->hide (250);
      else                    
	scene.get_basis_cube()->show (250);        
      break;
    case GLUT_KEY_F2:
      if (scene.get_color_bar()->get_visible())
	scene.get_color_bar()->hide (250);
      else                    
	scene.get_color_bar()->show (250);        
      break;    
    case GLUT_KEY_UP:    k = 'i'-'a' + 1; break; // Control-i
    case GLUT_KEY_DOWN:  k = 'j'-'a' + 1; break; // Control-j
    case GLUT_KEY_LEFT:  k = 'b'-'a' + 1; break; // Control-b
    case GLUT_KEY_RIGHT: k = 'f'-'a' + 1; break; // Control-f
    case GLUT_KEY_HOME:  k = 'a'-'a' + 1; break; // Control-a
    case GLUT_KEY_END:   k = 'e'-'a' + 1; break; // Control-e
    default:             k = 0;           break;
    }
    scene.key_press (k);
}
void on_mouse_button (int b, int state, int x, int y) {
    switch (b) {
    case GLUT_LEFT_BUTTON:   button = 1;  break;
    case GLUT_MIDDLE_BUTTON: button = 2;  break;
    case GLUT_RIGHT_BUTTON:
      button = 3;  
#if defined(__APPLE__)
      button = 2;
#endif
      break;
    default:                 button = 0;  break;
    }
    switch (state) {
    case GLUT_UP:
        scene.button_release (button, x, y);
        break;
    case GLUT_DOWN:
        scene.button_press (button, x, y);
        break;
    }
}
void on_mouse_move (int x, int y) {
    scene.mouse_motion (button, x, y);
}
void display (void) {
    scene.render ();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    scene.resize (width, height);
}
void timeout (int value) {
    display ();
    glutTimerFunc (int(1000.0/fps), timeout, 1);
}
float my_function (float x, float y) {
    return (1-x/2+pow(x,5)+pow(y,3))*exp(-x*x-y*y);
}
void
terminal_event (Terminal *terminal, std::string input) {
    terminal->print (std::string("You typed: \"") + input + "\"\n");
}

void
alain_terminal_event (Terminal *terminal, std::string input)
{
  
  std::vector<std::string> tokens;
  tokenize(input, tokens);

  if( tokens.size() > 0 ) {
    if (tokens[0] == "o") { // orientation
      scene.reorient( to_float( tokens[1]), to_float(tokens[2]));
      terminal->print (std::string("re-oriented\n"));
      return;
    }
    if (tokens[0] == "p") { // position
      scene.set_position( to_float( tokens[1]), to_float(tokens[2]));
      terminal->print (std::string("re-positionnedd\n"));
      return;
    }
    if (tokens[0] == "x") { // x range
      scene.set_range_coord_x( Range(to_float( tokens[1]), to_float(tokens[2]),
 				       to_float( tokens[3]),
 				       to_float(tokens[4])));
       terminal->print (std::string("re-dimX\n"));
      return;
    }
    if (tokens[0] == "y") { // y range
      scene.set_range_coord_y( Range(to_float( tokens[1]), to_float(tokens[2]),
 				       to_float( tokens[3]),
 				       to_float(tokens[4])));
       terminal->print (std::string("re-dimY\n"));
      return;
    }
    if (tokens[0] == "z") { // z range
      scene.set_range_coord_z( Range(to_float( tokens[1]), to_float(tokens[2]),
 				       to_float( tokens[3]),
 				       to_float(tokens[4])));
      terminal->print (std::string("re-dimZ\n"));
      return;
    }
    if (tokens[0] == "f") { // define a new function
      SmoothSurface *handle_surf = scene.get_surf ( to_int (tokens[1]) );
      if (handle_surf == NULL ) { // surface does not exists
	terminal->print (std::string ("Surface No ") + tokens[1] +
			 std::string (" does not exists.\n") );
	handle_surf = scene.new_surf ();
      }
      else {
	terminal->print (std::string ("Surface ") + tokens[1] +
			 std::string (" selected.\n") );
      }
      Data *surf_data = handle_surf->get_data();
      FunctionParser *fparser = new FunctionParser();
      int res = fparser->Parse( tokens[2], "x,y");
      if (res >= 0) {
        terminal->print (std::string(fparser->ErrorMsg()) + "\n\n");
        return;
      }
      else {
	terminal->print (std::string("Function parsed\n"));
      }
      surf_data->set_parsed_function( fparser );
      handle_surf->update();
      return;
    }
    if (tokens[0] == "s") { // switch function between visible or not
      SmoothSurface *handle_surf = scene.get_surf ( to_int (tokens[1]) );
      if (handle_surf != NULL ) {
	handle_surf->set_visible( !handle_surf->get_visible() );
	handle_surf->update();
      }
      return;
    }

  }
  terminal->print (std::string("You typed: \"") + input + "\"\n");
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("OpenGL viewer");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutMouseFunc (on_mouse_button);
    glutMotionFunc (on_mouse_move);
    glutKeyboardFunc (on_key_press);
    glutSpecialFunc (on_special_key_press);
    glutReshapeWindow (800,800);
    glutTimerFunc (int(1000.0/fps), timeout, 1);

    terminal = new Terminal();
    terminal->set_prompt ("> ");
    //terminal->connect (terminal_event);
    terminal->connect (alain_terminal_event);
    terminal->hide();
    scene.add (terminal);

    scene.build();
    
    glutMainLoop();
    return 0;
}

