/**
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include "colorbar.h" 

static Colorbar colorbar;

void display (void) {
    glClearColor (1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    colorbar.render();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    glViewport (0, 0, width, height);
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("Colorbar");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutReshapeWindow (256,256);

    glEnable (GL_DEPTH_TEST);
    glClearDepth (1.0f); 
    glDepthFunc (GL_LEQUAL);
    glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glShadeModel (GL_SMOOTH);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);

    colorbar.set_title ("Title");
    colorbar.set_gravity (0,0);
    colorbar.set_position (0,0);
    colorbar.set_size (.8, 24);
    colorbar.set_radius (0);
    
    glutMainLoop();
    return 0;
}

