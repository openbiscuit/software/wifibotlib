/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include "object.h" 
#include "viewport.h" 

static int button = 0;
static Object object;
static Viewport viewport;

void on_mouse_button (int b, int state, int x, int y) {
    switch (b) {
    case GLUT_LEFT_BUTTON:   button = 1;  break;
    case GLUT_MIDDLE_BUTTON: button = 2;  break;
    case GLUT_RIGHT_BUTTON:  button = 3;  break;
    default:                 button = 0;  break;
    }
    switch (state) {
    case GLUT_UP:
        viewport.button_release (button, x, y);
        break;
    case GLUT_DOWN:
        viewport.button_press (button, x, y);
        break;
    }
}
void on_mouse_move (int x, int y) {
    viewport.mouse_motion (button, x, y);
}
void display (void) {
    glClearColor (1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    glTranslatef (0.0, 0, -8.0f);
    glRotatef (25,0,1,0);
    glRotatef (5,0,0,1);
    object.render();
    viewport.render();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    glViewport (0, 0, width, height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    float aspect = 1.0f;
    //    if (width > height) 
    aspect = width / float(height);
    //    else
    //        aspect = height / float(width);
    float aperture = 25.0f;
    float near = 1.0f;
    float far = 100.0f;
    float top = tan(aperture*3.14159/360.0) * near;
    float bottom = -top;
    float left = aspect * bottom;
    float right = aspect * top;
    glFrustum (left, right, bottom, top, near, far);
    glMatrixMode (GL_MODELVIEW);
}
void timeout (int value) {
    display ();
    glutTimerFunc (int(1000.0/30), timeout, 1);
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("Object");
    glutMouseFunc (on_mouse_button);
    glutMotionFunc (on_mouse_move);
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutReshapeWindow (256,256);

    glEnable (GL_DEPTH_TEST);
    glClearDepth (1.0f); 
    glDepthFunc (GL_LEQUAL);
    glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glShadeModel (GL_SMOOTH);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);

    object.set_bg_color (1,1,1,.75);
    object.set_br_color (0,0,0,1);

    viewport.set_bg_color (.5,.5,.5,.25);
    viewport.set_br_color (0,0,0,1);
    viewport.set_gravity (-1,-1);
    viewport.set_position (5,5);
    viewport.set_size (100,100);
    viewport.set_radius (0);

    glutTimerFunc (int(1000.0/30), timeout, 1);

    glutMainLoop();
    return 0;
}

