/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "font.h"
#include "colorbar.h"


Colorbar::Colorbar (void) : Widget ()
{
    cmap_ = Colormap::Hot();
    texture_ = 0;
    set_margin (0,0,0,0);
    set_radius (0);
    set_size (.75, 24);
    set_position (0,75,1);
    set_gravity (0,1);
    set_fg_color (0,0,0,1);
    set_bg_color (0,0,0,.25);
    set_br_color (0,0,0,1);
    title_ = cmap_->name();
}

Colorbar::~Colorbar (void)
{
    if (texture_)
      glDeleteTextures (1, (const GLuint *) &texture_);
}

void
Colorbar::build (void)
{
    unsigned char *data = new unsigned char [512*3];
    float min = cmap_->min();
    float max = cmap_->max();

    for (int i=0; i<512; i++) {
        Color c = (*cmap_)(min+ (i/512.0f)*(max-min));
        data[i*3+0] = int(c.red*255);
        data[i*3+1] = int(c.green*255);
        data[i*3+2] = int(c.blue*255);
    }
    if (texture_)
        glDeleteTextures (1, (const GLuint *) &texture_);
    glGenTextures (1, (GLuint *) &texture_);
    glBindTexture (GL_TEXTURE_2D, texture_);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, 512, 1, 0,
                  GL_RGB, GL_UNSIGNED_BYTE, data);
    delete data;
}

void
Colorbar::render (void)
{
    if (not get_visible()) return;
    if (not texture_)  build ();
    if (not texture_)  return;

    render_start();

    Font *font = Font::Font16();
    float ticks = 10.0;

    float vertical = 1;
    if (get_size().y < get_size().x)
        vertical = 0;

    // Computes number of ticks and font size
    char text_min[32], text_max[32];
    snprintf (text_min, 31, "%+.2f", cmap_->min());
    snprintf (text_max, 31, "%+.2f", cmap_->max());
    unsigned int text_size = strlen (text_max);
    if (strlen (text_min) > text_size)
        text_size = strlen (text_min);
    if (vertical) {
        if ((Font::Font16()->glyph_size().y*2*ticks) > get_size().y) {
            font = Font::Font12();
            if ((Font::Font12()->glyph_size().y*2*ticks*text_size) > get_size().y)
                ticks = int(get_size().y / (2*Font::Font12()->glyph_size().y));
        }
    } else {
        if ((Font::Font16()->glyph_size().x*(text_size)*ticks) > get_size().x) {
            font = Font::Font12();
            if ((Font::Font12()->glyph_size().x*(text_size)*ticks) > get_size().x)
                ticks = int(get_size().x / (Font::Font12()->glyph_size().y*(text_size)));
        }
    }

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float height = viewport[3];

    glTranslatef (.375,.375,0);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glEnable (GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, texture_);
    glColor4f (1,1,1,alpha_);
    glBegin (GL_POLYGON);
    if (vertical) {
        glTexCoord2f (1, 0); glVertex2f (get_position().x,          height-get_position().y);
        glTexCoord2f (1, 0); glVertex2f (get_position().x+get_size().x, height-get_position().y);
        glTexCoord2f (0, 0); glVertex2f (get_position().x+get_size().x, height-get_position().y-get_size().y);
        glTexCoord2f (0, 0); glVertex2f (get_position().x,          height-get_position().y-get_size().y);
    } else {
        glTexCoord2f (0, 0); glVertex2f (get_position().x,          height-get_position().y);
        glTexCoord2f (1, 0); glVertex2f (get_position().x+get_size().x, height-get_position().y);
        glTexCoord2f (1, 0); glVertex2f (get_position().x+get_size().x, height-get_position().y-get_size().y);
        glTexCoord2f (0, 0); glVertex2f (get_position().x,          height-get_position().y-get_size().y);
    }

    glEnd();
    glDisable (GL_TEXTURE_2D);

    glTranslatef (0,0,1);
    glDepthMask (GL_FALSE);
    glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
    glEnable (GL_LINE_SMOOTH);
    glBegin (GL_LINE_LOOP);
    glVertex2f (get_position().x,          height-get_position().y);
    glVertex2f (get_position().x+get_size().x, height-get_position().y);
    glVertex2f (get_position().x+get_size().x, height-get_position().y-get_size().y);
    glVertex2f (get_position().x,          height-get_position().y-get_size().y);
    glEnd();

    // Ticks
    glColor4f (get_fg_color().r, get_fg_color().g, get_fg_color().b, get_fg_color().a*alpha_);
    glLineWidth (1);
    float tick_size = .25;

    glBegin(GL_LINES);
    if (vertical) {
        for (int i=1; i<ticks; i++) {
            glVertex3f (get_position().x+get_size().x*(1.0-tick_size) - 1,
                        height-get_position().y-(get_size().y)*i/ticks,
                        0);
            glVertex3f (get_position().x+get_size().x*1.00 - 1,
                        height-get_position().y-(get_size().y)*i/ticks,
                        0);
        }
    } else {
        for (int i=1; i<ticks; i++) {
            glVertex3f (get_position().x+ i/ticks*get_size().x,
                        height-get_position().y-get_size().y,
                        0);
            glVertex3f (get_position().x+ i/ticks*get_size().x,
                        height-get_position().y-get_size().y + tick_size*get_size().y,
                        0);
        }        
    }
    glEnd();

    glTranslatef (-.375,-.375, 0);
    glTranslatef (0,0,1);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glEnable (GL_TEXTURE_2D);
    glDepthMask (GL_TRUE);

    char text[16];
    for (int i=0; i<=int(ticks); i++) {
        glPushMatrix();
        float v = i/ticks;
        snprintf (text, 15, "%+.2f", cmap_->min() + v*(cmap_->max()-cmap_->min()));
        Size s = font->size(text);
        if (vertical) {
            glTranslatef (int (get_position().x+get_size().x*1.25),
                          int (height-get_position().y-get_size().y*(1-v)-s.y*.5),
                          0);
        } else {
            glTranslatef (int (get_position().x+ v*get_size().x - s.x/2),
                          int (height-get_position().y-get_size().y-s.y*1.25),
                          0);
        }
        font->render (text);
        glPopMatrix();
    }

    glPushMatrix();
    font = Font::Font24();
    Size s = font->size (title_);
    if (vertical) {
        glRotatef (90, 0,0,1);
        glTranslatef (int (height - get_position().y - (get_size().y+s.x)/2),
                      int (-get_position().x + s.y*.1),
                      0);
    } else {
        glTranslatef (int (get_position().x + (get_size().x - s.x)/2),
                      int (height - get_position().y + s.y*.1),
                      0);
    }
    font->render (title_);
    glPopMatrix();

    render_finish();
}

void
Colorbar::set_title (std::string title)
{
    title_ = title;
}

std::string
Colorbar::get_title (void)
{
    return title_;
}

std::string
Colorbar::title (void)
{
    return get_title();
}

void
Colorbar::set_cmap (Colormap *cmap)
{
    cmap_ = cmap;
}

Colormap *
Colorbar::get_cmap (void)
{
    return cmap_;
}

Colormap *
Colorbar::cmap (void)
{
    return get_cmap();
}
