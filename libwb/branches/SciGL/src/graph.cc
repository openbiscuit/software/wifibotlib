/*
 * Copyright (C) 2008 Nicolas P. Rougier, Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "graph.h"
#include "trackball.h"

// ===========================================================================
Graph::Graph (void)
{
  set_shape(Shape ( 100, 100, 1));

  axis_cube_ = new BasisCube();
  axis_cube_->set_position ( -.5, -.5, -.5);
  set_axis_size( 1.0, 1.0, 1.0 );

  colorbar_ = new Colorbar();
  colorbar_->set_cmap (Colormap::IceAndFire());
  Range range_z = axis_cube_->get_range_coord_z();
  colorbar_->get_cmap()->scale(range_z.min, range_z.max);

  add( colorbar_ );
   
}
// ===========================================================================
Graph::~Graph (void)
{
  delete axis_cube_;
}

// ===========================================================================  
void
Graph::set_axis_size (float size_x, float size_y, float size_z)
{
  axis_cube_->set_size ( size_x, size_y, size_z );
}
void
Graph::set_axis_size (Size s)
{
  set_axis_size (s.dx, s.dy, s.dz);
}
// ===========================================================================
void
Graph::set_shape (Shape s)
{
  shape_ = s;
}
// ===========================================================================
BasisCube *
Graph::get_basis_cube (void)
{
  return axis_cube_;
}
// ===========================================================================
Colorbar * 
Graph::get_color_bar (void)
{
  return colorbar_;
}
// ===========================================================================
SmoothSurface *
Graph::new_surf (void)
{
  // create a surface
  SmoothSurface *surf = new SmoothSurface ();

  // create Data
  Data *data = new Data ();
  float * data_float = new float [(int)shape_.width * (int)shape_.height];
  data->set_data( data_float, shape_);

//   // set up initial value
//   double vals[2];
//   for (int y=0; y<100; y++) {
//     for (int x=0; x<100; x++) {
      
//       vals[0] = -1.0+ x/100.0*2.0f;
//       vals[1] = -1.0+ y/100.0*2.0f;
//       *(data->get_data(x,y)) = (1-vals[0]/2+pow(vals[0],5)+
// 				pow(vals[1],3))*exp(-vals[0]*vals[0]-vals[1]*vals[1]);
//     }
//   }
  
  surf->set_data( data );
  surf->set_cmap (Colormap::IceAndFire());
  surf->set_position (0,0,0);
  surf->update();
  add (surf);
  return surf;
}
SmoothSurface *
Graph::get_surf (unsigned int index)
{
  if ( index < objects_.size()) {
    return ( (SmoothSurface *) objects_.at(index) );
  }
  else {
   return NULL;
  }
}
// ===========================================================================
void
Graph::render ()
{
  glClearColor (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity ();
  glTranslatef (0.0, 0.0, -5.0f);
  glScalef (zoom_, zoom_, zoom_);
  float m[4][4];
  build_rotmatrix (m, view_.data);
  glMultMatrixf (&m[0][0]);
  
  // Back widgets
  // glDisable (GL_DEPTH_TEST);
  for (unsigned int i=0; i<widgets_.size(); i++)
    if (widgets_.at(i)->get_position().z < 0)
      widgets_.at(i)->render();
  
  // Objects
  glEnable (GL_DEPTH_TEST);
  for (unsigned int i=0; i<objects_.size(); i++)
    objects_.at(i)->render();

  // BasisCube, rendered "after" to allow for transparency.
  axis_cube_->render();
  
  // Front widgets
  glDisable (GL_DEPTH_TEST);
  for (unsigned int i=0; i<widgets_.size(); i++)
    if (widgets_.at(i)->get_position().z >= 0)
      widgets_.at(i)->render();
}
// ============================================================================
void
Graph::set_range_coord_x (Range range)
{
  axis_cube_->set_range_coord_x (range);

  for (unsigned int i=0; i<objects_.size(); i++) {
    SmoothSurface *surf_handle = ((SmoothSurface *) (objects_.at(i)));
    Data *surf_data = surf_handle->get_data();
    surf_data->set_range_coord_x( range );
    surf_handle->update();
  }
  
}
void
Graph::set_range_coord_y (Range range)
{
  axis_cube_->set_range_coord_y (range);

  for (unsigned int i=0; i<objects_.size(); i++) {
    SmoothSurface *surf_handle = ((SmoothSurface *) (objects_.at(i)));
    Data *surf_data = surf_handle->get_data();
    surf_data->set_range_coord_y( range );
    surf_handle->update();
  }
}
void
Graph::set_range_coord_z (Range range)
{
  axis_cube_->set_range_coord_z (range);
  
  colorbar_->get_cmap()->scale( range.min, range.max);

  for (unsigned int i=0; i<objects_.size(); i++) {
    SmoothSurface *surf_handle = ((SmoothSurface *) (objects_.at(i)));
    Data *surf_data = surf_handle->get_data();
    surf_data->set_range_coord_z( range );
    surf_handle->update();
  }
}
