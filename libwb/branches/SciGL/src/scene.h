/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SCENE_H__
#define __SCENE_H__

#include "object.h"
#include "widget.h"

/**
 * Container for objects and widgets.
 *
 * A scene describes a set of objects and widgets and manage the view (zoom and
 * orientation). Rendering of elements is done in the following order:
 *
 * -# Widgets with a negative z
 * -# Objects
 * -# Widgets with a positive z
 *
 * \image html scene.png
 */
class Scene : public Widget {
public:
    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor.
     */
    Scene (void);

    /**
     * Destructor
     */
    virtual ~Scene (void);
    //@}



    /**
     * @name Rendering
     */
    /**
     * Setup scene.
     *
     * This method recursively call objects and widgets build method.
     */
    virtual void build (void);

    /**
     * Update scene.
     *
     * This method recursively call objects and widgets update method.
     */
    virtual void update (void);

    /**
     * Render the scene.
     *
     * Rendering is done in the following order:
     * -# Widgets with a negative z
     * -# Objects
     * -# Widgets with a positive z
     */
    virtual void render (void);
    virtual void save (std::string filename);
    //@}



    /**
     * @name Scene management
     */
    /**
     * Add a new object to the scene.
     *
     * @param object object to be added.
     */
    virtual void add (Object *object);

    /**
     * Add a new widget to the scene.
     *
     * @param widget widget to be added.
     */
    virtual void add (Widget *widget);
    //    virtual Object *get (std::string name);
    //    virtual Object *operator() (std::string name);
    //@}



    /**
     * @name Event processing
     */
    /**
     * Select callback.
     *
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     */
    virtual bool select (int x, int y);

    /**
     * Key press callback.
     *
     * @param key      key
     * @param modifier key modifier
     */
    virtual bool key_press (char key, int modifier=0);

    /**
     * Key release callback.
     *
     * @param key      key
     * @param modifier key modifier
     */
    virtual bool key_release (char key, int modifier=0);

    /**
     * Button press callback.
     *
     * @param button pressed button number
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     * @param modifier key modifier
     */
    virtual bool button_press   (int button, int x, int y, int modifier=0);

    /**
     * Button release callback.
     *
     * @param button pressed button number
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     * @param modifier key modifier
     */
    virtual bool button_release (int button, int x, int y, int modifier=0);

    /**
     * Mouse motion callback.
     *
     * @param button pressed button number
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     */
    virtual bool mouse_motion (int button, int x, int y);
    //@}



    /**
     * @name Frames per second
     */
    /**
     * Set fps.
     *
     * @param fps frames per second
     */
    virtual void set_fps (unsigned int fps);

    /**
     * Get fps.
     *
     * @return frames per second
     */
    virtual unsigned int get_fps (void);

    /**
     * Get fps.
     *
     * @return frames per second
     */
    virtual unsigned int fps (void);
    //@}



    /**
     * @name Zoom level
     */
    /**
     * Set zoom level.
     *
     * @param zoom zoom level
     */
    virtual void set_zoom (float zoom);

    /**
     * Get zoom level.
     *
     * @return zoom level
     */
    virtual float get_zoom (void);

    /**
     * Get zoom level.
     *
     * @return zoom level
     */
    virtual float zoom (void);
    //@}



    /**
     * @name Size
     */
    /**
     * Set size (viewport).
     *
     * @param size viewport size
     */
    virtual void set_size (Size size);

    /**
     * Set scene size (viewport).
     *
     * @param x viewport width
     * @param y viewport height
     */
    virtual void set_size (float x=1, float y=1);

    /**
     * Set scene size (viewport).
     *
     * @param size viewport size
     */
    virtual void resize (Size size);

    /**
     * Set scene size (viewport).
     *
     * @param x viewport width
     * @param y viewport height
     */
    virtual void resize (float x=1, float y=1);
    //@}



    /**
     * @name Orientation
     */
    /**
     * Set scene orientation along x and z axis.
     *
     * @param orientation orientation vector (x and z)
     */
    virtual void set_orientation (Orientation orientation);

    /**
     * Set scene orientation along x and z axis.
     *
     * @param x rotation around x axis (degrees)
     * @param z rotation around z axis (degrees)
     */
    virtual void set_orientation (float x=0, float z=0);

    /**
     * Set scene orientation along x and z axis.
     *
     * @param orientation orientation vector (x and z)
     */
    virtual void reorient (Orientation orientation);

    /**
     * Set scene orientation along x and z axis.
     *
     * @param x rotation around x axis (degrees)
     * @param z rotation around z axis (degrees)
     */
    virtual void reorient (float x=0, float z=0); 
    //@}

    /**
     * @name Focusable property
     */
    /**
     * Has scene or any child has focus ? 
     *
     * @return Whether scene or any child has focus or not
     */    
    virtual bool has_focus (int x, int y);
    //@}



protected:
    /**
     * List of widgets to render.
     */
    std::vector<Widget *> widgets_;

    /**
     * List of objects to render.
     */
    std::vector<Object *> objects_;

    /**
     * Focused widget
     */
    Widget *focus_;

    /**
     * Zoom level.
     */
    float zoom_;

    /**
     * Zoom maximum level.
     */
    float zoom_max_;

    /**
     * Zoom minimum level.
     */
    float zoom_min_;

    /**
     * Current view vector.
     */
    Vec4f view_;

    /**
     * Initial view orientation.
     */
    Orientation orientation_;

    /**
     * Frame Per Second.
     */
    unsigned int fps_; 

    /**
     * Pointer position at event start.
     */
    Position start_;  
};

#endif
