/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <png.h>
extern "C" {
#    include <jpeglib.h>
}
#include "data.h"


Data::Data (void)
{
    shape_ = Shape (0,0,0);
    data_ = 0;
    
    range_x_ = Range( -1, 1, 0, 0);
    range_y_ = Range( -1, 1, 0, 0);
    range_z_ = Range( -1, 1, 0, 0);

    // TODO mettre NULL au depart
    fparser_ = NULL;
    // fparser_ = new FunctionParser();
//     fparser_->Parse( "x*x + y*y*y", "x,y");
}

Data::~Data (void)
{
  if( fparser_ != NULL ) {
    delete fparser_;
  }
}

Shape
Data::get_shape (void) const
{
    return shape_;
}

Shape
Data::shape (void) const
{
    return get_shape();
}

float *
Data::get_data (void) const
{
    return data_;
}

float *
Data::data (void) const
{
    return get_data();
}

float *
Data::get_data (unsigned int x, unsigned int y, unsigned int i) const
{
    if (not data_)
        return data_;
    return &data_[int(y*shape_.x + x + i)];
}


float *
Data::data (unsigned int x, unsigned int y, unsigned int i) const
{
    return get_data(x,y,i);
}

void
Data::set_data(unsigned int x, unsigned int y, float data)
{
  data_[int(x*shape_.x+y)] = data;
}

void
Data::set_data (std::string filename)
{
    std::string extension = filename.substr (filename.find_last_of('.')+1);

    float *data = 0;
    unsigned int width = 0;
    unsigned int height = 0;
    unsigned int depth = 0;

    if (extension == "png") {
        data = open_png (filename, width, height, depth);
        if (data) {
            data_ = data;
            shape_ = Shape (width, height, depth);
        }
    } else if ((extension == "jpg") or (extension == "jpeg")) {
        data = open_jpg (filename, width, height, depth);
        if (data) {
            data_ = data;
            shape_ = Shape (width, height, depth);
        }
    }
}

void
Data::set_data (float (*function)(float,float),
                float xmin, float xmax,
                float ymin, float ymax,
                unsigned int resolution)
{
    data_ = new float [resolution*resolution];
    shape_ = Shape (resolution, resolution, 1);
    for (unsigned int y=0; y<resolution; y++) {
        for (unsigned int x=0; x<resolution; x++) {
            float value = (*function) (xmin + x/float(resolution) * (xmax-xmin),
                                       ymin + y/float(resolution) * (ymax-ymin));
            data_[y*resolution+x] = value;
        }
    }
}

void
Data::set_data (float *data, Shape shape)
{
    data_ = data;
    shape_ = shape;
}

void
Data::set_data (float *data, unsigned int width, unsigned int height, unsigned int depth)
{}

void
Data::set_data (unsigned char *data, Shape shape)
{}

void
Data::set_data (unsigned char *data, unsigned int width, unsigned int height, unsigned int depth)
{}

float *
Data::open_png (std::string filename,
                 unsigned int &width,
                 unsigned int &height,
                 unsigned int &depth)

{
    width = 0;
    height = 0;
    depth = 0;

    /* Open image file */
    FILE *fp = fopen (filename.c_str(), "rb");
    if (!fp)
        return 0;

    /* Read magic number */
    png_byte magic[8];
    fread (magic, 1, sizeof (magic), fp);

    /* Check for valid magic number */
    if (!png_check_sig (magic, sizeof (magic))) {
        fclose (fp);
        return 0;
    }

    /* Create a png read struct */
    png_structp png_ptr = png_create_read_struct
        (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        fclose (fp);
        return 0;
    }

    /* Create a png info struct */
    png_infop info_ptr = png_create_info_struct (png_ptr);
    if (!info_ptr) {
        fclose (fp);
        png_destroy_read_struct (&png_ptr, NULL, NULL);
        return 0;
    }

    /* Initialize the setjmp for returning properly after a libpng
       error occured */
    if (setjmp (png_jmpbuf (png_ptr))) {
        fclose (fp);
        png_destroy_read_struct (&png_ptr, &info_ptr, NULL);
        return 0;
    }
    
    /* Setup libpng for using standard C fread() function
       with our FILE pointer */
    png_init_io (png_ptr, fp);

    /* Tell libpng that we have already read the magic number */
    png_set_sig_bytes (png_ptr, sizeof (magic));

    /* Read png info */
    png_read_info (png_ptr, info_ptr);
 
    int bit_depth, color_type;

    /* Get some usefull information from header */
    bit_depth = png_get_bit_depth (png_ptr, info_ptr);
    color_type = png_get_color_type (png_ptr, info_ptr);

    /* Convert index color images to RGB images */
    if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb (png_ptr);

    /* Convert 1-2-4 bits grayscale images to 8 bits
       grayscale. */
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
        png_set_gray_1_2_4_to_8 (png_ptr);
    
    if (png_get_valid (png_ptr, info_ptr, PNG_INFO_tRNS))
        png_set_tRNS_to_alpha (png_ptr);

    if (bit_depth == 16)
        png_set_strip_16 (png_ptr);
    else if (bit_depth < 8)
        png_set_packing (png_ptr);

    /* Update info structure to apply transformations */
    png_read_update_info (png_ptr, info_ptr);

    /* Retrieve updated information */
    png_uint_32 w, h;
    png_get_IHDR (png_ptr, info_ptr, &w, &h, &bit_depth,
                  &color_type, NULL, NULL, NULL);
    width = w;
    height = h;

    switch (color_type) {
    case PNG_COLOR_TYPE_GRAY:
        depth = 1;
        break;
    case PNG_COLOR_TYPE_GRAY_ALPHA:
        depth = 2;
        break;
    case PNG_COLOR_TYPE_RGB:
        depth = 3;
        break;
    case PNG_COLOR_TYPE_RGB_ALPHA:
        depth = 4;
        break;
    default:
        /* Badness */
        break;
    }

    /* We can now allocate memory for storing pixel data */
    unsigned char *cdata = new unsigned char [w * h * depth];

    /* Setup a pointer array.  Each one points at the begening of a row. */
    png_bytep *row_pointers = new png_bytep [h];
    for (unsigned int i=0; i<h; i++)
        row_pointers[i] = (png_bytep)(cdata + ((h - (i + 1)) * w * depth));

    /* Read pixel data using row pointers */
    png_read_image (png_ptr, row_pointers);

    /* We don't need row pointers anymore */
    delete [] row_pointers;

    /* Finish decompression and release memory */
    png_read_end (png_ptr, NULL);
    png_destroy_read_struct (&png_ptr, &info_ptr, NULL);

    fclose (fp);

    float *data = new float [w * h * depth];
    for (unsigned int i=0; i<(h*w*depth); i++)
        data[i] = cdata[i]/256.0;
    delete cdata;
    return data;
}

float *
Data::open_jpg (std::string filename,
                unsigned int &width,
                unsigned int &height,
                unsigned int &depth)
{
    width = 0;
    height = 0;
    depth = 0;

    struct jpeg_decompress_struct cinfo;
    struct  jpeg_error_mgr jerr;

    FILE *fp = fopen (filename.c_str(), "rb");
    if (!fp)
        return false;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress (&cinfo);
    jpeg_stdio_src(&cinfo, fp);
    if (not jpeg_read_header(&cinfo, TRUE)) {
        fclose (fp);
        return false;
    }
    jpeg_start_decompress(&cinfo);

    unsigned int w = cinfo.image_width;
    unsigned int h = cinfo.image_height;
    width = w;
    height = h;
    depth = cinfo.num_components;
    unsigned int rowstride = w*depth;

    unsigned char *cdata = new unsigned char [rowstride];
    float *data = new float [w * h * depth];
    while (cinfo.output_scanline < cinfo.output_height) {
        jpeg_read_scanlines(&cinfo, &cdata, 1);
        for (unsigned int i=0; i< rowstride; i++)
            data[(h-cinfo.output_scanline)*rowstride+i] = cdata[i]/256.0;
    }
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    fclose (fp);

    delete [] cdata;
    return data;
}
// ============================================================================
void
Data::set_parsed_function( FunctionParser *fparser )
{
  fparser_ = fparser;
  compute_data();
}
// ============================================================================
void
Data::set_range_coord_x (Range range)
{
  range_x_ = range;
  compute_data();
}
void
Data::set_range_coord_y (Range range)
{
  range_y_ = range;
  compute_data();
}
void
Data::set_range_coord_z (Range range)
{
  range_z_ = range;
  compute_data();
}
// ============================================================================
void
Data::compute_data()
{
  if( fparser_ != NULL ) {
    double vals[2];
    double xrange = (range_x_.max - range_x_.min) / shape_.x;
    double yrange = (range_y_.max - range_y_.min) / shape_.y;
    // For Z, it's a bit peculiar
    double zrange = (range_z_.max - range_z_.min) / shape_.z / 2.0;
    float value =0;
    
    for( int index_y = 0; index_y < shape_.y; index_y++ ) {
      for( int index_x = 0; index_x < shape_.x; index_x++ ) {
	// X coordinate
	vals[0] = range_x_.min + (double) index_x * xrange;
	// Y coordinate
	vals[1] = range_y_.min + (double) index_y * yrange;
	// Z value
	value = fparser_->Eval(vals);
	
	// Z coordinate goes from -shape_.z to shape_.z
	//if (value > range_z_.max) value = range_z_.max;
	//if (value < range_z_.min) value = range_z_.min;
	value = (value - range_z_.min) / zrange - shape_.z;
	
	*(get_data(index_x,index_y)) = value;
	
      }
    }
  }

}
