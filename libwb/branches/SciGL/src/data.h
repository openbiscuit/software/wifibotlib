/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DATA_H__
#define __DATA_H__
#include <string>
#include "vec4f.h"

#include "../examples/fparser.h"


/**
 * Representation of data of  type z=f(x,y).
 *
 * Data is stored into a float array whatever the source (char or float). The
 * shape of the data is given by its width (x), its height (y) and its
 * depth. The depth represents the number of value describing a data value. For
 * example, RGB images will used a depth of 3 and these values can be
 * subsequently used to represent the data with the real color.
 *
 * [Added by Alain] : compute_data(), set_range(),
 * set_parsed_function() and range__ + fparser_ variables.
 */
class Data {
public:

    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor
     */
    Data (void);

    /**
     * Destructor
     */
    virtual ~Data (void);
    //@}

    /**
     * @name Shape
     */
    /**
     * Get shape.
     *
     * @return shape
     */
    virtual Shape get_shape (void) const;

    /**
     * Get shape.
     *
     * @return shape
     */
    virtual Shape shape (void) const;
    //@}


    /**
     * @name Data
     */
    /**
     * Get data pointer.
     *
     * @return data pointer
     */
    virtual float *get_data (void) const;

    /**
     * Get data pointer.
     *
     * @return data pointer
     */
    virtual float *data (void) const;

    /**
     * Get data at x,y,i
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param i data index for data with depth > 1
     * @return data at x,y,i
     */
    virtual float *get_data (unsigned int x,
                             unsigned int y,
                             unsigned int i=0) const;

    /**
     * Get data at x,y,i
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param i data index for data with depth > 1
     * @return data at x,y,i
     */
    virtual float *data (unsigned int x,
                         unsigned int y,
                         unsigned int i=0) const;

    /**
     * Set data from an image file.
     *
     * @param filename image filename (jpg or png).
     */
    virtual void set_data (std::string filename);

    /**
     * Set data from a function.
     *
     * @param function function to consider
     * @param xmin x lower bound
     * @param xmax x upper bound
     * @param ymin y lower bound
     * @param ymax y upper bound
     * @param resolution step increment between min and max (x and y)
     *
     */
    virtual void set_data (float(*function)(float,float),
                           float xmin, float xmax,
                           float ymin, float ymax,
                           unsigned int resolution=50);
    
    /**
     * Set data from a float array.
     *
     * @param data data float array
     * @param shape data shape (width, height and depth)
     */
    virtual void set_data (float *data,
                           Shape shape);

    /**
     * Set data from a float array.
     *
     * @param data data float array
     * @param width data width
     * @param height data height
     * @param depth data depth
     */
    virtual void set_data (float *data,
                           unsigned int width,
                           unsigned int height,
                           unsigned int depth=1);

    /**
     * Set data from a char array.
     *
     * Data will be translated into a float array.
     *
     * @param data data char array
     * @param shape data shape (width, height and depth)
     */
    virtual void set_data (unsigned char *data,
                           Shape shape);

    /**
       * set specific data in x and y
       *
       * @param x coor x
       * @param y coor y
       * @param data data to set
       */
    virtual void set_data(unsigned int x, unsigned int y, float data);

    /**
     * Set data from a char array.
     *
     * Data will be translated into a float array.
     *
     * @param data data char array
     * @param width data width
     * @param height data height
     * @param depth data depth
     */
    virtual void set_data (unsigned char *data,
                           unsigned int width,
                           unsigned int height,
                           unsigned int depth=1);

 /**
     * @name FunctionParser
     */
    /**
     * Set a new function parsed.
     */
    virtual void set_parsed_function( FunctionParser *fparser );
    //@}
    
    /**
     * @name SetRange
     *
     * Set Range = (min, max, nb_major_ticks, nb_minor_ticks)
     * for each coordinate.
     *
     * Force a computation of the data.
     * 
     * (Commented out in compute_data()) : If Z data is outside range, it is clamped in zRange.
     */
    virtual void set_range_coord_x (Range range);
    virtual void set_range_coord_y (Range range);
    virtual void set_range_coord_z (Range range);
    //@}

protected:
    /**
     * Open a PNG image file.
     *
     * @param filename image filename
     * @param width image width after opening or 0
     * @param height image height after opening or 0
     * @param depth bytes per pixels.
     * @return Image data as a contiguous float array or 0.
     */
    virtual float *open_png (std::string filename,
                             unsigned int &width,
                             unsigned int &height,
                             unsigned int &depth);

    /**
     * Open a JPG image file.
     *
     * @param filename image filename
     * @param width image width after opening or 0
     * @param height image height after opening or 0
     * @param depth bytes per pixels.
     * @return Image data as a contiguous float array or 0.
     */
    virtual float *open_jpg (std::string filename,
                             unsigned int &width,
                             unsigned int &height,
                             unsigned int &depth);

    /**
     * When using a fparser, one need to use it to recompute data.
     * (Commented out in compute_data()) : If Z data is outside range, it is clamped in zRange.
     */
    virtual void compute_data();

protected:
    /**
     * Actual data as a contiguous float array.
     */
    float * data_;

    /**
     * Data shape alogn width and height.
     */
    Shape shape_;
    
    /**
     * A function parser.
     */
    FunctionParser *fparser_;
    /**
     * Range in x.
     */
    Range range_x_;
    /**
     * Range in y.
     */
    Range range_y_;
    /**
     * Range in z.
     */
    Range range_z_;
};

#endif
