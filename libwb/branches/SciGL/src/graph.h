/*
 * Copyright (C) 2008 Nicolas P. Rougier, Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __GRAPH_H__
#define __GRAPH_H__

#include "scene.h"
#include "basis-cube.h"
#include "smooth-surface.h"
#include "colorbar.h"

/**
 * Class for displaying several SmoothSurface, each one can be defined
 * by a parsed string formula.
 *
 * The scene is made of a BasisCube and several SmoothSurfaces, the
 * range of all axis can be dynamically changed.
 *
 * The image below shows a basic configuration with 2 surfaces
 * (z=exp(x*x+y*y) and z=x+y).
 *
 * Default configuration based on default BasisCube except:
 * - axis_cube_->set_position ( -.5, -.5, -.5);
 * - set_axis_size( 1.0, 1.0, 1.0 );
 *
 * @todo choose consistent API
 * - get/set all objects (colorbar, surfaces, axis_cube), possibly
 * with iterator
 * - only predifined function, like "set_visible_axis/colorbar/surfaces'...
 *
 * \image html graph.png
 */
class Graph : public Scene {

 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Default constructor
   */
  Graph (void);
  /**
   * Destructor
   */
  virtual ~Graph (void);
  //@}

  /**
   * Render the scene.
   *
   * Rendering is done in the following order:
   * -# Widgets with a negative z
   * -# SmoothSurfaces
   * -# BasisCube, so as to allow for transparency
   * -# Widgets with a positive z
   */
  virtual void render (void);

  /**
   * Set the precision of the SmoothSurface to be added.
   *
   * @todo update existing SmootSurface accordingly.
   */
  virtual void set_shape (Shape precision);
  
  /**
   * @name SetAxisSize
   *
   * Sets the size of all axis.
   */
  virtual void set_axis_size (Size s);
  virtual void set_axis_size (float size_x=1, float size_y=1, float size_z=1);
  //@}
  
  /**
   * @name SetRange
   *
   * Set Range = (min, max, nb_major_ticks, nb_minor_ticks)
   * for each coordinate.
   * Update SmoothSurfaces accordingly.
   */
  virtual void set_range_coord_x (Range range);
  virtual void set_range_coord_y (Range range);
  virtual void set_range_coord_z (Range range);
  //@}

  /**
   * Create a new SmoothSurface avec un Data sans donnée mais avec le
  Shape de Graph.
  */
  virtual SmoothSurface *new_surf (void);
  /**
   * Get an existing SmoothSurface.
   *
   * @return ptr on SmoothSurface or NULL
   */
  virtual SmoothSurface *get_surf (unsigned int index);
  /**
   * Get number of SmoothSurface.
   */
  int get_nb_surf (void);

  /**
   * Get BasisCube.
   */
  BasisCube *get_basis_cube (void);

  /**
   * Get ColorBar.
   */
  Colorbar * get_color_bar (void);

 protected:
  /**
   * A BasisCube to display coordinates.
   */
  BasisCube * axis_cube_;
  /**
   * A ColorBar.
   */
  Colorbar * colorbar_;
  /**
   * Shape (and thus precision) of the various SmoothSurface to be
   * added.
   */
  Shape shape_;
};

#endif //__GRAPH_H__
