/*
 * Copyright (C) 2008 Nicolas P. Rougier, Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __BASIS_H__
#define __BASIS_H__

#include "object.h"
#include "font.h"

/**
 * A Basis made of 3 labelled arrows along Ox, Oy, Oz axes.
 *
 * Can apply 'set_position', 'set_size' on it.
 */
class Basis : public Object {
 public:
  // Base
  Basis (void);
  virtual ~Basis (void);
  
  // Rendering
  virtual void render (void);

 private:
  Font *font;
  float font_scale;
  void render_arrow (std::string label);

};

#endif // __BASIS_H__
