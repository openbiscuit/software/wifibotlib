/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "widget.h"


// ============================================================================
bool Widget::mode_set_ = false;
Size Widget::min_size_ = Size (20, 20);
Size Widget::max_size_ = Size (4096, 4096);

// ============================================================================
Widget::Widget (void) : Object()
{
    set_focusable  (true);
    set_visible   (true);
    set_sizeable  (true);
    set_moveable  (true);
    set_margin    (5,5,5,5);
    set_radius    (0);
    set_size      (1,1);
    set_position  (0,0,1);
    set_gravity   (1,1);
    set_fg_color  (0,0,0,1);
    set_bg_color  (1,1,1,1);
    set_br_color  (0,0,0,0);
    action_ = WIDGET_ACTION_NONE;
    mode_owned_ = false;
    fade_in_delay_ = 0;
    fade_out_delay_ = 0;
}

// ============================================================================
Widget::~Widget (void)
{}

// ============================================================================
void
Widget::compute_size (void)
{
    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float width  = viewport[2];
    float height = viewport[3];

    if (size_request_.x < 0) {
        if (size_request_.x > -1.0f) {
            size_.x = (1.0 + size_request_.x)*width;
        } else {
            size_.x = width + size_request_.x;
        }
    } else if (size_request_.x > 0) {
        if (size_request_.x <= 1.0f) {
            size_.x = size_request_.x*width;
        } else {
            size_.x = size_request_.x;
        }
    } else {
        size_.x = width;
    }
    if (size_request_.y < 0) {
        if (size_request_.y > -1.0f) {
            size_.y = (1.0 + size_request_.y)*height;
        } else {
            size_.y = height + size_request_.y;
        }
    } else if (size_request_.y > 0) {
        if (size_request_.y <= 1.0f) {
            size_.y = size_request_.y*height;
        } else {
            size_.y = size_request_.y;
        }
    } else {
        size_.y = height;
    }
    size_.x = int(size_.x);
    size_.y = int(size_.y);
    if (size_.x < min_size_.x) size_.x = min_size_.x;
    if (size_.y < min_size_.y) size_.y = min_size_.y;
    if (size_.x > max_size_.x) size_.x = max_size_.x;
    if (size_.y > max_size_.y) size_.y = max_size_.y;
}

// ============================================================================
void
Widget::compute_position (void)
{
    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float width  = viewport[2];
    float height = viewport[3];

    // Left
    if (gravity_.x == 1)
        // Absolute
        if ((position_request_.x >= 1.0f) or (position_request_.x <= -1.0f))
            position_.x = position_request_.x;
        // Relative
        else
            position_.x = position_request_.x*width;
    // Center
    else if (gravity_.x == 0)
        // Absolute
        if ((position_request_.x >= 1.0f) or (position_request_.x <= -1.0f))
            position_.x = width/2 - size_.x/2 - position_request_.x ;
        // Relative
        else
            position_.x = width/2 - size_.x/2 - position_request_.x*width;
    // Right
    else
        // Absolute
        if ((position_request_.x >= 1.0f) or (position_request_.x <= -1.0f))
            position_.x = width - position_request_.x - size_.x;
        // Relative
        else
            position_.x = (1 - position_request_.x)*width - size_.x;

    // Top
    if (gravity_.y == 1)
        // Absolute
        if ((position_request_.y >= 1.0f) or (position_request_.y <= -1.0f))
            position_.y = position_request_.y;
        // Relative
        else
            position_.y = position_request_.y*height;
    // Center
    else if (gravity_.y == 0)
        // Absolute
        if ((position_request_.y >= 1.0f) or (position_request_.y <= -1.0f))
            position_.y = height/2 - size_.y/2 - position_request_.y;
        // Relative
        else
            position_.y = height/2 - size_.y/2 - position_request_.y*height;
    // Bottom
    else
        // Absolute
        if ((position_request_.y >= 1.0f) or (position_request_.y <= -1.0f))
            position_.y = height - position_request_.y - size_.y;
        // Relative
        else
            position_.y = (1 - position_request_.y)*height - size_.y;

    position_.x = int (position_.x);
    position_.y = int (position_.y);
    position_.z = int (position_request_.z);
}

// ============================================================================
void
Widget::arc_circle (float x, float y, float radius, float theta1, float theta2)
{
    const GLfloat delta_theta = 15.0f ;
    if(theta2 > theta1) {
        for(GLfloat theta = theta1; theta <= theta2; theta += delta_theta) {
            GLfloat theta_rad = theta * 3.14159f / 180.0f ;
            glVertex2f(x+radius*cos(theta_rad), y-radius*sin(theta_rad)) ;
        }
    } else {
        for(GLfloat theta = theta1; theta >= theta2; theta -= delta_theta) {
            GLfloat theta_rad = theta * 3.14159f / 180.0f ;
            glVertex2f(x+radius*cos(theta_rad), y-radius*sin(theta_rad)) ;
        }
    }
}

// ============================================================================
void
Widget::round_rectangle (float x, float y, float w, float h, float radius)
{
    if (radius) {
        glVertex2f  (x,            y-radius);
        glVertex2f  (x,            y-h+1+radius);
        arc_circle  (x+radius,     y-h+1+radius, radius, -180.0f, -270.0f);
        glVertex2f  (x+radius,     y-h+1);
        glVertex2f  (x+w-1-radius, y-h+1);
        arc_circle  (x+w-1-radius, y-h+1+radius, radius, 90.0f, 0.0f);
        glVertex2f  (x+w-1,        y-h+1+radius);
        glVertex2f  (x+w-1,        y-radius);
        arc_circle  (x+w-1-radius, y-radius, radius, 0.0f, -90.0f);
        glVertex2f  (x+w-1-radius, y);
        glVertex2f  (x+radius,     y);
        arc_circle  (x+radius,     y-radius, radius, -90.0f, -180.0f);
    } else {
        glVertex2f (x,   y);
        glVertex2f (x+w, y);
        glVertex2f (x+w, y-h);
        glVertex2f (x,   y-h);
    }
}

// ============================================================================
void
Widget::render_start (void)
{
    compute_visibility();
    compute_size ();
    compute_position ();

    // Enter "2D" mode
    if (not mode_set_ )  {
        GLint viewport[4]; 
        glGetIntegerv (GL_VIEWPORT, viewport);
        glPushAttrib (GL_ENABLE_BIT);
        glMatrixMode (GL_PROJECTION);
        glPushMatrix ();
        glLoadIdentity ();
        glOrtho (0, viewport[2], 0, viewport[3], -1000, 1000);
        glMatrixMode (GL_MODELVIEW);
        glPushMatrix ();
        glLoadIdentity();
        glDisable (GL_TEXTURE_2D);
        glDisable (GL_LIGHTING);
        glEnable (GL_BLEND);
        glLineWidth (1.0f);
        glClear (GL_DEPTH_BUFFER_BIT);
        mode_set_ = true;
        mode_owned_ = true;
    } else {
        mode_owned_ = false;
    }
}

// ============================================================================
void
Widget::render_finish (void)
{
    if ((mode_set_) && (mode_owned_)) {
        glMatrixMode (GL_PROJECTION);
        glPopMatrix ();
        glMatrixMode (GL_MODELVIEW);
        glPopMatrix();
        glPopAttrib ();
        mode_set_ = false;
        mode_owned_ = false;
    }
}

// ============================================================================
void
Widget::render (void)
{
    if (not get_visible()) return;

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float height = viewport[3];

    render_start ();

    glTranslatef (.375,.375,0);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glPolygonOffset (1, 1);
    glEnable (GL_POLYGON_OFFSET_FILL);
    glColor4f (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a*alpha_);
    glBegin (GL_POLYGON);
    round_rectangle (get_position().x, height-1-get_position().y, get_size().x-1, get_size().y-1, radius_);
    glEnd();

    glTranslatef (0,0,1);
    glDepthMask (GL_FALSE);
    glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
    glEnable (GL_LINE_SMOOTH);
    glBegin (GL_LINE_LOOP);
    round_rectangle (get_position().x, height-1-get_position().y, get_size().x-1, get_size().y-1, radius_);
    glEnd ();
    glDepthMask (GL_TRUE);

    render_finish();
}

// ============================================================================
bool
Widget::button_press (int button, int x, int y, int modifier)
{
    if (not get_visible()) return false;
    if (button != 1)   return false;

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float x1 = get_position().x;
    float x2 = get_position().x+get_size().x;
    float y1 = get_position().y;
    float y2 = get_position().y+get_size().y;
    float limit = 10;

    action_ = WIDGET_ACTION_NONE;
    if ((x >= x1) && (x < x2) && (y >= y1) && (y < y2)) {
        if (get_sizeable()) {
            if (x < (x1 + limit)) {
                if (y > (y2 - limit)) {
                    action_ = WIDGET_ACTION_RESIZE_BOTTOM_LEFT;
                } else if (y < (y1 + limit)) {
                    action_ = WIDGET_ACTION_RESIZE_TOP_LEFT;
                } else {
                    action_ = WIDGET_ACTION_RESIZE_LEFT;
                }
            } else if (x > (x2 - limit)) {
                if (y > (y2 - limit)) {
                    action_ = WIDGET_ACTION_RESIZE_BOTTOM_RIGHT;
                } else if (y < (y1 + limit)) {
                    action_ = WIDGET_ACTION_RESIZE_TOP_RIGHT;
                } else {
                    action_ = WIDGET_ACTION_RESIZE_RIGHT;
                }
            } else if (y < (y1 + limit)) {
                action_ = WIDGET_ACTION_RESIZE_TOP;
            } else if (y > (y2 - limit)) {
                action_ = WIDGET_ACTION_RESIZE_BOTTOM;
            } else if (get_moveable()) {
                action_ = WIDGET_ACTION_MOVE;
            }
        } else if (get_moveable()) {
            action_ = WIDGET_ACTION_MOVE;
        }
    }
    action_start_.x = x;
    action_start_.y = y;
    saved_position_.x = position_.x;
    saved_position_.y = position_.y;
    saved_size_.x = size_.x;
    saved_size_.y = size_.y;

    if (action_ != WIDGET_ACTION_NONE)
        return true;
    return false;
}

bool
Widget::button_release (int button, int x, int y, int modifier)
{
    if (action_ != WIDGET_ACTION_NONE) {
        action_start_.x = 0;
        action_start_.y = 0;
        action_ = WIDGET_ACTION_NONE;
        return true;
    }
    return false;
}

bool
Widget::mouse_motion (int button, int x, int y)
{
    if (not get_visible()) return false;
    if (button != 1)       return false;

    float dx = x-action_start_.x;
    float dy = y-action_start_.y;
    switch (action_) {
    case WIDGET_ACTION_NONE:
        return false;
        break;
    case WIDGET_ACTION_MOVE:
        gravity_ = Gravity(1,1);
        position_request_.x = saved_position_.x + dx;
        position_request_.y = saved_position_.y + dy;
        break;
    case WIDGET_ACTION_RESIZE_LEFT:
        if ((saved_size_.x - dx) > 30) {
            position_request_.x = saved_position_.x + dx;
            size_request_.x = saved_size_.x - dx;
        }
        break;
    case WIDGET_ACTION_RESIZE_RIGHT: 
        if ((saved_size_.x + dx) > 30) {
            size_request_.x = saved_size_.x + dx;
        }
        break;
    case WIDGET_ACTION_RESIZE_BOTTOM:
        if ((saved_size_.y + dy) > 30) {
            size_request_.y = saved_size_.y + dy;
        }
        break;
    case WIDGET_ACTION_RESIZE_TOP:
        if ((saved_size_.y - dy) > 30) {
            position_request_.y = saved_position_.y + dy;
            size_request_.y = saved_size_.y - dy;
        }
        break;
    case WIDGET_ACTION_RESIZE_BOTTOM_LEFT:
        if ((saved_size_.x - dx) > 30) {
            position_request_.x  = saved_position_.x + dx;
            size_request_.x = saved_size_.x - dx;
        }
        if ((saved_size_.y + dy) > 30) {
            size_request_.y = saved_size_.y + dy;
        }
        break;
    case WIDGET_ACTION_RESIZE_TOP_LEFT:
        if ((saved_size_.x - dx) > 30) {
            position_request_.x  = saved_position_.x + dx;
            size_request_.x = saved_size_.x - dx;
        }
        if ((saved_size_.y - dy) > 30) {
            position_request_.y  = saved_position_.y + dy;
            size_request_.y = saved_size_.y - dy;
        }
        break;
    case WIDGET_ACTION_RESIZE_BOTTOM_RIGHT:
        if ((saved_size_.x + dx) > 30) {
            size_request_.x = saved_size_.x + dx;
        }
        if ((saved_size_.y + dy) > 30) {
            size_request_.y = saved_size_.y + dy;
        }
        break;
    case WIDGET_ACTION_RESIZE_TOP_RIGHT:
        if ((saved_size_.x + dx) > 30) {
            size_request_.x = saved_size_.x + dx;
        }
        if ((saved_size_.y - dy) > 30) {
            position_request_.y  = saved_position_.y + dy;
            size_request_.y = saved_size_.y - dy;
        }
        break;
    default:
        return false;
        break;
    };
    return true;
}

// ============================================================================
void
Widget::set_position (Position position)
{
    position_request_ = Position (position);
}
void
Widget::set_position (float x, float y, float z)
{
    set_position (Position (x,y,z));
}

// ============================================================================
void
Widget::set_size (Size size)
{
    size_request_ = Size (size);
}
void
Widget::set_size (float x, float y, float z)
{
    set_size (Size (x,y,z));
}

// ============================================================================
//  Gravity get/set
// ============================================================================
void
Widget::set_gravity (Gravity gravity)
{
    if (gravity.x == 0)       gravity_.x = 0;
    else if (gravity.x == -1) gravity_.x = -1;
    else                      gravity_.x = 1;
    if (gravity.y == 0)       gravity_.y = 0;
    else if (gravity.y == -1) gravity_.y = -1;
    else                      gravity_.y = 1;
}
void
Widget::set_gravity (float x, float y)
{
    set_gravity (Gravity (x,y));
}
Gravity
Widget::get_gravity (void)
{
    return gravity_;
}

// ============================================================================
//  Radius get/set
// ============================================================================
void
Widget::set_radius (int radius)
{
    radius_ = radius;
}
int
Widget::get_radius (void)
{
    return radius_;
}

// ============================================================================
//  Margin get/set
// ============================================================================
void
Widget::set_margin (Margin margin)
{
    margin_ = Size (margin);
}
void
Widget::set_margin (float u, float d, float l, float r)
{
   set_margin (Margin (u,d,l,r));
}
Margin
Widget::get_margin (void)
{
    return margin_;
}

void
Widget::set_focusable (bool focusable)
{
    focusable_ = focusable;
}

bool
Widget::get_focusable (void)
{
    return focusable_;
}

bool
Widget::has_focus (int x, int y)
{
    if (not get_visible()) return false;

    float x1 = get_position().x;
    float x2 = get_position().x+get_size().x;
    float y1 = get_position().y;
    float y2 = get_position().y+get_size().y;

    if ((x >= x1) && (x < x2) && (y >= y1) && (y < y2))
        return true;
    return false;


}

// ============================================================================
//  Sizeable get/set
// ============================================================================
void
Widget::set_sizeable (bool sizeable)
{
    sizeable_ = sizeable;
}
bool
Widget::get_sizeable (void)
{
    return sizeable_;
}

// ============================================================================
//  Moveable get/set
// ============================================================================
void
Widget::set_moveable (bool moveable)
{
    moveable_ = moveable;
}
bool
Widget::get_moveable (void)
{
    return moveable_;
}
