/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FLAT_SURFACE_H__
#define __FLAT_SURFACE_H__

#include "object.h"
#include "data.h"
#include "colormap.h"

/**
 * Represententation of a data source as a flat surface.
 *
 * <table border="0"><tr>
 * <td>@image html flat-surface.png</td>
 * <td>@image html image.png</td>
 * </td></table>
 */
class FlatSurface : public Object {
public:

    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor
     */
    FlatSurface (void);

    /**
     * Destructor.
     */
    virtual ~FlatSurface (void);
    //@}


    /**
     *  @name Rendering
     */
    /**
     * Build the surface.
     */
    void build (void);

    /**
     * Update the surface data.
     */
    void update (void);

    /**
     * Render the surface.
     */
    void render	(void);
    //@}


    /**
     *  @name Data
     */
    /**
     * Set data source.
     */
    virtual void set_data (Data *data);
    //@}


    /**
     *  @name Colormap
     */
    /**
     * Set surface colormap.
     */
    virtual void set_cmap (Colormap *cmap);

    /**
     * Get surface colormap.
     */
    virtual Colormap *get_cmap (void);

    /**
     * Get surface colormap.
     */
    virtual Colormap *cmap (void);
    //@}



protected:
    /**
     * Represented data.
     */
    Data * data_;

    /**
     * Colormap of single values data (depth = 1).
     */
    Colormap * cmap_;

    /**
     * Associated GL texture.
     */
    unsigned int texture_;
};

#endif
