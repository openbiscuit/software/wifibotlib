/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJECT_H__
#define __OBJECT_H__
#define GL_GLEXT_PROTOTYPES 1
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <OpenGL/glext.h>
#else
    #include <GL/gl.h>
    #include <GL/glext.h>
#endif
#include <cmath>
#include <string>
#include <vector>
#include <sys/time.h>
#include "vec4f.h"

#define  KEY_ACTIVE_SHIFT 0x0001
#define  KEY_ACTIVE_CTRL  0x0002
#define  KEY_ACTIVE_ALT   0x0004


/**
 * Base class for all renderable objects.
 *
 * An object is defined by its position, its size and a set of three colors
 * (foreround, background and border) that can be used by subsequent classes
 * that inherit from it. An object also implements a set of callbacks that
 * should be called from outside the class in order to get some user
 * interactions.
 *
 * \image html object.png
 */
class Object {

public:

    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor
     */
    Object (void);

    /**
     * Destructor
     */
    virtual ~Object (void);
    //@}



    /**
     *  @name Rendering
     */
    /**
     * Build the object
     */
    virtual void build (void);

    /**
     * Update the object (if necessary)
     */
    virtual void update (void);

    /**
     * Render the object
     */
    virtual void render (void);
    //@}



    /**
     * @name Event processing
     */
    /**
     * Select callback.
     *
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     */
    virtual bool select (int x, int y);

    /**
     * Key press callback.
     *
     * @param key      key
     * @param modifier key modifier
     */
    virtual bool key_press (char key, int modifier = 0);

    /**
     * Key release callback.
     *
     * @param key      key
     * @param modifier key modifier
     */
    virtual bool key_release (char key, int modifier = 0);

    /**
     * Button press callback.
     *
     * @param button pressed button number
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     * @param modifier key modifier
     */
    virtual bool button_press (int button, int x, int y, int modifier = 0);

    /**
     * Button release callback.
     *
     * @param button pressed button number
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     * @param modifier key modifier
     */
    virtual bool button_release (int button, int x, int y, int modifier = 0);

    /**
     * Mouse motion callback.
     *
     * @param button pressed button number
     * @param x      x pointer coordinates (window space)
     * @param y      y pointer coordinates (window space)
     */
    virtual bool mouse_motion (int button, int x, int y);
    //@}



    /**
     * @name Name
     */
    /**
     * Set name.
     *
     * @param name Name
     */
    virtual void set_name (std::string name);

    /**
     * Set name.
     *
     * @param name Name
     */
    virtual void rename (std::string name);

    /**
     * Get name.
     *
     * @return name
     */
    virtual std::string get_name (void) const;

    //@}



    /**
     * @name Visibility
     */
    /**
     * Set visibility.
     *
     * @param visible Visibility
     */
    virtual void set_visible (bool visible);

    /**
     * Get visibility.
     *
     * @return visibility
     */
    virtual bool get_visible (void) const;

    /**
     * Progressive disappearance of the object.
     *
     * To enforce disappearance, object has to use the alpha_ attribute and
     * keep it up to date with the <code>compute_visibility</code> method.
     *
     * @param timeout time (ms) for fade out object
     */
    virtual void hide (float timeout=0);

    /**
     * Progressive appearance of the object.
     *
     * To enforce appearance, object has to use the alpha_ attribute and 
     * keep it up to date with the <code>compute_visiblity</code> method.
     *
     * @param timeout time (ms) for fade out object
     */
    virtual void show (float timeout=0);
    //@}



    /**
     * @name Position
     */
    /**
     * Set position in GL space.
     *
     * @param position Position a tuple of 3 floats.
     */
    virtual void set_position (Position position);

    /**
     * Set position in GL space.
     *
     * @param x position along x axis
     * @param y position along y axis
     * @param z position along z axis
     */
    virtual void set_position (float x=0, float y=0, float z=0);

    /**
     * Set position.
     *
     * @param position Position a tuple of 3 floats.
     */
    virtual void reposition (Position position);

    /**
     * Set position in GL space.
     *
     * @param x position along x axis
     * @param y position along y axis
     * @param z position along z axis
     */
    virtual void reposition (float x=0, float y=0, float z=0);

    /**
     * Set position in GL space.
     *
     * @param position Position a tuple of 3 floats.
     */
    virtual void move (Position position);

    /**
     * Set position in GL space.
     *
     * @param x position along x axis
     * @param y position along y axis
     * @param z position along z axis
     */
    virtual void move (float x=0, float y=0, float z=0);

    /**
     * Get position in GL space.
     *
     * @return position
     */
    virtual Position get_position (void) const;
    //@}



    /**
     * @name Size
     */
    /**
     * Set bounding box size.
     *
     * @param size Size as a tuple of 3 floats.
     */
    virtual void set_size (Size size);

    /**
     * Set bounding box size in GL space.
     *
     * @param x size along x axis
     * @param y size along y axis
     * @param z size along z axis
     */
    virtual void set_size (float x=1, float y=1, float z=1);

    /**
     * Set bounding box size in GL space.
     *
     * @param size Size as a tuple of 3 floats.
     */
    virtual void resize (Size size);

    /**
     * Set bounding box size in GL space.
     *
     * @param x size along x axis
     * @param y size along y axis
     * @param z size along z axis
     */
    virtual void resize (float x=1, float y=1, float z=1);

    /**
     * Get bounding box size in GL space.
     *
     */
    virtual Size get_size (void) const;
    //@}



    /** 
     * @name Foreground color
     */
    /**
     * Set foreground color.
     *
     * @param color Color as a tuple of 4 floats.
     */
    virtual void  set_fg_color (Color color);

    /**
     * Set foreground color.
     *
     * @param r Red channel
     * @param g Green channel
     * @param b Blue channel
     * @param a Alpha channel
     */
    virtual void  set_fg_color (float r=0, float g=0, float b=0, float a=1);

    /**
     * Get foreground color.
     *
     * @return foreground color
     */
    virtual Color get_fg_color (void) const;
    //@}



    /**
     * @name Background color
     */
    /**
     * Set background color.
     *
     * @param color Color as a tuple of 4 floats.
     */
    virtual void set_bg_color (Color color);

    /**
     * Set background color.
     *
     * @param r Red channel
     * @param g Green channel
     * @param b Blue channel
     * @param a Alpha channel
     */
    virtual void set_bg_color (float r=0, float g=0, float b=0, float a=1);

    /**
     * Get background color.
     *
     * @return background color
     */
    virtual Color get_bg_color (void) const;
    //@}


    /**
     * @name Border color
     */
    /**
     * Set border color.
     *
     * @param color Color as a tuple of 4 floats.
     */
    virtual void set_br_color (Color color);

    /**
     * Set border color.
     *
     * @param r Red channel
     * @param g Green channel
     * @param b Blue channel
     * @param a Alpha channel
     */
    virtual void set_br_color (float r=0, float g=0, float b=0, float a=1);

    /**
     * Get border color.
     *
     * @return border color
     */
    virtual Color get_br_color (void) const;
    //@}



protected:
    /**
     * Update visibility status.
     *
     * Depending on #fade_in_delay_ or #fade_out_delay_ and #fade_time_,
     * #alpha_ and #visible_ are updated accordingly.
     */
    virtual void compute_visibility (void);



protected:
    /**
     * Name of the object
     */
    std::string name_;

    /**
     * Whether object is visible or not.
     */
    bool visible_;

    /**
     * Position.
     */
    Position position_;

    /**
     * Bounding box.
     */
    Size size_;

    /**
     * Foreground color.
     */
    Color fg_color_;

    /**
     * Background color.
     */
    Color bg_color_;

    /**
     * Border color.
     */
    Color br_color_;

    /**
     * Global transparency level.
     */
    float alpha_;

    /**
     * Last fade in/out start time.
     */
    struct timeval fade_time_;

    /**
     * Fade in time delay.
     */
    float fade_in_delay_;

    /**
     * Fade out time delay.
     */
    float fade_out_delay_;
};

#endif
