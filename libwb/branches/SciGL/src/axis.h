/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __AXIS_H__
#define __AXIS_H__
#include <string>
#include "font.h"
#include "object.h"


class Axis : public Object {
// ----------------------------------------------------------------------------
protected:
    std::string          label_;            // Axis label
    Range                range_;            // X axis range

// ----------------------------------------------------------------------------
public:
    // Base
    Axis (void);
    virtual ~Axis (void);

    // Rendering
    virtual void render (void);

    // Position
    virtual void set_position (float x=-.5);

    // Size
    virtual void set_size (float x=1);
    
    // Label
    virtual void        set_label (std::string label);
    virtual std::string get_label (void);

    // Ranges
    virtual void  set_range (Range range);
    virtual void  set_range (float min, float max);
    virtual Range get_range (void);
};

#endif
