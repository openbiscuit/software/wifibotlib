/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif
#include "font.h"

Font::Font (void)
{
    base_ = 0;
    texture_ = 0;
}

Font::~Font (void)
{}

void
Font::build (const unsigned char *data, int width, int height)
{
    glGenTextures (1,  (GLuint *) &texture_);
    glBindTexture (GL_TEXTURE_2D, texture_);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0,
                  GL_ALPHA, GL_UNSIGNED_BYTE, data);
    base_ = glGenLists (256);
    glyph_size_ = Size (width/16.0, height/16.0);
    float dx = glyph_size_.x / float (width);
    float dy = glyph_size_.y / float (height);
    for (int y=0; y<16; y++) {
        for (int x=0; x<16; x++) {
            glNewList (base_ + y*16+x, GL_COMPILE);
            if (((y%8)*16+x) == '\n') {
                glPopMatrix ();
                glTranslatef (0, -glyph_size_.y, 0);
                glPushMatrix ();
            } else if (((y%8)*16+x) == '\t') {
                glTranslatef (4*(glyph_size_.x-1), 0, 0);
            } else {
                glBegin(GL_QUADS);
                glTexCoord2f ((x  )*dx,      (y  )*dy);
                glVertex2f   (0,             glyph_size_.y);
                glTexCoord2f ((x  )*dx,      (y+1)*dy);
                glVertex2f   (0,             0);
                glTexCoord2f ((x+1)*dx,      (y+1)*dy);
                glVertex2f   (glyph_size_.x, 0);
                glTexCoord2f ((x+1)*dx,      (y  )*dy);
                glVertex2f   (glyph_size_.x, glyph_size_.y);
                glEnd();
                if (((y%8)*16+x) != '\0')
                    glTranslatef (glyph_size_.x-1, 0, 0);
            }
            glEndList();            
        }
    }
}

void
Font::render (std::string text, bool ansi, float alpha)
{
    if ((not base_) or (not texture_))
        return;

    glBindTexture (GL_TEXTURE_2D, texture_);
    if (ansi == false) {
        glListBase (base_);
        glCallLists (text.size(), GL_UNSIGNED_BYTE, text.c_str());
        glListBase (0);
        return;
    }
    int base = base_;

    Color default_color;
    glGetFloatv (GL_CURRENT_COLOR, default_color.data);
    Color fg_color = default_color;

    Color bg_color;
    glGetFloatv (GL_COLOR_CLEAR_VALUE, bg_color.data);
    bg_color.alpha = 0.0;

    bool inverse = false;
    Color sfg, sbg;
    const char *line = text.c_str();

    glPushMatrix();
    int i=0;
    int c=0;
    while (line[i] != 0) {
        if (strstr ((line+i), "\033[") == (char *)(line+i)) {
            int sequence = 1;
            i+=2;
            while (sequence) {
                /* Reset terminal attributes */
                if (strstr ((line+i), "00") == (char *)(line +i)) {
                    base = base_;
                    fg_color = default_color;
                    fg_color.alpha = 1;
                    bg_color = Color (1,1,1,0);
                    i+=2;
                /* Bold on */
                } else if (strstr ((line+i), "01") == (char *)(line +i)) {
                    base = base_ + 128;
                    i+=2;
                /* Bold off */
                } else if (strstr ((line+i), "22") == (char *)(line +i)) {
                    base = base_;
                    i+=2;
                /* Italic on */
                } else if (strstr ((line+i), "03") == (char *)(line +i)) {
                    fg_color.alpha = .5;
                    i+=2;
                /* Italic off */    
                } else if (strstr ((line+i), "23") == (char *)(line +i)) {
                    fg_color.alpha = 1;
                    i+=2;
                /* Underline on */
                } else if (strstr ((line+i), "04") == (char *)(line +i)) {
                    i+=2;
                /* Inverse on */
                } else if (strstr ((line+i), "07") == (char *)(line +i)) {
                    inverse = true;
                    sfg = fg_color;
                    sbg = bg_color;
                    fg_color = sbg;
                    fg_color.alpha = 1;
                    bg_color = sfg;
                    i+=2;
                /* Inverse off */
                } else if (strstr ((line+i), "27") == (char *)(line +i)) {
                    if (inverse) {
                        fg_color = sfg;
                        bg_color = sbg;
                        inverse = false;
                    }
                    i+=2;
                /* Underline off */
                } else if (strstr ((line+i), "24") == (char *)(line +i)) {
                    i+=2;
                /* Black foreground */
                } else if (strstr ((line+i), "30") == (char *)(line +i)) {
                    fg_color = Color (0,0,0,fg_color.alpha);
                    i+=2;    
                /* Red foreground */
                } else if (strstr ((line+i), "31") == (char *)(line +i)) {
                    fg_color = Color (1,0,0,fg_color.alpha);
                    i+=2;
                /* Green foreground */
                } else if (strstr ((line+i), "32") == (char *)(line +i)) {
                    fg_color = Color (0,1,0,fg_color.alpha);
                    i+=2;    
                /* Yellow foreground */
                } else if (strstr ((line+i), "33") == (char *)(line +i)) {
                    fg_color = Color (1,1,0,fg_color.alpha);
                    i+=2;    
                /* Blue foreground */
                } else if (strstr ((line+i), "34") == (char *)(line +i)) {
                    fg_color = Color (0,0,1,fg_color.alpha);
                    i+=2;    
                /* Magenta foreground */
                } else if (strstr ((line+i), "35") == (char *)(line +i)) {
                    fg_color = Color (1,0,1,fg_color.alpha);
                    i+=2;
                /* Cyan foreground */
                } else if (strstr ((line+i), "36") == (char *)(line +i)) {
                    fg_color = Color (0,1,1,fg_color.alpha);
                    i+=2;
                /* White foreground */
                } else if (strstr ((line+i), "37") == (char *)(line +i)) {
                    fg_color = Color (1,1,1,fg_color.alpha);
                    i+=2;
                /* Default foreground */
                } else if (strstr ((line+i), "39") == (char *)(line +i)) {
                    fg_color = default_color;
                    i+=2;    
                /* Black background */
                } else if (strstr ((line+i), "40") == (char *)(line +i)) {
                    bg_color = Color (0,0,0,1);
                    i+=2;
                /* Red background */
                } else if (strstr ((line+i), "41") == (char *)(line +i)) {
                    bg_color = Color (1,0,0,1);
                    i+=2;    
                /* Green background */
                } else if (strstr ((line+i), "42") == (char *)(line +i)) {
                    bg_color = Color (0,1,0,1);
                    i+=2;    
                /* Yellow background */
                } else if (strstr ((line+i), "43") == (char *)(line +i)) {
                    bg_color = Color (1,1,0,1);
                    i+=2;
                /* Blue background */
                } else if (strstr ((line+i), "44") == (char *)(line +i)) {
                    bg_color = Color (0,0,1,1);
                    i+=2;
                /* Magenta background */
                } else if (strstr ((line+i), "45") == (char *)(line +i)) {
                    bg_color = Color (1,0,1,1);
                    i+=2;    
                /* Cyan background */
                } else if (strstr ((line+i), "46") == (char *)(line +i)) {
                    bg_color = Color (0,1,1,1);
                    i+=2;    
                /* White background */
                } else if (strstr ((line+i), "47") == (char *)(line +i)) {
                    bg_color = Color (1,1,1,1);
                    i+=2;
                /* Default background */
                } else if (strstr ((line+i), "49") == (char *)(line +i)) {
                    bg_color = Color (1,1,1,0);
                    i+=2;
                /* Reset terminal attributes */
                } else if (strstr ((line+i), "0") == (char *)(line +i)) {
                    base = base_;
                    fg_color = default_color;
                    bg_color = Color (1,1,1,0);
                    glColor4fv (fg_color.data);
                    i+=1;
                /* Bold on */
                } else if (strstr ((line+i), "1") == (char *)(line +i)) {
                    base = base_+128;
                    i+=1;
                /* Italic on */
                } else if (strstr ((line+i), "3") == (char *)(line +i)) {
                    fg_color.alpha = .5;
                    glColor4fv (fg_color.data);                    
                    i+=1;
                /* Underline on */
                } else if (strstr ((line+i), "4") == (char *)(line +i)) {
                    i+=1;
                /* Inverse on */
                } else if (strstr ((line+i), "7") == (char *)(line +i)) {
                    Color color = fg_color;
                    fg_color = bg_color;
                    bg_color = color;
                    i+=2;
                }
                /* End of sequence */
                if (line[i++] == ';')
                    sequence = 1;
                else 
                    sequence = 0;
            }
            if (line[i] == 0)
                break;
        }
        else {
            if (bg_color.a*alpha > 0) {
                glColor4f (bg_color.r,bg_color.g, bg_color.b, bg_color.a*alpha);
                glCallList (base_);
            }
            glColor4f (fg_color.r,fg_color.g, fg_color.b, fg_color.a*alpha);
            glCallList (base+text[i]);
            i++;
        }
        c++;
    }
    glPopMatrix();
}



Size
Font::size (std::string text) const
{
    const char *line = text.c_str();
    int i=0;
    int c=0;
    std::string t;

    while (line[i] != 0) {
        if (strstr ((line+i), "\033[") == (char *)(line+i)) {
            int sequence = 1;
            i+=2;
            while (sequence) {
                /* Reset terminal attributes */
                if (strstr ((line+i), "00") == (char *)(line +i)) {
                    i+=2;
                /* Bold on */
                } else if (strstr ((line+i), "01") == (char *)(line +i)) {
                    i+=2;
                /* Bold off */
                } else if (strstr ((line+i), "22") == (char *)(line +i)) {
                    i+=2;
                /* Italic on */
                } else if (strstr ((line+i), "03") == (char *)(line +i)) {
                    i+=2;
                /* Italic off */    
                } else if (strstr ((line+i), "23") == (char *)(line +i)) {
                    i+=2;
                /* Underline on */
                } else if (strstr ((line+i), "04") == (char *)(line +i)) {
                    i+=2;
                /* Inverse on */
                } else if (strstr ((line+i), "07") == (char *)(line +i)) {
                    i+=2;
                /* Inverse off */
                } else if (strstr ((line+i), "27") == (char *)(line +i)) {
                    i+=2;
                /* Underline off */
                } else if (strstr ((line+i), "24") == (char *)(line +i)) {
                    i+=2;
                /* Black foreground */
                } else if (strstr ((line+i), "30") == (char *)(line +i)) {
                    i+=2;    
                /* Red foreground */
                } else if (strstr ((line+i), "31") == (char *)(line +i)) {
                    i+=2;
                /* Green foreground */
                } else if (strstr ((line+i), "32") == (char *)(line +i)) {
                    i+=2;    
                /* Yellow foreground */
                } else if (strstr ((line+i), "33") == (char *)(line +i)) {
                    i+=2;    
                /* Blue foreground */
                } else if (strstr ((line+i), "34") == (char *)(line +i)) {
                    i+=2;    
                /* Magenta foreground */
                } else if (strstr ((line+i), "35") == (char *)(line +i)) {
                    i+=2;
                /* Cyan foreground */
                } else if (strstr ((line+i), "36") == (char *)(line +i)) {
                    i+=2;
                /* White foreground */
                } else if (strstr ((line+i), "37") == (char *)(line +i)) {
                    i+=2;
                /* Default foreground */
                } else if (strstr ((line+i), "39") == (char *)(line +i)) {
                    i+=2;    
                /* Black background */
                } else if (strstr ((line+i), "40") == (char *)(line +i)) {
                    i+=2;
                /* Red background */
                } else if (strstr ((line+i), "41") == (char *)(line +i)) {
                    i+=2;    
                /* Green background */
                } else if (strstr ((line+i), "42") == (char *)(line +i)) {
                    i+=2;    
                /* Yellow background */
                } else if (strstr ((line+i), "43") == (char *)(line +i)) {
                    i+=2;
                /* Blue background */
                } else if (strstr ((line+i), "44") == (char *)(line +i)) {
                    i+=2;
                /* Magenta background */
                } else if (strstr ((line+i), "45") == (char *)(line +i)) {
                    i+=2;    
                /* Cyan background */
                } else if (strstr ((line+i), "46") == (char *)(line +i)) {
                    i+=2;    
                /* White background */
                } else if (strstr ((line+i), "47") == (char *)(line +i)) {
                    i+=2;
                /* Default background */
                } else if (strstr ((line+i), "49") == (char *)(line +i)) {                    
                    i+=2;
                /* Reset terminal attributes */
                } else if (strstr ((line+i), "0") == (char *)(line +i)) {
                    i+=1;
                /* Bold on */
                } else if (strstr ((line+i), "1") == (char *)(line +i)) {
                    i+=1;
                /* Italic on */
                } else if (strstr ((line+i), "3") == (char *)(line +i)) {
                    i+=1;
                /* Underline on */
                } else if (strstr ((line+i), "4") == (char *)(line +i)) {
                    i+=1;
                /* Inverse on */
                } else if (strstr ((line+i), "7") == (char *)(line +i)) {
                    i+=2;
                }
                /* End of sequence */
                if (line[i++] == ';')
                    sequence = 1;
                else 
                    sequence = 0;
            }
            if (line[i] == 0)
                break;
        }
        else {
            t += text[i];
            i++;
        }
        c++;
    }

    return Size (t.size()*(glyph_size_.x-1), glyph_size_.y, 0);
}

Size
Font::get_glyph_size (void) const
{
    return glyph_size_;
}

Size
Font::glyph_size (void) const
{
    return get_glyph_size ();
}

Font *
Font::Font12 (void) {
    static Font *font = 0;
    if (font == 0) {
        font = new Font();
        font->build (font_12.data, font_12.width, font_12.height);
    }
    return font;
}

Font *
Font::Font16 (void) {
    static Font *font = 0;
    if (font == 0) {
        font = new Font();
        font->build (font_16.data, font_16.width, font_16.height);
    }
    return font;
}

Font *
Font::Font24 (void) {
    static Font *font = 0;
    if (font == 0) {
        font = new Font();
        font->build (font_24.data, font_24.width, font_24.height);
    }
    return font;
}

Font *
Font::Font32 (void) {
    static Font *font = 0;
    if (font == 0) {
        font = new Font();
        font->build (font_32.data, font_32.width, font_32.height);
    }
    return font;
}
