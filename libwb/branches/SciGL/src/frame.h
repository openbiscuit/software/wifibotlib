/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FRAME_H__
#define __FRAME_H__
#include "object.h"
#include "axis.h"


typedef struct face {
    int index;
    float vertices[4*3];
    float normal[3];
    float z;
} face;


class Frame : public Object {
protected:
    Axis x_axis_;
    Axis y_axis_;
    Axis z_axis_;

protected:
    /**
     * Is z of face 'a' bigger that z of face 'b'.
     */  
    static int compare (const void *a, const void *b);
    void render_plane (float x, float y, int nx=0, int ny=0, int outline=1);

public:
    // Base
    Frame (void);
    virtual ~Frame (void);
    virtual void update(void);

    // Rendering
    virtual void render (void);

    // Visibility
    virtual void hide (float timeout=0);
    virtual void show (float timeout=0);

    // size
    virtual void set_frame_size(float x, float y, float z=1);

    // Axis range
    virtual void set_x_range (Range range);
    virtual void set_y_range (Range range);
    virtual void set_z_range (Range range);
    virtual void set_x_label (std::string label);
    virtual void set_y_label (std::string label);
    virtual void set_z_label (std::string label);
    virtual void set_x_position(double pos);
    virtual void set_y_position(double pos);
    virtual void set_z_position(double pos);
    virtual void set_x_size(int size);
    virtual void set_y_size(int size);
    virtual void set_z_size(int size);
};

#endif
