/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "trackball.h" 
#include "scene.h"

Scene::Scene (void) : Widget()
{
    view_ = Vec4f (0,0,0,1);
    set_orientation (65, 135);
    zoom_ = 2.5;
    zoom_max_ = 20;
    zoom_min_ = 0.25f;
    start_ = Position (-1,-1);
    fps_ = 40;
    focus_ = 0;
}

Scene::~Scene (void)
{}

void
Scene::build (void)
{
    glEnable (GL_DEPTH_TEST);
    glClearDepth (1.0f); 
    glDepthFunc (GL_LEQUAL);
    glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glShadeModel (GL_SMOOTH);
    glEnable (GL_NORMALIZE);
    glEnable (GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);
    GLfloat ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};
    GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat position[] = {2.0f, 2.0f, 2.0f, 0.0f};
    glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv (GL_LIGHT0, GL_POSITION, position);
    glEnable (GL_LIGHT0);
    glEnable (GL_LIGHTING);
}

void
Scene::update (void)
{
    for (unsigned int i=0; i<widgets_.size(); i++)
        widgets_.at(i)->update();
    for (unsigned int i=0; i<objects_.size(); i++)
        objects_.at(i)->update();
}

void
Scene::render (void)
{
    glClearColor (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    glTranslatef (0.0, 0.0, -5.0f);
    glScalef (zoom_, zoom_, zoom_);
    float m[4][4];
    build_rotmatrix (m, view_.data);
    glMultMatrixf (&m[0][0]);

    // Back widgets
   // glDisable (GL_DEPTH_TEST);
    for (unsigned int i=0; i<widgets_.size(); i++)
        if (widgets_.at(i)->get_position().z < 0)
            widgets_.at(i)->render();

    // Objects
    glEnable (GL_DEPTH_TEST);
    for (unsigned int i=0; i<objects_.size(); i++)
        objects_.at(i)->render();

    // Front widgets
    glDisable (GL_DEPTH_TEST);
    for (unsigned int i=0; i<widgets_.size(); i++)
        if (widgets_.at(i)->get_position().z >= 0)
            widgets_.at(i)->render();
}

void
Scene::save (std::string filename) {
    GLuint framebuffer, renderbuffer;
    GLenum status;

    GLuint width = int(get_size().x), height = int(get_size().y);
    glGenFramebuffersEXT (1, &framebuffer);
    glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, framebuffer);
    glGenRenderbuffersEXT (1, &renderbuffer);
    glBindRenderbufferEXT (GL_RENDERBUFFER_EXT, renderbuffer);
    glRenderbufferStorageEXT (GL_RENDERBUFFER_EXT, GL_RGBA8, width, height);
    glFramebufferRenderbufferEXT (GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
                                  GL_RENDERBUFFER_EXT, renderbuffer);
    status = glCheckFramebufferStatusEXT (GL_FRAMEBUFFER_EXT);
    if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
        return;
    render();
    glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
    GLubyte *buffer = new GLubyte[width*height*3];
    glPixelStorei (GL_PACK_ALIGNMENT, 1);
    glReadPixels (0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, buffer);
    FILE *file = fopen (filename.c_str(), "wb");
    fprintf (file, "P6 %d %d 255\n", (int) width, (int) height);
    fwrite (buffer, sizeof(GLubyte), width * height * 3, file);
    fclose (file);
    delete buffer;
    glDeleteRenderbuffersEXT (1, &renderbuffer);
}

void
Scene::add (Object *object)
{
    objects_.push_back (object);
}

void
Scene::add (Widget *widget)
{
    unsigned int i;
    for (i=0; i<widgets_.size(); i++) {
        if (widget->get_position().z < widgets_[i]->get_position().z) {
            widgets_.insert (widgets_.begin()+i, widget);
            return;
        }
    }
    widgets_.push_back (widget);
}

bool
Scene::select (int x, int y)
{
    return false;
}

bool
Scene::key_press (char key, int modifier)
{
    if (not key)
        return false;
    for (unsigned int i=0; i<widgets_.size(); i++)
        if (widgets_[i]->key_press (key, modifier))
            return true;
    for (unsigned int i=0; i<objects_.size(); i++)
        if (objects_[i]->key_press (key, modifier))
            return true;
    return false;
}

bool
Scene::key_release (char key, int modifier)
{
    return false;
}

bool
Scene::button_press (int button, int x, int y, int modifier)
{
    focus_ = 0;
    if (not has_focus(x,y))
        return false;

    if (modifier & KEY_ACTIVE_SHIFT) {
        for (unsigned int i=0; i<widgets_.size(); i++)
            if (widgets_[i]->has_focus(x,y))
                focus_ = widgets_[i];
        if (focus_) {
            focus_->button_press (button,x,y,modifier);
            return true;
        }
    }

    if (button == 1) {
        start_ = Position(x,y);
        return true;
    } else if (button == 2) {
        start_ = Position(x,y);
        return true;
    }

    return false;
}

bool
Scene::button_release (int button, int x, int y, int modifier)
{
    return false;
}

bool
Scene::mouse_motion (int button, int x, int y)
{
    if (focus_) {
        focus_->mouse_motion (button,x,y);
        return true;
    }

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float w = viewport[2];
    float h = viewport[3];

    if (button == 1) {
        float d_quat[4];
        trackball (d_quat,
                   (2.0 * start_.x - w) / w,
                   (h - 2.0 * start_.y) / h,
                   (2.0 * x - w) / w,
                   (h - 2.0 * y) / h);
        add_quats (d_quat, view_.data, view_.data);
        start_.x = x;
        start_.y = y;
        return true;
    } else if (button == 2) {
        zoom_ = zoom_ * (1.0 - (y - start_.y) / h);
        if (zoom_ > zoom_max_) {
            zoom_ = zoom_max_;
        } else if (zoom_ < zoom_min_) {
            zoom_ = zoom_min_;
        }
        start_.x = x;
        start_.y = y;
    }

    return false;
}

void
Scene::set_fps (unsigned int fps)
{
    fps_ = fps;
}

unsigned int
Scene::get_fps (void)
{
    return fps_;
}

unsigned int
Scene::fps (void)
{
    return get_fps();
}

void
Scene::set_zoom (float zoom)
{
    zoom_ = zoom;
    if (zoom_ > zoom_max_)
        zoom_ = zoom_max_;
    if (zoom_ < zoom_min_)
        zoom_ = zoom_min_;
}
float
Scene::get_zoom (void)
{
    return zoom_;
}
float
Scene::zoom (void)
{
    return get_zoom();
}

void
Scene::set_size (Size size)
{
    size_ = Size (size);
    unsigned int width = (unsigned int) size_.x;
    unsigned int height = (unsigned int) size_.y;
    glViewport (0, 0, width, height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();

    float aspect = 1.0f;
    //    if (width > height) 
    aspect = width / float(height);
    //    else
    //        aspect = height / float(width);
    float aperture = 30.0f;
    float near = 1.0f;
    float far = 100.0f;
    float top = tan(aperture*3.14159/360.0) * near;
    float bottom = -top;
    float left = aspect * bottom;
    float right = aspect * top;
    // gluPerspective (aperture, aspect, near, far);
    glFrustum (left, right, bottom, top, near, far);
    glMatrixMode (GL_MODELVIEW);
}

void
Scene::set_size (float x, float y)
{
    set_size (Size (x,y));
}

void
Scene::resize (Size size)
{
    set_size (size);
}

void
Scene::resize (float x, float y)
{
    set_size (Size (x,y));
}

void
Scene::set_orientation (Orientation orientation)
{
    orientation_ = Orientation (orientation);
    view_ = Vec4f (0,0,0,1);

    float xrot[4], zrot[4];
    float angle, sine;

    angle = orientation_.x * (M_PI / 180.0f);
    sine = sin (0.5 * angle);
    xrot[0] = 1 * sine;
    xrot[1] = 0 * sine;
    xrot[2] = 0 * sine;
    xrot[3] = cos (0.5 * angle);

    angle = orientation.y * (M_PI / 180.0f);
    sine = sin (0.5 * angle);
    zrot[0] = 0 * sine;
    zrot[1] = 0 * sine;
    zrot[2] = 1 * sine;
    zrot[3] = cos (0.5 * angle);
    add_quats (xrot, zrot, view_.data);
}

void
Scene::set_orientation (float x, float z)
{
    set_orientation (Orientation (x,z));
}

void
Scene::reorient (Orientation orientation)
{
    set_orientation (orientation);
}

void
Scene::reorient (float x, float z)
{
    set_orientation (Orientation (x,z));
}


bool
Scene::has_focus (int x, int y)
{
    bool focus = false;
    for (unsigned int i=0; i<widgets_.size(); i++)
        focus |= widgets_[i]->has_focus (x,y);
    focus |= Widget::has_focus(x,y);
    return focus;
}
