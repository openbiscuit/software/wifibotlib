/*
 * Copyright (C) 2008 Nicolas P. Rougier, Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "basis.h"

// ============================================================================
Basis::Basis (void) : Object()
{
  set_size      (0.5, 0.5, 0.5);
  set_position  (0,0,0);

  font = Font::Font24();
  font_scale = 0.01;
  }
// ============================================================================
Basis::~Basis (void)
{}
// ============================================================================
void
Basis::render (void)
{
  compute_visibility();
  if (not get_visible()) return;

  // set position and size
  glPushMatrix();
  glScalef (get_size().x, get_size().y, get_size().z);
  glTranslatef (get_position().x, get_position().y, get_position().z);

  // 3 arrows
  glColor3f ( 1.f , 0, 0);
  render_arrow ("X");

  glRotatef ( 90, 0, 0, 1);
  glColor3f (0.0f, 1.0f, 0.0f);
  render_arrow ("Y");

  glRotatef ( -90, 0, 1, 0);
  glColor3f (0.0f, 0.0f, 1.0f);
  render_arrow ("Z");
  
  glPopMatrix();
}

void
Basis::render_arrow( std::string label )
{
  glBegin(GL_LINE_STRIP); {
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);
    glVertex3f(0.75, 0.25, 0.0);
    glVertex3f(0.75, -0.25, 0.0);
    glVertex3f(1.0, 0.0, 0.0);
    glVertex3f(0.75, 0.0, 0.25);
    glVertex3f(0.75, 0.0, -0.25);
    glVertex3f(1.0, 0.0, 0.0);
  }
  glEnd();
  // print label
  //Size s = font->size (label);
  glEnable (GL_TEXTURE_2D);
  glEnable (GL_BLEND);
  glPushMatrix(); {
    glRotatef( 90, 1, 0, 0);
    glTranslatef ( 1.1, 0, 0 );
    glScalef (font_scale, font_scale, font_scale );
    font->render (label);
  }
  glPopMatrix();
  glDisable (GL_BLEND);
  glDisable (GL_TEXTURE_2D);
  
}
