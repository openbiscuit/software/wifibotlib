/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <png.h>
extern "C" {
#    include <jpeglib.h>
}
#include "flat-surface.h"

FlatSurface::FlatSurface (void)
{
    data_ = 0;
    texture_ = 0;
    //    cmap_ = Colormap::Grey();
    cmap_ = Colormap::Hot();
}

FlatSurface::~FlatSurface (void)
{
    if (texture_)
      glDeleteTextures (1, (const GLuint *) &texture_);
}

void
FlatSurface::update (void)
{
    if (texture_)
        glDeleteTextures (1, (const GLuint *) &texture_);
    build();
}

void
FlatSurface::build (void)
{
    if (not data_)         return;
    if (not data_->data()) return;
    if (not data_->shape().height) return;

    glGenTextures (1, (GLuint *) &texture_);
    glBindTexture (GL_TEXTURE_2D, texture_);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    unsigned int w = (unsigned int) (data_->shape().width);
    unsigned int h = (unsigned int) (data_->shape().height);
    unsigned int d = (unsigned int) (data_->shape().depth);

    float *rgb_data;

    switch (d) {
    case 1:
        rgb_data = new float[w*h*3];
        for (unsigned int i=0; i<(w*h); i++) {
            Color c = (*cmap_)((data_->data())[i]);
            rgb_data[i*3+0] = c.r;
            rgb_data[i*3+1] = c.g;
            rgb_data[i*3+2] = c.b;
        }
        glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, w, h, 0,
                      GL_RGB, GL_FLOAT, rgb_data);
        delete [] rgb_data;

        break;
    case 2:
        glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, w, h, 0,
                      GL_LUMINANCE_ALPHA, GL_FLOAT, data_->data());
        break;
    case 3:
        glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, w, h, 0,
                      GL_RGB, GL_FLOAT, data_->data());
        break;
    case 4:
        glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, w, h, 0,
                      GL_RGBA, GL_FLOAT, data_->data());
        break;
    default:
        glDeleteTextures (1, (const GLuint *) &texture_);
        texture_ = 0;
        break;
    }
}

void
FlatSurface::render (void)
{
    if (not data_)    return;
    if (not texture_) build();
    if (not texture_) return;
    if (!get_visible())   return;
    Object::compute_visibility();

    glDisable (GL_LIGHTING);
    glPolygonOffset (1.0f, 1.0f);
    glEnable (GL_POLYGON_OFFSET_FILL);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glBindTexture (GL_TEXTURE_2D, texture_);
    glEnable (GL_TEXTURE_2D);
    glEnable (GL_BLEND);

    glColor4f (1,1,1,alpha_);
    glNormal3f (0,0,1);
    glBegin (GL_QUADS);
    glTexCoord2f (0, 0); glVertex2f (get_position().x-get_size().x/2, get_position().y-get_size().y/2);
    glTexCoord2f (1, 0); glVertex2f (get_position().x+get_size().x/2, get_position().y-get_size().y/2);
    glTexCoord2f (1, 1); glVertex2f (get_position().x+get_size().x/2, get_position().y+get_size().y/2);
    glTexCoord2f (0, 1); glVertex2f (get_position().x-get_size().x/2, get_position().y+get_size().y/2);
    glEnd();

    glLineWidth (2.0f);
    glEnable (GL_LINE_SMOOTH);
    glDisable (GL_LIGHTING);
    glDisable (GL_POLYGON_OFFSET_FILL);
    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
    glBegin (GL_QUADS);
    glVertex2f (get_position().x-get_size().x/2, get_position().y-get_size().y/2);
    glVertex2f (get_position().x+get_size().x/2, get_position().y-get_size().y/2);
    glVertex2f (get_position().x+get_size().x/2, get_position().y+get_size().y/2);
    glVertex2f (get_position().x-get_size().x/2, get_position().y+get_size().y/2);
    glEnd();
}

void
FlatSurface::set_data (Data *data)
{
    data_ = data;
    if (texture_)
        glDeleteTextures (1, (const GLuint *) &texture_);
    texture_ = 0;
}

void
FlatSurface::set_cmap (Colormap *cmap)
{
    cmap_ = cmap;
}

Colormap *
FlatSurface::get_cmap (void)
{
    return cmap_;
}

Colormap *
FlatSurface::cmap (void)
{
    return get_cmap();
}
