/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif
#include "viewport.h"

Viewport::Viewport (void) : Widget()
{}

Viewport::~Viewport (void)
{}

void
Viewport::build (void)
{}

void
Viewport::update (void)
{}

void
Viewport::render (void)
{
    static Object object;

    compute_visibility();
    if (not get_visible()) return;

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float height = viewport[3];

    glPushAttrib (GL_VIEWPORT_BIT);
    render_start ();

    // Anti-alias border
    glTranslatef (.375,.375,0);
    glDepthMask (GL_FALSE);
    glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
    glEnable (GL_LINE_SMOOTH);
    glBegin (GL_LINE_LOOP);
    round_rectangle (get_position().x, height-1-get_position().y, get_size().x-1, get_size().y-1, radius_);
    glEnd ();
    glDepthMask (GL_TRUE);
    glTranslatef (-.375,-.375,0);

    // Scissor test to frame scene rendering
    glEnable (GL_SCISSOR_TEST);
    glScissor (int(get_position().x)+1, int(height-get_position().y-get_size().y)+1,
               int(get_size().x)-2, int(get_size().y)-2);

    // Set viewport
    glViewport (int(get_position().x)+1, int(height-get_position().y-get_size().y)+1,
                int(get_size().x)-2, int(get_size().y)-2);


    glClearColor (1,0,0,1);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glMatrixMode (GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity ();
    float aspect = 1.0f;
    aspect = get_size().x / get_size().y;
    float aperture = 25.0f;
    float near = 1.0f;
    float far = 100.0f;
    float top = tan(aperture*3.14159/360.0) * near;
    float bottom = -top;
    float left = aspect * bottom;
    float right = aspect * top;
    glFrustum (left, right, bottom, top, near, far);

    glMatrixMode (GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity ();
    glTranslatef (0.0, 0, -8.0f);
    glRotatef (25,0,1,0);
    glRotatef (5,0,0,1);
    object.render();

    glMatrixMode (GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode (GL_PROJECTION);
    glPopMatrix();

    glDisable (GL_SCISSOR_TEST);
    render_finish();
    glPopAttrib ();
}
