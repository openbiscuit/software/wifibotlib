/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __TERMINAL_H__
#define __TERMINAL_H__
#include "widget.h"
#include "font_12.h"


// A structure to hold a line and the associated display list
struct Line {
    std::string  text;
    unsigned int list;
    Line (std::string line) {
        text = line;
        list = 0;
    }
    ~Line () {
        if (list > 0)
            glDeleteLists (list, 1);
        list = 0;
    }
};


class Terminal : public Widget {
public:
    // Base
    Terminal (void);
    virtual ~Terminal (void);

    // Rendering
    virtual void render (void);

    // User interaction
    virtual bool key_press (char key, int modifier = 0);

    // I/O
    virtual void print (std::string text);
    virtual void connect (void (*func)(Terminal *,std::string));

    // Prompt
    virtual void set_prompt (std::string prompt);
    virtual std::string get_prompt (void);
    virtual std::string prompt (void);

    // Font
    virtual unsigned int get_font_base (void);
    virtual unsigned int font_base (void);
    virtual unsigned int get_font_texture (void);
    virtual unsigned int font_texture (void);

private:
    // Build font 
    virtual void makefont (void);

    // Render text with ascci sequences
    virtual void render (std::string text, bool prompt=false);

    // Render text without ascii sequences
    virtual void raw_render (std::string text);

protected:
    std::vector<Line *>  buffer_;           // Actual terminal line buffer
    std::string          prompt_;           // Terminal prompt
    std::string          raw_prompt_;       // Terminal raw prompt
    std::string          input_;            // Current line input
    std::string          kill_buffer_;      // Kill buffer
    std::vector<std::string> history_;          // Terminal history
    unsigned int         history_index_;    // Current history index
    unsigned int         history_size_;     // History maximum size
    unsigned int         cursor_;           // Cursor position
    unsigned int         font_base_;        // Font display list base
    unsigned int         font_texture_;     // Font texture id
    Size                 glyph_size_;       // Font glyph size
    void (*event_handler_)(Terminal *,std::string);    // Event handler if connected
};

#endif
