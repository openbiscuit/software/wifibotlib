/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __TEXT_H__
#define __TEXT_H__
#include "widget.h"

/**
 * Text rendering widget.
 *
 * Text widget is used to display some formated text.
 *
 * \image html text.png
 */
class Text : public Widget {

    /**
     * Internal line structure (to hold font size and justification)
     */
    struct Line {
        std::string text;
        int justification;
        int size;
        Line (std::string t, int s=12, int j=-1) {
            text = t;
            justification = j;
            size = s;
        }
    };

public:
    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor
     */
    Text (void);

    /**
     * Destructor
     */
    virtual ~Text (void);
    //@}

    
    /**
     * @name Text
     */
    /**
     * Append a line
     *
     * @param text Text to be added
     * @param size Font size (12, 16, 24 or 32 only)
     * @param justification -1 for left, 0 for center, +1 for right
     */
    void append (std::string text, int size=12, int justification=-1);

    /**
     * Remove all lines
     */
    void clear();
    //@}


    /**
     * @name Rendering
     */
    /**
     * Render
     */
    virtual void render (void);
    //@}


    /**
     * @name Size
     */
    /**
     * Set size in window space.
     *
     * It is possible to use relative size using coordinates between 0 and 1.
     * Negative sizes relates to the complement of the related dimension. For
     * example, if x is -10, then x size will be window_width-10. If x is -.25,
     * then x size will be (1-.25)*window_width.
     *
     * @param size size
     */
    virtual void set_size (Size size);

    /**
     * Set size in window space.
     *
     * It is possible to use relative size using coordinates between 0 and 1.
     * Negative sizes relates to the complement of the related dimension. For
     * example, if x is -10, then x size will be window_width-10. If x is -.25,
     * then x size will be (1-.25)*window_width. 
     *
     * @param x position along x axis
     * @param y position along y axis
     * @param z -1 for back, +1 for front
     */
    virtual void set_size (float x=1, float y=1, float z=1);
    //@}

protected:

    /**
     * Test lines.
     */
    std::vector<Line> lines_;

    /**
     * Whether to autosize or not
     */
    bool autosize_;
};

#endif
