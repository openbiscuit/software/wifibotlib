/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frame.h"

// ============================================================================
Frame::Frame (void) : Object()
{
    x_axis_.set_label ("x axis");
    x_axis_.set_position (-.5);
    x_axis_.set_size (1);
    x_axis_.set_range (Range (40 , 240, 4, 20));

    y_axis_.set_label ("y axis");
    y_axis_.set_position (.5);
    y_axis_.set_size (-1);
    y_axis_.set_range (Range (-3, 3, 4, 20));

    z_axis_.set_label ("z axis");
    z_axis_.set_position (.5);
    z_axis_.set_size (-2);
    z_axis_.set_range (Range (1, -3, 4, 20));

    set_size      (1, 1);
    set_position  (0,0);
    set_fg_color  (0,0,0,1);
    set_bg_color  (1,1,1,.8);
    set_br_color  (0,0,0,1);
}

// ============================================================================
Frame::~Frame (void)
{}

// ============================================================================
int
Frame::compare (const void *a, const void *b)
{
    const face *da = (const face *) a;
    const face *db = (const face *) b;
    return ((*da).z > (*db).z) - ((*da).z < (*db).z);
}

// ============================================================================
void
Frame::render_plane (float x, float y, int nx, int ny, int outline)
{
  
  // render face as a GL_QUADS
  glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
  glColor4f (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a*alpha_);
  glPolygonOffset (1, 1);
  glEnable (GL_POLYGON_OFFSET_FILL);
  glBegin (GL_QUADS);
  glVertex3f (0,  0, 0);
  glVertex3f (y,  0, 0);
  glVertex3f (y, x, 0);
  glVertex3f (0, x, 0);
  glEnd();
  
  
  
  glLineWidth (0.5f);
  glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
  glDisable (GL_POLYGON_OFFSET_FILL);
  glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
  glEnable (GL_LINE_SMOOTH);
  if (outline == 1) { // border along all sides
    glBegin (GL_QUADS);
    glVertex3f (0,  0, 0);
    glVertex3f (y,  0, 0);
    glVertex3f (y, x, 0);
    glVertex3f (0, x, 0);
    glEnd();    
  } else if (outline == 2) { // border on 1st and 3rd side
    glBegin (GL_LINES);
    glVertex3f (0,  0, 0);
    glVertex3f (y,  0, 0);
    glVertex3f (y, x, 0);
    glVertex3f (0, x, 0);
    glEnd();    
  }

  // Grid lines
  glEnable (GL_LINE_STIPPLE);
  glLineStipple (1, 0xf0f0);
  for(float i=0;i<=x;i+=.25)
    {
      glBegin (GL_LINES);
      glVertex2f (0, i);
      glVertex2f (y, i); 
      glEnd();
    }
  for(float i=0;i<=y;i+=.25)
    {
      glBegin (GL_LINES);
      glVertex2f (i, 0);
      glVertex2f (i, x); 
      glEnd();
    }
  glDisable (GL_LINE_STIPPLE);
}

// ============================================================================
void
Frame::render (void)
{

  Object::compute_visibility ();
  if (!get_visible())  return;
  
  glPushAttrib (GL_ENABLE_BIT);
  glDisable (GL_LIGHTING);
  glEnable (GL_BLEND);
  glDisable (GL_TEXTURE_2D);
  
  float m[16];
  // 3 faces du rep�re, 0:Oxz, 1:Oxy, 2:Oyz
  face faces[3] = {
        {0, {-.5,-.5,-.5,  .5,-.5,-.5,  .5,-.5, .5, -.5,-.5, .5}, {0,-1,0}, 0},
        {1, {-.5,-.5,-.5, -.5, .5,-.5,  .5, .5,-.5,  .5,-.5,-.5}, {0,0,-1}, 0},
        {2, {-.5,-.5,-.5, -.5,-.5, .5, -.5, .5, .5, -.5, .5,-.5}, {-1,0,0}, 0},
    };

  // trie les faces, 
  glGetFloatv (GL_MODELVIEW_MATRIX, m);
  for (int i=0; i<3; i++)
    faces[i].z = faces[i].normal[0]*m[2] + faces[i].normal[1]*m[6]
      + faces[i].normal[2]*m[10] + m[14]; 
  
  qsort (faces, 3, sizeof (face), compare);
  
    Range range_x = x_axis_.get_range ();
 //   printf("%f %f\n", x_axis_.get_size());
    for (int i=0; i<3; i++) {
        // YZ plane
        if (faces[i].index == 0) {
            glPushMatrix();
	    //glRotatef (90, 1, 0, 0);
            glTranslatef (x_axis_.get_position().x , 0, y_axis_.get_position().x );
            render_plane (z_axis_.get_size().x *-1, 
			  x_axis_.get_size().x, 4, 4, 2);
            glPopMatrix();
            glPushMatrix();
            glRotatef (90, 0,0,1);
            glRotatef (90, 0,1,0);
	    
            glTranslatef (  z_axis_.get_position().x *-1,
			    x_axis_.get_position().x , 
			    y_axis_.get_position().x *-1);
            z_axis_.render();
            glPopMatrix();
        // XY plane
        } else if (faces[i].index == 1) {
            glPushMatrix();
            glTranslatef (0.0f, 0.0f, 0.0f);
//            render_plane (1,1,4,4,0);
            glTranslatef (0.0f, y_axis_.get_position().x , 0.0f);
            x_axis_.render();
            glRotatef (90, 0,0,1);
            glTranslatef (y_axis_.get_position().x *-1, 
			  x_axis_.get_position().x , 0.0f);
           y_axis_.render();
            glPopMatrix ();

        // XZ plane
        } else if (faces[i].index == 2) {
            glPushMatrix();
            glRotatef (90, 1, 0, 0);
	    glRotatef (90, 0, 1, 0);

            glTranslatef (y_axis_.get_position().x *-1,
			  0,
			  x_axis_.get_position().x );

            render_plane (z_axis_.get_size().x *-1, 
			  y_axis_.get_size().x *-1, 4, 4, 2);

            glPopMatrix();
        }
    }
    glPopAttrib ();
}

// ============================================================================
void
Frame::hide (float timeout)
{
    Object::hide (timeout);
    x_axis_.hide (timeout);
    y_axis_.hide (timeout);
    z_axis_.hide (timeout);
}
void
Frame::show (float timeout)
{
    Object::show (timeout);
    x_axis_.show (timeout);
    y_axis_.show (timeout);
    z_axis_.show (timeout);
}

// ============================================================================
void
Frame::set_x_range (Range range)
{
    x_axis_.set_range (range);
}

void
Frame::set_y_range (Range range)
{
    y_axis_.set_range (range);
}

void
Frame::set_z_range (Range range)
{
    z_axis_.set_range (range);
}

void Frame::set_x_label(std::string label)
{
   x_axis_.set_label (label);
}
 

void Frame::set_y_label(std::string label)
{
   y_axis_.set_label (label);
}
 

void Frame::set_z_label(std::string label)
{
   z_axis_.set_label (label);
}
 

void Frame::set_x_position(double pos)
{
  x_axis_.set_position (pos);
}
 

void Frame::set_y_position(double pos)
{
  y_axis_.set_position (pos);
}
 

void Frame::set_z_position(double pos)
{
  z_axis_.set_position (pos);
}
 
void Frame::set_x_size(int size)
{
  x_axis_.set_size (size);
}
  
void Frame::set_y_size(int size)
{
  y_axis_.set_size (size);
}

 
void Frame::set_z_size(int size)
{
  z_axis_.set_size (size);
}

void Frame::set_frame_size(float x, float y, float z)
{
  set_size(x, y, z);
}

void Frame::update(void)
{
}
