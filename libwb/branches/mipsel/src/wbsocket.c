/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/*
 *	Gere la connection entre la cam et le robot.
 *	La socket est utilis�e pour r�cup�rer l'image
 *	dans wbimage.c et pour piloter la cam�ra
 *	dans wbcamera.c
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>	/* pour jouer avec les sockets */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "libwb/wb.h"

/*
 *	Initialise la socket entre la cam�ra et le robot.
 *	Renvoie l'identificateur associ� � la socket cr��e
 */


int initialiseSocket()
{
	int sk, err;
	struct sockaddr_in server_adress;

	/* on remplit la structure d'adresse de la socket */
	memset(&server_adress, 0, sizeof(struct sockaddr_in));
	server_adress.sin_family = AF_INET;
	server_adress.sin_port = htons(PORT);
	server_adress.sin_addr.s_addr = inet_addr(ADR);

	/* on cree la socket */
	sk = socket(AF_INET, SOCK_STREAM, 0);

	/* on se connecte dessus */
	err = connect(sk, (struct sockaddr*)&server_adress, sizeof(struct sockaddr_in));
	if(err != 0)	fprintf(stderr,"Erreur de connection � la socket\n");

	/* renvoie l'identificateur de la socket ouverte */
	return sk;
}
