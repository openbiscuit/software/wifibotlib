//fichier libemb.h

//Auteur : Jason PAUL
//ENSEM ISA SERTR PFE 2006

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

static char filename3[20];
static int i2cbus=0,file;

static unsigned char bufset[10];
static unsigned char bufget[3];
static unsigned char bufad[1];

static void initI2c();
static void setI2cL();
static void getI2cL();
static void setI2cR();
static void getI2cR();
static void getAD();
static void stop();

static unsigned char commande[2];
static unsigned char mesure[7];
