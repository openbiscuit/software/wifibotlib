/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/


/**
 * Evitement d'obstacles grace aux telemetres
 * Sur le Wifibot.
 */

#include "libwb/wb.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

#define V_MAX 20
#define V_MIN 0
#define V_TURN 40
#define DIST 80

/* verbose mode */
int verb = 0;


/*
 * Generateur de nombres aleatoires
 */
int rndm(int n)
{
	static int prem;
	time_t date;
	if(prem) {srand(time(&date)); prem = 0;}
	return rand()/(RAND_MAX + 1)*(n+1);
}


/* 
 * Pour faire un stop() lors d'un CTRL-C
 */
void handler(int sig)
{
	char buf[255];
	stop();
	stop();
	stop();
	printf("*** PAUSE ***\n");
	printf("exit pour quitter\n");
	scanf("%s", buf);
	if(!strcmp(buf, "exit")) exit(0);
}

/**
 * decode parameters.
 * detec --verbose --help
 */
void checkParam( int argc, char *argv[])
{
  int i;
  for( i=1; i< argc; i++ ) {
    if( strcmp(argv[i], "--verb") == 0 ) {
      verb = 1;
      printf("mode verbeux\n");
    }
    else if( strcmp(argv[i], "--help") == 0 ) {
      printf( "usage: %s [--verb] [--help]\n", argv[0]);
      exit(0);
    }
    else {
      printf( "%s : unknown argument\n", argv[i]);
      printf( "usage: %s [--verb] [--help]\n", argv[0]);
      exit(0);
    }
  }
}

/**
 * Main.
 */
int main(int argc, char *argv[])
{
	int vg, vd;	/* Vitesse gauche et droite */
	extern WifibotInfo winfo; /* Structure recueillant les valeurs des capteurs */
	struct sigaction nvt, old; /* Pour choper les <Ctrl-C> */
	int sens;
	
	/* check param */
	checkParam( argc, argv );

	/* Remplacer le gestionnaire par le notre */
	nvt.sa_handler = handler;
	sigaction(SIGINT, &nvt, &old);

	/* Initialisations */
	initI2C(NUM_BUS);	/* NUM_BUS est d�fini dans wblib/include/wbI2C.h */
	vd = V_MAX;
	vg = V_MAX;
	/* avancer(V_MAX); */

	/* Boucle principale */
	while(1)
	{
		getI2CInfo(ADR_LEFT);	/* Recupere les infos du robot */
		getI2CInfo(ADR_RIGHT);
		
		if(verb) printf( "Capteur G %d, D %d =>", winfo.irLeft, winfo.irRight);
		// gestion du mur devant
		if(winfo.irLeft > DIST && winfo.irRight > DIST)
		{
			/*
			if(!sens) sens = rndm(1) +1;
			// demi-tour
			if(sens == 1) tournerHoraire(V_TURN);
			else tournerAntiHoraire(V_TURN);
			*/
			tournerHoraire(V_TURN);
			if(verb) printf( "tournerHoraire(%d)\n", V_TURN);
		}
		else
		{
			vd = V_MAX;
			vg = V_MAX;
			if(winfo.irLeft > DIST)
			  {
			    vd = V_MIN; vg=V_MAX;
			    if(verb) printf( "=> vitesse %d %d\n", vg, vd);
			  }
			else if(winfo.irRight > DIST)
			  {
			    vg = V_MIN; vd=V_MAX;
			    if(verb) printf( "=> vitesse %d %d\n", vg, vd);
			  }
			else
			  {
			    sens = 0;
			    /*avancerCourbe(vd, vg);*/
			  }
			if(verb) printf( "avancerCourbe( %d, %d)\n", vd, vg);
			avancerCourbe( vg, vd);
		}
		printf("G %d, D %d, MG %d, MD %d\n", 
		  winfo.irLeft, winfo.irRight, vg, vd);
		usleep(10000);
	}

}
