// fichier controle_deporte.c

//Auteur : Jason PAUL
//ENSEM ISA SERTR PFE 2006
//Programme de commande du Wifibot en controle deporte
//Detection d'obstacle et manoeuvre de contournement
#include    <stdio.h>
#include    <sys/types.h>
#include    <sys/socket.h>
#include    <netinet/in.h>
#include "libtrio/libcom.h"
main()
{
        //Declaration des variables du main
        unsigned char comg, comd;
        int i, distanceG, distanceD;
        int sock_phys, resultat;
        struct sockaddr_in sock_logique;
        int taille_sock_logique=sizeof(sock_logique);
        struct envoi_data donnee_envoi;
        struct recep_data donnee_recep;
      printf("création de la socket physique \n");
      //Creation de la socket phyqsique
      sock_phys=socket(AF_INET,SOCK_STREAM,0);
      //On teste si la socket a ete cree correctement
      if (sock_phys<0)
      {
            printf("Impossible creer la socket physique \n");
            exit(-1);
      }
      sock_logique.sin_family=AF_INET;
      sock_logique.sin_port=htons(PORT_SERVEUR);
      sock_logique.sin_addr.s_addr=inet_addr(NUM_IP_SERVEUR);
      //Connexion au serveur
      printf("connexion au serveur \n");
      resultat=connect(sock_phys, (struct sockaddr *) &sock_logique, taille_sock_logique);
      //On teste si la connexion a reussi
      if (resultat<0)
      {
            printf("Impossible se connecter au serveur \n");
            exit(-1);
      }
      //On initialise les variables correspondant a la distance entre le WiFiBot et un obstacle
      distanceG = 0;
      distanceD = 0;
      //On veut executer 10 fois l'algorithme
      for (i=0;i<10;i++)
      {
		//Tant que la distance avec un obstacle est inferieure a 50 (130 correspond a une collision)
		while((distanceG < 50) && (distanceD < 50))
		{
			//On fixe la marche avant avec une vitesse max du robot
			comg = (char) 255;
			comd = (char) 255;
			//On enregistre la donnee dans le message qu'on va transmettre
			donnee_envoi.buf[0]=(unsigned char)(comg);     //Commande de la partie gauche
			donnee_envoi.buf[1]=(unsigned char)(comd);     //Commande de la partie droite
			//On envoie le message au serveur embarque
			resultat=sendto(sock_phys, (char *) &donnee_envoi, sizeof(donnee_envoi),
					 0, (struct sockaddr *) &sock_logique, taille_sock_logique);
			//On teste si l'envoi c'est deroule correctement
			if (resultat<0)
			{
			printf("Impossible d'envoyer la donnee \n");
			close(sock_phys);
			exit(-1);
			}
			//On affiche le message envoye
			printf("Le client envoie: %s \n", donnee_envoi.buf);
			//On recupere le message correspondant aux mesures des capteurs/detecteurs
			resultat=recvfrom(sock_phys, (char *) &donnee_recep, sizeof(donnee_recep), 
					0, (struct sockaddr *) &sock_logique, &taille_sock_logique);
			//On teste la reception c'est deroule correctement
			if (resultat<0)
			{
			printf("Impossible de recevoir la donnee \n");
			close(sock_phys);
			exit(-1);
			}
			//On affiche le message recu
			int n;
			for (n=0; n<=6; n++) {
			        printf("Le client recoit: %d \n",donnee_recep.buf2[n]);
			}
			/*
			printf("Le client recoit: %d \n",donnee_recep.buf2[0]);
			printf("Le client recoit: %d \n",donnee_recep.buf2[1]);
			printf("Le client recoit: %d \n",donnee_recep.buf2[2]);
			printf("Le client recoit: %d \n",donnee_recep.buf2[3]);
			printf("Le client recoit: %d \n",donnee_recep.buf2[4]);
			printf("Le client recoit: %d \n",donnee_recep.buf2[5]);
			printf("Le client recoit: %d \n",donnee_recep.buf2[6]);
			*/
			//On memorise la distance a laquelle on se trouve d'un potentiel obstacle
			distanceG = (int) donnee_recep.buf2[5];
			distanceD = (int) donnee_recep.buf2[6];
		}
            //On teste si la distance a gauche est superieure a la distance a droite
            if (distanceG > distanceD)
            {
                  //Si oui, on pivote a droite
                  comg = (char) 255;
                  comd = (char) 191;
            }
            else
            {
                  //Si non on pivote a gauche
                  comg = (char) 191;
                  comd = (char) 255;
            }
            do
            {
                  //On enregistre la donnee dans le message qu'on va transmettre
                  donnee_envoi.buf[0]=(unsigned char)(comg);
                  donnee_envoi.buf[1]=(unsigned char)(comd);
                  //On envoie le message au serveur embarque
                  resultat=sendto(sock_phys, (char *) &donnee_envoi,
				sizeof(donnee_envoi), 0, (struct sockaddr *) &sock_logique, taille_sock_logique);
                  //On teste si l'envoi c'est deroule correctement
                  if (resultat<0)
                  {
                  printf("Impossible d'envoyer la donnee \n");
                  close(sock_phys);
                  exit(-1);
                  }
                  //On recupere le message correspondant aux mesures des capteurs/detecteurs
                  resultat=recvfrom(sock_phys, (char *) &donnee_recep,
				sizeof(donnee_recep), 0, (struct sockaddr *) &sock_logique, &taille_sock_logique);
                  //On teste la reception c'est deroule correctement
                  if (resultat<0)
                  {
                  printf("Impossible de recevoir la donnee \n");
                  close(sock_phys);
                  exit(-1);
                  }
                  //On memorise la distance a laquelle on se trouve d'un potentiel obstacle
                  distanceG = (int) donnee_recep.buf2[5];
                  distanceD = (int) donnee_recep.buf2[6];
            //On continue de pivoter jusqu'a ce qu'il n'a plus d'obstacle a une distance de 20
            } while ((distanceG > 20) || (distanceD > 20));
      }
      //On ferme la socket physique
      close(sock_phys);
}
