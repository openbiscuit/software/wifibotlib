/* fichier stop_trio.c


*/
/*
 *      untitled.c
 *
 * 		Author : Thomas Blanchard, TRIO team, LORIA
 * 		Initial author: Jason PAUL
 * 		ENSEM ISA SERTR PFE 2006
 * 
 *      Copyright 2007 Thomas Blanchard <bouktin@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include "libtrio/libemb.h"


void initI2c()
{
	sprintf(filename3,"/dev/i2c/%d",i2cbus);
	if ((file = open(filename3,O_RDWR)) < 0) printf("error I2C open file\n");
}

int main()
{
	initI2C();
	
	bufset[0]=0;
	bufset[1]=0;
	if (ioctl(file,I2C_SLAVE,0x51) < 0) printf("error I2C ioctl slave\n");
	if (write(file,bufset,2) !=2)
	{
		printf("error write i2c \n");
	}
	bufset[0]=0;
	bufset[1]=0;
	if (ioctl(file,I2C_SLAVE,0x52) < 0) printf("error I2C ioctl slave\n");
	if (write(file,bufset,2) !=2)
	{
		printf("error write i2c \n");
	}
}

