// fichier controle_main.c

//Auteur : Jason PAUL
//ENSEM ISA SERTR PFE 2006

#include "libtrio/libemb.h"
int main()
{
      int i;
      //Initialisation des buffers pour les capteurs/detecteurs
      mesure[0]=0;            //Niveau de la batterie
      mesure[1]=0;            //Vitesse roue avant gauche
      mesure[2]=0;            //Vitesse roue arriere gauche
      mesure[3]=0;            //Vitesse roue avant droite
      mesure[4]=0;            //Vitesse roue arriere droite
      mesure[5]=0;            //Detecteur infrarouge gauche
      mesure[6]=0;            //Detecteur infrarouge droite
      bufad[0]=0;
      printf("running....\n");
      //On appelle la fonction qui initialise la communication sur le bus I2C
      initI2c();
      //On veut executer 5 fois l'algorithme
      for(i=0; i<5; i++)
      {
            do
            {
                  //On ordonne la marche avant a vitesse maximale
                  commande[0] = 255;
                  commande[1] = 255;
                  //On appel les fonctions qui envoient les commandes et recuperent les mesures
                  getAD();                      //Mesure niveau de batterie
                  setI2cL();                    //Commande de la partie gauche du robot
                  setI2cR();                    //Commande de la partie droite du robot
                  getI2cL();                    //Mesure de la vitesse des roues gauches et du detecteur infrarouge gauche
                  getI2cR();                    //Mesure de la vitesse des roues droites et du detecteur infrarouge droite
            //Tant que la distance avec un obstacle est inferieure a 50
            }while((mesure[5] < 50) && (mesure[6] < 50));
            //On appelle la fonction qui arrete le robot
            stop();
            //On affiche la distance mesuree par les deux detecteurs infrarouge
            printf("Obstacle detecte \t Left : %d \t Right : %d\n",mesure[5],mesure[6]);
            //On teste si la distance a gauche est superieure a la distance a droite
            if(mesure[5] > mesure[6])
            {
                  //Si oui, on pivote a droite
                  commande[0] = 255;
                  commande[1] = 191;
            }
            else
            {
                  //Si non, on pivote a gauche
                  commande[0] = 191;
                  commande[1] = 255;
            }
            do
            {
                  //On appel les fonctions qui envoient les commandes et recuperent les mesures
                  getAD();                      //Mesure niveau de batterie
                  setI2cL();                    //Commande de la partie gauche du robot
                  getI2cL();                    //Commande de la partie droite du robot
                  setI2cR();                    //Mesure de la vitesse des roues gauches et du detecteur infrarouge gauche
                  getI2cR();                    //Mesure de la vitesse des roues droites et du detecteur infrarouge droite
            //Tant que la distance avec un obstacle est superieure a 20
            }while((mesure[5] > 20) || (mesure[6] > 20));
            //On appelle la fonction qui arrete le robot
            stop();
      }
}
//////////////////////////
//WIFIBOT I2C Functions//
////////////////////////
//La fonction qui gere l'initialisation de la communication
void initI2c()
{
	sprintf(filename3,"/dev/i2c/%d",i2cbus);
	if ((file = open(filename3,O_RDWR)) < 0) printf("error I2C open file\n");
}
//La fonction qui transmet les commandes a la partie gauche du robot
void setI2cL()
{
	bufset[0]=commande[0];
	bufset[1]=commande[0];
	//if (ioctl(file,I2C_SLAVE,0x51) < 0) printf("error I2C ioctl slave\n");
	if (ioctl(file,I2C_SLAVE,0x51) < 0) printf("error I2C ioctl slave\n");
	if (write(file,bufset,2) !=2)
	{
		printf("error write i2c \n");
	}
}
//La fonction qui recupere les mesure de la partie gauche du robot
void getI2cL()
{
	if (read(file,bufget,3) !=3)
	{
		printf("error read i2c\n");
	}
	else {mesure[1]=bufget[0];mesure[2]=bufget[1];mesure[5]=bufget[2];};
}
//La fonction qui transmet les commandes a la partie droite du robot
void setI2cR()
{
		bufset[0]=commande[1];
		bufset[1]=commande[1];
		if (ioctl(file,I2C_SLAVE,0x52) < 0) printf("error I2C ioctl slave\n");
		if (write(file,bufset,2) !=2)
		{
			printf("error write i2c \n");
		}
}
//La fonction qui recupere les mesure de la partie droite du robot
void getI2cR()
{
      if (read(file,bufget,3) !=3)
      {
            printf("error read i2c\n");
      }
      else {mesure[3]=bufget[0];mesure[4]=bufget[1];mesure[6]=bufget[2];};
}
//La fonction qui recupere le niveau de batterie du robot
void getAD()
{
      bufset[0]=0x40;
      bufset[1]=250;
      if (ioctl(file,I2C_SLAVE,0x4e) < 0) printf("error I2C ioctl slave\n");
      if (write(file,bufset,2) !=2)
      {
            printf("error write i2c \n");
      }
      if (read(file,bufad,1) !=1)
      {
            printf("error read i2c\n");
      }
      else {mesure[0]=bufad[0]/2;}
}
//La fonction qui gere l'arret du robot
void stop()
{
      bufset[0]=0;
      bufset[1]=0;
      if (ioctl(file,I2C_SLAVE,0x51) < 0) printf("error I2C ioctl slave\n");
      if (write(file,bufset,2) !=2)
      {
            printf("error write i2c \n");
      }
      bufset[0]=0;
      bufset[1]=0;
      if (ioctl(file,I2C_SLAVE,0x52) < 0) printf("error I2C ioctl slave\n");
      if (write(file,bufset,2) !=2)
      {
            printf("error write i2c \n");
      }
}
