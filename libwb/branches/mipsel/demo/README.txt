/*
 *      README.txt
 * 
 *      Copyright 2007 Thomas Blanchard <bouktin@gmail.com>
 *             TRIO team - Loria
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

* Informations sur l'objet à reconnaître

Définition de la taille de la balle sur l'image au repos : 
Target haut=291 bas=134 gauche=180 droite=350
taille 20430

-> taille objet ~ 20000

hauteur ~150
largeur : ~ 170

-> objet cercle 
diamètre : 160	delta : 20 
aire : 20000	delta : 5000 


