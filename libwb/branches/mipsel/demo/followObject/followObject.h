/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**				2007 Thomas Blanchard
* 			TRIO Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

/*structures*/
//Type pixel, compos� d'une valeur (la teinte), et d'un booleen qui indique s'il a �t� trait� ou pas
typedef struct Tpx{
	unsigned char valeur;
	char traite;
}Tpx;

/*Type objet, pour les objets de la couleur demand�e que l'on a rep�r�s sur l'image, compos� de ses points le plus haut, le plus bas, le plus � gauche et le plus � droite, et du nombre approximatif de pixels qui le compose*/
typedef struct {
	int haut, bas, droite, gauche; //coordonn�es extreme entourant l'objet
	int taille;  //nombre de pixels approximatif composant l'objet
	int x,y; //coordonn�es du centre approximatif de l'objet
}Tobjet;

typedef enum {
	Rouge, Rose, BleuF, BleuC, Vert, Jaune
}Tcouleur;

typedef struct {
	int inf, sup;
}TtableCouleur;

typedef struct {
	int side, incCol, incLig, deltaObjSize, temps, vitesse, sautVoisin;
}TParam;


/*constantes*/
#define NBLIGNES 240		// no more used
#define NBCOLONNES 320		// no more used
#define saut 2
#define sautVoisin 1

/* communication with network camera */
#define ADR "192.168.0.21"
#define PORT 80

/* Hue min and max of objects to be detected
 * pink (TMAX= 255 : 213 -> 229) (TMAX= 360 : 300 -> 340  (changer unsigned char en int pour utiliser 360) cf backups)
 */
#define TMAX 255	// scale max Hue value 
#define HUE_MIN_PINK 212
#define HUE_MAX_PINK 238

/** 
 * used to evaluate distance between Object and camera
 * if Object Size is between SIZE +- DELTA, no movement
 */
int OBJECT_SIZE = 20000;
int OBJECT_SIZE_DELTA = 6000;
/**
 * NB_MIN_OBJ		: Minimium number of object detected for evaluation update
 * MIN_OBJECT_SIZE	: Minimum size for selecting an object
 * INCR_OBJECT_SIZE	: Increment value for increasing/decreasing MIN_OBJECT_SIZE
 * LIMIT_OBJECT_SIZE: Minimum value for MIN_OBJECT_SIZE
 * LAST_TIME_MAX	: max time for "normal" treatment loop time in millisecond
 * LAST_TIME_MIN	: min time for "normal" treatment loop time in millisecond
 */
int NB_MIN_OBJ = 1;
int MIN_OBJECT_SIZE = 500;
int INCR_OBJECT_SIZE = 100;
int LIMIT_OBJECT_SIZE = 50;
int LAST_TIME_MAX = 80;
int LAST_TIME_MIN = 30;

/**
 * AVG_ACQ_TTMT_TIME: average global time for acquisition and treatment
 * AVG_TTMT_TIME 	: average time for treatment (low:250 < < high:280)
 */
int AVG_ACQ_TTMT_TIME = 750;
//int AVG_ACQ_TTMT_TIME_DELTA = 100;
int AVG_TTMT_TIME = 270;

/** 
 * Increment value for analysing loop 
 *  - in function "traitement"
 * 		INC_COL : column increment
 * 		INC_LIG : line increment (french : ligne)
 *  - in function "voisin"
 * 		SAUT_VOISIN : line and column increment
 * 		PREC_MAX	: number of analysed pixel which might not match aimed hue interval
 */
int INC_COL = 1;
int INC_LIG = 1; // WARNING : may cause recognition problems
int SAUT_VOISIN = 1;
int PREC_MAX = 1; // number of pixel which may not be of the good color and accepted finally
int MAX_INC = 5;

int SIDE = 4;	// set side's part which command to move left, right, forward or backward

/* set the Line Incrementation (INC_LIG) of object search */
void setIncLig (int val);
/* set the Column Incrementation (INC_COL) of object search */
void setIncCol (int val);

/* set the Incrementation (SAUT_VOISIN) of neighbour (voisin) search */
void setSautVoisin (int val);

/* set the object size (OBJECT_SIZE) */
void setObjectSize (int val);
/* set the Delta of the object size (OBJECT_SIZE_DELTA) */
void setDeltaObjectSize (int val);
/* set the minimum size to select an object (MIN_OBJECT_SIZE)*/
void setMinObjectSize (int val);
