/*
 *      followObjectServer.c
 *
 *      Copyright 2007 Cédric Bernier, Julien Le Guen,Alain Dutech
 *			Maia Team, LORIA.
 * 						Thomas Blanchard <bouktin@gmail.com> 
 * 				TRIO team, LORIA
 * 
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Programme de demonstration de la libwb.
 * Reconnaissance d'une balle de couleur et suivi par le robot.
 * Serveur pour envoyer les donnees de la cible sur port 16000.
 *  when "save" is send to the server, the current image and info are saved
 *  (debugImage.ppm and debugInfo.txt)
 *  otherwise info about the actual position of the target can be asked
 *  through a socket (see viewer.c).
 * Debug level
 *  0 : no debug
 *  1 : with all msg between server and client
 *  2 : just print info about target position and camera movement, but the
 *      camera does not move.
 *
 * Parametres a considerer:
 *  - char prec : marge d'erreur
 *  - bornes pour la teinte (H) : hueMin, hueMax
 *
 *	/!\ --- LES FLOATS, C'EST MAAAAAL --- /!\
 *	Un calcul en float (tout ptit le calcul) dans rvb2hsl
 *	entraine un facteur x20 dans le temps d'exécution /!\
 */


#include "followObject.h"
#include "libwb/wb.h"

#include <pthread.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <time.h>
#include <sys/time.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

// to allow online parameter change, using variable instead of const
#define INC 1

/*variables globales*/
Tobjet objets[500]; //les objets récupérés après traitement
int nbelt;
Tobjet obj; // will be shared by main thread and fn_thread_com

/* variables de paramétrage*/
char prec=1; // nb de pixels analysés pouvant ne pas etre de la couleur demandée, donc marge d'erreur
int lastTime=0, last_action_time=0, last_algo_time=0;	//permet de calculer le temps entre 2 iterations 
int lastNbelt=0;

int debug;	//verbose level
int reg;	// 0: no regulation (default) 1: regulation
/** 
 * debug_exit : exit mode (default 0)
 * 		0 : no problem
 * 		1 : problem : close sockets and exit
 */ 
int debug_exit;
int AFS=0; // Ask For Save
int autonomy; // autonomy of followObjectServer 1: yes - 0: no (get params from client)

/** 
 * the consign for time use : [-1;1] 
 * 		-1 : speed (no time available)
 * 		0 : nothing (keep the rhythm)
 * 		1 : slow (some time available)
 */
//#define REGUL_SPEED_1 	-1
//#define REGUL_STD 		0	
//#define REGUL_SLOW_1 		1
enum regul {REGUL_SPEED_2, REGUL_SPEED_1, REGUL_STD, REGUL_SLOW_1, REGUL_SLOW_2};
int consign = REGUL_STD;

int taille, width, height;

int hueMin = (int) HUE_MIN_PINK, hueMax = (int) HUE_MAX_PINK;
// unsigned char hueMin = HUE_MIN_PINK, hueMax = HUE_MAX_PINK;

/* macros */
// mesure du temps.
#define START(tv) gettimeofday(&tv, NULL)
#define END(tvi, tvf) (gettimeofday(&tvf, NULL), (tvf.tv_sec - tvi.tv_sec) * 1000 + (tvf.tv_usec - tvi.tv_usec) / 1000 )

#define INIT(tv) (gettimeofday(&tv, NULL), tv.tv_sec * 1000 + tv.tv_usec / 1000)
#define NOW(tv) (gettimeofday(&tv, NULL), tv.tv_sec * 1000 + tv.tv_usec / 1000)

// variables thread and socket
#define NB_THREADS	2
void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);

static int connected=0;
pthread_t thread [NB_THREADS];
int dog=0;
pthread_mutex_t	mutex_obj = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;

#define MAXPENDING 5    /* Maximum outstanding connection requests */
static int servSock;                    /* Socket descriptor for server */
static int clntSock;                    /* Socket descriptor for client */
static struct sockaddr_in echoServAddr; /* Local address */
static struct sockaddr_in echoClntAddr; /* Client address */
static unsigned short echoServPort=16000;     /* Server port */
static unsigned int clntLen;            /* Length of client address data structure */

static int recvMsgSize=0; 
static unsigned char buffso_rcv[32];

// function declaration
ssize_t sendMsg(int socket, char *msg);
ssize_t sendData(int socket, Tobjet *data);

void printTarget( Tobjet *target );

void dieWithErrorMsg( char *msg);
void dieWithErrorNb(int ret);


/*
 * Calculate the Hue of a pixel from its rgb values 
 * see function rvb2hsl
 */
int pix_rvb2hsl(unsigned char r, unsigned char v, unsigned char b)
{
	
	//pour le cacul de la teinte
	int T0, delta; // delta=max(r,v,b)-min(r,v,b)
	unsigned char cst=TMAX; // pour la formule du calcul de la teinte
	
	if( r == v && v == b ) {
		// cas special
		T0 = 0;
	}
	else if(r >= v && r >= b) {
		delta = (v > b)? r-b : r-v;
		T0 = cst*(v-b)/ (6*delta);
	} else if(v >= b && v >= r) {
		delta = (b > r)? v-r : v-b;
		T0 = cst*((b-r)+2*delta)/ (6*delta);
	} else {
		delta = (r > v)? b-v : b-r;
		T0 = cst*((r-v)+4*delta)/ (6*delta);
	}
	// modulo TMAX
	if (T0<0) T0+=cst;
	else if (T0>cst) T0-=cst;
	
	//if (debug==1) printf("r=%d,v=%d,b=%d,hue=%d",r,v,b,T0);
	return T0;
}


/*
 * Transforme une image RVB stockée dans un tableau 3D en coordonnées HSL (teinte seulement)
 * H : Hue (coloration, teinte)
 * S : Saturation
 * L : Luminance
 *
 * L'algo de transformation a été trouvé sur wikipedia
 * http://fr.wikipedia.org/wiki/Codage_informatique_des_couleurs#Formules_de_changement_de_système_de_codage
 *
 * image : image RVB d'entree
 * teinte : buffer contenant les infos sur la teinte
 */
void rvb2hsl(unsigned char *image, Tpx teinte[480][640], int width, int height)
{
	//pour le cacul de la teinte
	int T0; // , delta delta=max(r,v,b)-min(r,v,b)
	//unsigned char cst=TMAX; // pour la formule du calcul de la teinte
	
	unsigned char r, v, b; // couleurs
	int i=0, j; //parcours du tableau hsl
	int temp=0; //parcours du tableau image

	//on traite l'image pixel par pixel
	while(i<height){
		j=0;
		//if(i%10 == 0) printf("ligne %d\n", i);
		while(j<width){
			//recuperation des composantes rvb
			
			r=image[temp];
			v=image[temp+1];
			b=image[temp+2];
			temp += 3*INC;
			
			T0 = pix_rvb2hsl(r, v, b);
			
			//remplir le tableau hsl avec seulement la composante h
			teinte[i][j].valeur=T0;
			teinte[i][j].traite=0;
			
			// on avance dans la ligne
			j+=INC;  
		} // while(width)
		// on change de ligne
		i+=INC; 
	}// while(height)
}// rvb2hsl()

/** 
 * name: saveImage
 * @param
 * @return
*/
int saveImage (unsigned char *image)
{
	//save Image
	printf( "sauvegarde de l'image\n" );
	//AFS = 0;
	return ecrireImage( "debugImage.ppm", image, taille, width, height);
}

void saveInfo (char * s)
{
	FILE *fp;
	printf( "write info to file\n");
	
	fp = fopen( "debugInfo.txt", "w");
	if( fp == NULL ) {
		dieWithErrorNb( errno );
	}
	fprintf( fp, s);
	fclose( fp );
}

/** 
 * name: evalError
 * 		print type of I2C error
 * 
 * @param int error, char *msg
 * @return
*/
void evalError(int error, char *msg)
{
	char str[30];
	switch (error)
	{
		case -1 :
		case I2C_OK : 
			sprintf(str, "I2C_OK");
			//str="I2C_OK";
			break;
		case I2C_OPEN_ERROR	: 
			sprintf(str, "I2C_OPEN_ERROR");
			//str="I2C_OPEN_ERROR";
			break;
		case I2C_IO_ERROR : 
			sprintf(str, "I2C_IO_ERROR");
			//str="I2C_IO_ERROR";
			break;
		case I2C_WRITE_ERROR : 
			sprintf(str, "I2C_WRITE_ERROR");
			//str="I2C_WRITE_ERROR";
			break;
		case I2C_READ_ERROR : 
			sprintf(str, "I2C_READ_ERROR");
			//str="I2C_READ_ERROR";
			break;
		case I2C_ADR_ERROR : 
			sprintf(str, "I2C_ADR_ERROR");
			//str="I2C_ADR_ERROR";
			break;
		default: 
			sprintf(str, "unknown I2C status error");
			//str="unknown I2C status error";
			break;
	}
	printf("%s - %s (%d)\n", msg, str,error);
}

/** 
 * name: thread_param
 * @param
 * @return
* /
void * thread_param (void * num) 
{
	int numero = (int) num;
	if (debug == 3) printf("num thread param = %d", numero);
	
	TParam tempParam;	// TParam { int side, incCol, incLig, deltaObjSize, temps, vitesse, sautVoisin; }
	
	int locTime = lastTime;
	
	// evaluation of needed time or get param from client
	if (autonomy == 0)
	{
		// get parameters from client
	}
	else 
	{
		// calculate time estimation
		// tempParam.
	}
	
	
	// modification of parameters
	//int val=0;
	//setIncLig (val);	// set the Line Incrementation (INC_LIG) of object search 
	//setIncCol (val);	// set the Column Incrementation (INC_COL) of object search 
	//setSautVoisin (val);	// set the Incrementation (SAUT_LIN) of neighbour (voisin) search 
	//setObjectSize (val);	// set the object size (OBJECT_SIZE) 
	//setDeltaObjectSize (val);	// set the Delta of the object size (OBJECT_SIZE) 
	//setMinObjectSize (val); // set the minimum size to select an object (MIN_OBJECT_SIZE)
	
	
	pthread_exit (NULL);
}
*/

/*
 *  Here follow the setters
 */

/** 
 * name: setIncLIg
 * 		set the Line Incrementation (INC_LIG) of object search
 * @param 
 * 		int val : value to set
 * @return
*/
void setIncLig (int val) 
{
	if (val<=0) val=1;
	if (val>MAX_INC) val=MAX_INC;
	
	INC_LIG = val;
}

/** 
 * name: setIncCol
 * 			set the Column Incrementation (INC_COL) of object search
 * @param  
 * 		int val : value to set
 * 
 * @return
*/
void setIncCol (int val)
{
	if (val<=0) val=1;
	else if (val>MAX_INC) val=MAX_INC;
	
	INC_COL = val;
}

/** 
 * name: setSautCol
 * 			set the Incrementation (SAUT_VOISIN) of neighbour (voisin) search
 * @param
 * 		int val
 * @return
*/
void setSautVoisin (int val)
{
	if (val<=0) val=1;
	else if (val>MAX_INC) val=MAX_INC;
	
	SAUT_VOISIN = val;
}

/** 
 * name: setDeltaObjectSize
 * 			set the Delta of the object size (OBJECT_SIZE_DELTA)
 * @param int val : value to set
 * @return
*/
void setDeltaObjectSize (int val)
{
	OBJECT_SIZE_DELTA = val;
}

/** 
 * name: setObjectSize
 * 			set the object size (OBJECT_SIZE)
 * @param int val : value to set
 * @return
*/
void setObjectSize (int val)
{
	OBJECT_SIZE = val;
}

/** 
 * name: setMinObjectSize
 * 			set the minimum size to select an object (MIN_OBJECT_SIZE)
 * @param int val : value to set
 * @return
*/
void setMinObjectSize (int val)
{
	if ( val < 0 ) val = 0;
	MIN_OBJECT_SIZE = val;
}

/** 
 * name: setSide
 * 			set side's part which command to move left, right, forward or backward
 * @param int val : value to set
 * @return
*/
void setSide (int val)
{
	SIDE = val;
}

/** 
 * name: elapsedTime
 * @param tvi : initial time
 * @param tvf : final time
 * @return
*/
int elapsedTime(struct timeval * tvi, struct timeval * tvf) 
{
	return ((tvf->tv_sec - tvi->tv_sec) * 1000 + (tvf->tv_usec - tvi->tv_usec) / 1000 );
}

/** 
 * name: regulMain
 * @param ltim 	: last iteration's mesure of traitementImage's time
 * @param lnb 	: last iteration's nbelt found
 * @return
*/
void regulMain (int ltim, int lnb)
{
	//
	
	printf("regulMain : ");
	
	if (lnb > NB_MIN_OBJ)
	{
		// augmenter la taille min des objets
		printf("INCR SIZE");
		//MIN_OBJECT_SIZE += INCR_OBJECT_SIZE;
		MIN_OBJECT_SIZE *= 2;
		
	}
	else if (lnb == 0)
	{
		printf("DECR SIZE");
		//MIN_OBJECT_SIZE -= INCR_OBJECT_SIZE;
		//if (MIN_OBJECT_SIZE < 0) MIN_OBJECT_SIZE = 0;
		MIN_OBJECT_SIZE /= 2;
		if (MIN_OBJECT_SIZE < 1) MIN_OBJECT_SIZE = 1;
	}
	else {
		printf("KEEP SIZE");
	}
	printf(" - object size: %d\n",MIN_OBJECT_SIZE);
	
	
	
	printf("regulMain : ");
	if (ltim > LAST_TIME_MAX)
	{
		// order to reduce ttmt time = speed up!
		consign = REGUL_SPEED_1;
		printf("decrease");
	}
	else if (ltim < LAST_TIME_MIN)
	{
		// allow to increase ttmt time = slow down!
		consign = REGUL_SLOW_1;
		printf("increase");
	}
	else 
	{
		// keep the same parameters
		consign = REGUL_STD;
		printf("no change");
	}
	printf(" ltim : %d\n", ltim);
}

/**
 * name: regulAcq
 * 		regulation of Acquisition loop : switch time of acquisition, give plus 
 * 		or minus time to treatment loop, aiming a constant time for global loop 
 * 		without care of decision / action time, which depends on whether it finds 
 * 		an object or not, and take 0, 1 or 2 seconds (1s a move and 2 moves max)
 * @param
 * @return
*/
void regulAcq(int ltim)
{
	int alloc_time = 0; // time to allocate to treatment loop to perform recognition
	
	int DIV_MAX = 10;	// may depend of quality which may depend of 1/INC_LIG*INC_COL and/or whether (nbelt=0)
	int MOY_TTMT = 250;
	int ref_time = AVG_ACQ_TTMT_TIME;
	
	int acq_time = ltim;	// effective acquisition time for this iteration
	
	int tmp_inc_lig = 1;	// temp line increment value
	int tmp_inc_col = 1;	// temp column increment value
	
	if (debug == 1) alloc_time = ref_time - acq_time;
	
	printf("allocated time : %d, acq time : %d\n", alloc_time, acq_time);
	
	int div = 1;
	while (div < DIV_MAX)
	{
		int recup = alloc_time / (MOY_TTMT / div);
		//printf("recup = %d\n", recup);
		if (recup >= 1)
		{
			tmp_inc_col = div/tmp_inc_lig;
			break;
		}
		else if (recup == 0)
		{
			div++;
			if ((div % MAX_INC) == 0)
			{
				tmp_inc_lig++;
			}
			tmp_inc_col = div/tmp_inc_lig;
		}
		else 	// if (recup < 0)
		{
			// no time => hurry up !! INC_* = MAX_INC
			tmp_inc_col=MAX_INC;
			tmp_inc_lig=MAX_INC;
			break;
		}
		
	}
	
	INC_LIG = tmp_inc_lig;
	INC_COL = tmp_inc_col;
	
	printf("div = %d\nINC_LIG = %d\nINC_COL = %d\n", div, INC_LIG, INC_COL);
	
	/*
	if (ltim > (AVG_ACQ_TTMT_TIME + AVG_ACQ_TTMT_TIME_DELTA))
	{
		//INC_COL = ;
		//INC_LIG = ;
		// order to reduce ttmt time = speed up!
		printf("decrease");
	}
	else if (ltim < (AVG_ACQ_TTMT_TIME - AVG_ACQ_TTMT_TIME_DELTA))
	{
		//INC_COL = ;
		//INC_LIG = ;
		// allow to increase ttmt time = slow down!
		printf("increase");
	}
	else 
	{
		//INC_COL = ;
		//INC_LIG = ;
		// keep the same parameters
		printf("no change");
	}
	printf(" allocate to voisin : %d\n", alloc_time);
	*/
}

/**
 * name: regulTtmt
 * @param
 * @return
*/
void regulTtmt ()
{
	//INC_COL
	//INC_LIG
	printf("regulTtmt - consigne : %d\n", consign);
	
	int val_L = INC_LIG, val_C = INC_COL;
	
	switch (consign)
	{
		case REGUL_SLOW_2 : 
			//printf("");
			val_L -= MAX_INC;
			
			if (val_L < 1)
			{
				val_L = 1;
				val_C = 1;
			}
			break;
		case REGUL_SLOW_1 : 
			//printf("");
			val_C--;
			if (val_C <= 0)
			{
				val_L -= MAX_INC;
				val_C = MAX_INC;
			}
			if (val_L < 1)
			{
				val_L = 1;
				val_C = 1;
			}
			break;
		case REGUL_SPEED_1 : 
			//printf("");
			val_C++;
			if (val_C > MAX_INC) 
			{
				val_L += MAX_INC;
				val_C = 1;
			}
			break;
		case REGUL_SPEED_2 : 
			//printf("");
			val_L += MAX_INC;
			break;
		case REGUL_STD : 
		default: 
			//printf("");
			break;
	}
	
	INC_COL = val_C;
	INC_LIG = val_L;
	//setIncCol(val_C);
	//setIncLig(val_L);
	printf("\tINC_LIG = %d \n\tINC_COL = %d\n", INC_LIG, INC_COL);
}

/** 
 * name: regulVoisin
 * @param
 * 		
 * @return
*/
void regulVoisin()
{
	// SAUT_VOISIN : increase / decrease
	//printf("regulVoisin : \n");
}

/*
 * Recherche des voisins : on cherche ligne par ligne, les pixels à gauche puis ceux à droite du pixel courant
 * x et y sont les coordonnées de départ pour la recherche
 * inf et sup sont les bornes de la zone de couleur recherchée
 */
void voisins(unsigned char traite[480][640], int x, int y, int inf, int sup, int width, int height, unsigned char *image)	//pix
{
	int i=x,j;	// pour le parcours du tableau hsl
	int b; // booleen disant s'il faut continuer dans la direction en cours
	int nbpx=0, nb=-1; //nbpx : nb de pixels trouvés dans la zone, nb : nb de pixels trouvés dans la boucle precedente, comme ça, on peut sortitr dès qu'il n'y a plus d'évolution
	int haut, bas, droite, gauche;
	
	int tempindex, indi; //used for the computation of hue (teinte) value
	int tmpi, tmpj;	// to avoid problems of online changing values of SAUT_VOISIN
	int hue;
	
	haut=x;
	gauche=y;
	droite=y;
	
	//int deltaH = 10;	// delta sur teinte de reconnaissance des pixels
	
	//if (debug==1) printf("voisin début ... ");
	
	tempindex = 0;
	indi = i*width;
	tmpi = SAUT_VOISIN; tmpj = SAUT_VOISIN;
	while(i<height && nbpx>nb)
	{
		j=y;
		nb=nbpx;
		/*on regarde les pixels précédents du pixel courant sur la meme ligne*/
		b=1;
		
		while(j>0 && b==1) {
			tempindex = (indi+j)*3;
			// calcul de la teinte du pixel
			hue = pix_rvb2hsl (image[tempindex], image[tempindex+1], image[tempindex+2]);
			
			if(traite[i][j]==0) {
				if(hue>=inf && hue<=sup) //c'est la couleur recherchée
				{
					prec=PREC_MAX;
					nbpx+=tmpj*tmpi;
					//regarder gauche
					if(j<gauche) gauche=j;
				}else{
					if(prec==0)
						b=0;
					else prec--;
				} 
				traite[i][j]=1; //le pixel a été analysé
			}
			tmpj = SAUT_VOISIN;
			j -= tmpj;
		}
		/*on regarde les pixels successeurs du pixel courant sur la meme ligne*/
		j=y;
		b=1;
		
		while(j<width && b==1)
		{
			tempindex = (indi+j)*3;
			// calcul de la teinte du pixel
			hue = pix_rvb2hsl (image[tempindex], image[tempindex+1], image[tempindex+2]);	// pix
			
			if(traite[i][j]==0){
				if(hue>=inf && hue<=sup) //c'est la couleur recherchée	
				{
					prec=PREC_MAX;
					nbpx+=tmpj*tmpi;
					//regarder droite
					if(j>droite) droite=j;
				}else{
					if(prec==0)
						b=0;
					else prec--;
				}
				traite[i][j]=1; //le pixel a été analysé
			} 
			
			tmpj = SAUT_VOISIN;
			j += tmpj;
		}
		
		tmpi = SAUT_VOISIN;
		i += tmpi;
		indi = width*i;
	}
	bas = i-1;

	//if (debug==1) printf("nbpx=%d h=%d hue=%d\n",nbpx, haut, hue);

	/* on regarde si c'est un objet assez grand, dans ce cas on recupere les coordonnées du centre*/ 
	//if(nbpx>tailleObjet) {  // si le nombre de pixels de cet objet est suffisant
	if(nbpx > MIN_OBJECT_SIZE) {  // si le nombre de pixels de cet objet est suffisant
		objets[nbelt].taille=nbpx;
		objets[nbelt].x=(gauche+droite)/2;
		objets[nbelt].y=(haut+bas)/2;
		objets[nbelt].haut = bas;
		objets[nbelt].bas = haut;
		objets[nbelt].gauche = gauche;
		objets[nbelt].droite = droite;
		
		//if (debug==1) 
			//printf("nb pixels=%d\ncentre : x=%d y=%d\nhaut=%d bas=%d gauche=%d droite=%d\n", 
				//nbpx,objets[nbelt].x,objets[nbelt].y,haut,bas,gauche,droite);
		
		nbelt++;
	}
	//if (debug==1) printf("%d elmt trouves - fin voisin\n", nbelt);
	
	if (reg == 1) regulVoisin();
}

/** 
 * Acquisition et Traitement de l'image
 * retourne le num de l'objet sélectionné, c'est-à-dire celui qui est le plus proche
 */
int traitementImage(unsigned char *image, unsigned char traite[480][640], int inf, int sup)
{
	int i=0, j;
	int index;
	/* 100ko l'image compressée c'est déjà pas mal */
	unsigned char buffer[100000];
	nbelt = 0;
	
	// ref time measurement
	struct timeval tvi;	// initial time value
	struct timeval tvf; // final time value
	int tvi_ms = 0, tvf_ms = 0, tv_delta = 0;	// initial and finale time value in millisecond
	
	tvi_ms = INIT(tvi);
	// end ref time measurement
	
	memset(traite, 0, 480*640*sizeof(unsigned char) );	//pix
	
	//tvf_ms = NOW(tvf);
	//tv_delta = tvf_ms - tvi_ms;
	//if (debug==1) printf("time for memset :%d\n", tv_delta);
	
	/*recupération de l'image rvb*/
	//if (debug==1) printf("getImage ... ");
	taille = getImage(buffer);
	
	tvf_ms = NOW(tvf);
	tv_delta = tvf_ms - tvi_ms - tv_delta;
	if (debug==1) printf("time for getImage :%d\n", tv_delta);

	//if (debug==1) printf("decompresseImage\n");
	taille = decompresseImage(image, buffer, taille, &width, &height);
	
	tvf_ms = NOW(tvf);
	tv_delta = tvf_ms - tvi_ms - tv_delta;
	if (debug==1) printf("time for decompressImage :%d\n", tv_delta);
	
	// time calculation
	//gettimeofday(&tvf, NULL);
	//int tim = elapsedTime(&tvi, &tvf);
	tvf_ms = NOW(tvf);
	tv_delta = tvf_ms - tvi_ms;
	if (debug==1) printf("total acquisition : %d \n", tv_delta);
	// end time calculation
	
	// REGULATION 1 : ACQUISITION -> degradation of treatment part
	if (reg == 1) regulAcq(tv_delta);
	
	//if (debug==1) printf("recherche des objets\n");
	
	tvi_ms = INIT(tvi);
	
	int tempindex = 0, tmpi = 0;
	int hue;	
	//int count = 0
	while(i<height){
		j=0;
		
		while(j<width){
			if(traite[i][j]==0)	
			{
				// calcul de la teinte du pixel
				tempindex = (tmpi+j)*3;
				hue = pix_rvb2hsl (image[tempindex], image[tempindex+1], image[tempindex+2]);

				if(hue>=inf && hue<=sup)  // c'est la couleur recherchée		pix
				{
					//count++;
					voisins(traite,i,j,inf,sup, width, height, image);		// pix
				}
			}
			j+=INC_COL;
		}
		i+=INC_LIG;
		tmpi = width*i;
	}
	
	tvf_ms = NOW(tvf);
	last_algo_time = tvf_ms - tvi_ms;
	if (debug==1) printf("time for treatment :%d\n", last_algo_time);
	
	
	//ask for save
	if (AFS == 1) {
	 	saveImage(image);
	 	char s[200];
			
		sprintf( s, "Target haut=%d, bas=%d, gauche=%d, droite=%d\n \
			\tx=%d, y=%d, taille=%d\n", 
			obj.haut, obj.bas, obj.gauche, obj.droite, 
			obj.x, obj.y, obj.taille);
	 	saveInfo(s);
	 	AFS = 0;
	}
	
	//if (debug==1) printf("compte de pixels : %d\n", count);
	
	taille = 0;
	index = 0;
	for(i=0; i<nbelt; i++)
	{
		if(objets[i].taille > taille)
		{
			index = i;
			taille = objets[i].taille;
		}
	}
	// REGULATION 2 : TRAITEMENT
	//if (reg == 1) regulTtmt();
	
	return index;
}

/*
 * Here starts the fun !
 */
int main(int argc, char *argv[])
{
	unsigned char traite[480][640];
	//memset(traite, 0, 480*640*sizeof(unsigned char) );
	
	autonomy = 1; // default : autonomous 
	unsigned char image[480][640][3];
	int index_objet;
	SENS sens, senswb;
	int pan, tilt; //, degree;
	// defined for 480 x 640 image
	// must be re-defined after the size of the image is known.
	// pthaut -------------> x
	//   |
	// y |
	//   |
	//   V               pt bas
	//  
	int xbas, xhaut, ybas, yhaut;
	//int xbas = 210;
	//int xhaut = 430;
	//int ybas = 320;
	//int yhaut = 320;
	// int socket = -1;

	int ret;
	int resi2c=0;
	
	debug_exit = 0;
	debug = 0;
	reg = 0;
	if( argc > 1 ) 
	{
		debug = atoi(argv[1]);
		if (argc > 2)
		{
			reg = atoi(argv[2]);
		}
	} 
	
	printf("%s running with debug=%d and regul=%d\n", argv[0],debug, reg);
	printf("Hue min:%d max:%d\n", hueMin, hueMax);
	
	PanTiltMoveCamera(0,0,Home);	// recaler la caméra au centre

	// Alain : thread[0] est un chien de garde pour fermer le thread de connection
	// si ouvert trop longtemps...
	if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) 
	{
		dieWithErrorNb( ret );
	}
	
	// Alain : thread[1] est le thread de connection
	if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) 
	{
		dieWithErrorNb( ret );
	}
	
	// init I2C to control motors
	evalError(initI2C(NUM_BUS), "init I2C");
	
	while(1)
	{
		/* On récupère la teinte prédominante dans l'image pour ensuite
		* suivre l'objet le plus gros ayant cette teinte */
		
		memset(objets, 0, 500*sizeof(Tobjet) );
		
		if (debug==1) printf("\ndébut nouvelle boucle\n");
		
		struct timeval tvi;	// initial time value
		struct timeval tvf; // final time value
		
		// calcul of execution time for "traitement(...)" function
		//gettimeofday(&tvi, NULL);
		START(tvi);
		
		// recherche d'un objet, index_objet nous donne celui qui a ete sélectionné
		index_objet = traitementImage((unsigned char*)image, traite, hueMin, hueMax);
		
		//gettimeofday(&tvf, NULL);
		//lastTime = elapsedTime(&tvi, &tvf);
		lastTime = END(tvi, tvf);
		
		if (debug==1) printf("fin traitement\nnbelt : %d\ntemps global : %d\n", nbelt, lastTime);
		
		// to ensure mutex
		pthread_mutex_lock (& mutex_obj);
		obj = objets[index_objet];
		pthread_mutex_unlock (& mutex_obj);
		
		// on a l'index de l'objet le plus proche, maintenant faut il suivre
		if (debug==1) printf("Objet %d [%d %d %d %d] (%d,%d)\nnbpix obj : %d\n", index_objet,
			obj.haut, obj.gauche, obj.bas, obj.droite,
			obj.x, obj.y, obj.taille);
		
		//printf("teinte centre : %d\n", teinte[240][320].valeur);
		
		START(tvi);
		/* Ici on a un objet, on peut se caler dessus */
		/* Attention, le "haut" de l'image est vers les x décroissants */
		if(nbelt > 0)
		{
			
			//printf("Objet %d [%d %d %d %d] %d pixels (%d,%d)\n", index_objet,
				//obj.haut, obj.gauche, obj.bas, obj.droite,
				//obj.taille, obj.x, obj.y);
			// re-definition of the side zone
			xhaut = width/SIDE; 
			xbas = width-xhaut; 
			
			//yhaut = height/SIDE;
			//ybas = height-yhaut;
			
			// move wifibot : different cases to analyse
			
			// first : right, left or none?
			senswb = SENS_Unknown;
			if(obj.x < xhaut) senswb = Left;
			else if(obj.x > xbas) senswb = Right;
			else senswb = Home;
			
			/* move the wifibot */
			if(debug == 1) printf("Move wifibot 1 WBU, sens %d ...", senswb);
			resi2c = unitMove(senswb);
			if(debug == 1) evalError(resi2c, "unitMove");
			
			// second : top, bottom or none?
			if (obj.taille > (OBJECT_SIZE + OBJECT_SIZE_DELTA)) senswb = Bottom;
			else if (obj.taille < (OBJECT_SIZE - OBJECT_SIZE_DELTA)) senswb = Top;
			else senswb = Home;
			
			if(debug == 1) printf("Move wifibot 1 WBU, sens %d ...", senswb);
			resi2c = unitMove(senswb);
			if(debug == 1) evalError(resi2c, "unitMove");
			
			//printf("teinte centre : %d\n", teinte[240][320].valeur);
		}	
		
		last_action_time = END(tvi, tvf);
		if (debug==1) printf("temps décision et action: %d\n", last_action_time );

		// if an error occur (i.e. debug_exit = 1), properly exit program
		if( debug_exit == 1 ) 
		{
			saveImage((unsigned char*) image);
			
			char s[200];
			
			sprintf( s, "Target haut=%d, bas=%d, gauche=%d, droite=%d\n \
				\tx=%d, y=%d, taille=%d\n \
				camera xhaut=%d, yhaut=%d, xbas=%d, ybas=%d\n \
				\tsens=%d, pan=%d, tilt=%d\n", 
				obj.haut, obj.bas, obj.gauche, obj.droite, obj.x, obj.y, 
				obj.taille, xhaut, yhaut, xbas, ybas, sens, pan, tilt);
			
			saveInfo(s);
			
			dieWithErrorMsg( "On a eu un 'debug_exit'");
		}
		
		// REGULATION PRINCIPALE
		if (reg == 1) regulMain(last_algo_time, nbelt);
		
		lastNbelt = nbelt;
	}

	pthread_join (thread [0], NULL); //blocking
	pthread_join (thread [1], NULL); //blocking
	
	return 0;
}


/**
 * WatchDog for the Com Thread.
 * Every 3 seconds, variable 'dog' is incremented. If 'dog' reaches 5, it ends
 * everything.
 */
void * fn_thread_dog (void * num)
{
	int numero = (int) num;
	if (debug == 3) printf("num thread dog = %d", numero);
	
	{
		// decrease priority (increase priority value)
		int recup, erreur, set=5;
		errno=0;
		recup = setpriority(PRIO_PROCESS, (int) NULL, set);
		erreur = errno;
		if (erreur != 0)
		{
			char * sterr = strerror(erreur);
			printf("set priority to %d - strerror(errno) : %s (%d)\n", set, sterr, erreur);
		}
	}
	
	while (1) 
	{
		usleep (3 * 100000);
		pthread_mutex_lock (& mutex_dog);
		dog ++;
		pthread_mutex_unlock (& mutex_dog);
		if (dog>5) 
		{
			dog=0;
			if (connected) 
			{
				connected=0;
				int ret=pthread_cancel (thread[1]);
				close(clntSock);
				close(servSock);
				sleep(2);
				if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) 
				{
					dieWithErrorNb( ret );
				}
			}
		}
	}//end while
	pthread_exit (NULL);
}


/**
 * Thread for com. Open a socket, wait for connection, create socket with 
 * client and then listen and answers commands.
 */
void * fn_thread_com (void * num) 
{
	int numero = (int) num;
	if (debug == 3) printf("num thread dog = %d", numero);
	
	{
		// decrease priority (increase priority value)
		int recup, erreur, set=5;
		errno=0;
		recup = setpriority(PRIO_PROCESS, (int) NULL, set);
		erreur = errno;
		if (erreur != 0)
		{
			char * sterr = strerror(erreur);
			printf("set priority to %d - strerror(errno) : %s (%d)\n", set, sterr, erreur);
		}
	}
	
	/* Create socket for incoming connections (ip(7)*/
	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) 
	{
		fprintf( stderr, "socket failed()\n");
		//dieWithErrorMsg("socket failed()");
	}

	/* Construct local address structure */
	memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
	echoServAddr.sin_family = AF_INET;                /* Internet address family */
	echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	echoServAddr.sin_port = htons(echoServPort);      /* Local port */

	/* Bind to the local address */
	int autorisation=1;
	setsockopt(servSock,SOL_SOCKET,SO_REUSEADDR,&autorisation,sizeof(int));

	if (bind(servSock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) 
	{
		fprintf(stderr, "bind()failed\n");
		//dieWithErrorMsg("bind() failed");
	}

	for (;;) /* Run forever */
	{       
		/* Mark the socket so it will listen for incoming connections */
		if (listen(servSock, MAXPENDING) < 0) 
		{
			printf("listen() failed\n");
			//dieWithErrorMsg("listen() failed");
		}

		/* Set the size of the in-out parameter */
		clntLen = sizeof(echoClntAddr);
		/* Wait for a client to connect */
		if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0) 
		{
			fprintf( stderr, "accept() failed\n");
			dieWithErrorMsg("accept() failed");
		}

		/* clntSock is connected to a client! */
		do{
			/* Wait to receive adequate msg ("target")*/
			if ((recvMsgSize = recv(clntSock, buffso_rcv, 5, 0)) < 1) 
			{
				shutdown(clntSock,1);
			}
			else {
				pthread_mutex_lock (& mutex_dog);
				connected=1;	
				dog =0;
				pthread_mutex_unlock (& mutex_dog);
				if( strcmp( (char *)buffso_rcv, "save" ) == 0 ) 
				{
					if( debug == 3 ) 
					{
						printf( "recognised : -%s-\n", buffso_rcv);
					}
					//debug_exit = 1;
					AFS = 1;
				}
				/* Answer the request */
				if( debug == 3 ) {
					printf("server rcvd: [%s]\n",buffso_rcv);
				}
				/* TODO */
				//sendMsg( clntSock, "s->c");
				sendData( clntSock, &obj );
				//printf("server send: [%s]\n",buffMsg);
				if( debug == 3 ) {
					printf("server send:\n");
					printTarget( &obj );
				}
			}
			//memset(buffso_rcv, 0, sizeof(buffso_rcv));
			//buffso_rcv[0]= "\0";
		}
		while(recvMsgSize>0);

		connected=0;
		sleep(1);
	}//end for(;;)
	pthread_exit (NULL);
}//end thread


/**
 * Function used to send DATA over the socket.
 * 
 * @return length of data send
 */
ssize_t sendData(int socket, Tobjet *data)
{
	Tobjet bufTobjet;

	// perform data conversion (compatible high/low endian)

	bufTobjet.haut = htonl(data->haut);
	bufTobjet.bas = htonl(data->bas);
	bufTobjet.gauche = htonl(data->gauche);
	bufTobjet.droite = htonl(data->droite);
	bufTobjet.taille = htonl(data->taille);
	bufTobjet.x = htonl(data->x);
	bufTobjet.y = htonl(data->y);

	return send( socket, (void *) &bufTobjet, sizeof(Tobjet), 0);
}

/**
 * Print info about target Tobjet.
 */
void printTarget( Tobjet *target )
{
	printf( "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", target->haut, target->bas, target->gauche, target->droite);
	printf( "       x=%d, y=%d, taille=%d\n", target->x, target->y, target->taille);
}

/**
 * Die while emiting an error msg.
 */
void dieWithErrorMsg( char *msg)
{
	int i;
	for( i=0; i<NB_THREADS; i++) 
	{
		pthread_cancel (thread[i]);
	}

	close(clntSock);
	close(servSock);
	fprintf (stderr, "Program exit with error: %s\n", msg);
	exit (1);
}

/**
 * Die with an error msg number.
 */
void dieWithErrorNb(int ret)
{
	int i;
	for( i=0; i<NB_THREADS; i++) 
	{
		pthread_cancel (thread[i]);
	}
	close(clntSock);
	close(servSock);
	fprintf (stderr, "Program exits with error: %s\n", strerror (ret));
	exit (1);
}
