/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * A viewer to monitor the behavior of the 'suitBalle' module of the wifibot.
 * This require the use of suitBalleServer.
 *
 * Still in Beta...
 * - load picture from file, not from wifibot-camera.
 * - draw green rectangle around static target, not from server.  
 */

#include <stdlib.h>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_gfxPrimitives.h"
#include "suitBalle.h"

#include <sys/types.h>	/* pour jouer avec les sockets */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <netdb.h>
#include <sys/ioctl.h>

#define NB_THREADS	2
void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);

void dieWithErrorNb(int ret);
void dieWithErrorMsg(char *msg);
void gestionnaire (int numero);

int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole);
void printTarget( Tobjet *target );


pthread_t thread [NB_THREADS];

int dog=0;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;

#define MAXPENDING 5    /* Maximum outstanding connection requests */
int                sockWB, sockCM; // 
struct sockaddr_in    addr; // address Wifibot
int recvMsgSize=0; 
unsigned char buffso_rcv[32]; 
//static unsigned char buffso_send[7];
char sendbuf[10];
char rcvbuf[7],rcvbuftemp[7];

Tobjet myObjet;

int first=1;
int connected=0;

int debug_exit;

/**
 * Get an JPEG image (from file or from the Wifibot (TODO)) 
 * and display it on the screen.
 */

SDL_Surface *screen;
SDL_Surface *image;

#define SDL_GFX_GREEN 0x00FF00FF
#define CROSS_RADIUS 10
#define max(a,b)   ((a) < (b) ? (b) : (a))
#define min(a,b)   ((a) > (b) ? (b) : (a))

#define SIZE	5000
#define SIZEIMG 150000


/**
 * Open a socket to the camera of the Wifibot.
 */
int openSocket(int port, char *ip_address)
{
  struct sockaddr_in server_address;

  /* fill the address struct */
  memset(&server_address, 0, sizeof(struct sockaddr_in));
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(port);
  server_address.sin_addr.s_addr = inet_addr(ip_address);

  /* create IP socket */
  sockCM = socket(AF_INET, SOCK_STREAM, 0);
  if( sockCM < 0 ) {
    fprintf(stderr, "Couldn't get a socket endpoint.\n");
    exit(1);
  }

  /* connect with this socket */
  if ( connect(sockCM, (struct sockaddr *)&server_address, sizeof(struct sockaddr)) != 0 ) {
    fprintf(stderr, "Couldn't connect to the server.\n");
    exit(1);
  }

  printf( "Connection successfull\n");

  return 1;
}

/**
 * Load a given JPG file and display it on the screen.
 */
int loadImage()
{
  //load voleur.jpg into image
  SDL_RWops *rwop;
  rwop=SDL_RWFromFile("voleur.jpg", "rb");
  image=IMG_LoadJPG_RW(rwop);
  if(!image) {
    printf("IMG_LoadJPG_RW: %s\n", IMG_GetError());
  }
  else {
    printf("IMG_LoadJPG_RW: voleur.jpg loaded\n");
    printf("IMG size : %dx%d\n",image->w, image->h);
  }
  
  /* Initialize the best video mode */
  /* Have a preference for 24-bit, but accept any depth */
  screen = SDL_SetVideoMode(image->w, image->h, 8, SDL_SWSURFACE|SDL_ANYFORMAT);
  if ( screen == NULL ) {
    fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
	    SDL_GetError());
    exit(1);
  }
  /*printf("Set 640x480 at %d bits-per-pixel mode\n",
    screen->format->BitsPerPixel);*/

  // Draws the image on the screen:
  /* Blit onto the screen surface */
    if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
      fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
    SDL_UpdateRect(screen, 0, 0, image->w, image->h);

    return 0;
}
/**
 * Display an image pointed at by a SDL_RW.
 */
int displayImage( SDL_RWops *rwop )
{
  image=IMG_LoadJPG_RW(rwop);
  if(!image) {
    printf("display>IMG_LoadJPG_RW: %s\n", IMG_GetError());
    return 1;
  }
  else {
    printf("IMG size : %dx%d\n",image->w, image->h);
    /* Initialize the best video mode */
    /* Have a preference for 24-bit, but accept any depth */
    screen = SDL_SetVideoMode(image->w, image->h, 8, SDL_SWSURFACE|SDL_ANYFORMAT);
    if ( screen == NULL ) {
      fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
	      SDL_GetError());
      exit(1);
    }
    /*printf("Set 640x480 at %d bits-per-pixel mode\n",
      screen->format->BitsPerPixel);*/

    // Draws the image on the screen:
    /* Blit onto the screen surface */
    if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
      fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
    SDL_UpdateRect(screen, 0, 0, image->w, image->h);

    return 0;
  }
}

/**
 * Get an image from the socket.
 * The image is copied into the buffer, its size is the returned bt the function.
 * Warning : allow a buffer large enough 
 */
int getImage(int sk, unsigned char *buffer)
{
  unsigned char buf_rec[SIZE];
  int img_size = 0;
  int retval, i;

  /* On demande le flux JPG */
  send(sk, "GET /IMAGE.JPG HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n", 128, 0);

  /* On commence � r�cup�rer l'image */
  retval = recv(sk, buf_rec, SIZE, 0);
  while(retval != 0)
    {
      /* On copie les donn�es du buffer dans le buffer final */
      memcpy(buffer+img_size, buf_rec, retval);
      img_size += retval;
      retval = recv(sk, buf_rec, SIZE, 0);
    }

  /* Le d�but du buffer est pollu� par les headers HTTP */
  for(i=0; i<img_size-1; i++)
    {
      // une image JPEG commence par 0xFFD8
      if(buffer[i] == 0xFF && buffer[i+1] == 0xD8)
	{
	  img_size -= i;
	  memmove(buffer, buffer+i, img_size);
	  break;
	}
    }
  /* L'image est enti�rement dans buffer, et fait img_size octets */
  return img_size;
}

/**
 * Draw a rectangle around target and a cross at its center.
 */
int drawTarget( Tobjet *target )
{
  Sint16 xmin = max( 0, target->x - CROSS_RADIUS );
  Sint16 xmax = min( image->w, target->x + CROSS_RADIUS );

  Sint16 ymin = max( 0, target->y - CROSS_RADIUS );
  Sint16 ymax = min( image->w, target->y + CROSS_RADIUS );

  hlineColor( screen, xmin, xmax, target->y, SDL_GFX_GREEN);
  vlineColor( screen, target->x, ymin, ymax, SDL_GFX_GREEN);

  rectangleColor( screen, target->gauche, target->haut, 
		  target->droite, target->bas, SDL_GFX_GREEN );
  
  SDL_UpdateRect(screen, 0, 0, image->w, image->h);
  
  return 1;
}



int main(int argc, char *argv[])
{
  SDL_Event event;

  int ret;
  
  int sizeImage;
  unsigned char bufferImg[SIZEIMG];

  /* This is the RWops structure we'll be using */
  SDL_RWops *rw;

   if (lecture_arguments (argc, argv, &addr, "tcp") < 0) {
     dieWithErrorMsg( "Check argument\n");
   }
   addr.sin_family = AF_INET;

  // Alain : thread[0] est un chien de garde pour fermer le thread de connection
  // si ouvert trop longtemps...
  if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
    dieWithErrorNb( ret );
  }
  // Alain : thread[1] est le thread de connection
  if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
    dieWithErrorNb( ret );
  }
  
  if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  }
  
  /* to be called at exit */
  atexit(SDL_Quit);
  
  loadImage();

  // generate new Tobjet
  Tobjet cible;
  cible.haut = 100;
  cible.bas = 50;
  cible.gauche = 200;
  cible.droite = 300;
  cible.x = 225;
  cible.y = 90;
  
  printTarget( &cible );
  drawTarget( &cible );

  
  while(1) {
    SDL_WaitEvent(&event);
    
    switch (event.type) {
    case SDL_QUIT:
      printf("Quitting...\n");
      SDL_FreeSurface ( image );
      exit(0);
    case SDL_KEYDOWN:
      printf("The %s key was pressed!\n",
	     SDL_GetKeyName(event.key.keysym.sym));
      if(event.key.keysym.sym==SDLK_ESCAPE) {
	printf("Quitting...\n");
	SDL_FreeSurface ( image );
	exit(0);
      }
      if(event.key.keysym.sym==SDLK_i) {
	printf( "Open Connection...\n");
	openSocket( 80, "192.168.1.115");
	//openSocket( 80, "192.168.1.115");

	printf( "Get image...\n");
	sizeImage = getImage( sockCM, bufferImg );
	

	printf( "Set SDL RW structure..._n");
	rw = SDL_RWFromMem(bufferImg, sizeImage);

	printTarget( &myObjet );
	// try to infer a SDL_surface from JPEG in Memory
	displayImage( rw );
	drawTarget( &myObjet );
      }
      else if( event.key.keysym.sym==SDLK_s ) {
	printf( "ask for save\n");
	debug_exit = 1;
      }
      break;
      
    }
  }

  pthread_join (thread [0], NULL);//blocking
  pthread_join (thread [1], NULL);//blocking

  return 0;
}

/**
 * Function used to receive DATA over the socket.
 */
ssize_t recvData( int socket, Tobjet *data )
{
  Tobjet bufTobjet;
  int ret;

  ret = recv( socket, (void *) &bufTobjet, sizeof(Tobjet), 0);

  // perform data conversion (compatible high/low endian)

  data->haut = ntohl(bufTobjet.haut);
  data->bas = ntohl(bufTobjet.bas);
  data->gauche = ntohl(bufTobjet.gauche);
  data->droite = ntohl(bufTobjet.droite);
  data->taille = ntohl(bufTobjet.taille);
  data->x = ntohl(bufTobjet.x);
  data->y = ntohl(bufTobjet.y);

  return ret;
}

/**
 * Wait for some time before reseting the client.
 */
void * fn_thread_dog (void * num)
{
  int numero = (int) num;
  while (1) {
    // wait for 0.06s
    usleep (60000);

    /* TODO send message */
    if( debug_exit == 0 ) {
      strcpy( sendbuf, "c->s");
    }
    else {
      strcpy( sendbuf, "save");
    }
    send(sockWB, sendbuf, 5, 0);
    //printf("client send: [%s]\n",sendbuf);

    pthread_mutex_lock (& mutex_dog);
    dog ++;
    pthread_mutex_unlock (& mutex_dog);

    if (dog>100) {//If no reception during 6 second : reset the client
      dog=0;
      connected=0;
      int ret=pthread_cancel (thread[1]);
      close(sockWB);
      sleep(2);
      if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
	dieWithErrorNb( ret );
      }			
    }
  }//end while
  pthread_exit (NULL);
}

/**
 * Thread for com. Open a socket, listen for msg.
 */
void * fn_thread_com (void * num) 
{
  int numero = (int) num;
    
  if ((sockWB = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
    dieWithErrorMsg( "socket() fail");
  }
  if (connect (sockWB, (struct sockaddr *) &addr, sizeof (struct sockaddr)) < 0) {
    dieWithErrorMsg( "connect() fail"); 
  }
  else {
    printf("Connected to the WIFIBOT\n");
    connected=1;        
    do{
      // wait for a message in the socket
      //if ((recvMsgSize = recv(sockWB, rcvbuftemp, 5, 0)) < 1) {
      if( (recvMsgSize = recvData( sockWB, &myObjet )) < 1) {
	printf("rcv error\n");shutdown(sockWB,1);
      }
      else {
	// when message received, 'dog' is set to 0
	pthread_mutex_lock (& mutex_dog);
	connected=1;	
	dog =0;
	pthread_mutex_unlock (& mutex_dog);

	/* TODO : decode message */
        //struct SensorData RobotSensors;
	//RobotSensors.BatVoltage=(int)rcvbuftemp[0];
	//RobotSensors.SpeedFrontLeft=(int)rcvbuftemp[1];
	//RobotSensors.SpeedRearLeft=(int)rcvbuftemp[2];
	//RobotSensors.SpeedFrontRight=(int)rcvbuftemp[3];
	//RobotSensors.SpeedRearRight=(int)rcvbuftemp[4];
	//RobotSensors.IRLeft=(int)rcvbuftemp[5];
	//RobotSensors.IRRight=(int)rcvbuftemp[6];
	//printf("bat %d\n",RobotSensors.BatVoltage);
	//printf("client rcvd: [%s]\n",rcvbuftemp);
	//printf("client rcvd:\n");
	//printTarget( &myObjet );
      }
    }while(recvMsgSize>0);
    connected=0;
  }//end else
  pthread_exit (NULL);
}//end thread

/**
 * Die while emiting an error msg.
 */
void dieWithErrorMsg(char *msg)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }
  close(sockWB);
  close(sockCM);

  fprintf (stderr, "Program exit with error: %s\n", msg);
  exit (1);
}
/**
 * Die with an error msg number.
 */
void dieWithErrorNb(int ret)
{
  int i;
  for( i=0; i<NB_THREADS; i++) {
    pthread_cancel (thread[i]);
  }
  close(sockWB);
  close(sockCM);

  fprintf (stderr, "Program exits with error: %s\n", strerror (ret));
  exit (1);
}
/**
 * Print info about target.
 */
void printTarget( Tobjet *target )
{
  printf( "Target haut=%d, bas=%d, gauche=%d, droite=%d\n", target->haut, target->bas, target->gauche, target->droite);
  printf( "       x=%d, y=%d, taille=%d\n", target->x, target->y, target->taille);
}

/**
 * Interprets the arguments.
 *
 * @return -1 if error
 */
int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole)
{
  char * liste_options = "a:p:h";
  int    option;
	
  char * hote  = "152.81.10.250";
  char * port = "16000";

  struct hostent * hostent;
  struct servent * servent;

  int    numero;

  while ((option = getopt (argc, argv, liste_options)) != -1) {
    switch (option) {
    case 'a' :
      hote  = optarg;
      break;
    case 'p' :
      port = optarg;
      break;
    case 'h' :
      fprintf (stderr, "Syntaxe : %s [-a adresse] [-p port] [-j] j for usb joystick\n",
	       argv [0]);
      return (-1);
    default	: 
      break;
    }
  }
  memset (adresse, 0, sizeof (struct sockaddr_in));
  if (inet_aton (hote, & (adresse -> sin_addr)) == 0) {
    if ((hostent = gethostbyname (hote)) == NULL) {
      fprintf (stderr, "hote %s inconnu \n", hote);
      return (-1);
    }
    adresse -> sin_addr . s_addr =
      ((struct in_addr *) (hostent -> h_addr)) -> s_addr; 
  }
  if (sscanf (port, "%d", & numero) == 1) {
    adresse -> sin_port = htons (numero);
    return (0);
  }
  if ((servent = getservbyname (port, protocole)) == NULL) {
    fprintf (stderr, "Service %s inconnu \n", port);
    return (-1);
  }
  adresse -> sin_port = servent -> s_port;
  return (0);
}
