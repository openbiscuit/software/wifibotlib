/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

//#include "libwb/wb.h"

/*structures*/
//Type pixel, compos� d'une valeur, et d'un booleen qui indique s'il a �t� trait� ou pas
typedef struct Tpx{
	unsigned char valeur;
	char traite;
}Tpx;

/*Type objet, pour les objets de la couleur demand�e que l'on a rep�r�s sur l'image, compos� de ses points le plus haut, le plus bas, le plus � gauche et le plus � droite, et du nombre approximatif de pixels qui le compose*/
typedef struct {
	int haut, bas, droite, gauche; //coordonn�es extreme entourant l'objet
	int taille;  //nombre de pixels approximatif composant l'objet
	int x,y; //coordonn�es du centre approximatif de l'objet
}Tobjet;

typedef enum {
	Rouge, Rose, BleuF, BleuC, Vert, Jaune
}Tcouleur;

typedef struct {
	int inf, sup;
}TtableCouleur;

/*constantes*/
#define TMAX 255
#define NBLIGNES 240
#define NBCOLONNES 320
#define saut 2
#define sautVoisin 1


/*fonctions*/

