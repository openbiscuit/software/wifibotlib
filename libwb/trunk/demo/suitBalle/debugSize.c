/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

#include "suitBalle.h"
#include <stdio.h>
#include <arpa/inet.h>

/**
 * Debug program used for 
 * knowing size of different structure, both on wifibot and other devices.
 */

void toBinary( unsigned char *data, int length)
{
  int i;
  
  for( i=0; i<length; i++ ) {
    printf("[%2d] %#02x\n", i, *(data+i));
  }

}

int main( int argc, char *argv[] )
{
  Tobjet objet;
  int entier = 67;
  uint32_t indEntier;
  short sentier = 23;

  objet.haut = 100;
  objet.bas = 170;
  objet.gauche = 200;
  objet.droite = 400;
  objet.taille = 123456;
  objet.x = 150;
  objet.y = 300;


  printf( "Tobjet = %d bytes\n", sizeof(Tobjet));
  printf( "int    = %d bytes\n", sizeof(int));
  printf( "char   = %d bytes\n", sizeof(char));
  printf( "un char= %d bytes\n", sizeof(unsigned char));
  printf( "short  = %d bytes\n", sizeof(short));

  printf( "short =%d\n", sentier);
  toBinary( (unsigned char*) &sentier, sizeof(short));

  printf( "int =%d\n", entier);
  toBinary( (unsigned char*) &entier, sizeof(int));

  indEntier = htonl( entier );
  printf( "int =%d\n", indEntier);
  toBinary( (unsigned char*) &indEntier, sizeof(uint32_t));

  

  //printf( "objet =%d\n", objet);
  //toBinary( (unsigned char*) &objet, sizeof(Tobjet));
  
  return 0;
}
