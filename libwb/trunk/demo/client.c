/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Cédric Bernier, Julien Le Guen,Alain Dutech
**               2007-2008 Nicolas Beaufort
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * Un client qui permet de téléguider un wifibot avec un joystick ou avec le clavier.
 * Suppose que 'remote.wb' tourne sur le robot.
 *
 * C'est clairement une version prototype, il faudrait le nettoyer un peu
 * et le rendre plus lisible.
 *
 * @todo gérer correctement les include, notamment pour jpeg et wb.h
 * @todo faire autrement que passer par wbWriteImage
 * @todo commentaire pour rappeler l'interface clavier et joystick.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include "SDL.h"
#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"

#define CLCK_PER_MILISEC (CLOCKS_PER_SEC/1000)
#define DEFAULT_PORT 6080
#define DEFAULT_IP "192.168.1.115"
#define STDIN 0
#define SIZEIMG 150000
#define SIZE	150000
#define SDL_VIDEO_FLAGS (SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_ANYFORMAT)
#include "SDL_image.h"
#include "SDL_gfxPrimitives.h"
typedef enum direction {stop,droite0,droite1,droite2,gauche0,gauche1,gauche2,haut0,haut1,haut2,bas0,bas1,bas2} direction ;
typedef enum action {a_stop,a_avance,a_camera,a_recule,a_senshoraire,a_sensantihoraire}action;
typedef enum {
	TopLeft, Top, TopRight, Left, Home, Right, BottomLeft, Bottom, BottomRight, WBSens_Unknown
} WBSens;

	SDL_Surface * screen;
	SDL_Surface *image;
	int thread_exit;
	int sem1,sem2;
	pthread_mutex_t mut;
	int size_IMG;
	unsigned char bufferImg[SIZEIMG];
	unsigned char buffer[SIZEIMG];

	unsigned char img[SIZEIMG];
	direction dir=stop;
	int displayImage( SDL_RWops *rwop )
{
	image=IMG_LoadJPG_RW(rwop);
	if(!image) {
		printf("display>IMG_LoadJPG_RW: %s\n", IMG_GetError());
		return 1;
	}
	else {
	//	printf("IMG size : %dx%d\n",image->w, image->h);
		/* Initialize the best video mode */
		/* Have a preference for 24-bit, but accept any depth */
	//	screen = SDL_SetVideoMode(image->w, image->h, 8, SDL_SWSURFACE|SDL_ANYFORMAT);
		if ( screen == NULL ) {
			fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
				SDL_GetError());
			exit(1);
		}
    /*printf("Set 640x480 at %d bits-per-pixel mode\n",
		screen->format->BitsPerPixel);*/

    // Draws the image on the screen:
		/* Blit onto the screen surface */
		if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
			fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
		SDL_UpdateRect(screen, 0, 0, image->w, image->h);

		SDL_FreeSurface(image);

		return 0;
	}
}


/* Expanded data source object for tab input */
typedef struct {
	struct jpeg_source_mgr pub; /* public fields */
	JOCTET * buffer;	/* Start of buffer */
	int nb_bytes;
	boolean start_of_file;
} tab_source_mgr;

typedef tab_source_mgr * tab_src_ptr;


/*
 * Initialise source --- called by jpeg_read_header
 * before any data is actually read
 */
METHODDEF(void) init_source(j_decompress_ptr cinfo)
{
	tab_src_ptr src = (tab_src_ptr) cinfo->src; 
  	/* We reset the empty-input-file flag for each image,
   	 * but we don't clear the input buffer.
   	 * This is correct behavior for reading a series of images from one source.
   	 */
	src->start_of_file = TRUE;
}




METHODDEF(boolean) fill_input_buffer(j_decompress_ptr cinfo)
{
	tab_src_ptr src = (tab_src_ptr) cinfo->src;
	src->pub.next_input_byte = src->buffer;
	src->pub.bytes_in_buffer = src->nb_bytes;
	src->start_of_file = FALSE;

	return TRUE;
}



METHODDEF(void) skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
	tab_src_ptr src = (tab_src_ptr) cinfo->src;
	if(num_bytes > 0)
	{
		src->pub.next_input_byte += (size_t) num_bytes;
		src->pub.bytes_in_buffer -= (size_t) num_bytes;
	}
}


METHODDEF(void) term_source(j_decompress_ptr cinfo)
{
	/* void */
}


/*
 * Source manager à partir d'un tableau déjà chargé en mémoire.
 * Il suffit d'indiquer l'adresse du tableau, ainsi que sa taille,
 * pour ensuite pouvoir décompresser l'image stockée dans ce tableau.
 * Une fois l'image décompressée on peut travailler dessus.
 */
GLOBAL(void) jpeg_tab_src(j_decompress_ptr cinfo, JOCTET *buffer, int nbytes)
{
	tab_src_ptr src;
	if(cinfo->src == NULL)
	{
		cinfo->src = (struct jpeg_source_mgr *)
			(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
										SIZEOF(tab_source_mgr));
		src = (tab_src_ptr) cinfo->src;
	}

	src = (tab_src_ptr) cinfo->src;
	src->pub.init_source = init_source;
	src->pub.fill_input_buffer = fill_input_buffer;
	src->pub.skip_input_data = skip_input_data;
	src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
	src->pub.term_source = term_source;
	src->nb_bytes = nbytes;
	src->buffer = buffer;
	src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
	src->pub.next_input_byte = NULL; /* until buffer loaded */

}
int wbWriteImage(char *fichier, unsigned char *img, int taille, int width, int height)
{
	FILE *fp;
	int i;

	fp = fopen(fichier, "w");
	if(fp == NULL)
	{
		fprintf(stderr, "Erreur : impossible d'ouvrir le fichier %s\n", fichier);
		return(1);
	}
	// on �crit les entetes ppm (24 bits RVB)
	fprintf(fp, "P6 %d %d 255 ", width, height);
	// on copie le tableau dans le fichier
	for(i=0; i<taille; i++)
	{
		fprintf(fp, "%c", img[i]);
	}
	
	return fclose(fp);
} /* ecrireImage() */



/* 
 * D�compresse une image contenue dans un buffer.
 *  - image		: double pointeur sur un buffer qui contiendra l'image d�compress�e (non allou�)
 *  - buffer	: buffer contenant l'image compress�e
 *  - taille	: taille en octets de l'image compress�e
 * Renvoie la taille de l'image d�compres�e (en octets)
 * Met � jour : width et height.
 */
/** 
 * Decompress an jpeg image pointed at by buffer.
 * 
 * @param image will contain the decompressed image (will be allocated)
 * @param buffer points to the compressed image
 * @param taille of the compressed image
 * @param width of the image (pixels)
 * @param height of the image (pixels)
 * 
 * @return size of the decompressed image (bytes)
 */
int wbDecompressImage(unsigned char *image, unsigned char *buffer, int taille,
		     int *width, int *height)
{
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	unsigned char *ligne;
	
	/* Initialisation de la structure d'info */
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	/* On lui dit "d�compresse ce qu'il y a dans le buffer l�" */
	jpeg_tab_src(&cinfo, buffer, taille);

	/* On r�cup�re les infos de l'image et on d�compresse */
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	*height = cinfo.output_height;
	*width = cinfo.output_width;

	/* Buffer o� l'image d�compress�e sera plac�e */
	//printf("Allocation m�moire image (%d)\n", width*height*3*sizeof(char));
	//*image = malloc(height*width*3*sizeof(unsigned char));
	ligne = image;

	/* Tant qu'il y a des scanlines, on les place dans le buffer */
	while(cinfo.output_scanline < (*height))
	{
		ligne = image + 3 * (*width) * cinfo.output_scanline;
		jpeg_read_scanlines(&cinfo, &ligne, 1);
	}

	/* On termine proprement */
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	return 3* (*height) * (*width);
} /* decompresseImage() */

int send_info(int sk,action ac,int v1,int v2,int sens)
{
	char buffer[10];
	
	sprintf(buffer,"%d\0",ac);
	if(v1<10)
	{
		sprintf(buffer,"%s0%d\0",buffer,v1);
		
	}
	else
	{
		sprintf(buffer,"%s%d\0",buffer,v1);
	}
	if(v2<10)
	{
		sprintf(buffer,"%s0%d\0",buffer,v2);
		
	}
	else
	{
		sprintf(buffer,"%s%d\0",buffer,v2);
	}
	if(sens<10)
	{
		sprintf(buffer,"%s0%d\0",buffer,sens);
		
	}
	else
	{
		sprintf(buffer,"%s%d\0",buffer,sens);
	}
	send(sk,buffer,strlen(buffer),0);
	return 0;
	
}
int Recup_Flux_Mpeg(int sk, unsigned char *bufferIMG)// a n'appelé qu'a l'interieur d'un thread
{
	unsigned char buf_rec[SIZE];
	unsigned char buffer[SIZEIMG];
	int img_size = 0;
	int retval, i,j=0;
	SDL_RWops *rw;
	/* On demande le flux JPG */
	send(sk, "GET /MJPEG.CGI HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n", 128, 0);

	/* On commence à récupérer l'image */
	lbl:
			img_size=0;
	retval = recv(sk, buf_rec, SIZE, 0);
	while(retval != 0 && !thread_exit)
	{
		/* On copie les données du buffer dans le buffer final */
		
		memcpy(buffer+img_size, buf_rec, retval);

		
		img_size += retval;
		for(i=img_size-retval-1;i<img_size-1;i++)
		{
			if(buffer[i] == 0xFF && buffer[i+1] == 0xD9)
			{
		//	printf("end of image detected\n");
			
				for(j=0; j<img_size-1; j++)
				{
      // une image JPEG commence par 0xFFD8
					if(buffer[j] == 0xFF && buffer[j+1] == 0xD8)
					{
						img_size -= j;
						memmove(buffer, buffer+j, img_size);
						break;
					}
				}
			//mutex lock
				pthread_mutex_lock(&mut);
				memcpy(bufferIMG,buffer,img_size);
				size_IMG = img_size;
				pthread_mutex_unlock(&mut);
			//mutex unlock
		//	printf("lala\n");
				goto lbl;
			}
		}
		
		retval = recv(sk, buf_rec, SIZE, 0);
	
		
		
		
		
	}

	return img_size;
}
void *thread_flux_mpeg(void *arg)
{
	int sk;
	struct sockaddr_in addr2;
	sk = socket_connect(80,"192.168.1.115",&addr2);
	Recup_Flux_Mpeg(sk,bufferImg);
	close(sk);
	pthread_exit(NULL);
	
}
int socket_connect(int port,char *adresse,struct sockaddr_in *serv_addr)
{
	int res;
	struct  hostent         *server;
	if (! (server=gethostbyname(adresse)))
	{printf("erreur DNS\n"); return -1;}
  //  printf("on a l'ip....\n");
	serv_addr->sin_addr.s_addr = inet_addr(adresse);
	serv_addr->sin_port = htons(port);
	serv_addr->sin_family = AF_INET;
//printf("on a tout connecté\n");
	if((res=socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{perror("socket"); return -1;}
	if (connect(res, (struct sockaddr*)serv_addr, sizeof(*serv_addr)) < 0)
	{perror("connect"); return -1;}
	return res;
}


int main(int argc,char *argv[])
{
	char c;
	char file[20];
	int port;
	int s;
	int fin=0;
	int sk;
	int sizeImage;
	int taille;
	int width;
	int height;
	SDL_Joystick *joystick;
	clock_t clock1,clock2;
	SDL_RWops *rw;
	
	
	int i=0;

	pthread_t thread[1];
	pthread_attr_t attr;
	
	SDL_Event event;

	clock1 = clock()/CLCK_PER_MILISEC ;
	struct sockaddr_in addr;
	pthread_mutex_init(&mut, NULL);
	if(argc>1)
	{
		port = atoi(argv[1]);

	}
	else
	{
		port = DEFAULT_PORT;
	}

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) == -1)
	{
		fprintf(stderr, "Erreur lors de l'initialisation de SDL: %s\n",
			SDL_GetError());
		return 1;
	}
	// on ouvre le joystick
	joystick =  SDL_JoystickOpen  (0);
	SDL_JoystickUpdate();

	screen = SDL_SetVideoMode(640, 480, 8, SDL_VIDEO_FLAGS);
	s = socket_connect(DEFAULT_PORT,DEFAULT_IP,&addr);
	fcntl(STDIN, F_SETFL, O_NONBLOCK) ;// rend stdin non bloquant
	SDL_EnableUNICODE(1);
	thread_exit=0;
	pthread_create(&thread[0],NULL,thread_flux_mpeg,NULL);
	while(!fin){
		/* Un évènement attend d'être traité? */
		clock2 = clock()/CLCK_PER_MILISEC ;
		if((clock2 - clock1)>=30 )
		{
			//printf("ici\n");
			pthread_mutex_lock(&mut);
			//printf("ici\n");
			rw = SDL_RWFromMem(bufferImg, size_IMG);
		
			displayImage( rw );
			//printf("laaaaaaaaaa\n");
			SDL_FreeRW(rw);
			pthread_mutex_unlock(&mut);
		
		//	usleep(100000);
			clock1=clock()/CLCK_PER_MILISEC ;
		}
		
		while (SDL_PollEvent(&event))
		{
			printf("event : %d\n",event.type);
			switch (event.type)
			{
				case SDL_JOYAXISMOTION:
			printf("event: axe joystick\n");
			printf("event type:%d\n",event.type);
			switch(event.jaxis.axis)
			{
				
				case 0://axe horizontal
					printf("axis : %d\,valeur %d\n",event.jaxis.axis,event.jaxis.value);
				
					if(event.jaxis.value >128)
					{
						// on va a droite
						if(event.jaxis.value < 150)
						{
							//stop
							if(dir!=stop)
							{
							send_info(s,a_stop,0,0,0);
							dir=stop;
							}
						}
						else
						{
							// on avance a droite
							if(dir!=droite0)
							{
							dir = droite0;
							send_info(s,a_senshoraire,20,20,0);
							}
						}
					
					}
					else
					{
					if(event.jaxis.value !=128)
					{
						// on va a gauche
						if(event.jaxis.value > 106)
						{
							// stop
							if(dir!=stop)
							{
								send_info(s,a_stop,0,0,0);
								dir=stop;
							}
							
						}
						else
						{
							// on avance a gauche
							if(dir!=gauche0)
							{
							dir = gauche0;
							send_info(s,a_sensantihoraire,20,20,0);
							}
						}
						
						
					}
					else
					{
						// on se stope
						if(dir!=stop)
						{
							send_info(s,a_stop,0,0,0);
							dir=stop;
						}
					}
					}
					break;
					
				case 1://axe vertical
					if(event.jaxis.value >128)
					{
						// on va en bas
						if(event.jaxis.value < 150)
						{
							//stop
							if(dir!=stop)
							{
								send_info(s,a_stop,0,0,0);
								dir=stop;
							}
						}
						else
						{
							// on avance en ariere
							if(dir!=droite0)
							{
								dir = bas0;
								send_info(s,a_recule,20,20,0);
							}
						}
					
					}
					else
					{
						if(event.jaxis.value !=128)
						{
						// on va en haut
							if(event.jaxis.value > 106)
							{
							// stop
								if(dir!=stop)
								{
								
									send_info(s,a_stop,0,0,0);
									dir=stop;
								}
							
							}
							else
							{
							// on avance en avant
								if(dir!=haut0)
								{
									dir = haut0;
								
									send_info(s,a_avance,20,20,0);
								}
							}
						
						
						}
						else
						{
						// on se stope
							if(dir!=stop)
							{
								send_info(s,a_stop,0,0,0);
								dir=stop;
							}
						}
					}
					break;
				default:
					printf("axis : %d\,valeur %d\n",event.jaxis.axis,event.jaxis.value);
					break;
				
				
			}
					break;

				case 9:
					// gestion du petit stick de controle de la camera
					switch(event.jhat.value)
					{
						case 1:
							//stick vers le haut
							send_info(s,a_camera,0,5,(int) Top);
							break;
						case 2:
							//stick vers la droite
							send_info(s,a_camera,5,0,(int) Right);
							break;
						case 4://stick vers le bas
							send_info(s,a_camera,0,5,(int) Bottom);
							break;
						case 8: //stick vers la gauche
							send_info(s,a_camera,5,0,(int) Left);
							break;
							
						default:
							break;
							
					}
					
					printf("%d %d \n",event.jhat.value);
					break;
				case 10:
					send_info(s,a_camera,0,0,Home);
					break;
				case SDL_KEYDOWN:
			//	printf("Touche %d enfoncée (caractère produit: %c)\n", event.key.keysym.sym, event.key.keysym.unicode);
				
				
					switch (event.key.keysym.sym)
					{
						case SDLK_q:
							send(s,"2050003",7,0);
							break;
						case SDLK_d:
							send(s,"2050005",7,0);
							break;
						case SDLK_s:
							send(s,"2000004",7,0);
							break;
						case SDLK_z:
							send(s,"2000501",7,0);
							break;
						case SDLK_x:
							send(s,"2000507",7,0);
							break;
						case SDLK_LEFT:
							send(s,"5100000",7,0);
							break;
						case SDLK_RIGHT:
							send(s,"4100000",7,0);
							break;
						case SDLK_UP:
							send(s,"1303000",7,0);
							break;
						case SDLK_DOWN:
							send(s,"3303000",7,0);
							break;
						default:
							break;
					}
				
					break;
				
				case SDL_KEYUP:
		//		printf("Touche %d relâchée\n", event.key.keysym.sym);
					switch (event.key.keysym.sym)
					{
						case SDLK_LEFT:
							send(s,"0000000",7,0);
							break;
						case SDLK_RIGHT:
							send(s,"0000000",7,0);
							break;
						case SDLK_UP:
							send(s,"0000000",7,0);	
							break;
						case SDLK_p:
							send(s,"0000000",7,0);
							fin =1;
							break;
					
						case SDLK_DOWN:
							send(s,"0000000",7,0);
							break;
						default:
							break;
					}
					break;
				default:
				
					break;
			}
	
		}
		
	}

	close(s);
	SDL_JoystickClose(joystick);

	thread_exit=1;
	usleep(100000);
	return 0;

}
