/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Cédric Bernier, Julien Le Guen,Alain Dutech
**               2007-2008 Nicolas Beaufort
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * Exemple très simple de serveur tournant sur le robot.
 * Socket qui écoute sur un port (DEFAULT_PORT) et qui appelle
 * les fonctions de base de la libwb.
 * Utilisé pour téléguider le robot.
 *
 * Le message envoyé au serveur doit faire 7 char de long.
 * <li>char[0] : commande<br><ul>
 *   <li> 0 -> wbForceStop()</li>
 *   <li> 1 -> wbForceAvancerCourbe(v1, v2)</li>
 *   <li> 2 -> wbPanTiltMoveCamera(v1, v2, sens)</li>
 *   <li> 3 -> wbForceReculerCourbe(v1, v2)</li>
 *   <li> 4 -> wbTournerHoraire(v1)</li>
 *   <li> 5 -> wbTournerAntiHoraire(v1)</li>
 * </ul></li>
 * <li>char[1] et char[2] : chiffres de v1</li>
 * <li>char[3] et char[4] : chiffres de v2</li>
 * <li>char[5] et char[6] : chiffres de sens</li>
 */


#include "libwb/wb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#define DEFAULT_PORT 6080
#define STDIN 0
int socket_decoute;
int socket_connec;
int socket_ecoute(int port,struct sockaddr_in *local)
{
	int res;
	if((res=socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{perror("erreur socket"); return -1;}
	bzero(local, sizeof(*local));
	local->sin_family = AF_INET;
	local->sin_port = htons(port);
	local->sin_addr.s_addr = htonl(INADDR_ANY);
	bzero(&(local->sin_zero), 8);

	if(bind(res, (struct sockaddr *)local, sizeof(struct sockaddr))== -1)
	{perror("bind"); return -1;}
	printf("bindage réussi\n");
	if(listen(res, 6) == -1)        {perror("listen"); return -1;}
	printf("listen réussi sur le port: %d\n",port);

	return res;
}

void handler(int sig)
{
	char buf[255];
	wbForceStop();
	wbStop();
	wbStop();
	printf("Arrêt du robot\n");
	close(socket_connec);
	usleep(50000);
	close(socket_decoute);

	exit(0);

}



int main(int argc,char *argv)
{
	char c,instr;
	char buffer[10];
	char v1[3],v2[3],sens[3];
	int n;
	int i;
	
	struct sigaction nvt, old;
	struct sockaddr_in addr,addr2;
	nvt.sa_handler = handler;
	sigaction(SIGINT, &nvt, &old);
	int size = sizeof(struct sockaddr_in);
//fcntl(STDIN, F_SETFL, O_NONBLOCK) ;// rend stdin non bloquant
	socket_decoute = socket_ecoute(DEFAULT_PORT,&addr);

	if((socket_connec=accept(socket_decoute, (struct sockaddr *)&addr2, (socklen_t*)&size)) != -1)
	{
		wbInitI2C(WB_NUM_BUS);//initialise le robot;
		while((n=read(socket_connec,buffer,10))!=0)
		{
			buffer[n]='\0';
			if(strlen(buffer)==7)
			{
				instr=buffer[0];
				v1[0]=buffer[1];v1[1]=buffer[2];v1[2]='\0';
				v2[0]=buffer[3];v2[1]=buffer[4];v2[2]='\0';
			
				sens[0]=buffer[5];sens[1]=buffer[6];sens[2]='\0';
				printf("v1 %s v2 %s sens %s \n",v1,v2,sens);
				wbGetI2CInfo(WB_ADR_LEFT);	/* Recupere les infos du robot */
				wbGetI2CInfo(WB_ADR_RIGHT);
				switch(instr)
				{
				
					case '0':
						wbForceStop();
						break;
					case '1':
						wbForceAvancerCourbe(atoi(v1),atoi(v2));
						break;
					case '2':
						wbPanTiltMoveCamera(atoi(v1),atoi(v2),(WBSens)atoi(sens));
						break;
					case '3':
						wbForceReculerCourbe(atoi(v1),atoi(v2));
						break;
					case '4':
						wbTournerHoraire(atoi(v1));
						break;

					case '5':
						wbTournerAntiHoraire(atoi(v1));
						break;
					default:
						break;
					
				
	
				}
			}
		}
	} 
	else
	{
		printf("erreur d'acceptation\n");
	}

	return 0;
	
	
}
