#
# PyrBiRL - Reinforcement Learning for URBI and Python.
#  Copyright (C) 2008 Alain Dutech
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
# 
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
 
""" core components

PyrBiRL -- Reinforcement Learning in Python and Urbi
====================================================
"""

from libpyrbirl import *

def test_regression():
    """ What can be done with Regression."""

    print "*** TEST Regression"

    print "#creation default"
    print "reg = Regression()"
    reg = Regression()
    print reg

    print "\nxq = MultiDouble((2.1,2.2,3.3))"
    xq = MultiDouble((2.1,2.2,3.3))
    print "xq =",xq

    print "\nvec = [(MultiDouble((1,2,3)),1.1),(MultiDouble((4,5,6)),2.2)]"
    vec = [(MultiDouble((1,2,3)),1.1),(MultiDouble((4,5,6)),2.2)]
    print "vec =",vec

    print "\nvect = [((1,2,3),1.1), ((4,5,6),2.2)]"
    vect = [((1,2,3),1.1), ((4,5,6),2.2)]
    print "vect =",vect

    print "\ndi = MultiDouble((2,2,2))"
    di = MultiDouble((2,2,2))
    print "di =",di

    print "****"
    
    res = reg.lwr(xq,vec,di)
    print "reg.lwr(xq,vec,di) = ",res

    res = reg.lwr((2.1,2.2,3.3),vec,(2,2,2))
    print "reg.lwr((2.1,2.2,3.3),vec,(2,2,2)) =",res

    res = reg.lwr(xq,vect,di)
    print "reg.lwr(xq,vect,di) = ",res

if __name__ == "__main__":
    test_regression()
