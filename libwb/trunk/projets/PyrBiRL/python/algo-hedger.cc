/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <boost/python/operators.hpp>

#include "algo-hedger.h"
#include "multi-double.h"

// ============================================================================
double
AlgoHedger_in_ivh_from_tuple_list(rlal::AlgoHedgerPtr self,
				  tuple query_,
				  list neighbors_)
{
  // extract MultiDouble from query_
  rlco::MultiDouble _multi_query;
  int size_query = len(query_);
  _multi_query = rlco::MultiDouble(size_query);
  for (int index = 0; index < size_query; index++) {
    _multi_query[index] = extract<double>(query_[index]);
  }

  // need to extract a std::vector<TDataPtr> from neighbors
  std::vector<rlal::TDataPtr> vec_neighbors_;
  
  int nb_example = len(neighbors_);
  for( int i_ex = 0; i_ex < nb_example; ++i_ex) {
    tuple example = extract<tuple>(neighbors_[i_ex]);
    // is key a tuple or a MultiDouble
    object obj_key = extract<object>(example[0]);
    object _class = obj_key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    
    //std::cout << "key " << i_ex << " is a " << _type << "\n";
    rlco::MultiDouble _multi_key;
    if( _type == "MultiDouble" ) {
      _multi_key = extract<rlco::MultiDouble> (example[0]);
    }
    else if( _type == "tuple" ) {
      tuple key_tu = extract<tuple>(example[0]);
      // convert to MultiDouble
      int size_key = len(key_tu);
      _multi_key = rlco::MultiDouble(size_key);
      for (int index = 0; index < size_key; index++) {
	_multi_key[index] = extract<double>(key_tu[index]);
      }
    }

    double _val = extract<double>(example[1]);
    //rlnu::TData _data = boost::make_tuple( _multi_key, _val);
    rlal::TDataPtr _data_ptr(new  rlal::TData( _multi_key, _val));
    vec_neighbors_.push_back(_data_ptr);
  }
  
  double res = self->in_ivh( _multi_query, vec_neighbors_);
  return res;
}
// ============================================================================
void
AlgoHedger_learn( rlal::AlgoHedgerPtr self,
		  object old_state_, object action_, object new_state_,
		  double reward )
{
  rlco::MultiDouble old_state = MultiDouble_from_object( old_state_ );
  rlco::MultiDouble action = MultiDouble_from_object( action_ );
  rlco::MultiDouble new_state = MultiDouble_from_object( new_state_ );

  self->learn( old_state, action, new_state, reward );
}
// ============================================================================
tuple
AlgoHedger_predict( rlal::AlgoHedgerPtr self,
			 object query_)
{
  object _class = query_.attr("__class__");
  object _name = _class.attr("__name__");
  std::string _type = extract<std::string>(_name)();

  rlal::TPrediction status;

  if( (_type == "tuple") or (_type == "list") or (_type == "MultiDouble") ) {
    // extract MultiDouble from val
    rlco::MultiDouble _query;
    int size_query = len( query_ );
    _query = rlco::MultiDouble(size_query);
    for (int index = 0; index < size_query; index++) {
      _query[index] = extract<double>(query_[index]);
    }
    
    double res = self->predict( _query, status);
    tuple result = boost::python::make_tuple( res, status);
    return result;
  }
  else {
    std::string msg("set_bandwidth with a "+_type);
    throw std::out_of_range(msg);
  }
} 
// ============================================================================
void
AlgoHedger_set_bandwidth(rlal::AlgoHedgerPtr self,
			 object val)
{
  object _class = val.attr("__class__");
  object _name = _class.attr("__name__");
  std::string _type = extract<std::string>(_name)();

  if( (_type == "tuple") or (_type == "list") or (_type == "MultiDouble")) {
    // extract MultiDouble from val
    rlco::MultiDouble _bandwidth;
    int size_val = len(val);
    _bandwidth = rlco::MultiDouble(size_val);
    for (int index = 0; index < size_val; index++) {
      _bandwidth[index] = extract<double>(val[index]);
    }
    self->set_bandwidth( _bandwidth );
  }
  else {
    std::string msg("set_bandwidth with a "+_type);
    throw std::out_of_range(msg);
  }
}
// ============================================================================
void
AlgoHedger_set_list_actions_from_list(rlal::AlgoHedgerPtr self,
				      list vec_action)
{
  // need to extract a std::vector<MultiDouble> from vec_action
  std::vector<rlco::MultiDouble> vec_action_;
  
  int nb_action = len(vec_action);
  for( int i_ac = 0; i_ac < nb_action; ++i_ac) {
    // is action a tuple or a MultiDouble
    object obj_key = extract<object>(vec_action[i_ac]);
    object _class = obj_key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    
    rlco::MultiDouble action_;
    if( _type == "MultiDouble" ) {
      action_ = extract<rlco::MultiDouble> (vec_action[i_ac]);
    }
    else if( _type == "tuple" ) {
      tuple tple_action = extract<tuple> (vec_action[i_ac]);
      // convert to MultiDouble
      int size_act = len(tple_action);
      action_ = rlco::MultiDouble(size_act);
      for (int index = 0; index < size_act; ++index) {
	action_[index] = extract<double>(tple_action[index]);
      }
    }
    
    vec_action_.push_back( action_ );
  }
  self->set_list_actions( vec_action_ );
}
// ============================================================================
void AlgoHedger_export (void)
{
  register_ptr_to_python <rlal::AlgoHedgerPtr> ();

  enum_<rlal::TPrediction>("TPrediction")
    .value("PRED_NORMAL",rlal::PRED_NORMAL)
    .value("PRED_EXACT",rlal::PRED_EXACT)
    .value("PRED_NOT_ENOUGH",rlal::PRED_NOT_ENOUGH)
    .value("PRED_IVH",rlal::PRED_IVH)
    .value("PRED_BOUNDS",rlal::PRED_BOUNDS)
    ;

  class_<rlal::AlgoHedger, bases <dana::core::Atom> >
    ("AlgoHedger",
     "AlgoHedger is an RL algorithm based on W. Smart work.                  \n"
     "                                                                       \n")

    .def("__init__",
	 make_constructor (rlal::AlgoHedger::make<rlal::AlgoHedger>),
	 "Create a new AlgoHedger                                            \n"
	 "                                                                   \n"
	 "__init(self,tree)__ -> new AlgoHedger(tree)                        \n")
  
    .def("learn",
	 AlgoHedger_learn,
	 "Learn from a given interaction (state, action, state') + reward    \n"
	 "                                                                   \n"
	 "learn( old_state, action, new_state, reward )                      \n")

    .def("predict",
	 AlgoHedger_predict,
	 "Predict the Q-Value of a query point given the actual q_val tree   \n"
	 "                                                                   \n"
	 "predict( MultiDouble query_pt ) --> (double, status)               \n")

    .def("in_ivh",
	 AlgoHedger_in_ivh_from_tuple_list,
	 "Check if a query point is in the IVH of the neighbors.             \n"
	 "                                                                   \n"
	 "in_ivh( tuple query_point, list neighbors )                        \n")

    .def("__repr__",
	 &rlal::AlgoHedger::repr,
	 "AlgoHedger string representation of parameters                 \n"
	 "                                                               \n"
	 "repr(self) -> string                                           \n"
	 "  string -- object representation                              \n"
         "                                                               \n")

    .add_property("ivh_tolerance",
		  &rlal::AlgoHedger::get_ivh_tolerance,
		  &rlal::AlgoHedger::set_ivh_tolerance,
		  "Tolerance coeficient for being into the IVH               \n")
    .add_property("nb_pts_for_regression",
		  &rlal::AlgoHedger::get_nb_pts_for_regression,
		  &rlal::AlgoHedger::set_nb_pts_for_regression,
		  "Needed nb of points for a proper regression               \n")
    .add_property("bandwidth",
		  &rlal::AlgoHedger::get_bandwidth,
		  AlgoHedger_set_bandwidth,
		  "One bandwidth coefficient along each dimension            \n")
    .add_property("qval_tree",
		  &rlal::AlgoHedger::get_qval_tree,
		  &rlal::AlgoHedger::set_qval_tree,
		  "KdTree for storing Q-Values.                              \n")
    .add_property("list_actions",
		  &rlal::AlgoHedger::get_list_actions,
		  AlgoHedger_set_list_actions_from_list,
		  "List of all the possible actions.                         \n")
    .add_property("alpha",
		  &rlal::AlgoHedger::get_alpha,
		  &rlal::AlgoHedger::set_alpha,
		  "Learning coef. for Q-Learning.                            \n")
    .add_property("gamma",
		  &rlal::AlgoHedger::get_gamma,
		  &rlal::AlgoHedger::set_gamma ,
		  "Discounted coef. for Q-Learning.                          \n")
    ;
}
