#
# PyrBiRL - Reinforcement Learning for URBI and Python.
#  Copyright (C) 2008 Alain Dutech
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
# 
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
 
""" core components

PyrBiRL -- Reinforcement Learning in Python and Urbi
====================================================
"""

from libpyrbirl import *

def test_multi_double():
    """ What can be done with multi-double."""

    print "*** TEST MultiDouble"

    # creation with size
    multi = MultiDouble( 2 )
    print "creation multi = MultiDouble( 2 ) : ", multi
    print "size : ", multi.size

    # creation with tuple
    multi = MultiDouble( (1,3.2))
    print "creation m = MultiDouble( (1,3.2)) : ", multi
    print "size : ", multi.size

    # assignment like array
    multi[1] = 1.2
    print "assignment : ", multi

    # distance
    m2 = MultiDouble( (1,2.9))
    w = MultiDouble( (1,2))
    print multi, " et ", m2, " dist = ", multi.square_dist(m2)
    print multi, " et ", m2, " weighted_dist = ", multi.weighted_square_dist(m2,w)

    # concatenation
    print "m2.concat(w)=", m2.concat(w)
    print "m2.concat( (2,5) )=", m2.concat( (2,5) )
    print "m2.concat( [3,4] )=", m2.concat( [3,4] )
    
if __name__ == "__main__":
    test_multi_double()


    
