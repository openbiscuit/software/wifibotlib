/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <boost/python/operators.hpp>

#include "regression.h"
// ============================================================================
float
Regression_lwr_from_MultiDouble_list (rlnu::RegressionPtr self,
				      rlco::MultiDoublePtr query_,
				      list neighbors_,
				      rlco::MultiDoublePtr bandwidth_)
{
  // need to extract a std::vector<TDataPtr> from neighbors
  std::vector<rlnu::TDataPtr> vec_neighbors_;

  int nb_example = len(neighbors_);
  for( int i_ex = 0; i_ex < nb_example; ++i_ex) {
    tuple example = extract<tuple>(neighbors_[i_ex]);
    // is key a tuple or a MultiDouble
    object obj_key = extract<object>(example[0]);
    object _class = obj_key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    
    //std::cout << "key " << i_ex << " is a " << _type << "\n";
    rlco::MultiDouble _multi_key;
    if( _type == "MultiDouble" ) {
      _multi_key = extract<rlco::MultiDouble> (example[0]);
    }
    else if( _type == "tuple" ) {
      tuple key_tu = extract<tuple>(example[0]);
      // convert to MultiDouble
      int size_key = len(key_tu);
      _multi_key = rlco::MultiDouble(size_key);
      for (int index = 0; index < size_key; index++) {
	_multi_key[index] = extract<double>(key_tu[index]);
      }
    }

    double _val = extract<double>(example[1]);
    //rlnu::TData _data = boost::make_tuple( _multi_key, _val);
    rlnu::TDataPtr _data_ptr(new  rlnu::TData( _multi_key, _val));
    vec_neighbors_.push_back(_data_ptr);
  }
  
  double res = self->lwr_regression( *query_, vec_neighbors_, *bandwidth_ );
  //std::cout << "res = " << res << "\n";
  return res;
}
float
Regression_lwr_from_tuple_list (rlnu::RegressionPtr self,
				tuple query_,
				list neighbors_,
				tuple bandwidth_)
{
  // extract MultiDouble from query_
  rlco::MultiDouble _multi_query;
  int size_query = len(query_);
  _multi_query = rlco::MultiDouble(size_query);
  for (int index = 0; index < size_query; index++) {
    _multi_query[index] = extract<double>(query_[index]);
  }
  
  // need to extract a std::vector<TDataPtr> from neighbors
  std::vector<rlnu::TDataPtr> vec_neighbors_;
  
  int nb_example = len(neighbors_);
  for( int i_ex = 0; i_ex < nb_example; ++i_ex) {
    tuple example = extract<tuple>(neighbors_[i_ex]);
    // is key a tuple or a MultiDouble
    object obj_key = extract<object>(example[0]);
    object _class = obj_key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    
    //std::cout << "key " << i_ex << " is a " << _type << "\n";
    rlco::MultiDouble _multi_key;
    if( _type == "MultiDouble" ) {
      _multi_key = extract<rlco::MultiDouble> (example[0]);
    }
    else if( _type == "tuple" ) {
      tuple key_tu = extract<tuple>(example[0]);
      // convert to MultiDouble
      int size_key = len(key_tu);
      _multi_key = rlco::MultiDouble(size_key);
      for (int index = 0; index < size_key; index++) {
	_multi_key[index] = extract<double>(key_tu[index]);
      }
    }

    double _val = extract<double>(example[1]);
    //rlnu::TData _data = boost::make_tuple( _multi_key, _val);
    rlnu::TDataPtr _data_ptr(new  rlnu::TData( _multi_key, _val));
    vec_neighbors_.push_back(_data_ptr);
  }
  
  // extract MultiDouble from bandwidth_
  rlco::MultiDouble _multi_bandwidth;
  int size_bandwidth = len(bandwidth_);
  _multi_bandwidth = rlco::MultiDouble(size_bandwidth);
  for (int index = 0; index < size_bandwidth; index++) {
    _multi_bandwidth[index] = extract<double>(bandwidth_[index]);
  }

  double res = self->lwr_regression( _multi_query, vec_neighbors_, _multi_bandwidth );
  return res;
}

// ============================================================================
void Regression_export (void)
{
  register_ptr_to_python <rlnu::RegressionPtr> ();

  class_<rlnu::Regression, bases <dana::core::Atom> >
    ("Regression",
     "Regression is a toolbox for regression computations                    \n"
     "                                                                       \n")

    .def("__init__",
	 make_constructor (rlnu::Regression::make<rlnu::Regression>),
	 "Create a new Regression                                            \n"
	 "                                                                   \n"
	 "__init(self)__ -> new Regression()                                 \n")

    .def("__repr__",
	 &rlnu::Regression::repr,
	 "Regression string representation                               \n"
	 "                                                               \n"
	 "repr(self) -> string                                           \n"
	 "  string -- object representation                              \n"
	 "                                                               \n")

    .def("lwr",
	 Regression_lwr_from_MultiDouble_list,
	 "compute a lwr regression                                              \n"
         "                                                                      \n"
         "lwr( query_point, vec_neighbors, bandwidth, constant )                \n"
         "    query_pt MultiDouble/tuple query vector                                 \n"
	 "    vec_neighbors a vector of (key,val) neighbors of query_pt         \n"
	 "    bandwidth along each dimension (MultiDouble/tuple)                      \n"
	 "    constant 0 or 1, i.e., add constant column no or yes (default yes)\n")
    .def("lwr",
	 Regression_lwr_from_tuple_list)
    ;
}
