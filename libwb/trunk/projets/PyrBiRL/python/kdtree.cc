/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/python/operators.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/foreach.hpp>

#include "kdtree.h"

typedef pyrbirl::core::KdTree<double> KdTreeDouble;
typedef  boost::shared_ptr<KdTreeDouble> KdTreeDoublePtr;

typedef boost::tuple<MultiDouble,double> TDataDouble;
/** pointer to stored Element */
typedef boost::shared_ptr<TDataDouble> TDataDoublePtr;

// ============================================================================
KdTreeDoublePtr
KdTreeDoublePtr_from_dim_nb_data( int nb_dimension = 1,
				  int nb_data_in_leaf = 10 )
{
  KdTreeDoublePtr self;
  self = KdTreeDoublePtr (new KdTreeDouble( nb_dimension, nb_data_in_leaf) );

  return self;
}
KdTreeDoublePtr
KdTreeDoublePtr_from_weight_nb_data( MultiDoublePtr weight_from_python,
				  int nb_data_in_leaf = 10 )
{
  KdTreeDoublePtr self;
  int dim = weight_from_python->size();
  // build weight from tuple
  self = KdTreeDoublePtr (new KdTreeDouble( dim, nb_data_in_leaf, weight_from_python));

  return self;
}
// ============================================================================
int
KdTreeDouble_add_tuple_val(KdTreeDoublePtr self,  tuple key, double val )
{
  // extract MultiDouble from key
  int size = len( key );

  if( size != self->get_nb_dim() ) {
    throw std::out_of_range("dim must agree");
  }

  MultiDouble key_tmp(size);
  for (int index = 0; index < size; index++) {
    key_tmp[index] = extract<double>(key[index]);
  }

  return self->add( key_tmp, val );
}
// ============================================================================
list
KdTreeDouble_get_nearest_approximate(KdTreeDoublePtr self,
				     MultiDoublePtr query,
				     int nb_point)
{
  std::vector<TDataDoublePtr> result = self->get_nearest_approximate( *query, nb_point);

  // fill up a list with the nearest points
  list res;
  BOOST_FOREACH( TDataDoublePtr data, result ) {
    //std::cout <<  KdTreeDouble::get_key(data).repr() << " ==> " << KdTreeDouble::get_val(data) << "\n";
    // extract key and val
    MultiDouble key = KdTreeDouble::get_key(data);
    double val = KdTreeDouble::get_val(data);
    // add new tuple element to list
    tuple elem = boost::python::make_tuple( key, val);
    res.append( elem );
    
  }
  return res;
}
list
KdTreeDouble_get_nearest_approximate_tuple(KdTreeDoublePtr self,
				     tuple query,
				     int nb_point)
{
  // extract MultiDouble from key
  int size = len( query );

  if( size != self->get_nb_dim() ) {
    throw std::out_of_range("dim must agree");
  }

  MultiDoublePtr query_tmp = MultiDoublePtr (new MultiDouble(size) );
  for (int index = 0; index < size; index++) {
    (*query_tmp)[index] = extract<double>(query[index]);
  }
  return KdTreeDouble_get_nearest_approximate( self, query_tmp, nb_point );
}

// ============================================================================
void KdTreeDouble_export (void)
{
  register_ptr_to_python <TDataDoublePtr> ();
  register_ptr_to_python <KdTreeDoublePtr> ();

  class_<TDataDouble>
    ("DataDouble",
     "")
    ;

  class_<std::vector<TDataDoublePtr> >
     ("DataVec",
     "list of tuple (MultiDouble,double) \n")
    .def(vector_indexing_suite< std::vector<TDataDoublePtr> >())
    ;


  class_<KdTreeDouble, bases <dana::core::Atom> >
    ("KdTreeDouble",
     "A KdTreeDouble is a mapping from MultiDouble to Double with split nodes    \n"
     "                                                                     \n")

    .def("__init__",
	 make_constructor (KdTreeDouble::make<KdTreeDouble>),
    	 "Create a new KdTreeDouble                                        \n"
    	 "                                                                 \n"
 	 "__init__(self) -> new KdTreeDouble(1,10,TWeightPtr::shared_ptr())\n"
	 "__init__(self, nb_dim, nb_data_in_leaf)                          \n"
    	 "__init__(self, weight, nb_data_in_leaf)                          \n") 
    .def("__init__",
    	 make_constructor (KdTreeDoublePtr_from_dim_nb_data))
    .def("__init__",
    	 make_constructor (KdTreeDoublePtr_from_weight_nb_data))

    .def("__repr__",
	 &KdTreeDouble::repr,
	 "KdTreeDouble string representation                               \n"
	 "                                                                 \n"
	 "repr(self) -> string                                             \n")
    .def("add",
	 &KdTreeDouble::add,
	 "Add a new (key,val) to the KdTreeDouble                          \n"
	 "                                                                 \n"
	 "add(MultiDouble, Double) -> int                                  \n"
	 "add(tuple, Double) -> int                                        \n")
    .def("add",
	 KdTreeDouble_add_tuple_val)
    .def("clear",
	 &KdTreeDouble::clear,
	 "Clear up the tree, not touching the weight.                      \n")
    .def("get_nearest_approximate",
	 KdTreeDouble_get_nearest_approximate,//&KdTreeDouble::get_nearest_approximate,
	 "Quick search for the 'n' approximately nearest points            \n"
	 "                                                                 \n"
	 "get_nearest_approximate(MultiDouble key, int n) -> list          \n"
	 "get_nearest_approximate(tuple key, int n) -> list                \n")    
    .add_property("nb_dim",
		  &KdTreeDouble::get_nb_dim,
		  "nb of dimension for Key\n")
    ;
}
