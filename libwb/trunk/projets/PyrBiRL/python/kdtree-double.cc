/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/python/operators.hpp>

#include "kdtree.h"

typedef KdTree<double> KdTreeDouble;

// ============================================================================
void KdtreeDouble_export (void)
{
  class <KdTreeDouble, bases <dana::core::Atom> >
    ("KdTree",
     "A KdTree is a mapping from MultiDouble to Double with split nodes    \n"
     "                                                                     \n")

    .def("__init__",
	 make_constructor (KdTreeDouble::make<KdTreeDouble>),
	 "Create a new KdTreeDouble                                        \n",
	 "                                                                 \n",
	 "__init__(self) -> new KdTreeDouble( 1, 10, empty_ptr )           \n")

    .def("__repr__",
	 &KdTreeDouble::repr,
	 "KdTreeDouble string representation                               \n",
	 "                                                                 \n",
	 "repr(self) -> string                                             \n")
    ;
}
