# Standard things
# saves the variable $(d) that holds the current directory on the stack,
# and sets it to the current directory given in $(dir), 
# which was passed as a parameter by the parent rules.mk

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Subdirectories, if any, in random order

#dir	:= $(d)/test
#include		$(dir)/Rules.mk

# Next, we create an immediately evaluated variable $(OBJS_common) 
# that holds all the objects in that directory.
# We also create $(DEPS_common), which lists any automatic dependency files 
# generated later on.
# To the global variable $(CLEAN), we add the files that the rules present here 
# may create, i.e. the ones we want deleted by a make clean command.)

# Local variables

OBJS_$(d)	:= $(d)/global.o                      \
                   $(d)/export.o                      \
                   $(d)/multi-double.o                \
                   $(d)/kdtree.o                      \
                   $(d)/regression.o                  \
                   $(d)/algo-hedger.o
DEPS_$(d)	:= $(OBJS_$(d):%=%.d)

TGT_LIB         := $(TGT_LIB) verbose_$(d) $(d)/libpyrbirl.so
CLEAN		:= $(CLEAN) $(OBJS_$(d)) $(DEPS_$(d)) \
                   $(d)/libpyrbirl.so

# Now we list the dependency relations relevant to the files in this subdirectory.
# Most importantly, while generating the objects listed here, we want to set 
# the target-specific compiler flags $(CF_TGT) to include a flag that adds 
# this directory to the include path, so that local headers may be included 
# using #include <localfile.h>, which is, as said, more portable 
# than local includes when the source directory is not the current directory.)

# Local rules -> create archive 'libpyrbirl.so'
.PHONY : verbose_$(d)
verbose_$(d): $(d)/libpyrbirl.so
	@echo "Generating $^"


$(OBJS_$(d)):	CF_TGT := -I. -I$(d) -I/usr/include/python2.5
$(OBJS_$(d)):	LL_TGT := -lboost_python -lpython2.5 \

$(d)/libpyrbirl.so: LL_TGT := -lboost_python -lpython2.5 \
                              -lgsl -lgslcblas -lm \
                              -llog4cxx
$(d)/libpyrbirl.so: $(OBJS_$(d)) dana-cpp/dana.a dana-python/dana.a cpp/core.a
	$(DYN_LIB)



# As a last step, we restore the value of the current directory variable $(d) 
# by taking it from the current location on the stack, and we "decrement" 
# the stack pointer by stripping away the .x suffix 
# we added at the top of this file.

# Standard things

-include	$(DEPS_$(d))

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))

