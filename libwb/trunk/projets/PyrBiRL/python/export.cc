/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "export.h"
#include <dana-python/atom.h>

// important to name it with the name of the dynamic library created...
BOOST_PYTHON_MODULE(libpyrbirl) {

  Atom_export();
  Global_export ();
  MultiDouble_export ();
  KdTreeDouble_export ();
  Regression_export ();
  AlgoHedger_export ();
}
