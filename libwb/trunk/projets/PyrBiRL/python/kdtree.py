#
# PyrBiRL - Reinforcement Learning for URBI and Python.
#  Copyright (C) 2008 Alain Dutech
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
# 
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
 
""" core components

PyrBiRL -- Reinforcement Learning in Python and Urbi
====================================================
"""

from libpyrbirl import *

def test_kdtree():
    """ What can be done with kdtree-double."""

    print "*** TEST KdTreeDouble"

    print "#creation default"
    print "tree = KdTreeDouble()"
    tree = KdTreeDouble()
    print tree

    print "#creation with dim, nb in leaf"
    print "tree = KdTreeDouble( 4, 7 )"
    tree = KdTreeDouble( 4, 7 )
    print tree

    print "#creation with MultiDouble weight and nb in leaf"
    print "tree = KdTreeDouble( MultiDouble( (1,2,3) ), 6 )"
    tree = KdTreeDouble( MultiDouble( (1,2,3) ), 6 ) 
    print tree

    print "#ajout d'un Multidouble"
    print "tree.add( MultiDouble( (2,2,2) ), 6)"
    tree.add( MultiDouble( (2,2,2) ), 6)
    print tree

    print "#ajout d'un tuple"
    print "tree.add( (3,3,3), 9)"
    tree.add( (3,3,3), 6)
    print tree

    print "#ajout et split automatique"
    for x in range(0.0,15.0):
        tree.add( (x,2*x, 1+(x/10.0)), x*x)
    print tree

    print "# get_nearest_approximate"
    print "rep = tree.get_nearest_approximate( MultiDouble((1,4,9)),7)"
    rep = tree.get_nearest_approximate( MultiDouble((1,4,9)),7)
    for data in rep:
        print data

    print "# clear up"
    print "tree.clear()"
    tree.clear()
    print tree
        
if __name__ == "__main__":
    test_kdtree()
