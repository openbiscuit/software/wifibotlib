/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "global.h"

using namespace pyrbirl::core;
using namespace boost::python;

// ============================================================================
// Generate wrappers for function init with at least 0 args and 1 at most
//BOOST_PYTHON_FUNCTION_OVERLOADS(init_overloads, init, 0, 1)
// ============================================================================
int
Global_init_void (void)
{
  pyrbirl::core::init();
  return 0;
}
int
Global_init_string (std::string prop_file)
{
  pyrbirl::core::init(prop_file);
  return 0;
}
// ============================================================================
void Global_export (void)
{
  def("init",
       Global_init_void,
       "Initialize the libPyrBiRL module, especially for logging.          \n"
       "                                                                   \n"
       " init()                                                            \n"
       " init( name_of_log4cxx_properties_file )                           \n")
    ;   
}
