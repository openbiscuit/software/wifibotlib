#
# PyrBiRL - Reinforcement Learning for URBI and Python.
#  Copyright (C) 2008 Alain Dutech
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
# 
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

from libpyrbirl import *

import sys
# --- GUI
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
except:
    sys.exit(1)
# --- GUI

class Transition(object):
    """
    A Transition is etat_from + action => etat_to [rec].
    """

    def __init__(self, data):
        """
        Build a Transition from a list of double.
        @data list of double.
        """
        self.etat_from = MultiDouble( (data[0],data[1]) )
        self.action = data[2]
        self.etat_to = MultiDouble( (data[3],data[4]) )
        self.rec = data[5]

    def __repr__(self):
        """
        Standard representation.
        """
        return self.etat_from.__repr__()+" + "+self.action.__repr__()+" => "+self.etat_to.__repr__()+" ["+self.rec.__repr__()+"]"

class Trace(object):
    """
    A list of Transitions.
    """
    def load(self, file_name):
        """
        Load a Trace from a file.
        """
        self.trans = []
        input = open( file_name, 'r')

        # pour chaque ligne
        for line in input.readlines():
            if( (line.startswith("/*") == False ) |
                (line.startswith("#") == False) ):
                # separe chaque nombre (avec espace entre eux)
                data = map(float, line.split())

                trans_ = Transition(data)
                self.trans.append(trans_)


class TraceViewer:
    """
    Using a pygtk.TreeView to display the Trace.
    """
    
    def __init__(self, _trace):
        """
        Create self.widget that will hold a TreeView.
        """
        self.trace = _trace
        # TreeStore with 4 columns (etat_from, action, etat_to, rec)
        self.treestore = gtk.ListStore(str,str,str,str)

        # adding data
        for transition in _trace.trans:
            self.treestore.append((transition.etat_from.__repr__(),
                                   transition.action.__repr__(),
                                   transition.etat_to.__repr__(),
                                   transition.rec.__repr__()) )
            
        # TreeView from TreeStore
        self.treeview = gtk.TreeView( self.treestore)
        self.treeview.set_rules_hint( True )
        # columns
        col_names = ['etat from', 'action', 'etat to', 'rec']
        for col in range(4):
            column = gtk.TreeViewColumn( col_names[col] )
            self.treeview.append_column(column)
            cell = gtk.CellRendererText()
            column.pack_end(cell, False) # not expandable
            column.add_attribute(cell, 'text', col)

        # connecting callback
        self.treeview.connect("row-activated", self.row_activated_event) 

        self.widget = self.treeview
        self.widget.show()

    def row_activated_event(self, _widget, _path, _view_col ):
        """
        Called when a row is double clicked.
        [Will] Simulate learning with the transitions up to the one 
        selected.
        """
        print "row_activated at path=", _path
        nb_row = _path[0]
        for transition in self.trace.trans[:nb_row+1]:
            print "learning with ",transition

class MainWindowGTK:
    """
    Window to which a widget must be attached.
    """
    
    def __init__(self):
        # create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        # When the window is given the "delete_event" signal (this is given
        # by the window manager, usually by the "close" option, or on the
        # titlebar), we ask it to call the delete_event () function
        # as defined above. The data passed to the callback
        # function is NULL and is ignored in the callback function.
        self.window.connect("delete_event", self.delete_event)
    
        # Here we connect the "destroy" event to a signal handler.  
        # This event occurs when we call gtk_widget_destroy() on the window,
        # or if we return FALSE in the "delete_event" callback.
        self.window.connect("destroy", self.destroy)

        # and the window
        self.window.show()

    def delete_event(self, widget, event, data=None):
        # If you return FALSE in the "delete_event" signal handler,
        # GTK will emit the "destroy" signal. Returning TRUE means
        # you don't want the window to be destroyed.
        # This is useful for popping up 'are you sure you want to quit?'
        # type dialogs.
        print "delete event occurred"

        # Change FALSE to TRUE and the main window will not be destroyed
        # with a "delete_event".
        return False

    def destroy(self, widget, data=None):
        print "destroy signal occurred"
        gtk.main_quit()

    def main(self):
        # All PyGTK applications must have a gtk.main(). Control ends here
        # and waits for an event to occur (like a key press or mouse event).
        gtk.main()

# main will load a trace and view it in gtk.TreeView
if __name__ == "__main__":
    trace = Trace()
    trace.load( "traces.txt" )
    trace_viewer = TraceViewer( trace )
    win = MainWindowGTK()
    win.window.add( trace_viewer.widget )
    win.main()
    

    
