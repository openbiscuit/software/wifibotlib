#
# PyrBiRL - Reinforcement Learning for URBI and Python.
#  Copyright (C) 2008 Alain Dutech
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
# 
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

""" core components

PyrBiRL -- Reinforcement Learning in Python and Urbi
====================================================
"""

from libpyrbirl import *
import scipy
import scipy.linalg
import pylab
import matplotlib.patches
import math

def test_algo_hedger():
    """
    Test of AlgoHedger.
    """
    # for logging
    init()
    
    print "*** TEST AlgoHedger"

    print "#creation"
    print "algo = AlgoHedger()"
    algo = AlgoHedger()
    print "algo.ivh_tolerance = 1.5"
    algo.ivh_tolerance = 1.5
    print algo

    data = []
    x_data = []
    y_data = []
    data_key = [(1,1),(2,2),(1.25,1.75),(1.75,1.25)]
    for (x,y) in data_key:
        data.append(((x,y),1.0))
        x_data.append(x)
        y_data.append(y)
    print "\ndata = ",data

    print "\nxq = (1.2, 1)"
    xq = (1.2, 1)

    print "****"

    res = algo.in_ivh( xq, data )
    print "algo.in_ivh( xq, data ) = ",res

    # for each point, add to 'cross' if outside IVH
    # or to 'plus' if inside
    x_cross = []
    y_cross = []
    x_plus = []
    y_plus = []
    xrange = scipy.arange(0,3,0.2)
    yrange = scipy.arange(0,3,0.2)
    for x in xrange:
        for y in yrange:
            if( algo.in_ivh( (x,y), data)):
                x_plus.append(x)
                y_plus.append(y)
            else:
                x_cross.append(x)
                y_cross.append(y)

    fig = pylab.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim(0,3)
    ax.set_ylim(0,3)

    # affiche les points avec des croix rouges
    ax.plot(x_plus,y_plus,'g+')
    # affiche les points avec des etoiles rouges
    ax.plot(x_cross,y_cross,'rx')
    # affiche les points de reference
    ax.plot(x_data,y_data,'go')
    # TEMP pylab.show()

    # create a tree
    qv_tree = KdTreeDouble( 2, 10 )
    qv_tree.add( (1,1), 1)
    qv_tree.add( (2,1), 1.5)
    #qv_tree.add( (1.5,1.5), 2)
    print "** qv_tree**\n",qv_tree
                               
    # attach to algo
    algo.qval_tree = qv_tree
    # bandwidth
    algo.bandwidth = (1,1)

    print "**algo**\n",algo

    # prediction
    res = algo.predict( (1.5,1.25) )
    print "algo.predict( (1.5,1.25) ) => ", res

    qv_tree.add( (1.5,1.5), 2)
    # prediction
    res = algo.predict( (1.5,1.25) )
    print "algo.predict( (1.5,1.25) ) => ", res
    # prediction
    res = algo.predict( (1.001,0.99) )
    print "algo.predict( (1.001,0.99) ) => ", res
    # prediction
    res = algo.predict( (0.0,3.0) )
    print "algo.predict( (0.0,3.0) ) => ", res


    print "*** LEARNING ***"
    algo.qval_tree.clear()

    print "Add a list of action"
    vec = [ (0), (1)]
    algo.list_actions = vec 

    state = MultiDouble( (1.0,1.0) )
    action = MultiDouble( (0) )
    new_state = MultiDouble( (2.0, 1.0) )
    reward = 1.0
    algo.learn( state, action, new_state, reward )
    print "algo.learn(", state, ", ", action, ", ", new_state, ", ", reward," )"
    print "**qvtree\n", algo.qval_tree

    state = new_state;
    new_state = MultiDouble( (1.5, 1.5) )
    reward = 1.5
    algo.learn( state, action, new_state, reward )
    print "algo.learn(", state, ", ", action, ", ", new_state, ", ", reward," )"
    print "**qvtree\n", algo.qval_tree

    state = new_state;
    new_state = MultiDouble( (1.0, 1.0) )
    reward = 2.0
    algo.learn( state, action, new_state, reward )
    print "algo.learn(", state, ", ", action, ", ", new_state, ", ", reward," )"
    print "**qvtree\n", algo.qval_tree
                         
    
if __name__ == "__main__":
    test_algo_hedger()

