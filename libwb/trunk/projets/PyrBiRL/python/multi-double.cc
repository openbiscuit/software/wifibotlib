/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/python/operators.hpp>

#include "multi-double.h"

// ============================================================================
double
MultiDouble_get_item (MultiDoublePtr self, object key) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();

    if (_type == "int") {
        int index = extract<int>(key)();
	return (*self)[index];

    }
    //    else if ((_type == "tuple") or (_type == "list")) {
      
//         int y = extract<int>(key[0])();
//         int x = extract<int>(key[1])();
//         return self->get_item(y,x);

//     } else if (_type == "slice") {
//         object indices = key.attr("indices")(self->size());
//         int start = extract< int >(indices[0])();
//         int stop = extract<int>(indices[1])();
//         int step = extract<int>(indices[2])();
//         return self->get_slice (start, stop, step);    
//     }
    throw std::out_of_range("Key must be an integer");// or a slice");
}
// ============================================================================
void
MultiDouble_set_item (MultiDoublePtr self, object key, object item) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    
    if (_type == "int") {
      int index = extract<int>(key)();
      double val = extract<double>(item)();
      (*self)[index] = val;
      return;
    }
    throw std::out_of_range("Key must be an integer");
}
// ============================================================================
MultiDoublePtr
MultiDouble_from_tuple( tuple obj )
{
  MultiDoublePtr self;
  int size = len( obj );

  self = MultiDoublePtr (new MultiDouble(size));
  
  for (int index = 0; index < size; index++) {
    (*self)[index] = extract<double>(obj[index]);
  }
  return self;
}
// ============================================================================
MultiDouble
MultiDouble_from_object( object obj )
{
  MultiDouble result;

  // type of object
  object _class = obj.attr("__class__");
  object _name = _class.attr("__name__");
  std::string _type = extract<std::string>(_name)();

  if(_type == "tuple" ) {
    tuple object = extract<tuple> (obj);
    // convert to MultiDouble
    int size_res = len(object);
    result = MultiDouble(size_res);
    for (int index = 0; index < size_res; ++index) {
      result[index] = extract<double>(object[index]);
      }
  }
  else if(_type == "list") {
    list object = extract<list> (obj);
    // convert to MultiDouble
    int size_res = len(object);
    result = MultiDouble(size_res);
    for (int index = 0; index < size_res; ++index) {
      result[index] = extract<double>(object[index]);
      }
  }
  else if(_type == "MultiDouble") {
    result = extract<MultiDouble> (obj);
  }
  return result;
}
// ============================================================================
MultiDoublePtr
MultiDouble_concat( MultiDoublePtr self,
		    object other )
{
  object _class = other.attr("__class__");
  object _name = _class.attr("__name__");
  std::string _type = extract<std::string>(_name)();

  if( (_type == "tuple") or (_type == "list") or (_type == "MultiDouble") ) {
    // extract MultiDouble from other
    MultiDouble _other;
    int _size_other = len( other );
    _other = MultiDouble( _size_other );
    for (int _index = 0; _index < _size_other; ++_index ) {
      _other[_index] = extract<double>(other[_index]);
    }
    
    MultiDoublePtr _res = MultiDoublePtr (new MultiDouble( self->concat(_other)));
    return _res;
  }
  else {
    std::string msg("concatenate MultiDouble with "+_type);
    throw std::invalid_argument(msg);
  }
}
// ============================================================================
void MultiDouble_export (void)
{
  register_ptr_to_python <MultiDoublePtr> ();

  class_<MultiDouble, bases <dana::core::Atom> >
    ("MultiDouble",
     "A MultiDouble is an array composed of Double                        \n"
     "                                                                    \n")
    .def("__init__",
	 make_constructor (MultiDouble::make<MultiDouble>),
	 "Create a new MultiDouble                                       \n"
	 "                                                               \n"
	 "__init__(self) -> new MultiDouble(1)                           \n"
	 "__init__(self,size) -> new MultiDouble(size)                   \n"
	 "__init__(self,tuple) -> new MultiDouble(tuple.size)+assign     \n"
	 "                                                               \n")
    .def("__init__", make_constructor (MultiDouble_from_tuple))
    .def("__repr__",
	 &MultiDouble::repr,
	 "MultiDouble string representation                              \n"
	 "                                                               \n"
	 "repr(self) -> string                                           \n"
	 "  string -- object representation                              \n"
	 "                                                               \n")
    .def("__len__",
	 &MultiDouble::size,
	 "x.__len__ <==> len(x)")
    .def("__getitem__",
	 MultiDouble_get_item,
	 "x.__getitem__ (y) <==> x[y]")    
    .def("__setitem__",
	 MultiDouble_set_item,
	 "x.__setitem__(i, y) <==> x[i]=y")
    .def("concat",
	 MultiDouble_concat,
	 "(self,concat) = self.concat(other)")
    .def("square_dist",
	 &MultiDouble::square_dist,
	 "compute the square of the euclidian distance to other")
    .def("weighted_square_dist",
	 &MultiDouble::weighted_square_dist,
	 "compute the square of the weighted euclidian distance to other")
    
    .add_property("size",
		  &MultiDouble::size,
		  "nb of double")
    ;
}
    


