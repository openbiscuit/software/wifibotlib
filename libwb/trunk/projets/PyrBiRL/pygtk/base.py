#!/usr/bin/env python

# example base.py

import pygtk
pygtk.require('2.0')
import gtk

# Essai pour faire un objet qui s'affiche
class HelloButton(object):
    def __init__(self, name):
        self.name = name
        self.hello_button = gtk.Button( self.name )
        self.hello_button.connect("clicked", self.cbk_hello, None)
        self.hello_button.show()
        
    def cbk_hello(self, widget, data=None):
        print self.name," says Hello"

    

class Base:
    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        # When the window is given the "delete_event" signal (this is given
        # by the window manager, usually by the "close" option, or on the
        # titlebar), we ask it to call the delete_event () function
        # as defined above. The data passed to the callback
        # function is NULL and is ignored in the callback function.
        self.window.connect("delete_event", self.cbk_delete_event)

        # Here we connect the "destroy" event to a signal handler.  
        # This event occurs when we call gtk_widget_destroy() on the window,
        # or if we return FALSE in the "delete_event" callback.
        self.window.connect("destroy", self.cbk_destroy)

        self.window.show()

    def add(self, widget):
        self.window.add(widget)

    def cbk_delete_event(self, widget, event, data=None):
        # If you return FALSE in the "delete_event" signal handler,
        # GTK will emit the "destroy" signal. Returning TRUE means
        # you don't want the window to be destroyed.
        # This is useful for popping up 'are you sure you want to quit?'
        # type dialogs.
        print "TOP LEVEL delete event occurred --> TODO widget.?"

    def cbk_destroy(self, widget, data=None):
        print "destroy signal occurred"
        gtk.main_quit()
        
    def main(self):
        gtk.main()

print __name__
if __name__ == "__main__":
    hello_button = HelloButton("truc") 
    base = Base()
    base.add( hello_button.hello_button )
    base.main()
