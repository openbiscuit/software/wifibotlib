#!/usr/bin/env python

import sys
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)

class HellowWorldGTK:
    """This is an Hello World GTK application"""

    def __init__(self):
        #Set the Glade file
        self.gladefile = "hello_world.glade"  
        self.wTree = gtk.glade.XML(self.gladefile) 
		
        #Create our dictionay and connect it
        dic = { "on_hello_button_clicked" : self.hello_button_clicked,
                "on_mainWindow_delete_event" : self.mainWindow_delete_event,
                "on_mainWindow_destroy" : self.cbk_destroy }
        self.wTree.signal_autoconnect(dic)

        #Get the Main Window, and connect the "destroy" event
        #self.window = self.wTree.get_widget("MainWindow")
        #if (self.window):
        #    self.window.connect("destroy", cbk_destroy)

    def hello_button_clicked(self, widget):
	print "Hello World!"
    
    def mainWindow_delete_event(self, widget, event):
        # If you return FALSE in the "delete_event" signal handler,
        # GTK will emit the "destroy" signal. Returning TRUE means
        # you don't want the window to be destroyed.
        # This is useful for popping up 'are you sure you want to quit?'
        # type dialogs.
        print "TOP LEVEL delete event occurred --> TODO widget.?"

    def cbk_destroy(self, widget, data=None):
        print "destroy signal occurred"
        gtk.main_quit()

if __name__ == "__main__":
	hwg = HellowWorldGTK()
	gtk.main()

