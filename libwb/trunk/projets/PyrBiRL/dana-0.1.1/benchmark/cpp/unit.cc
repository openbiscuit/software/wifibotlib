
/*
 * Benchmark -- Dana benchmark objects
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unit.h"

using namespace dana::benchmark;
using namespace std;

Unit::Unit() : dana::core::Unit()
{}

Unit::Unit(const Unit &other) : dana::core::Unit(other)
{}

Unit::~Unit()
{}

Unit *
Unit::clone(void) const
{
    return new Unit(*this);
}

float
Unit::compute_dp (float dt)
{
    float input = get_input ("input");
    potential -= dt * (potential - input/9.0);
}
