/*
 * Benchmark -- Dana benchmark objects
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DANA_BENCHMARK_UNIT_H__
#define __DANA_BENCHMARK_UNIT_H__

#include "../../cpp/unit.h"

namespace dana {
    namespace benchmark {
        typedef boost::shared_ptr <class Unit> UnitPtr;
        class Unit: public dana::core::Unit {
        public:
            Unit();
            Unit(const Unit &other);
            virtual ~Unit(void);
            virtual Unit *clone (void) const;
            float compute_dp (float dt);
        };
    } // namespace benchmark
} //namespace dana

#endif
