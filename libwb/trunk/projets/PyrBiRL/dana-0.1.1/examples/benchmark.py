#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" DANA benchmarks """

import time
import timeit
from pylab import *
import random, numpy
import dana.core as core
import dana.benchmark as benchmark

equation = core.Equation()
equation.variables  = "dt,V,input"
equation.definition = "(input/9-V)*dt"

class Unit (core.Unit):
    def __init__(self):
        core.Unit.__init__(self)
    def compute_dp (self, dt):
        self.potential += (self.get_input('input')/9 - self.potential)*dt
    def compute_dw (self, dt):
        pass

def connect (group, size):
    for i in range(1,size-1):
        for j in range(1,size-1):
            for dx in [-1,0,+1]:
                for dy in [-1,0,+1]:
                    group[j*size+i].connect (group[i+dx+(j+dy)*size], 1.0, 'input')


def test1(size, iterations):
    print "---------------------------------------"
    print "Test 1: %d units for %d iterations" % (size*size, iterations)
    print "---------------------------------------"
    group1 = size*size*[Unit()]
    connect (group1, size)
    t = time.time()
    for i in range(iterations):
        for unit in group1:
            unit.compute_dp(.01)
            unit.compute_dw(.01)
    print "pure python unit: %.3f second(s)" % (time.time() - t)

    group2 = core.Unit()*[size,size]
    group2.equation = equation
    connect (group2, size)
    t = time.time()
    for i in range(iterations):
        for unit in group2:
            unit.compute_dp(.01)
            unit.compute_dw(.01)
    print "dana.core unit:   %.3f second(s)" % (time.time() - t)
    print "---------------------------------------"
    print

def test2(size, iterations):
    print "---------------------------------------"
    print "Test 2: %d units for %d iterations" % (size*size, iterations)
    print "---------------------------------------"
    group1 = size*size*core.Unit()
    group1.equation = equation
    connect (group1, size)
    t = time.time()
    for i in range(iterations):
        for unit in group1:
            unit.compute_dp(.01)
            unit.compute_dw(.01)
    print "dana.core unit:      %.3f second(s)" % (time.time() - t)

    group2 = size*size*benchmark.Unit()
    connect (group2, size)
    t = time.time()
    for i in range(iterations):
        for unit in group2:
            unit.compute_dp(.01)
            unit.compute_dw(.01)
    print "dana.benchmark unit: %.3f second(s)" % (time.time() - t)
    print "---------------------------------------"
    print

def test3(size, iterations):
    print "---------------------------------------"
    print "Test 3: %d units for %d iterations" % (size*size, iterations)
    print "---------------------------------------"
    group1 = size*size*core.Unit()
    group1.equation = equation
    connect (group1, size)
    t = time.time()
    for i in range(iterations):
        group1.compute_dp(.01)
        group1.compute_dw(.01)
    print "dana.core unit:      %.3f second(s)" % (time.time() - t)

    group2 = size*size*benchmark.Unit()
    connect (group2, size)
    t = time.time()
    for i in range(iterations):
        group2.compute_dp(.01)
        group2.compute_dw(.01)
    print "dana.benchmark unit: %.3f second(s)" % (time.time() - t)
    print "---------------------------------------"
    print

def test4(size, iterations):
    print "---------------------------------------"
    print "Test 4: %d units for %d iterations" % (size*size, iterations)
    print "---------------------------------------"
    group1 = size*size*core.Unit()
    group1.equation = equation
    connect (group1, size)
    t = time.time()
    group1.run(1, 1/float(iterations))
    print "dana.core unit:      %.3f second(s)" % (time.time() - t)

    group2 = size*size*benchmark.Unit()
    connect (group2, size)
    t = time.time()
    group2.run(1, 1/float(iterations))
    print "dana.benchmark unit: %.3f second(s)" % (time.time()-t)
    print "---------------------------------------"
    print

def test5(size):
    print "---------------------------------------"
    print "Test 5: Speedup factor"
    print "---------------------------------------"

    group1 = size*size*[Unit()]
    connect (group1, size)
    t = time.time()
    for i in range(10):
        for unit in group1:
            unit.compute_dp(.01)
            unit.compute_dw(.01)
    t1 = time.time()-t

    group2 = size*size*benchmark.Unit()
    connect (group2, size)
    t = time.time()
    group2.run(1, 1/float(100000))
    t2 = time.time()-t

    print "Speedup factor: x %d" % int((t1/10.0) / (t2/100000.0))
    print "---------------------------------------"
    print

test1(50,10)
test2(50,250)
test3(50,3000)
test4(50,5000)
test5(50)
