#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

from pylab import *
from dana import *

def gaussian(height, center_x, center_y, width_x, width_y):
    """ Returns a gaussian function with the given parameters """

    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

def group(Z, unit=Unit()):
    """ Create a group from a 2 dimensional numpy array """
    g = unit*Z.shape
    for unit, z in zip(g, Z.flatten()):
        unit.potential = z
    return g


group1 = Unit()*[5,5]
group2 = Unit()*[10,10]
X,Y = mgrid[0:20, 0:20]
Z = gaussian(1.5, 10, 10, 3, 3)(X,Y) - gaussian(0.75, 10, 10, 10, 10)(X,Y)
group1.connect (group2, group(Z))

weights = []
for i in range(5):
    weights.append ([])
    for j in range(5):
        g = group1[4-i,j].weights(group2)
        g.name = ''
        weights[i].append (g)

view = View (weights, size=2, use_ticks=False)
show()
