#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.


from dana import *

u1 = Unit()
u1.potential = 0.0
u2 = Unit()
u2.potential = 1.0

u1.connect (u2, 1.0, 'input') 
u2.connect (u1, 1.0, 'input')

eq = Equation()
eq.variables  = "dt,potential,input"
eq.definition = "potential - (potential-input)*dt"
u1.equation = u2.equation = eq

dt = 0.1
for i in range(10):
    u1.compute_dp(dt)
    u2.compute_dp(dt)

print '%f' % u1.potential
print '%f' % u2.potential



