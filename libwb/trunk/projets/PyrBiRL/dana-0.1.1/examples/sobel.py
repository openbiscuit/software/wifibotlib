#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" Diffusion """

from pylab import *
from dana import *

input = Group(flipud(imread('./lena.png')[:,:,0]))
input.name = "Input"

filter_x = Group(zeros([input.shape[0],input.shape[1]]))
filter_x.name = "Intermediate filter x"

filter_y = Group(zeros([input.shape[0],input.shape[1]]))
filter_y.name = "Intermediate filter y"

sobel   = Group(zeros([input.shape[0],input.shape[1]]))
sobel.name = "Sobel filter"

kernel_x = array ([[-1, 0,+1], [-2, 0,+2], [-1, 0, 1]])
filter_x.connect (input, Group(kernel_x), type='input')
kernel_y = array ([[+1,+2,+1], [ 0, 0, 0], [-1,-2,-1]])
filter_y.connect (input, Group(kernel_y), type='input')
sobel.connect (filter_x, Group(ones((1,1))), type='x')
sobel.connect (filter_y, Group(ones((1,1))), type='y')

eq = Equation()
filter_x.equation = eq
eq.variables = "dt,potential,input"
eq.definition = "input"

eq = Equation()
filter_y.equation = eq
eq.variables = "dt,potential,input"
eq.definition = "input"

eq = Equation()
sobel.equation = eq
eq.variables = "dt,potential,x,y"
eq.definition = "sqrt(x*x + y*y)"

net = input + filter_x + filter_y + sobel
net.run(1,1)

view = View([[input, filter_x],
             [filter_y, sobel]], scale=[0,1], cmap=cm.gray)

savefig('sobel.png', dpi=50)
