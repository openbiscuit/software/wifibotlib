#!/usr/bin/python
#
#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

from dana import *
import math

print
print '-----------------------------------------------'
print ' "Dynamic Neural Field with Local Inhibition." '
print ' N.P. Rougier in Biological Cybernetics        '
print ' volume 94, number 3, pp 169-179, March 2006.  '
print '-----------------------------------------------'
print
print 'Starts with "ipython --pylab"'
print

def gaussian(Z, center_x, center_y, intensity, width):
    """ Returns a gaussian array with the given parameters """
    for y in range(Z.shape[0]):
        for x in range(Z.shape[1]):
            d = sqrt((center_x-x)**2 + (center_y-y)**2)
            Z[y,x] = intensity*exp (-(d*d)/(width*width))
    return Z

sz = 40
input = Unit()*[sz,sz]
input.name = 'input'

focus = Unit()*[sz,sz]
focus.name = 'focus'

eq = Equation()
focus.equation = eq
eq.constants['tau'] = 0.75
eq.constants['alpha'] = 12.5
eq.constants['baseline'] = 0.1
eq.constants['minact'] = -1
eq.constants['maxact'] =  1
eq.variables = \
    "dt,potential,input,lateral"
eq.definition = \
    "max(minact, min (maxact, potential + dt*(-potential+baseline+(1.0/alpha)*(lateral+input))/tau))"

# Input -> focus: one to one
Z = ones((1,1))
focus.connect (input, Group(Z*1.5), 'input')

# focus -> focus: DoG
s = sz
Z = gaussian(zeros([s, s]), s/2, s/2, 3.15, 2) - gaussian(zeros([s, s]), s/2, s/2, 0.9, 4)
Z[s/2,s/2] = 0
focus.connect (focus, Group(Z), 'lateral', toric=True)

net = input + focus
view = View( [input,focus] )
show()

theta = 0
def run(t,dt=1):
    global theta,sz

    for i in range (int(t/dt)): 
        #theta += 0.01*dt*pi
        x0,y0 = sz/2+cos(theta)*10,    sz/2+sin(theta)*10
        x1,y1 = sz/2+cos(theta+pi)*10, sz/2+sin(theta+pi)*10
        input.potentials = gaussian(zeros([sz, sz]), x0, y0, 1, 4) \
                           + gaussian(zeros([sz, sz]), x1, y1, 1, 4) \
                           + uniform(0,.1, [sz,sz])
        net.run(dt,dt)
        view.update()
