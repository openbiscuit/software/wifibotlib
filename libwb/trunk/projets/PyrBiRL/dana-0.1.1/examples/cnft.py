#!/usr/bin/python
#
#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

from pylab import *
from dana import *

print
print '-----------------------------------------------------'
print ' "Emergence of Attention within a Neural Population" '
print ' N.P. Rougier and J. Vitay in Neural Networks        '
print ' volume 19, number 5, pp 573-581, June 2006.         '
print '-----------------------------------------------------'
print

def gaussian(height, center_x, center_y, width_x, width_y):
    """ Returns a gaussian function with the given parameters """
    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

size = 30
input = Unit()*[size,size]
input.name = 'input'
focus = Unit()*[size,size]
focus.name = 'focus'
eq = Equation()
focus.equation = eq
eq.constants['tau'] = 2.75
eq.constants['alpha'] = 13.5
eq.constants['minact'] = 0
eq.constants['maxact'] = 1
eq.variables = \
    "dt,potential,input,lateral"
eq.definition = \
    "max(minact, min (maxact, potential + dt*(-potential+(1.0/alpha)*(lateral+input))/tau))"

# Input -> focus: one to one
Z = ones((1,1))
focus.connect (input, Group(Z), 'input')

# focus -> focus: DoG
X,Y = mgrid[0:2*size, 0:2*size]
Z = gaussian(2.2, size, size, 3, 3)(X,Y) - gaussian(0.75, size, size, 30, 30)(X,Y)
Z[size,size] = 0
focus.connect (focus, Group(Z), 'lateral')

# Network
net = input + focus

# Set input
X,Y = mgrid[0:size, 0:size]
Z = gaussian(1, 5, 5, 3, 3)(X,Y) + gaussian(1,24,24, 3, 3)(X,Y)
input.potentials = Z

view = View( [input,focus] )
show()
