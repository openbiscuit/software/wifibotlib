#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" Equations

Equations are objects that can evaluate an arbitrary expression with an
arbitrary number of variables and constants.

"""

from dana import Equation

equation = Equation()
equation.variables  = "x,y"
equation.constants["a"] = 2.0
equation.definition = "a*x*y"

print equation.evaluate ([2,2])
print equation.evaluate ([4,4])

