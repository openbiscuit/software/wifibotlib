#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" """

from pylab import *
from dana import *

grid = Unit()*[64,64]
grid.name = ''
grid.equation = Equation()
grid.equation.variables = "dt,potential,neighbours"
grid.equation.definition = "potential - dt*(potential-neighbours/8)"

Z = ones((3,3))
Z[1,1] = 0
grid.connect (grid, Group(Z), 'neighbours', noise=0.0, toric=True)
grid.potentials = randint(0,2, (grid.shape[0],grid.shape[1]))

Z0 = Group(grid.potentials)
Z0.name="Initial state"
grid.run(2,.01)
Z1 = Group(grid.potentials)
Z1.name="Final state (2 seconds)"

view = View([Z0,Z1])
savefig('diffusion.png', dpi=50)

