#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" Unit

Units are objects that possess a potential and can be evaluated using an
equation and links.

"""

from dana import Unit

unit = Unit()
print 'Potential:   ', unit.potential
print 'Name:        ', unit.name
print 'Equation:    ', unit.equation
print 'Links   :    ', unit.links
print
print 'UID:         ',  unit.uid
print 'C Type:      ', unit.__type__
print 'Python Type: ', unit.__class__
print 'Ref. count:  ', unit.__count__

