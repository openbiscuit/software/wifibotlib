#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" Game of Life """

from pylab import *
from dana import *

print
print '-----------------------------------------------------'
print ' "Game of Life                                       '
print ' Synchronous and Asynchronous version                '
print '-----------------------------------------------------'
print

size = 100
sync = SynUnit()*[size,size]
sync.name = 'Synchronous'
async = Unit()*[size,size]
async.name = 'ASynchronous'

net = sync + async

net.equation = Equation()
net.equation.variables = \
    "dt,potential,neighbours"
net.equation.definition = \
    "min(1, if(neighbours-2,0,1)*potential + if(neighbours-3,0,1))"

Z = ones((3,3))
Z[1,1] = 0
sync.connect (sync, Group(Z), 'neighbours', toric=True)
async.connect (async, Group(Z), 'neighbours', toric=True)
sync.potentials = randint (0,2, [size,size])
async.potentials = sync.potentials

view = View([sync, async], scale=[-1,1])
show()

def run (t, dt=.1):
    for i in range(int(t/dt)):
        net.run(dt,dt)
        view.update()
