#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" DANA Tutorial 7 """

from pylab import *
import numpy
from dana import *


def gaussian(height, center_x, center_y, width_x, width_y):
    """ Returns a gaussian function with the given parameters """

    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

def make (size, Z, noise=0.0):
    g = Unit()*[size,size]
    g.connect(g, Group(Z), noise=noise)
    Z = numpy.array(g[size/2*(1+size)].weights (g), dtype=float)
    Z.resize(size,size)
    return Group(Z)

size = 20

Z_point = make (size, ones ([1,1]))
Z_point.name = "One to One"

Z_line = make (size,  ones ([1, 2*size]))
Z_line.name = "Line"

Z_column = make (size, ones ([2*size, 1]))
Z_column.name = "Column"

Z_noisy = make (size, ones((2*size, 2*size)) / 2.0, .25)
Z_noisy.name = "Noisy"

X,Y = mgrid[0:2*size, 0:2*size]
Z = gaussian(1, size, size, 3, 3)(X,Y)
Z[size,size] = 0
Z_gaussian = make (size, Z, 0)
Z_gaussian.name = "Gaussian"

X,Y = mgrid[0:2*size, 0:2*size]
Z = gaussian(2.0, size, size, 3, 3)(X,Y) - gaussian(0.75, size, size, 20, 20)(X,Y)
Z[size,size] = 0
Z_dog = make (size, Z, 0)
Z_dog.name = "Difference of Gaussians"


View ([[Z_point, Z_line, Z_column], [Z_noisy, Z_gaussian, Z_dog]])
show()


#make ("constant.png", Box(1,1),     Euclidean(), Constant(1),           Full())
#make ("point.png",    Point(),      Euclidean(), Constant(1),           Full())
#make ("line.png",     Box(1,0),     Euclidean(), Constant(1),           Full())
#make ("column.png",   Box(0,1),     Euclidean(), Constant(1),           Full())
#make ("box.png",      Box(.25,.25), Euclidean(), Constant(1),           Full())
#make ("disc.png",     Disc(.25),    Euclidean(), Constant(1),           Full())
#make ("linear.png",   Box(1,1),     Euclidean(), Linear(0,1),           Full())
#make ("uniform.png",  Box(1,1),     Euclidean(), Uniform(0,1),          Full())
#make ("gaussian.png", Box(1,1),     Euclidean(), Gaussian(1,.5),        Full())
#make ("dog.png",      Box(1,1),     Euclidean(), DoG(2.2,0.15,.75,1.0), Full())
#make ("manhattan.png",Box(1,1),     Manhattan(), Gaussian(1,.5),        Full())
#make ("max.png",      Box(1,1),     Max(),       Gaussian(1,.5),        Full())
#make ("sparse.png",   Box(1,1),     Euclidean(), Constant(1),           Sparse(.25))
#make ("sparser.png",  Box(1,1),     Euclidean(), Constant(1),           Sparser(.5))


