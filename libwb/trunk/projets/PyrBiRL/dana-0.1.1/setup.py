#!/usr/bin/env python
#
#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

from tools import ez_setup
ez_setup.use_setuptools()

import os
import sys
from glob  import glob
from setuptools import setup, Extension
from distutils import sysconfig
import commands


def pkgconfig (packages, flags="cflags"):
    """ Simple and dirty wrap-up on pkg-config """
    pkgs = ' '.join(packages)
    status, output = commands.getstatusoutput (
        'pkg-config --silence-errors --%s %s' % (flags, pkgs))
    if status != 0:
        print "Package %s was not found" % pkgs
        return []
    return [i[2:] for i in output.split()]


long_description = '''
DANA is a library that support distributed, asynchronous, numerical and
adaptive computation which is closely related to both the notion of artificial
neural networks and cellular automaton.

From a conceptual point of view, the computational paradigm supporting the
library is grounded on the notion of a unit that is essentially a potential
that can vary along time under the influence of other units and learning. Those
units are organized into groups and network: a network is made of one to
several groups and a group is made of one to several units. Furthermore, each
unit can be linked to any other unit (included itself) using a weighted link.
'''

requires = ['numpy>=1.1', 'matplotlib>=0.98']

sysconfig.get_config_vars()['OPT'] = ""

runtime_library_dirs = []
pyversion = 'python%s.%s' % (sys.version_info[0],sys.version_info[1])
runtime_library_dirs.append (os.path.abspath ('/usr/lib/%s/site-packages' % pyversion))
runtime_library_dirs.append (os.path.abspath ('/usr/local/lib/%s/site-packages' % pyversion))
runtime_library_dirs.append (os.path.abspath ('/home/local/lib/%s/site-packages' % pyversion))
runtime_library_dirs.append (os.path.abspath ('lib'))

setup(name="dana",
      version="0.1.1",
      url='http://www.loria.fr/~rougier/dana/',
      download_url='http://www.loria.fr/~rougier/dana/',
      license='GPL',
      author='Nicolas Rougier',
      author_email='Nicolas.Rougier@loria.fr',
      description='Distributed Numerical Computing',
      long_description=long_description,
      zip_safe=False,
      classifiers=[
              'Development Status :: 4 - Beta',
              'Environment :: Console',
              'Environment :: Web Environment',
              'Intended Audience :: Science/Research',
              'License :: OSI Approved :: GNU General Public License (GPL)',
              'Operating System :: OS Independent',
              'Programming Language :: C++',
              'Programming Language :: Python',
              'Topic :: Scientific/Engineering',
              ],
      platforms='any',
      packages=['dana',
                'dana.core',
                'dana.core.tests',
                'dana.benchmark',
                'dana.pylab'],
      package_dir={'dana': '.',
                   'dana.core': 'python',
                   'dana.benchmark': 'benchmark/python',
                   'dana.pylab': 'pylab'},
      ext_modules=[
          Extension('libdana_core', glob ('cpp/*.cc'),
                    include_dirs=[] + pkgconfig(['libxml-2.0']),
                    library_dirs=['lib'],
                    runtime_library_dirs=runtime_library_dirs,
                    libraries=['xml2']),
          Extension('dana.core._core', glob ('python/*.cc'),
                    include_dirs=[] + pkgconfig(['libxml-2.0']),
                    library_dirs=['lib'],
                    runtime_library_dirs=runtime_library_dirs,
                    libraries=['boost_python-mt', 'dana_core']),
          Extension('libdana_benchmark', glob ('benchmark/cpp/*.cc'),
                    include_dirs=[] + pkgconfig(['libxml-2.0']),
                    library_dirs=['lib'],
                    runtime_library_dirs=runtime_library_dirs,
                    libraries=['xml2', 'dana_core']),
          Extension('dana.benchmark._benchmark', glob ('benchmark/python/*.cc'),
                    include_dirs=[] + pkgconfig(['libxml-2.0']),
                    library_dirs=['lib'],
                    runtime_library_dirs=runtime_library_dirs,
                    libraries=['boost_python-mt', 'dana_core', 'dana_benchmark']),
          ],
      headers=glob('cpp/*.h'),
      install_requires=requires,
)
