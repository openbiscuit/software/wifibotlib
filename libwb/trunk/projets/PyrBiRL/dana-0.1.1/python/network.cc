/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "network.h"


// ------------------------------------------------------------ Network_get_item
ObjectPtr
Network_get_item (NetworkPtr net, object key) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    if (_type == "int") {
        int index = extract<int>(key)();
        return net->get_item(index);
    } else if (_type == "slice") {
        object indices = key.attr("indices")(net->size());
        int start = extract< int >(indices[0])();
        int stop = extract<int>(indices[1])();
        int step = extract<int>(indices[2])();
        return net->get_slice (start, stop, step);    
    }
    throw std::out_of_range("Key must be an integer");
}


// ------------------------------------------------------------ Network_del_item
void
Network_del_item (NetworkPtr net, object key) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    if (_type == "int") {
        int index = extract<int>(key)();
        net->del_item(index);
        return;
    } else if (_type == "slice") {
        object indices = key.attr("indices")(net->size());
        int start = extract< int >(indices[0])();
        int stop = extract<int>(indices[1])();
        int step = extract<int>(indices[2])();
        net->del_slice (start, stop, step);
        return;
    }
    throw std::out_of_range("Key must be an integer");
}


// ------------------------------------------------------------ Network_set_item
void
Network_set_item (NetworkPtr net, object key, object item) {
    object key_class = key.attr("__class__");
    object key_name = key_class.attr("__name__");
    std::string key_type = extract<std::string>(key_name)();

    if (key_type == "int") {
        int index = extract<int>(key)();
        GroupPtr group = extract<GroupPtr>(item)();
        net->set_item(index, group);
        return;
    } else if (key_type == "slice") {
        object indices = key.attr("indices")(net->size());
        NetworkPtr other = extract<NetworkPtr>(item)();
        int start = extract< int >(indices[0])();
        int stop = extract<int>(indices[1])();
        int step = extract<int>(indices[2])();
        net->set_slice (start, stop, step, other);
        return;
    }
    throw std::out_of_range("Key must be an integer");
}


// ---------------------------------------------------------------- Group_export
void Network_export  (void)
{
    register_ptr_to_python <NetworkPtr> ();
     class_<Network, bases <Object> > 
        ("Network",
         "A Network is a set of groups                                      \n"
         "                                                                  \n")

         .def("__init__",
              make_constructor (Object::make<Network>),
              "Create a new Network                                         \n"
              "                                                             \n"
              "__init__(self) -> new Network                                \n"
              "                                                             \n")

         .def("run", &Network::run,
              "Compute dp & dw for given duration                           \n"
              "                                                             \n"
              "run(t, dt)                                                   \n"
              "  t  -- Time to run                                          \n"
              "  dt -- Elementary time step                                 \n"
              "                                                             \n")

         .def("compute_dp", &Network::compute_dp,
              "Compute new potential and return difference                  \n"
              "                                                             \n"
              "compute_dp(self, dt) -> dp                                   \n"
              "  dt -- elementaty time step                                 \n"
              "  dp -- difference of potential between t and t+dt           \n"
              "                                                             \n")


         .def("compute_dw", &Network::compute_dw,
              "Compute new weights and return difference                    \n"
              "                                                             \n"
              "compute_dw(self, dt) -> dw                                   \n"
              "  dt -- elementaty time step                                 \n"
              "  dw -- difference of weights between t and t+dt             \n"
              "                                                             \n")

         .def("size",
              &Network::size,
              "Return network size                                          \n"
              "                                                             \n"
              "len(self) -> int                                             \n"
              "  int -- network size                                        \n"
              "                                                             \n")

         .def ("__len__",
               &Network::size,
              "Return network size                                          \n"
              "                                                             \n"
              "len(self) -> int                                             \n"
              "  int -- network size                                        \n"
              "                                                             \n")

         .def ("__getitem__",
               Network_get_item,
               "x.__getitem__ (y) <==> x[y]")

         .def ("__delitem__",
               Network_del_item,
               "x.__delitem__ (y) <==> del x[y]")
         
         .def ("__setitem__",
               &Network_set_item,
               "x.__setitem__(i, y) <==> x[i]=y")

         .def ("__contains__",
               &Network::contains,
               "x.__contains__(y) <==> y in x")
        

         .add_property ("equation",
                        &Network::get_equation, &Network::set_equation,
                        "Network wide equation")

         .def(self  +  NetworkPtr()) // __add__
         .def(self  += NetworkPtr()) // __iadd__
         .def(self  +  GroupPtr())   // __add__
         .def(self  += GroupPtr())   // __iadd__
        ;
}
