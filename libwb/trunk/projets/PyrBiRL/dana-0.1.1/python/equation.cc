/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "equation.h"

using namespace boost::python;

float
Equation_evaluate (Equation &self, list values)
{
    unsigned int size = self.variables.size();
    for (unsigned int i=0; i<size; i++)
        self.values[i] = extract<float>(values[i])();
    return self.evaluate(self.values);
}

void Equation_export  (void)
{
    register_ptr_to_python <EquationPtr> ();
    class_<Equation, bases <Object> > 
        ("Equation",
         "Equation is used for fast computation.\n")

         .def("__init__",
              make_constructor (Object::make<Equation>),
              "Create a new Equation                                        \n"
              "                                                             \n"
              "__init__(self) -> new Equation                               \n"
              "                                                             \n")

        .def ("evaluate",
              Equation_evaluate,
              "Evaluate equation                                            \n"
              "                                                             \n"
              "evaluate(self, values)  -> result                            \n"
              "  values -- A list of values                                 \n"
              "  result -- Equation evaluation                              \n"
              "                                                             \n")

        .add_property ("definition",
                       &Equation::get_definition,
                       &Equation::set_definition,
                       "Equation definition")

        .add_property ("variables",
                       &Equation::get_variables,
                       &Equation::set_variables,
                       "Equation variables")

        .add_property ("constants",
                       &Equation::get_constants,
                       &Equation::set_constants,
                       "Equation constants")
        ;
}
