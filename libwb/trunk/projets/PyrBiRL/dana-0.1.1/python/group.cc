/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/python/numeric.hpp>
#include <boost/python/tuple.hpp>
#include <numpy/arrayobject.h>
#include "group.h"


// -------------------------------------------------------------- Group_get_item
ObjectPtr
Group_get_item (GroupPtr self, object key) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();

    if (_type == "int") {
        int index = extract<int>(key)();
        return self->get_item(index);

    } else if ((_type == "tuple") or (_type == "list")) {
        int y = extract<int>(key[0])();
        int x = extract<int>(key[1])();
        return self->get_item(y,x);

    } else if (_type == "slice") {
        object indices = key.attr("indices")(self->size());
        int start = extract< int >(indices[0])();
        int stop = extract<int>(indices[1])();
        int step = extract<int>(indices[2])();
        return self->get_slice (start, stop, step);    
    }
    throw std::out_of_range("Key must be an integer or a slice");
}


// -------------------------------------------------------------- Group_del_item
void
Group_del_item (GroupPtr self, object key) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();

    if (_type == "int") {
        int index = extract<int>(key)();
        self->del_item(index);
        return;

    } else if ((_type == "tuple") or (_type == "list")) {
        int x = extract<int>(key[0])();
        int y = extract<int>(key[1])();
        self->del_item(x+y*self->width);

    } else if (_type == "slice") {
        object indices = key.attr("indices")(self->size());
        int start = extract< int >(indices[0])();
        int stop = extract<int>(indices[1])();
        int step = extract<int>(indices[2])();
        self->del_slice (start, stop, step);
        return;
    }
    throw std::out_of_range("Key must be an integer or a slice");
}


// -------------------------------------------------------------- Group_set_item
void
Group_set_item (GroupPtr self, object key, object item) {
    object _class = key.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();

    if (_type == "int") {
        int index = extract<int>(key)();
        UnitPtr unit = extract<UnitPtr>(item)();
        self->set_item(index, unit);
        return;

    } else if ((_type == "tuple") or (_type == "list")) {
        int x = extract<int>(key[0])();
        int y = extract<int>(key[1])();
        UnitPtr unit = extract<UnitPtr>(item)();
        self->set_item(x+y*self->width, unit);

    } else if (_type == "slice") {
        object indices = key.attr("indices")(self->size());
        GroupPtr other = extract<GroupPtr>(item)();
        int start = extract< int >(indices[0])();
        int stop = extract<int>(indices[1])();
        int step = extract<int>(indices[2])();
        self->set_slice (start, stop, step, other);
        return;
    }
    throw std::out_of_range("Key must be an integer or a slice");
}

// ------------------------------------------------------------- Group_set_shape
void
Group_set_shape (GroupPtr self, tuple shape)
{
    int h = extract<int>(shape[0])();
    int w = extract<int>(shape[1])();
    self->set_shape (h,w);
}

// ------------------------------------------------------------- Group_get_shape
tuple
Group_get_shape (GroupPtr self)
{
    return make_tuple (self->height, self->width);
}

// ------------------------------------------------------------------- Group_add
NetworkPtr
Group_add (Group &self, GroupPtr other)
{
    return self + other;
}

// -------------------------------------------------------- Group_get_potentials
boost::python::object
Group_get_potentials (GroupPtr self)
{
    npy_intp dims[2] = {self->height, self->width};
    float *data = self->get_potentials();
    boost::python::object
        obj(boost::python::handle<>
            (PyArray_FromDimsAndData (2, dims, PyArray_FLOAT, (char *) data)));
    return boost::python::extract<numeric::array>(obj);
}

// -------------------------------------------------------- Group_set_potentials
void
Group_set_potentials (GroupPtr self, numeric::array obj)
{
    object _class = obj.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    if (_type != "ndarray")
        throw std::invalid_argument
            ("Potentials must be a 2D numpy.array of same size and of type real");
    PyArrayObject *array = (PyArrayObject *) obj.ptr();
    if (array == 0)
        throw std::invalid_argument
            ("Potentials must be a 2D numpy.array of same size and of type real");
    int nd = array->nd;
    if (nd == 2) {
        unsigned int width = array->dimensions[1];
        unsigned int height= array->dimensions[0];
        if (width*height == self->width*self->height) {
            if (array->descr->type_num == PyArray_DOUBLE) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(double *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_FLOAT) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++) {
                    for (unsigned int j=0; j< height; j++) {
                        self->units[i*height+j]->potential =
                            *(float *)(data+i*array->strides[0]+j*array->strides[1]);
                    }
                }
                return;
            } else if (array->descr->type_num == PyArray_BYTE) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(char *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_UBYTE) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(unsigned char *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_INT) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(int *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_UINT) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(unsigned int *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_SHORT) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(short *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_USHORT) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(unsigned short *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_LONG) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(long *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            } else if (array->descr->type_num == PyArray_ULONG) {
                char *data = (char *) array->data;
                for (unsigned int i=0; i< width; i++)
                    for (unsigned int j=0; j< height; j++)
                        self->units[i*height+j]->potential =
                            *(unsigned long *)(data+i*array->strides[0]+j*array->strides[1]);
                return;
            }
        }
    }
    throw std::invalid_argument
        ("Potentials must be a 2D numpy.array of same size and of type real");
}

// ------------------------------------------------------------ Group_from_array
GroupPtr
Group_from_array_1 (numeric::array obj, const Unit &unit = Unit())
{
    GroupPtr self;
    object _class = obj.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    if (_type != "ndarray")
        throw std::invalid_argument
            ("first argument must be a 2D numpy.array of type real");
    PyArrayObject *array = (PyArrayObject *) obj.ptr();
    if (array == 0)
        throw std::invalid_argument
            ("first argument must be a 2D numpy.array of type real");
    int nd = array->nd;
    if (nd == 2) {
        unsigned int width = array->dimensions[1];
        unsigned int height= array->dimensions[0];
        self = GroupPtr (new Group());
        self->units.clear();
        for (unsigned int i=0; i< (width*height); i++)
            self->units.push_back (UnitPtr (unit.clone()));
        self->set_shape (height, width);
        Group_set_potentials (self, obj);
        return self;
    }
    throw std::invalid_argument
        ("first argument must be a 2D numpy.array of type real");
}
GroupPtr
Group_from_array_2 (numeric::array obj)
{
    return Group_from_array_1 (obj);
}




// ---------------------------------------------------------------- Group_export
void Group_export (void)
{
    import_array();
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");

    register_ptr_to_python <GroupPtr> ();
     class_<Group, bases <Object> > 
        ("Group",
         "A Group is a matrix of units.                                     \n"
         "                                                                  \n")

         .def("__init__",
              make_constructor (Object::make<Group>),
              "Create a new Group                                           \n"
              "                                                             \n"
              "__init__(self)                                               \n"
              "__init__(self, array)                                        \n"
              "__init__(self, array, unit)                                  \n"
              "                                                             \n")

         .def("__init__", make_constructor (Group_from_array_1))
         .def("__init__", make_constructor (Group_from_array_2))

         .def("run", &Group::run,
              "Compute dp & dw for given duration                           \n"
              "                                                             \n"
              "run(t, dt)                                                   \n"
              "  t  -- Time to run                                          \n"
              "  dt -- Elementary time step                                 \n"
              "                                                             \n")

         .def("compute_dp", &Group::compute_dp,
              "Compute new potential and return difference                  \n"
              "                                                             \n"
              "compute_dp(self, dt) -> dp                                   \n"
              "  dt -- elementaty time step                                 \n"
              "  dp -- difference of potential between t and t+dt           \n"
              "                                                             \n")


         .def("compute_dw", &Group::compute_dw,
              "Compute new weights and return difference                    \n"
              "                                                             \n"
              "compute_dw(self, dt) -> dw                                   \n"
              "  dt -- elementaty time step                                 \n"
              "  dw -- difference of weights between t and t+dt             \n"
              "                                                             \n")

         .def("connect",
              &Group::connect,
              "connect(source, profile, type, noise, toric)                \n"
              "  source   -- Source group                                  \n"
              "  profile  -- Weights profile as group                      \n"
              "  type     -- Link type (arbritrary string)                 \n"
              "  noise    -- Noise level                                   \n"
              "  probability -- Connection probability                     \n"
              "  toric    -- Toric                                         \n"
              "                                                            \n",
              (arg("source"), arg("profile"), arg("type") = "default",
               arg("noise") = 0.0, arg("probability")=1.0,
               arg("toric") = false))

         .def("__len__",
              &Group::size,
              "Return group size                                            \n"
              "                                                             \n"
              "len(self) -> int                                             \n"
              "  int -- group size                                          \n"
              "                                                             \n")

         .def("__getitem__",
              Group_get_item,
              "x.__getitem__ (y) <==> x[y]")

         .def("__delitem__",
              Group_del_item,
              "x.__delitem__ (y) <==> del x[y]")
         
         .def("__setitem__",
              &Group_set_item,
              "x.__setitem__(i, y) <==> x[i]=y")

         .def("__contains__",
              &Group::contains,
              "x.__contains__(y) <==> y in x")

         .def("__add__", Group_add,
              "Create a network with current group and other                \n"
              "                                                             \n"
              "__add__(self, other) -> net                                  \n"
              "  other -- group to be added in network                      \n"
              "  net   -- newly created net                                 \n"
              "                                                             \n")

         .add_property("potentials",
                       Group_get_potentials, Group_set_potentials,
                       "Group potentials as a numpy.array")
         .add_property("size",
                       &Group::size,
                       "Number of units")
         .add_property("equation",
                       &Group::get_equation, &Group::set_equation,
                       "Group wide equation")
         .add_property("shape",
                       &Group_get_shape, &Group_set_shape,
                       "Group shape")
        ;
}
