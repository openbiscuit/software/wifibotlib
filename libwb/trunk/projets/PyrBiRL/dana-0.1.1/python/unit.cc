/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <boost/python/operators.hpp>
#include "unit.h"
#include "group.h"

// ------------------------------------------------------------------- operator*
GroupPtr
Unit_mul_shape (UnitPtr unit, const object &shape)
{
    object _class = shape.attr("__class__");
    object _name = _class.attr("__name__");
    std::string _type = extract<std::string>(_name)();
    if ((_type == "tuple") or (_type == "list")) {
        int w = 1, h = 1;
        int size = extract< int > (shape.attr("__len__")());
        if (size < 1) 
            throw std::out_of_range
                ("Shape must be an integer or a tuple of 1 or 2 integers");       
        else 
            w = extract<int>(shape[0])();
        if (size > 1) 
            h = extract<int>(shape[1])();
        if ((w < 1) or (h< 1))
            throw std::out_of_range("Shape dimensions must be positive");

        GroupPtr group = fabs(w*h)*unit;
        group->set_shape (w,h);
        return group;
    }
    throw std::out_of_range
        ("Shape must be an integer or a tuple of 1 or 2 integers");
}

GroupPtr Unit_rmul (UnitPtr self, int n)
{
    GroupPtr group = n*self;
    return group;
}


void Unit_connect_Unit (UnitPtr self, UnitPtr source, float weight=0.0,
                        std::string type="default")
{
    self->connect (source, weight, type);
}

void Unit_connect_Group (UnitPtr self, GroupPtr source, GroupPtr profile,
                         std::string type="default", float noise=0.0f,
                         float probability = 1.0f, bool toric=false)
{
    self->connect(source, profile, type, noise, probability, toric);
}

void Unit_export  (void)
{
    register_ptr_to_python <UnitPtr> ();
    register_ptr_to_python <LinkPtr> ();
    register_ptr_to_python <LinkVecPtr> ();
    register_ptr_to_python <LinkMapPtr> ();

    class_<LinkVec >("LinkVec")
        .def(vector_indexing_suite <LinkVec, true >())        
        .def ("__repr__", &LinkVec::repr)
        ;

    class_<LinkMap >("LinkMap")
        .def(map_indexing_suite <LinkMap, true >())
        .def ("__repr__", &LinkMap::repr)
        ;
 
    class_<Unit, bases <Object> > 
        ("Unit",
         "A Unit is essentially a potential that can vary along time under the\n"
         "influence of other units and learning.                              \n"
         "                                                                    \n")

        .def("__init__",
             make_constructor (Object::make<Unit>),
             "Create a new Unit                                               \n"
             "                                                                \n"
             "__init__(self) -> new Unit                                      \n"
             "                                                                \n")

        .def("compute_dp", &Unit::compute_dp,
             "Compute new potential and return difference                     \n"
             "                                                                \n"
             "compute_dp(self, dt) -> dp                                      \n"
             "  dt -- elementaty time step                                    \n"
             "  dp -- difference of potential between t and t+dt              \n"
             "                                                                \n")
        
        .def("compute_dw", &Unit::compute_dw,
             "Compute new weights and return difference                       \n"
             "                                                                \n"
             "compute_dw(self, dt) -> dw                                      \n"
             "  dt -- elementaty time step                                    \n"
             "  dw -- difference of weights between t and t+dt                \n"
             "                                                                \n")

        .def("connect",
             Unit_connect_Unit,
             "Connect self to source unit or group                           \n"
             "                                                               \n"
             "connect(self, source, weight, type)                            \n"
             "  source -- source unit                                        \n"
             "  weight -- link weight (scalar)                               \n"
             "  type   -- link type (arbitrary string, default is 'default')  ",
             (arg("source"), arg("weight")=0.0, arg("type")="default"))

        .def("connect",
             Unit_connect_Group,
             "connect(self, source, profile, type, noise, toric)             \n"
             "  source  -- source group                                      \n"
             "  profile -- profile as a group of weights (= potentials)      \n"
             "  type    -- link type (arbitrary string, default is 'default')\n"
             "  noise   -- noise level                                       \n"
             "  probability -- connection probability                        \n"
             "  toric   -- whether profile is toric                          \n"
             "                                                               \n",
             (arg("source"),arg("profile"), arg("type")="default",
              arg("noise")=0.0, arg("proabability") = 1.0, arg("toric")=false))

        .def("get_input",
             &Unit::get_input,
             "Get contribution of a given type of input                       \n"
             "                                                                \n"
             "get_input(self, type) -> float                                  \n"
             "  type  -- link type (arbitrary string, default is 'default')   \n"
             "  float -- overall contribution of 'type' links                 \n"
             "                                                                \n")

        .def("weights",
             &Unit::weights,
             "Get weights from a source group                                 \n"
             "                                                                \n"
             "weights(self, source) -> group                                  \n"
             "  source -- source group to consider                            \n"
             "  group -- group representing weights                           \n"
             "                                                                \n")

        // .def(int() * self)
        // .def(self + UnitPtr())
        // .def(int() * self)
        .def(self * int())

        .def("__rmul__",
             Unit_rmul,
             "Create a group of specified size                                \n"
             "                                                                \n"
             "__rmul__(self, n) -> group                                      \n"
             "  n -- a size                                                   \n"
             "  group -- a new group with shape (n,1)                         \n"
             "                                                                \n")


        .def("__mul__",
             Unit_mul_shape,
             "Create a group of specified shape                               \n"
             "                                                                \n"
             "__mul__(self, shape) -> group                                   \n"
             "  shape -- a tuple/list of 1 or 2 integers or a single integer  \n"
             "  group -- a new group with specified shape                     \n"
             "                                                                \n")

        .def("__float__",
             &Unit::get_potential,
             "Return potential                                                \n"
             "                                                                \n"
             "__float__(self) -> potential                                    \n"
             "  potential -> unit potential                                   \n"
             "                                                                \n")

        .add_property ("potential",
                       &Unit::get_potential, &Unit::set_potential,
                       "Potential")

        .add_property ("equation",
                       &Unit::get_equation, &Unit::set_equation,
                       "Equation governing potential over time")

        .def_readwrite ("links",
                        &Unit::links,
                        "Unit links to other units")

        .def_readonly("x",
                      &Unit::x,
                      "x position within group")

        .def_readonly("y",
                      &Unit::y,
                      "y position within group")
        ;

    class_<SynUnit, bases <Unit> > 
        ("SynUnit",
         "A synchronous init computes its potential in a temporary buffer   \n"
         "and is actually updated when the compute_dw method is called.     \n"
         "                                                                  \n")

        .def("__init__",
             make_constructor (Object::make<SynUnit>),
             "Create a new SynUnit                                          \n"
             "                                                              \n"
             "__init__(self) ->  new SynUnit                                \n"
             "                                                              \n")
        ;


    class_<KUnit, bases <Unit> > 
        ("KUnit",
         "A kohonen typed unit                                              \n"
         "                                                                  \n")

        .def("__init__",
             make_constructor (Object::make<KUnit>),
             "Create a new KUnit                                            \n"
             "                                                              \n"
             "__init__(self) ->  new KUnit                                  \n"
             "                                                              \n")
        ;

    class_<DeadUnit, bases <Unit> > 
        ("DeadUnit",
         "A DeadUnit represents an inactive Unit with a null potential      \n"
         "                                                                  \n")

        .def("__init__",
             make_constructor (Object::make<DeadUnit>),
             "Create a new DeadUnit                                         \n"
             "                                                              \n"
             "__init__(self) ->  new DeadUnit                               \n"
             "                                                              \n")
        ;
}
