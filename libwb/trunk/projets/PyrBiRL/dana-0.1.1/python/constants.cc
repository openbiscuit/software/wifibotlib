/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "constants.h"

void Constants_export (void)
{
    register_ptr_to_python <ConstantsPtr> ();
    class_<Constants, bases <Object> > 
        ("Constants",
         "Constants hold arbitrary real constants.                          \n"
         "                                                                  \n")

         .def("__init__",
              make_constructor (Object::make<Constants>),
              "Create a new Constants object                                \n"
              "                                                             \n"
              "__init__(self) -> new Constants                              \n"
              "                                                             \n")

         .def("size",
              &Constants::size,
              "Return number of constants                                   \n"
              "                                                             \n"
              "size(self) -> int                                            \n"
              "  int -- number of constants                                 \n"
              "                                                             \n")

         .def("__len__",
              &Constants::size,
              "Return number of constants                                   \n"
              "                                                             \n"
              "size(self) -> int                                            \n"
              "  int -- number of constants                                 \n"
              "                                                             \n")

         .def("__getitem__",
              &Constants::get_item,
              "x.__getitem__ (y) <==> x[y]")

        .def("__delitem__",
             &Constants::del_item,
             "x.__delitem__ (y) <==> del x[y]")
         
         .def("__setitem__",
              &Constants::set_item,
              "x.__setitem__(i, y) <==> x[i]=y")

         .def("__contains__",
              &Constants::contains,
              "x.__contains__(y) <==> y in x")
        ;
}
