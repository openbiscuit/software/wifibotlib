/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atom.h"

void Atom_export  (void)
{
    register_ptr_to_python < AtomPtr > ();

    class_<Atom> 
        ("Atom",
         "The Atom class is the base class providing an interface with internal \n"
         "C++ memory management.                                                \n")
        
        .def("__init__",
             make_constructor (Atom::make<Atom>),
             "Create a new Atom                                              \n"
             "                                                               \n"
             "__init__ (self) -> new Atom                                    \n"
             "                                                               \n")

        .add_property("__self__",
                      &Atom::get_self,
                      "Reference on the underlying C++ object")

        .add_property("__count__",
                      &Atom::get_ref_count,
                      "Reference count for the underlying C++ object")

        .add_property("__type__",
                      &Atom::get_type,
                      "Underlying C++ type")
        ;
}
