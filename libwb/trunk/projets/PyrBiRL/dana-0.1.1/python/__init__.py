#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

""" core components

DANA -- Distributed Asyncronous Numerical Adaptive computing framework
======================================================================

DANA is a multi-platform python library for distributed asynchronous
numerical and adaptive computation. The computational paradigm
supporting the library is grounded on the notion of a unit that is
essentially a potential that can vary along time under the influence
of other units and learning.

Website: http://www.loria.fr/~rougier/dana/
"""

#import os, sys
#import libxml2
from _core import *
from tests import test

#__all__ = ['Atom', 'Object', 'Link', 'Unit', 'DeadUnit', 'SynUnit',
#           'Group', 'Network', 'Equation', 'Constants']


# def __dummy_write (self, filename):
#     """ Dummy write function used during read """
#     pass

# def read (filename):
#     """ Read and return a previously saved dana object (any type) """

#     doc = libxml2.parseFile(filename)
#     root = doc.children
#     script_text = ''
#     script_saver = ''
#     script_filename = ''
#     for child in root:
#         if child.type == "element" and child.get_name() == 'script':
#             prop = child.get_properties()
#             while prop:
#                 if prop.name == 'filename':
#                     script_filename = prop.content
#                 elif prop.name == 'saver':
#                     script_saver = prop.content
#                 prop = prop.next
#             script_text = child.get_content()
#     if script_text:
#         # Since we may execute the script present in the file, we disable
#         # the write function temporarily
#         Object_write = Object.write
#         Object.write = __dummy_write
#         exec (script_text, globals(), locals())
#         Object.write = Object_write
#         __name = ''
#         __object = None
#         for __name in locals():
#             __object = locals()[__name]
#             if isinstance (__object, Object) and __name == script_saver:
#                 __object.read(filename)
#                 return __object
#     return None


# Object.__write = Object.write
# def __write (self, filename):
#     """ Overloading of the write function of core.Object

#         This function is able to access the current script in order to save a
#         copy within the xml file.
#     """

#     frame = sys._getframe(1)
#     script_saver = ''
#     for __name in frame.f_locals:
#         __object = frame.f_locals[__name]
#         if isinstance (__object, Object) and __object.uid == self.uid:
#             script_saver = __name
#     if not filename.endswith('.gz'):
#         filename += '.gz'
#     script_text = '\n'
#     co = frame.f_code
#     script_filename = os.path.abspath (co.co_filename)
#     try:
#         f = file (script_filename, "r")
#     except:
#         script_filename = ''
#         script_text = ''
#     else:
#         for line in f:
#             script_text += line
#         f.close()
#     self.__write (filename, script_filename, script_text, script_saver)
        
# Object.write = __write
