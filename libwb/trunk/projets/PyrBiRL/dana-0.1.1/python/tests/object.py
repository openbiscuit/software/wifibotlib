#!/usr/bin/env python
#
# Copyright (c) 2006-2007 Nicolas Rougier.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
# 
# $Id: unit.py 278 2007-08-17 06:23:47Z rougier $

import unittest
from dana.core import Object

class UnitTests (unittest.TestCase):
    def setUp (self):
        self.object = Object()
    
    def testType (self):
        """ Check object type is 'Object' """
        self.assertEqual (self.object.__type__, 'Object')

    def testGetUID (self):
        """ Check object got an identification """
        self.assert_ (self.object.uid)

    def testGetName (self):
        """ Check object got a name """
        self.assert_ (self.object.name)

    def testSetName (self):
        """ Check object name can be set """
        self.object.name = 'name'
        self.assertEqual (self.object.name, 'name')

    def testUIDSet (self):
        """ Check object uid cannot be set """
        self.assertRaises (AttributeError, setattr, self.object, 'uid', 0)

    def testDiffrentUID (self):
        """ Check objects got different uid """
        obj = Object()
        self.assertNotEqual (self.object.uid, obj.uid)

# Test suite
suite = unittest.TestLoader().loadTestsFromTestCase(UnitTests)

if __name__ == "__main__":
    unittest.main()
