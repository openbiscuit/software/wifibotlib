#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

""" Regression tests """

import sys
import unittest
import atom, object, unit, link, group

def test(verbosity=2):
    """ Perform dana regression tests """
    
    suite = unittest.TestSuite()
    suite.addTest (atom.suite)
    suite.addTest (object.suite)
    suite.addTest (unit.suite)
    suite.addTest (link.suite)
    suite.addTest (group.suite)
    runner = unittest.TextTestRunner(stream=sys.stdout, verbosity=verbosity)
    result = runner.run(suite)


if __name__ == '__main__':
    test()
    
