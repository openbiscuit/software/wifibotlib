#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from dana.core import Unit

class LinkTests (unittest.TestCase):
    def setUp (self):
        pass
    
    def testType (self):
        """ Check link default type """
        unit = Unit()
        unit.connect (Unit(), 0)
        self.assert_ (unit.links['default'])

    def testDefault (self):
        """ Check link default settings """
        tgt = Unit()
        src = Unit()
        tgt.connect (src)
        self.assertEqual (tgt.links['default'][0].source, src)
        self.assertEqual (tgt.links['default'][0].weight, 0.0)

    def testLinkNaming (self):
        """ Check link naming """
        tgt = Unit()
        src = Unit()
        tgt.connect (src, 0, 'zorglub')
        self.assertEqual (tgt.links['zorglub'][0].source, src)

    def testLinkIteration (self):
        """ Check link iteration """
        tgt = Unit()
        src = Unit()
        tgt.connect (src)
        tgt.connect (src, 0, 'zorglub')
        for lm in src.links:
            for l in lm.data():
                self.assertEqual (l.source, src)


# Test suite
suite = unittest.TestLoader().loadTestsFromTestCase(LinkTests)

if __name__ == "__main__":
    unittest.main()
