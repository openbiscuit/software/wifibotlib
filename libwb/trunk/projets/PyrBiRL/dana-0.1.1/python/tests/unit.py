#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from dana.core import Unit

class UnitTests (unittest.TestCase):
    def setUp (self):
        self.unit = Unit()
    
    def testType (self):
        """ Check unit type is 'Unit' """
        self.assertEqual (self.unit.__type__, 'Unit')

    def testPotential (self):
        """ Check unit default potential is 0.0 """
        self.assertEqual (self.unit.potential, 0.0)

    def testSetPotential(self):
        """ Check unit potential can be set """
        self.unit.potential = 1.2
        self.assertAlmostEqual (self.unit.potential, 1.2)

    def testConnectDefaultArgument (self):
        """ Check unit connect with default arguments """
        unit = Unit()
        self.unit.connect (unit)
        self.assertEqual (len(self.unit.links['default']), 1)
        l = self.unit.links['default'][0]
        self.assertEqual (l.weight, 0.0)

    def testConnectWeightArgument (self):
        """ Check unit connect with specified weight """
        unit = Unit()
        self.unit.connect (unit, 1.0)
        l = self.unit.links['default'][0]
        self.assertEqual (l.weight, 1.0)

    def testConnectTypeArgument (self):
        """ Check unit connect with specified type """
        unit = Unit()
        self.unit.connect (unit, 1.0, 'id')
        l = self.unit.links['id'][0]
        self.assertEqual (l.weight, 1.0)


# Test suite
suite = unittest.TestLoader().loadTestsFromTestCase(UnitTests)

if __name__ == "__main__":
    unittest.main()
