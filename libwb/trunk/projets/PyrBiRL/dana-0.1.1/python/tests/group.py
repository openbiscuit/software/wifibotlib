#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from dana.core import Unit, Group

class GroupTests (unittest.TestCase):
    def setUp (self):
        pass
    
    def testType (self):
        """ Check group type is 'Group' """
        group = Group()
        self.assertEqual (group.__type__, 'Group')

    def testDefaultConstruction (self):
        """ Check group default construction """
        group = Group()
        self.assertEqual (group.size, 0)

    def testExtensionConstruction1 (self):
        """ Check group construction 1 """
        group = Unit()*[10,10]
        self.assertEqual (group.size, 100)
        self.assertEqual (group.shape[0], 10)
        self.assertEqual (group.shape[1], 10)

    def testExtensionConstruction2 (self):
        """ Check group construction 2 """
        group = Unit()*[10]
        self.assertEqual (group.size, 10)
        self.assertEqual (group.shape[0], 10)
        self.assertEqual (group.shape[1], 1)

    def testSetItem (self):
        """ Check group set item """
        group = Unit()*[10]
        unit = Unit()
        group[0] = unit
        self.assertEqual (group[0], unit)

    def testSetSlice (self):
        """ Check group set slice """
        group = Unit()*[10]
        group[:] = Unit()*[10]
        self.assertEqual (group.size, 10)

    def testGetItem (self):
        """ Check group get item """
        group = Unit()*[10]
        unit = Unit()
        group[0] = unit
        self.assertEqual (group[0], unit)

    def testGetSlice (self):
        """ Check group get slice """
        group = Unit()*[10]
        self.assertEqual (group[:].size, 10)
        self.assertEqual (group[::2].size, 5)

    def testDelItem (self):
        """ Check group del item """
        group = Unit()*[10]
        del group[0]
        self.assertEqual (group.size, 9)

    def testDelSlice(self):
        """ Check group del slice """
        group = Unit()*[10]
        del group[::2]
        self.assertEqual (group.size, 5)

    def testIteration (self):
        """ Check group iterations """
        group = Unit()*[10]
        for unit in group:
            pass
        self.assertEqual (group.size, 10)


# Test suite
suite = unittest.TestLoader().loadTestsFromTestCase(GroupTests)

if __name__ == "__main__":
    unittest.main()
