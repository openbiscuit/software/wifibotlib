#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from dana.core import Atom

class UnitTests (unittest.TestCase):
    def setUp (self):
        self.atom = Atom()
    
    def testType (self):
        """ Check atom type is 'Atom' """
        self.assertEqual (self.atom.__type__, 'Atom')

    def testRefCount (self):
        """ Check atom got a single reference count """
        self.assertEqual (self.atom.__count__, 1)

    def testSelf (self):
        """ Check atom got a shared pointer on itself """
        self.assert_ (self.atom.__self__)


# Test suite
suite = unittest.TestLoader().loadTestsFromTestCase(UnitTests)

if __name__ == "__main__":
    unittest.main()
