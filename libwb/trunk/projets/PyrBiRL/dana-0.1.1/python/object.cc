/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "object.h"

void Object_export  (void)
{
    register_ptr_to_python <ObjectPtr> ();

    int (Object::*Object_read) (const std::string) = &Object::read;
    int (Object::*Object_write)(const std::string) = &Object::write;

    class_<Object, bases <Atom> > 
        ("Object",
         "The Object class is a base class providing naming and identification\n"
         "services.                                                           \n")
         
        .def("__init__",
             make_constructor (Object::make<Object>),
             "Create a new Object                                            \n"
             "                                                               \n"
             "__init__(self) ->  new Object                                  \n"
             "                                                               \n")
        .def("__repr__",
             &Object::repr,
             "Object string representation                                   \n"
             "                                                               \n"
             "repr(self) -> string                                           \n"
             "  string -- object representation                              \n"
             "                                                               \n")
        .def("write",
             Object_write,
             "Write object to file                                           \n"
             "                                                               \n"
             "write(self, filename) -> status                                \n"
             "  filename -- file to write to                                 \n"
             "  status   -- 1 if write successful, 0 else                    \n"
             "                                                               \n")
        .def("read",
             Object_read,
             "Read object from file                                          \n"
             "                                                               \n"
             "read(self, filename) -> status                                 \n"
             "  filename -- file to read from                                \n"
             "  status   -- 1 if read successful, 0 else                     \n"
             "                                                               \n")
        .add_property ("uid",
                       &Object::get_uid,
                       "Unique identifier")
        .add_property ("name",
                       &Object::get_name, &Object::set_name,
                       "Name")
        ;
}
