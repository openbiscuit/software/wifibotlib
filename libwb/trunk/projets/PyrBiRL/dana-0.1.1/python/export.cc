/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "export.h"

BOOST_PYTHON_MODULE(_core) {

    //    import_array();
    //    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");

    docstring_options doc_options;
    doc_options.disable_signatures();

    Atom_export();
    Object_export();
    Link_export();
    Unit_export();
    Equation_export();
    Constants_export();
    Group_export();
    Network_export();
    //    Shape_export();
    //    Distance_export();
    //    Profile_export();
    //    Density_export();
}
