Tutorial
========


Unit
----

Unit are the basic objects of DANA that represent potentials. Creation is
straightforward:

.. code-block:: pycon

   >>> from dana import Unit
   >>> unit = Unit()

Once the unit has been created, you have access to several attributes:

  ===============  ============== =============================
  **Name**         **Writeable**   **Description**
  ===============  ============== =============================
  __type__         No             Underlying C type
  __count__        No             Number of references
  __self__         No             Reference to itself
  uid              No             Unique identifier
  name             Yes            Human readable identification 
  potential        Yes            Potential
  links            Yes            Dictionnary of typed links
  equation         Yes            Potential equation
  ===============  ============== =============================

The most important attributes are the potential that describe the unit
current potential, the equation that describe how the potential evolve
through time and the links that represent other connections from other units.



Link
--------------------------------------------------------------------------------

Units can be connected together using weighted links. Links are owned by the
unit that initiate the connection and they may be given a type using a string
identifier. If no identifier is provided, a "default" type is used:

.. code-block:: pycon

   >>> from dana import *
   >>> unit_1 = Unit()
   >>> unit_1.name = "unit_1"
   >>> unit_2 = Unit()
   >>> unit_2.name = "unit_2"
   >>> unit_1.connect (unit_2, 0.0)
 
Once links have been created, you can access them easily using the links
attribute which is a dictionnay describings link lists, one for each type:
  
.. code-block:: pycon

   >>> print unit_1.links
   {'default': [(Unit_2, 0), ], }

   >>> print unit_1.links["default"]
   [(Unit_2, 0), ]

   >>> print unit_1.links["default"][0]
   (Unit_2, 0)

   >>> print unit_1.links["default"][0].source
   0

   >>> print unit_1.links["default"][0].weight
   0.0


Equation
--------------------------------------------------------------------------------

DANA core units are governed by an equation that can be easily described
using the Equation object as follows:

.. code-block:: pycon

   >>> from dana import Equation
   >>> eq = Equation()

This equation is initially undefined and thus does not compute anything. You
have to provide a definition for the equation as well as specifying what are
the variables of the equation:

.. code-block:: pycon

   >>> eq.variables = "x,y"
   >>> eq.definition = "x*y"

Once variables and definition have been set, you can use the equation to
evaluate it for a given list of values:

.. code-block:: pycon

   >>> eq.evaluate ([2,2])
   4.0

Note that you can also introduce some constants within the equation (before
re-defining the equation):

.. code-block:: pycon

   >>> eq.constants = {"dt": 0.1}
   >>> eq.definition = "x*y*dt"
   >>> eq.evaluate ([2,2])
   0.4

Equation gives you access to regular mathematical expressions through the
function parser library designed by Warp (http://warp.povusers.org/FunctionParser/):

  =========== ==================================================================
   **Name**    **Description**
  =========== ==================================================================
  abs(A)      Absolute value of A. If A is negative, returns -A otherwise
              returns A.
  ----------- ------------------------------------------------------------------
  acos(A)     Arc-cosine of A. Returns the angle, measured in radians,
              whose cosine is A.
  ----------- ------------------------------------------------------------------
  acosh(A)    Same as acos() but for hyperbolic cosine.
  ----------- ------------------------------------------------------------------
  asin(A)     Arc-sine of A. Returns the angle, measured in radians, whose
              sine is A.
  ----------- ------------------------------------------------------------------
  asinh(A)    Same as asin() but for hyperbolic sine.
  ----------- ------------------------------------------------------------------
  atan(A)     Arc-tangent of (A). Returns the angle, measured in radians,
              whose tangent is (A).
  ----------- ------------------------------------------------------------------
  atan2(A,B)  Arc-tangent of A/B. The two main differences to atan() is
              that it will return the right angle depending on the signs of
              A and B (atan() can only return values betwen -pi/2 and pi/2),
              and that the return value of pi/2 and -pi/2 are possible.
  ----------- ------------------------------------------------------------------
  atanh(A)    Same as atan() but for hyperbolic tangent.
  ----------- ------------------------------------------------------------------
  ceil(A)     Ceiling of A. Returns the smallest integer greater than A.
              Rounds up to the next higher integer.
  ----------- ------------------------------------------------------------------
  cos(A)      Cosine of A. Returns the cosine of the angle A, where A is
              measured in radians.
  ----------- ------------------------------------------------------------------
  cosh(A)     Same as cos() but for hyperbolic cosine.
  ----------- ------------------------------------------------------------------
  cot(A)      Cotangent of A (equivalent to 1/tan(A)).
  ----------- ------------------------------------------------------------------
  csc(A)      Cosecant of A (equivalent to 1/sin(A)).
  ----------- ------------------------------------------------------------------
  eval(...)   This a recursive call to the function to be evaluated. The
              number of parameters must be the same as the number of parameters
              taken by the function. Must be called inside if() to avoid
              infinite recursion.
  ----------- ------------------------------------------------------------------
  exp(A)      Exponential of A. Returns the value of e raised to the power
              A where e is the base of the natural logarithm, i.e. the
              non-repeating value approximately equal to 2.71828182846.
  ----------- ------------------------------------------------------------------
  floor(A)    Floor of A. Returns the largest integer less than A. Rounds
              down to the next lower integer.
  ----------- ------------------------------------------------------------------
  if(A,B,C)   If int(A) differs from 0, the return value of this function is B,
              else C. Only the parameter which needs to be evaluated is
              evaluated, the other parameter is skipped; this makes it safe to
              use eval() in them.
  ----------- ------------------------------------------------------------------
  int(A)      Rounds A to the closest integer. 0.5 is rounded to 1.
  ----------- ------------------------------------------------------------------
  log(A)      Natural (base e) logarithm of A.
  ----------- ------------------------------------------------------------------
  log10(A)    Base 10 logarithm of A.
  ----------- ------------------------------------------------------------------
  max(A,B)    If A>B, the result is A, else B.
  ----------- ------------------------------------------------------------------
  min(A,B)    If A<B, the result is A, else B.
  ----------- ------------------------------------------------------------------
  sec(A)      Secant of A (equivalent to 1/cos(A)).
  ----------- ------------------------------------------------------------------
  sin(A)      Sine of A. Returns the sine of the angle A, where A is
              measured in radians.
  ----------- ------------------------------------------------------------------
  sinh(A)     Same as sin() but for hyperbolic sine.
  ----------- ------------------------------------------------------------------
  sqrt(A)     Square root of A. Returns the value whose square is A.
  ----------- ------------------------------------------------------------------
  tan(A)      Tangent of A. Returns the tangent of the angle A, where A
              is measured in radians.
  ----------- ------------------------------------------------------------------
  tanh(A)     Same as tan() but for hyperbolic tangent.
  =========== ==================================================================

Examples of function string understood by the class:

* "1+2"
* "x-1"
*  "-sin(sqrt(x^2+y^2))"
*  "sqrt(XCoord*XCoord + YCoord*YCoord)"


Putting things together
-----------------------

At this point, we have anough stuff to start doing some (useless)
computations. We can have some units connected together and now, we can also
define an arbitraty equation that will govern unit potential over time. The
important point to note is that unit equations **must** have dt and potential
as first variables of the equation. The reason is that the automagic that
operates in the dark relies on these two variables.  Let's start with the
definition of two units that are connected together with type "input":

.. code-block:: pycon

   >>> u1 = Unit()
   >>> u1.potential = 0.0
   >>> u2 = Unit()
   >>> u2.potential = 1.0
   >>> u2.connect (u1, 1.0, "input")
   >>> u1.connect (u2, 1.0, "input") 


Now we will define the equation for both units. The nice thing is that
you can use the variable "input" as a regular variable in your equation. In
the present case, "input" is the overall weighted sum of potentials that
reach the unit using "input" links. Of course, if you have named your links
"zorglub", you would have had access to the "zorglub" variable.

.. code-block:: pycon

   >>> eq = Equation()
   >>> eq.variables  = "dt,potential,input"
   >>> eq.definition = "(input-potential)*dt"
   >>> u1.equation = u2.equation = eq

Now we can evaluate iteratively the respective potential of the two units by
calling the "compute_dp" method with relevant dt:

.. code-block:: pycon

   >>> dt = 0.01
   >>> for i in range(1000):
   >>>     u1.compute_dp(dt)
   >>>     u2.compute_dp(dt)
   >>> print u1.potential, u2.potential
   0.503, 0.503

I know, you can't believe we did all that to compute the diffusion of a dual
particles system...



It's slow, it's so slow...
--------------------------------------------------------------------------------

Well, not that slow indeed. Here are come comparative results between a pure
python unit, the core DANA unit and a dedicated C++ unit:

  =================  =================== ================
   Unit type          Iteration type     Reference time
  =================  =================== ================
  Pure Python         Python              ~ 32 minutes
  -----------------  ------------------- ----------------
  DANA core                               ~ 63.7 seconds
  -----------------  ------------------- ----------------
  DANA benchmark                          ~ 70.1 seconds
  -----------------  ------------------- ----------------
  DANA core           C++                 ~  5.1 seconds
  -----------------  ------------------- ----------------
  DANA benchmark                          ~  5.4 seconds
  =================  =================== ================

As explained before, the core DANA unit offers basic services to run some
simulations using equations. Even if there is some C code behind, the dynamic
evaluation of the equation does not make things optimal. If you need faster
computation, then you'll have to write your own C Unit object (that will
inherit from the basic Unit object) and implements the computational part
directly in the source code. Fortunately, dana provides a small script that
build a source tree ready to be compiled and installed. You only have to
provide a name, write the equation part and install it. Then instead of using
the basic Unit, you can now use your own.
