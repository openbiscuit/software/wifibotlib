:tocdepth: 2

.. _examples:


Examples
========

Game of Life
--------------------------------------------------------------------------------

This example implements the well known Game of Life in both synchronous mode
(the regular mode) and asynchronous mode where unit evaluate and update their
state immediately.

.. literalinclude:: _static/game-of-life.py
   :language: python

.. figure:: _static/game-of-life.png

   Synchronous and Asynchronous game of life after a few iterations.



Diffusion
--------------------------------------------------------------------------------

This is very simple diffusion network where units update their state according
to the mean activity of their neighbours.

.. literalinclude:: _static/diffusion.py
   :language: python

.. figure:: _static/diffusion.png

   Network state after some iterations of the diffusion processus.


Sobel filter
--------------------------------------------------------------------------------

This implements sobel filters using information from X and Y filters.

.. literalinclude:: _static/sobel.py
   :language: python

.. figure:: _static/sobel.png

   Sobel filter applied to lena.



Continuum Neural Field Theory
--------------------------------------------------------------------------------
