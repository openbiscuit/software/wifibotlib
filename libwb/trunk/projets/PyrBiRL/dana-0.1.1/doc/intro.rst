Introduction
============

This is the documentation for the DANA computing framework. DANA is a library
that support distributed, asynchronous, numerical and adaptive computation
which is closely related to both the notion of artificial neural networks and
cellular automaton. However, there exist two major differences. The first
difference lies in the distributed nature of computation that does not allow to
implement "regular" neural networks which require non local functions. For
example, perceptron, multi layer perceptron, Kohonen maps or Hopfield networks
both requires at some point a global function and then, cannot be implemented
using DANA. The second difference lies in the asynchronous nature of
computations: units are evaluated in random order and updated immediately.

From a conceptual point of view, the computational paradigm supporting the
library is grounded on the notion of a unit that is essentially a potential
that can vary along time under the influence of other units and learning. Those
units are organized into layers, maps and network: a network is made of one to
several map, a map is made of one to several layer and a layer is made of a set
of several units. Each unit can be linked to any other unit (included itself)
using a weighted link.

DANA library offers a set of core classes needed to design and run such
networks. However, what is actually computed by a unit or what is learned is
far beyond the scope of the library. User is in charge of implementing a unit
derived class that actually does something useful for his own purposes.


Implementation
--------------

DANA is based on both C++ and Python languages. While C++ ensures some decent
speed for simulation, python provides a powerful scripting language that can be
used for model design, interaction and visualization. The challenge is to
export C++ objects to python with minimal effort and this is precise ly what
the DANA library has been designed for. In the end, the user is able to build
various models simply by importing the relevant libraries into python. Say
differently by Dave Abrahams (talk summary from BoostCon 07):

  *Python and C++ are in many ways as different as two languages could be:
  while C++ is usually compiled to machine-code, Python is
  interpreted. Python's dynamic type system is often cited as the foundation of
  its flexibility, while in C++ static typing is the cornerstone of its
  efficiency. C++ has an intricate and difficult compile-time meta-language,
  while in Python, practically everything happens at runtime.*

  *Yet for many programmers, these very differences mean that Python and C++
  complement one another perfectly. Performance bottlenecks in Python programs
  can be rewritten in C++ for maximal speed, and authors of powerful C++
  libraries choose Python as a middleware language for its flexible system
  integration capabilities.*


Requirements
------------

These are external packages and libraries which you will need to install before
installing DANA.

:boost:
    Free peer reviewed portble C++ libraries (http://www.boost.org)

:python:
    Dynamic object-oriented programming language (http://www.python.org)

:numpy:
    Scientific computing with python (http://numpy.scipy.org/)

:matplotlib:
    2D plotting python library (http://matplotlib.sourceforge.net)

:XML Library:
  XML C parser and toolkit (http://www.xmlsoft.org)

:IPython:
  Enhanced interactive python shell (http://ipython.scipy.org)


Installing
----------

Use ``setup.py``::

   python setup.py build
   sudo python setup.py install

