#  DANA -- Distributed Asynchronous Numerical Adaptive computing framework
#  Copyright (C) 2006,2007,2008 Nicolas P. Rougier
# 
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
""" """


from functools import partial
from matplotlib.colors import LinearSegmentedColormap
import dana.core as core
import numpy
import pylab


class View (object):
    """ """

    def __init__ (self, groups, size=5, fontsize=16, cmap = None, scale=[-1,1], use_ticks=False):
        """ """

        pylab.rc('figure.subplot',
           left   = 0.05,
           right  = 0.95,
           bottom = 0.05,
           top    = 0.95,
           hspace = 0.10,
           wspace = 0.10)

        # Check overall size
        self.rows = 1
        self.cols = 1
        grps = []

        # First case: a single list of groups
        if isinstance(groups, (core.Group,numpy.ndarray)):
            grps.append ([groups, 1])
        # Second case: at least a list of groups
        elif isinstance(groups,(list,tuple)):
            # Check if there are only groups within
            only_group = True
            for group in groups:
                if isinstance(group,(list,tuple)):
                    only_group = False
                    break
            if only_group:
                self.rows = 1
                self.cols = len(groups)
                i = 1
                for group in groups:
                    if isinstance(group,(core.Group,numpy.ndarray)):
                        grps.append ([group, i])
                    i+=1
            else:
                self.rows = len(groups)
                for group in groups:
                    if isinstance(group,(list,tuple)):
                        self.cols = max(self.cols, len(group))
                i = 1
                for group in groups:
                    x = 0
                    if isinstance(group,(list,tuple)):
                        for subgroup in group:
                            if isinstance(subgroup,(core.Group,numpy.ndarray)):
                                grps.append ([subgroup, i+x])
                            x+=1
                    elif isinstance(subgroup,(core.Group,numpy.ndarray)):
                        grps.append ([subgroup, i])
                    i += self.cols
        else:
            return

        self.selected_unit = None
        self.selected_unit_group = None
        self.selected_unit_x = -1
        self.selected_unit_y = -1
        self.groups = []
        self.figure = pylab.figure(figsize=(self.cols*size, self.rows*size))

#        if title:
#            suptitle (title, fontsize=24)

        pylab.connect('button_press_event', self._on_click)

        data = {
            'red':   ((0., 0., 0.), (.5, 1., 1.), (.75, 1., 1.), (1., 1., 1.)),
            'green': ((0., 0., 0.), (.5, 1., 1.), (.75, 1., 1.), (1., 0., 0.)),
            'blue':  ((0., 1., 1.), (.5, 1., 1.), (.75, 0., 0.), (1., 0., 0.))}

        if not cmap:
            cmap = LinearSegmentedColormap('cm',  data)
        #   cmap = cmap.hot
        cmap.set_bad('k', 1.0)
#        cmap.set_over('k', 1.0)
#        cmap.set_under('k', 1.0)

        for group, i in grps:
            subplt = pylab.subplot(self.rows, self.cols, i)
            #grid()
            if not use_ticks:
                pylab.xticks('')
                pylab.yticks('')
            subplt.group = group
            subplt.background = None
            subplt.format_coord = partial (self._format_coord, subplt)
            if isinstance(group,core.Group):
                Z = group.potentials; #array(group, dtype=float)
            else:
                Z = group
            #Z = ma.masked_where(isnan(Z), Z)
            subplt.data = Z
            im = pylab.imshow (Z, cmap=cmap, vmin=scale[0], vmax=scale[1],
                               origin='lower', interpolation='nearest',
                               extent=(0, Z.shape[1], 0,Z.shape[0]))
            if hasattr(group, 'name'):
                subplt.text (0.0, 1.01, group.name, fontsize=fontsize,
                             transform = subplt.transAxes)
            self.groups.append ([group, subplt, im])
        self.update()
            

    def _format_coord(self, subplt, xdata, ydata):
        """ Return position/value as a string """
        group = subplt.group
        if xdata is None or ydata is None or group is None:
            return ''
        x = int(xdata)
        y = int(ydata)
        if x < group.shape[1] and y < group.shape[0]:
            return '[%d,%d]: %f' % (x,y, subplt.data[y,x])
        return ''


    def _on_click (self, event):
        """ Show/hide weights for the selected unit """
        
        if event.inaxes:
            group = event.inaxes.group
            if group is None or not isinstance(group,core.Group):
                return
            if event.button == 1:
                x = int(event.xdata)
                y = int(event.ydata)
                unit = group[y*group.shape[0]+x]
                if self.selected_unit and unit.uid == self.selected_unit.uid:
                    self.selected_unit = None
                    self.update()
                else:
                    self.selected_unit = unit
                    self.selected_unit_group = group
                    self.selected_unit_x = x
                    self.selected_unit_y = y
                    self.update()
        else:
            self.selected_unit = None
            self.update()

    def update (self):
        """ Update figure """

        if self.selected_unit:
            for group, subplt, im in self.groups:
                if isinstance(group,core.Group):
                    g = self.selected_unit.weights (group)
                    Z = g.potentials
                    subplt.data = Z
                    im.set_data (Z)
                    #im.set_data (ma.masked_where(isnan(Z), Z))
        else:
            for group,subplt,im in self.groups:
                if isinstance(group,core.Group):
                    Z = group.potentials #array(group, dtype=float)
                    subplt.data = Z
                    im.set_data (Z)
                    #im.set_data (ma.masked_where(isnan(Z), Z))
        pylab.draw()


