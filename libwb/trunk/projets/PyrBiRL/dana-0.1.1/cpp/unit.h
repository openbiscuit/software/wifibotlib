/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_UNIT_H__
#define __DANA_CORE_UNIT_H__

#include <map>
#include <boost/pool/detail/singleton.hpp>
#include "object.h"
#include "link.h"
#include "equation.h"

namespace dana {
    namespace core {

        typedef boost::shared_ptr <class Group> GroupPtr;
        GroupPtr operator*(unsigned int n, const UnitPtr unit);

        typedef boost::shared_ptr <class Unit> UnitPtr;

        /**
         * Unit class 
         *
         * A Unit is essentially a potential that can vary along time under the
         * influence of other units and learning.
         */
        class Unit: public Object {
        public:

            /**
             * @name Creation/Destruction
             */
            /**
             * Default constructor.
             */
            Unit (void);

            /**
             * Copy constructor.
             *
             * @param other object to be copied
             */
            Unit (const Unit &other);

            /**
             * Copy.
             *
             * @param other object to be copied
             */
            virtual Unit &operator=(const Unit &other);

            /**
             * Destructor.
             */
            virtual ~Unit(void);

            /*
             * Clone
             */
            virtual Unit *clone (void) const;
            //@}

            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);            
            /**
             * Get string representation.
             *
             * @return Object representation as a string
             */
            virtual std::string repr (void);

            /**
             * Compute potential at time+dt
             *
             * @param dt time difference
             * @return Difference of potential
             */
            virtual float compute_dp(float dt);

            /**
             * Compute weights at time+dt
             *
             * @param dt time difference
             * @return Difference of weights
             */
            virtual float compute_dw(float dt);

            /**
             * Get contribution from links of type name
             *
             * @param name Name of inout to consider
             */
            virtual float get_input(std::string name);

            /**
             * Connect unit to another one
             *
             * @param source Source Unit
             * @param weight link weight
             * @param type   link type
             */
            virtual void connect (UnitPtr source,
                                  float weight=0.0,
                                  std::string type="default");

            /**
             * Connect unit to a group using given profile
             *
             * @param source  source group
             * @param profile weights profile
             * @param type    link type
             */
            virtual void connect (GroupPtr source,
                                  GroupPtr profile,
                                  std::string type="default",
                                  float noise = 0.0f,
                                  float probability = 1.0f,
                                  bool toric = false);


            /**
             * Get weights from source
             *
             * @param source Group to consider
             * @return A group representing weights
             */
            virtual GroupPtr weights (GroupPtr source);

            /**
             * Get equation
             *
             * @return Equation governing unit potential
             */
            virtual EquationPtr get_equation (void);

            /**
             * Set equation
             *
             * @param equation Equation
             */
            virtual void set_equation (EquationPtr equation);

            /**
             * Get potential
             *
             * @return Unit potential
             */
            virtual float get_potential (void);

            /**
             * Set potential
             *
             * @param value New potential
             */
            virtual void set_potential (float value);

            /**
             * Create a group of n units
             * 
             * @param n Number of unit
             * @return A new group of n units
             */
            virtual GroupPtr operator* (unsigned int n);

            /**
             * Create a new group with this and unit.
             *
             * @param unit unit to be added in group
             * @return A new group with this and unit
             */
            virtual GroupPtr operator+ (const UnitPtr unit);
            //@}

        public:
            /**
             * Potential
             */
            float potential;

            /**
             * Links
             */
            LinkMap links;

            /**
             * Equation governing potential
             */
            EquationPtr equation;

            /**
             * Unit normalized position within group
             */ 
            double x,y;
        };


        class SynUnit : public Unit {
        public:
            /**
             * @name Creation/Destruction
             */
            /**
             * Default constructor.
             */
            SynUnit (void);

            /**
             * Copy constructor.
             *
             * @param other object to be copied
             */
            SynUnit (const SynUnit &other);

            /**
             * Copy.
             *
             * @param other object to be copied
             */
            virtual SynUnit &operator=(const SynUnit &other);

            /**
             * Destructor.
             */
            virtual ~SynUnit(void);

            /*
             * Clone
             */
            virtual SynUnit *clone (void) const;
            //@}

            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);            

            /**
             * Compute potential at time+dt
             *
             * @param dt time difference
             * @return Difference of potential
             */
            virtual float compute_dp(float dt);

            /**
             * Compute weights at time+dt
             *
             * @param dt time difference
             * @return Difference of weights
             */
            virtual float compute_dw(float dt);

        public:
            /**
             * Potential at t-1
             */
            float _potential;
        };



        class KUnit : public Unit {
        public:
            /**
             * @name Creation/Destruction
             */
            /**
             * Default constructor.
             */
            KUnit (void);

            /**
             * Copy constructor.
             *
             * @param other object to be copied
             */
            KUnit (const KUnit &other);

            /**
             * Copy.
             *
             * @param other object to be copied
             */
            virtual KUnit &operator=(const KUnit &other);

            /**
             * Destructor.
             */
            virtual ~KUnit(void);

            /*
             * Clone
             */
            virtual KUnit *clone (void) const;
            //@}

            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);

            /**
             * Get contribution from links of type name
             *
             * @param name Name of inout to consider
             */
            virtual float get_input(std::string name);
        };

        class DeadUnit : public Unit {
        public:
            /**
             * @name Creation/Destruction
             */
            /**
             * Default constructor
             */
            DeadUnit (void);

            /**
             * Copy constructor
             *
             * @param other object to be copied
             */
            DeadUnit (const DeadUnit &other);

            /**
             * Copy
             *
             * @param other object to be copied
             */
            virtual DeadUnit &operator=(const DeadUnit &other);

            /**
             * Destructor.
             */
            virtual ~DeadUnit(void);

            /*
             * Clone
             */
            virtual DeadUnit *clone (void) const;
            //@}

            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);            

            /**
             * Compute potential at time+dt
             *
             * @param dt time difference
             * @return Difference of potential
             */
            virtual float compute_dp(float dt);

            /**
             * Compute weights at time+dt
             *
             * @param dt time difference
             * @return Difference of weights
             */
            virtual float compute_dw(float dt);

            /**
             * Set potential
             *
             * @param value New potential
             */
            virtual void set_potential (float value);

            /**
             * Get potential
             *
             * @return Unit potential
             */
            virtual float get_potential (void);
        };
            
    } // namespace core
} //namespace dana

#endif
