/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_CONSTANTS_H__
#define __DANA_CORE_CONSTANTS_H__

#include <string>
#include <map>
#include "object.h"

namespace dana {
    namespace core {

        typedef boost::shared_ptr <class Constants> ConstantsPtr;

        /**
         * Constants class 
         *
         * A Constants object is used to hold arbitrary real constants.
         */
        class Constants: public Object {
        public:
            
            /**
             * @name Creation/Destruction
             */
            /**
             * Default constructor.
             */
            Constants (void);

            /**
             * Copy constructor.
             *
             * @param other object to be copied
             */
            Constants (const Constants &other);

            /**
             * Copy.
             *
             * @param other object to be copied
             */
            virtual Constants &operator=(const Constants &other);

            /**
             * Destructor.
             */
            virtual ~Constants(void);
            //@}


            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);

            /**
             * Get string representation.
             *
             * @return Object representation as a string
             */
            virtual std::string repr (void);

            /**
             * Get size
             *
             * @return size
             */
            int size (void);

            /**
             * Set item
             *
             * @param name  constant name
             * @param value constant value
             */
            void set_item (std::string name, float value);

            /**
             * Get item
             *
             * @param name   constant name
             * @return       constant value
             */            
            float get_item (std::string name);

            /**
             * Delete item
             *
             * @param name   constant name
             */            
            void del_item (std::string name);

            /**
             * Check if constant is defined
             *
             * @param  name constant name to be checked
             * @return      true if constant is defined, false else
             */
            virtual bool contains(std::string name);

        public:
            /**
             * Constants
             */
            std::map<std::string,float> constants;
        };

    } // namespace core
} //namespace dana

#endif
