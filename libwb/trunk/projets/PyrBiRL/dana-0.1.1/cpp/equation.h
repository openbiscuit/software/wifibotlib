/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_EQUATION_H__
#define __DANA_CORE_EQUATION_H__

#include <string>
#include "object.h"
#include "constants.h"
#include "fparser.hh"

namespace dana {
    namespace core {

        typedef boost::shared_ptr <class Equation> EquationPtr;
        class Equation: public Object {

        public:
            Equation(void);
            virtual ~Equation(void);
            
            virtual std::string repr (void) const;
            virtual std::string get_type (void) const;

            void        set_definition (std::string definition);
            std::string get_definition (void) const;
            void        set_variables (std::string vars);
            std::string get_variables (void) const;

            /**
             * Get constants
             *
             * @return Constants related to potential evaluation
             */
            virtual ConstantsPtr get_constants (void);
            
            /**
             * Set constants related to potential evaluation
             *
             * @param constants Constants
             */
            virtual void set_constants (ConstantsPtr constants);


            float       evaluate (double *vals);

            void        parse (void);

        public:
            std::string                     definition;
            std::vector<std::string>        variables;
            double *                        values;
            ConstantsPtr                    constants;
            FunctionParser                  parser;
            bool                            valid;
        };

    } // namespace core
} //namespace dana

#endif
