/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_GROUP_H__
#define __DANA_CORE_GROUP_H__

#include <map>
#include "object.h"
#include "unit.h"
#include "equation.h"


namespace dana {
    namespace core {

        typedef boost::shared_ptr <class Group> GroupPtr;
        typedef boost::shared_ptr <class Network> NetworkPtr;

        /**
         * Group class 
         *
         * A Group is an ordered set of Units with a shape.
         */
        class Group: public Object {

        public:

            /**
             * @name Creation/Copy/Destruction
             */

            // Group (void);
            // Group (const unsigned int &size, const Unit &unit);
            // Group (const unsigned int &w, const unsigned int &h, const Unit &unit);
            // Group (const Group &other);
            // virtual ~Group(void);

            /**
             * Default constructor.
             */
            Group (void);

            /**
             * Copy constructor.
             *
             * @param other object to be copied
             */
            Group (const Group &other);

            /**
             * Constructor.
             *
             * @param cols number of column
             * @param rows number of row
             * @param unit to be cloned for filling the group
             */

            Group (const unsigned int height,
                   const unsigned int width,
                   const Unit &unit);

            /**
             * Constructor.
             *
             * @param size number of unit
             * @param unit to be cloned for filling the group
             */

            Group (const unsigned int size,
                   const Unit &unit);

            /**
             * Copy
             *
             * @param other object to be copied
             */
            virtual Group &operator=(const Group &other);

            /**
             * Destructor.
             */
            virtual ~Group(void);

            /**
             * Clone
             */
            Group *clone (void) const;
            //@}

            /**
             * Compute dp and dw for duration using dt as elementary time step
             *
             * @param duration time to run
             * @param dt elementary time step
             */ 
            virtual void run(float duration, float dt);

            /**
             * Compute potential for all units
             *
             * @param dt elementary time step
             */ 
            virtual float compute_dp(float dt);

            /**
             * Compute weights for all units
             *
             * @param dt elementary time step
             */ 
            virtual float compute_dw(float dt);

            /**
             * Get equation
             *
             * @return equation
             */
            virtual EquationPtr get_equation(void);

            /**
             * Set equation.
             *
             * Setting equation for group will set equation for all units
             * within group.
             *
             * @param equation Equation
             */
            virtual void set_equation(EquationPtr equation);


            /**
             * Connect to other considered as a source.
             *
             * @param other     Source group
             * @param profile   Weights profile as a group
             * @param type      Link type
             */
            virtual void connect (GroupPtr other,
                                  GroupPtr profile,
                                  std::string type = "default",
                                  float noise = 0.0f,
                                  float probability = 1.0f,
                                  bool toric = false);


            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);
            /**
             * Get string representation.
             *
             * @return Object representation as a string
             */
            virtual std::string repr (void);

            /**
             * Get group size
             *
             * @return group size
             */
            virtual int size(void);

            /**
             * Check if item is in group
             *
             * @param  item item to be checked
             * @return      True if item is in group, False else
             */
            virtual bool contains(UnitPtr item);

            /**
             * Find index of item
             *
             * @param  item item to be found
             * @return      index of item or -1
             */
            virtual int find(UnitPtr item);

            /**
             * Get item
             *
             * @param index index of the item to be get
             * @return      specified item
             */
            virtual UnitPtr get_item(int index);

            /**
             * Get item
             *
             * @param index index of the item to be get
             * @return      specified item
             */
            virtual UnitPtr get_item(int y, int x);

            /**
             * Get a slice
             *
             * @param  start start of the slice
             * @param  stop  stop of the slice
             * @param  step  step to use to go from start to stop
             * @return       group slice
             */
            virtual GroupPtr get_slice(int start, int stop, int step);

            /**
             * Set an item
             *
             * @param index index of the item to be set
             * @param other item to be set
             */
            virtual void set_item(int index, UnitPtr item);

            /**
             * Set a slice
             *
             * @param start start of the slice
             * @param stop  stop of the slice
             * @param step  step to use to go from start to stop
             * @param slice group to replace the slice
             */
            virtual void set_slice(int start, int stop,
                                   int step, GroupPtr slice);
            /**
             * Delete an item
             *
             * @param index index of the item to be deleted
             */
            virtual void del_item(int index);

            /**
             * Delete a slice
             *
             * @param start start of the slice
             * @param stop  stop of the slice
             * @param step  step to use to go from start to stop
             *
             */
            virtual void del_slice(int start, int stop, int step);

            /**
             * Set shape
             *
             * @param w new width
             * @param h new height
             *
             */
            virtual void set_shape (unsigned int h, unsigned int w);

            /**
             * Create a network with current group and other
             *
             * @param  other group to be added with current
             * @return       a new network
             */
            virtual NetworkPtr operator+(const GroupPtr other);

            
            /**
             * Get potentials as a float array
             *
             * Note: array must not be deallocated
             */
            virtual float *get_potentials(void);
            //@}


        public:
            /**
             * Units composing group
             */
            std::vector<UnitPtr> units;

            /**
             * Vector of unit potentials (for faster access)
             */
            float *potentials;
            
            /**
             * 100 random vector of indices
             */
            std::vector< std::vector<int> > indices;

            /**
             * Group wide equation
             */
            EquationPtr equation;

            /**
             * Shape of group
             */
            unsigned int width, height;
        };

    } // namespace core
} //namespace dana

#endif
