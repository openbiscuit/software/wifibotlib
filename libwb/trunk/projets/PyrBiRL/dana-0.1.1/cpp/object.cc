/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <sstream>
#include "object.h"

using namespace dana::core;
using namespace std;

// ----------------------------------------------------------------- uid_counter
unsigned long Object::uid_counter = 1;

// ---------------------------------------------------------------------- Object
Object::Object (void) : Atom()
{
    uid = uid_counter++;
    std::ostringstream n;
    n << "Object_" << uid;
    name = n.str();
}

// ---------------------------------------------------------------------- Object
Object::Object(const Object &other)
{
    uid = uid_counter++;
    std::ostringstream n;
    n << "Object_" << uid;
    name = n.str();
}

// --------------------------------------------------------------------- ~Object
Object::~Object (void)
{}


// ----------------------------------------------------------------------- clone
Object *
Object::clone (void) const
{
    return new Object(*this);
}

// ------------------------------------------------------------------- operator=
Object &
Object::operator=(const Object &other)
{
    return *this;
}

// ------------------------------------------------------------------------ repr
std::string
Object::repr(void)
{
    return get_name();
}

// -------------------------------------------------------------------- get_type
std::string
Object::get_type (void)
{
    return ("Object");
}

// --------------------------------------------------------------------- get_uid
unsigned long
Object::get_uid (void)
{
    return uid;
}

// -------------------------------------------------------------------- get_name
std::string
Object::get_name (void)
{
    return name;
}

// -------------------------------------------------------------------- set_name
void
Object::set_name (std::string n)
{
    name = n;
}

// -------------------------------------------------------------- read_attribute
std::string
dana::core::read_attribute (xmlTextReaderPtr reader,
                            const char *name)
{
    xmlChar *tmp = xmlTextReaderGetAttribute (reader, BAD_CAST name);
    if (tmp != NULL) {
        std::string value = (char *) tmp;
        xmlFree (tmp);
        return value;
    }
    return std::string("");
}

// ------------------------------------------------------------------------ read
int
Object::read (xmlTextReaderPtr reader)
{
    std::istringstream iss;
    std::string name;
    iss.clear();
    iss.str (read_attribute (reader, "name"));
    iss >> name;
    set_name (name);
    return 0;
}

// ----------------------------------------------------------------------- write
int
Object::write (xmlTextWriterPtr writer)
{
    // <Object>
    xmlTextWriterStartElement (writer,
                               BAD_CAST "Object");    
    xmlTextWriterWriteFormatAttribute (writer,
                                       BAD_CAST "name",
                                       "%s", get_name().c_str());
    // </Object>
    xmlTextWriterEndElement (writer);

    return 0;
}

// ------------------------------------------------------------------------ read
int
Object::read (const std::string filename)
{
    xmlTextReaderPtr reader;
    xmlReaderTypes   type   = XML_READER_TYPE_NONE;
    std::string      name   = "";
    int              status = 1;
    std::string version, date, author;
    
    reader = xmlReaderForFile (filename.c_str(), NULL, 1);
    if  (reader == NULL) {
        fprintf (stderr, "Unable to open %s\n", filename.c_str());
        return 1;
    }
        
    do  {
        status = xmlTextReaderRead (reader);
        if (status != 1)
            break;
        name = (char *) xmlTextReaderConstName(reader);
        type = (xmlReaderTypes) xmlTextReaderNodeType(reader);
        
        if (type == XML_READER_TYPE_ELEMENT) {
            if (name == "dana") {
                version = read_attribute (reader, "version");
                date    = read_attribute (reader, "date");
                author  = read_attribute (reader, "author");
                printf("Version: %s\n", version.c_str());
                printf("Date: %s\n",    date.c_str());
                printf("Author: %s\n",  author.c_str());
            }
            else if (name == "script") {
            }
            else {
                read (reader);
            }
        }
    } while (status == 1);

    xmlFreeTextReader(reader);
    if (status != 0) {
        fprintf (stderr, "%s : failed to parse\n", filename.c_str());
        return 1;
    }
    return 0;
}


// ----------------------------------------------------------------------- write
int
Object::write (const std::string filename)
{
    xmlTextWriterPtr writer;
    std::string uri = filename;

    // Collect information

    // Get time
    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);
    std::string date = asctime(timeinfo);
    date.erase (date.size()-1);   

    // Get author name
    std::string author = getlogin();

    // Get version
    std::string version = "1.0";
    
    // Get comment
    std::string comment = "";
    
    // Actual save
    writer = xmlNewTextWriterFilename (uri.c_str(), 1);
    if (writer == NULL) {
        printf ("Error creating the xml writer\n");
        return 1;
    }
    xmlTextWriterSetIndent (writer, 1);
    xmlTextWriterSetIndentString (writer, BAD_CAST "    ");    
    xmlTextWriterStartDocument (writer, NULL, "utf-8", NULL);

    // <dana>
    xmlTextWriterStartElement (writer,
                               BAD_CAST "dana");
    xmlTextWriterWriteAttribute (writer,
                                 BAD_CAST "version",
                                 BAD_CAST version.c_str());
    xmlTextWriterWriteAttribute (writer,
                                 BAD_CAST "date",
                                 BAD_CAST date.c_str());
    xmlTextWriterWriteAttribute (writer,
                                 BAD_CAST "author",
                                 BAD_CAST author.c_str());

    // <script>
    // xmlTextWriterStartElement (writer, BAD_CAST "script");
    // xmlTextWriterWriteAttribute (writer,
    //                             BAD_CAST "filename",
    //                             BAD_CAST script_file.c_str());
    // xmlTextWriterWriteAttribute (writer,
    //                             BAD_CAST "saver",
    //                             BAD_CAST script_saver.c_str());
    // xmlTextWriterWriteString (writer,
    // BAD_CAST script_text.c_str());
    // </script>
    // xmlTextWriterEndElement (writer);

    // <Object>
    write (writer);
    // </Object>

    // </dana>
    xmlTextWriterEndDocument(writer);
    
    xmlFreeTextWriter(writer);
    return 0;
}
