/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <sstream>
#include <cmath>
#include "unit.h"
#include "link.h"
#include "group.h"

using namespace dana::core;
using namespace std;

// ------------------------------------------------------------------------ Unit
Unit::Unit(void) : Object()
{
    std::ostringstream n;
    n << "Unit_" << uid;
    name = n.str();
    potential = 0.0f;
    x = -1;
    y = -1;
}

// ------------------------------------------------------------------------ Unit
Unit::Unit(const Unit &other) : Object()
{
    //    name = other.name;
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
}

// ------------------------------------------------------------------- operator=
Unit &
Unit::operator=(const Unit &other)
{
    if (this == &other) return *this;
    name = other.name;
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
    return *this;
}

// ----------------------------------------------------------------------- ~Unit
Unit::~Unit(void)
{}


// ----------------------------------------------------------------------- clone
Unit *
Unit::clone (void) const
{
    return new Unit(*this);
}

// ------------------------------------------------------------------------ repr
std::string
Unit::repr (void)
{
    std::ostringstream s;
    s << potential;
    return s.str();
    //return Object::repr();
}

// -------------------------------------------------------------------- get_type
std::string
Unit::get_type (void)
{
    return ("Unit");
}


// ------------------------------------------------------------------ compute_dp
float
Unit::compute_dp (float dt)
{
    /*
    std::map<std::string,LinkVecPtr>::iterator m_iter;
    std::vector<LinkPtr>::iterator v_iter;
    std::map<std::string, float> inputs;
    for(m_iter = links.begin(); m_iter != links.end(); m_iter++ ) {
        std::vector<LinkPtr>::iterator begin = (*m_iter).second->begin();
        std::vector<LinkPtr>::iterator end   = (*m_iter).second->end();
        inputs[(*m_iter).first] = 0.0;
        for(v_iter = begin; v_iter != end; v_iter++ ) {
            inputs[(*m_iter).first] += (*v_iter)->source->potential *
                                       (*v_iter)->weight;
        }
    }
    */
    if (equation) {
        // Reset all values to 0
        memset (equation->values,0,equation->variables.size()*sizeof(double));
        int i=0;
        equation->values[i++] = dt;
        equation->values[i++] = potential;
        for (unsigned int v=0; v<(equation->variables.size()-2); v++) {
            equation->values[i] = get_input (equation->variables[i]);
            i++;
        }
        potential = equation->evaluate (equation->values);
    }
    return potential;
}

// ------------------------------------------------------------------ compute_dw
float
Unit::compute_dw (float dt)
{
    float dw = 0;
    return dw;
}

// ------------------------------------------------------------------- get_input
float
Unit::get_input (std::string name)
{
    if (links.find(name) == links.end())
        return 0.0;
    float input = 0.0f;
    std::vector<LinkPtr>::iterator iter;
    std::vector<LinkPtr>::iterator begin = links[name]->begin();
    std::vector<LinkPtr>::iterator end   = links[name]->end();
    for(iter = begin; iter != end; iter++ )
        input += (*iter)->evaluate();
    return input;
}


// ---------------------------------------------------------------- get_equation
EquationPtr
Unit::get_equation (void)
{
    return EquationPtr(equation);
}

// ---------------------------------------------------------------- set_equation
void
Unit::set_equation (EquationPtr equation)
{
    this->equation = EquationPtr(equation);
}

// --------------------------------------------------------------- get_potential
float
Unit::get_potential (void)
{
    return potential;
}

// --------------------------------------------------------------- set_potential
void
Unit::set_potential (float value)
{
    potential = value;
}

// --------------------------------------------------------------------- connect
void
Unit::connect(UnitPtr source, float weight, std::string type)
{
    if (not links[type])
        links[type] = LinkVecPtr (new LinkVec);
    links[type]->push_back (LinkPtr (new Link (source, weight)));
}

// --------------------------------------------------------------------- connect
void
Unit::connect (GroupPtr source, GroupPtr profile,
               std::string type, float noise, float probability, bool toric)
{
    float pw = profile->width;
    float ph = profile->height;
    float src_w = source->width;
    float src_h = source->height;
    float sx = rint(src_w*x - (pw-.1)/2.0);
    float sy = rint(src_h*y - (ph-.1)/2.0);

    for (int j=0; j<int(ph); j++) {
        for (int i=0; i<int(pw); i++) {
            int src_x = sx+i;
            int src_y = sy+j;
            if (toric) {
                src_x = int(src_x+src_w) % int(src_w);
                src_y = int(src_y+src_h) % int(src_h);
            }
            if ((src_x >= 0) and (src_y >= 0) and (src_x < src_w) and (src_y < src_h)) {
                float v = profile->get_item(j,i)->potential;
                if (noise > 0.0f) v = v+(2*drand48()-1)*noise;
                if (drand48() < probability)
                    this->connect (source->get_item(src_y,src_x), v, type);
            }
        }
    }
}


// ------------------------------------------------------------------- operator*
GroupPtr
Unit::weights (GroupPtr source)
{
    UnitPtr unit(new Unit());
    GroupPtr group = source->size()*unit;

    group->set_shape (source->height, source->width);
    std::map<std::string,LinkVecPtr>::iterator m_iter;
    std::vector<LinkPtr>::iterator v_iter;
    for(m_iter = links.begin(); m_iter != links.end(); m_iter++ ) {
        std::vector<LinkPtr>::iterator begin = (*m_iter).second->begin();
        std::vector<LinkPtr>::iterator end   = (*m_iter).second->end();
        for(v_iter = begin; v_iter != end; v_iter++ ) {
            UnitPtr unit = (*v_iter)->source;
            int index = source->find (unit);
            if (index > -1) {
                group->units[index]->potential += (*v_iter)->weight; 
            }
        }
    }
    return group;
}

// ------------------------------------------------------------------- operator*
GroupPtr
dana::core::operator* (unsigned int n, const UnitPtr unit)
{
    GroupPtr group = GroupPtr (new Group());
    group->units.clear();
    for (unsigned int i=0; i<n; i++)
        group->units.push_back(UnitPtr(unit->clone()));
    group->set_shape (group->units.size(), 1);
    return group;
}

// ------------------------------------------------------------------- operator*
GroupPtr
Unit::operator* (unsigned int n)
{
    GroupPtr group = GroupPtr (new Group());
    group->units.clear();
    for (unsigned int i=0; i<n; i++)
        group->units.push_back(UnitPtr(this->clone()));
    group->set_shape (group->units.size(), 1);
    return group;
}

// ------------------------------------------------------------------- operator+
GroupPtr
Unit::operator+ (const UnitPtr unit)
{
    GroupPtr group = GroupPtr (new Group());
    group->units.clear();
    group->units.push_back(UnitPtr(this->clone()));
    group->units.push_back(unit);
    group->set_shape (group->units.size(), 1);
    return group;
}



// --------------------------------------------------------------------- SynUnit
SynUnit::SynUnit(void) : Unit()
{
    _potential = 0.0f;
}

// --------------------------------------------------------------------- SynUnit
SynUnit::SynUnit(const SynUnit &other) : Unit(other)
{
    _potential = other._potential;
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
}

// ------------------------------------------------------------------- operator=
SynUnit &
SynUnit::operator=(const SynUnit &other)
{
    if (this == &other) return *this;
    //name = other.name;
    potential = other.potential;
    _potential = other._potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
    return *this;
}

// -------------------------------------------------------------------- ~SynUnit
SynUnit::~SynUnit(void)
{}

// -------------------------------------------------------------------- get_type
std::string
SynUnit::get_type (void)
{
    return ("SynUnit");
}

// ----------------------------------------------------------------------- clone
SynUnit *
SynUnit::clone (void) const
{
    return new SynUnit(*this);
}

// ------------------------------------------------------------------ compute_dp
float
SynUnit::compute_dp(float dt)
{
    float p = potential;
    Unit::compute_dp (dt);
    _potential = potential;
    potential = p;
    return _potential;
}

// ------------------------------------------------------------------ compute_dw
float
SynUnit::compute_dw(float dt)
{
    potential = _potential;
    return Unit::compute_dw(dt);
}




// ----------------------------------------------------------------------- KUnit
KUnit::KUnit(void) : Unit()
{}

// ----------------------------------------------------------------------- KUnit
KUnit::KUnit(const KUnit &other) : Unit(other)
{
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
}

// ------------------------------------------------------------------- operator=
KUnit &
KUnit::operator=(const KUnit &other)
{
    if (this == &other) return *this;
    //name = other.name;
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
    return *this;
}

// ---------------------------------------------------------------------- ~KUnit
KUnit::~KUnit(void)
{}

// -------------------------------------------------------------------- get_type
std::string
KUnit::get_type (void)
{
    return ("KUnit");
}

// ----------------------------------------------------------------------- clone
KUnit *
KUnit::clone (void) const
{
    return new KUnit(*this);
}

// ------------------------------------------------------------------- get_input
float
KUnit::get_input (std::string name)
{
    if (links.find(name) == links.end())
        return 0.0;
    if (name != "input")
        return Unit::get_input(name);
    float input = 0.0f;
    std::vector<LinkPtr>::iterator iter;
    std::vector<LinkPtr>::iterator begin = links[name]->begin();
    std::vector<LinkPtr>::iterator end   = links[name]->end();
    for(iter = begin; iter != end; iter++ ) {
        float potential = (*iter)->get_source()->get_potential();
        float weight = (*iter)->get_weight();
        input += (potential-weight)*(potential-weight);
    }
    input = sqrt(input / links[name]->size());
    return input;
}



// --------------------------------------------------------------------- DeadUnit
DeadUnit::DeadUnit(void) : Unit()
{
    potential = NAN;
}

// --------------------------------------------------------------------- DeadUnit
DeadUnit::DeadUnit(const DeadUnit &other) : Unit(other)
{
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
}

// ------------------------------------------------------------------- operator=
DeadUnit &
DeadUnit::operator=(const DeadUnit &other)
{
    if (this == &other) return *this;
    //name = other.name;
    potential = other.potential;
    equation = other.equation;
    x = other.x;
    y = other.y;
    return *this;
}

// ----------------------------------------------------------------------- ~Unit
DeadUnit::~DeadUnit(void)
{}

// -------------------------------------------------------------------- get_type
std::string
DeadUnit::get_type (void)
{
    return ("DeadUnit");
}

// ----------------------------------------------------------------------- clone
DeadUnit *
DeadUnit::clone (void) const
{
    return new DeadUnit(*this);
}

// ------------------------------------------------------------------ compute_dp
float
DeadUnit::compute_dp(float dt)
{
    return 0.0f;
}

// ------------------------------------------------------------------ compute_dw
float
DeadUnit::compute_dw(float dt)
{
    return 0.0f;
}

// --------------------------------------------------------------- set_potential
void
DeadUnit::set_potential (float value)
{}

// --------------------------------------------------------------- set_potential
float
DeadUnit::get_potential (void)
{
    return NAN;
}
