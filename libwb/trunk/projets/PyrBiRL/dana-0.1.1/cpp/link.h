/*
 * DANA - Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_LINK_H__
#define __DANA_CORE_LINK_H__

#include <map>
#include <vector>
#include "atom.h"
#include "object.h"

namespace dana {
    namespace core {

        typedef boost::shared_ptr<class Link> LinkPtr;
        typedef boost::shared_ptr<class Unit> UnitPtr;
        // typedef std::vector<LinkPtr> LinkVec;
        typedef boost::shared_ptr<class LinkVec> LinkVecPtr;
        //        typedef std::map<std::string, LinkVecPtr> LinkMap;
        typedef boost::shared_ptr<class LinkMap> LinkMapPtr;

        class Link : public Atom {
        public:
            Link (void);
            Link (UnitPtr source, float weight=0.0f);
            virtual ~Link (void);

            std::string repr (void) const;

            float         evaluate (void);
            UnitPtr       get_source (void) const;
            void          set_source (const UnitPtr source);
            virtual float get_weight (void) const;
            virtual void  set_weight (const float weight);

        public:
            UnitPtr source;
            float   weight;
        };

        class LinkVec : public std::vector<LinkPtr> {
        public:
            LinkVec(void) {};
            LinkVec(const LinkVec &v) : std::vector<LinkPtr>(v) {};
            LinkVec(size_type num, const LinkPtr &val = LinkPtr()) 
                : std::vector<LinkPtr>(num,val) {};
            LinkVec(std::vector<LinkPtr>::iterator start,
                    std::vector<LinkPtr>::iterator end)
                    : std::vector<LinkPtr>(start,end) {};

            std::string repr (void) const;
        };

        class LinkMap : public std::map<std::string, LinkVecPtr> {
        public:
            LinkMap()
                : std::map<std::string, LinkVecPtr>() {};
            LinkMap( const LinkMap& m )
                : std::map<std::string, LinkVecPtr>(m) {};
            LinkMap (std::map<std::string, LinkVecPtr>::iterator start,
                     std::map<std::string, LinkVecPtr>::iterator end)
                : std::map<std::string, LinkVecPtr>(start,end) {};
            LinkMap (std::map<std::string, LinkVecPtr>::iterator start,
                     std::map<std::string, LinkVecPtr>::iterator end,
                     const key_compare& cmp)
                : std::map<std::string, LinkVecPtr>(start,end,cmp) {};
            LinkMap (const key_compare& cmp)
                : std::map<std::string, LinkVecPtr>(cmp) {};
            
            std::string repr (void) const;
        };

        
    } // namespace core
} //namespace dana

#endif
