/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>

#include "equation.h"

using namespace dana::core;
using namespace std;

// -------------------------------------------------------------------- Equation
Equation::Equation(void) : Object()
{
    definition= "0.0";
    valid = false;
    values = 0;
    constants = ConstantsPtr (new Constants());
}

// ------------------------------------------------------------------- ~Equation
Equation::~Equation(void)
{}

// -------------------------------------------------------------------- get_type
std::string
Equation::get_type (void) const
{
    return ("Equation");
}

// ------------------------------------------------------------------------ repr
std::string
Equation::repr (void) const
{
    std::string text = "f(";
    text += get_variables();
    text += ") = ";
    text += definition;
    return text;
}

// -------------------------------------------------------------- set_definition
void
Equation::set_definition (std::string definition)
{
    this->definition = definition;
    parse();
}

// -------------------------------------------------------------- get_definition
std::string
Equation::get_definition (void) const
{
    return definition;
}

// --------------------------------------------------------------- set_variables
void
Equation::set_variables (std::string vars)
{
    variables.clear();
    std::string token;
    std::istringstream iss(vars);
    while (getline(iss, token, ',') ) {
        size_t startpos = token.find_first_not_of(" \t");
        size_t endpos   = token.find_last_not_of(" \t");
        if ((string::npos == startpos) || (string::npos == endpos))
            token = "";
        else {
            token = token.substr (startpos, endpos-startpos+1);
            variables.push_back (token);
        }
    }
    if (values)
        delete [] values;
    values = new double[variables.size()];
    memset (values, 0, variables.size()*sizeof(double));
    //    parse();
}

// --------------------------------------------------------------- get_variables
std::string
Equation::get_variables (void) const
{
    std::string v;
    for (unsigned int i=0; i<variables.size(); i++) {
        if (i == (variables.size()-1)) {
            v += variables[i];
        } else {
            v += variables[i];
            v += ",";
        }
    }
    return v;
}

// --------------------------------------------------------------- get_constants
ConstantsPtr
Equation::get_constants (void)
{
    return ConstantsPtr(constants);
}

// --------------------------------------------------------------- set_constants
void
Equation::set_constants (ConstantsPtr constants)
{
    this->constants = constants;
    std::map<std::string, float>::iterator iter;
    for (iter=constants->constants.begin(); iter!=constants->constants.end(); iter++)
        parser.AddConstant ((*iter).first, (*iter).second);    
}

// -------------------------------------------------------------------- evaluate
float
Equation::evaluate (double *vals)
{
    if (not valid)
        return 0.0;
    return parser.Eval(vals);
}

// ----------------------------------------------------------------------- parse
void
Equation::parse (void)
{
    if (constants) {
        std::map<std::string, float>::iterator iter;
        for (iter=constants->constants.begin(); iter!=constants->constants.end(); iter++)
            parser.AddConstant ((*iter).first, (*iter).second);
    }
    if (parser.Parse(definition, get_variables()) == -1) {
        parser.Optimize();
        valid = true;
    } else {
        printf("Equation parser error: %s\n", parser.ErrorMsg());
        valid = false;
    }
}
