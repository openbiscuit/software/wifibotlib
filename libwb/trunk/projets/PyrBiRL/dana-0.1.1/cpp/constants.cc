/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include "constants.h"

using namespace dana::core;
using namespace std;

// ------------------------------------------------------------------- Constants
Constants::Constants(void) : Object()
{
}

// ------------------------------------------------------------------- Constants
Constants::Constants(const Constants &other) : Object()
{
    constants.clear();
    std::map<std::string,float>::const_iterator iter;
    for (iter=constants.begin(); iter != constants.end(); iter++)
        constants[(*iter).first] = (*iter).second;
}

// ------------------------------------------------------------------- operator=
Constants &
Constants::operator=(const Constants &other)
{
    if (this == &other) return *this;
    constants.clear();
    std::map<std::string,float>::const_iterator iter;
    for (iter=constants.begin(); iter != constants.end(); iter++)
        constants[(*iter).first] = (*iter).second;
    return *this;
}

// ------------------------------------------------------------------- ~Constants
Constants::~Constants(void)
{}

// -------------------------------------------------------------------- get_type
std::string
Constants::get_type (void)
{
    return ("Constants");
}

// ------------------------------------------------------------------------ repr
std::string
Constants::repr (void)
{
    std::ostringstream s;
    s << '{';
    std::map<std::string,float>::iterator iter;
    for (iter=constants.begin(); iter != constants.end(); iter++) {
        s << (*iter).first << ": " << (*iter).second << ", ";
    }
    s << '}';
    return s.str();
}

// ------------------------------------------------------------------------ size
int
Constants::size (void)
{
    return constants.size();
}

// -------------------------------------------------------------------- get_item
float
Constants::get_item (std::string name)
{
    try {
        return constants[name];
    } catch (...) {
        constants[name] = 0.0;
    }
    return constants[name];
}

// -------------------------------------------------------------------- del_item
void
Constants::del_item (std::string name)
{
    constants.erase(name);
}

// -------------------------------------------------------------------- set_item
void
Constants::set_item (std::string name, float value)
{
    constants[name] = value;
}

// -------------------------------------------------------------------- contains
bool
Constants::contains(std::string name)
{
    std::map<std::string,float>::iterator iter;
    for (iter=constants.begin(); iter != constants.end(); iter++)
        if ((*iter).first == name)
            return true;
    return false;
}

