/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include "unit.h"
#include "group.h"
#include "network.h"

using std::random_shuffle;
using namespace dana::core;
using namespace std;


// ----------------------------------------------------------------------- Group
Group::Group(void) : Object()
{
    std::ostringstream n;
    n << "Group_" << uid;
    name = n.str();
    width = 0;
    height = 0;
    potentials = 0;
}

// ----------------------------------------------------------------------- Group
Group::Group(const Group &other) : Object()
{
    units.clear();
    potentials = 0;
    std::vector<UnitPtr>::const_iterator iter;
    for (iter=other.units.begin(); iter != other.units.end(); iter++)
        units.push_back (UnitPtr(*iter));
    name = other.name;
    set_shape (other.height, other.width);
    equation = other.equation;
}

// ----------------------------------------------------------------------- Group
Group::Group (const unsigned int height,
              const unsigned int width,
              const Unit &unit) : Object()
{
    potentials = 0;
    for (unsigned int i=0; i<(width*height); i++)
        units.push_back (UnitPtr (unit.clone()));
    set_shape (height, width);
}

// ----------------------------------------------------------------------- Group
Group::Group (const unsigned int size, const Unit &unit) : Object()
{
    Group (size, 1, unit);
}

// ------------------------------------------------------------------- operator=
Group &
Group::operator=(const Group &other)
{
    if (this == &other) return *this;
    units.clear();
    std::vector<UnitPtr>::const_iterator iter;
    for (iter=other.units.begin(); iter != other.units.end(); iter++)
        units.push_back (UnitPtr(*iter));
    name = other.name;
    set_shape (other.height, other.width);
    equation = other.equation;
    return *this;
}

// ---------------------------------------------------------------------- ~Group
Group::~Group(void)
{
    units.clear();
}

// ----------------------------------------------------------------------- clone
Group *
Group::clone (void) const
{
    return new Group(*this);
}


// ------------------------------------------------------------------------- run
void
Group::run(float duration, float dt)
{
    float t=0;
    while (t <= duration) {
        compute_dp (dt);
        compute_dw (dt);
        t += dt;
    }
}

// ------------------------------------------------------------------ compute_dp
float
Group::compute_dp(float dt)
{
    float dp = 0.0f;
    int index = rand()%100;
    for (int i=0; i<size(); i++)
        dp += units[indices[index][i]]->compute_dp (dt);
    return dp;
}


// ------------------------------------------------------------------ compute_dw
float
Group::compute_dw(float dt)
{
    float dw = 0.0f;
    int index = rand()%100;
    for (int i=0; i<size(); i++)
        dw += units[indices[index][i]]->compute_dw (dt);
    return dw;
}


// ---------------------------------------------------------------- get_equation
EquationPtr
Group::get_equation (void)
{
    return EquationPtr(equation);
}


// ---------------------------------------------------------------- set_equation
void
Group::set_equation (EquationPtr equation)
{
    this->equation = EquationPtr(equation);
    std::vector<UnitPtr>::const_iterator iter;
    for (iter=units.begin(); iter != units.end(); iter++)
        (*iter)->set_equation (EquationPtr(equation));
}

// --------------------------------------------------------------------- connect
void
Group::connect (GroupPtr other, GroupPtr profile,
                std::string type, float noise, float probability, bool toric)
{
    for (int i=0; i<size(); i++)
        units[i]->connect (other, profile, type, noise, probability, toric);
}


// ------------------------------------------------------------------------ repr
std::string
Group::repr (void)
{
    std::ostringstream s;
    s << "[";
    for (unsigned int j=0; j<height; j++) {
        if (j == 0)
            s << "[";
        else
            s << " [";
        for (unsigned int i=0; i<width; i++) {
            if (i == (width-1))
                s << get_item(j,i)->repr() << "]";
            else
                s << get_item(j,i)->repr() << ", ";
        }
        if (j == (height-1))
            s << "]\n";
        else
            s << ",\n";
    }
    return s.str();
}

// ------------------------------------------------------------------------ size
int
Group::size (void)
{
    return units.size();
}


// -------------------------------------------------------------------- contains
bool
Group::contains (UnitPtr unit)
{
    std::vector<UnitPtr>::iterator iter;
    for (iter=units.begin(); iter != units.end(); iter++)
        if ((*iter) == unit)
            return true;
    return false;
}

// -------------------------------------------------------------------- contains
int
Group::find (UnitPtr unit)
{
    for (unsigned int i=0; i<units.size(); i++)
        if (units[i] == unit)
            return i;
    return -1;
}


// -------------------------------------------------------------------- get_item
UnitPtr
Group::get_item (int index)
{
    if (index < 0)
        index += size();
    if ((index >= 0) and (index < size())) {
        return UnitPtr(units[index]);
    }
    throw std::out_of_range("Index out of range");
}

// -------------------------------------------------------------------- get_item
UnitPtr
Group::get_item (int y, int x)
{
    if (x < 0)
        x += width;
    if (y < 0)
        y += height;
    return UnitPtr(units[y*width+x]);
}


// ------------------------------------------------------------------- get_slice
GroupPtr
Group::get_slice (int start, int stop, int step)
{
    GroupPtr group = GroupPtr (new Group());
    group->units.clear();
    if (start < 0)
        start += size();
    if (stop < 0)
        stop += size();
    if ((start > size()) or (stop > size()))
        throw std::out_of_range("Index out of range");
    int i = start;
    while (i<stop) {
        group->units.push_back (UnitPtr(units[i]));
        i += step;
    }
    return group;
}


// -------------------------------------------------------------------- set_item
void
Group::set_item (int index, UnitPtr unit)
{
    if (index < 0)
        index += size();
    if ((index >= 0) and (index < size())) {
        units[index] = UnitPtr(unit);
        return;
    }
    throw std::out_of_range("Index out of range");
}

// ------------------------------------------------------------------- set_slice
void
Group::set_slice (int start, int stop, int step, GroupPtr group)
{
    std::vector<int> indices;
    if (start < 0)
        start += size();
    if (stop < 0)
        stop += size();
    if ((start > size()) or (stop > size()))
        throw std::out_of_range("Index out of range");
    int i = start;
    while (i<stop) {
        indices.push_back (i);
        i += step;
    }
    if (int(indices.size()) != group->size()) {
        std::ostringstream s;
        s << "Attempt to assign sequence of size " <<  group->size();
        s << " to extended slice of size " << indices.size();
        throw std::out_of_range (s.str());
    } else {
        for (unsigned int i=0; i<indices.size();i++)
            units[indices[i]] = UnitPtr(group->units[i]);
        return;
    }
    throw std::out_of_range("Index out of range");
}


// -------------------------------------------------------------------- del_item
void
Group::del_item (int index)
{
    if (index < 0)
        index += size();
    if ((index >= 0) and (index < size())) {
        units.erase (units.begin()+index);
        set_shape(size(),1);
        return;
    }
    throw std::out_of_range("Index out of range");    
}

// ------------------------------------------------------------------- del_slice
void
Group::del_slice (int start, int stop, int step)
{
    GroupPtr group = GroupPtr (new Group());
    std::vector<int> indices;
    group->units.clear();
    if (start < 0)
        start += size();
    if (stop < 0)
        stop += size();
    if ((start > size()) or (stop > size()))
        throw std::out_of_range("Index out of range");
    for (int i=0; i<size(); i++)
        indices.push_back (i);
    int i = start;
    while (i<stop) {
        std::vector<int>::iterator where;
        where = std::find(indices.begin(), indices.end(), i);
        if (where != indices.end())
            indices.erase (where);
        i += step;
    }
    std::vector<int>::iterator iter;
    for (iter=indices.begin(); iter != indices.end(); iter++)
        group->units.push_back (UnitPtr(units[*iter]));
    units = group->units;
    set_shape(group->size(),1);
}


// -------------------------------------------------------------------- get_type
std::string
Group::get_type (void)
{
    return ("Group");
}

// ------------------------------------------------------------------- set_shape
void
Group::set_shape (unsigned int h, unsigned int w)
{
    if (int(w*h) != size())
        throw std::domain_error("Shape does not correspond to size");

    width = w;
    height = h;
    for (unsigned int x = 0; x < width; x++) {
        for (unsigned int y = 0; y < height; y++) {
            int index = y*w+x;
            if (index < size()) {
                units[index]->x = x/float(width);
                units[index]->y = y/float(height);
            }
        }
    }

    // Generate random indices
    indices.clear();

    /*
    // Method 1: random
    for (unsigned int i=0; i<100; i++) {
        std::vector<int> vi;
        for (int j=0; j<size(); j++)
            vi.push_back(rand()%size());
        indices.push_back (vi);
    }
    */
    // Method 2: permutation
    std::vector<int> vi;
    for (int i=0; i<size(); i++)
        vi.push_back(i);
    for (unsigned int i=0; i<100; i++) {
        random_shuffle(vi.begin(), vi.end());
        indices.push_back (vi);
    }

    if (potentials != 0)
        delete [] potentials;
    potentials = new float[width*height];
}

// ------------------------------------------------------------------- operator+
NetworkPtr
Group::operator+ (const GroupPtr group)
{
    NetworkPtr net = NetworkPtr (new Network());
    net->groups.push_back (GroupPtr(this->clone()));
    net->groups.push_back (GroupPtr (group));
    return net;
}

// --------------------------------------------------------------- get_potentials
float *
Group::get_potentials(void)
{
    if (not potentials)
        set_shape (height, width);
    for (int i=0; i<size(); i++)
        potentials[i] = units[i]->potential;
    return potentials;
}
