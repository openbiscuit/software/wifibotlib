/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_NETWORK_H__
#define __DANA_CORE_NETWORK_H__

#include <map>
#include "object.h"
#include "unit.h"
#include "group.h"
#include "equation.h"


namespace dana {
    namespace core {

        typedef boost::shared_ptr <class Network> NetworkPtr;
        NetworkPtr operator+(const GroupPtr group, const NetworkPtr net);

        /**
         * Network class 
         *
         * A Network is a set of Groups.
         */
        class Network: public Object {

        public:
            
            /**
             * @name Creation/Destruction
             */
            /**
             * Default constructor
             */
            Network (void);

            /**
             * Copy constructor
             */            
            Network (const Network &other);

            /**
             * Copy
             */            
            virtual Network &operator=(const Network &other);

            /**
             * Destructor
             */                        
            virtual ~Network(void);

            /*
             * Clone
             */
            Network *clone (void) const;
            //@}

            /**
             * Compute dp and dw for duration using dt as elementary time step
             *
             * @param duration time to run
             * @param dt elementary time step
             */ 
            virtual void run(float duration, float dt);

            /**
             * Compute potential for all units
             *
             * @param dt elementary time step
             */ 
            virtual float compute_dp(float dt);

            /**
             * Compute weights for all units
             *
             * @param dt elementary time step
             */ 
            virtual float compute_dw(float dt);

            /**
             * Get equation
             *
             * @return equation
             */
            virtual EquationPtr get_equation (void);

            /**
             * Set equation.
             *
             * Setting equation for group will set equation for all units
             * within group.
             *
             * @param equation Equation
             */
            virtual void set_equation (EquationPtr equation);
            
            /**
             * Get string representation.
             *
             * @return Object representation as a string
             */
            virtual std::string repr(void);
            
            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type(void);

            /**
             * Get group size
             *
             * @return group size
             */
            virtual int size(void);

            /**
             * Check if item is in group
             *
             * @param  item item to be checked
             * @return      True if item is in group, False else
             */
            virtual bool contains(GroupPtr group);

            /**
             * Get item
             *
             * @param index index of the item to be get
             * @return      specified item
             */
            virtual GroupPtr get_item(int index);

            /**
             * Get a slice
             *
             * @param  start start of the slice
             * @param  stop  stop of the slice
             * @param  step  step to use to go from start to stop
             * @return       network slice
             */
            virtual NetworkPtr get_slice(int start, int stop, int step);

            /**
             * Set an item
             *
             * @param index index of the item to be set
             * @param other item to be set
             */
            virtual void set_item(int index, GroupPtr group);

            /**
             * Set a slice
             *
             * @param start start of the slice
             * @param stop  stop of the slice
             * @param step  step to use to go from start to stop
             * @param slice group to replace the slice
             */
            virtual void set_slice(int start, int stop,
                                   int step, NetworkPtr slice);

            /**
             * Delete an item
             *
             * @param index index of the item to be deleted
             */
            virtual void del_item(int index);

            /**
             * Delete a slice
             *
             * @param start start of the slice
             * @param stop  stop of the slice
             * @param step  step to use to go from start to stop
             *
             */
            virtual void del_slice(int start, int stop, int step);
            
            /**
             * Create a new network with current and other
             *
             * @param  other network to be added
             * @return       new network
             */
            virtual NetworkPtr operator+(const NetworkPtr other);

            /**
             * Merge current network with other
             *
             * @param  other network to be merged with current
             * @return       modified network
             */
            virtual Network & operator+=(const NetworkPtr other);

            /**
             * Create a new network with current and other group
             *
             * @param  other group to be added
             * @return      new group
             */            
            virtual NetworkPtr operator+(const GroupPtr other);

            /**
             * Merge current network with other group
             *
             * @param  other group to be merged
             * @return       modified network
             */                        
            virtual Network & operator+=(const GroupPtr other);

        public:
            /**
             * Groups composing network
             */
            std::vector<GroupPtr> groups;

            /**
             * Network wide equation
             */
            EquationPtr equation;

            /**
             * Network wide constants
             */
            ConstantsPtr constants;
        };

    } // namespace core
} //namespace dana

#endif
