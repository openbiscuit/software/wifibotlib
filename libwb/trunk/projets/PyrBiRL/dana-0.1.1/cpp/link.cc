/*
 * DANA - Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include "link.h"
#include "unit.h"

using namespace dana::core;
using namespace std;


// ------------------------------------------------------------------------ Link
Link::Link (void) : Atom()
{}

// ------------------------------------------------------------------------ Link
Link::Link (UnitPtr source, float weight) : Atom()
{
    this->source = source;
    this->weight = weight;
}

// ----------------------------------------------------------------------- ~Link
Link::~Link (void)
{}

// ------------------------------------------------------------------------ repr
std::string
Link::repr (void) const
{
    std::ostringstream s;

    s << '(';
    if (source) {
        s << source->Object::repr();
        s << ", ";
        s << weight;
    }
    s << ')';
    return s.str();
}

// -------------------------------------------------------------------- evaluate
float
Link::evaluate (void)
{
    if ((source->potential > 0) or (weight > 0))
        return source->potential*weight;
    return 0;
}

// ------------------------------------------------------------------ get_source
UnitPtr
Link::get_source (void) const
{
    return UnitPtr(source);
}

// ------------------------------------------------------------------ set_source
void
Link::set_source (const UnitPtr source)
{
    this->source = source;
}

// ------------------------------------------------------------------ get_weight
float
Link::get_weight (void) const
{
    return weight;
}

// ------------------------------------------------------------------ set_weight
void
Link::set_weight (const float weight)
{
    this->weight = weight;
}

// ------------------------------------------------------------------------ repr
std::string
LinkVec::repr (void) const
{
    std::ostringstream s;
    std::vector<LinkPtr>::iterator iter, iter_start, iter_end;
    iter_start = ((std::vector<LinkPtr> *)this)->begin();
    iter_end = ((std::vector<LinkPtr> *)this)->end();

    s << '[';
    for(iter = iter_start; iter != iter_end; iter++ )
        s << (*iter)->repr() << ", ";
    s << ']';

    return s.str();
}

// ------------------------------------------------------------------------ repr
std::string
LinkMap::repr (void) const
{
    std::ostringstream s;
    std::map<std::string,LinkVecPtr>::iterator iter, iter_start, iter_end;
    iter_start = ((std::map<std::string,LinkVecPtr> *)this)->begin();
    iter_end = ((std::map<std::string,LinkVecPtr> *)this)->end();

    s << '{';
    for(iter = iter_start; iter != iter_end; iter++ )
        s << "'" << (*iter).first << "': " << (*iter).second->repr() << ", ";
    s << '}';
    return s.str();
}

