/*
 * DANA - Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_ATOM_H__
#define __DANA_CORE_ATOM_H__

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>


namespace dana {
    namespace core {

        typedef boost::shared_ptr<class Atom> AtomPtr;

        /**
         * Base class 
         *
         * Atom is the base class for simple objects.
         */
        class Atom : public boost::enable_shared_from_this <Atom> {
        public:

            /**
             * @name Creation/Destruction
             */

            /**
             * Default constructor
             */
            Atom (void);

            /**
             * Copy constructor
             */
            Atom (const Atom &other);
            
            /**
             * Destructor
             */
            virtual ~Atom (void);

            /**
             * Template make function
             *
             * @return A shared pointer on a new X
             */
            template <class X>
            static boost::shared_ptr<X> make(void) {
                boost::shared_ptr<X> x =
                    boost::dynamic_pointer_cast<X>
                    (boost::shared_ptr<X> (new X()));
                assert(x);
                return x; 
            }

            /**
             * Clone
             *
             * @return A shared pointer on a clone of this as X
             */

            Atom *clone (void) {return new Atom(*this);}

            /*
            template <class X>
            boost::shared_ptr<X> clone(void) {
                boost::shared_ptr<X> x =
                    boost::dynamic_pointer_cast<X>
                    (boost::shared_ptr<X> (new X(static_cast<const X &>(*this))));
                assert(x);
                return x; 
            }
            */
            //@}


            /**
             * @name Object management
             */
            /**
             * C++ type
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);

            /**
             * Self
             *
             * @return shared pointer on this if any
             */
            virtual AtomPtr get_self (void);

            /**
             * Reference count
             *
             * @return number of references on this object
             */
            virtual int get_ref_count (void);
            //@}
        };
    } // namespace core
} //namespace dana

#endif
