/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "network.h"

using namespace dana::core;
using namespace std;


// --------------------------------------------------------------------- Network
Network::Network(void) : Object()
{
    std::ostringstream n;
    n << "Network_" << uid;
    name = n.str();
}

// --------------------------------------------------------------------- Network
Network::Network(const Network &other) : Object()
{
    groups.clear();
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=other.groups.begin(); iter != other.groups.end(); iter++)
        groups.push_back (GroupPtr(*iter));
    name = other.name;
    equation = other.equation;
}

// ------------------------------------------------------------------- operator=
Network &
Network::operator=(const Network &other)
{
    if (this == &other) return *this;
    groups.clear();
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=other.groups.begin(); iter != other.groups.end(); iter++)
        groups.push_back (GroupPtr(*iter));
    name = other.name;
    equation = other.equation;
    return *this;
}

// -------------------------------------------------------------------- ~Network
Network::~Network(void)
{
    groups.clear();
}

// ----------------------------------------------------------------------- clone
Network *
Network::clone (void) const
{
    return new Network(*this);
}

// ------------------------------------------------------------------------- run
void
Network::run(float duration, float dt)
{
    float t=0;
    while (t <= duration) {
        compute_dp (dt);
        compute_dw (dt);
        t += dt;
    }
}

// ------------------------------------------------------------------ compute_dp
float
Network::compute_dp(float dt)
{
    float dp = 0.0f;
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=groups.begin(); iter != groups.end(); iter++)
        dp += (*iter)->compute_dp (dt);
    return dp;
}


// ------------------------------------------------------------------ compute_dw
float
Network::compute_dw(float dt)
{
    float dw = 0.0f;
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=groups.begin(); iter != groups.end(); iter++)
        dw += (*iter)->compute_dw (dt);
    return dw;
}

// ---------------------------------------------------------------- get_equation
EquationPtr
Network::get_equation (void)
{
    return EquationPtr(equation);
}

// ---------------------------------------------------------------- set_equation
void
Network::set_equation (EquationPtr equation)
{
    this->equation = EquationPtr(equation);
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=groups.begin(); iter != groups.end(); iter++)
        (*iter)->set_equation (EquationPtr(equation));
}


// ------------------------------------------------------------------------ repr
std::string
Network::repr (void)
{
    std::ostringstream s;
    s << '[';
    std::vector<GroupPtr>::iterator iter;
    for (iter=groups.begin(); iter != groups.end(); iter++)
        s << (*iter)->repr() << ", ";
    s << ']';
    return s.str();
}

// ------------------------------------------------------------------------ size
int
Network::size (void)
{
    return groups.size();
}


// -------------------------------------------------------------------- contains
bool
Network::contains (GroupPtr group)
{
    std::vector<GroupPtr>::iterator iter;
    for (iter=groups.begin(); iter != groups.end(); iter++)
        if ((*iter) == group)
            return true;
    return false;
}


// -------------------------------------------------------------------- get_item
GroupPtr
Network::get_item (int index)
{
    if (index < 0)
        index += size();
    if ((index >= 0) and (index < size())) {
        return GroupPtr(groups[index]);
    }
    throw std::out_of_range("Index out of range");
}


// ------------------------------------------------------------------- get_slice
NetworkPtr
Network::get_slice (int start, int stop, int step)
{
    NetworkPtr network = NetworkPtr (new Network());
    network->groups.clear();
    if (start < 0)
        start += size();
    if (stop < 0)
        stop += size();
    if ((start > size()) or (stop > size()))
        throw std::out_of_range("Index out of range");
    int i = start;
    while (i<stop) {
        network->groups.push_back (GroupPtr(groups[i]));
        i += step;
    }
    return network;
}


// -------------------------------------------------------------------- set_item
void
Network::set_item (int index, GroupPtr group)
{
    if (index < 0)
        index += size();
    if ((index >= 0) and (index < size())) {
        groups[index] = GroupPtr(group);
        return;
    }
    throw std::out_of_range("Index out of range");
}


// ------------------------------------------------------------------- set_slice
void
Network::set_slice (int start, int stop, int step, NetworkPtr network)
{
    std::vector<int> indices;
    if (start < 0)
        start += size();
    if (stop < 0)
        stop += size();
    if ((start > size()) or (stop > size()))
        throw std::out_of_range("Index out of range");
    int i = start;
    while (i<stop) {
        indices.push_back (i);
        i += step;
    }
    if (int(indices.size()) != network->size()) {
        std::ostringstream s;
        s << "Attempt to assign sequence of size " <<  network->size();
        s << " to extended slice of size " << indices.size();
        throw std::out_of_range (s.str());
    } else {
        for (unsigned int i=0; i<indices.size();i++)
            groups[indices[i]] = GroupPtr(network->groups[i]);
        return;
    }
    throw std::out_of_range("Index out of range");
}


// -------------------------------------------------------------------- del_item
void
Network::del_item (int index)
{
    if (index < 0)
        index += size();
    if ((index >= 0) and (index < size())) {
        groups.erase (groups.begin()+index);
        return;
    }
    throw std::out_of_range("Index out of range");    
}


// ------------------------------------------------------------------- del_slice
void
Network::del_slice (int start, int stop, int step)
{
    NetworkPtr network = NetworkPtr (new Network());
    std::vector<int> indices;
    network->groups.clear();
    if (start < 0)
        start += size();
    if (stop < 0)
        stop += size();
    if ((start > size()) or (stop > size()))
        throw std::out_of_range("Index out of range");
    for (int i=0; i<size(); i++)
        indices.push_back (i);
    int i = start;
    while (i<stop) {
        std::vector<int>::iterator where;
        where = std::find(indices.begin(), indices.end(), i);
        if (where != indices.end())
            indices.erase (where);
        i += step;
    }
    std::vector<int>::iterator iter;
    for (iter=indices.begin(); iter != indices.end(); iter++)
        network->groups.push_back (GroupPtr(groups[*iter]));
    groups = network->groups;
}


// -------------------------------------------------------------------- get_type
std::string
Network::get_type (void)
{
    return ("Network");
}

// ------------------------------------------------------------------- operator+
NetworkPtr
dana::core::operator+(const GroupPtr group, const NetworkPtr other)
{
    NetworkPtr net = NetworkPtr (new Network());
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=other->groups.begin(); iter != other->groups.end(); iter++)
        net->groups.push_back(GroupPtr(*iter));
    net->groups.push_back(GroupPtr(group));
    return net;
}

// ------------------------------------------------------------------- operator+
NetworkPtr
Network::operator+ (const NetworkPtr other)
{
    NetworkPtr net = NetworkPtr (new Network(*this));
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=other->groups.begin(); iter != other->groups.end(); iter++)
        net->groups.push_back(GroupPtr(*iter));
    return net;
}

// ------------------------------------------------------------------ operator+=
Network &
Network::operator+= (const NetworkPtr other)
{
    if (this == other.get()) return *this;
    std::vector<GroupPtr>::const_iterator iter;
    for (iter=other->groups.begin(); iter != other->groups.end(); iter++)
        groups.push_back(GroupPtr(*iter));
    return *this;
}


// ------------------------------------------------------------------- operator+
NetworkPtr
Network::operator+ (GroupPtr other)
{
    NetworkPtr net = NetworkPtr (new Network(*this));
    net->groups.push_back(GroupPtr(other));
    return net;
}


// ------------------------------------------------------------------ operator+=
Network &
Network::operator+= (GroupPtr other)
{
    groups.push_back (GroupPtr(other));
    return *this;
}

