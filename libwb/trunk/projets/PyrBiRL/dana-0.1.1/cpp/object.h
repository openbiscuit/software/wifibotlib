/*
 * DANA -- Distributed Asynchronous Numerical Adaptive computing framework
 * Copyright (C) 2006,2007,2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DANA_CORE_OBJECT_H__
#define __DANA_CORE_OBJECT_H__

#include <libxml/xmlwriter.h>
#include <libxml/xmlreader.h>
#include "atom.h"


namespace dana {
    namespace core {

        /**
         * Helper function to read attribute from an xml stream
         *
         * @param reader opened xml stream
         * @param name   attribute to read value from
         * @return attribute value
         */
        std::string read_attribute (xmlTextReaderPtr reader,
                                    const char *name);

        
        /**
         * Forward declaration of Object shared pointer.
         */
        typedef boost::shared_ptr<class Object> ObjectPtr;


        /**
         * Base class 
         *
         * Object is the base class for complex objects.
         */
        class Object: public Atom {

        public:
            /**
             * Default constructor.
             */
            Object (void);

            /**
             * Copy constructor.
             *
             * @param other object to be copied
             */
            Object (const Object &other);

            /**
             * Copy.
             *
             * @param other object to be copied
             */
            virtual Object &operator=(const Object &other);
            /**
             * Destructor.
             */
            virtual ~Object (void);

            /*
             * Clone
             */
            Object *clone (void) const;

            /**
             * Get C++ type.
             *
             * @return C++ type string
             */
            virtual std::string get_type (void);
            /**
             * Get string representation.
             *
             * @return Object representation as a string
             */
            virtual std::string repr (void);

            /**
             * Get unique identification number.
             *
             * @return unique identification number
             */
            virtual unsigned long get_uid  (void);

            /**
             * Set name.
             *
             * @param name Name
             */
            virtual void set_name (std::string name);
            /**
             * Get name.
             *
             * @return name
             */
            virtual std::string get_name (void);
            
            /**
             * Write object to opened xml stream.
             *
             * @param writer opened xml stream
             * @return 1 if write failed, 0 else
             */            
            virtual int write (xmlTextWriterPtr writer);

            /**
             * Read object from opened xml stream.
             *
             * @param reader opened xml stream
             * @return 1 if read failed, 0 else
             */            
            virtual int read (xmlTextReaderPtr reader);

            /**
             * Write object to filename.
             *
             * @param  filename  filename to write to
             * @return 1 if write failed, 0 else
             */            
            virtual int write (const std::string filename);

            /**
             * Read object from filename
             *
             * @param  filename  filename to read from
             * @return 1 if read failed, 0 else
             */            
            virtual int read  (const std::string filename);

        public:
            /**
             * Identification counter.
             */
            static unsigned long uid_counter;

            /**
             * Identification number.
             *
             * Each object is assigned a unique identification number.
             */
            unsigned long uid;

            /**
             * Object name.
             *
             * Each object can be assigned an arbitray name (a default one is
             * provided) and thus cannot serve as identification.
             */
            std::string name;
        };

    } // namespace core
 
} //namespace dana

#endif
