/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "matrix.h"

using namespace pyrbirl::core;

// ============================================================================
Matrix::Matrix(int row, int col)
{
  _row = row;
  _col = col;
  _M = gsl_matrix_calloc (_row, _col);
}
Matrix::Matrix( const Matrix& A) // -------------------------------------------
{
  _row = A._row;
  _col = A._col;
  _M = gsl_matrix_calloc (_row, _col);
  gsl_matrix_memcpy(_M, A._M);
}
Matrix &
Matrix::operator= ( const Matrix &other ) // ----------------------------------
{
  if (this == &other) return *this;
  _row = other._row;
  _col = other._col;

  _M = gsl_matrix_calloc (_row, _col);
  gsl_matrix_memcpy(_M, other._M);

  return *this;
}
Matrix *
Matrix::clone (void) const // -------------------------------------------------
{
    return new Matrix(*this);
}
// ============================================================================
Matrix::~Matrix(void)
{
  gsl_matrix_free(_M);
}
// ============================================================================
const Matrix
Matrix::dot(const Matrix &other) const
{
  Matrix ret = Matrix(_row, other._col);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
		  1.0, _M, other._M,
		  0.0, ret._M);

  return ret;
}
const Matrix
Matrix::transpose(void) const // ----------------------------------------------
 {
  Matrix ret = Matrix(_col, _row);
  gsl_matrix_transpose_memcpy (ret._M, _M);
  return ret;
}
const Matrix
Matrix::pinv(void) const // ---------------------------------------------------
{
  if(_col > _row)
    return NULL;

  Matrix ret = Matrix(_col, _row);
  gsl_matrix * V = gsl_matrix_calloc (_col, _col);
  gsl_vector * S = gsl_vector_calloc (_col);
  gsl_vector * work = gsl_vector_calloc (_col);
  gsl_matrix * U = gsl_matrix_calloc (_row, _col);
  gsl_matrix_memcpy(U, _M);
  gsl_linalg_SV_decomp (U, V, S, work);

  gsl_matrix * Sp = gsl_matrix_calloc (_col, _col);
  for(unsigned int i=0;i<S->size;i++)
    {
      if( gsl_vector_get(S, i) > 0.00000001)
	{
	  double val = (double)1.0 / gsl_vector_get(S, i);
	  gsl_matrix_set (Sp, i, i, val);
	}
      else
	gsl_matrix_set (Sp, i, i, 0.0);
    }

  /*printf("U\n");
  gsl_matrix_fprintf( stdout, U, "%g" );
  printf("V\n");
  gsl_matrix_fprintf( stdout, V, "%g" );
  printf("S\n");
  gsl_vector_fprintf( stdout, S, "%g" );
  printf("Sp\n");
  gsl_matrix_fprintf( stdout, Sp, "%g" );
*/
  gsl_matrix *m1 = gsl_matrix_calloc (Sp->size1, U->size1);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans,
		  1.0, Sp, U,
		  0.0, m1);

 // printf("m1\n");
 // gsl_matrix_fprintf( stdout, m1, "%g" );


  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
		  1.0, V, m1,
		  0.0, ret._M);

 // printf("ret\n");
 // gsl_matrix_fprintf( stdout, ret->_M, "%g" );

  gsl_matrix_free(V);
  gsl_matrix_free(Sp);
  gsl_matrix_free(U);
  gsl_matrix_free(m1);
  gsl_vector_free(S);
  gsl_vector_free(work);

  return ret;
}
// ============================================================================
// gsl_matrix *CMatrix::Dot(gsl_matrix *A, gsl_matrix *B)
// {
//   if(A->size2 != B->size1)
//     return NULL;

// //  CMatrix *ret = new CMatrix(A->_row, A->_col);
//   gsl_matrix *ret = gsl_matrix_calloc (A->size1, B->size2);
//   gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
// 		  1.0, A, B,
// 		  0.0, ret);
//   return ret;
// }

// CMatrix *CMatrix::Dot(CMatrix *A)
// {
//   CMatrix *ret = new CMatrix(_row, A->_col);

//   gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
// 		  1.0, _M, A->_M,
// 		  0.0, ret->_M);

//   return ret;
// }

// ============================================================================
double
Matrix::max_diag(void)
{
  unsigned int min = _M->size1;
  if(_M->size2 < min)
    min = _M->size2;
  
  double res = get(0, 0);
  for( unsigned int i=0;i<min; ++i) {
    if( get(i, i) > res ) {
      res = get(i, i);
    }
  }
  return res;
}
// ============================================================================
bool 
Matrix::is_null(void)
{
  for(unsigned int i=0;i<_M->size1;i++)
    {
      for(unsigned int j=0;j<_M->size2;j++)
	{
	  if(get(i, j) != 0)
	    return false;
	}
    }
  return true;
}
// ============================================================================
// void CMatrix::Normalize(int dimension)
// {
//   double min, max;
//   int i;
//   min = max = Get(0, dimension);
//   for(i=0;i<_row;i++)
//     {
//       if(min > Get(i, dimension))
// 	min = Get(i, dimension);
//       if(max < Get(i, dimension))
// 	max = Get(i, dimension);
//     }
//   double offset = max - min;
//   for(i=0;i<_row;i++)
//     {
//       Set(i, dimension, (Get(i, dimension)-min)*10/offset);
//     }
// }

// void CMatrix::Print(void)
// { 
//   for(int i=0;i< _row; i++)  
//     {
//       for(int j=0;j<_col;j++)
// 	{
// 	  printf("%g ", gsl_matrix_get (_M, i, j));
// 	}
//       printf("\n");
//     }
// }
// ============================================================================
double
Matrix::get( int row_index, int col_index)
{
  return gsl_matrix_get(_M, row_index, col_index);
}
void
Matrix::set( int row_index, int col_index, double value) // -------------------
{
   gsl_matrix_set (_M, row_index, col_index, value);
}
// ============================================================================
std::string 
Matrix::repr()
{
  std::stringstream strbuf;
  for( int i = 0; i < _row; i++) {
    for( int j = 0; j < _col; j++ ) {
      strbuf << " " << get( i, j );
    }
    strbuf << "\n";
  }

  return strbuf.str();
}
// ============================================================================

namespace pyrbirl {
  namespace core {
    // ============================================================================
    std::ostream&
    operator<< ( std::ostream& os, pyrbirl::core::Matrix &mat)
    {
      os << mat.repr();
      return os;
    }
  }
}
