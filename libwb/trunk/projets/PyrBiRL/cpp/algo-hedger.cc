/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "algo-hedger.h"
#include <boost/foreach.hpp>
#include <limits>
#include <math.h>

using namespace pyrbirl::algorithm;

using namespace log4cxx;
// ============================================================================
#define QDEF (0.0)
#define EPSILON (0.001)
// ============================================================================
LoggerPtr
AlgoHedger::logger(Logger::getLogger("pyrbi.algo.AlgoHedger"));

numeric::Regression
AlgoHedger::regressor;
// ============================================================================
AlgoHedger::AlgoHedger(TKdTreeDoublePtr qval_tree_) : Atom()
{
  qval_tree = qval_tree_;
  ini_param();

  r_min_ = std::numeric_limits<double>::max();
  r_max_ = -std::numeric_limits<double>::max();
  q_min_ = std::numeric_limits<double>::max();
  q_max_ = -std::numeric_limits<double>::max();
}
AlgoHedger::AlgoHedger( const AlgoHedger &other) : Atom() // -----------------
{
  qval_tree = other.qval_tree;
  // parameters
  ivh_tolerance = other.ivh_tolerance;
  nb_pts_for_regression = other.nb_pts_for_regression;
  bandwidth = other.bandwidth;
}
AlgoHedger &
AlgoHedger::operator=( const AlgoHedger &other) // ----------------------------
{
  if (this == &other) return *this;
  return *this;
}
AlgoHedger *
AlgoHedger::clone (void) const // ---------------------------------------------
{
  return new AlgoHedger(*this);
}
// ============================================================================
AlgoHedger::~AlgoHedger()
{
}
// ============================================================================
std::string
AlgoHedger::repr()
{
  std::stringstream strbuf;
  strbuf << "AlgoHedger";
  strbuf << "\n  nb_pts_for_regression = " << get_nb_pts_for_regression();
  strbuf << "\n  ivh_tolerance = " << get_ivh_tolerance();
  strbuf << "\n  bandwidth = " << get_bandwidth().repr();
  strbuf << "\n  alpha = " << get_alpha();
  strbuf << "\n  gamma = " << get_gamma();
  
  strbuf << "\n" ;
  strbuf << "\n(r_min=" << r_min_ << ", r_max=" << r_max_ << ")";
  strbuf << "\n(q_min=" << q_min_ << ", q_max=" << q_max_ << ")";
  return strbuf.str();
}
// ============================================================================
void
AlgoHedger::learn( core::MultiDouble old_state,
		   core::MultiDouble action,
		   core::MultiDouble new_state,
		   double reward )
{

  LOG4CXX_INFO(logger, "\nlearn " << old_state << " + " << action << " => " << new_state << "(" << reward << ")");

  // update r_max_ and r_min_
  if( r_min_ > reward ) {
    r_min_ = reward;
    q_min_ = r_min_ / (1.0 - get_gamma());
  }
  if( r_max_ < reward ) {
    r_max_ = reward;
    q_max_ = r_max_ / (1.0 - get_gamma());
  }
  LOG4CXX_DEBUG(logger, "\nr_min=" << r_min_ << "(" << q_min_ << ") r_max=" << r_max_ << "(" << q_max_ << ")");

  // look for max Q(new_state, action')
  LOG4CXX_INFO(logger, "\n max Q(new_state, action')");

  double q_max_new_state = -std::numeric_limits<double>::max();
  TPrediction request_status;
  BOOST_FOREACH( core::MultiDouble action_pot, list_actions ) {
    // query_pt is the concatenation of state and action
    MultiDouble query_pt = new_state.concat( action_pot );
    
    double res = predict( query_pt, request_status );
    LOG4CXX_DEBUG(logger, "\n query_pt = " << query_pt << " ==> res=" << res << " (status=" << request_status <<")");
    if( res > q_max_new_state ) {
      q_max_new_state = res;
    }
  }
  LOG4CXX_DEBUG(logger, "\nq_max_new_state=" << q_max_new_state);

  // q-value for (old_state+action)
   MultiDouble query_pt = old_state.concat(action);
  double q_old_state = predict( query_pt, request_status);
  LOG4CXX_DEBUG(logger, "\nq_old_state=" << q_old_state << " (status=" << request_status <<")");
    
  // new q-value for old_state
  double new_q_old_state = q_old_state + 
    get_alpha() * (reward + get_gamma() * q_max_new_state - q_old_state);
  LOG4CXX_DEBUG(logger, "\nnew_q_old_state = " << new_q_old_state);


  // mettre à jour les voisins si dans l'IVH ???
  bool is_in_tree = false;
  
  if(( request_status == PRED_EXACT ) || ( request_status == PRED_NORMAL )) {
    // update all neigbors close enough
    
    // look for the neighbors of 'old_state'
    std::vector<TDataPtr> neighbors = qval_tree->get_nearest_approximate( query_pt, get_nb_pts_for_regression() );

  
    BOOST_FOREACH( TDataPtr data, neighbors ) {
      core::MultiDouble neighbor_pt = boost::tuples::get<0>(*data);
      LOG4CXX_DEBUG(logger, "\ncompare " << neighbor_pt.repr() << " with xq=" << query_pt);
    
      double dist_ =  query_pt.square_dist( neighbor_pt );
      if( dist_ < EPSILON ) {
	LOG4CXX_DEBUG(logger, "\ndist = " << dist_ << " ==> same point");
	is_in_tree = true;
	// change value
	boost::tuples::get<1>(*data) = new_q_old_state;
	LOG4CXX_DEBUG(logger, "\nnew_q = " << new_q_old_state);
      }
      else {
	LOG4CXX_DEBUG(logger, "\nneigboring point");
	// actual q_value of neighbor
	double neighbor_q_val = boost::tuples::get<1>(*data);
	// compute weighted_distance
	double weight = sqrt(query_pt.weighted_square_dist( neighbor_pt, bandwidth ));
	// new qval
	neighbor_q_val = get_alpha() * weight * new_q_old_state + neighbor_q_val;
	// change value
	boost::tuples::get<1>(*data) = neighbor_q_val;
	LOG4CXX_DEBUG(logger, "\nnew_q = " << neighbor_q_val << "with weight=" << weight);
      }
    }
  }
    
  // must we add a new point ?
  if( is_in_tree == false ) {
    LOG4CXX_DEBUG(logger, "\nadding new point in tree");
    qval_tree->add( query_pt, new_q_old_state);
  }
}
		   
// ============================================================================
double
AlgoHedger::predict( core::MultiDouble query_pt,
		     TPrediction& status )
{
  // normal status
  status = PRED_NORMAL;

  LOG4CXX_INFO(logger, "\npredict for xq = " << query_pt << " [" << status << "]");

  // look for the neighbors of 'xq'
  std::vector<TDataPtr> neighbors = qval_tree->get_nearest_approximate( query_pt, get_nb_pts_for_regression() );

  // query points is explicitely in the tree ?
  BOOST_FOREACH( TDataPtr data, neighbors ) {
    core::MultiDouble neighbor_pt = boost::tuples::get<0>(*data);
    LOG4CXX_DEBUG(logger, "\ncompare " << neighbor_pt.repr() << " with xq=" << query_pt);
    
    double dist_ =  query_pt.square_dist( neighbor_pt );
    if( dist_ < EPSILON ) {
      LOG4CXX_DEBUG(logger, "\ndist = " << dist_ << " ==> same point");
      status = PRED_EXACT;
      return boost::tuples::get<1>(*data);
    }
  }
  
  // enought points ??
  if( neighbors.size() < get_nb_pts_for_regression() ) {
    LOG4CXX_DEBUG(logger, "\nonly found " << neighbors.size() << " points ==> not enough");
    status = PRED_NOT_ENOUGH;
    return QDEF;
  }

  // check if in ivh 
  if( in_ivh( query_pt, neighbors ) == false) {
    LOG4CXX_DEBUG(logger, "\n==> not in IVH");
    status = PRED_IVH;
    return QDEF;
  }

  // LWR Regression
  double res = regressor.lwr_regression( query_pt, neighbors, bandwidth );
  
  // within proper bounds
  if( (res < q_min_) || (res > q_max_)) {
    LOG4CXX_DEBUG(logger, "\nres=" << res <<" not in [" << q_min_ << ", " << q_max_<< "] ==> not in bounds");
    status = PRED_BOUNDS;
    return QDEF;
  }
  
  // normal computation then
  LOG4CXX_DEBUG(logger, "\nres = " << res );
  return res;
}
// ============================================================================
bool
AlgoHedger::in_ivh( core::MultiDouble query_pt, 
		    std::vector<TDataPtr> vec_neighbors )
{
  // dimension of the upcoming Matrices
  int X_row = vec_neighbors.size();
  int X_col = query_pt.size();
  ++X_col; // will add a 1.0 component
  
  // xq = query_pt
  core::Matrix xq( 1, X_col );
  xq.set(0, X_col-1,1.0);
  for( int col = 0; col < query_pt.size(); ++col ) {
    xq.set( 0, col, query_pt[col]);
  }
  LOG4CXX_DEBUG(logger, "\nxq = " << xq);
  
  // K is made of one row for each neighbor, with an ending 1.0
  core::Matrix K(X_row, X_col);
  std::vector<TDataPtr>::iterator it_vec_neighbors = vec_neighbors.begin();
  for (int row = 0; row < K.nb_row(); ++row ) {
    TDataPtr data = (*it_vec_neighbors);
    core::MultiDouble neighbor = boost::tuples::get<0>( *data );
    
    for (int col = 0; col < K.nb_col(); ++col ) {
      if( col >= query_pt.size() ) { // last component
	// the last columnis a '1'
	K.set( row, col, 1.0 );
      }
      else {
	K.set( row, col, neighbor[col] );
      }
    }
    // next query point
    it_vec_neighbors++;
  }
  LOG4CXX_DEBUG(logger, "\nK = " << K);
  
  // KTK_inv = pinv(K'.K)
  core::Matrix KT = K.transpose();
  LOG4CXX_DEBUG(logger, "\nKT = " << KT);
  core::Matrix KTK_inv = KT.dot(K);
  LOG4CXX_DEBUG(logger, "\nKTK = " << KTK_inv);
  KTK_inv = KTK_inv.pinv();
  LOG4CXX_DEBUG(logger, "\nKTK_inv = " << KTK_inv);

  // V = K.KTK_inv.K'
  core::Matrix V = KTK_inv.dot(KT);
  LOG4CXX_DEBUG(logger, "\nKTK_inv.KT = " << V);
  V = K.dot(V);
  LOG4CXX_DEBUG(logger, "\nV = " << V);
  double max_vi = V.max_diag();
  LOG4CXX_DEBUG(logger, "\nmax_vi = " << max_vi);
  
  // H = X'.KTK_inv.X
  core::Matrix H = xq;
  LOG4CXX_DEBUG(logger, "\nxq = " << H);
  H = H.dot(KTK_inv);
  LOG4CXX_DEBUG(logger, "\nxq.KTK_inv = " << H);
  H = H.dot(xq.transpose());
  LOG4CXX_DEBUG(logger, "\nH = " << H);
  
  if( H.get(0,0) <= get_ivh_tolerance() * max_vi ) {
    return true;
  }
  else {
    return false;
  }
}
// ============================================================================
void
AlgoHedger::set_gamma(double val)
{
  gamma = val;
  q_min_ = r_min_ / (1.0 - get_gamma());
  q_max_ = r_max_ / (1.0 - get_gamma());
}
// ============================================================================
namespace pyrbirl {
  namespace algorithm {
    // ========================================================================
    std::ostream&
    operator<< ( std::ostream& os, pyrbirl::algorithm::AlgoHedger &algo)
    {
      os << algo.repr();
      return os;
    }
  }
}
// ============================================================================
