/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "multi-double.h"

// to read from string
#include <boost/tokenizer.hpp>

using namespace pyrbirl::core;

// ============================================================================
// needed to tokenize string in order to extract MultiDouble
typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
boost::char_separator<char> separator("(), ");
// ============================================================================
MultiDouble::MultiDouble( int size_ ) : Atom()
{
  // set name
  //std::ostringstream n;
  //n << "MultiDouble_" << uid;
  //name = n.str();

  // size and memory allocation
  this->size_ = size_;
  this->data = new double[this->size()];
  
  for( int i = 0; i<this->size(); i++) {
    this->data[i] = 0;
  }
}
MultiDouble::MultiDouble( const MultiDouble &other) : Atom() // ---------------
{
  // size and memory allocation
  this->size_ = other.size_;
  this->data = new double[this->size()];
  
  for( int i = 0; i<this->size(); i++) {
    this->data[i] = other[i];
  }
}
MultiDouble::MultiDouble( const std::string str_) : Atom() // -----------------
{
  Tokenizer tokens(str_, separator);
  // nb of elements
  int _nb_elem = 0;
  for (Tokenizer::iterator tok_iter = tokens.begin();
       tok_iter != tokens.end(); ++tok_iter, ++_nb_elem);
  this->size_ = _nb_elem;
  this->data = new double[this->size()];

  int _index = 0;
  for (Tokenizer::iterator tok_iter = tokens.begin();
       tok_iter != tokens.end(); ++tok_iter, ++_index) {
    this->data[_index] = strtod( (*tok_iter).c_str(), NULL);
  }
}
MultiDouble &
MultiDouble::operator=(const MultiDouble &other) // ---------------------------
{
  if (this == &other) return *this;
  //name = other.name;
  
  this->size_ = other.size_;
  this->data = new double[this->size()];
  for( int i = 0; i<this->size(); i++) {
    this->data[i] = other[i];
  }
  return *this;
}
MultiDouble *
MultiDouble::clone (void) const // --------------------------------------------
{
    return new MultiDouble(*this);
}

// ============================================================================
MultiDouble::~MultiDouble()
{
  delete [] this->data;
}
// ============================================================================
double &
MultiDouble::operator[](int index_) const
{
  return this->data[index_];
}
double * 
MultiDouble::operator*()
{
  return this->data;
}
// ============================================================================
MultiDouble
MultiDouble::concat(MultiDouble &other) 
{
  MultiDouble result_(this->size()+other.size());
  
  int index = 0;
  // copy *this
  for( int i = 0; i < this->size(); ++i) {
    result_.data[index++] = this->data[i];
  }
  // copy other
  for( int i = 0; i < other.size(); ++i) {
    result_.data[index++] = other[i];
  }

  return result_;
}
// ============================================================================
std::string 
MultiDouble::repr()
{
  std::stringstream strbuf;
  strbuf << "(";
  for( int i = 0; i<this->size(); i++) {
    strbuf << " " << this->data[i];
  }
  strbuf << ")";

  return strbuf.str();
}
int
MultiDouble::from_repr (std::string str_)
{
  Tokenizer tokens(str_, separator);
  // nb of elements
  int _nb_elem = 0;
  for (Tokenizer::iterator tok_iter = tokens.begin();
       tok_iter != tokens.end(); ++tok_iter, ++_nb_elem);
  int _index = 0;
  for (Tokenizer::iterator tok_iter = tokens.begin();
       tok_iter != tokens.end(); ++tok_iter, ++_index) {
    //std::cout << "<" << *tok_iter << "> ";
    data[_index] = strtod( (*tok_iter).c_str(), NULL);
  }
  //std::cout << "\n";
  //std::cout << _multi << "\n";
  return EXIT_SUCCESS;
}
// ============================================================================
void 
MultiDouble::write(std::ostream& os)
{
  os << repr() << "\n";
}
void
MultiDouble::read(std::istream& is)
{
  std::string _str;
  std::getline( is, _str);
  from_repr(_str);
}
// ============================================================================
std::string
MultiDouble::get_type (void)
{
    return ("MultiDouble");
}
// ============================================================================
int
MultiDouble::size (void) const
{
  return this->size_;
}
// ============================================================================
double
MultiDouble::square_dist ( const MultiDouble &other ) const
{
  if( size() != other.size() )
    return -1.0;

  double distance = 0.0;
  for( int index = 0; index < size(); index ++ ) {
    distance += (this->data[index] - other[index]) * (this->data[index] - other[index]);
  }
  return distance;
}
double
MultiDouble::weighted_square_dist (const MultiDouble &other, // ---------------
				   const MultiDouble &weight)
{
  if( (size() != other.size()) || (size() != weight.size()))
    return -1.0;

  double distance = 0.0;
  for( int index = 0; index < size(); index ++ ) {
    distance += weight[index] * (this->data[index] - other[index]) * (this->data[index] - other[index]);
  }
  return distance;
}
// ============================================================================

namespace pyrbirl {
  namespace core {
    // ============================================================================
    std::ostream&
    operator<< ( std::ostream& os, pyrbirl::core::MultiDouble &multi)
    {
      os << multi.repr();
      return os;
    }
  }
}
