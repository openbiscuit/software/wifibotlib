/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PYRBIRL__NUMERIC__REGRESSION_H
#define __PYRBIRL__NUMERIC__REGRESSION_H

#include "multi-double.h"
#include "matrix.h"

#include <vector>

#include "boost/tuple/tuple.hpp"
#include "boost/tuple/tuple_io.hpp"
#include <boost/shared_ptr.hpp>

// dana::core
#include "dana-cpp/atom.h"

namespace core = pyrbirl::core;

namespace pyrbirl {
  namespace numeric {
    
    /** type of data points */
    typedef boost::tuple<core::MultiDouble,double> TData;
    /** pointer to data points */
    typedef boost::shared_ptr<TData> TDataPtr;
    
    /**
     * Forward declaration of shared pointer.
     */
    typedef boost::shared_ptr<class Regression> RegressionPtr;
    
    class Regression : public dana::core::Atom
      {
      public:
	/**
	 * @name Creation/Destruction
	 */
	/**
	 * Constructor.
	 */
	Regression();
	
	/**
	 * Copy constructor
	 */
	Regression( const Regression &other);
	/**
	 * Copy.
	 *
	 * @param other object to be copied
	 */
	virtual Regression &operator= (const Regression &other);
	
	/**
	 * Destructor.
	 */
	~Regression();
	/*
	 * Clone
	 */
	virtual Regression *clone (void) const;
	//@}
	
	/**
	 * Get string representation.
	 *
	 * @return representation as a string
	 */
	virtual std::string repr (void);
	
	/**
	 * Compute a locally weighted least square regression.
	 * Given a list of (key,val) and their weight relative to the 'query',
	 * compute the approximate val(query) using LWR.
	 *
	 * Reference: Schaal, S., & Atkeson, C. G. (1994). Assessing the quality of 
	 * learned local models. In J. Cowan, G. Tesauro, & J. Alspector (Eds.), 
	 * Advances in Neural Information Processing Systems 6 (pp. 160-167). 
	 * San Mateo, CA: Morgan Kaufmann.
	 *
	 * @param query_pt query vector
	 * @param vec_neighbors a vector of (key,val) "neighbors" of query_pt
	 * @param bandwidth along each dimension
	 * @param constant 0 or 1, i.e., add constant column no or yes (default yes)
	 */
	double lwr_regression( core::MultiDouble query_pt, 
			       std::vector<TDataPtr> vec_neighbors,
			       core::MultiDouble bandwidth,
			       double constant = 1 );
      };
  } // numeric
} // pyrbirl
#endif // __PYRBIRL__NUMERIC__REGRESSION_H

