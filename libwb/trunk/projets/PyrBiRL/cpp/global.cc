/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "global.h"

#include "log4cxx/propertyconfigurator.h"
#include "log4cxx/helpers/exception.h"

namespace pyrbirl {
  namespace core {
    // ============================================================================
    int
    init( std::string properties_log_file )
    {
      int result = EXIT_SUCCESS;
      try
	{
	  // PropertyConfigurator for all Loggers
	  log4cxx::PropertyConfigurator::configure( properties_log_file );
	}
      catch(log4cxx::helpers::Exception&)
	{
	  result = EXIT_FAILURE;
	}
      
      return result;
    }
    // ============================================================================
  }
}
