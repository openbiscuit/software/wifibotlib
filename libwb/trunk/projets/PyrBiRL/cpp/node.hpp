/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __NODE_HPP
#define __NODE_HPP

#include <stdlib.h>
#include <iostream>
#include <string>
#include <sstream>

#include <vector>

#include "boost/tuple/tuple.hpp"
#include "boost/tuple/tuple_io.hpp"
#include <boost/shared_ptr.hpp>

#include "multi-double.h"
using namespace pyrbirl::core;

namespace pyrbirl {
  namespace core {
    // forward declaration
    template <typename TVal>
    class Node;
    
    /** type of key */
    typedef MultiDouble TKey;
    /** Ptr to Key */
    typedef boost::shared_ptr<TKey> TKeyPtr;

    /**
     * A Node is either a Leaf containing a vector of shared_pointer to <TKey,TVal> or has 2 children.
     */
    template< typename TVal >
    class Node
    {
      /** Ptr to TVal */
      //typedef boost::shared_ptr<TVal> TValPtr;
    public:
    /** type of stored Element */
    typedef boost::tuple<TKey,TVal> TData;
    /** pointer to stored Element */
    typedef boost::shared_ptr<TData> TDataPtr;
      
      

    public :
      /**
       * @name Creation/Destruction
       */
      /**
       * Constructor.
       *
       * @param nb_max_data_ max number of data in a leaf
       */
      Node(int nb_max_data_)
      {
	this->node_left = NULL;
	this->node_right = NULL;
	this->cut_dimension = -1;
	this->threshold = 0.0;
	this->isLeaf = true;
	this->nb_data = 0;
	this->nb_max_data = nb_max_data_; 
	this->data = new TDataPtr[this->nb_max_data];
	
      }
      /**
       * Copy constructor
       */
      Node( const Node &other)
      {
	this->cut_dimension = other.cut_dimension;
	this->threshold = other.threshold;
	this->isLeaf = other.isLeaf;
	this->nb_data = other.nb_data;
	this->nb_max_data = other.nb_max_data;
	if( other.node_left != NULL ) 
	  this->node_left = new Node(*other.node_left);
	if( other.node_right != NULL ) 
	  this->node_right = new Node(*other.node_right);
      }
      /**
       * Copy.
       *
       * @param other object to be copied
       */
      virtual Node &operator= (const Node &other)
      {
	if (this == &other) return *this;

	this->cut_dimension = other.cut_dimension;
	this->threshold = other.threshold;
	this->isLeaf = other.isLeaf;
	this->nb_data = other.nb_data;
	this->nb_max_data = other.nb_max_data;
	if( other.node_left != NULL ) 
	  this->node_left = new Node(*other.node_left);
	if( other.node_right != NULL ) 
	  this->node_right = new Node(*other.node_right);

	return *this;
      }
      /**
       * Destructor.
       */
      ~Node()
      {
	if( this->node_left != NULL ) delete this->node_left;
	if( this->node_right != NULL ) delete this->node_right;
	if( this->data != NULL ) delete [] this->data ;
      }
      /**
       * Clone
       */
      virtual Node *clone (void) const
      {
	return new Node(*this);
      }
      //@}

      /**
       * Get the i-th TKey of this node.
       *
       * @param int index of the wanted TKey.
       * @return wanted TKey.
       */
      TKey& get_key( int index )
      {
	return boost::tuples::get<0>( (*this->data[index]) );
      }
      /**
       * Get the i-th TVal of this node.
       *
       * @param int index of the wanted TVal.
       * @return wanted TVal.
       */
      TVal& get_val( int index )
      {
	return boost::tuples::get<1>( (*this->data[index]) );
      }

      /**
       * Clear up a node.
       * Delete childs after deleting, then delete all data.
       */
      void clear(void)
      {
	this->cut_dimension = -1;
	this->threshold = 0.0;
	this->isLeaf = true;
	this->nb_data = 0;
	this->data = new TDataPtr[this->nb_max_data];
	
	// clear children
	if( this->node_left != NULL ) {
	  this->node_left->clear();
	  delete this->node_left;
	  this->node_left = NULL;
	}
	if( this->node_right != NULL ) {
	  this->node_right->clear();
	  delete this->node_right;
	  this->node_right = NULL;
	}
      }

      /**
       * Add data to node or to child node.
       * Adds only if some room left for new data.
       *
       * @return NULL if added or the Node that need to be split.
       */
      Node * add(TKey key_, TVal val_)
      {
	if( this->isLeaf ) { // should add to this node
	  if( this->nb_data < this->nb_max_data ) { // still some room
	    // create a pointer to the new TData
	    TDataPtr p(new TData(key_, val_));
	    this->data[this->nb_data] = p;
	    this->nb_data++;
	    return NULL; // nothing else to do
	  }
	  else {
	    return this; // this node is full
	  }
	}
	else { // adds in one child node
	  if( key_[this->cut_dimension] <= this->threshold ) {
	    return this->node_left->add( key_, val_);
	  }
	  else {
	    return this->node_right->add( key_, val_);
	  }
	}
      }
      /**
       * Split a node along a given dimension.
       * Two child nodes are created and populated.
       *
       * @todo ensure that node splitted is empty. 
       */
      void split(int cut_dimension_, double threshold_)
      {
	// create children
	this->node_left = new Node(this->nb_max_data);
	this->node_right = new Node(this->nb_max_data);
	
	// transfert pointers to elements
	for( int i = 0; i < this->nb_data; i++) {
	  if( get_key(i)[cut_dimension_] < threshold_) {
	    this->node_left->add( this->data[i] );
	  }
	  else {
	    this->node_right->add( this->data[i] );
	  }
	}
	
	// not a Leaf but a cut node
	this->isLeaf = false;
	this->cut_dimension = cut_dimension_;
	this->threshold = threshold_;
	delete [] this->data ;
	this->data = NULL;
	this->nb_data = 0;
      }
      

      /**
       * Fill a vector with approximatively the n "nearest" neigbors of query_key_.
       * If enough points in the leaf of query_key, it returns the n first points
       * of the leaf. If there is not enough point in the leaf, it will look in the
       * sibling leaf/tree, in a recursive way.
       * 
       * @param query_point_
       * @param nb_points_ nb of neighbors wanted
       * @param vec_ vector where to add points that are "not too far away"
       */
      void fill_vector_with_approximate_neighbors( TKey query_point_, int nb_points_,
						 std::vector<TDataPtr>& vec_)
      {
	// leaf -> fill up vector
	if( this->isLeaf ) {
	  for( int i=0; i<this->nb_data; i++) {
	    vec_.push_back(data[i]);
	  }
	}
	else {
	  // look in the right child node
	  if( query_point_[this->cut_dimension] <= this->threshold ) {
	    this->node_left->fill_vector_with_approximate_neighbors( query_point_, nb_points_, vec_);
	    // if does not fill with enough data, look in sinling node
	    if( (int) vec_.size() < nb_points_) {
	      this->node_right->fill_vector_with_approximate_neighbors( query_point_, nb_points_, vec_);
	    }
	  }
	  else {
	    this->node_right->fill_vector_with_approximate_neighbors( query_point_, nb_points_, vec_);
	    // if does not fill with enough data, look in sibling node
	    if( (int) vec_.size() < nb_points_) {
	      this->node_left->fill_vector_with_approximate_neighbors( query_point_, nb_points_, vec_);
	    }
	  }
	} 
      }

      /**
       * String representation.
       */
      std::string repr()
      {
	std::stringstream strbuf;
	if( this->isLeaf ) {
	  strbuf << "### LEAF (" << this->nb_data << "/" << this->nb_max_data << " data)\n";
	  for( int i=0; i < this->nb_data; i++ ) {
	    strbuf << get_key(i).repr() << "==>" << get_val(i) << "\n";
	    // strbuf << (*this->data[i]));
	  }
	}
	else {
	  strbuf << "#### NODE (cut for " << this->cut_dimension << " at " << this->threshold << ")\n";
	  strbuf << "# Node Left\n" << this->node_left->repr() << "\n";
	  strbuf << "# Node Right\n" << this->node_right->repr() << "\n";
	}
	
	return strbuf.str();
      }
      /** 
       * Standard Output.
       */
      friend std::ostream& operator<< (std::ostream& os, Node& node)
      {
	os << node.repr();
	return os;
      }

      /**
       * Write Node.
       */
      void write(std::ostream& os)
      {
	os << "# NODE\n";
	os << "# cut_dimension   threshold\n" ;
	os<< cut_dimension << " " << threshold << "\n";
	os << "# isLeaf nb_data nb_max_data\n";
	os << isLeaf << " " << nb_data << " " << nb_max_data << "\n";
	for( int i=0; i < this->nb_data; ++i ) {
	  os << get_key(i) << "\n"; 
	  //get_key(i).write( os );
	  os << get_val(i) << "\n";
	}
	if( node_left == NULL ) {
	  os << "# NULL_LEFT" << "\n";
	}
	else {
	  os << "# Child Left\n";
	  node_left->write(os);
	}
	if( node_right == NULL ) {
	  os << "# NULL_RIGHT" << "\n";
	}
	else {
	  os << "# Child Right\n";
	  node_right->write(os);
	}	
      }
      /**
       * Read node.
       */
      void read(std::istream& is)
      {
	std::string line;
	std::getline( is, line); //"# NODE\n"
	//std::cout << "line=" << line << "-\n";
	std::getline( is, line); //"# cut_dimension   threshold\n"
	is >> this->cut_dimension;
	is >> this->threshold;
	std::getline( is, line);
	//std::cout << "Read " << this->cut_dimension << " " << this->threshold << "\n";
	std::getline( is, line); // "# isLeaf nb_data nb_max_data\n"
	int tmp_nb_data;
	is >> this->isLeaf >> tmp_nb_data >> this->nb_max_data;
	std::getline( is, line);
	//std::cout << "Read " << this->isLeaf << " " << tmp_nb_data << " " << this->nb_max_data << "\n";
	if( this->data != NULL ) delete [] this->data;
	this->data = new TDataPtr[this->nb_max_data];
	// read data points if any
	for( int i=0; i < tmp_nb_data; ++i ) {
	  std::getline( is, line);
	  //std::cout << "TKEY["<<i<<"/"<<tmp_nb_data<<"] =-" << line << "-\n";
	  TKey _key(line);
	  TVal _val;
	  is >> _val;
	  std::getline( is, line);
	  //std::cout << "Read " << _key << " " << _val << "\n"; 
	  add( _key, _val);
	}
	
	// Node Left
	std::getline( is, line);
	//std::cout << "line=" << line << "-\n";
	if ( line.find("# NULL") == std::string::npos) {
	  //std::cout << "Read " << " new node \n";
	  this->node_left = new Node(this->nb_max_data);
	  this->node_left->read( is );
	}
	else {
	  //std::cout << "Read " << " NULL node \n"; 
	  this->node_left = NULL;
	}

	// Node Right
	std::getline( is, line);
	//std::cout << "line=" << line << "-\n";
	if ( line.find("# NULL") == std::string::npos) {
	  //std::cout << "Read " << " new node \n";
	  this->node_right = new Node(this->nb_max_data);
	  this->node_right->read( is );
	}
	else {
	  //std::cout << "Read " << " NULL node \n"; 
	  this->node_right = NULL;
	}
      }

    public:
      /** Child node */
      Node *node_left;
      /** Child node */
      Node *node_right;
      /** Dimension along which this node was cut */
      int cut_dimension;
      /** Thresold for the cut dimension */
      double threshold;
      /** Is this node a Leaf Node */
      bool isLeaf;
      /** Array of Ptr to TData */
      TDataPtr *data;
      /** Nb of Data */
      int nb_data;
      /** Max nb of Data */
      int nb_max_data;
      
    protected:
      /**
       * Add TDataPtr to node if room left.
       * Called internally when node is split.
       *
       * @return true if okay
       */  
      bool add(TDataPtr p_)
      {
	if( this->isLeaf ) { // should add to this node
	  if( this->nb_data < this->nb_max_data ) { // still some room
	    this->data[this->nb_data] = p_;
	    this->nb_data++;
	    return true; // nothing else to do
	  }
	}
	return false;
      }
      
      
    };
  } // core
} // pyrbirl

#endif //__NODE_HPP
