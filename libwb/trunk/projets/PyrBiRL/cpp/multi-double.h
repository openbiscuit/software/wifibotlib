/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PYRBIRL__CORE__MULTI_DOUBLE_H
#define __PYRBIRL__CORE__MULTI_DOUBLE_H

#include <iostream>
#include <string>
#include <sstream>

// dana::core
#include "dana-cpp/atom.h"

namespace pyrbirl {
  namespace core {

    /**
     * Forward declaration of shared pointer.
     */
    typedef boost::shared_ptr <class MultiDouble> MultiDoublePtr;

    class MultiDouble : public dana::core::Atom
    {
    public:
      /**
       * @name Creation/Destruction
       */
      /**
       * Constructor with size, init to 0.
       *
       * @param size_ size of the array of double
       */
      MultiDouble( int size_ = 1 );
      /**
       * Constructor from string.
       *
       * @param str_ string representing a MultiDouble.
       */
      MultiDouble( const std::string str_);

      /**
       * Copy constructor
       */
      MultiDouble( const MultiDouble &other);
      /**
       * Copy.
       *
       * @param other object to be copied
       */
      virtual MultiDouble &operator= (const MultiDouble &other);

      /**
       * Destructor.
       */
      ~MultiDouble();
      /*
       * Clone
       */
      virtual MultiDouble *clone (void) const;
      /**
       * Template make function
       *
       * @return A shared pointer on a new X
       */
      template <class X>
	static boost::shared_ptr<X> make(int size_ = 1) {
	boost::shared_ptr<X> x =
	  boost::dynamic_pointer_cast<X>
	  (boost::shared_ptr<X> (new X(size_)));
	assert(x);
	return x; 
      }
      //@}

      /**
       * Get C++ type.
       *
       * @return C++ type string
       */
      virtual std::string get_type (void);            
      /**
       * Get string representation.
       *
       * @return representation as a string
       */
      virtual std::string repr (void);
      /**
       * Read from a string representation.
       *
       * @param string representation
       * @return EXIT_SUCCESS
       */
      virtual int from_repr (std::string str_);

      /**
       * @name IO
       *
       * Read/Write from/to file.
       */
      void write(std::ostream& os);
      void read(std::istream& is);
      //@}

      /**
       * @name Access
       */
      /**
       * Access as regular array.
       */
      double & operator[] (int index) const;
      /**
       * Access to base pointer.
       */
      double * operator* (void);
      //@}

      /**
       * Create a concatenation of *this with another.
       *
       * @param other another MultiDouble to concatenate
       * 
       * @return a new MultiDouble (*this,other)
       */
      MultiDouble concat(MultiDouble &other);

      /**
       * get the nb of double.
       */
      int size (void) const;
      

      /** 
       * square distance to another MultiDouble of same dimension.
       *
       * @return dist or -1.0
       */
      double square_dist ( const MultiDouble &other ) const;

      /**
       * weighted square distance tn another MultiDouble of same
       * dimension.
       *
       * @return dist of -1.0
       */
      double weighted_square_dist (const MultiDouble &other,
				   const MultiDouble &weight);

      /** 
       * Standard Output.
       */
      friend std::ostream& operator<< (std::ostream& os, MultiDouble& multi);

    protected:
      /** the actual ptr to data */
      double *data;
      /** size of the data */
      int size_;
    };
  } // core
} // pyrbirl
#endif // __PYRBIRL__CORE__MULTI_DOUBLE_H
