/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "regression.h"
#include <math.h>

using namespace pyrbirl::numeric;
// ============================================================================
Regression::Regression() : Atom()
{
}
Regression::Regression( const Regression &other) : Atom() // -----------------
{
}
Regression &
Regression::operator=( const Regression &other) // ----------------------------
{
  if (this == &other) return *this;
  return *this;
}
Regression *
Regression::clone (void) const // ---------------------------------------------
{
  return new Regression(*this);
}
// ============================================================================
Regression::~Regression()
{
}
// ============================================================================
std::string
Regression::repr()
{
  return std::string("Regression");
}
// ============================================================================
double 
Regression::lwr_regression( core::MultiDouble query_pt,
			    std::vector<TDataPtr> vec_neighbors,
			    core::MultiDouble bandwidth,
			    double constant )
{ 
  // build up query matrix X and result matrix Y
  // Rows of X are made of neighbors of query_pt 
  // A last column made of '1' is added if 'constant == 1'
  //
  // For the weight matrix, use the Mahalanobis distance
  // w = exp( -(X-xq)^2/bandwidth^2)

  int X_row = vec_neighbors.size();
  int X_col = query_pt.size();

  if( constant == 1 ) {
      ++X_col;
  }

  // xq = query_pt
  core::Matrix xq( 1, X_col );
  xq.set(0, X_col-1,1.0);
  for( int col = 0; col < query_pt.size(); ++col ) {
    xq.set( 0, col, query_pt[col]);
  }

  core::Matrix X(X_row, X_col);
  core::Matrix Y(X_row, 1);
  core::Matrix w(X_row, 1);

  std::vector<TDataPtr>::iterator it_vec_neighbors = vec_neighbors.begin();
  for (int row = 0; row < X.nb_row(); ++row ) {
    TDataPtr data = (*it_vec_neighbors);
    core::MultiDouble neighbor = boost::tuples::get<0>( *data );
    double val = boost::tuples::get<1>( *data );
    
    Y.set( row, 0, val );
    
    double weighted_distance = 0.0;
    for (int col = 0; col < X.nb_col(); ++col ) {
      if( col >= query_pt.size() ) { // possible only if 'constant == 1'
	// the last columnis a '1'
	X.set( row, col, 1.0 );
      }
      else {
	X.set( row, col, neighbor[col] );
	// weight
	weighted_distance += (neighbor[col] - query_pt[col])*(neighbor[col] - query_pt[col]) / (bandwidth[col]*bandwidth[col]);
      }
    }
    
    // weight
    weighted_distance = exp( -weighted_distance);
    w.set( row, 0, weighted_distance );
    
    // next query point
    it_vec_neighbors++;
  }
  
  // DEBUG
  //std::cout << "Matrix xq\n" << xq;
  //std::cout << "Matrix X\n" << X;
  //std::cout << "Matrix w\n" << w;
  
  // compute (wX)'
  core::Matrix wX(X_row, X_row);
  
  for( int diag = 0; diag < X_row; ++diag) {
    wX.set(diag, diag, w.get(diag,0));
  }
  // DEBUG
  //std::cout << "Matrix wX\n" << wX;
  wX = wX.dot(X);
  if( constant == 1) {
    for( int diag = 0; diag < X_row; ++diag) {
      wX.set(diag, X_col-1, 1.0 );
    }
  }
  wX = wX.transpose();
  // DEBUG
  //std::cout << "Matrix wX\n" << wX;
  
  // P = pinv( wX'*X )
  core::Matrix P = wX.dot(X);
  P = P.pinv();
  // DEBUG
  //std::cout << "Matrix P\n" << P;
  // theta = P*wX'*Y;
  core::Matrix theta = P.dot(wX);
  theta = theta.dot(Y);
  // DEBUG
  //std::cout << "Matrix theta\n" << theta;
  // yq = xq'*theta;
  core::Matrix yq = xq.dot(theta);
  // DEBUG
  //std::cout << "Matrix yq\n" << yq;
  
  return yq.get(0,0);
}
// ============================================================================
