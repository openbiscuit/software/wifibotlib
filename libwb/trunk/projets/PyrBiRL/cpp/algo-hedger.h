/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PYRBIRL__ALGORITHM__ALGOHEDGER_H
#define __PYRBIRL__ALGORITHM__ALGOHEDGER_H

#include "global.h"
#include "multi-double.h"
#include "matrix.h"
#include "regression.h"
#include "kdtree.hpp"

#include <vector>

#include "boost/tuple/tuple.hpp"
#include "boost/tuple/tuple_io.hpp"
#include <boost/shared_ptr.hpp>

// Logger
#include "log4cxx/logger.h"

// dana::core
#include "dana-cpp/atom.h"

namespace core = pyrbirl::core;
namespace numeric = pyrbirl::numeric;

namespace pyrbirl {
  namespace algorithm {

    /** type of tree */
    typedef core::KdTree<double> TKdTreeDouble;
    /** pointer to tree */
    typedef boost::shared_ptr<TKdTreeDouble> TKdTreeDoublePtr;
    
    /** type of data points */
    typedef boost::tuple<core::MultiDouble,double> TData;
    /** pointer to data points */
    typedef boost::shared_ptr<TData> TDataPtr;

    /**
     * type de prédiction
     */
    enum TPrediction
      {
	PRED_NORMAL = 0,
	PRED_EXACT, ///< query point is explicitely in KdTree
	PRED_NOT_ENOUGH, ///< not enough point for the computation
	PRED_IVH, ///< not in the IVH
	PRED_BOUNDS ///< outside QVal bounds
      };
    
    /**
     * Forward declaration of shared pointer.
     */
    typedef boost::shared_ptr<class AlgoHedger> AlgoHedgerPtr;

    class AlgoHedger : public dana::core::Atom
      {
      public:
	/**
	 * @name Creation/Destruction
	 */
	/**
	 * Constructor.
	 */
	AlgoHedger( TKdTreeDoublePtr qval_tree_ = TKdTreeDoublePtr::shared_ptr() );
	/**
	 * Copy constructor
	 */
	AlgoHedger( const AlgoHedger &other);
	/**
	 * Copy.
	 *
	 * @param other object to be copied
	 */
	virtual AlgoHedger &operator= (const AlgoHedger &other);
	
	/**
	 * Destructor.
	 */
	~AlgoHedger();
	/*
	 * Clone
	 */
	virtual AlgoHedger *clone (void) const;
	/**
	 * Template make function
	 *
	 * @return A shared pointer on a new X
	 */
	template <class X>
	  static boost::shared_ptr<X> make(TKdTreeDoublePtr tree_= TKdTreeDoublePtr::shared_ptr()) {
	  boost::shared_ptr<X> x =
	    boost::dynamic_pointer_cast<X>
	    (boost::shared_ptr<X> (new X(tree_)));
	  assert(x);
	  return x; 
	}
	//@}
	/**
	 * Get string representation.
	 *
	 * @return representation as a string
	 */
	virtual std::string repr (void);
	/** 
	 * Standard Output.
	 */
	friend std::ostream& operator<< (std::ostream& os, AlgoHedger& algo);	

	/**
	 * Kind of Q-learning using KdTree to store Q(states+action).
	 *
	 * @param old_state state wher q-value will change
	 * @param action chosen action for old_state
	 * @param new_state after transition, ended in that state
	 * @param reward reward received after transition
	 */
	void learn( core::MultiDouble old_state,
		    core::MultiDouble action,
		    core::MultiDouble new_state,
		    double reward );

	/**
	 * From the points stored by the data structure (KdTree), try to regress
	 * the Qvalue of the query point.
	 *
	 * @param query_pt query vector
	 * @param status status of the prediction (see enum TPrediction)
	 */
	double predict( core::MultiDouble query_pt,
			TPrediction& status );


	/**
	 * Check if the query_pt is inside the IVH defined by its neighbors.
	 * IVH = .
	 *
	 * @param query_pt query vector
	 * @param vec_neighbors a vector of (key,val) "neighbors" of query_pt
	 */
	bool in_ivh( core::MultiDouble query_pt, std::vector<TDataPtr> vec_neighbors );

	
        
      public:
	/**
	 * @name parameters
	 *
	 * Parameters of the algorithms
	 */
	void set_ivh_tolerance(double val) {ivh_tolerance = val;}
	double get_ivh_tolerance(void) {return ivh_tolerance;}

	void set_nb_pts_for_regression(int val) {nb_pts_for_regression = val;}
	unsigned int get_nb_pts_for_regression(void) {return nb_pts_for_regression;}

	void set_bandwidth(core::MultiDouble val) {bandwidth = val;}
	core::MultiDouble get_bandwidth(void) {return bandwidth;}

	void set_alpha(double val) {alpha=val;}
	double get_alpha(void) {return alpha;}

	void set_gamma(double val);
	double get_gamma(void) {return gamma;}

	void set_list_actions(std::vector<core::MultiDouble> vec) {list_actions = vec;}
	std::vector<core::MultiDouble> get_list_actions(void) {return list_actions;}
	//@}

	
	/** KdTree for values */
	TKdTreeDoublePtr qval_tree;
	/**
	 * @name qval_tree
	 *
	 * Getter and Setter for qval_tree
	 */
	void set_qval_tree(TKdTreeDoublePtr tree_) {qval_tree = tree_;}
	TKdTreeDoublePtr get_qval_tree(void) {return qval_tree;}
	//@}
	
      private:
	/** Regression tools */
	static numeric::Regression regressor;
	
	/** Tolerance factor for IVH */
	double ivh_tolerance;
	/** Nb of points needed for regression */
	unsigned int nb_pts_for_regression;
	/** Bandwidth along each dimension */
	core::MultiDouble bandwidth;

	/** Learning coef. */
	double alpha;
	/** Discount coef. */
	double gamma;

	/** list of possible actions */
	std::vector<core::MultiDouble> list_actions;

	/**
	 * @name bounds
	 *
	 * Bounds for Q-Value computation.
	 */
	double r_min_;
	double r_max_;
	double q_min_;
	double q_max_;
	//@}

	/** init parameters */
	void ini_param(void){
	  set_ivh_tolerance(1.0);
	  set_nb_pts_for_regression(3);
	  // bandwidth is set to MultiDouble's default.

	  set_alpha(0.5);
	  set_gamma(0.9);
	}
      private:
	/** Logger */
	static log4cxx::LoggerPtr logger;
      };

  } // algorithm
} // pyrbirl
#endif // __PYRBIRL__ALGORITHM__ALGOHEDGER_H
