
#ifndef __PYRBIRL_CORE_KDTREE_HPP
#define __PYRBIRL_CORE_KDTREE_HPP

#include <stdlib.h>
#include <iostream>
#include "node.hpp"

#include <vector>

// dana::core
#include <dana-cpp/atom.h>

// template <typename TData> std::ostream& operator<<(std::ostream&, const KdTree<TData>&);

using namespace pyrbirl::core;

namespace pyrbirl {
  namespace core {

    // forward declaration
    template <typename TData>
    class KdTree;

    /** type of weight */
    typedef TKey TWeight;
    typedef TKeyPtr TWeightPtr;

    template< typename TVal >
    class KdTree : public dana::core::Atom
    {
    public:
      /** type of Node */
      typedef Node<TVal> TNode;
      /** type of stored Element */
      typedef boost::tuple<TKey,TVal> TData;
      /** pointer to stored Element */
      typedef boost::shared_ptr<TData> TDataPtr;
      /**
       * @name Creation/Destruction
       */
      /**
       * Constructor.
       *
       * @param nb_dimension nb of dimension in TKey
       * @param nb_data_in_leaf max number of data in each leaf
       * @param weight a vector of weight along dimension, by default it will be
       * initialized to [1; 1; 1; ...].
       */
      KdTree( int nb_dimension = 1,
	      int nb_data_in_leaf = 10,
	      TWeightPtr weight = TWeightPtr::shared_ptr()); // empty
							     // share_ptr
      /**
       * Copy constructor
       */
      KdTree( const KdTree &other);
      /**
       * Copy.
       *
       * @param other object to be copied
       */
      virtual KdTree &operator= (const KdTree &other);
      /** 
       * Destructor.
       */
      ~KdTree();
      /**
       * Clone
       */
      virtual KdTree *clone (void) const;
      /**
       * Template make function
       *
       * @return A shared pointer on a new X
       */
      template <class X>
      static boost::shared_ptr<X> make(int size_ = 1) {
	boost::shared_ptr<X> x =
	  boost::dynamic_pointer_cast<X>
	  (boost::shared_ptr<X> (new X(size_)));
	assert(x);
	return x; 
      }
      //@}


      /**
       * Get the TKey of this TDataPtr.
       *
       * @param data is a TDataPtr
       * @return wanted TKey.
       */
      static TKey& get_key(TDataPtr data_ptr)
      {
	return boost::tuples::get<0>( *data_ptr );
      }
      /**
       * Get the TVal of this TDataPtr.
       *
       * @param data is a TDataPtr
       * @return wanted TVal.
       */
      static TVal& get_val(TDataPtr data_ptr)
      {
	return boost::tuples::get<1>( *data_ptr );
      }

      /**
       * Get Data.
       */
      //TData operator[] (const TKey key) const;
      //TData get (const TKey key) const;

      /**
       * Return a vector made of approximatively the n "nearest" neigbors of query_key.
       * If enough points in the leaf of query_key, it returns the n first points
       * of the leaf. If there is not enough point in the leaf, it will look in the
       * sibling leaf/tree, in a recursive way.
       * 
       * @param query_point_
       * @param nb_points_ nb of neighbors wanted
       * @return a vector of at least n points if possible.
       */
      std::vector<TDataPtr> get_nearest_approximate( TKey query_point_, int nb_point_ )
      {
	std::vector<TDataPtr> result_vec;
	head.fill_vector_with_approximate_neighbors( query_point_, nb_point_, result_vec);
	return result_vec;
      }
      

      /**
       * Add Data.
       */
      int add(TKey key_, TVal val_);
      
      /**
       * Clear up tree (but not weight).
       */
      int clear();
      
      /**
       * String representation.
       */
      std::string repr();
      /** 
       * Standard Output.
       */
      friend std::ostream& operator<< ( std::ostream& os, KdTree& tree)
      {
	os << tree.repr();
	return os;
      }      
      
      /**
       * @name IO
       *
       * Read/Write from/to file.
       */
      void write(std::ostream& os);
      void read(std::istream& is);
      //@}

      /**
       * Nb of dimensions for the key.
       */
      int get_nb_dim() { return nb_dimension_; }
      
    private:
      /** root of the node */
      TNode head;
      /** nb of dimension */
      int nb_dimension_;
      /** nb max of data in leaves of the KdTree */
      //int nb_max_data_in_leave_;
      /** weight for the Keys */
      TKeyPtr weight_;

    };
    // ============================================================================
    template< typename TVal >
    KdTree<TVal>::KdTree( int nb_dimension,
			   int nb_max_data_in_leaf,
			   TWeightPtr weight)
      : Atom(),
	head( nb_max_data_in_leaf ) // create tree root
    {
      nb_dimension_ = nb_dimension;
      
      weight_ = weight; // weighting 
      
      if( weight_.use_count() == 0 ) { // empty weight
	TWeightPtr p(new TWeight(nb_dimension_));
	weight_ = p;
	for( int i=0; i<weight_->size(); i++) {
	  (*weight_)[i] = 1.0;
	}
      }
      else { // check if right dimension
	if( weight_->size() != nb_dimension_) {
	  throw std::string( "KdTree : weight has wrong size" );
	}
      }
      
    }   
    template< typename TVal >
    KdTree<TVal>::~KdTree()
    {
    }
    template< typename TVal >
    KdTree<TVal>::KdTree( const KdTree &other)
      : Atom(),
	head( other.head ) // create tree root
    {
      nb_dimension_ = other.nb_dimension_;
      weight_ = other.weight_;
      
    }
    template< typename TVal >
    KdTree<TVal>&
    KdTree<TVal>::operator= (const KdTree &other)
    {
      if (this == &other) return *this;

      nb_dimension_ = other.nb_dimension_;
      weight_ = other.weight_;

      head = other.head;

      return *this;
    }
    template< typename TVal >
    KdTree<TVal>*
    KdTree<TVal>::clone (void) const
    {
      return new KdTree<TVal>(*this);
    }
    // ============================================================================
    template< typename TVal >
    int
    KdTree<TVal>::add( TKey key_, TVal val_)
    {
      // add starting from the root node
      TNode *split_node = head.add( key_, val_ );
      
      if( split_node != NULL ) {
	// need to split node returned by Node::add()
	
	// look for maximally spread dimension in Node
	int cut_dim = 0;
	double deviation_max = 0.0;
	
	MultiDouble deviation(nb_dimension_);
	MultiDouble mean(nb_dimension_);
	for( int index = 0; index < split_node->nb_data; index++) {
	  TKey tmp_key = split_node->get_key( index );
	  for( int dim=0; dim < nb_dimension_; dim++ ) {
	    // update mean
	    mean[dim] += tmp_key[dim];
	    // update deviation
	    deviation[dim] += tmp_key[dim] * tmp_key[dim];
	  }
	}
	// wrap it up
	for( int dim=0; dim < nb_dimension_; dim++ ) {
	  mean[dim] /= (double) split_node->nb_data;
	  double tmp_deviation = (*weight_)[dim] * (*weight_)[dim] 
	    * ((deviation[dim])/( (double) split_node->nb_data ) - mean[dim] * mean[dim]);
	  std::cout << dim << " dev=" << tmp_deviation << " mean=" << mean[dim] << "\n"; 

	  if( tmp_deviation >= deviation_max ) {
	    cut_dim = dim;
	    deviation_max = tmp_deviation;
	  }
	}

	// effectively split node
	split_node->split(cut_dim, mean[cut_dim]);
	// and add our new data
	split_node->add( key_, val_);
      }
      return 0;
    }
    template< typename TVal > // -------------------------------------------------
    int
    KdTree<TVal>::clear(void)
    {
      // start from the head node
      head.clear();
      return 0;
    }
    // ============================================================================
    template< typename TVal >
    std::string
    KdTree<TVal>::repr()
    {
      std::stringstream strbuf;
      strbuf << "KdTree\n";
      strbuf << "weight = " << (*this->weight_) << "\n";
      strbuf << this->head;
      
      return strbuf.str();
    }
    // ============================================================================
    template< typename TVal >
    void
    KdTree<TVal>::write(std::ostream& os)
    {
      os << "# KDTREE\n";
      os << "# nb_dimension\n" << nb_dimension_ << "\n";
      os << "# weight\n";
      weight_->write( os );
      head.write( os );
    }
    template< typename TVal >
    void
    KdTree<TVal>::read(std::istream& is)
    {
      std::string line;
      std::getline( is, line); //"# KDTREE\n";
      std::getline( is, line); //"# nb_dimension\n";
      is >> nb_dimension_;
      std::getline( is, line);
      //std::cout << "Read " << nb_dimension_ << "\n";

      std::getline( is, line); //"weight\n";
      //std::cout << "line=" << line << "-\n";
      std::getline( is, line);
      //std::cout << "line=" << line << "-\n";
      TWeightPtr p(new TWeight(line));
      weight_ = p;
      //std::cout << "Read " << *weight_ << "\n"; 

      head.read(is);
    }
    // ============================================================================
  } //core
} // pyrbirl
#endif // __PYRBIRL_CORE_KDTREE_HPP
