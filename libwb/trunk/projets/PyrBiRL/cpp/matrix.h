/*
 * PyrBiRL - Reinforcement Learning for URBI and Python.
 * Copyright (C) 2008 Alain Dutech
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PYRBIRL_CORE__MATRIX__H
#define __PYRBIRL_CORE__MATRIX__H

#include <iostream>
#include <string>
#include <sstream>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

// dana::core
#include "dana-cpp/atom.h"

namespace pyrbirl {
  namespace core {

    class Matrix
      {
      public:
	/**
	 * @name Creation/Destruction
	 */
	/**
	 * Constructor with size (row, col). 
	 * All elements are set to 0.
	 *
	 * @param row number of rows (default=1)
	 * @param col number of columns (default=1)
	 */
	Matrix(int row = 1, int col = 1);
	/**
	 * Copy constructor
	 */
	Matrix( const Matrix& A);
	/**
	 * Copy operator
	 */
	virtual Matrix &operator= (const Matrix &A);
	/*
	 * Clone
	 */
	virtual Matrix *clone (void) const;
	/**
	 * Destructor.
	 */
	~Matrix(void);
	//@}

	/**
	 * @name Operators
	 */
	/**
	 * Product of 'this dot other'.
	 *
	 * @return Matrix product
	 */
	const Matrix dot(const Matrix &other) const;
	/**
	 * Transpose of the Matrix.
	 *
	 * @return Matrix transposed.
	 */
	const Matrix transpose(void) const;
	/**
	 * Compute the Pseudo-Inverse.
	 *
	 * @return Matrix pseudo-inverse
	 */
	const Matrix pinv(void) const;
	//@}

	//void Normalize(int dimension);

	/**
	 * Get max element of the diagonal.
	 */
	double max_diag(void);

	/**
	 * Are every elements equal to 0 ?
	 */
	bool is_null(void);

	/**
	 * @name getter/setter
	 */
	double get( int row_index, int col_index);
	void set( int row_index, int col_index, double value);
	int nb_row() { return _row; }
	int nb_col() { return _col; }
	//@}
	
	/**
	 * Get string representation.
	 *
	 * @return representation as a string
	 */
	virtual std::string repr (void);
	/** 
	 * Standard Output.
	 */
	friend std::ostream& operator<< (std::ostream& os, Matrix& multi);

      protected:
	/** the actual matrix data */
	gsl_matrix *_M;
	/** number of rows */
	int _row;
	/** number of columns */
	int _col;
	
      private:
	gsl_matrix *Dot(gsl_matrix *A, gsl_matrix *B);
      };
  }// core
} // pyrbirl

#endif // __PYRBIRL_CORE__MATRIX__H
