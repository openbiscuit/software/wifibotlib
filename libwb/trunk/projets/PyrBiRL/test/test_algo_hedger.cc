#include <stdio.h>
#include "algo-hedger.h"
#include <boost/foreach.hpp>

namespace rlco = pyrbirl::core;
//namespace rlnu = pyrbirl::numeric;
namespace rlal = pyrbirl::algorithm;


// include log4cxx header files.
#include "log4cxx/logger.h"
#include "log4cxx/helpers/exception.h"

using namespace log4cxx;
//using namespace log4cxx::helpers;

LoggerPtr logger(Logger::getLogger("test_algo_hedger"));

int
main (int argc, char *argv[] )
{
  if (argc > 1)
    {
      // configuration of module
      rlco::init(argv[1]);
    }
  else
    {
      rlco::init();
    }
      
  LOG4CXX_INFO(logger, "Entering application.");
  
  std::vector<rlal::TDataPtr> vec_voisins;
  
  rlco::MultiDouble query(2);
  
  rlco::MultiDouble key1(2);
  key1[0] = 1.0; key1[1] = 1.0;
  rlal::TDataPtr p1( new rlal::TData( key1, 1 ));
  vec_voisins.push_back( p1 );
  rlco::MultiDouble key2(2);
  key2[0] = 2.0; key2[1] = 1.0;
  rlal::TDataPtr p2( new rlal::TData( key2, 2 ));
  vec_voisins.push_back( p2 );
  rlco::MultiDouble key3(27);
  key3[0] = 1.0; key3[1] = 2.0;
  rlal::TDataPtr p3( new rlal::TData( key3, 2 ));
  vec_voisins.push_back( p3 );
  
  
  // Create algo
  rlal::AlgoHedger algo;
  algo.set_ivh_tolerance( 1.5 );
  std::cout << algo;
  bool res;
  
  // along Ox axis
  query[1] = 1.0;
  for( double x = 0.0; x < 3.0; x += 0.2) {
    query[0] = x;
    res = algo.in_ivh( query, vec_voisins ); 
    std::cout << query << " : " << res << "\n";
  } 
  
  // adding a KdTree
  // Create KdTree
  rlal::TKdTreeDoublePtr tree(new rlal::TKdTreeDouble(2, 5) );
  
  rlco::MultiDouble key(2);
  key[0] = 1; key[1] = 1;
  tree->add( key, 1.0 );
  key[0] = 2; key[1] = 1;
  tree->add( key, 1.5 );
  std::cout << "**Tree**\n" << (*tree);
  
  algo.set_qval_tree( tree );
  LOG4CXX_INFO(logger, "Adding a tree to algo");
  
  // bandwidth
  rlco::MultiDouble band(2);
  band[0] = 1.0; band[1] = 1.0;
  algo.set_bandwidth( band );
  LOG4CXX_INFO(logger, "Adding a bandwidth");
  std::cout << algo << "\n";
  
  
  query[0] = 1.5; query[1] = 1.25;
  rlal::TPrediction status;
  LOG4CXX_INFO(logger, "Prediction for (1.5, 1.25)");
  double pred = algo.predict( query, status );
  std::cout << "algo.predict( query, status ) == > " << pred << " status=" << status << "\n";
  
  
  // add new points
  key[0] = 1.5; key[1] = 1.5;
  tree->add( key, 2.0 );
  LOG4CXX_INFO(logger, "add a point to the tree");
  query[0] = 1.5; query[1] = 1.25;
  LOG4CXX_INFO(logger, "Prediction for (1.5, 1.25)");
  pred = algo.predict( query, status );
  std::cout << "algo.predict( query, status ) == > " << pred << " status=" << status << "\n";
  
  query[0] = 1.001; query[1] = 0.99;
  LOG4CXX_INFO(logger, "Prediction for (1.001, 0.99)");
  pred = algo.predict( query, status );
  std::cout << "algo.predict( query, status ) == > " << pred << " status=" << status << "\n";
  
  query[0] = 0.0; query[1] = 3.0;
  LOG4CXX_INFO(logger, "Prediction for (0.0, 3.0)");
  pred = algo.predict( query, status );
  std::cout << "algo.predict( query, status ) == > " << pred << " status=" << status << "\n";
  
  
  // ***** LEARNING *********
  LOG4CXX_INFO(logger, "Clear up tree");
  tree->clear();
  std::cout << "**Tree**\n" << (*tree);
  std::cout << "**Algo**\n" << algo;
  
  // list of action
  std::vector<core::MultiDouble> list_of_actions;
  rlco::MultiDouble act(1);
  act[0] = 0.0;
  list_of_actions.push_back(act);
  act[0] = 1.0;
  list_of_actions.push_back(act);
  BOOST_FOREACH( core::MultiDouble action, list_of_actions ) {
    std::cout << "action = " << action << "\n";
  }
  LOG4CXX_INFO(logger, "Add list of actions");
  algo.set_list_actions( list_of_actions );
  
  // learning
  rlco::MultiDouble state(2);
  rlco::MultiDouble new_state(2);
  double reward;
  
  state[0] = 1.0; state[1] = 1.0;
  act[0] = 0;
  new_state[0] = 2.0; new_state[1] = 1.0;
  reward = 1.0;
  LOG4CXX_INFO(logger, "Learning for (1,1)");
  algo.learn( state, act, new_state, reward);
  std::cout << "**Tree**\n" << (*tree);
  
  
  state = new_state;
  act[0] = 0.0;
  new_state[0] = 1.5; new_state[1] = 1.5;
  reward = 1.5;
  LOG4CXX_INFO(logger, "Learning for (2,0)");
  algo.learn( state, act, new_state, reward);
  std::cout << "**Tree**\n" << (*tree);
  
  state = new_state;
  act[0] = 0.0;
  new_state[0] = 1.0; new_state[1] = 1.0;
  reward = 2.0;
  LOG4CXX_INFO(logger, "Learning for (1.5,1.5)");
  algo.learn( state, act, new_state, reward);
  std::cout << "**Tree**\n" << (*tree);
  
  LOG4CXX_INFO(logger, "Exiting application.");

  return 0;
}


/* Desired verbose results...
*****************************
*/
