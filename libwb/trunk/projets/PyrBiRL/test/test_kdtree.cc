
#include <stdio.h>
#include "kdtree.hpp"

#include <iostream>
#include <fstream>

#include <boost/foreach.hpp>

namespace rl = pyrbirl::core;

typedef rl::KdTree<double> KdTreeDouble;
typedef KdTreeDouble::TDataPtr KdTreeDataPtr;

int
main (int argc, char *argv[] )
{

  rl::TWeightPtr w(new TWeight(2));
  (*w)[0] = 1.0;
  (*w)[1] = 40.0;
  
  try {
    KdTreeDouble tree( 2 /*dim*/, 5 /*nb_data*/, w);
    std::cout << tree;
    std::cout << tree.repr();

    for( int i=0; i<12; i++) {

      rl::TKey key(2);
      key[0] = i;
      key[1] = 1.0 + i / 10.0;
      
      tree.add( key, (double) i*i );
      std::cout << tree;

      key[0] = 3.2;
      key[1] = 1.6;
      std::vector<KdTreeDataPtr> tmp_vec = tree.get_nearest_approximate( key, 1000); 
     
      std::cout << "*** list of neighors of " << key << "\n";
      BOOST_FOREACH( KdTreeDataPtr data, tmp_vec ) {
	std::cout <<  KdTreeDouble::get_key(data).repr() << " ==> " << KdTreeDouble::get_val(data) << "\n";
      }
    }

    std::cout << "** tree\n" << tree << "\n";
    std::cout << "** write in file kdtree.data\n";
    std::ofstream file;
    file.open ("kdtree.data");
    tree.write( file );
    file.close();

    std::ifstream ifile;
    ifile.open ("kdtree.data");
    KdTreeDouble arbre;
    arbre.read( ifile );
    ifile.close();
    std::cout << arbre << "\n";

    std::cout << "*** clear tree ***\n";
    arbre.clear();
    std::cout << "** arbre\n" << arbre << "\n";
  }
  catch( std::string err_msg ) {
    std::cerr << err_msg << "\n";
  }

  return 0;
}
