#include <stdio.h>
#include "regression.h"

namespace rlco = pyrbirl::core;
namespace rlnu = pyrbirl::numeric;

int
main (int argc, char *argv[] )
{
  std::vector<rlnu::TDataPtr> vec_voisins;

  rlco::MultiDouble query(3);
  query[0] = 2.1; query[1] = 2.2; query[2] = 3.3;

  rlco::MultiDouble key1(3);
  key1[0] = 1.0; key1[1] = 2.0; key1[2] = 3.0;
  rlnu::TDataPtr p1( new rlnu::TData( key1, 1.1 ));
  vec_voisins.push_back( p1 );
  rlco::MultiDouble key2(3);
  key2[0] = 4.0; key2[1] = 5.0; key2[2] = 6.0;
  rlnu::TDataPtr p2( new rlnu::TData( key2, 2.2 ));
  vec_voisins.push_back( p2 );

  rlco::MultiDouble distance(3);
  distance[0] = 2.0; distance[1] = 2.0; distance[2] = 2.0;

  rlnu::Regression reg;
  std::cout << "Query = " << query << "\n";
  std::cout << "distance = " << distance << "\n";
  reg.lwr_regression( query, vec_voisins, distance ); 

  return 0;
} 

/* Desired verbose results...
*****************************

Query = ( 2.1 2.2 3.3)
distance = ( 2 2 2)
Matrix xq
 2.1 2.2 3.3 1
Matrix X
 1 2 3 1
 4 5 6 1
Matrix w
 0.715338
 0.00923273
Matrix wX
 0.715338 0
 0 0.00923273
Matrix wX
 0.715338 0.0369309
 1.43068 0.0461637
 2.14601 0.0553964
 1 1
Matrix P
 -0.0945961 -0.203672 -0.312749 0.363554
 -0.0204163 -0.045107 -0.0697978 0.117814
 0.0537636 0.113458 0.173153 -0.127926
 0.0741799 0.158565 0.242951 -0.24574
Matrix theta
 1.59107e-16
 0.122222
 0.244444
 0.122222
Matrix yq
 1.19778
*/
