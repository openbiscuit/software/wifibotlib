
#include <stdio.h>
#include "matrix.h"

namespace rl = pyrbirl::core;

int
main (int argc, char *argv[] )
{
  rl::Matrix mat_A(3,3);
  std::cout << "*** mat_A\n" << mat_A << "\n";

  double val = 1.0;
  for( int row=0; row < mat_A.nb_row(); ++row ) {
    for( int col=0; col < mat_A.nb_col(); ++col ) {
      mat_A.set( row, col, val);
      val += 1.0;
    }
  }
  std::cout << "*** après initialisation\n" << mat_A << "\n";

  rl::Matrix mat_B(mat_A);
  std::cout << "*** copy constructor\n" << mat_B << "\n";
  
  rl::Matrix mat_C = mat_A.transpose();
  std::cout << "*** transpose\n" << mat_C << "\n";

  mat_B = mat_A.dot(mat_C);
  std::cout << "*** mat_A.mat_C \n" << mat_B << "\n";

  mat_C = mat_A.pinv();
  std::cout << "*** mat_A.pinv()\n" << mat_C << "\n";

  return 0;
} 
