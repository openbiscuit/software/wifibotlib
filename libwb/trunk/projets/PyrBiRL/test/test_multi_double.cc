
#include <stdio.h>
#include "multi-double.h"

namespace rl = pyrbirl::core;

int
main (int argc, char *argv[] )
{

  rl::MultiDouble multi(2);
  std::cout << multi << "\n";
  std::cout << multi.repr() << "\n";

  multi[0] = 3.2;
  std::cout << multi.repr() << "\n";

  rl::MultiDouble clon(multi);
  std::cout << multi.repr() << "\n";

  std::cout << multi.repr() << " et " << clon.repr() << " distance = " << clon.square_dist(multi) << "\n";
  clon[1] = 2.1;
  std::cout << multi.repr() << " et " << clon.repr() << " distance = " << clon.square_dist(multi) << "\n";

  rl::MultiDouble w(2);
  w[0] = 1.0;
  w[1] = 2.0;
  std::cout << multi.repr() << " et " << clon.repr() << " weighted_distance = " << clon.weighted_square_dist(multi, w) << "\n";

  rl::MultiDoublePtr m_p = rl::MultiDouble::make<rl::MultiDouble>(3);
  (*m_p)[1] = 2.3;
  std::cout << m_p->repr() << "\n";
  std::cout << m_p->repr() << "~" << m_p->get_ref_count() << "\n";


  std::cout << "Reading from string\n";
  m_p->from_repr( "( 0 2.3 1.4)" );
  std::cout << *m_p << "\n";
  std::cout << "Create from string\n";
  rl::MultiDouble n("(1 2.3 4.1)");
  std::cout << n << "\n";
  return 0;
}
