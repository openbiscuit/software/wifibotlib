# Standard things
# saves the variable $(d) that holds the current directory on the stack,
# and sets it to the current directory given in $(dir), 
# which was passed as a parameter by the parent rules.mk

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Subdirectories, if any, in random order

#dir	:= $(d)/test
#include		$(dir)/Rules.mk


# Local rules and target

TGTS_$(d)	:= $(d)/test_multi_double   \
                   $(d)/test_kdtree \
                   $(d)/test_matrix \
                   $(d)/test_regression \
                   $(d)/test_algo_hedger
DEPS_$(d)	:= $(TGTS_$(d):%=%.d)

TGT_BIN		:= $(TGT_BIN) verbose_$(d) $(TGTS_$(d))
CLEAN		:= $(CLEAN) $(TGTS_$(d)) $(DEPS_$(d))


$(TGTS_$(d)):	$(d)/rules.mk 

$(TGTS_$(d)):	CF_TGT := -Icpp -I. -DRADDB=\"$(DIR_ETC)\"
$(TGTS_$(d)):	LF_TGT := -lboost_python -lpython2.5 \
                          -lgsl -lgslcblas -lm \
                          -llog4cxx
#                         -L./dana -ldana		
$(TGTS_$(d)):	LL_TGT := $(S_LL_INET) cpp/core.a dana-cpp/dana.a
$(d)/test_multi_double:	$(d)/test_multi_double.cc cpp/core.a dana-cpp/dana.a
		$(COMPLINK)
$(d)/test_kdtree:	$(d)/test_kdtree.cc cpp/core.a dana-cpp/dana.a
		$(COMPLINK)
$(d)/test_matrix:	$(d)/test_matrix.cc cpp/core.a dana-cpp/dana.a
		$(COMPLINK)
$(d)/test_regression:	$(d)/test_regression.cc cpp/core.a dana-cpp/dana.a
		$(COMPLINK)
$(d)/test_algo_hedger:	$(d)/test_algo_hedger.cc cpp/core.a dana-cpp/dana.a
		$(COMPLINK)


.PHONY : verbose_$(d)
verbose_$(d): $(TGTS_$(d))
	@echo "Generating $^"

# As a last step, we restore the value of the current directory variable $(d) 
# by taking it from the current location on the stack, and we "decrement" 
# the stack pointer by stripping away the .x suffix 
# we added at the top of this file.

# Standard things

-include	$(DEPS_$(d))

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))

