
#include <stdio.h>
#include "node.hpp"

namespace rl = pyrbirl::core;

typedef rl::Node<double> NodeDouble;

int
main (int argc, char *argv[] )
{

  NodeDouble node(5);
  std::cout << node;
  std::cout << node.repr();

  NodeDouble *split_node;

  for( int i=0; i<6; i++) {
    //int *key = new int[1];
    rl::MultiDouble key(2);
    key[0] = i;
    key[1] = 1.0 + i / 10.0;
    split_node = node.add( key, (double) i*i );
    if( split_node != NULL ) {
      std::cout << "splitting...\n";
      split_node->split(0, (double) i/2);
      split_node->add( key, (double) i*i );
    }
    std::cout << node.repr();
  }


  return 0;
}
