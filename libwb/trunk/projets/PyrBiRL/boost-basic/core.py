"""
Some boost mechanisms in situation
----------------------------------------------------------------------
This simple example illustrates some boost mechanisms among which:
  - inheritance
  - class having back references onto the PyObject that holds
     the underlying C++ object
  - vector indexing suite
     (that gives access to C++ vector-based object "a la python"
  - dotted notation for children
     (child access via father.child or father[0])
  - simple deep clone mechanism for containers
  - creation of python objects on c++ side
  - docstrings
----------------------------------------------------------------------
"""

from libcore import *

__author__  = "Nicolas Rougier <Nicolas.Rougier@loria.fr>"
__date__    = "April 2006"
__version__ = "1.0"
__license__ = "GNU General Public License v2 or higher"

__all__ = ['Object', 'Bag']



def make (self, typename='Object'):
    'Generic make method'
    return  eval('%s()' % typename)

# Extending Object with a make method
Object.make = make


def clone (self):
    'Generic clone method'
    _clone = type(self).__call__()
    _clone.copy (self)
    
    if (hasattr(self, '__len__')):
        for child in self:
            _child = type(child).__call__()
            _child.copy (child)
            _clone.append (_child)
    return _clone

# Extending Object with a clone method
Object.clone = clone


if __name__ == '__main__':
    import sys

    bag = Bag ('bag')
    for i in range(3):
        name = 'child_%s' % i
        bag.append (Object (name))

    print bag
    for child in bag:
        print child
    
    print bag[0], bag.child_0

    bag_c = bag.clone()
    print bag_c

    print bag[0] is bag_c[0]

    bag.clear()
    bag.populate (3)
    print bag
