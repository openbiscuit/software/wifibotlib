/*
 * Some boost mechanisms in situation
 * Copyright (c) 2006 Nicolas Rougier
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/*
 *  This simple example illustrates some boost mechanisms among which:
 *   - inheritance
 *   - class having back references onto the PyObject that holds
 *      the underlying C++ object
 *   - vector indexing suite
 *      (that gives access to C++ vector-based object "a la python"
 *   - dotted notation for children
 *      (child access via father.child or father[0])
 *   - simple deep clone mechanism for containers
 *   - creation of python objects on c++ side
 *   - docstrings
 *
 *  Two classes are defined:
 *   - Object is the base class
 *   - Bag is a container (std::vector)
 * 
 */
#include <string>
#include <vector>
#include <iostream>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

namespace py = boost::python;

typedef boost::shared_ptr<class Bag>	BagPtr;
typedef boost::shared_ptr<class Object>	ObjectPtr;


class Object {
public:
    std::string		name;
    PyObject *		self;

    Object (PyObject *self_, std::string name_="object")
        : name(name_), self(self_)
    {}

    Object (PyObject *self_, const Object &other)
        : name(other.name), self (self_)
    {}

    virtual ~Object (void)
    {}

    py::handle<> myself()
    {
        return py::handle<>(py::borrowed(self));
    }

    void copy (Object &other)
    {
        name = other.name + "_clone";
    }    

    // A nice representation
    virtual std::string repr (void)
    {
        // name only
        return name;

        // type:name
        //return self->ob_type->tp_name + std::string(":") + name;
    }    
};



class Bag : public std::vector<ObjectPtr>, public Object {
public:
    Bag (PyObject *self_, std::string name_="bag")
        : std::vector<ObjectPtr> (), Object (self_, name_)
    {}

    Bag (PyObject *self_, const Bag &other)
        : std::vector<ObjectPtr> (other),  Object (self_, other)
    {}

    ~Bag (void)
    {}

    void copy (Bag &other)
    {
        Object::copy (other);
    }    

    // Search for an object named 'name_'
    ObjectPtr get (std::string name_)
    {
        std::vector<ObjectPtr>::iterator i;
        for (std::vector<ObjectPtr>::iterator i = begin(); i != end(); i++)
            if ((*i)->name == name_)
                return *i;
        std::string msg = "'";
        msg += self->ob_type->tp_name;
        msg += "' object '" + name;
        msg += "' has no attribute '" + name_;
        msg += "'";
        PyErr_SetString(PyExc_AttributeError, msg.c_str());
        py::throw_error_already_set();
        return *end();
    }

    // Populate with some objects
    void populate (int n)
    {
        for (int i=0; i<n; i++)
            push_back (py::call_method <ObjectPtr> (self, "make", "Object"));
    }


    // A nice representation
    std::string repr (void)
    {
        std::string r = "(";
        for (std::vector<ObjectPtr>::iterator i=begin(); i!= end(); i++) {
            r += (*i)->repr();
            r += ", ";
        }
        if (size())
            r = r.substr(0, r.size()-2);
        r +=")";
        return r;
    }

};


// ===================================================================
//
//  Boost wrapping code
//
// ===================================================================

namespace boost {
    namespace python {
        template <> struct has_back_reference<Object> : mpl::true_ {};
        template <> struct has_back_reference<Bag>    : mpl::true_ {};
    }
}

BOOST_PYTHON_MODULE(libcore)
{
    using namespace boost::python;
    register_ptr_to_python< boost::shared_ptr<Object> >();
    register_ptr_to_python< boost::shared_ptr<Bag> >();

    // Object
    class_<Object>
        ("Object", "Base class for named object",
         init<optional <std::string> >())

        .add_property  ("self",     &Object::myself)
        .def_readwrite ("name",     &Object::name)
        .def           ("__repr__", &Object::repr)
        .def           ("copy", 	&Object::copy,  "copy (object)")
        ;

    // std::vector<ObjectPtr> (mandatory)
    class_<std::vector<ObjectPtr> >("ObjectVector")
        .def(vector_indexing_suite< std::vector<ObjectPtr>, true >());

    // Bag
    class_<Bag, bases < std::vector<ObjectPtr>, Object > >
        ("Bag", "Bag of objects",
         init< optional<std::string> >() )

        .def			("__getattr__", &Bag::get)
        .def			("clear",		&Bag::clear, "clear ()")
        .def			("copy", 		&Bag::copy, "copy (bag)")
        .def			("populate", 	&Bag::populate, "populate (n)")
        .add_property	("size",		&Bag::size)
        ;
}
