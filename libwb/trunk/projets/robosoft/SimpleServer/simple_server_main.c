#include "wifibot.h"

int main(int argc, char *argv[])
{
  // Alain : pour des actions differentes
  struct sigaction action;

  buffso_send[0]=0;
  buffso_send[1]=0;
  buffso_send[2]=0;	
  buffso_send[3]=0;
  buffso_send[4]=0;
  buffso_send[5]=0;
  buffso_send[6]=0;
  bufad[0]=0; 

  debug = atoi(argv[1]);
  printf("running....\n");

  initI2c();

  // Alain : thread[0] est un chien de garde pour fermer le thread de connection
  // si ouvert trop longtemps...
  if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
    fprintf (stderr, "%s", strerror (ret));
    exit (1);
  }
  // Alain : thread[1] est le thread de connection
  if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
    fprintf (stderr, "%s", strerror (ret));
    exit (1);
  }

  action.sa_handler = gestionnaire;
  sigemptyset (& (action.sa_mask));
  action.sa_flags = 0;

  if (sigaction (SIGQUIT, & action, NULL) != 0) {
    fprintf (stderr, "Erreur %d\n", errno);
    exit (1);
  }

  action . sa_handler = gestionnaire;
  sigemptyset (& (action . sa_mask));
  action . sa_flags = SA_RESTART | SA_RESETHAND;

  if (sigaction (SIGINT, & action, NULL) != 0) {
    fprintf (stderr, "Erreur %d\n", errno);
    exit (1);
  }


  while ( 1 ){
    //usleep (1 * 100000);
    sleep(5);
  }

  pthread_join (thread [0], NULL);//blocking
  pthread_join (thread [1], NULL);//blocking
  return 0;
}

/**
 * WatchDog for the Com Thread.
 * @comment Alain
 */
void * fn_thread_dog (void * num)
{
  int numero = (int) num;
	
  while (1) {
    usleep (3 * 100000);
    pthread_mutex_lock (& mutex_dog);
    dog ++;
    pthread_mutex_unlock (& mutex_dog);
    if (dog>5) {
      dog=0;
      if (debug==2) printf("dog>5\n");
      if (connected) {
	connected=0;
        stop();
        int ret=pthread_cancel (thread[1]);
       	close(clntSock);
	close(servSock);
	sleep(2);
	if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
	  fprintf (stderr, "%s", strerror (ret));
	  exit (1);
	}
      }
    }
  }//end while
  pthread_exit (NULL);
}
/**
 * Thread for com. Open a socket, listen and send commands to the wifibot.
 * @comment Alain
 */
void * fn_thread_com (void * num) 
{
  int numero = (int) num;

  /* Create socket for incoming connections (ip(7)*/
  if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("socket create error\n");
  // DieWithError("socket() failed");

  /* Construct local address structure */
  memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
  echoServAddr.sin_family = AF_INET;                /* Internet address family */
  echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
  echoServAddr.sin_port = htons(echoServPort);      /* Local port */

  /* Bind to the local address */
  int autorisation=1;
  setsockopt(servSock,SOL_SOCKET,SO_REUSEADDR,&autorisation,sizeof(int));

  if (bind(servSock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
    printf("bind error\n");
  //DieWithError("bind() failed");

  for (;;) /* Run forever */
    {       
      stop();
      /* Mark the socket so it will listen for incoming connections */
      if (listen(servSock, MAXPENDING) < 0)
        printf("listen error\n");
      //DieWithError("listen() failed");

      /* Set the size of the in-out parameter */
      clntLen = sizeof(echoClntAddr);
      /* Wait for a client to connect */
      if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0) printf("accept error\n");
      //DieWithError("accept() failed");

      /* clntSock is connected to a client! */
      do{
        if ((recvMsgSize = recv(clntSock, buffso_rcv, 2, 0)) < 1) shutdown(clntSock,1);
	else{
	  pthread_mutex_lock (& mutex_dog);
	  connected=1;	
	  dog =0;
	  pthread_mutex_unlock (& mutex_dog);	       	          
	  getAD();	
	  setI2cL();
	  getI2cL(); // Alain : works only if right after setI2cL() !!! 
	  setI2cR();
	  getI2cR();  // Alain : works only if right after setI2cR() !!! 
	  send(clntSock,buffso_send,7,0);//pb crach soft
	}
      }while(recvMsgSize>0);
      connected=0;
      stop();
      sleep(1);
    }//end for(;;)
  pthread_exit (NULL);
}//end thread

//////////////////////////
//WIFIBOT I2C Functions//
////////////////////////
void initI2c(){
  // Alain : for MIPS : sprintf(filename3,"/dev/i2c/%d",i2cbus);
  sprintf(filename3,"/dev/i2c-%d",i2cbus);
  if ((file = open(filename3,O_RDWR)) < 0) printf("error I2C open file\n");
}
/**
 * Set speed on the left.
 * @comment Alain
 */
void setI2cL(){
  bufset[0]=buffso_rcv[0];
  bufset[1]=buffso_rcv[0];
  if (ioctl(file,I2C_SLAVE,0x51) < 0) printf("error I2C ioctl slave\n");
  if (write(file,bufset,2) !=2) {
    printf("error write i2c \n");
  }
}
/**
 * Get left speed info BUT suppose that last 'set' was on this side !!!
 * @comment Alain
 */
void getI2cL(){
  if (read(file,bufget,3) !=3) {
    printf("error read i2c\n");
  }
  else {buffso_send[1]=bufget[0];buffso_send[2]=bufget[1];buffso_send[5]=bufget[2];};
}

/**
 * Set speed on the right.
 * @comment Alain
 */
void setI2cR(){
  bufset[0]=buffso_rcv[1];
  bufset[1]=buffso_rcv[1];
  if (ioctl(file,I2C_SLAVE,0x52) < 0) printf("error I2C ioctl slave\n");
  if (write(file,bufset,2) !=2) {
    printf("error write i2c \n");
  }
}
/**
 * Get left speed info BUT suppose that last 'set' was on this side !!!
 * @comment Alain
 */
void getI2cR(){
  if (read(file,bufget,3) !=3) {
    printf("error read i2c\n");
  }
  else {buffso_send[3]=bufget[0];buffso_send[4]=bufget[1];buffso_send[6]=bufget[2];};
}
/**
 * Get info from Battery.
 * @comment Alain
 */
void getAD(){
  bufset[0]=0x40;
  bufset[1]=250;//DAC
    
  if (ioctl(file,I2C_SLAVE,0x4e) < 0) printf("error I2C ioctl slave\n");
  if (write(file,bufset,2) !=2) {
    printf("error write i2c \n");
  }

  if (read(file,bufad,1) !=1) {
    printf("error read i2c\n");
  }
  else {buffso_send[0]=bufad[0]/2;if (debug==1) printf("adc val = %d\n",bufad[0]);}
}

/**
 * Stop the robot, emergency??
 * @comment Alain
 */
void stop(){
  bufset[0]=0;
  bufset[1]=0;
  if (ioctl(file,I2C_SLAVE,0x51) < 0) printf("error I2C ioctl slave\n");
  if (write(file,bufset,2) !=2) {
    printf("error write i2c \n");}
  
  bufset[0]=0;
  bufset[1]=0;
  if (ioctl(file,I2C_SLAVE,0x52) < 0) printf("error I2C ioctl slave\n");
  if (write(file,bufset,2) !=2) {
    printf("error write i2c \n");}
}

/**
 * Deal with SIGNALs.
 * @comment Alain
 */
void gestionnaire (int numero)
{
  switch (numero) {
  case SIGQUIT :
    fprintf (stdout, "\nSIGQUIT re�u\n"); fflush (stdout);		        	
    break;
  case SIGINT :
    fprintf (stdout, "\nSIGINT re�u\n"); fflush (stdout);
    stop();
    break;
  }
}
