#ifndef TCPSock_h
#define TCPSock_h

#include "SockBase.h"

int CreateTCPServer(int Port);

int CreateTCPClient(char * HostName,int Port);

int CreateTCPClient2(struct sockaddr_in dest_addr);

int AcceptConn(int Sock,char * RemoteAddr);

int TCPSend(int Sock,char * Data,int Len);

int TCPRecv(int Sock,char * Buf,int Len);

#endif
