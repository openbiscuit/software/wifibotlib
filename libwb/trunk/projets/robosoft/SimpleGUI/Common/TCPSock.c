#include "TCPSock.h"



int CreateTCPServer(int Port)
{
	int s;

	s = CreateSock(SOCK_STREAM);

	if ( s == -1 )
		return -1;

	if (BindSock(s,Port) == -1 )
		return -1;

	if (listen(s, SOMAXCONN) < 0)
		return -1;

	return s;
}




int AcceptConn(int Sock,char * RemoteAddr)
{
	struct sockaddr_in addr;
	int addrSize = sizeof(struct sockaddr_in);
	int s;

	s = accept(Sock,(struct sockaddr *)&addr,(socklen_t *)&addrSize);

	strcpy(RemoteAddr,inet_ntoa(addr.sin_addr));

	return s;
}

int CreateTCPClient2(struct sockaddr_in dest_addr)
{
	int s;
	//struct sockaddr_in dest_addr;

	s = CreateSock(SOCK_STREAM);

	if ( s == -1 )
		return -1;

	dest_addr.sin_family = AF_INET;
	//dest_addr.sin_port = htons(Port);
	//HostNameToAddr(HostName,&dest_addr);

    if ( connect(s, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr)) == -1 )
    	return -1;

    return s;
}

int CreateTCPClient(char * HostName,int Port)
{
	int s;
	struct sockaddr_in dest_addr;

	s = CreateSock(SOCK_STREAM);

	if ( s == -1 )
		return -1;

	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(Port);
	HostNameToAddr(HostName,&dest_addr);

    if ( connect(s, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr)) == -1 )
    	return -1;

    return s;
}

int TCPSend(int Sock,char * Data,int Len)
{
	return send(Sock, Data, Len, 0);
}

int TCPRecv(int Sock,char * Buf,int Len)
{

	return recv(Sock, Buf, Len, 0);
}
