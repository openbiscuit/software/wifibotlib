#ifndef SockBase_h
#define SockBase_h

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>



int CreateSock(int SockType);

int BindSock(int Sock,int Port);

void CloseSock(int Sock);

int HostNameToAddr(char * HostName,struct sockaddr_in * Addr);

#endif
