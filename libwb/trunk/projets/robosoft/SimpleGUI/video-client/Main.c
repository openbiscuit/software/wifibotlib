#include "ihm.h"

gboolean
on_Video_button_press_event            (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean configure_event_video( GtkWidget *widget, GdkEventConfigure *event, gpointer pData );

gboolean expose_event_video( GtkWidget *widget, GdkEventExpose *event,gpointer pData );

void TCPListener(gpointer data, gint source, GdkInputCondition condition);

void OnStart(GtkWidget *pWidget, gpointer pData);

void OnHome(GtkWidget *pWidget, gpointer pData);

void OnStop(GtkWidget *pWidget, gpointer pData);

void CreateIHM();

int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole);


int main (int argc, char *argv[])
{

if (lecture_arguments (argc, argv, & addr, "tcp") < 0)
		exit (1);
	addr.sin_family = AF_INET;

  	gtk_init (&argc, &argv); //Initialise GTK


	CreateIHM();

	gtk_widget_show_all(Main_Wnd); //Affiche la fenetre

        gtk_main();


	return 0;
}

gboolean
on_Video_button_press_event            (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
    gint x;
    gint y;
    GdkModifierType state;
   
    GtkWidget * Video;

    //Video = lookup_widget(GTK_WIDGET(Video),"Video");
    
    if(event->button == 1)
    {
        gdk_window_get_pointer (widget->window, &x, &y, &state);        
    }
    if(event->button == 3)
    {
        gdk_window_get_pointer (widget->window, &x, &y, &state);        
    }

        /*MovePanTiltToPos - Method
        
        Description

        Move the lens of the camera server to assigned position.

        Syntax
        Long = xplug.MovePanTiltToPos(slPanDegree As Long, slTiltDegree As Long)

        Return Value
        Return Value Description
        CV_OK  Successfully set the position.
        CV_ERROR  Fail to set the position.

        Parameter
        Parameters Description
        slPanDegree  Pan Degree. (0-340 are valid value)
        slTiltDegree  Tilt Degree. (0-135 are valid value)
   */
	//HOME H=158 V=46

if ((x>140)&&(x<180)) x=158;//deadzone
if ((y>110)&&(y<130)) y=120;//deadzone

sprintf(DataPacketPTU,"POST /PANTILTCONTROL.CGI HTTP/1.0\r\nContent-Type: text/plain\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Lenght: 69\r\n\r\nPanTiltHorizontal=%d&PanTiltVertical=%d&PanTiltPositionMove=true\r\n\r\n",x,(int)((y*-0.75)+135));

sockPTU = CreateTCPClient2(addr);
	if (sockPTU == -1)
	{
		perror("TCP sock ");
		//return 1;
	}
usleep(400000);

TCPSend(sockPTU,DataPacketPTU,192);

CloseSock(sockPTU);

return FALSE;
}

gboolean configure_event_video( GtkWidget *widget, GdkEventConfigure *event, gpointer pData )
{
  if (VideoMap)
    gdk_pixmap_unref(VideoMap);

 VideoMap = gdk_pixmap_new(widget->window,w,h,-1);
  return TRUE;
}

gboolean expose_event_video( GtkWidget *widget, GdkEventExpose *event,gpointer pData )
{
  if (VideoMap == NULL ) printf("NULL\n");
  //printf("exp_video : %d / %d\n",event->area.width, event->area.height);
  gdk_draw_pixmap(widget->window,widget->style->fg_gc[GTK_WIDGET_STATE (widget)],VideoMap,
  event->area.x, event->area.y,
  event->area.x, event->area.y,
  event->area.width, event->area.height);

 return FALSE;
}

void TCPListener(gpointer data, gint source, GdkInputCondition condition)
{

	int er;
	char szMessage[IMG_SIZE_MAX];
	char szCompleteImage[IMG_SIZE_MAX];
        char img_buf2[IMG_SIZE_MAX];
	int width,height;
 
	iTotalSize = 0;

	retval = TCPRecv(source,szMessage,sizeof(szMessage));

	if((retval == -1))//||(retval == SOCKET_ERROR))
	{
		printf("errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr\n");
	}//end of if
	else 
	{  
		    
		memset(szCompleteImage,0,320*240*3);
		memcpy(szAux,szMessage,15);
		   
		if(!strcmp(szAux,"Content-length:"))//we continue only if we are at the beginning of a new image
		{
		memcpy(szCompleteImage,szMessage,retval);
		iTotalSize = retval;
		   
		while((strcmp(szAux,"video boundary:")!=0)&&(retval!=0)&&(retval!=-1))   
		{
			retval = TCPRecv(source,szMessage,sizeof(szMessage));
			memcpy(szCompleteImage+iTotalSize,szMessage,retval);
			iTotalSize = iTotalSize + retval;
			memcpy(szAux,szMessage+(retval-18),14);
		}
		  
		if(!strcmp(szAux,"video boundary:"))	
		{
                //printf("size %d\n",iTotalSize);
		if (iTotalSize>9999)
		er=read_JPEG_memory(img_buf2, &width,&height,szCompleteImage+51,iTotalSize);
		else
		er=read_JPEG_memory(img_buf2, &width,&height,szCompleteImage+50,iTotalSize);

		gdk_draw_rgb_image(Video->window,Video->style->white_gc,0,0,width,height,GDK_RGB_DITHER_NORMAL,img_buf2,width*3);

   }
  }//end of if
}//end if
}

void OnStart(GtkWidget *pWidget, gpointer pData)
{
    sock = CreateTCPClient2(addr);
	if (sock == -1)
	{
		perror("TCP sock ");
		//return 1;
	}

  	ID_TCPListen = gdk_input_add(sock,GDK_INPUT_READ,TCPListener,NULL);

  	// Start
	// Populate the data packet
		strcpy(DataPacket, "GET /VIDEO.CGI HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n");

  	TCPSend(sock,DataPacket,128);
}

void OnHome(GtkWidget *pWidget, gpointer pData)
{
    /*
              PanSingleMoveDegree=x
              
              TilSingleMoveDegree=x

              PanTiltSingleMove=5 RIGHT
              
              PanTiltSingleMove=4 HOME
              
              PanTiltSingleMove=7 DOWN
              
              PanTiltSingleMove=3 LEFT
              
              PanTiltSingleMove=1 UP
*/

strcpy(DataPacketPTU,"POST /PANTILTCONTROL.CGI HTTP/1.0\r\nContent-length=68\r\nUser-Agent: user\r\nAuthorization: Basic \r\n\r\nPanSingleMoveDegree=1&TilSingleMoveDegree=1&PanTiltSingleMove=4\r\n\r\n");
   sockPTU = CreateTCPClient2(addr);
	if (sockPTU == -1)
	{
		perror("TCP sock ");
		//return 1;
	}
   usleep(500000);

   TCPSend(sockPTU,DataPacketPTU,164);

   CloseSock(sockPTU);
}

void OnStop(GtkWidget *pWidget, gpointer pData)
{
  
  CloseSock(sock);
  gdk_input_remove(ID_TCPListen);
  rcv_nb=0;
}


void CreateIHM()
{
	GtkWidget * MainBox,* ButtonBox, *StopBtn, *StartBtn, *HomeBtn;

	Main_Wnd = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	MainBox = gtk_vbox_new(FALSE,10);

	Video = gtk_drawing_area_new();
	gtk_widget_set_size_request(GTK_WIDGET (Video), w, h);
	g_signal_connect (G_OBJECT (Video), "expose_event",G_CALLBACK (expose_event_video), NULL);
	g_signal_connect (G_OBJECT (Video),"configure_event",G_CALLBACK (configure_event_video), NULL);
	gtk_widget_set_events (Video, GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);

	gtk_box_pack_start(GTK_BOX(MainBox), Video, TRUE, FALSE, 0);

	ButtonBox = gtk_hbutton_box_new();
	gtk_box_set_spacing(GTK_BOX(ButtonBox),5);

	StartBtn =  gtk_button_new_with_label("Start");
	gtk_box_pack_start(GTK_BOX(ButtonBox), StartBtn, TRUE, FALSE, 0);

	HomeBtn =  gtk_button_new_with_label("HOME");
	gtk_box_pack_start(GTK_BOX(ButtonBox), HomeBtn, TRUE, FALSE, 0);

	StopBtn = gtk_button_new_with_label("Stop");
	gtk_box_pack_start(GTK_BOX(ButtonBox), StopBtn, TRUE, FALSE, 0);

	g_signal_connect(G_OBJECT(StartBtn),"clicked",G_CALLBACK(OnStart),NULL);
	g_signal_connect(G_OBJECT(StopBtn),"clicked",G_CALLBACK(OnStop),NULL);
	g_signal_connect(G_OBJECT(HomeBtn),"clicked",G_CALLBACK(OnHome),NULL);

	g_signal_connect ((gpointer) Video, "button_press_event",
                    G_CALLBACK (on_Video_button_press_event),
                    NULL);

	gtk_box_pack_start(GTK_BOX(MainBox), ButtonBox, TRUE, FALSE, 0);


	gtk_container_add(GTK_CONTAINER(Main_Wnd),GTK_WIDGET(MainBox));

}

int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole)
{
	char * liste_options = "a:p:h";
	int    option;
	char * hote  = "192.168.0.20";
	char * port = "80";

	struct hostent * hostent;
	struct servent * servent;

	int    numero;

	while ((option = getopt (argc, argv, liste_options)) != -1) {
		switch (option) {
			case 'a' :
				hote  = optarg;
				break;
			case 'p' :
				port = optarg;
				break;
			case 'h' :
				fprintf (stderr, "Syntaxe : %s [-a adresse] [-p port] \n",
						argv [0]);
				return (-1);
			default	: 
				break;
		}
	}
	memset (adresse, 0, sizeof (struct sockaddr_in));
	if (inet_aton (hote, & (adresse -> sin_addr)) == 0) {
		if ((hostent = gethostbyname (hote)) == NULL) {
			fprintf (stderr, "hôte %s inconnu \n", hote);
			return (-1);
		}
		adresse -> sin_addr . s_addr =
			((struct in_addr *) (hostent -> h_addr)) -> s_addr; 
	}
	if (sscanf (port, "%d", & numero) == 1) {
		adresse -> sin_port = htons (numero);
		return (0);
	}
	if ((servent = getservbyname (port, protocole)) == NULL) {
		fprintf (stderr, "Service %s inconnu \n", port);
		return (-1);
	}
	adresse -> sin_port = servent -> s_port;
	return (0);
}
