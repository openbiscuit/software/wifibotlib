#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

#include <gtk/gtk.h>

#include <pthread.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <linux/joystick.h>

#define NB_THREADS	3
#define MAX_JOYVALUE 32767
// Driving Pad Size
#define W_JOYPAD 300
#define H_JOYPAD 300

int xr=0,yr=0;
//Declare une zone de dessin
static GdkPixmap *pixmap = NULL;
int joy_usb=0;
int comon=0;
//joy
struct js_event js;
int joyd;

float Xjoy = 0.0, Yjoy = 0.0;
float XjoyTmp = 0.0, YjoyTmp = 0.0;

struct CPoint
{
int x;
int y;
};
struct CPoint CalculConsigne(struct CPoint joypoint);
int minmax,moyx=0,moyy=0,smg,smd,vmd=0,vmg=0,comg=0,comd=0,stop=0;
int ctrlg=0,ctrld=0;
struct CPoint cons;
char sendbuf[2];
char rcvbuf[7],rcvbuftemp[7];

pthread_t thread [NB_THREADS];
int       i;
int       ret;

int dog=0;
pthread_mutex_t	mutex_dog = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t	mutex_joy_usb = PTHREAD_MUTEX_INITIALIZER;
#define MAXPENDING 5    /* Maximum outstanding connection requests */
int                sock;
struct sockaddr_in addr;
int recvMsgSize=0; 
unsigned char buffso_rcv[32]; 
//static unsigned char buffso_send[7];

int first=1;
int connected=0;
static int ID_TCPListen;

struct SensorData
	{
	int BatVoltage;
	int SpeedFrontLeft;
	int SpeedRearLeft;
	int SpeedFrontRight;
	int SpeedRearRight;
	int IRLeft;
	int IRRight;	
	};
