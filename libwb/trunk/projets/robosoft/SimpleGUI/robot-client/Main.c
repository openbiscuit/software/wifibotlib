#include "ihm.h"

int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole);

void * fn_thread_dog (void * numero);
void * fn_thread_com (void * numero);
void * fn_thread_joy_usb (void * numero);

void TCPListener(gpointer data, gint source, GdkInputCondition condition);

void
on_ComOn                     (GtkButton       *button,
                                        gpointer         user_data);
void
on_ComOff                  (GtkButton       *button,
                                        gpointer         user_data);
static gint
configure_event (GtkWidget *widget, GdkEventConfigure *event);

static gint
expose_event (GtkWidget *widget, GdkEventExpose *event);

static void
draw_brush (GtkWidget *widget, gdouble x, gdouble y);

static gint
button_press_event (GtkWidget *widget, GdkEventButton *event);

static gint
button_released_event (GtkWidget *widget, GdkEventButton *event);

static gint
motion_notify_event (GtkWidget *widget, GdkEventMotion *event);

void
quit ();

int
main (int argc, char *argv[])
 {

   GtkWidget *window; //Declare une fentre GTK
   GtkWidget *drawing_area; //Declare une zone de dessi
   GtkWidget *vbox; //Declare un une boite
  
   GtkWidget *button; //Declarre un bouton
   GtkWidget *button2; //Declarre un bouton
   GtkWidget *button3; //Declarre un bouton
   
   if (lecture_arguments (argc, argv, & addr, "tcp") < 0)
		exit (1);
   addr.sin_family = AF_INET;

   gtk_init (&argc, &argv); //Initialise GTK
  
   window = gtk_window_new (GTK_WINDOW_TOPLEVEL); //Creer uen fenetre GTK
   gtk_widget_set_name (window, "WIFIBOT");
  
   vbox = gtk_vbox_new (FALSE, 0); //Crer une nouvelle boite
   gtk_container_add (GTK_CONTAINER (window), vbox); //Rajoute la boite a la fenetre
   gtk_widget_show (vbox); //Affiche la boite
 
   gtk_signal_connect (GTK_OBJECT (window), "destroy",
                       GTK_SIGNAL_FUNC (quit), NULL);
 
   /* Create the drawing area */
  
   drawing_area = gtk_drawing_area_new (); //Crer une zone de dessins
   gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area), 240, 240); //Initialise la zone de dessins
   gtk_box_pack_start (GTK_BOX (vbox), drawing_area, TRUE, TRUE, 0); //Ajoute la zone de dessin a la boute
  
  gtk_widget_show (drawing_area); //Affiche la zone de dessin
  
   
   //Les differents signaux a connecteer
   gtk_signal_connect (GTK_OBJECT (drawing_area), "expose_event",
                       (GtkSignalFunc) expose_event, NULL);
   gtk_signal_connect (GTK_OBJECT(drawing_area),"configure_event",
                      (GtkSignalFunc) configure_event, NULL);
  
  
   gtk_signal_connect (GTK_OBJECT (drawing_area), "motion_notify_event",
                       (GtkSignalFunc) motion_notify_event, NULL);
   gtk_signal_connect (GTK_OBJECT (drawing_area), "button_press_event",
                       (GtkSignalFunc) button_press_event, NULL);
   g_signal_connect (GTK_OBJECT (drawing_area), "button_release_event",
                    G_CALLBACK (button_released_event),
                    NULL);
   gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK
                          | GDK_LEAVE_NOTIFY_MASK
                          | GDK_BUTTON_PRESS_MASK
                          | GDK_POINTER_MOTION_MASK
                          | GDK_POINTER_MOTION_HINT_MASK
  			  | GDK_BUTTON_RELEASE_MASK);
  
   button2 = gtk_button_new_with_label ("COM ON"); //Creer un bouton avec le label Quitter
   gtk_box_pack_start (GTK_BOX (vbox), button2, FALSE, FALSE, 0); //Initialise le bouton et l'ajoute a la boite
   
   button3 = gtk_button_new_with_label ("COM OFF"); //Creer un bouton avec le label Quitter
   gtk_box_pack_start (GTK_BOX (vbox), button3, FALSE, FALSE, 0); //Initialise le bouton et l'ajoute a la boite
   
   button = gtk_button_new_with_label ("Quitter"); //Creer un bouton avec le label Quitter
   gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0); //Initialise le bouton et l'ajoute a la boite
 
   //Ajoute le signal
   gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                              GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (window));
   gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                              GTK_SIGNAL_FUNC (on_ComOn),
                             GTK_OBJECT (window));  
   gtk_signal_connect_object (GTK_OBJECT (button3), "clicked",
                              GTK_SIGNAL_FUNC (on_ComOff),
                             GTK_OBJECT (window));
   gtk_widget_show (button2); //Affiche le bouton
   gtk_widget_show (button3); //Affiche le bouton
   gtk_widget_show (button); //Affiche le bouton
   
   gtk_widget_show (window); //Affiche la fenetre

   draw_brush (window, 120, 120);  

   //ID_TCPListen = gdk_input_add(sock,GDK_INPUT_READ,TCPListener,NULL);

   gtk_main (); //Rend la main a l'application
  
   return 0;
 }

void * fn_thread_dog (void * num)
{
      //int numero = (int) num;
      while (1) {
	// attend 0.06s
	usleep (60000);
	
if (!joy_usb)
{ 
sendbuf[0]=(char)(cons.x);
sendbuf[1]=(char)(cons.y);
}
else 
{
struct CPoint tmp;
pthread_mutex_lock (& mutex_joy_usb);
tmp.x=XjoyTmp;tmp.y=YjoyTmp;
pthread_mutex_unlock (& mutex_joy_usb);
cons=CalculConsigne(tmp); 
sendbuf[0]=(char)(cons.x);
sendbuf[1]=(char)(cons.y);
}
	
        send(sock, sendbuf, 2, 0);
	pthread_mutex_lock (& mutex_dog);
	dog ++;
	pthread_mutex_unlock (& mutex_dog);
	if (dog>100) {//If no reception during 5 second : reset the client
  	dog=0;
	connected=0;
        int ret=pthread_cancel (thread[1]);
	close(sock);
	sleep(2);
	if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
			fprintf (stderr, "%s", strerror (ret));
			exit (1);
			}			
	}
	}//end while
pthread_exit (NULL);
}

void * fn_thread_com (void * num) 
{
      //int numero = (int) num;
    
      if ((sock = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
		perror ("socket");
		exit (1);}
	if (connect (sock, & addr, sizeof (struct sockaddr_in)) < 0) {
		perror ("connect");
		exit (1);
	}
else {
	printf("Connected to the WIFIBOT\n");
	connected=1;        
do{
  // wait for a message in the socket
       if ((recvMsgSize = recv(sock, rcvbuftemp, 7, 0)) < 1) {printf("rcv error\n");shutdown(sock,1);}
       else {
	 // when message received, 'dog' is set to 0
		pthread_mutex_lock (& mutex_dog);
	        connected=1;	
        	dog =0;
		pthread_mutex_unlock (& mutex_dog);
        struct SensorData RobotSensors;
	RobotSensors.BatVoltage=(int)rcvbuftemp[0];
	RobotSensors.SpeedFrontLeft=(int)rcvbuftemp[1];
	RobotSensors.SpeedRearLeft=(int)rcvbuftemp[2];
	RobotSensors.SpeedFrontRight=(int)rcvbuftemp[3];
	RobotSensors.SpeedRearRight=(int)rcvbuftemp[4];
	RobotSensors.IRLeft=(int)rcvbuftemp[5];
	RobotSensors.IRRight=(int)rcvbuftemp[6];
	//printf("bat %d\n",RobotSensors.BatVoltage); 
}
}while(recvMsgSize>0);
connected=0;
}//end else
pthread_exit (NULL);
}//end thread

void * fn_thread_joy_usb (void * num)
{
      //int numero = (int) num;
      while (1) {



	usleep(100000);  
	while ( (read(joyd, &js, sizeof(struct js_event)) != -1) )
	    {
	  //printf("Event: type %d, time %d, number %d, value %d\n",js.type, js.time, js.number, js.value);
	  
	  // If Joystick movment : update X or Y value
	  if (js.type == 2)
	    {
	      // X axis
	      if (js.number == 0){
		Xjoy = (js.value * W_JOYPAD) / (float)(MAX_JOYVALUE)/(float)5.0;
		//printf("Event: type %d, time %d, number %d, value %d\n",js.type, js.time, js.number, js.value);
	      }
	      // Y axis
	      if (js.number == 1){
		Yjoy = (js.value * W_JOYPAD) / (float)(MAX_JOYVALUE)/(float)5.0;
		//printf("Event: type %d, time %d, number %d, value %d\n",js.type, js.time, js.number, js.value);
		}
	    }
if ((Xjoy>-15)&&(Xjoy<15)) Xjoy=0;
if ((Yjoy>-15)&&(Yjoy<15)) Yjoy=0;
if (Xjoy>60) Xjoy=60;
if (Xjoy<-60) Xjoy=-60;
if (Yjoy>60) Yjoy=60;
if (Yjoy<-60) Yjoy=-60;
pthread_mutex_lock (& mutex_joy_usb);
XjoyTmp=Xjoy;
YjoyTmp=Yjoy;
pthread_mutex_unlock (& mutex_joy_usb);
}
}
}

int lecture_arguments (int argc, char * argv [], struct sockaddr_in * adresse, char * protocole)
{
	char * liste_options = "a:p:h:j";
	int    option;
	
	char * hote  = "192.168.0.112";
	char * port = "15000";

	struct hostent * hostent;
	struct servent * servent;

	int    numero;

	while ((option = getopt (argc, argv, liste_options)) != -1) {
		switch (option) {
			case 'a' :
				hote  = optarg;
				break;
			case 'p' :
				port = optarg;
				break;
			case 'h' :
				fprintf (stderr, "Syntaxe : %s [-a adresse] [-p port] [-j] j for usb joystick\n",
						argv [0]);
				return (-1);
			case 'j' :
				joy_usb=1;
				break;
			default	: 
				break;
		}
	}
	memset (adresse, 0, sizeof (struct sockaddr_in));
	if (inet_aton (hote, & (adresse -> sin_addr)) == 0) {
		if ((hostent = gethostbyname (hote)) == NULL) {
			fprintf (stderr, "hote %s inconnu \n", hote);
			return (-1);
		}
		adresse -> sin_addr . s_addr =
			((struct in_addr *) (hostent -> h_addr)) -> s_addr; 
	}
	if (sscanf (port, "%d", & numero) == 1) {
		adresse -> sin_port = htons (numero);
		return (0);
	}
	if ((servent = getservbyname (port, protocole)) == NULL) {
		fprintf (stderr, "Service %s inconnu \n", port);
		return (-1);
	}
	adresse -> sin_port = servent -> s_port;
	return (0);
}

struct CPoint CalculConsigne(struct CPoint joypoint)
{
	struct CPoint calcpoint;

    ////debut filtrage	
	moyx=joypoint.x;
	moyy=-joypoint.y;
		
	if(moyy>=0)
	{
	vmg=(abs(moyx+moyy));
	vmd=(abs(moyx-moyy));
	}
	else if (moyy<0)
	{
	vmg=(abs(moyx-moyy));
	vmd=(abs(moyx+moyy));
	}

	if (ctrlg==128) minmax=40;
	else minmax=60;
	
	if (moyy>0) 
	{	
		if (moyx>=0) 
		{
			if(vmg>minmax) vmg=minmax;
			if(moyx>moyy) vmd=0;
		}

		if (moyx<0)  
		{
			if(abs(moyx)>moyy) vmg=0;
			if(vmd>minmax) vmd=minmax;
		}
	comg=ctrlg+64+vmg;comd=ctrld+64+vmd;							   //Av
	}	
	else if (moyy<0) 
	{
	if (moyx>=0) 
	{
		if(vmg>minmax) vmg=minmax;
		if(moyx>abs(moyy)) vmd=0;		
	}

	if (moyx<0)  
	{
		if(vmd>minmax) vmd=minmax;
		if(moyx<moyy) vmg=0;		
	}
	comg=ctrlg+vmg;comd=ctrld+vmd; 							//Ar
	}
	
	else 
	{
	if ((moyy==0)&&(moyx<0)) {comg=ctrlg+vmg;comd=ctrld+64+vmd;} 			//Gpivot
	if ((moyy==0)&&(moyx>0)) {comg=ctrlg+64+vmg;comd=ctrld+vmd;}			//Dpivot
	if ((moyx==0)&&(moyy==0)) {stop=1;comg=0;comd=0;vmg=0;vmd=0;}
    //Fin filtrage
	}
	calcpoint.x=comg;
	calcpoint.y=comd;

	return calcpoint;
}

/////////////////////////////////////////////////
void TCPListener(gpointer data, gint source, GdkInputCondition condition)
{	
	printf("rcv gtk\n");
}

void
on_ComOn                     (GtkButton       *button,
                                        gpointer         user_data)
{
if(!comon)
{
if (joy_usb)
{ 
// Open joy dev
      if ((joyd = open("/dev/js0", O_RDONLY)) < 0) {
	printf("error joy dev \n");
	exit(1);
      }
     
      js.type = 0;
      js.number = 0;
      js.value = 0;

if ((ret = pthread_create (& thread [2], NULL, fn_thread_joy_usb, (void *) 1)) != 0) {
			fprintf (stderr, "%s", strerror (ret));
			exit (1);
			}
}

if ((ret = pthread_create (& thread [0], NULL, fn_thread_dog, (void *) 0)) != 0) {
			fprintf (stderr, "%s", strerror (ret));
			exit (1);
			}
	
if ((ret = pthread_create (& thread [1], NULL, fn_thread_com, (void *) 1)) != 0) {
			fprintf (stderr, "%s", strerror (ret));
			exit (1);

			}
comon=1;
}
}


void
on_ComOff                  (GtkButton       *button,
                                        gpointer         user_data)
{
if (comon)
{
int ret=pthread_cancel (thread[1]);
ret=pthread_cancel (thread[0]);
//gdk_input_remove(ID_TCPListen);
close(sock);
close(joyd);
comon=0;
}
}

 //Initialize la zone de dessin, avec la taille qu'elle doit avoir ainsi que sa position
 static gint
 configure_event (GtkWidget *widget, GdkEventConfigure *event)
 {
   if (pixmap)
     gdk_pixmap_unref(pixmap);
  
   pixmap = gdk_pixmap_new(widget->window,
                           widget->allocation.width,
                           widget->allocation.height,
                           -1);//Creer une fenetre avec la zone de dessins
   gdk_draw_rectangle (pixmap, //Alloue la taille et la position de la fenetre
                       widget->style->white_gc, //Fond blanc
                       TRUE,
                       0, 0,
                       widget->allocation.width, //Largeur
                       widget->allocation.height); //Heuteur
  
   return TRUE;
 }
  
 //Redessine la zone de dessin en fonction du tracer
 static gint
 expose_event (GtkWidget *widget, GdkEventExpose *event)
{
   //Permet de dessiner le tracer fait avec la souris
   gdk_draw_pixmap(widget->window,
                   widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                  pixmap,
                   event->area.x, event->area.y, //Evenement 1 (x, y)
                   event->area.x, event->area.y, //Evenement 2 (x, y)
                   event->area.width, event->area.height); //Largeur et hauteur
  
   return FALSE;
 }
  
 //Dessine un rectangle sur la zone de dessin pour representer le tracer,
//equivalent du pinceau dans un logiciel de dessin
 static void
 draw_brush (GtkWidget *widget, gdouble x, gdouble y)
 {
   GdkRectangle update_rect; //Creer un rectangle

   update_rect.x = 0; //position x du rectangle
   update_rect.y = 0; //position y du rectangle
   update_rect.width = 240; //Largeur du rectangle
   update_rect.height = 240; //hauteur du rectangle

gdk_draw_rectangle (pixmap,
          widget->style->white_gc, //Rectangle de couleur noir
                       TRUE,
                      update_rect.x, update_rect.y,
                       update_rect.width, update_rect.height);

 gtk_widget_draw (widget, &update_rect);

   update_rect.x = x - 5; //position x du rectangle
   update_rect.y = y - 5; //position y du rectangle
   update_rect.width = 10; //Largeur du rectangle
  update_rect.height = 10; //hauteur du rectangle
   //Dessine le rectangle
  gdk_draw_rectangle (pixmap,
          widget->style->black_gc, //Rectangle de couleur noir
                       TRUE,
                      update_rect.x, update_rect.y,
                       update_rect.width, update_rect.height);
gtk_widget_draw (widget, &update_rect);   
}

 //Gere le clique de la sourir, si clique de la souris dessine un rectnagle
 static gint
 button_press_event (GtkWidget *widget, GdkEventButton *event)
 {
	
if (!joy_usb)
{ 
	int x, y;
        GdkModifierType state;
   if (event->button == 1 && pixmap != NULL)
{
     	draw_brush (widget, event->x, event->y);
	gdk_window_get_pointer (event->window, &x, &y, &state); 
	
	if ((x>110)&&(x<130)) x=120;
	if ((y>110)&&(y<130)) y=120;

	struct CPoint tmp;
	tmp.x=(x-120)/2;tmp.y=(y-120)/2;
	cons=CalculConsigne(tmp);	

}
}
   return TRUE;
 }

static gint
 button_released_event (GtkWidget *widget, GdkEventButton *event)
 {
if (!joy_usb)
{ 
	int x, y;
        GdkModifierType state;
;
   if (event->button == 1 && pixmap != NULL)
{
     	draw_brush (widget, 120, 120);
	gdk_window_get_pointer (event->window, &x, &y, &state);

	struct CPoint tmp;
	tmp.x=0;tmp.y=0;
	cons=CalculConsigne(tmp); 	

}
}
   return TRUE;
 }
  
 static gint
 motion_notify_event (GtkWidget *widget, GdkEventMotion *event)
 {
if (!joy_usb)
{ 
  int x, y;
   GdkModifierType state;
  
   if (event->is_hint)
     gdk_window_get_pointer (event->window, &x, &y, &state);
   else
     {
       x = event->x;
       y = event->y;
       state = event->state;
     }
     
   if (state & GDK_BUTTON1_MASK && pixmap != NULL)
     {
	if (x<0) x=0;
	if (x>240) x=240;
	if (y<0) y=0;
	if (y>240) y=240;
	draw_brush (widget, x, y);
	//printf("x %d y %d\n",x-120,y-120);
	if ((x>110)&&(x<130)) x=120;
	if ((y>110)&&(y<130)) y=120;
	struct CPoint tmp;
	tmp.x=(x-120)/2;tmp.y=(y-120)/2;
	cons=CalculConsigne(tmp);
	//printf("x %d y %d\n",cons.x,cons.y);   	
}
}  
  return TRUE;
 }
  
 void
 quit ()
 {
   gtk_exit (0); //Quitte l'aplication
 }
