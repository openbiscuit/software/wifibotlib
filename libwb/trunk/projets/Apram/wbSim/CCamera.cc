#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#include "CCamera.h"

CCamera::CCamera(void)
{
}

CCamera::~CCamera(void)
{

}

void CCamera::PositionCamera(v3f view, v3f pos, v3f vector)
{
  _vector = vector;
  _view = view;
  _pos = pos;
}


void CCamera::PositionCamera(float xView, float yView, float zView,
			float xPos, float yPos, float zPos,
			float xVector, float yVector, float zVector)
{
  _view = v3f(xView, yView, zView);
  _pos = v3f(xPos, yPos, zPos);
  _vector = v3f(xVector, yVector, zVector);
}

void CCamera::LookAt(void)
{
  gluLookAt(_view._x, _view._y, _view._z,
	    _pos._x, _pos._y, _pos._z,
	    _vector._x, _vector._y, _vector._z);
}

void CCamera::Rotate(float angle, float x, float y, float z)
{
  v3f newValue;
  v3f PosDiff;

  PosDiff._x = _view._x - _pos._x;
  PosDiff._y = _view._y - _pos._y;
  PosDiff._z = _view._z - _pos._z;
  
  float cosTheta = (float)cos(angle);
  float sinTheta = (float)sin(angle);
  
  newValue._x  = (cosTheta + (1 - cosTheta) * x * x)* PosDiff._x;
  newValue._x += ((1 - cosTheta) * x * y - z * sinTheta)* PosDiff._y;
  newValue._x += ((1 - cosTheta) * x * z + y * sinTheta)* PosDiff._z;

  newValue._y  = ((1 - cosTheta) * x * y + z * sinTheta)	* PosDiff._x;
  newValue._y += (cosTheta + (1 - cosTheta) * y * y)		* PosDiff._y;
  newValue._y += ((1 - cosTheta) * y * z - x * sinTheta)	* PosDiff._z;

  newValue._z  = ((1 - cosTheta) * x * z - y * sinTheta)	* PosDiff._x;
  newValue._z += ((1 - cosTheta) * y * z + x * sinTheta)	* PosDiff._y;
  newValue._z += (cosTheta + (1 - cosTheta) * z * z)		* PosDiff._z;

  _view._x = _pos._x + newValue._x;
  _view._y = _pos._y + newValue._y;
  _view._z = _pos._z + newValue._z;
}


void CCamera::Avance(float fValue)
{
  float pos_x = ( _view._x-_pos._x ) * fValue;
  float pos_z = ( _view._z-_pos._z ) * fValue;

  PositionCamera(_view._x+pos_x,
		 _view._y,
		 _view._z+pos_z,
		 _pos._x+pos_x,
		 _pos._y,
		 _pos._z+pos_z,
		 _vector._x,
		 _vector._y,
		 _vector._z);
}

