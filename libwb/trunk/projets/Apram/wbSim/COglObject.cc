#include <iostream>
#include <gtkmm.h>
#include <gtkglmm.h>
#include <GL/gl.h>
#include <GL/glu.h>


#include "COglObject.h"

COglObject::COglObject(void)
{
}

COglObject::~COglObject(void)
{

}

void COglObject::Translate3f(float x, float y, float z)
{
  _trans = v3f(x, y, z);
}

void COglObject::Rotate3f(float x, float y, float z)
{
  _rot = v3f(x, y, z);
}
void COglObject::gTranslate3f(float x, float y, float z)
{
  _gtrans = v3f(x, y, z);
}

void COglObject::gRotate3f(float x, float y, float z)
{
  _grot = v3f(x, y, z);
}

void COglObject::Vertex3f(float x, float y, float z)
{
  float x2 = x;
  float y2 = y;
  float z2 = z;
  calc_rotation(y2, z2, _rot._x);
  calc_rotation(x2, z2, _rot._y);
  calc_rotation(x2, y2, _rot._z);
  x2 += _trans._x;
  y2 += _trans._y;
  z2 = _trans._z;
  glVertex3f(x2, y2, z2);
}

void COglObject::Pave(float width, float height, float depth, float texcdw, float texcdh, float texcfw, float texcfh, int mosaic)
{
	// face avant
	if (width != 0 && height !=0)	//optimisation, on on !!!
	{
		glBegin(GL_QUADS);
		Texture(texcdw,texcdh);
		Vertex3f(width/2*-1, height/2*-1, depth/2); 	// 1
		if (mosaic == 1) texcfw = texcfw/6;
		Texture(texcfw,texcdh);
		Vertex3f(width/2, height/2*-1, depth/2);		// 2
		Texture(texcfw,texcfh);
		Vertex3f(width/2, height/2, depth/2);			// 3
		Texture(texcdw,texcfh);
		Vertex3f(width/2*-1, height/2, depth/2); 		// 4
		glEnd ();

	}

	if (height !=0 && depth != 0)
	{
	  //cote gauche
		glBegin(GL_QUADS);
		if (mosaic == 1) texcdw = texcfw;
		Texture(texcdw,texcdh);
		Vertex3f(width/2*-1, height/2*-1, depth/2*-1);	// 5
		if (mosaic == 1) texcfw = 2*texcfw;
		Texture(texcfw,texcdh);
		Vertex3f(width/2*-1, height/2*-1, depth/2);		// 1
		Texture(texcfw,texcfh);
		Vertex3f(width/2*-1, height/2, depth/2); 		// 4
		Texture(texcdw,texcfh);
		Vertex3f(width/2*-1, height/2, depth/2*-1); 	// 8
		glEnd ();

	}

	if (width !=0 && height !=0 && depth != 0)
	{
	//cote droit
		glBegin(GL_QUADS);	
		if (mosaic == 1) texcdw = texcfw;
		Texture(texcdw,texcdh);
		Vertex3f(width/2, height/2*-1, depth/2);		// 2
		if (mosaic == 1) texcfw = 1.5f * texcfw;
		Texture(texcfw,texcdh);
		Vertex3f(width/2, height/2*-1, depth/2*-1);		// 6
		Texture(texcfw,texcfh);
		Vertex3f(width/2, height/2, depth/2*-1); 		// 7
		Texture(texcdw,texcfh);
		Vertex3f(width/2, height/2, depth/2); 			// 3
		glEnd ();
	}

	if (width != 0 && height !=0 && depth != 0)
	{
		//face arriere
		glBegin(GL_QUADS);

		if (mosaic == 1) texcdw = texcfw;
		Texture(texcdw,texcdh);
		Vertex3f(width/2, height/2*-1, depth/2*-1);		// 6
		if (mosaic == 1) texcfw = 4*texcfw/3;
		Texture(texcfw,texcdh);
		Vertex3f(width/2*-1, height/2*-1, depth/2*-1);	// 5
		Texture(texcfw,texcfh);
		Vertex3f(width/2*-1, height/2, depth/2*-1); 	// 8
		Texture(texcdw,texcfh);
		Vertex3f(width/2, height/2, depth/2*-1); 		// 7
		glEnd ();

	}

	if (width !=0 && depth != 0)
	{
		// dessus
		glBegin(GL_QUADS);
		if (mosaic == 1) texcdw = texcfw;
		Texture(texcdw,texcdh);
		Vertex3f(width/2*-1, height/2, depth/2); 		// 4
		if (mosaic == 1) texcfw = 5*texcfw/4;
		Texture(texcfw,texcdh);
		Vertex3f(width/2, height/2, depth/2); 			// 3
		Texture(texcfw,texcfh);
		Vertex3f(width/2, height/2, depth/2*-1); 		// 7
		Texture(texcdw,texcfh);
		Vertex3f(width/2*-1, height/2, depth/2*-1); 	// 8
		glEnd ();
	}

	if (width !=0 && height !=0 && depth != 0)
	{
		//dessous
		glBegin(GL_QUADS);
		if (mosaic == 1) texcdw = texcfw;
		Texture(texcdw,texcdh);
		Vertex3f(width/2*-1, height/2*-1, depth/2*-1);	// 5
		if (mosaic == 1) texcfw = 6*texcfw/5;
		Texture(texcfw,texcdh);
		Vertex3f(width/2, height/2*-1, depth/2*-1);		// 6
		Texture(texcfw,texcfh);
		Vertex3f(width/2, height/2*-1, depth/2);		// 2
		Texture(texcdw,texcfh);
		Vertex3f(width/2*-1, height/2*-1, depth/2); 	// 1
		glEnd ();
	}

}


void COglObject::Disk(float rayon, float rayoni, float angle, float depth, float texcdw, float texcdh, float texcfw, float texcfh)
{
	int i = 0;
	int temp = 0;
	float k = 0;
	k = angle / 10;						//affichage de 10Â° par 10Â°
	float k1,k2;						//coordonnÃ©es pour les textures
	if (rayon != 0 || rayoni != 0) 		//evite les plantages...
	{
		glBegin(GL_QUAD_STRIP);
		for (i = 0; i < k; i++)
		{
			k2 = (texcfw - texcdw)/2;
			k1 = k2*(rayoni/rayon);
			
			Texture((float)cos((2*M_PI/36)*(i))*k1+texcdw+k2,
				(float)sin((2*M_PI/36)*(i))*k1+texcdh+k2);
			Vertex3f((float)cos((2*M_PI/36)*(i))*rayoni,
				(float)sin((2*M_PI/36)*(i))*rayoni, 0.0);

			Texture((float)cos((2*M_PI/36)*(i))*k2+texcdw+k2,
				(float)sin((2*M_PI/36)*(i))*k2+texcdh+k2);
			Vertex3f((float)cos((2*M_PI/36)*(i))*rayon,
				(float)sin((2*M_PI/36)*(i))*rayon, 0.0);

			Texture((float)cos((2*M_PI/36)*(i+1))*k1+texcdw+k2,
				(float)sin((2*M_PI/36)*(i+1))*k1+texcdh+k2);
			Vertex3f(cos((2*M_PI/36)*(i+1))*rayoni,
				(float)sin((2*M_PI/36)*(i+1))*rayoni, 0.0);

			Texture((float)cos((2*M_PI/36)*(i+1))*k2+texcdw+k2,
				(float)sin((2*M_PI/36)*(i+1))*k2+texcdh+k2);
			Vertex3f((float)cos((2*M_PI/36)*(i+1))*rayon,
				(float)sin((2*M_PI/36)*(i+1))*rayon, 0.0);
			temp = i;
		}
		glEnd ();

		if (depth != 0)
		{
			glBegin(GL_QUAD_STRIP);
			for (i = 0; i < k; i++)
			{
				if (rayon == 0) rayon = 0.001f; 	//evite les plantages...
				k2 = (texcfw - texcdw)/2;
				k1 = k2*(rayoni/rayon);
				Texture((float)cos((2*M_PI/36)*(i))*k1+texcdw+k2,
					(float)sin((2*M_PI/36)*(i))*k1+texcdh+k2);
				Vertex3f((float)cos((2*M_PI/36)*(i))*rayoni,
					(float)sin((2*M_PI/36)*(i))*rayoni, depth);

				Texture((float)cos((2*M_PI/36)*(i))*k2+texcdw+k2,
					(float)sin((2*M_PI/36)*(i))*k2+texcdh+k2);
				Vertex3f((float)cos((2*M_PI/36)*(i))*rayon,
					(float)sin((2*M_PI/36)*(i))*rayon, depth);

				Texture((float)cos((2*M_PI/36)*(i+1))*k1+texcdw+k2,
					(float)sin((2*M_PI/36)*(i+1))*k1+texcdh+k2);
				Vertex3f((float)cos((2*M_PI/36)*(i+1))*rayoni,
					(float)sin((2*M_PI/36)*(i+1))*rayoni, depth);

				Texture((float)cos((2*M_PI/36)*(i+1))*k2+texcdw+k2,
					(float)sin((2*M_PI/36)*(i+1))*k2+texcdh+k2);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayon, sin((2*M_PI/36)*(i+1))*rayon, depth);
			}
			glEnd ();

			glBegin(GL_QUAD_STRIP);
			for (i = 0; i < k; i++)
			{
				Texture(texcdw+((i/k)*(texcfw-texcdw)), texcfh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayoni, sin((2*M_PI/36)*(i))*rayoni, 0.0);
				Texture(texcdw+((i/k)*(texcfw-texcdw)), texcdh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayoni, sin((2*M_PI/36)*(i))*rayoni, depth);
				Texture(texcdw+(((i+1)/k)*(texcfw-texcdw)), texcfh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayoni, sin((2*M_PI/36)*(i+1))*rayoni, 0.0);
				Texture(texcdw+(((i+1)/k)*(texcfw-texcdw)), texcdh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayoni, sin((2*M_PI/36)*(i+1))*rayoni, depth);
			}
			glEnd ();

			glBegin(GL_QUAD_STRIP);
			for (i = 0; i < k; i++)
			{
				Texture(texcdw+((i/k)*(texcfw-texcdw)), texcfh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayon, sin((2*M_PI/36)*(i))*rayon, 0.0);
				Texture(texcdw+((i/k)*(texcfw-texcdw)), texcdh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayon, sin((2*M_PI/36)*(i))*rayon, depth);
				Texture(texcdw+(((i+1)/k)*(texcfw-texcdw)), texcfh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayon, sin((2*M_PI/36)*(i+1))*rayon, 0.0);
				Texture(texcdw+(((i+1)/k)*(texcfw-texcdw)), texcdh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayon, sin((2*M_PI/36)*(i+1))*rayon, depth);
			}
			glEnd ();

			i = 0;
			if (angle>0 && angle<360)
			{
				glBegin(GL_QUAD_STRIP);

				Texture(texcdw,texcdh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayoni, sin((2*M_PI/36)*(i))*rayoni, 0.0);
				Texture(texcfw,texcdh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayon, sin((2*M_PI/36)*(i))*rayon, 0.0);
				Texture(texcdw,texcfh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayoni, sin((2*M_PI/36)*(i))*rayoni, depth);
				Texture(texcfw, texcfh);
				Vertex3f(cos((2*M_PI/36)*(i))*rayon, sin((2*M_PI/36)*(i))*rayon, depth);
				glEnd();
				
				i = temp;
				glBegin(GL_QUAD_STRIP);

				Texture(texcdw,texcdh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayoni, sin((2*M_PI/36)*(i+1))*rayoni, 0.0);
				Texture(texcfw,texcdh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayon, sin((2*M_PI/36)*(i+1))*rayon, 0.0);
				Texture(texcdw,texcfh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayoni, sin((2*M_PI/36)*(i+1))*rayoni, depth);
				Texture(texcfw, texcfh);
				Vertex3f(cos((2*M_PI/36)*(i+1))*rayon, sin((2*M_PI/36)*(i+1))*rayon, depth);
				glEnd();
			}
		}
	}
}


///////////// PRIVATE
void COglObject::calc_rotation(float &x, float &y, float rot)
{
  float tmpVal1 = x;
  x = (x) * (float)cos(RADIAN(rot)) - (y) * (float)sin(RADIAN(rot));
  y = (y) * (float)cos(RADIAN(rot)) + (tmpVal1) * (float)sin(RADIAN(rot));
}

void COglObject::Texture(float x, float y , float z)
{
  glTexCoord3f(x, y, z);
}

