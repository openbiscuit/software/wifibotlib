/** 
 * \file
 * \brief permet de contrôler un wifibot
 */
#ifndef CWIFIBOTCLIENT_H
#define CWIFIBOTCLIENT_H

#include <gtkmm.h>
#include <gtkglmm.h>

#include "CThread.h"
#include "CTcp.h"

typedef enum
  {
    COW_ACT_FORWARD,
    COW_ACT_BACKWARD,
    COW_ACT_TURNONLEFT,
    COW_ACT_TURNONRIGHT,
    COW_ACT_NONE
  } COglWifibot_Action;

class CWifibotClient : public CThread, public CTcp
{
 public:
	CWifibotClient(void);
	~CWifibotClient(void);
	/**
	 * Gestion des réception/émition
	 * @param rcv octets reçus
	 * @param snd octets envoyés
	 */
	void treat(char *rcv, char *snd);
	void interupt(void);
	Gtk::Widget *parent;
	COglWifibot_Action _act;
 protected:
	void setup(void);
	void execute(void);
 private:
	CTcp_Cli *client;
};

#endif

