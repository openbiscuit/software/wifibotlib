#ifndef CSCENE_H
#define CSCENE_H

/**
 * \file
 * Cette classe fait le rendu de la scene
 */


#include "COglCamera.h"
#include "COglTarget.h"
#include "CCamera.h"

#define GRID_SIZE	10

class CScene
{
 public:
  CScene(void);
  ~CScene(void);

  void init(void);
  void render(void);
  void setSize(int w, int h);

  void save(char *filename);
  void save_bmp(char *filename);

  int _width; ///< largeur de la fenêtre
  int _height; ///< hauteur de la fenêtre

  CCamera camera; ///< caméra opengl
    
  COglCamera _oglCamera;
  COglTarget _target;
 private:
  void draw_floor(void);
};

#endif
