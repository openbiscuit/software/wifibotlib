#ifndef CHTTPCLIENT_H
#define CHTTPCLIENT_H

#include "CThread.h"
#include "CTcp.h"
#include "CImage.h"

class CHttpClient : public CThread, public CTcp
{
 public:
  CHttpClient(void);
  ~CHttpClient(void);
  /**
   * Envoi d'une image
   * @param img image
   * @return true si l'image est envoyé correctement
   * @return false en cas d'erreur
   */
  bool send_image(CImage *img);
  void interupt(void);
 protected:
  void setup(void);
  void execute(void);
 
 private:
  CTcp_Cli *client;
};

#endif

