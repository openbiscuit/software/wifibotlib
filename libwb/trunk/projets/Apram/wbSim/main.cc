#include <iostream>
#include <gtkmm.h>
#include <gtkglmm.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "CGtkScene.h"

int main(int argc, char** argv)
{
  Gtk::Main kit(argc, argv);

  Gtk::GL::init(argc, argv);


  int major, minor;
  Gdk::GL::query_version(major, minor);
  std::cout << "OpenGL extension version - "
	    << major << "." << minor << std::endl;
 
  CGtkScene simple;
  kit.run(simple);

  return 0;
}
