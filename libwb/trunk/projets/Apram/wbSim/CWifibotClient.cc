#include <iostream>
#include <memory.h>

#include "CWifibotClient.h"

using namespace std;


CWifibotClient::CWifibotClient(void)
{
_act = COW_ACT_NONE;
}

CWifibotClient::~CWifibotClient(void)
{
  interupt();
}

void CWifibotClient::setup(void)
{
  client = (CTcp_Cli *)_Arg;
}

void CWifibotClient::interupt(void)
{
  StopServer();
}

void CWifibotClient::execute(void)
{
  char rcv[7];
  char snd[7];
  while(1)
    {
      m_socket = client->sock;
      memset(rcv, 0, 7);
      memset(snd, 0, 7);
      if(Rec(rcv, 7) > 0)
	{
      		treat(rcv, snd);
      		Send(snd, 7);
	}
      usleep(1);
    }
}

void CWifibotClient::treat(char *rcv, char *snd)
{
  snd[0] = rcv[0];
  printf("%c", rcv[0]);
  for(int i=1;i<7;i++)
    printf(" %d ", rcv[i]);
  printf("\n");
  switch(rcv[0])
    {
    case 'Q': // quit
      break;
    case 'K': // stop motors
      break;
    case 'F': // forward
      _act = COW_ACT_FORWARD;
      break;
    case 'R': // reverse
      _act = COW_ACT_BACKWARD;
      break;
    case 'T': // turn
      if(rcv[2] == 0)
	_act = COW_ACT_TURNONLEFT;
      else
	_act = COW_ACT_TURNONRIGHT;
      break;
    case 'B': // battery level
      break;
    case 'S': // sensors
      break;
    }
}

