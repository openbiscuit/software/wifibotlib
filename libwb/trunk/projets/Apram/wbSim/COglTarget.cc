#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include "COglTarget.h"


COglTarget::COglTarget(void)
{
  Translate3f(-10/2, 2, -20);
}

COglTarget::~COglTarget(void)
{

}

void COglTarget::draw(void)
{
  glColor4f(1, .1, .5, 0);
  Disk(1.3, 0.8, 360, .1, 0, 0, 0, 0);
  glColor4f(0, 0, 1, 0);
  Disk(0.8, 0.6, 360, .1, 0, 0, 0, 0);
  glColor4f(1, 1, 0, 0);
  Disk(0.6, 0, 360, 0.1, 0, 0, 0, 0);
}
