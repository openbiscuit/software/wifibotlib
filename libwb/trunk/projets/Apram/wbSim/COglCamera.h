#ifndef COGLCAMERA_H
#define COGLCAMERA_H


#include "COglObject.h"

/**
 * \file
 * Cette classe simule la caméra du wifibot
 * permet de gérer l'affichage dans la fenêtre opengl
 * et gère les déplacement de l'objet en pan et tilt
 * @author Béchu Jérôme
 */

/**
 * définition des différents types d'action de mouvement
 * de la caméra
 */
typedef enum
  {
    COC_ACT_UP, ///< vers le haut
    COC_ACT_DOWN, ///< vers le bas
    COC_ACT_LEFT, ///< à gauche
    COC_ACT_RIGHT, ///< à droite
    COC_ACT_HOME ///< position initiale
  } COglCamera_Action;

#define COC_ANGLEDIF 6000
#define COC_ANGLE COC_ANGLEDIF/2

/**
 * classe qui hérite de l'objet opengl et singleton
 * (une seule instance de camera)
 */
class COglCamera : public COglObject
{
 public:
  /**
   * constructeur
   */
  COglCamera(void);
  /**
   * déstructeur
   */
  ~COglCamera(void);

  /**
   * affecte le degré de pan
   * @param d le nouveau degré de 1 à 10
   */
  void setPanDegree(int d);
  /**
   * affecte le degré en tilt
   * @param d le nouveau degré de 1 à 10
   */
  void setTiltDegree(int d);
  
  void execute(COglCamera_Action act);
  void draw(void);
 private:
  int _pan_degree; ///< contient le degrée de rotation en pan
  int _tilt_degree; ///< contient le degrée de rotation en tilt
  float	_angle;
  float	_angleDif;
};

#endif

