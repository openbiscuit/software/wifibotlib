#include <iostream>

#include "CGtkScene.h"
#include "Singleton.h"
#include "COglScene.h"

CGtkScene::CGtkScene(void) : m_VBox(false, 0)
{
  //
  // Top-level window.
  //
  COglScene *ogl = COglScene::getInstance();
  set_title("Wifibot Simulator");

  // Get automatically redrawn if any of their children changed allocation.
  set_reallocate_redraws(true);

  add(m_VBox);

  //
  // Simple OpenGL scene.
  //

  ogl->set_size_request(640, 480);

  m_VBox.pack_start(*ogl);


  Gtk::Main::signal_key_snooper().connect(sigc::mem_fun(*this,&CGtkScene::on_keyb));
  

  // start very very tiny web server
  web_server.start(NULL);
  // start wifibot tcp server simulator
  wifibot.start(NULL);
  wifibot.cli.parent = this;

  d_timeout_function_connection = Glib::signal_timeout().connect(
    sigc::mem_fun(*this, &CGtkScene::on_timeout),
    500);

  // Show window.
  show_all();
}

bool CGtkScene::on_timeout(void)
{
  COglScene *ogl = COglScene::getInstance();
  switch(wifibot.cli._act)
    {
    case COW_ACT_FORWARD:
      ogl->canevas.camera.Avance(-0.2);
      break;
    case COW_ACT_BACKWARD:
      ogl->canevas.camera.Avance(0.2);
      break;
    case COW_ACT_TURNONLEFT:
      ogl->canevas.camera.Rotate(0.2, 0, 1, 0);
      break;
    case COW_ACT_TURNONRIGHT:
      ogl->canevas.camera.Rotate(0.2, 0, -1, 0);
      break;
    case COW_ACT_NONE:
      break;
    }
  wifibot.cli._act = COW_ACT_NONE;
  ogl->invalidate();
  ogl->update();
  return true;
}


CGtkScene::~CGtkScene(void)
{
}

void CGtkScene::forward(void)
{
  COglScene *ogl = COglScene::getInstance();
  ogl->canevas.camera.Avance(-0.1);
//  ogl->invalidate();
 // ogl->update();
 
}

int CGtkScene::on_keyb(Gtk::Widget * widget, GdkEventKey *event)
{
  if(event->type != GDK_KEY_RELEASE)
    return TRUE;
 COglScene *ogl = COglScene::getInstance();
  if(event->keyval == GDK_Up)
    {
      ogl->canevas.camera.Avance(-0.1);
//      forward();
  //    return 3;
      //canevas._wifibot.execute(COW_ACT_FORWARD);
    }
 if(event->keyval == GDK_Left)
    {
       ogl->canevas.camera.Rotate(0.1, 0, 1, 0);
//      CamTmp.Rotate(0.1f, 0.0f, 1.0f, 0.0f);
      //canevas._wifibot.execute(COW_ACT_TURNONLEFT);
    }
 if(event->keyval == GDK_Right)
    {
      ogl->canevas.camera.Rotate(0.1, 0, -1, 0);
      //canevas._wifibot.execute(COW_ACT_TURNONRIGHT);
    }
 if(event->keyval == GDK_Down)
    {
      ogl->canevas.camera.Avance(0.1);
     // canevas._wifibot.execute(COW_ACT_BACKWARD);
    }

 if(event->keyval == GDK_q)
   {
     web_server.interupt();
     Gtk::Main::quit();
   }

 ogl->invalidate();
 ogl->update();

  return TRUE;
}

