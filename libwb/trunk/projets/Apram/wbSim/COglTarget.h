#ifndef COGLTARGET_H
#define COGLTARGET_H

#include "COglObject.h"

class COglTarget : public COglObject
{
 public:
  COglTarget(void);
  ~COglTarget(void);
  void draw(void);
};

#endif

