#ifndef COGLSCENE_H
#define COGLSCENE_H

#include <gtkmm.h>
#include <gtkglmm.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "CScene.h"
#include "Singleton.h"

class COglScene : public Gtk::DrawingArea,
  public Gtk::GL::Widget<COglScene>,
  public Singleton<COglScene>
{
 public:
  COglScene(void);
  virtual ~COglScene(void);
  void invalidate(void);
  void update(void);
  CScene canevas;
 protected:
  virtual void on_realize();
  virtual bool on_configure_event(GdkEventConfigure* event);
  virtual bool on_expose_event(GdkEventExpose* event);
};

#endif

