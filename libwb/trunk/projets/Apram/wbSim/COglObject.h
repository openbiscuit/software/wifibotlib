#ifndef COGLOBJECT_H
#define COGLOBJECT_H


/**
 * \file
 * définition d'un objet opengl
 * @author Béchu Jérôme
 */

#include "types.h"

//#define M_PI 3.1415926535897932384626433832795f
#define RADIAN(X) (2*M_PI*X)/360

class COglObject
{
 public:
  COglObject(void);
  ~COglObject(void);

  /**
   * translation des coordonnées de l'objet
   * @param x translation x
   * @param y translation y
   * @param z translation z
   */
  void Translate3f(float x, float y, float z);
  void gTranslate3f(float x, float y, float z);
  /**
   * rotation de l'object
   * @param x rotation en x
   * @param y rotation en y
   * @param z rotation en z
   */
  void Rotate3f(float x, float y, float z);
  void gRotate3f(float x, float y, float z);
  void Vertex3f(float x, float y, float z);
  void Texture(float x, float y, float z=0);
  void Pave(float width, float height, float depth, float texcdw, float texcdh, float texcfw, float texcfh, int mosaic);
  void Disk(float rayon, float rayoni, float angle, float depth, float texcdw, float texcdh, float texcfw, float texcfh);
  void draw(void) {};
 protected:
 // v3f _pos;
  v3f _rot; ///< vecteur de rotation
  v3f _trans; ///< vecteur de translation
  v3f _grot;
  v3f _gtrans;
  void calc_rotation(float &x, float &y, float rot);
};

#endif

