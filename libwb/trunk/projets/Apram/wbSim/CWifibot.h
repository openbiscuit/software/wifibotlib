#ifndef CWIFIBOT_H
#define CWIFIBOT_H

#include "CWifibotClient.h"
#include "CThread.h"
#include "CTcp.h"

class CWifibot : public CThread, public CTcp
{
 public:
  CWifibot(void);
  ~CWifibot(void);
  void interupt(void);
  CWifibotClient cli;
 protected:
  void setup(void);
  void execute(void);
 
};

#endif
