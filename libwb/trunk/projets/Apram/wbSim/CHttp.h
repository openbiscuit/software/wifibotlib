#ifndef CHTTP_H
#define CHTTP_H


#include "CHttpClient.h"
#include "CThread.h"
#include "CTcp.h"

class CHttp : public CThread, public CTcp
{
 public:
  CHttp(void);
  ~CHttp(void);
  void interupt(void);

 protected:
  void setup(void);
  void execute(void);
  CHttpClient cli;
};

#endif

