#ifndef CGTKSCENE_H
#define CGTKSCENE_H

#include <gtkmm.h>
#include <gtkglmm.h>

#include "CWifibot.h"

#include "CHttp.h"

class CGtkScene : public Gtk::Window
{
 public:
  CGtkScene(void);
  virtual ~CGtkScene(void);
  ///< gestion du clavier
  int on_keyb(Gtk::Widget *,GdkEventKey *);
  void forward(void);
 protected:
  // signal handlers:

  bool on_timeout(void);
  // member widgets:
  Gtk::VBox m_VBox;

  
  CHttp web_server;
  CWifibot wifibot;
  Glib::Timer       d_wasted_time_tracker;
  sigc::connection  d_timeout_function_connection;
};

#endif

