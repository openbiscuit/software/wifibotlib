#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>

#include "Singleton.h"
#include "List.h"
#include "CImage.h"
#include "CListImage.h"




#include "CScene.h"

CScene::CScene(void)
{
 
}

CScene::~CScene(void)
{

}

void CScene::setSize(int w, int h)
{
  _width = w;
  _height = h;
}

void CScene::init(void)
{
  // fond bleu
  glClearColor (0.227f,0.431f,0.647f,0.0f);
  // efface le Depth Buffer
  glClearDepth(1.0);
  // activation texture ...
  glEnable(GL_TEXTURE_2D);
  // z buffer
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);										// Enable Smooth Shading
  glClearDepth(1.0f);												// Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);										// Enables Depth Testing
  glDepthFunc(GL_LEQUAL);											// The Type Of Depth Testing To Do
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);				// Really Nice Perspective Calculations
  camera.PositionCamera(7, 2.0f, 7,  0, 2.0f, 0,   0, 1, 0);
}

void CScene::draw_floor(void)
{
  glColor3ub(0, 0, 100);
  
  // Draw a 1x1 grid along the X and Z axis'
  for(float i = -GRID_SIZE; i <= GRID_SIZE; i += 1)
    {
      // Start drawing some lines
      glBegin(GL_LINES);
      
      // Do the horizontal lines (along the X)
      glVertex3f(-GRID_SIZE, 0, i);
      glVertex3f(GRID_SIZE, 0, i);
      
      // Do the vertical lines (along the Z)
      glVertex3f(i, 0, -GRID_SIZE);
      glVertex3f(i, 0, GRID_SIZE);
      
      // Stop drawing lines
		glEnd();
    }
}

void CScene::render(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0f, _width/_height, .1f ,600.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  
  camera.LookAt();

  draw_floor();
  
  _target.draw();
  
  CImage img;
  GLubyte *buffer = new GLubyte[ _width * _height * 3];

  glReadPixels (0, 0, _width, _height, GL_RGB, GL_UNSIGNED_BYTE, buffer); 

  if(CListImage::getInstance()->GetLength() < 10)
    CListImage::getInstance()->add_picture(buffer, _width, _height);

  delete buffer;
}

void CScene::save (char *filename) 
{
  GLubyte *buffer = new GLubyte[_width*_height*3];
  glPixelStorei (GL_PACK_ALIGNMENT, 1);
  glReadPixels (0, 0, _width, _height, GL_RGB, GL_UNSIGNED_BYTE, buffer); 
  FILE *file = fopen (filename, "wb");
  fprintf (file, "P6 %d %d 255\n", _width, _height);
  fwrite (buffer, sizeof(GLubyte), _width * _height * 3, file);
  fclose (file);
  delete buffer;
}

