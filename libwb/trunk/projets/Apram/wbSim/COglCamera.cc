#include <iostream>
#include <math.h>
#include "COglCamera.h"

COglCamera::COglCamera(void) : _pan_degree(1), _tilt_degree(1),
			       _angle(0), _angleDif(0)
{
  
}

COglCamera::~COglCamera(void)
{

}

void COglCamera::setPanDegree(int d)
{
  if(d < 1 || d > 10) return ;
  _pan_degree = d;
}

void COglCamera::setTiltDegree(int d)
{
  if(d < 1 || d > 10) return ;
  _tilt_degree = d;
}

void COglCamera::execute(COglCamera_Action act)
{
  switch(act)
    {
    case COC_ACT_UP:
      
      break;
    case COC_ACT_DOWN:
      break;
    case COC_ACT_LEFT:
      _angle -= M_PI/(COC_ANGLE);
      _angleDif += M_PI/(COC_ANGLEDIF);
      break;
    case COC_ACT_RIGHT:
      //_angle += M_PI/(COC_ANGLE);
      //_angleDif -= M_PI/(COC_ANGLEDIF);
      Rotate3f(0, _angle, 0);
      //Rotate3f(_angle, _angle, 0);
      _angle += 5;
      break;
    case COC_ACT_HOME:
    default:
       Rotate3f(0, 0, 0);
       Translate3f(0, 0, 0);
      break;
    }
}


void COglCamera::draw(void)
{
  Disk(1, 0, 180, 1, 0, 0, 0, 0);
}

