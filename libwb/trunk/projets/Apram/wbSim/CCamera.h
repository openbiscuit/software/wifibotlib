#ifndef CCAMERA_H
#define CCAMERA_H

#include "types.h"
#include "Singleton.h"
/**
 * \file
 * camera opengl
 * @author Béchu Jérôme
 */

class CCamera : public Singleton<CCamera>
{
 public:
  CCamera(void);
  ~CCamera(void);
  /*
   * positionne la caméra
   * @param view
   * @param pos
   * @param vector
   */
  void	PositionCamera(v3f view, v3f pos, v3f vector);
  void	PositionCamera(	float xView, float yView, float zView,
			float xPos, float yPos, float zPos,
			float xVector, float yVector, float zVector);
  void	LookAt(void);
  void	Avance(float fValue);
  void	Rotate(float angle, float x, float y, float z);
  
  v3f _vector;
  v3f _view;
  v3f _pos;
  v3f _rot;
};

#endif

