/**
@mainpage APRAM : Apprentissage par Renforcement pour un Agent Mobile
@authors loria - inria
@date 2008

@section intro Introduction
Cette documentation porte sur le logiciel APRAM. Apram est une implémentation de l'apprentissage par renforcement pour un robot. Le robot en question est un Wifibot (http://www.wifibot.com/).<br>
Ce dépôt contient 5 logiciels :
<ul>
 <li>algoSim : simulateur de 2D d'un apprentissage.</li>
 <li>aps : automatic parameter selection : permet de tester une série de valeurs de paramètres.</li>
 <li>viewer : permet d'exploiter les traces de l'apprentissage et d'afficher un graphe 3D du résultat de cet apprentissage.</li>
 <li>wbDriver : logiciel de pilotage de l'apprentissage, permet à l'utilisateur d'indiquer au robot les actions appropriées à réaliser</li>
 <li>wbSim : simulateur de l'environnement du wifibot (permet de tester les algorithmes sur un pc)</li>
</ul>


@section requirements Pré-requis

Voici les dépendances nécessaires à la compilation du dépôt :
<ul>
 <li>gtk : http://www.gtk.org/</li>
 <li>gtkmm : http://www.gtkmm.org/</li>
 <li>opengl : http://www.opengl.org/</li>
 <li>gsl : http://www.gnu.org/software/gsl/</li>
 <li>jpeg : http://www.ijg.org/</li>
</ul>


@section wbDriver
Un fichier de configuration doit être présent dans le même répertoire que le binaire. Ce fichier doit se nommer config.xml et dispose des paramètres suivants : 
<ul>
 <li>Adresse ip et socket de connexion au wifibot</li>
 <li>Adresse ip et socket de connexion à la caméra</li>
 <li>bandwith : bande passante</li>
 <li>nb_points_min : nombre de points minimum pour réaliser l'interpolation</li>
 <li>nb_points_max : nombre de points maximum pour réaliser l'interpolation</li>
 <li>alpha</li>
 <li>gamma</li>
 <li>lambda</li>
 <li>threshold</li>
</ul>

@section viewer
Le fichier de trace pris en compte doit se situer dans le même répertoire que le binaire et doit se nommer traces.txt. Attention sur un système US le fichier doit contenir des nombre réels avec des points et des virgules pour un système fr.
Pour ne tenir compte que d'une partie des traces, il faut double cliquer sur la dernière trace à prendre en compte dans la boite de dialogue de liste des traces.


*/


