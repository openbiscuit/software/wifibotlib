#ifndef CGENERATOR_H
#define CGENERATOR_H

#include "Singleton.h"
#include "SciGL.h"

/**
 * définition d'un paramètre
 */
typedef struct
{
  GtkWidget *label; ///< label gtk
  GtkWidget *entry; ///< input gtk correspondant
  const char *name; ///< nom du paramètre
} param_input;

class CGenerator : public Singleton<CGenerator>
{
 public:
  CGenerator(void);
  ~CGenerator(void);

  /**
   * créé et affiche la fenêtre
   */
  void Show(void);

  // accesseurs
  /**
   * récupère la valeur entier d'un paramètre
   * @param p label du paramètre
   * @return valeur du paramètre
   */
  int get_parameter(const char *p);

  /**
   * récupère une valeur flottante d'un param
   * @param p label du paramètre
   * @return valeur du param
   */
  float get_parameter_f(const char *p);

  /**
   * affecter un paramètre
   * @param value valeur du paramètre
   * @param p nom du paramètre
   */
  void set_parameter(char *value, const char *p);

  /**
   * affecter un paramètre
   * @param value valeur du paramètre
   * @param p nom du paramètre
   */
  void set_parameter(int value, const char *p);

  /**
   * affecter un paramètre
   * @param value valeur du paramètre
   * @param p nom du paramètre
   */
  void set_parameter(float value, const char *p);

  /**
   * récupère l'option interpolation
   * @return true activé l'interpolation
   * @return false interpolation désactivé
   */
  bool interpolation(void);

  /**
   * récupère l'option peng
   * @return true active peng
   * @return false peng désactivé
   */
  bool peng(void);

  static SciGL *_graph;
 private:
  /**
   * bouton afficher le graph
   */
  static void show_graph(GtkWidget *widget, gpointer data);
  /**
   * bouton afficher arbre.
   */
  static void display_tree(GtkWidget *widget, gpointer data);

  /**
   * option activer/désactiver l'interpolation
   */
  static void switch_interpol(GtkWidget *widget, gpointer window);

  /**
   * option activier/désactiver peng
   */
  static void switch_peng(GtkWidget *widget, gpointer window);


  GtkWidget *_window; ///< handle fenêtre
  GtkWidget *_fixed; ///< layout
  GtkWidget *_b_show_graph; ///< bouton afficher graphe
  GtkWidget *_b_display_tree; ///< bouton afficher arbre
  GtkWidget *_check_interpol; ///< check interpolation
  GtkWidget *_check_peng; ///< check peng

  static bool _interpol;
  static bool _peng;
  static param_input *_parameters;

};


#endif
