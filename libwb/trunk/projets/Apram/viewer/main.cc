#include <gtk/gtk.h>
#include <gtk/gtkgl.h>

#include "CTrace.h"
#include "CGenerator.h"
#include "CQuery.h"
#include "CMatrix.h"

int main(int argc, char **argv)
{
 /* CMatrix *K = new CMatrix(4, 4);
  K->Set(0, 0, 66);
  K->Set(1, 0, 71);
  K->Set(2, 0, 85);
  K->Set(3, 0, 1);

  K->Set(0, 1, 503);
  K->Set(1, 1, 516);
  K->Set(2, 1, 549);
  K->Set(3, 1, 1);

  K->Set(0, 2, 1);
  K->Set(1, 2, 1);
  K->Set(2, 2, 1);
  K->Set(3, 2, 1);

  K->Set(0, 3, 1);
  K->Set(1, 3, 1);
  K->Set(2, 3, 1);
  K->Set(3, 3, 1);

  
 K->Print();
 K->Normalize(0);
 K->Normalize(1);
 K->Print();*/
 /* CMatrix *a = K->PseudoInverse();
  a->Print();
  delete a;
  printf("\n");

 DMat nK;
 DMat Ki;
 nK = svdNewDMat(4, 4);

 nK->value[0][0] = 66;
 nK->value[1][0] = 71;
 nK->value[2][0] = 85;
 nK->value[3][0] = 1;

 nK->value[0][1] = 503;
 nK->value[1][1] = 516;
 nK->value[2][1] = 549;
 nK->value[3][1] = 1;

 nK->value[0][2] = 1;
 nK->value[1][2] = 1;
 nK->value[2][2] = 1;
 nK->value[3][2] = 1;

 nK->value[0][3] = 1;
 nK->value[1][3] = 1;
 nK->value[2][3] = 1;
 nK->value[3][3] = 1;

// printmatrix(nK);
 Ki = matrix_inv(nK);
 printmatrix(Ki);
 svdFreeDMat(nK);
 svdFreeDMat(Ki);
*/
  gtk_init (NULL, NULL);

  CTrace *traces = CTrace::getInstance();
  CGenerator *generator = CGenerator::getInstance();
  CQuery *query = CQuery::getInstance();
  
  printf("Chargement du fichier de trace ...\n");
  if(traces->load() == false)
    {
      return 0;
    }

  generator->Show();
  traces->Show();
  query->Show();

  gtk_main ();
  return 0;
}


