#include <gtk/gtk.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>

#include "CTrace.h"
#include "CGenerator.h"

col_item colonnes[] = {
  {INDICE, "indice"},
  {OLD_DIST, "old dist"},
  {OLD_ANGLE, "old angle"},
  {ACTION, "action"},
  {DIST, "dist"},
  {ANGLE, "angle"},
  {REWARD, "reward"}
};

int CTrace::_nb_points = 0;
CList *CTrace::_tr = new CList();


CTrace::CTrace(void)
{
  odi = oan = act = di = an = 0;
  dist_min = dist_max = angle_min = angle_max;
}

void CTrace::cb_select (GtkTreeView *p_tree_view, GtkTreePath *arg1, GtkTreeViewColumn *arg2, gpointer user_data)
{
  gint indice = -1;
  GtkTreeIter iter;
  GtkTreeModel *p_tree_model = NULL;

  p_tree_model = gtk_tree_view_get_model (p_tree_view);
  gtk_tree_model_get_iter (p_tree_model, &iter, arg1);
  gtk_tree_model_get (p_tree_model, &iter, 0, &indice, -1);
  _nb_points = indice;
}

bool CTrace::load(void)
{
  FILE * pf = fopen("traces.txt", "r");
  if(pf == NULL)
    {
      printf("pas de fichier traces.txt !\n");
      return false;
    }

  dist_max = angle_max = 0;
  dist_min = angle_min = 0xFFFF;
  float r;
  char *p;
  char line[255];
  memset(line, 0, 255);

  while ((fscanf(pf, "%[^\n]", line)) != EOF)
    {
      fgetc(pf);
    if(strlen(line) > 0)
	{
	  p = line;
	  odi = oan = act = di = an = 0;
	  r = 0.0;
	  
	  sscanf(line, "%f\t%f\t%d\t%f\t%f\t%f",
		 &odi, &oan, &act, &di, &an, &r);

	  if(di > 0 && an > 0)
	    {
	      if(di < dist_min) dist_min = di;
	      if(di > dist_max) dist_max = di;
	      if(an < angle_min) angle_min = an;
	      if(an > angle_max) angle_max = an;
	    }
	  
	  item_list *il = new item_list;
	  il->old_dist = odi;
	  il->old_angle = oan;
	  il->action = act;
	  il->dist = di;
	  il->angle = an;
	  il->reward = r;

	  _tr->Add(il);
	}
    memset(line, 0, 255);
    }
  fclose(pf);
  return true;
}

void CTrace::Show(void)
{
  _window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(_window), "traces");
  gtk_widget_set_size_request (_window, GDKTREEINFO_WITH,  GDKTREEINFO_HEIGHT);
  gtk_window_move((GtkWindow *)_window, 10, 10);
  store = gtk_tree_store_new (N_COLUMNS,
			      G_TYPE_INT,
                              G_TYPE_FLOAT,
			      G_TYPE_FLOAT,
			      G_TYPE_INT,
			      G_TYPE_FLOAT,
			      G_TYPE_FLOAT,
			      G_TYPE_FLOAT);
  tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  
  g_object_unref (G_OBJECT (store));
  
  renderer = gtk_cell_renderer_text_new ();
  g_object_set (G_OBJECT (renderer),
		"foreground", "red",
		NULL);
  
  for(int i=0;i<N_COLUMNS;i++)
    {
      renderer = gtk_cell_renderer_text_new ();
      column = gtk_tree_view_column_new_with_attributes (colonnes[i].mask,
							 renderer,
							 "text", colonnes[i].id,
							 NULL);
      gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);
    }
  

   scrollbar = gtk_scrolled_window_new (NULL, NULL);
   gtk_container_add(GTK_CONTAINER(_window), scrollbar);
   pVBox = gtk_vbox_new(FALSE, 0);
   gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar), pVBox);
   gtk_box_pack_start(GTK_BOX(pVBox), tree, TRUE, FALSE, 0);
   
   g_signal_connect (G_OBJECT (tree), "row-activated", G_CALLBACK (CTrace::cb_select), NULL);
   gtk_widget_show_all(_window);

   selection = gtk_tree_view_get_selection((GtkTreeView *)tree);

   for(int i=0;i<_tr->GetLength();i++)
     {
       item_list *il = (item_list *)_tr->Get(i);
       AddRow(il);
     }
}

void CTrace::AddRow(item_list *il)
{
  static int indice = 0;
  gtk_tree_store_append (store, &il->iter, NULL);

  gtk_tree_store_set (store, &il->iter,
		      INDICE, indice++,
		      OLD_DIST, il->old_dist,
		      OLD_ANGLE, il->old_angle,
		      ACTION, il->action,
		      DIST, il->dist,
		      ANGLE, il->angle,
		      REWARD, il->reward,
                    -1);
}
