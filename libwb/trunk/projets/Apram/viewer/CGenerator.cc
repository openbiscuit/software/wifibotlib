#include <gtk/gtk.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>

#include "CTrace.h"
#include "CGenerator.h"

#define PARAM_COUNT 14
const char *list_param[] = { \
  "distance min", 
  "distance max",
  "dist precision",
  "angle min",
  "angle max",
  "angle precision",
  "bandwith",
  "nb points feuille",
  "nb points",
  "nb points max",
  "alpha",
  "gamma",
  "lambda",
  "action"
};

bool CGenerator::_interpol = false;
bool CGenerator::_peng = false;
param_input *CGenerator::_parameters = NULL;
SciGL *CGenerator::_graph = NULL;

CGenerator::CGenerator(void)
{
 
}

CGenerator::~CGenerator(void)
{
  delete _parameters;
}

void CGenerator::Show(void)
{
  int i = 0;
  _window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(_window), "generator");
 
  gtk_window_set_default_size(GTK_WINDOW(_window), 290, PARAM_COUNT*30+150);
  //gtk_window_set_position(GTK_WINDOW(_window), GTK_WIN_POS_CENTER);
  
  _fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(_window), _fixed);
 gtk_window_move((GtkWindow *)_window, 10, 400);
  _b_show_graph = gtk_button_new_with_label("show graph");
 // gtk_fixed_put(GTK_FIXED(_fixed), _b_show_graph, 150, 60);
  gtk_widget_set_size_request(_b_show_graph, 90, 35);
  g_signal_connect(G_OBJECT(_b_show_graph), "clicked", 
		   G_CALLBACK(CGenerator::show_graph), NULL);
  _b_display_tree = gtk_button_new_with_label("display tree");
 // gtk_fixed_put(GTK_FIXED(_fixed), _b_show_graph, 150, 60);
  gtk_widget_set_size_request(_b_display_tree, 90, 35);
  g_signal_connect(G_OBJECT(_b_display_tree), "clicked", 
		   G_CALLBACK(CGenerator::display_tree), NULL);
  
  
  _check_interpol = gtk_check_button_new_with_label("Enable interpolation");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_check_interpol), FALSE);

  _check_peng = gtk_check_button_new_with_label("Enable peng");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_check_peng), FALSE);

  //
  g_signal_connect(G_OBJECT(_check_interpol), "clicked", 
		   G_CALLBACK(CGenerator::switch_interpol), NULL);
  g_signal_connect(G_OBJECT(_check_peng), "clicked", 
		   G_CALLBACK(CGenerator::switch_peng), NULL);

  _parameters = new param_input[ PARAM_COUNT ];
  for(i=0;i<PARAM_COUNT; i++)
    {
      _parameters[i].label = gtk_label_new( list_param[i] );
      _parameters[i].entry = gtk_entry_new();
      _parameters[i].name = list_param[i];
       gtk_fixed_put(GTK_FIXED(_fixed), 
		     _parameters[i].label, 2, i*30);
       gtk_fixed_put(GTK_FIXED(_fixed), 
		     _parameters[i].entry, 120, i*30);
    }
  gtk_fixed_put(GTK_FIXED(_fixed), _check_interpol, 2, (i++)*30);
  gtk_fixed_put(GTK_FIXED(_fixed), _check_peng, 2, (i++)*30);
  gtk_fixed_put(GTK_FIXED(_fixed), _b_show_graph, 2, (i++)*30);
  gtk_fixed_put(GTK_FIXED(_fixed), _b_display_tree, 2, (i++)*30);

  g_signal_connect_swapped(G_OBJECT(_window), "destroy", 
			   G_CALLBACK(gtk_main_quit), NULL);


   gtk_widget_show_all(_window);

   // chargement des paramètres :
 /*  set_parameter(CTrace::getInstance()->dist_min, "distance min");
   set_parameter(CTrace::getInstance()->dist_max, "distance max");
   set_parameter(CTrace::getInstance()->angle_min, "angle min");
   set_parameter(CTrace::getInstance()->angle_max, "angle max");
   set_parameter(1, "dist precision");
   set_parameter(1, "angle precision");*/
   set_parameter(0.0f, "distance min");
   set_parameter(1.0f, "distance max");
   set_parameter(0.0f, "angle min");
   set_parameter(1.0f, "angle max");
   set_parameter(0.01f, "dist precision");
   set_parameter(0.01f, "angle precision");

   set_parameter(HEDGER_BANDWITH_DEFAULT_VALUE, "bandwith");
   set_parameter( HEDGER_NB_POINTS_FEUILLE, "nb points feuille");
   set_parameter( HEDGER_NB_POINTS, "nb points");
   set_parameter( HEDGER_NB_POINTS_MAX, "nb points max");
   set_parameter( HEDGER_ALPHA_DEFAULT_VALUE, "alpha");
   set_parameter( HEDGER_GAMMA_DEFAULT_VALUE, "gamma");
   set_parameter( CPENG_DEFAULT_LAMBDA, "lambda");
   set_parameter( 1, "action");

   _graph = new SciGL();
   _graph->create();
}


void CGenerator::show_graph(GtkWidget *widget, gpointer data)
{
  _graph->compute_algo();
}

void CGenerator::display_tree(GtkWidget *widget, gpointer data)
{
  _graph->tree->Display();
}

void CGenerator::set_parameter(int value, const char *p)
{
  char sText[10];
  sprintf(sText, "%d", value);
  for(int i=0;i< PARAM_COUNT;i++)
    {
      if(strcmp(p, _parameters[i].name) == 0)
	{
	  gtk_entry_set_text(GTK_ENTRY(_parameters[i].entry), sText);
	  return ;
	}
    }
}

void CGenerator::set_parameter(float value, const char *p)
{
  char sText[10];
  sprintf(sText, "%f", value);
  for(int i=0;i< PARAM_COUNT;i++)
    {
      if(strcmp(p, _parameters[i].name) == 0)
	{
	  gtk_entry_set_text(GTK_ENTRY(_parameters[i].entry), sText);
	  return ;
	}
    }
}

void CGenerator::set_parameter(char *value, const char *p)
{
  for(int i=0;i< PARAM_COUNT;i++)
    {
      if(strcmp(p, _parameters[i].name) == 0)
	{
	  gtk_entry_set_text(GTK_ENTRY(_parameters[i].entry), value);
	  return ;
	}
    }
}

int CGenerator::get_parameter(const char *p)
{
  const gchar *sText;
  for(int i=0;i< PARAM_COUNT;i++)
    {
      if(strcmp(p, _parameters[i].name) == 0)
	{
	   sText = gtk_entry_get_text(GTK_ENTRY(_parameters[i].entry));
	   return atoi(sText);
	}
    }
  return 0;
}

float CGenerator::get_parameter_f(const char *p)
{  
  const gchar *sText;
  for(int i=0;i< PARAM_COUNT;i++)
    {
      if(strcmp(p, _parameters[i].name) == 0)
	{
	   sText = gtk_entry_get_text(GTK_ENTRY(_parameters[i].entry));
	   return atof(sText);
	}
    }
  return 0.0;

}

bool CGenerator::interpolation(void)
{
  return _interpol;
}

bool CGenerator::peng(void)
{
  return _peng;
}


void CGenerator::switch_interpol(GtkWidget *widget, gpointer window)
{
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) 
    {
      _interpol = true;	
    } 
  else 
    {
      _interpol = false;
    }
}


void CGenerator::switch_peng(GtkWidget *widget, gpointer window)
{
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) 
    {
      _peng = true;	
    } 
  else 
    {
      _peng = false;
    }
}

