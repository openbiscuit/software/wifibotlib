#ifndef CTRACE_H
#define CTRACE_H

#include "Singleton.h"
#include "List.h"

// algo
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"
#include "CPeng.h"

#define GDKTREEINFO_WITH 500 ///<largeur de la fenêtre
#define GDKTREEINFO_HEIGHT 300 ///< hauteur de la fenêtre

#define TRACE_FILE "./traces.txt"

///< liste des colonnes
enum
{
  INDICE,
  OLD_DIST,
  OLD_ANGLE,
  ACTION,
  DIST,
  ANGLE,
  REWARD,
  N_COLUMNS
};

/**
 * structure de description d'une colonne
 */
typedef struct
{
  int id;
  const char *mask;
} col_item;


/**
 * contenu d'une ligne
 */
typedef struct
{
  float old_dist;
  float old_angle;
  int action;
  float dist;
  float angle;
  float reward;
  GtkTreeIter iter;
} item_list;

class CTrace : public Singleton<CTrace>
{
public:
  /**
   * constructeur du wiget informations sur le kd-tree
   */
  CTrace(void);

  /**
   * charge le fichier de trace
   */
  bool load(void);

  /**
   * instancie le widget et l'affiche
   */
  void Show(void);

  /**
   * ajoute une ligne à la liste
   * @param indice indice de la ligne
   * @param odi old distance
   * @param oan old angle
   * @param act action
   * @param di nouvelle distance
   * @param an nouvel angle
   * @param r récompense
   */
  void AddRow(item_list *il);

  static CList *_tr; ///< traces
  static int _nb_points; ///< nombre de points
  float odi, oan, di, an;
  int act;
  float dist_min, dist_max, angle_min, angle_max;
private:
  // méthodes
  static void cb_select (GtkTreeView *p_tree_view, GtkTreePath *arg1, GtkTreeViewColumn *arg2, gpointer user_data);

  // attributs
  GtkWidget *_window; ///< handle de la fenêtre
  GtkWidget *pVBox; ///< layout
  GtkWidget *scrollbar; ///< scroll horizontale et verticale

  // la liste
  GtkTreeStore *store;
  GtkWidget *tree;
  GtkTreeSelection *selection;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
};

#endif

