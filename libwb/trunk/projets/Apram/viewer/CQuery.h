/**
 * \file
 * \brief interface permettant de calculer la valeur d'interpolation
 * d'un point distance/angle spécifié
 */
#ifndef CQUERY_H
#define CQUERY_H

#include "Singleton.h"

class CQuery : public Singleton<CQuery>
{
 public:
  CQuery(void);
  ~CQuery(void);
  
  ///< affiche le widget
  void Show(void);
 private:
 
 	/**
 	 * interpole un point (callback)
 	 * @param widget 
 	 * @param data
 	 */
 	static void interpol(GtkWidget *widget, gpointer data);

	///< handle de la fenêtre
	GtkWidget *_window;
	GtkWidget *_fixed;

	GtkWidget *_b_interpol;
	GtkWidget *label_distance;
	static GtkWidget *entry_distance;
	GtkWidget *label_angle;
	static GtkWidget *entry_angle;
	GtkWidget *label_bestaction;
	static GtkWidget *entry_bestaction;
	GtkWidget *label_status;
	static GtkWidget *entry_status;
	GtkWidget *label_qvalue;
	static GtkWidget *entry_qvalue;
};



#endif
