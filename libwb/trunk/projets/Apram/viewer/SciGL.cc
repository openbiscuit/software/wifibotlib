#include <stdlib.h>
#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <iostream>
#include <gdk/gdkkeysyms.h>


using namespace std;

#include "SciGL.h"
#include "CTrace.h"
#include "CGenerator.h"


// initialisation des objets du graphe
Frame *SciGL::frame = NULL;
Scene *SciGL::scene = new Scene();
float *SciGL::value_data = NULL;
Data *SciGL::data_graph = new Data();
SmoothSurface *SciGL::surf = new SmoothSurface ();
SciGL *SciGL::graph = new SciGL();

// initialisation des variables pour gtk
//GtkObject *SciGL::Adjust = NULL;
//GtkWidget *SciGL::pScrollbar = NULL;
//int SciGL::_iScrollbarValue = 1;
guint SciGL::idle_id = 0;

// initialisation des variables pour les algos
CKdtree *SciGL::tree = new CKdtree();
CHedger *SciGL::smart = new CHedger(SciGL::tree);
CKdtreeExport *SciGL::out = new CKdtreeExport();
CPeng *SciGL::peng = NULL;


void SciGL::init(GtkWidget *widget, gpointer   data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
    return;
  Colormap::IceAndFire()->scale(-1,1);

  surf->set_data (data_graph);
  surf->set_cmap (Colormap::IceAndFire());
  surf->set_size (1, 1, 1);
  surf->set_position (0, 0, 0);
  scene->add (surf);
  frame = new Frame();
  scene->add (frame);
  set_frame_size(1, 1, 1);
  scene->set_zoom (1.25);

  set_axis_label("distance", "angle", "q-value");

  scene->update();
  scene->build();

  gdk_gl_drawable_gl_end (gldrawable);


//  we->value[3] = 1;
}

void SciGL::compute_algo(void)
{
  KdVector *x;
  KdVector *xp;
  CTrace *traces = CTrace::getInstance();
  CGenerator *gtor = CGenerator::getInstance();
  
  // libération de la mémoire
  delete tree;
  delete smart;
//  out->Reset();

  tree = new CKdtree();
  smart = new CHedger(tree);
/*  we = CKdtree::NewKdVector(3);
  we->value[0] = 1; //poid de la distance :la distance a très peut d'importance
  we->value[1] = 1; //poid de l'angle : importance normal
  we->value[2] = 100;//l'action a une importance maximal
  smart->S->SetWeight(we);*/
//  smart->_verbosity = 3;
  out->Reset();


  if(gtor->peng() == true)//&& gtor->interpolation() == true)
    {
      peng = new CPeng(smart);
    }
  else
    {
      smart->_peng = NULL;
    }

  smart->_verbosity = 0;
 // smart->_verbosity = 2;
 // smart->S->_verbosity = 2;
  // récupération des informations de la fenêtre générator
  float h = gtor->get_parameter_f("bandwith");
  int points = gtor->get_parameter_f("nb points");
  int points_max = gtor->get_parameter_f("nb points max");
  int points_feuille = gtor->get_parameter_f("nb points feuille");
  float dist_min = gtor->get_parameter_f("distance min");
  float dist_max = gtor->get_parameter_f("distance max");
  float dist_pre = gtor->get_parameter_f("dist precision");
  float angle_min = gtor->get_parameter_f("angle min");
  float angle_max = gtor->get_parameter_f("angle max");
  float angle_pre = gtor->get_parameter_f("angle precision");
  float alpha = gtor->get_parameter_f("alpha");
  float gamma = gtor->get_parameter_f("gamma");
  float lambda = gtor->get_parameter_f("lambda");
  int action = gtor->get_parameter("action");

 // if(action < 0 || action > 4) return ;
 /*if(h == 0) return ;
  if(points == 0) return ;
  if(dist_min == 0 || dist_max == 0 || dist_pre == 0)
    return ;
  if(angle_min == 0 || angle_max == 0 || angle_pre == 0)
    return ;
*/
  tree->_limit_of_bag = points_feuille;

  smart->_bandwith = h;
  smart->_nb_points = points;
  smart->_nb_points_max = points_max;
  smart->_alpha = alpha;
  smart->_gamma = gamma;
  if(smart->_peng)
    smart->_peng->_lambda = lambda;
  int max = traces->_nb_points;
  if(max > traces->_tr->GetLength())
    max = traces->_tr->GetLength()-1;
   if(max == 0)
    max = traces->_tr->GetLength()-1; 
  for(int i=0;i<=max;i++)
    {
      item_list *il = (item_list *)traces->_tr->Get(i);
      x = CKdtree::NewKdVector(2);
      x->value[0] = il->old_dist;
      x->value[1] = il->old_angle;
      
      xp = CKdtree::NewKdVector(2);
      xp->value[0] = il->dist;
      xp->value[1] = il->angle;

      // ALAIN : moi c'est ici que je testerais si le point actuel n'est pas égal
      // à (-2,-2) pour remettre Peng à zéro.
      if( (xp->value[0] == -2) && (xp->value[1] == -2)) {
	if(smart->_peng != NULL)
	  smart->_peng->reset();
      }


      // ALAIN : bizarre, on aurait pensé que 'reward' était un paramètre de 'training()'.
      smart->_rewards = il->reward;

      smart->Training(x, xp, il->action);

      
    }

  
  // calcul du nombre de point
  long pts = ((dist_max - dist_min)/dist_pre) * ((angle_max - angle_min)/angle_pre);
  if(pts > 50000)
    {
      printf("trop de point !\n");
      return ;
    }
  printf("generation du graph !\n");
  fflush(stdout);

  out->setLimits(dist_min, angle_min, dist_max, angle_max);
  out->setPrecision(dist_pre, angle_pre);
  out->setKdtree(smart);
  out->Allocate();
//  out->ScaleZ( graph->_iScrollbarValue );
  if(gtor->interpolation() == true)
    {
      out->Interpol(action);
     // out->Interpol("test.txt", action);
    }
  else
    out->Raw(action);

  data_graph->reset();
  surf->reset();

  float dist_size = (dist_max-dist_min)/dist_pre;
  float angle_size = (angle_max-angle_min)/angle_pre;

  if(value_data) delete value_data;
  value_data = new float[(int)(dist_size * angle_size)];
  for(int i=0;i<dist_size;i++)
    {
      for(int j=0;j<angle_size;j++)
	{
	  value_data[j*(int)dist_size+i] = 0;
	}
    }
  g_resolution_x = dist_size;
  g_resolution_y = angle_size;

  data_graph->set_data(value_data,
		       Shape(g_resolution_x, g_resolution_y, 1));

  
  frame->set_frame_size(dist_size/100, 
			angle_size/100,
			1);
  surf->set_size (dist_size/100, 
			angle_size/100,
			1);

  set_x_range(dist_min, dist_max, dist_size/100);
  set_y_range(angle_min, angle_max, angle_size/100);
  set_z_range(0, (int)out->_max_z);
  upd(out->z);

  // set_size(200, 200);
  surf->set_data (data_graph);
  surf->set_cmap (Colormap::IceAndFire());
 
  surf->set_position (0,0,0);
  surf->update();

  tree->SaveTo((char *)"kdtree.txt");
}

double SciGL::best_action(double distance, double angle, int &status, double &qvalue)
{
  KdVector *xp = CKdtree::NewKdVector(3);
  double mindist = 0;
  double maxq = -1;
  double val = 0;
  int sts = 0;
  int act = 0;
  KdVectorList *list = CKdtree::NewKdVectorList();
  xp->value[0] = distance;
  xp->value[1] = angle;
  
  smart->_verbosity = 3;
  smart->S->_verbosity = 3;
  for(int i=0;i<=HEDGER_ACTION_TURNONRIGHT;i++)
    {
      xp->value[2] = i;
      val = smart->Prediction(xp, list, &mindist, &sts);
      if(val > maxq)
	{
	  maxq = val;
	  qvalue = val;
	  act = i;
	  status = sts;
	}
      smart->S->FreeMemWithoutVector(list->head);
      delete list;
      list = CKdtree::NewKdVectorList();
    }
  smart->S->FreeMemWithoutVector(list->head);
  delete list;
  smart->S->FreeMem(xp);
  smart->_verbosity = 0;
  smart->S->_verbosity = 0;
  return act;
}


gboolean SciGL::idle (GtkWidget *widget)
{

  gdk_window_invalidate_rect (widget->window, &widget->allocation, FALSE);

  return TRUE;
}



void SciGL::idle_add (GtkWidget *widget)
{
  if (idle_id == 0)
    {
      idle_id = g_idle_add_full (GDK_PRIORITY_REDRAW,
                                 (GSourceFunc) idle,
                                 widget,
                                 NULL);
    }
}

void SciGL::idle_remove (GtkWidget *widget)
{
  if (idle_id != 0)
    {
      g_source_remove (idle_id);
      idle_id = 0;
    }
}

gboolean SciGL::map (GtkWidget   *widget,
     GdkEventAny *event,
     gpointer     data)
{
  idle_add (widget);

  return TRUE;
}

gboolean SciGL::unmap (GtkWidget   *widget,
       GdkEventAny *event,
       gpointer     data)
{
  idle_remove (widget);

  return TRUE;
}

gboolean SciGL::visible (GtkWidget          *widget,
	 GdkEventVisibility *event,
	 gpointer            data)
{
  if (event->state == GDK_VISIBILITY_FULLY_OBSCURED)
    idle_remove (widget);
  else
    idle_add (widget);

  return TRUE;
}

/* change view angle, exit upon ESC */
gboolean SciGL::key (GtkWidget   *widget,
     GdkEventKey *event,
     gpointer     data)
{

  switch (event->keyval)
    {
    case GDK_z:
//      view_rotz += 5.0;
      break;
    case GDK_Z:
  //    view_rotz -= 5.0;
      break;
    case GDK_Up:
    //  view_rotx += 5.0;
      break;
    case GDK_Down:
      //view_rotx -= 5.0;
      //mouse_y += 5;
  //    mouse_x += 5;
//      scene.mouse_motion (1, mouse_x, mouse_y);
      break;
    case GDK_Left:
      //view_roty += 5.0;
      break;
    case GDK_Right:
     // view_roty -= 5.0;
      break;
    case GDK_Escape:
      gtk_main_quit ();
      break;
    case GDK_s:
      scene->save("graph.ppm");
      break;
    default:
      return FALSE;
    }

  gdk_window_invalidate_rect (widget->window, &widget->allocation, FALSE);

  return TRUE;
}


gboolean SciGL::button_press_event (GtkWidget      *widget,
		    GdkEventButton *event,
		    gpointer        data)
{
  int x = event->x;
  int y = event->y;

// if (event->state & GDK_BUTTON1_MASK)
   {
     scene->button_press (1, x, y);
   }
 // begin_x = event->x;
  //begin_y = event->y;

  return FALSE;
}

gint SciGL::motion_notify_event( GtkWidget *widget, GdkEventMotion *event, gpointer obj)
{
  int x = event->x;
  int y = event->y;


  if (event->state & GDK_BUTTON1_MASK)
    {
      scene->mouse_motion (1, x, y);
    }

  if (event->state & GDK_BUTTON2_MASK)
    {
      scene->mouse_motion (2, x, y);
    }

  return TRUE;
}

void SciGL::OnScrollbarChange(GtkWidget *pWidget, gpointer data)
{
//   _iScrollbarValue = gtk_range_get_value(GTK_RANGE(pWidget));
//   CGraph3D::getInstance()->ScaleZValues();
 //  out->ScaleZ( graph->_iScrollbarValue );
//   this->upd(out->z);
  // this->set_z_range(0, graph->_iScrollbarValue );
}

SciGL::SciGL(void)
{

}

SciGL::~SciGL(void)
{
  delete value_data;
  
  delete scene;
  delete frame;
  delete data_graph;
  delete surf;

//  gtk_widget_destroy(window);
}

void SciGL::upd(int i, int j, float v)
{
  // i est l'axe distance
  // j est l'axe angle
  value_data[j*g_resolution_x+i] = v;
  surf->update();
}

void SciGL::upd(float **tab)
{
 
  for(int i=0;i<g_resolution_x;i++)
    {
      for(int j=0;j<g_resolution_y;j++)
	{
	  value_data[j*g_resolution_x+i] = tab[i][j];
	}
    }
  surf->update();

}


void SciGL::set_axis_label(const char *x, const char *y, const char *z)
{
  frame->set_x_label(x);
  frame->set_y_label(y);
  frame->set_z_label(z);
}

void SciGL::set_frame_size(float x, float y, float z)
{

}

void SciGL::set_x_range(float min, float max, float size)
{
  frame->set_x_range(Range (min, max, 4, 20));
  frame->set_x_size(size);
  frame->set_x_position(-1*((float)size/2));
}

void SciGL::set_y_range(float min, float max, float size)
{
  frame->set_y_range(Range (min, max, 4, 20));
  frame->set_y_size(size*-1);
  frame->set_y_position((float)size/2);
}

void SciGL::set_z_range(float min, float max, float size)
{
  frame->set_z_range(Range (max, min, 4, 20));
  frame->set_z_size(-1*size);
  frame->set_z_position((float)size);
  Colormap::IceAndFire()->scale((float)size*-1, (float)size);
}



int SciGL::create (void)
{
  GdkGLConfig *glconfig;
  
  GtkWidget *vbox;
  GtkWidget *drawing_area;
  GtkWidget *button;

  gtk_gl_init (NULL, NULL);

  glconfig = gdk_gl_config_new_by_mode (GdkGLConfigMode(GDK_GL_MODE_RGB    |
					GDK_GL_MODE_DEPTH  |
							GDK_GL_MODE_DOUBLE));
  if (glconfig == NULL)
    {
      g_print ("*** Cannot find the double-buffered visual.\n");
      g_print ("*** Trying single-buffered visual.\n");

      /* Try single-buffered visual */
      glconfig = gdk_gl_config_new_by_mode (GdkGLConfigMode(GDK_GL_MODE_RGB   |
							    GDK_GL_MODE_DEPTH));
      if (glconfig == NULL)
	{
	  g_print ("*** No appropriate OpenGL-capable visual found.\n");
	  exit (1);
	}
    }

  /*
   * Top-level window.
   */

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
//   gtk_window_move((GtkWindow *)window, 650, 200);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_title (GTK_WINDOW (window), "** SciGL **");

  /* Get automatically redrawn if any of their children changed allocation. */
  gtk_container_set_reallocate_redraws (GTK_CONTAINER (window), TRUE);

  g_signal_connect (G_OBJECT (window), "delete_event",
		    G_CALLBACK (gtk_main_quit), NULL);

  /*
   * VBox.
   */

  vbox = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_widget_show (vbox);

  /*
   * Drawing area for drawing OpenGL scene.
   */

  drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 600, 600);

  /* Set OpenGL-capability to the widget. */
  gtk_widget_set_gl_capability (drawing_area,
				glconfig,
				NULL,
				TRUE,
				GDK_GL_RGBA_TYPE);

  gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK
    | GDK_LEAVE_NOTIFY_MASK
    | GDK_BUTTON_PRESS_MASK
    | GDK_POINTER_MOTION_MASK
    | GDK_POINTER_MOTION_HINT_MASK);

  gtk_widget_add_events (drawing_area,
			 GDK_VISIBILITY_NOTIFY_MASK);

  g_signal_connect_after (G_OBJECT (drawing_area), "realize",
                          G_CALLBACK (SciGL::init), NULL);

  g_signal_connect (G_OBJECT (drawing_area), "configure_event",
		    G_CALLBACK (SciGL::reshape), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "expose_event",
		    G_CALLBACK (SciGL::draw), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "map_event",
		    G_CALLBACK (SciGL::map), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "unmap_event",
		    G_CALLBACK (SciGL::unmap), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "visibility_notify_event",
		    G_CALLBACK (SciGL::visible), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "motion_notify_event",
		    G_CALLBACK (SciGL::motion_notify_event), this);
  g_signal_connect (G_OBJECT (drawing_area), "button_press_event",
		    G_CALLBACK (SciGL::button_press_event), this);

  g_signal_connect_swapped (G_OBJECT (window), "key_press_event",
			    G_CALLBACK (SciGL::key), drawing_area);


  gtk_box_pack_start (GTK_BOX (vbox), drawing_area, TRUE, TRUE, 0);

  gtk_widget_show (drawing_area);


  button = gtk_button_new_with_label ("Quit");

  g_signal_connect (G_OBJECT (button), "clicked",
		    G_CALLBACK (gtk_main_quit), NULL);

//  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  //gtk_widget_show (button);

//  Adjust = gtk_adjustment_new(1, 1, 10, 1, 10, 1);
//  pScrollbar = gtk_vscrollbar_new(GTK_ADJUSTMENT(Adjust));
///  g_signal_connect(G_OBJECT(pScrollbar), "value-changed",
//		    G_CALLBACK(SciGL::OnScrollbarChange), NULL);
  //gtk_box_pack_start(GTK_BOX(vbox), pScrollbar, TRUE, TRUE, 0);

 
  gtk_widget_show_all(window); 

  return 0;
}


gboolean SciGL::draw (GtkWidget      *widget,
      GdkEventExpose *event,
      gpointer        data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

  /*** OpenGL BEGIN ***/
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
    return FALSE;


  scene->render ();

  if (gdk_gl_drawable_is_double_buffered (gldrawable))
    gdk_gl_drawable_swap_buffers (gldrawable);
  else
    glFlush ();

  gdk_gl_drawable_gl_end (gldrawable);
  /*** OpenGL END ***/

  return TRUE;
}

/* new window size or exposure */
gboolean SciGL::reshape (GtkWidget         *widget,
	 GdkEventConfigure *event,
	 gpointer           data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

  /*** OpenGL BEGIN ***/
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
    return FALSE;

  scene->resize (widget->allocation.width, widget->allocation.height);

  gdk_gl_drawable_gl_end (gldrawable);
  /*** OpenGL END ***/

  return TRUE;
}

