#include <gtk/gtk.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>

#include "CQuery.h"
#include "CGenerator.h"

GtkWidget *CQuery::entry_distance = NULL;
GtkWidget *CQuery::entry_angle = NULL;
GtkWidget *CQuery::entry_bestaction = NULL;
GtkWidget *CQuery::entry_status = NULL;
GtkWidget *CQuery::entry_qvalue = NULL;

CQuery::CQuery(void)
{

}

CQuery::~CQuery(void)
{

}

void CQuery::Show(void)
{
  _window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(_window), "** query **");
  _fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(_window), _fixed);
  _b_interpol = gtk_button_new_with_label("Interpoler");
  gtk_window_move((GtkWindow *)_window, 700, 10);
///  gtk_widget_set_size_request(_b_interpol, 90, 35);
  g_signal_connect(G_OBJECT(_b_interpol), "clicked", 
		   G_CALLBACK(CQuery::interpol), NULL);
   int i = 0;

   label_distance = gtk_label_new( "distance : " );
   entry_distance = gtk_entry_new();
   gtk_fixed_put(GTK_FIXED(_fixed), label_distance, 2, 30*i);
   gtk_fixed_put(GTK_FIXED(_fixed), entry_distance, 120, 30*i);
   
   i++;
   label_angle = gtk_label_new( "angle : " );
   entry_angle = gtk_entry_new();
   gtk_fixed_put(GTK_FIXED(_fixed), label_angle, 2, 30*i);
   gtk_fixed_put(GTK_FIXED(_fixed), entry_angle, 120, 30*i);
   
   i++;
   gtk_fixed_put(GTK_FIXED(_fixed), _b_interpol, 2, 30*i);
   
   i++;
   label_bestaction = gtk_label_new( "meilleure action : " );
   entry_bestaction = gtk_entry_new();
   gtk_fixed_put(GTK_FIXED(_fixed), label_bestaction, 2, 30*i);
   gtk_fixed_put(GTK_FIXED(_fixed), entry_bestaction, 120, 30*i);

   i++;
   label_status = gtk_label_new( "etat interpolation : " );
   entry_status = gtk_entry_new();
   gtk_fixed_put(GTK_FIXED(_fixed), label_status, 2, 30*i);
   gtk_fixed_put(GTK_FIXED(_fixed), entry_status, 120, 30*i);

   i++;
   label_qvalue = gtk_label_new( "qvalue : " );
   entry_qvalue = gtk_entry_new();
   gtk_fixed_put(GTK_FIXED(_fixed), label_qvalue, 2, 30*i);
   gtk_fixed_put(GTK_FIXED(_fixed), entry_qvalue, 120, 30*i);

   g_signal_connect_swapped(G_OBJECT(_window), "destroy", 
			    G_CALLBACK(gtk_main_quit), NULL);
   gtk_widget_show_all(_window);
}

void CQuery::interpol(GtkWidget *widget, gpointer data)
{
  char sText[10];
  int status = 0;
  double qvalue = 0;
  CGenerator *generator = CGenerator::getInstance();
  
  int val = generator->_graph->best_action(
					   atof( gtk_entry_get_text(GTK_ENTRY(entry_distance)) ),
					   atof( gtk_entry_get_text(GTK_ENTRY(entry_angle)) ),
					   status, qvalue);
  sprintf(sText, "%d", val);
  gtk_entry_set_text(GTK_ENTRY(entry_bestaction), sText);
  sprintf(sText, "%d", status);
  gtk_entry_set_text(GTK_ENTRY(entry_status), sText);
  sprintf(sText, "%f", qvalue);
  gtk_entry_set_text(GTK_ENTRY(entry_qvalue), sText);
}
