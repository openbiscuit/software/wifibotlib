#ifndef V3F
#define V3F

class v3f
{
 public:
  v3f()
    {
      _x = _y = _z = 0;
    }
  v3f(float x, float y, float z)
    {
      _x = x;
      _y = y;
      _z = z;
    }
  v3f(const v3f &inst)
    {
      _x = inst._x;
      _y = inst._y;
      _z = inst._z;
    }
  v3f &operator= (const v3f &inst)
    {
      if(this == &inst)
	return *this;
      _x = inst._x;
      _y = inst._y;
      _z = inst._z;
      return *this;
    }
  v3f operator+ (const v3f &inst)
    {
      return v3f(_x+inst._x, _y+inst._y, _z+inst._z);
    }
  v3f operator- (const v3f &inst)
    {
      return v3f(_x-inst._x, _y-inst._y, _z-inst._z);
    }
  v3f operator* (const v3f &inst)
    {
      return v3f(_x*inst._x, _y*inst._y, _z*inst._z);
    }
  v3f operator/ (const v3f &inst)
    {
      return v3f(_x/inst._x, _y/inst._y, _z/inst._z);
    }
  float _x, _y, _z;
};

#endif
