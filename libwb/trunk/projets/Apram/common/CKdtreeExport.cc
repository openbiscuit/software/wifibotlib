#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <memory.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "Singleton.h"
#include "List.h"
#include "CImage.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"
//#include "tcp.h"

CKdtreeExport::CKdtreeExport(void) 
{
  x = y = NULL;
  z = z_backup = NULL;
  _max_z = 0;
  _limits = NULL;
  _sel = NULL;
  _smart = NULL;
  _prec_x = 1;
  _prec_y = 1;
  _limits = CKdtree::NewKdVector(4);
  _mem_allow = 0;
  _buf_image = NULL;
  _buf_image_status = NULL;
  for(int i=PRED_NORMAL;i<PRED_NULL;i++)
    _TypeOfPrediction[i] = 0;
  
}

CKdtreeExport::~CKdtreeExport(void)
{
  Reset();
}

void CKdtreeExport::Store(FILE *pf, KdNode *n, KdVector *sel)
{
  if(n->leaf)
    {
      Store(pf, n->bag, sel);
    }
  else
    {
      Store(pf, n->ChildLeft, sel);
      Store(pf, n->ChildRight, sel);
    }
}

void CKdtreeExport::Store(FILE *pf, KdBag *b, KdVector *sel)
{
  Store(pf, b->list, sel);
}

void CKdtreeExport::Store(FILE *pf, KdVector *v, KdVector *sel)
{
  if(v->size != sel->size) return ;
  for(int i=0;i<v->size;i++)
    {
      if(sel->value[i] != 0)
		if(sel->value[i] != v->value[i])
	  		return ;   
    }
  for(int i=0;i<v->size;i++)
    {
      if(sel->value[i] == 0)
		fprintf(pf, "%f ", v->value[i]);
    }
  fprintf(pf, "%f", v->key);
  fprintf(pf, "\n");
}

void CKdtreeExport::Store(FILE *pf, KdVectorList *l, KdVector *sel)
{
  KdVectorItemList *itemlist = l->head;
  while(itemlist != NULL)
    {
      Store(pf, itemlist->v, sel);
      itemlist = itemlist->next;
    }
}

void CKdtreeExport::setLimits(double minx, double miny, double maxx, double maxy)
{
  _limits->value[0] = minx;
  _limits->value[1] = maxx;
  _limits->value[2] = miny;
  _limits->value[3] = maxy;
}

void CKdtreeExport::setPrecision(double x, double y)
{
  _prec_x = x;
  _prec_y = y;
}

void CKdtreeExport::setKdtree(CHedger *tree)
{
  _smart = tree;
}

void CKdtreeExport::Interpol(double act)
{
  Build(EXPORT_INTERPOL, act);
}

void CKdtreeExport::Raw(double act)
{
  Build(EXPORT_RAW, act);
}



void CKdtreeExport::Interpol(char *file, double act)
{
  FILE *pf;
  int i, j;
  pf = fopen(file, "w");
  if(pf == NULL) return ;
	
  Interpol(act);
 

  for(i=0;i<indicex;i++)
    {
      for(j=0;j<indicey;j++)
	{
	  fprintf(pf, "%d %d %4.4f\n", i, j, z[i][j]);
	}
    }

  fclose(pf);


}

void CKdtreeExport::Raw(char *file, double act)
{
  FILE *pf;
  int i, j;
  pf = fopen(file, "w");
  if(pf == NULL)
    {
      printf("impossible d'ouvrir le fichier\n");
      return ;
    }
	
  Raw(act);

  for(i=0;i<indicex;i++)
    {
      for(j=0;j<indicey;j++)
	{
	  fprintf(pf, "%d %d %4.4f\n", i, j, z[i][j]);
	}
    }

//  fprintf(pf, "\n");
  fclose(pf);


}

void CKdtreeExport::ScaleZ(double new_prec_z)
{
  for(int i=0;i<indicex;i++)
    {
      for(int j=0;j<indicey;j++)
	{
	  if(z[i][j] != 0)
	    {
	      double dVal = z_backup[i][j] * new_prec_z;
	      
	      dVal = dVal / _max_z;
	      float fVal = (float)dVal;
	      z[i][j] = fVal;
	    }
	}
    }
}

double CKdtreeExport::KdInterpol(double dist, double angle, double act)
{
  if(_smart->S->GetTotalSize() == 0) return 0;

  KdVector *xp = CKdtree::NewKdVector(3);
  xp->value[0] = dist;
  xp->value[1] = angle;
  xp->value[2] = act;
  double mindist = 0;
  int status = 0;
  KdVectorList *list = CKdtree::NewKdVectorList();
  
  double val = _smart->Prediction(xp, list, &mindist, &status);

  _TypeOfPrediction[status]++;
 
  _smart->S->FreeMemWithoutVector(list->head);
 // _smart->S->FreeMem(list);
  delete list;
  _smart->S->FreeMem(xp);
  return val;
}


double CKdtreeExport::KdRaw(double dist, double angle, double act)
{
  if(_smart->S->GetTotalSize() == 0) return 0;
  KdVector *xp = CKdtree::NewKdVector(3);
  xp->value[0] = dist;
  xp->value[1] = angle;
  xp->value[2] = act;
  KdVector *weight = CKdtree::NewKdVector(3);
  weight->value[0] = 1;
  weight->value[1] = 1;
  weight->value[2] = 200;
  double qvalue = 0;
  double distance = _smart->S->KdFindClosestInTree(xp, qvalue, weight);
  if(distance > _prec_y && distance > _prec_x)
    {
      qvalue = 0;
    }
  _smart->S->FreeMem(xp);
  return qvalue;
}

void CKdtreeExport::Allocate(void)
{
  int i;
  indicex = _limits->value[1] - _limits->value[0];
  indicey = _limits->value[3] - _limits->value[2];
  indicey /= _prec_y;
  indicex /= _prec_x;
  // size of matrix z
  indicez = indicex * indicey;
  x = new float[indicex];
  y = new float[indicey];
  z = new float * [indicex];
  for(i=0;i<indicex;i++)
    z[i] = new float[indicey];
  z_backup = new float * [indicex];
  for(i=0;i<indicex;i++)
    z_backup[i] = new float[indicey];
}

void CKdtreeExport::Build(TypeOfExport toe, double act)
{
	int i, j;
	double xx;
	double yy;

	for(i=0;i<PRED_NULL;i++)
	{
		_TypeOfPrediction[i] = 0;
	}
	_mem_allow = 1;


	double val_x = _limits->value[0];// limit min
	for(i=0;i<indicex;i++)
	{
		x[i] = val_x;
		val_x += _prec_x;
	}
	double val_y = _limits->value[2]; // limit min
	for(i=0;i<indicey;i++)
	{
		y[i] = val_y;
		val_y += _prec_y;
	}
	// enter z value
	for(i=0;i<indicex;i++)
		for(j=0;j<indicey;j++)
			z[i][j] = 0;

	_max_z = 0;
	if(toe == EXPORT_INTERPOL)
	{
		for(i=0;i<indicex;i++)
		{
			xx = (i * _prec_x + _limits->value[0]);
			for(j=0;j<indicey;j++)
			{
				yy = (j * _prec_y + _limits->value[2]);
				z[i][j] = KdInterpol(xx, yy, act);
				if(z[i][j] > _max_z)
				{
					_max_z = z[i][j];
				}
			}
		}
	}

	if(toe == EXPORT_RAW)
	{
		KdNode *head = _smart->S->_head;
		RawLoop(head, act);
	}
	for(i=0;i<indicex;i++)
	{
		for(j=0;j<indicey;j++)
		{
			z_backup[i][j] = z[i][j];
		}
	}
	ScaleZ(1);
}


void CKdtreeExport::RawLoop(KdNode *n, double action)
{
  KdVector *v = NULL;
  KdVectorItemList *il = NULL;
  double distance = 0;
  double angle = 0;
  if(n->leaf)
    {
      il = n->bag->list->head;
      while(il!=NULL)
	{
	  v = il->v;
	  distance = v->value[0];
	  angle = v->value[1];
	  if(v->value[v->size -1] == action || action == -1)
	    {
	      if(distance > _limits->value[0]
		 && distance < _limits->value[1]
		 && angle > _limits->value[2]
		 && angle < _limits->value[3])
		{
		  int x = 
		    (distance-_limits->value[0])/_prec_x;
		  int y = 
		(angle-_limits->value[2])/_prec_y;
		  //	      printf("dist %d angle %d %d %d\n", distance, angle, x, y);
		  z[x][y] = v->key;
		  if(z[x][y] > _max_z)
		    _max_z = z[x][y];
		}
	    }
	  // work on v
	  il = il->next;
	}
    }
  else
    {
      RawLoop(n->ChildLeft, action);
      RawLoop(n->ChildRight, action);
    }
}

void CKdtreeExport::Reset(void)
{
  if(_mem_allow == 0) return ;
  _mem_allow = 0;
  if(x) delete x;
  if(y) delete y;
  if(z)
    {
      for(int i=0;i<indicex;i++)
	delete z[i];
      delete z;
    }
  if(z_backup)
    {
      for(int i=0;i<indicex;i++)
	delete z_backup[i];
      delete z_backup;
    }
  indicex = indicey = indicez = 0;
  if(_buf_image) delete _buf_image;
  if(_buf_image_status) delete _buf_image_status;
}


void CKdtreeExport::initValuesImage(void)
{
  double val_x = _limits->value[0];// limit min
  int i;
  for(i=0;i<indicex;i++)
    {
      x[i] = val_x;
      val_x += _prec_x;
    }
  double val_y = _limits->value[2]; // limit min
  for(i=0;i<indicey;i++)
    {
      y[i] = val_y;
      val_y += _prec_y;
    }
  _buf_image = new unsigned char[indicez * 3];
  _buf_image_status = new unsigned char[indicez * 3];
}

void CKdtreeExport::InterpolImage(char *file, char *filestatus, double act)
{
  KdVector *xp = CKdtree::NewKdVector(3);
  int i, j;
  unsigned char r, v, b;


  for(i=0;i<PRED_NULL;i++)
    {
      _TypeOfPrediction[i] = 0;
    }

  memset(_buf_image, 255, indicez*3);
  memset(_buf_image_status, 255, indicez*3);

  for(i=0;i<indicex;i++)
    {
      for(j=0;j<indicey;j++)
	{
	  int indice = ((indicex-i)*indicey+j)*3;
	  r = v = b = 255;
	  xp->value[0] = x[i];
	  xp->value[1] = y[j];
	  xp->value[2] = act;
	  double mindist = 0;
	  int status = 0;
	  KdVectorList *list = CKdtree::NewKdVectorList();
	  
	  _smart->Prediction(xp, list, &mindist, &status);
	  _TypeOfPrediction[status]++;

	  r = 0;
	  v = 0;
	  b = 255;
	  
	  switch(status)
	    {
	    case PRED_NORMAL:
	      r = 255;
	      v = 0;
	      b = 0;
	      break;
	    case PRED_EXACT:
	      r = 255;
	      v = 255;
	      b = 0;
	      break;
	    case PRED_NOT_ENOUGH:
	      r = 255;
	      v = 255;
	      b = 255;
	      break;
	    case PRED_IVH:
	      r = 0;
	      v = 255;
	      b = 0;
	      break;
	    case PRED_BOUNDS:
	      r = 0;
	      v = 255;
	      b = 255;
	      break;
	    case PRED_EXT:
	      r = 255;
	      v = 0;
	      b = 255;
	      break;
	    case PRED_NULL:
	      r = 0;
	      v = 0;
	      b = 0;
	      break;
	    }
	  _buf_image_status[indice] = r;
	  _buf_image_status[indice+1] = v;
	  _buf_image_status[indice+2] = b; 

	  if(status ==  PRED_NORMAL
	     || status == PRED_EXACT)
	    {
	      r = 0;
	      v = 0;
	      b = 255;

	      switch(_smart->BestAction(xp))
		{
		case HEDGER_ACTION_NOTHING:
		  r = 255;
		  v = 0;
		  b = 0;
		  break;
		case HEDGER_ACTION_FORWARD:
		  r = 0;
		  v = 165;
		  b = 255;
		  break;
		case HEDGER_ACTION_BACKWARD:
		  r = 0;
		  v = 255;
		  b = 0;
		  break;
		case HEDGER_ACTION_TURNONLEFT:
		  r = 255;
		  v = 255;
		  b = 0;
		  break;
		case HEDGER_ACTION_TURNONRIGHT:
		  r = 0;
		  v = 0;
		  b = 0;
		  break;

		}	 
	      _buf_image[indice] = r;
	      _buf_image[indice+1] = v;
	      _buf_image[indice+2] = b; 
	    }


	  _smart->S->FreeMemWithoutVector(list->head);
	  delete list;
	}
    }

  
  CImage image;
  image.set_size(indicey, indicex);
  image.set_buffer(_buf_image, CIT_PPM);
  image.save(file);

  CImage image2;
  image2.set_size(indicey, indicex);
  image2.set_buffer(_buf_image_status, CIT_PPM);
  image2.save(filestatus);

  _smart->S->FreeMem(xp);
}

