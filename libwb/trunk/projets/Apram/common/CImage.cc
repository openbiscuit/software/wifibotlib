#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include <jpeglib.h>
#include <jerror.h>
#include <memory.h>
#include "Singleton.h"
#include "CImage.h"

FILE *CImage::fi = NULL;
JOCTET *CImage::_jpeg_buf = NULL;
unsigned int CImage::_index_jpeg_buf = 0;

CImage::CImage(void)
{
  _filename = NULL;
  _buffer = NULL;
  _jpeg_buf  = NULL;
  _width = _height = 0;
  _quality = 50;
}

CImage::~CImage(void)
{
 // delete _jpeg_buf;
  //delete _buffer;
}

void CImage::del_buffer(void)
{
 // if(_jpeg_buf) delete _jpeg_buf;
//  if(_buffer) delete _buffer;
}

void CImage::set_size(int w, int h)
{
  _width = w;
  _height = h;
}

void CImage::set_buffer(unsigned char *buffer, CImage_Type type)
{
 // if(_buffer != NULL) delete _buffer;
  _jpeg_buf = NULL;
  _buffer = NULL;
  if(type == CIT_JPG)
    _jpeg_buf = buffer;
  else
    _buffer = buffer;
  
  _type = type;
}

void CImage::set_quality(int q)
{
  _quality = q;
}

bool CImage::convert_to(CImage_Type type)
{
  bool bRet = true;
  if(_type == CIT_PPM && type == CIT_JPG)
    {
      if(_jpeg_buf)
	delete _jpeg_buf;
      bRet = ppm_to_jpg();
      return true;
    }
 if(_type == CIT_PPM && type == CIT_BMP)
    {   
      bRet = bgr_to_rgb();
      _type = CIT_BMP;
      return true;
    }
 if(_type == CIT_BMP && type == CIT_PPM)
   {
     bRet = bgr_to_rgb();
     _type = CIT_PPM;
     return true;
   }

 if(_type == CIT_JPG && type == CIT_PPM)
   {
     bRet = jpg_to_ppm();
   }

 if(_type == CIT_JPG && type == CIT_BMP)
   {
     bRet = jpg_to_ppm();
     bRet += bgr_to_rgb();
      _type = CIT_BMP;
   }
   return bRet;
}

unsigned char *CImage::get_buffer(void)
{
  if(_type == CIT_JPG)
    return _jpeg_buf;
  return _buffer;
}

void CImage::set_length(unsigned int l)
{
  _index_jpeg_buf = l;
}

unsigned int CImage::get_length(void)
{
  if(_type == CIT_JPG)
    return _index_jpeg_buf;
  return -1;     
}


bool CImage::open(char *filename, CImage_Type type)
{
  _filename = filename;
  _type = type;
//  if(_buffer != NULL) delete _buffer;
  switch(type)
    {
    case CIT_JPG:
      return open_jpg();
    case CIT_BMP:
    	return open_bmp();
    case CIT_PPM:
      return open_ppm();
    }
  return false;
}

bool CImage::save(char *filename)
{
  _filename = filename;
  switch(_type)
    {
    case CIT_JPG:
      return save_jpg();
    case CIT_PPM:
      return save_ppm();
    case CIT_BMP:
      return save_bmp();
    }
  return false;
}

bool CImage::save_ppm(void)
{
  FILE *file = fopen (_filename, "wb");
  if(file == NULL)
    return false;
  fprintf (file, "P6 %d %d 255\n", _width, _height);
  fwrite (_buffer, sizeof(unsigned char), _width * _height * 3, file);
  fclose (file);
  return true;
}

bool CImage::open_ppm(void)
{
  FILE *file = fopen (_filename, "rb");
  if(file == NULL)
    return false;
  fscanf (file, "P6 %d %d 255\n", &_width, &_height);
 
  if(_buffer == NULL) _buffer = new unsigned char[_width * _height * 3];
  fread (_buffer, sizeof(unsigned char), _width * _height * 3, file);
  fclose (file);
  return true;
}

bool CImage::save_bmp(void)
{

  CImage_BMP_FileHeader  fileheader;
  CImage_BMP_ImageHeader imageheader;

  fileheader.size = 14;
  fileheader.type = 0x4D42;
  fileheader.r1 = 0;
  fileheader.r2=0;
  fileheader.data_offset = 14+40;
  imageheader.header_size = 40;
  imageheader.r1 = 1;
  imageheader.depth = 24;
  imageheader.r2 = 0;
  imageheader.size = _width * _height * 3;
  imageheader.r3 = 0;
  imageheader.r4 = 0;
  imageheader.r5 = 0;
  imageheader.r6 = 0;
  imageheader.width = _width;
  imageheader.height = _height;
  
  FILE *fichier = fopen(_filename,"wb");
  if(fichier == NULL) return false;

  fwrite( &fileheader.type,  sizeof(fileheader.type),  1, fichier);
  fwrite( &fileheader.size,  sizeof(fileheader.size),  1, fichier);
  fwrite( &fileheader.r1,  sizeof(fileheader.r1),  1, fichier);
  fwrite( &fileheader.r2,  sizeof(fileheader.r2),  1, fichier);
  fwrite( &fileheader.data_offset,  sizeof(fileheader.data_offset),  1, fichier);
  fwrite( &imageheader.header_size, sizeof(imageheader.header_size), 1, fichier);
  fwrite( &imageheader.width, sizeof(imageheader.width), 1, fichier);
  fwrite( &imageheader.height, sizeof(imageheader.height), 1, fichier);
  fwrite( &imageheader.r1, sizeof(imageheader.r1), 1, fichier);
  fwrite( &imageheader.depth, sizeof(imageheader.depth), 1, fichier);
  fwrite( &imageheader.r2, sizeof(imageheader.r2), 1, fichier);
  fwrite( &imageheader.size, sizeof(imageheader.size), 1, fichier);
  fwrite( &imageheader.r3, sizeof(imageheader.r3), 1, fichier);
  fwrite( &imageheader.r4, sizeof(imageheader.r4), 1, fichier);
  fwrite( &imageheader.r5, sizeof(imageheader.r5), 1, fichier);
  fwrite( &imageheader.r6, sizeof(imageheader.r6), 1, fichier);

  unsigned int i,j,k;
  for(j=_height-1;j>=0;j--)
    for(i=0;i<_width;i++)
      for(k=0;k<3;k++)
	{
	  fwrite(&_buffer[(i+j*_width)*3+k], sizeof(unsigned char), 1, fichier);
	}

  fclose(fichier);

  return true;
}

bool CImage::open_bmp(void)
{
  CImage_BMP_FileHeader  fileheader;
  CImage_BMP_ImageHeader imageheader;

  fileheader.size = 14;
  fileheader.type = 0x4D42;
  fileheader.r1 = 0;
  fileheader.r2=0;
  fileheader.data_offset = 14+40;
  imageheader.header_size = 40;
  imageheader.r1 = 1;
  imageheader.depth = 24;
  imageheader.r2 = 0;
  imageheader.size = _width * _height * 3;
  imageheader.r3 = 0;
  imageheader.r4 = 0;
  imageheader.r5 = 0;
  imageheader.r6 = 0;
  imageheader.width = _width;
  imageheader.height = _height;
    
  

  FILE *fichier = fopen(_filename,"rb");
  if(fichier == NULL) return false;
    //fwrite(&fileheader, 1, 14, fichier);
 
//    fwrite(&imageheader, sizeof( pejaxx_BmpImageHeader), 1, fichier);

  fread( &fileheader.type,  sizeof(fileheader.type),  1, fichier);
  fread( &fileheader.size,  sizeof(fileheader.size),  1, fichier);
  fread( &fileheader.r1,  sizeof(fileheader.r1),  1, fichier);
  fread( &fileheader.r2,  sizeof(fileheader.r2),  1, fichier);
  fread( &fileheader.data_offset,  sizeof(fileheader.data_offset),  1, fichier);
  fread( &imageheader.header_size, sizeof(imageheader.header_size), 1, fichier);
  fread( &imageheader.width, sizeof(imageheader.width), 1, fichier);
  fread( &imageheader.height, sizeof(imageheader.height), 1, fichier);
  fread( &imageheader.r1, sizeof(imageheader.r1), 1, fichier);
  fread( &imageheader.depth, sizeof(imageheader.depth), 1, fichier);
  fread( &imageheader.r2, sizeof(imageheader.r2), 1, fichier);
  fread( &imageheader.size, sizeof(imageheader.size), 1, fichier);
  fread( &imageheader.r3, sizeof(imageheader.r3), 1, fichier);
  fread( &imageheader.r4, sizeof(imageheader.r4), 1, fichier);
  fread( &imageheader.r5, sizeof(imageheader.r5), 1, fichier);
  fread( &imageheader.r6, sizeof(imageheader.r6), 1, fichier);
  _width = imageheader.width;
  _height =  imageheader.height;
  _buffer = new unsigned char[_width * _height * 3];
  unsigned int i,j,k;
  for(j=_height-1;j>=0;j--)
    for(i=0;i<_width;i++)
      for(k=0;k<3;k++)
	{
	  fread(&_buffer[(i+j*_width)*3+k], sizeof(unsigned char), 1, fichier);
	}
//  fwrite(_buffer, sizeof(char), _height*_width*3, fichier);
  fclose(fichier);
  return true;
}

/*
 * Initialise source --- called by jpeg_read_header
 * before any data is actually read
 */
void CImage::init_source(j_decompress_ptr cinfo)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src; 
  /* We reset the empty-input-file flag for each image,
   * but we don't clear the input buffer.
   * This is correct behavior for reading a series of images from one source.
   */
  src->start_of_file = TRUE;
}




boolean CImage::fill_input_buffer(j_decompress_ptr cinfo)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src;
  src->pub.next_input_byte = src->buffer;
  src->pub.bytes_in_buffer = src->nb_bytes;
  src->start_of_file = FALSE;

  return TRUE;
}



void CImage::skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src;
  if(num_bytes > 0)
    {
      src->pub.next_input_byte += (size_t) num_bytes;
      src->pub.bytes_in_buffer -= (size_t) num_bytes;
    }
}


void CImage::term_source(j_decompress_ptr cinfo)
{

}


void CImage::jpeg_tab_src(j_decompress_ptr cinfo, JOCTET *buffer, int nbytes)
{
  static tab_src_ptr src;
  if(cinfo->src == NULL)
    {
      cinfo->src = (struct jpeg_source_mgr *)
	(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
				    sizeof(tab_source_mgr));
      src = (tab_src_ptr) cinfo->src;
    }

  src = (tab_src_ptr) cinfo->src;
  src->pub.init_source = CImage::init_source;
  src->pub.fill_input_buffer = CImage::fill_input_buffer;
  src->pub.skip_input_data = CImage::skip_input_data;
  src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src->pub.term_source = CImage::term_source;
  src->nb_bytes = nbytes;
  src->buffer = buffer;
  src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
  src->pub.next_input_byte = NULL; /* until buffer loaded */

}


bool CImage::jpg_to_ppm(void)
{
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
  unsigned char *ligne;
  unsigned char *image;
  
  try 
    {
      cinfo.err = jpeg_std_error(&jerr);
      jpeg_create_decompress(&cinfo);
      
      jpeg_tab_src(&cinfo, _jpeg_buf, _index_jpeg_buf);
      
      if(not jpeg_read_header(&cinfo, TRUE))
	return false;
 
      jpeg_start_decompress(&cinfo);
    }
  catch(int error)
    {
      return false;
    }
  _height = cinfo.output_height;
  _width = cinfo.output_width;
  
  image = (unsigned char *)malloc(_height*_width*3*sizeof(unsigned char));
  ligne = image;

  /* Tant qu'il y a des scanlines, on les place dans le buffer */
  while(cinfo.output_scanline < (_height))
    {
      ligne = image + 3 * (_width) * cinfo.output_scanline;
      jpeg_read_scanlines(&cinfo, &ligne, 1);
    }
  
  /* On termine proprement */
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  
  _buffer = image;
  _type = CIT_PPM;
  return true;
}

bool CImage::ppm_to_jpg(void)
{

  struct jpeg_destination_mgr mgr;
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  JSAMPROW                     jrows[1], jrow;

  memset(&cinfo, 0, sizeof(cinfo));
  memset(&jerr, 0, sizeof(jerr));
  memset(&mgr, 0, sizeof(mgr));
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);

  mgr.init_destination = CImage::file_init_destination;
  mgr.empty_output_buffer = CImage::file_empty_output_buffer;
  mgr.term_destination = CImage::file_term_destination;
  cinfo.dest = &mgr;

  cinfo.image_width = _width;
  cinfo.image_height = _height;
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB; 

  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, _quality, TRUE);

  jpeg_start_compress(&cinfo, FALSE);
  
  bgr_to_rgb();
//  return ;

  unsigned int nRowLen = (((_width * 24) + 31) / 32) * 4;
  while(cinfo.next_scanline < cinfo.image_height)
    {
      unsigned int i, j, tmp;
   
      jrow = &_buffer[(cinfo.image_height - cinfo.next_scanline - 1) * nRowLen];
 
      for(i = 0; i < cinfo.image_width; i++)
	{
	  j = i * 3;
	  tmp = jrow[j];
	  jrow[j] = jrow[j + 2];
	  jrow[j + 2] = tmp;
	}
      jrows[0] = jrow;
      jpeg_write_scanlines(&cinfo, jrows, 1);
    } 
  
  jpeg_finish_compress(&cinfo);

  jpeg_destroy_compress(&cinfo);

  _type = CIT_JPG;
  return true;
}

bool CImage::save_jpg(void)
{
  FILE *fp;
  fp = fopen(_filename, "w");
  if(fp == NULL)
    {
      return false;
    }
  if(_jpeg_buf == NULL)
    fwrite(_buffer, sizeof(char), _index_jpeg_buf, fp);
  else
    fwrite(_jpeg_buf, sizeof(JOCTET), _index_jpeg_buf, fp);
  fclose(fp);
  return true;
}

bool CImage::open_jpg(void)
{
  FILE * fp;
		
  fp = fopen(_filename, "rb");
  if (fp == NULL)
    return false;
  fseek (fp , 0 , SEEK_END);
  _index_jpeg_buf = ftell (fp);
  rewind (fp);
  _jpeg_buf = new JOCTET[_index_jpeg_buf];
  fread (_jpeg_buf, sizeof(char), _index_jpeg_buf, fp);
  fclose(fp);
  return true;
}


void CImage::file_init_destination(j_compress_ptr cinfo) 
{ 
  struct jpeg_destination_mgr*dmgr = 
    (struct jpeg_destination_mgr*)(cinfo->dest);

 
  _jpeg_buf = new JOCTET[OUTBUFFER_SIZE];
  if(!_jpeg_buf) 
    {
    perror("malloc");
    printf("Out of memory!\n");
    exit(1);
  }
  
  dmgr->next_output_byte = _jpeg_buf;
  dmgr->free_in_buffer = OUTBUFFER_SIZE;
}


boolean CImage::file_empty_output_buffer(j_compress_ptr cinfo)
{ 
  struct jpeg_destination_mgr*dmgr = 
      (struct jpeg_destination_mgr*)(cinfo->dest);

  dmgr->next_output_byte = _jpeg_buf;
  dmgr->free_in_buffer = 0;

  return 1;
}

void CImage::file_term_destination(j_compress_ptr cinfo) 
{
  struct jpeg_destination_mgr*dmgr = 
      (struct jpeg_destination_mgr*)(cinfo->dest);
  _index_jpeg_buf =  OUTBUFFER_SIZE-dmgr->free_in_buffer;
  dmgr->free_in_buffer = 0;
}

bool CImage::bgr_to_rgb(void)
{
  unsigned char tempRGB;
  for (unsigned int i = 0; i < (_width*_height*3); i += 3)
    {
      tempRGB = _buffer[i];
      _buffer[i] = _buffer[i + 2];
      _buffer[i + 2] = tempRGB;
    }
  return true;
}


