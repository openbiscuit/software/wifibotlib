#include <memory.h>
#include <iostream>
#include "List.h"


CList::CList(void)
{
	head = end = NULL;
	length = 0;
	m_lock = false;
}

void	CList::Lock(void)
{
	m_lock = !m_lock;
}

CList::~CList(void)
{
	Free();	// lors de l'appel du destructeur on vide la mémoire allouer
}

void CList::Append(void *item)
{
	tsbList *temp;

	if(head == NULL)
	{ // la liste est vide ajout d'un élément
		head = new tsbList;
		head->data = item;
		head->next = NULL;
		head->prev = NULL;
		end = head;
	}
	else
	{ // ajout à la suite
		temp = new tsbList;
		temp->data = item;
		temp->next = NULL;
		temp->prev = end;
		end->next = temp;
		end = temp;
		end->next = NULL;
	}
	length++;
}

void CList::Insert(void *item, int pos)
{
	tsbList *temp;
	tsbList *newValue;
	int i = 0;
	if(pos <= 0 || head == NULL)
	{
		Prepend(item);
		return ;
	}
	if(pos >= GetLength())
	{
		Append(item);
		return ;
	}

	if(head == NULL) return ;
	temp = head;
	do
	{
		if(i == pos)
		{
			newValue = new tsbList;
			newValue->data = item;
			newValue->prev = temp;
			newValue->next = temp->next;
			temp->next = newValue;
			length++;
			break;
		}
		temp = temp->next;
		i++;
	} while(temp != NULL);

}

void CList::Prepend(void *item)
{
	tsbList *temp;

	if(head == NULL)
	{ // la liste est vide ajout d'un élément
		head = new tsbList;
		head->data = item;
		head->next = NULL;
		head->prev = NULL;
		end = head;
	}
	else
	{ // sinon on ajoute au début de la liste
		temp = new tsbList;
		temp->data = item;
		temp->next = head;
		temp->prev = NULL;
		head->prev = temp;
		head = temp;
	}
	length++;
}

void CList::RemoveHead(void)
{
  tsbList *temp = head->next;
  delete head;
  head = temp;
  head->prev = NULL;
  length--;
}

void CList::RemoveEnd(void)
{
  tsbList *temp = end->prev;
  delete end;
  end = temp;
  end->next = NULL;
  length--;
}

void *CList::GetHead(void)
{
	return head->data;
}

void *CList::GetEnd(void)
{
	return end->data;
}

void CList::Remove(int indice)
{
//	Remove(Get(indice));
	tsbList *temp;
	int ind = 0;
	if(head == NULL) return ;
	temp = head;
	do
	{
		if(ind == indice)
		{
			if(temp == head)
			{
				RemoveHead();
				return ;
			}
			if(temp == end)
			{
				RemoveEnd();
				return ;
			}
			temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
			delete temp;
			length--;
			return ;
		}
		temp = temp->next;
		ind++;
	} while(temp != NULL);
}




void *CList::Get(int indice)
{
	tsbList *temp;
	int ind = 0;
	if(head == NULL) return NULL;
	temp = head;
	do
	{
		if(ind == indice)
			return temp->data;
		temp = temp->next;
		ind++;
	} while(temp != NULL);
	return NULL;
}

void CList::Remove(void *item)
{
	tsbList *temp;
	temp = Find(item);
	if(temp == NULL) return ;
	if(temp == head)
	{
		RemoveHead();
		return ;
	}
	if(temp == end)
	{
		RemoveEnd();
		return ;
	}
	temp->prev->next = temp->next;
	temp->next->prev = temp->prev;
	delete temp;
	length--;
}

int CList::GetIndice(void *item)
{
	tsbList *temp;
	int ind = 0;
	if(head == NULL) return -1;
	temp = head;
	do
	{
		if(memcmp(temp->data, item, sizeof(item)) == 0)
		{
			return ind;
		}
		temp = temp->next;
		ind++;
	} while(temp != NULL);
	return -1;
}

tsbList *CList::Find(void *item)
{
	tsbList *temp;

	if(head == NULL) return NULL;
	temp = head;
	do
	{
		if(memcmp(temp->data, item, sizeof(item)) == 0)
		{
			return temp;
		}
		temp = temp->next;
	} while(temp != NULL);
	return NULL;
}

void	CList::Free(void)
{
	tsbList *temp;
	if(head == NULL) return ;
	do
	{
		temp = head->next;
		delete head;
		head = temp;
	} while(head != NULL);
	length = 0;
}

int CList::GetLength()
{
	if(m_lock) return -1;
	return length;
}
