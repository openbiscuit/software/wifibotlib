#include <iostream>
#include "Singleton.h"
#include "List.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CPeng.h"

using namespace std;

CPeng::CPeng(CHedger *smart)
{
  _smart = smart;
  _smart->_peng = this;
 // _gamma = CPENG_DEFAULT_GAMMA;
  _lambda =  CPENG_DEFAULT_LAMBDA;
  //_alpha =  CPENG_DEFAULT_ALPHA;
}

CPeng::~CPeng(void)
{
  reset();
}

/*
  r = la récompense
  le meilleur qsa de l'état d'arriver
  le qsa pris par l'ordi
  le meilleur qsa que l'on pouvait prendre
*/
void CPeng::compute(KdVector *xp, double qvalue, double rewards, KdVector *v, double weight)
{
//  CKdtree *tree = CKdtree::getInstance();
  // ALAIN : pourrait peut-être le récupérer lors de l'appel de la fonction..
  double Vxp = bestActionFromState(xp);
  double Vx = bestActionFromStateAction(v);
  int i;
  CPeng_Trace *trace = NULL;


  // recherche si le vecteur est déjà dans la liste
  for(i=0;i<_Tr.GetLength();i++)
    {
      CPeng_Trace *temp = (CPeng_Trace *)_Tr.Get(i);
      if(temp->v == v)
	{
	  trace = temp;
	  break;
	}
    }

  // création de la trace
  if(trace == NULL)
    {
      trace = new CPeng_Trace;
      trace->v = v;
      trace->tr = 0.0;
    }

  _eMax = rewards + _smart->_gamma*Vxp - Vx;
  // Alain : pas besoin car deja fait dans CHedger (Q-Learning)
  //_e = rewards + _smart->_gamma*Vxp - qvalue;
  

  for(i=0;i<_Tr.GetLength();i++)
    {
      CPeng_Trace *temp = (CPeng_Trace *)_Tr.Get(i);
      temp->tr = _smart->_gamma *_lambda * temp->tr;
      temp->v->key = temp->v->key + _smart->_alpha * temp->tr * _eMax;
    }

  // Alain : pas besoin car deja fait dans CHedger (Q-Learning)
  // v->key = v->key + _smart->_alpha*_eMax;

  trace->tr += weight;

  // ajout du @vecteur@ à la liste d'éligibilité
  _Tr.Add(trace);
}

void CPeng::reset(void)
{
  _Tr.Free();
}

double CPeng::bestActionFromState(KdVector *etat)
{
  KdVectorList *l1 = CKdtree::NewKdVectorList();
  KdVector *v = CKdtree::NewKdVector( etat->size+1 );
  double maxq = 0;
  double mindist = 0.0;
  int status = 0;
  double m = 0.0;
  int i = 0;
  for(i=0;i< etat->size ;i++)
    {
      v->value[i] = etat->value[i];
    }
  v->value[v->size -1] = HEDGER_ACTION_NOTHING;
  maxq = _smart->Prediction(v, l1, &mindist, &status);

  CKdtree::FreeMemWithoutVector(l1->head);
  l1->head = NULL;
  l1->size=0;
  l1->tail = NULL;

  for(i=0;i<=HEDGER_ACTION_TURNONRIGHT;i++)
    {
      v->value[v->size -1] = i;
      if((m=_smart->Prediction(v, l1, &mindist, &status))>maxq)
	{
	  maxq = m;
	}
      CKdtree::FreeMemWithoutVector(l1->head);
      l1->head = NULL;
      l1->size=0;
      l1->tail = NULL;
    }
  delete l1;
  CKdtree::FreeMem(v);
  return maxq;
}


double CPeng::bestActionFromStateAction(KdVector *etatAction)
{
  KdVectorList *l1 = CKdtree::NewKdVectorList();
  KdVector *v = CKdtree::NewKdVector( etatAction->size );
  double maxq = 0;
  double mindist = 0.0;
  int status = 0;
  double m = 0.0;
  int i = 0;
  for(i=0;i< etatAction->size-1 ;i++)
    {
      v->value[i] = etatAction->value[i];
    }
  v->value[v->size -1] = HEDGER_ACTION_NOTHING;
  maxq = _smart->Prediction(v, l1, &mindist, &status);

  CKdtree::FreeMemWithoutVector(l1->head);
  l1->head = NULL;
  l1->size=0;
  l1->tail = NULL;

  for(i=1;i<=HEDGER_ACTION_TURNONRIGHT;i++)
    {
      v->value[v->size -1] = i;
      if((m=_smart->Prediction(v, l1, &mindist, &status))>maxq)
	{
	  maxq = m;
	}
      CKdtree::FreeMemWithoutVector(l1->head);
      l1->head = NULL;
      l1->size=0;
      l1->tail = NULL;
    }
  delete l1;
  CKdtree::FreeMem(v);
  return maxq;
}
