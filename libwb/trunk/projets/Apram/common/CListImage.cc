#include <iostream>
#include <memory.h>

#include "CListImage.h"


CListImage::CListImage(void)
{
}


CListImage::~CListImage(void)
{
}

void CListImage::add_picture(unsigned char *img, unsigned int w, unsigned int h)
{
  CListImage_picture *pic = new CListImage_picture;
  pic->data = new unsigned char[w*h*3];
  pic->width = w;
  pic->height = h;
  pic->size = w*h*3;
  memcpy(pic->data, img, w*h*3);
  Add(pic);
}

void CListImage::add_picture(unsigned char *img, unsigned int size)
{
  CListImage_picture *pic = new CListImage_picture;
  pic->data = new unsigned char[size];
  pic->width = 0;
  pic->height = 0;
  pic->size = size;
  memcpy(pic->data, img, size);
  Add(pic);
}
