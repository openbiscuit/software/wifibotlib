#include "CMatrix.h"


CMatrix::CMatrix(int row, int col)
{
  _row = row;
  _col = col;
  _M = gsl_matrix_calloc (_row, _col);
}

CMatrix::CMatrix(CMatrix *A)
{
  CMatrix(A->_row, A->_col);
  gsl_matrix_memcpy(_M, A->_M);
}

CMatrix::~CMatrix(void)
{
  gsl_matrix_free(_M);
}

CMatrix *CMatrix::Transpose(void)
{
  CMatrix *ret = new CMatrix(_col, _row);
  gsl_matrix_transpose_memcpy (ret->_M, _M);
  return ret;
}

CMatrix *CMatrix::PseudoInverse(void)
{
  if(_col > _row)
    return NULL;

  CMatrix *ret = new CMatrix(_col, _row);
  gsl_matrix * V = gsl_matrix_calloc (_col, _col);
  gsl_vector * S = gsl_vector_calloc (_col);
  gsl_vector * work = gsl_vector_calloc (_col);
  gsl_matrix * U = gsl_matrix_calloc (_row, _col);
  gsl_matrix_memcpy(U, _M);
  gsl_linalg_SV_decomp (U, V, S, work);

  gsl_matrix * Sp = gsl_matrix_calloc (_col, _col);
  for(unsigned int i=0;i<S->size;i++)
    {
      if( gsl_vector_get(S, i) > 0.00000001)
	{
	  double val = (double)1.0 / gsl_vector_get(S, i);
	  gsl_matrix_set (Sp, i, i, val);
	}
      else
	gsl_matrix_set (Sp, i, i, 0.0);
    }

  /*printf("U\n");
  gsl_matrix_fprintf( stdout, U, "%g" );
  printf("V\n");
  gsl_matrix_fprintf( stdout, V, "%g" );
  printf("S\n");
  gsl_vector_fprintf( stdout, S, "%g" );
  printf("Sp\n");
  gsl_matrix_fprintf( stdout, Sp, "%g" );
*/
  gsl_matrix *m1 = gsl_matrix_calloc (Sp->size1, U->size1);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans,
		  1.0, Sp, U,
		  0.0, m1);

 // printf("m1\n");
 // gsl_matrix_fprintf( stdout, m1, "%g" );


  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
		  1.0, V, m1,
		  0.0, ret->_M);

 // printf("ret\n");
 // gsl_matrix_fprintf( stdout, ret->_M, "%g" );

  gsl_matrix_free(V);
  gsl_matrix_free(Sp);
  gsl_matrix_free(U);
  gsl_matrix_free(m1);
  gsl_vector_free(S);
  gsl_vector_free(work);

  return ret;
}

gsl_matrix *CMatrix::Dot(gsl_matrix *A, gsl_matrix *B)
{
  if(A->size2 != B->size1)
    return NULL;

//  CMatrix *ret = new CMatrix(A->_row, A->_col);
  gsl_matrix *ret = gsl_matrix_calloc (A->size1, B->size2);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
		  1.0, A, B,
		  0.0, ret);
  return ret;
}

CMatrix *CMatrix::Dot(CMatrix *A)
{
  CMatrix *ret = new CMatrix(_row, A->_col);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
		  1.0, _M, A->_M,
		  0.0, ret->_M);

  return ret;
}

double CMatrix::getMaxDiag(void)
{
  double res = 0.0;
  unsigned int min = _M->size1;
  if(_M->size2 < min)
    min = _M->size2;
  res = Get(0, 0);
 
  for(unsigned int i=0; i<min; i++)
    {
      if(Get(i, i) > res)
	res = Get(i, i);
    }
  return res;
}

bool CMatrix::IsEmpty(void)
{
  for(unsigned int i=0;i<_M->size1;i++)
    {
      for(unsigned int j=0;j<_M->size2;j++)
	{
	  if(Get(i, j) != 0)
	    return false;
	}
    }
  return true;
}

void CMatrix::Set(int r, int c, double value)
{
  gsl_matrix_set (_M, r, c, value);
}

double CMatrix::Get(int r, int c)
{
  return gsl_matrix_get(_M, r, c);
}

void CMatrix::Normalize(int dimension)
{
  double min, max;
  int i;
  min = max = Get(0, dimension);
  for(i=0;i<_row;i++)
    {
      if(min > Get(i, dimension))
	min = Get(i, dimension);
      if(max < Get(i, dimension))
	max = Get(i, dimension);
    }
  double offset = max - min;
  for(i=0;i<_row;i++)
    {
      Set(i, dimension, (Get(i, dimension)-min)*10/offset);
    }
}

void CMatrix::Print(void)
{ 
  for(int i=0;i< _row; i++)  
    {
      for(int j=0;j<_col;j++)
	{
	  printf("%g ", gsl_matrix_get (_M, i, j));
	}
      printf("\n");
    }
}

