/**
 * \file
 *  \brief utilisation d'une socket en c++
 */
#ifndef CTCP_H
#define CTCP_H

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define TCP_MAX_BUFFER_SIZE	250 ///< taille maximum d'un buffer
#define SOCKET int ///< utilisation du mot socket

typedef void * (*PtrFonctClient)(void*); ///< pointeur de fonction pour un serveur

/**
 * informations sur un client qui se connecte à un serveur
 */
typedef struct ClientInfos
{
  int	id; ///< identifiant unique du client
  char *HostName; ///< nom de la machine avec laquelle il se connecte
  char IP[16]; ///< son adresse ip
  SOCKET sock; ///< handle de connexion client
} CTcp_Cli;

/**
 * classe de connexion tcp
 */
class CTcp
{
public:
    CTcp();
    void setSock(SOCKET s);
    ~CTcp();

    /**
       * client : Connect to a tcp server
       *
       * @param host a ip adresss or hostname
       * @param sock socket of the host
       * @return true if success else false
       */
    bool Connect(char *host, int sock);

    /**
       * server : Create a standalone server
       *
       * @param sock socket of the server
       * @param fct pointer of function call when a client request connexion
       * this function receive a ClientInfos
       * @return false if trouble
       */
    bool waiting(int sock,  CThread *task);

    /**
       * stop the current server
       */
    void StopServer(void);

	/**
	 * envoi les données sur la socket
	 * @param data à envoyer
	 * @return true si l'émission est correcte
	 * @return false en cas de problème
	 */
    bool Send(char *data);
    
 	/**
	 * envoi les données sur la socket
	 * @param data à envoyer
	 * @param sz la taille des données
	 * @return true si l'émission est correcte
	 * @return false en cas de problème
	 */
    bool Send(char* data, int sz );
    
 	/**
	 * envoi les données sur la socket
	 * fonctionne comme un printf : Sendf("%s %d", "test", 30);
	 * @param format définition des données
	 * @param sz la taille des données
	 * @return true si l'émission est correcte
	 * @return false en cas de problème
	 */
    bool Sendf(const char *format, ...);

	/**
	 * réception des données brutes
	 * @param data pointeur pour la réception des données
	 * @param max, longueur maximum de données pouvant être utilisées sur ce pointeur
	 * @return le nombre d'octets lus
	 */
    int Rec(char *data, int max);
    
    /**
     * réception des données dans une boucle
     * @param data données à transmettre
     * @param max nombre de données pouvant être reçues
     * @return le nombre d'octets lus
     */
    int LoopReceive(char *data, int max);
    
    /**
     * réception de données dans une boucle en tenant compte d'une longueur de données définie
     * @param data pointeur pour la réception des données
     * @param len longueur maximum de données pouvant être lues
     * @param max nombre d'octets à lire
     */
    void Receive(char *data, int &len, int max=TCP_MAX_BUFFER_SIZE);
    
    /**
     * lecture d'une ligne sur la socket
     * @param data données à transmettre
     * @param max nombre de données pouvant être reçues
     */
    int ReadLine(char *data, int max);
    
    /**
     * stoppe la connexion
     */
    void StopClient(void);
protected:
    SOCKET	m_socket;	///< socket de connexion
    struct sockaddr_in serv_addr; ///< structure d'information sur l'addresse du serveur
    struct hostent *phost;
    bool	Run;
    int SServer;
};

#endif
