/**
   * \file
   * \brief classe permettant de gérer un kdtree
   * \author Nicolas Beaufort
   */

#ifndef CKDTREE_H
#define CKDTREE_H

/**
   * définition d'un KdVector
   */
typedef struct _kdtree_KdVector
{
  int size; ///< taille du KdVector
  double *value; ///< valeurs stockées
  double key; ///< q-value
  double dist; ///< calcul la distance minimum pour un point xp @see CHedger
  double weight; ///< poid du vecteur @see CHedger
} KdVector;

typedef struct _kdtree_VectorItemList KdVectorItemList;

/**
   * définition d'une liste de vecteur (liste chainée)
   */
struct _kdtree_VectorItemList
{
  KdVector *v; ///< vecteur courant
  KdVectorItemList *next; ///< item suivant
  KdVectorItemList *back; ///< item précédant
};

/**
   * définition d'une liste de item list
   */
struct KdVectorList
{
  KdVectorItemList *head; ///< tête de la liste chainée
  KdVectorItemList *tail; ///< queue de la liste chainée
  int size; ///< taille de la liste
};

/**
   * définition d'une feuille
   */
typedef struct KdBag
{
  int sizeofbag; ///< taille de la feuille
  KdVectorList *list; ///< liste de la feuille
} KdBag;

typedef struct _kdtree_Node KdNode;

/**
   * définition d'un noeud de l'arbre
   */
struct _kdtree_Node
{
  int index_dim; ///< index de dimension pour la coupure
  double value; ///< valeur de la coupure
  unsigned char leaf; ///< vaut 1 si c'est une feuille
  KdNode *ChildLeft; ///< noeud enfant gauche
  KdNode *ChildRight; ///< noeud enfant droit
  KdBag *bag; ///< pointeur vers la feuile
};

/**
 * class CKdtree
 */
class CKdtree// : public Singleton<CKdtree>
{
 public:
  /**
     * créer un arbre
     * @param dim nombre de dimension de l'arbre
     * @param limit_of_bag nombre maximum de feuilles par branche
     */
//  CKdtree(int dim, int limit_of_bag);
  CKdtree();
  ~CKdtree();

  // global methods
  void init(void);
  /**
   * affecte les poids des dimensions
   * @param v vecteur de poids doit contenir autant de valeur que de
   * dimensions définies
   */
  void SetWeight(KdVector *v);
  /**
   * retourne le nombre de points de l'arbre
   * @return nombre de points du kdtree
   */
  int GetTotalSize(void);
  /**
   * sauvegarde le kdtree dans un fichier
   * @param file chemin du fichier
   */
  void	SaveTo(char *file);
  /**
   * charge le contenu d'un fichier dans le kdtree
   * @param file chemin du fichier
   */
  void	LoadFrom(char *file);
  
  // kdtree handling
  /**
   * ajoute un vecteur à l'arbre
   * @param v vecteur a ajouté
   * le vecteur doit avoir le même nombre de dimension
   * que l'arbre
   */
  int AddVector(KdVector *v);
  /**
   * ajoute un vecteur à une liste de vecteur
   * @param l la liste à mettre à jour
   * @param v le vecteur à ajouté
   */
  void AddVector(KdVectorList *l, KdVector *v);
  /**
   * ajoute un vecteur à une liste en tenant compte de la distance
   * @param l liste de vecteur à mettre à jour
   * @param v vecteur à mettre à jour
   */
  void AddVectorSortByDist(KdVectorList *l, KdVector *v);
  /**
   * compare deux vecteurs
   * @param v1 vecteur 1
   * @param v2 vecteur 2
   * @return int 1 si les vecteurs sont identiques, 0 sinon
   */
  int CompareVector(KdVector *v1, KdVector *v2); 
  /**
   * retourne la distance entre deux vecteurs pondérés par leurs poids
   * @param v1 vecteur 1
   * @param v2 vecteur 2
   * @param weight poids à considérer
   */
  double GetVectorDistance(KdVector *v1, KdVector *v2, KdVector *weight=NULL);
  /**
   * récupère un certain nombre de vecteur dans l'arbre autour d'une cible
   * @param n noeud de départ de la recherche
   * @param target cible de la recherche
   * @param nb_values nombre de valeurs à rechercher
   * @param l liste de vecteur contenant le résultat de la recherche
   */
  void GrabValues(KdNode *n, KdVector *target, double h, KdVectorList *l);

  /**
   * recherche si un vecteur est dans la liste
   * @param l liste où effectuer la recherche
   * @param v vecteur à rechercher
   * @return 1 si vecteur trouvé sinon 0
   */
   int LookUpVector(KdVectorList *l, KdVector *v);

   /**
    * Recherche le point le plus proche de 'target'.
    * Inspiré par [Moore91].
    *
    * @param target le KdVector dont on cherche le plus proche voisin
    * @param closest retourne le point le plus proche
    *
    * @return la distance du meilleur point trouvé ou maxDist
    * @return qvalue  la valeur q du point le plus proche
    */
   double KdFindClosestInTree( KdVector *target, double &qvalue, KdVector *w);

  // memory allocation
   /**
    * créer un nouveau vecteur
    * @param size taille du nouveau vecteur
    * @return un pointeur de vecteur alloué
    */
  static KdVector *NewKdVector(int size=3);
  /**
   * créer une nouvelle liste de vecteur
   * @return un pointeur sur liste de vecteur
   */
  static KdVectorList *NewKdVectorList(void);

  // print methods
  /**
   * affiche le contenu de l'arbre sur la sortie standard
   */
  void Display(void);
  /**
   * affiche le contenu d'un noeud sur la sortie standard
   * @param n noeud à afficher
   */
  void Display(KdNode *n);
  /**
   * affiche le contenu d'une feuille sur la sortie standard
   * @param b feuille à afficher
   */
  void Display(KdBag *b);
  /**
   * affiche un vecteur sur la sortie standard
   * @param b vecteur à afficher
   */
  void Display(KdVector *v);
  /**
   * affiche une liste sur la sortie standard
   * @param l liste à afficher
   */
  void Display(KdVectorList *l);
  
  // free memory
  /**
   * libère la mémoire d'une liste de vecteur sans désalouer les vecteurs
   * @param il liste de vecteur à traiter
   */
  static void FreeMemWithoutVector(KdVectorItemList *il);
  /**
   * libère la mémoire d'un vecteur
   * @param v vecteur à désalouer
   */
  static void FreeMem(KdVector *v);
  void FreeMem(KdVectorList *l);
  /**
     * libère la mémoire occupé par le kdtree
     */
  void FreeMem(void);

      // free memory
    /**
     * libère la mémoire d'un noeud
     * @param n noeud à libérer
     */
    void FreeMem(KdNode *n);

    /**
     * libère la mémoire d'une liste de vecteur et libère
     * les vecteurs associés
     * @param il liste de vecteur
     */
    void FreeMem(KdVectorItemList *il);

  double _qmin; ///< q-value minimum de l'arbre
  double _qmax; ///< q-value maximum de l'arbre
  double _rmin; ///< récompense minimum de l'arbre
  double _rmax; ///< récompense maximum de l'arbre
  KdVector *_weight; ///< poids des dimensions de l'arbre
  KdNode *_head; ///< tête de l'abre (premier noeud)
  unsigned char _verbosity; ///< pour activer le mode verbeux
 protected:

  KdVector *_maxValues; ///< contient les valeurs max de chaque dimension
  
  //private :
 public:
 	int _dim; ///< nombre de dimension 
    int _limit_of_bag; ///< limit de points par feuille

  // new memory allocation
    /*
     * créer un nouveau noeud
     * @return un pointeur de noeud
     */
    KdNode *NewKdNode(void);
    /*
     * créer une liste de vecteur
     * @param v vecteur à renseigner
     * @param next pointeur sur liste suivant
     * @param back pointeur sur lsite précédent
     * @return une nouvelle liste de vecteur
     */
  KdVectorItemList *NewKdVectorItemList(KdVector *v, KdVectorItemList *next, KdVectorItemList *back);
  /**
   * céer une nouvelle feuille
   * @return un pointeur sur un nouveau KdBag
   */
  KdBag *NewKdBag(void);

  // kdtree handling
  /**
   * découpe un noeud en deux parties
   * @param n noeud à découper
   */
  void SplitNode(KdNode *n);
  /**
   * retourne la dimension la plus répandues sur un KdBag
   * @param b KdBag pour effectuer la recherche
   * @parm nbdim nombre de dimension de la feuille
   */
  int GetMoreRependDim(KdBag *b, int nbdim);
  /**
   * retourne la valeur minimum d'une dimension d'un sac
   * @param b sac où effectuer la rechercher
   * @param dim dimension sur laquelle effectuer la recherche
   * @return valeur minimum contenue dans le sac
   */
  double min_of_dim(KdBag *b,int dim);
  /**
   * retourne la valeur maximum d'une dimension d'un sac
   * @param b sac où effectuer la rechercher
   * @param dim dimension sur laquelle effectuer la recherche
   * @return valeur maximum contenue dans le sac
   */
  double max_of_dim(KdBag *b,int dim);
 /**
   * retourne la valeur moyenne d'une dimension d'un sac
   * @param b sac où effectuer la rechercher
   * @param dim dimension sur laquelle effectuer la recherche
   * @return valeur moyenne contenue dans le sac
   */
  double moy_of_dim(KdBag *b,int dim);
 /**
   * retourne l'écart type d'une dimension d'un sac
   * @param b sac où effectuer la rechercher
   * @param dim dimension sur laquelle effectuer la recherche
   * @return écart type
   */
  double ecar_of_dim(KdBag *b,int dim,double w);

  /**
   * retourne la taille d'un noeud
   * @param n noeud à analyser
   * @return nombre de points du noeud
   */
  int GetSizeOfKdNode(KdNode *n);

  /**
   * Recherche le point le plus proche de 'target'.
   * Inspiré par [Moore91].
   *
   * @param node Le KdNode où on commence la recherche
   * @param targer le KdVector dont on cherche le plus proche voisin
   * @param qvalue qvalue du point le plus proche
   * @param hyperRectMin les coordonnées mini de l'hyper rectangle actuel de recherche
   * @param hyperRectMax les coordonnées maxi de l'hyper rectangle actuel de recherche
   * @param maxDist la distance max de recherche
   *
   * @return la distance du meilleur point trouvé ou maxDist
   * @return la qvalue de ce point
   */
  double KdFindClosestInNode(KdNode *node, KdVector *target, double &qvalue,
		       KdVector *hyperRectMin, KdVector *hyperRectMax,
			     double maxDist, KdVector *w);

  // store methods
  /**
   * enregiste un noeud dans un fichier
   * @param pf pointeur de fichier
   * @param n noeud
   */
  void Store(FILE *pf, KdNode *n);
  /**
   * enregistre un sac dans un fichier
   * @param pf pointeur de fichier
   * @param b sac
   */
  void Store(FILE *pf, KdBag *b);
  /**
   * enregistre un vecteur dans un fichier
   * @param pf pointeur de fichier
   * @param v vecteur
   */
  void Store(FILE *pf, KdVector *v);
  /**
   * enregiste une liste dans un fichier
   * @param pf pointeur de fichier
   * @param l liste de vecteur 
   */
  void Store(FILE *pf, KdVectorList *l);

  void info(char v, const char *format, ...);
};

#endif


