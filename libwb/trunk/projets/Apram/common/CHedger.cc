#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include "Singleton.h"
#include "List.h"
#include "CKdtree.h"
#include "CHedger.h"
//#include "calcul.h"
#include "CPeng.h"
#include "CMatrix.h"

CHedger::CHedger(CKdtree *kdtree)
{
  _peng = NULL;
  S = kdtree;
  possible_actions = S->NewKdVector(5);
  
  possible_actions->value[0] = HEDGER_ACTION_NOTHING;
  possible_actions->value[1] = HEDGER_ACTION_FORWARD;
  possible_actions->value[2] = HEDGER_ACTION_BACKWARD;
  possible_actions->value[3] = HEDGER_ACTION_TURNONLEFT;
  possible_actions->value[4] = HEDGER_ACTION_TURNONRIGHT;
  
  seuil = HEDGER_THRESHOLD;
  _angle = 0;
  _distance = 0;
  _alpha = HEDGER_ALPHA_DEFAULT_VALUE;
  _gamma = HEDGER_GAMMA_DEFAULT_VALUE;
  _bandwith = HEDGER_BANDWITH_DEFAULT_VALUE;
  _nb_points = HEDGER_NB_POINTS;
  _nb_points_max = HEDGER_NB_POINTS_MAX;
  _old_distance = 0;
  _verbosity = 0;


  // poids
  KdVector *we = CKdtree::NewKdVector(3);
  we->value[0] = 1; //poid de la distance :la distance a très peut d'importance
  we->value[1] = 1; //poid de l'angle : importance normal
  we->value[2] = 100;//l'action a une importance maximal
  S->SetWeight(we);

  _rewards = 0;
  _extern_states = S->NewKdVectorList();
  KdVector *temp = CKdtree::NewKdVector(6);
  temp->value[0] = -1;
  temp->value[1] = -1;
  temp->value[2] = HEDGER_ACTION_FORWARD;
  S->AddVector(_extern_states, temp);  
  temp = CKdtree::NewKdVector(3);
  temp->value[0] = -1;
  temp->value[1] = -1;
  temp->value[2] = HEDGER_ACTION_BACKWARD;
  S->AddVector(_extern_states, temp);  
  temp = CKdtree::NewKdVector(3);
  temp->value[0] = -1;
  temp->value[1] = -1;
  temp->value[2] =  HEDGER_ACTION_TURNONLEFT;
  S->AddVector(_extern_states, temp);  
  temp = CKdtree::NewKdVector(3);
  temp->value[0] = -1;
  temp->value[1] = -1;
  temp->value[2] =  HEDGER_ACTION_TURNONRIGHT;
  temp->value[0] = -2;
  temp->value[1] = -2;
  temp->value[2] = HEDGER_ACTION_FORWARD;
  S->AddVector(_extern_states, temp);  
  temp = CKdtree::NewKdVector(3);
  temp->value[0] = -2;
  temp->value[1] = -2;
  temp->value[2] = HEDGER_ACTION_BACKWARD;
  S->AddVector(_extern_states, temp);  
  temp = CKdtree::NewKdVector(3);
  temp->value[0] = -2;
  temp->value[1] = -2;
  temp->value[2] =  HEDGER_ACTION_TURNONLEFT;
  S->AddVector(_extern_states, temp);  
  temp = CKdtree::NewKdVector(3);
  temp->value[0] = -2;
  temp->value[1] = -2;
  temp->value[2] =  HEDGER_ACTION_TURNONRIGHT;
  S->AddVector(_extern_states, temp);

}

CHedger::~CHedger(void)
{
  S->FreeMem( _extern_states );
  S->FreeMem( possible_actions );
}

void CHedger::ActionToString(double act, char *txt)
{
  switch((int)act)
    {
    case HEDGER_ACTION_NOTHING:
      strcpy(txt, "rien ;)");
      break;
    case HEDGER_ACTION_FORWARD:
      strcpy(txt, "tout droit");
      break;
    case HEDGER_ACTION_BACKWARD:
      strcpy(txt, "derriere");
      break;
    case HEDGER_ACTION_TURNONLEFT:
      strcpy(txt, "a gauche");
      break;
    case HEDGER_ACTION_TURNONRIGHT:
      strcpy(txt, "a droite");
      break;
    }
}

void CHedger::setDistance(double dist)
{
  _old_distance = _distance;
  _distance = dist;
}

void CHedger::setAngle(double angle)
{
  _angle = angle;
}

int CHedger::Rewards(void)
{
  return _rewards;
}

int CHedger::Training(KdVector *x,
	       KdVector *xp,
	       double ac)
{
  int i;
  int maxind;
  int isinlist=0;
  double qtp,qt,qnew,maxq,m;
  double mindist;
  int status;
  int result;
  double maxstatus;
  KdVectorList *l1;
  KdVectorItemList *p;
  KdVector *v;

  KdVectorList *l= S->NewKdVectorList();
  l1 = S->NewKdVectorList();
  int rewards = Rewards();


  // mise à jour de rmin et rmax
  if(S->_rmin > rewards) 
    {
      S->_rmin = rewards;
      S->_qmin = S->_rmin/(1-_gamma);
    }
  if(S->_rmax < rewards)
    {
      S->_rmax = rewards;
      S->_qmax = S->_rmax/(1-_gamma);
    }

  ///affecter  qtp : maximum de q sur toutes les actions
    // v est le point (état+action) oÃ¹ on cherche une approximation
    // pour chaque action (v->value[v->size -1] = action_possible->value[i])
    // on calcule sa qValeur
    v = S->NewKdVector(xp->size + 1);
    for(i=0;i<xp->size;i++)
      {
	v->value[i] = xp->value[i];
      }
    v->value[v->size - 1] = possible_actions->value[0];
    
    maxq = Prediction( v, l1, &mindist, &status);
    maxind = 0;
    maxstatus = status;
    S->FreeMemWithoutVector(l1->head);
    l1->head = NULL;
    l1->size=0;
    l1->tail = NULL;
    //	printf("m %d: %f\n",0,maxq);
    for(i=1;i<possible_actions->size;i++)
      {
	v->value[v->size - 1] = possible_actions->value[i];
	if((m=Prediction(v,l1,&mindist,&status))>maxq)
	  {
	    maxq = m;
	    maxind = i;
	    maxstatus = status;
	  }
	//	printf("m %d: %f\n",i,m);
	S->FreeMemWithoutVector(l1->head);
	l1->head = NULL;
	l1->size=0;
	l1->tail = NULL;
      }
    S->FreeMem(l1);

    // qtp c'est max_a{q(s,a)}
    qtp = maxq;
    
    ///affecter qt = Q(s,a)
      // On calcule la qValeur de (s,ac)
      for(i=0;i<xp->size;i++)
	{
	  v->value[i] = x->value[i];
	}
      v->value[v->size - 1] = ac;
      
      mindist = 1000000000000000.0;
      qt = Prediction(v,l,&mindist,&status);
	//	printf("on a fini pour le qt\n");
	
      // nouvelle valeur de Q(s,ac)
      qnew = _alpha*(rewards + _gamma*qtp-qt)+qt;
	info(2, "rewards : %d", rewards);
	info(2, "qnew = %f,qtp : %f,qt %f,max %f",qnew,qtp,qt,maxq);
	//printVector(v);
	//	printf("\n");
	//	printf("l:\n");
	//	printListVector(l);
	//	printf("fin l\n");
//	p=l->head;
	v->key = qnew;

	// si 'xp' est un etat extérieur, on ne remet à jour que sa valeur
	// cherche si xp est dans _extern_states
	if( _extern_states != NULL ) {
	  p = _extern_states->head;
	  while( p!=NULL ) {
	    if( S->CompareVector(v, p->v) ) {
	      // dans la liste des _extern_states
	      p->v->key = qnew;
	      info(2, "_extern_states => maxind : %d", maxind);
	      S->FreeMem(v);
	      S->FreeMem(l);
	      return maxind; // @ voir
	    }
	    p = p->next;
	  }
	}

	// sinon (xp n'est pas dans la liste), on agit "normalement"
	p=l->head;
	while(p!=NULL && isinlist != 1 )
	{

	  if(S->CompareVector(p->v,v) == 1)
	    {
	      // si le point xp est dans l'arbre, on change sa valeur
	      info(2, " point cible DONC modifié!");
	      p->v->key = qnew;
	      // JBE
	     if(_peng  != NULL)
		_peng->compute(xp, qnew, rewards, p->v, 1.0);
	      isinlist = 1;
	    }
	  else
	    {
	      // on modifie aussi ses voisins
	      //   printf( " point voisin");
	      //if(qt!=QDEF)
	      if ((status == PRED_NORMAL) || (status == PRED_EXACT)) 
		{
		  //	 printf( " modifié!");
		  info(2, " p->v->weight %f ",p->v->weight);
		  p->v->key = _alpha*(p->v->weight)*(qnew-p->v->key) + p->v->key;
		 if(_peng  != NULL)
		    _peng->compute(xp, qnew, rewards, p->v, p->v->weight);
		  info(2, " new = %f ",p->v->key);
		}
	      //	      printf( "\n");
	    }
	  p=p->next;
	}
	
	// si le point xp est dans la liste (donc dans l'arbre)
	if(isinlist)
	{
	  info(2, "  MAJ : rien car pt dans arbre");
	  // on fait rien de plus
	  S->FreeMem(v);
	}
	else
	{
	  if(mindist > seuil) 
	    { // ajout du point dans l'arbre
	      info(2, "MAJ: Ajoute nouveau point dans arbre (%f)? ",mindist);
	      result = S->AddVector(v);
	      info(2, " OUI (%d)",result );
	      // JBE
	      if(_peng  != NULL)
		{
		  info(2, "Calcul de peng");
		  _peng->compute(xp, qnew, rewards, v, 1.0);
		}
	    }
	  else
	     S->FreeMem(v);
	}
 
	S->FreeMemWithoutVector(l->head);
	delete l;

	return maxind;
}



double CHedger::Prediction(KdVector *xq,
		    KdVectorList *l,
		    double *mindist,
		    int *status)
{
  CMatrix *K;
  CMatrix *V;
  CMatrix *KT;
  CMatrix *KTK;
  CMatrix *KTK_inv;
  CMatrix *X;
  CMatrix *XT;
  CMatrix *H;
  CMatrix *temp1;
  CMatrix *temp2;
  KdVectorItemList *p;
  int i=0,j;
  double maxvi;
  double res=QDEF;
  *status = PRED_NORMAL;
  
  info(2, "** Prediction (distance : %f angle : %f action %f) **", xq->value[0], xq->value[1], xq->value[2]);

  //on init K
  
  // cherche si pas dans ext_state
  if( _extern_states != NULL ) 
    {
      p = _extern_states->head;
      while( p!=NULL ) {
	if( S->CompareVector( xq, p->v) ) 
	  {
	    // dans la liste des ext_states, renvoie valeur "fixee"
	    //      printf("point externe\n"); 
	    info(2, "point externe");
	    *status = PRED_EXT;
	    return p->v->key;
	  }
	p = p->next;
      }
    }

  // La feuille contenant xp.
  GrabValues(S->_head, xq, l);
  info(2, "nombre de points : %d", l->size);

  p = l->head;
  while(p!=NULL)
    {
      //	    printf( "// cherche si point exact !\n");
      if(S->CompareVector(xq,p->v))
	{
	  //printf("point exacte !\n");
	  info(2, "point exacte");
	  *status = PRED_EXACT; // rien
	  return p->v->key;
	  
	}
      p = p->next;
      
    }
  
  
  //	printf( "// assez de points ?\n");
  if(l->size < _nb_points)
    {
      //	printf("pas assez de point %d (%d)\n", _nb_points, l->size);
//      info(2, "pas assez de point %d (%d)", _nb_points, l->size);
      *status = PRED_NOT_ENOUGH; //rien
      res = QDEF;
    }
  else
    {
      
      //printf("assez de point %d (%d)\n", _nb_points, l->size);
      //  printf( "// cherche si xp est dans l'enveloppe convexe (ivh)\n");
      info(2,  "// cherche si xp est dans l'enveloppe convexe (ivh)");
     // K = svdNewDMat(l->size,l->head->v->size+1);
      K = new CMatrix(l->size,l->head->v->size+1);
      p = l->head;
      while(p!=NULL)
	{
	  info(3, "  pt: %f %f %f %f (%f)", 
	       p->v->value[0], p->v->value[1], p->v->value[2], p->v->key, p->v->weight); 
	  for(j=0;j<p->v->size;j++)
	    {
	      //K->value[i][j] = p->v->value[j];
	      K->Set(i, j, p->v->value[j]);
	    }
	  // Ajout d'une composante pour valeur à l'origine
//	  K->value[i][j] = 1.0;
	  K->Set(i, j, 1.0);
	  p=p->next;
	  i++;
	}
      //printf( "//on init X\n");
      X = new CMatrix(xq->size+1,1);
//      X = svdNewDMat(xq->size+1,1);
      for(j=0;j<xq->size;j++)
	    {
	      //X->value[j][0] = xq->value[j];
	      X->Set(j, 0, xq->value[j]);
	    }
      // Ajout d'une composante pour valeur à l'origine
//      X->value[j][0] = 1.0;
       X->Set(j, 0, 1.0);
	  //printf( "//calcule de l'ivh\n");
      info(2, "calcul de l'ivh");
//      K->Print();
      
     // printf("K\n");
      //K->Print();
      KT = K->Transpose();
     // printf("KTn");
      //KT->Print();

  //    KT->Print();
      KTK = KT->Dot(K);
      //printf("KTK\n");
      //KTK->Print();

      KTK_inv = KTK->PseudoInverse();
     // printf("KTK_inv\n");
     // KTK_inv->Print();

      temp1 = KTK_inv->Dot(KT);
     // printf("temp1\n");
     // temp1->Print();

      V = K->Dot(temp1);
     // printf("V\n");
      //V->Print();
      maxvi = V->getMaxDiag();
      //printf("maxvi : %f\n",maxvi);
      info(2, "maxvi : %f", maxvi);
      
      XT = X->Transpose();
      temp2 = KTK_inv->Dot(X);
      H = XT->Dot(temp2);
		//printf("V\n");
	  //printmatrix(V);
	  //printf("H : \n");
	  ///printmatrix(H);
	    //printf("\n\n");
	    
	if(H->Get(0, 0) <= maxvi)
	{
		// le point x est dans la zone ivh
	  info(2, "on est dans l'ivh");
		res = LWR_PREDICTION(l,xq,S->_weight,mindist);
		if((res < S->_qmin)||(res > S->_qmax))
		{
		//	    printf("mais c'est en dehors des bornes (%f) <> [%f, %f]\n",res, S->_qmin, 10*S->_qmax);
			*status = PRED_BOUNDS; //rien
			res = QDEF;
		}
	}
	else
	{
	  info(2, "on est pas dans l'ivh");
		//printf("on est pas dans l'ivh");
		*status = PRED_IVH; //rien
		res = QDEF;
	}
	    
	// free memory
	delete K;
	delete V;
	delete KT;
	delete KTK;
	delete KTK_inv;
	delete X;
	delete XT;
	delete H;
	delete temp1;
	delete temp2;

    }
  info(2, "qvalue retourne : %f", res);
  return res;
}



double CHedger::LWR_PREDICTION(KdVectorList *l,KdVector *xq,KdVector *w,double *mindist)
{
  int i=0,j=0;
  double res;
  KdVectorItemList *p;
  KdVectorList *l2;
  KdVectorList *l3;
  CMatrix *A = new CMatrix(l->size,l->head->v->size + 1);
  CMatrix *B = new CMatrix(l->size,1);
  CMatrix *X;
  CMatrix *K = new CMatrix(l->size,l->head->v->size + 1);
  CMatrix *Zinv;
  CMatrix *AtK;
  
  
  // Trie les vecteurs de 'l' et calcule la distance minimale à xp.
  p = l->head;
  l2 = S->NewKdVectorList();
  *mindist = S->GetVectorDistance(p->v,xq,w);
  while(p!=NULL)
    {
      p->v->dist = S->GetVectorDistance(p->v, xq, w);
      info(3, "distance %f", p->v->dist);
      if(p->v->dist < *mindist)
	{	
	  *mindist = p->v->dist;
	}
      S->AddVectorSortByDist(l2, p->v);
      p = p->next;
    }
  
  // 'l3' est composée, au maximum, des K vecteurs les plus proches de xp.
  p=l2->head;
  l3 = S->NewKdVectorList();
  while(p!=NULL)// && i<_nb_points)
    {
      S->AddVector(l3,p->v);
  info(3, "  pt ordonnee : %f %f %f %f (%f)", 
	       p->v->value[0], p->v->value[1], p->v->value[2], p->v->key, p->v->weight); 
      p=p->next;
      i++;
    }
  S->FreeMemWithoutVector(l2->head);
  delete l2;
  
  // Prépare les matrices A et B
  // A composée des examples (pondérés par poids) et B de leur qValeur
  p=l3->head;
  i=0;
  while(p!=NULL)
    {
      p->v->weight = kernel( p->v->dist, _bandwith );
      for(j=0;j<p->v->size;j++)
	{
	  A->Set(i, j, p->v->value[j] * p->v->weight);
	  K->Set(i, j, p->v->value[j]);
	}
      A->Set(i, j, 1.0 * p->v->weight);
      K->Set(i, j, 1.0);
      B->Set(i, 0, p->v->key);
      i++;
      p = p->next;
    }

  //A->Normalize(0);
  //A->Normalize(1);
//  Zinv = A->PseudoInverse();
  AtK = A->Transpose()->Dot(K);
  Zinv = AtK->PseudoInverse()->Dot(A->Transpose());
  
  if(Zinv->IsEmpty() == false)
    {
      // Si la pseudo inverse est non-nulle.
      X = Zinv->Dot(B);
      i=0;
      res = 0;
	
      for(i=0;i<xq->size;i++)
	{
	  res+= X->Get(i, 0) * xq->value[i];
	}
      res+= X->Get(i,0);
      delete X;
    }
  else
    {
      // sinon on fait la moyenne des qValeurs des examples
      res=0;
      for(i=0;i<B->_row;i++)
	{
	  res+= B->Get(i,0);
	}
      res = res/(B->_row);
    }

  delete A;
  delete B;
  delete Zinv;
  delete AtK;

  S->FreeMemWithoutVector(l3->head);
  delete l3;
  info(3, "resultat de la regression : %f\n",res);
  return res;
}

bool CHedger::readTraces(char *path)
{
  KdVector *x;
  KdVector *xp;
  float odi, oan, di, an;
  int act;
  FILE * pf = fopen("traces.txt", "r");
  if(pf == NULL)
    {
      printf("pas de fichier traces.txt !\n");
      return false;
    }
  float r;
  char *p;
  char line[255];
  memset(line, 0, 255);

  while ((fscanf(pf, "%[^\n]", line)) != EOF)
    {
      fgetc(pf);
    if(strlen(line) > 0)
	{
	  p = line;
	  odi = oan = act = di = an = 0;
	  r = 0.0;
	  
	  sscanf(line, "%f\t%f\t%d\t%f\t%f\t%f",
		 &odi, &oan, &act, &di, &an, &r);

	  x = CKdtree::NewKdVector(2);
	  x->value[0] = odi;
	  x->value[1] = oan;
      
	  xp = CKdtree::NewKdVector(2);
	  xp->value[0] = di;
	  xp->value[1] = an;
	  
	  _rewards = r;

	  Training(x, xp, act);
	}
    memset(line, 0, 255);
    }
  fclose(pf);
  return true;
}

void CHedger::GrabValues(KdNode *n, KdVector *target, KdVectorList *l)
{
    KdVectorItemList *itemlist;
  if(!n) return ;
  if(n->leaf)
    {
      itemlist = n->bag->list->head;
      while(itemlist != NULL && l->size <  _nb_points_max)
	{
	  itemlist->v->dist = S->GetVectorDistance(itemlist->v, target, S->_weight);
	  itemlist->v->weight = kernel( itemlist->v->dist, _bandwith );
	//  info(2, "GrabValues, %f %f",
	  //     itemlist->v->value[itemlist->v->size -1],
	    //   target->value[target->size -1]);

	  //if(itemlist->v->key != 0.0)
	    if((itemlist->v->value[itemlist->v->size -1] == target->value[target->size -1]))
	      {
		//	      if( itemlist->v->weight >=  _kernel_min_value )
		{
		  // add vector sort by dist
		  S->AddVectorSortByDist(l, itemlist->v);
		}
	      }
	  itemlist = itemlist->next;
	}
    }
  else
    {
      //GrabValues( n->ChildRight, target, l);
      //GrabValues( n->ChildLeft, target, l);
      if( target->value[n->index_dim] > n->value )
	{
	  GrabValues( n->ChildRight, target, l);
	  if(l->size < _nb_points)
	    GrabValues( n->ChildLeft, target, l);
	}
      else
	{
	  GrabValues( n->ChildLeft, target, l);
	  if(l->size < _nb_points)
	   GrabValues( n->ChildRight, target, l);
	}
    }
}

double CHedger::kernel(double d, double h)
{
  return exp(-((d/h)*(d/h)));
}

void CHedger::info(int v, const char *format, ...)
{
  if(v <= _verbosity)
    {
      if (format)
	{
	  va_list pa;
	  
	  va_start (pa, format);
	  vprintf (format, pa);
	  printf("\n");
	  fflush(stdout);
	}
    }
}

int CHedger::BestAction(KdVector *s)
{
  KdVectorList *l1 = CKdtree::NewKdVectorList();
  KdVector *v = CKdtree::NewKdVector( s->size );
  double maxq = 0;
  int action = 0;
  double mindist = 0.0;
  int status = 0;
  double m = 0.0;
  int i = 0;
  for(i=0;i< s->size-1 ;i++)
    {
      v->value[i] = s->value[i];
    }
  v->value[v->size -1] = HEDGER_ACTION_NOTHING;
  maxq = Prediction(v, l1, &mindist, &status);
  action =  HEDGER_ACTION_NOTHING;
  CKdtree::FreeMemWithoutVector(l1->head);
  l1->head = NULL;
  l1->size=0;
  l1->tail = NULL;

  for(i=1;i<=HEDGER_ACTION_TURNONRIGHT;i++)
    {
      v->value[v->size -1] = i;
      if((m=Prediction(v, l1, &mindist, &status))>maxq)
	{
	  maxq = m;
	  action = i;
	}
      CKdtree::FreeMemWithoutVector(l1->head);
      l1->head = NULL;
      l1->size=0;
      l1->tail = NULL;
    }
  delete l1;
  CKdtree::FreeMem(v);
  return action;
}
