/** \file
 * \brief mini parseur xml
 * Cette classe permet de parser un fichier xml
 * et charge le contenu dans des listes stl
 \todo supprimer cette classe et utiliser tinyxml (http://www.grinninglizard.com/tinyxml/) 
*/

#ifndef xmlParser_h
#define xmlParser_h

#include <iostream>
#include <cstdlib>
#include <stdarg.h>

#include <vector>
#include <deque>

struct _xmlAttr;
struct _xmlTag;

typedef _xmlAttr xmlAttr, *LPxmlAttr;
typedef _xmlTag xmlTag, *LPxmlTag;
typedef std::vector<LPxmlAttr> xmlAttrs, *LPxmlAttrs;
typedef std::vector<LPxmlTag> xmlTags, *LPxmlTags;


/**
 * définition d'un attribut d'un tag
 */
typedef struct _xmlAttr
{
	char	*pcName;
	char	*pcValue;
	
	xmlTag	*parent;
} xmlAttr;

/**
 * définition d'un tag xml
 */
typedef struct _xmlTag
{
	char		*pcName;
	char		*pcValue;
	
	xmlTag		*parent;
	xmlAttrs	Attr;
	bool		bTagCompleted;
} xmlTag;


#define MAX_LEN	500	///< longueur maximum d'une chaîne de caractère

#define SKIP_SPACE(x) while( x && (isspace(*x) || *x=='\n' || *x=='\t') ) x++;
#define SKIP_SPACE_VALUE(x) while( x && (*x=='\n' || *x=='\t') ) x++;

#define COPY_STRING(x, y)						\
  i=0;									\
  while ((y[i++] = *x++)) {						\
    if((*x=='\r') || (*x=='\n') || (*x=='\t') || (*x==' ') ||		\
       (*x=='<')  || (*x=='>')  || (*x=='=')  || (*x=='\"'))		\
      break;								\
  }

#define COPY_STRING2(x, y)						\
  i=0;									\
  while((y[i++] = *x++)) {						\
    if((*x=='\r') || (*x=='\n') || (*x=='\t') ||			\
       (*x=='<')  || (*x=='>')  || (*x=='\"'))				\
      break;								\
  }

#define COPY_STRING_VALUE(x, y)						\
  i=0;									\
  while((y[i++] = *x++)) {						\
    if((*x=='\r') || (*x=='\n')  || (*x=='\t') || (*x=='\"') ||		\
       (*x=='<'&&(*(x-1))!='\\') || (*x=='>'&&(*(x-1))!='\\'))		\
      break;								\
  }

class CXmlParser  
{
public:
	CXmlParser();
	virtual ~CXmlParser();
	
	bool	InitializeWithFile(char *pcFileName);
	
	bool	Initialize(char *pcFileName);

	void	WriteTo(char *pcFileName);

	void	WriteToAutoCall(FILE *pf, xmlTag *parent);

	void	Error(const char *pcMessage, ...);

	void	Clear(void);

	xmlTag *Search(char *pcAway);

	xmlTag *GetChild(xmlTag *parent);

	xmlTag *NextChild(xmlTag *child);

	xmlTag *AddTag(xmlTag *parent, char *pcName, char *pcValue);

	xmlTag *SetTag(int indice, xmlTag *parent, char *pcName, char *pcValue);

	xmlAttr *AddAttr(xmlTag *tag, char *pcName, char *pcValue);

	void	RemoveTag(char *lpData, const char * lpTagStart, const char * lpTagEnd);

	void printStartTag(FILE *pf, xmlTag *TagSel);

	void printEndTag(FILE *pf, xmlTag *TagSel);

	xmlTags		m_XmlDocument;	///< document xml
	xmlTag		*m_TagSelected; ///< tag en cours de sï¿½ection
};


#endif
