/**
 * \file
 * \brief classe permettant l'exportation d'un kdtree
 */
#ifndef KDTREEEXPORT_H
#define KDTREEEXPORT_H

/**
 * type d'exportation : données brutes ou données interpolées
 */
typedef enum TypeOfExport
  {
    EXPORT_NOTHING,
    EXPORT_INTERPOL,
    EXPORT_RAW
  } TypeOfExport;

class CKdtreeExport
{
 public:
	  CKdtreeExport(void);
	  ~CKdtreeExport(void);

	/**
	 * Affecte l'adresse de l'instance hedger
	 * @param tree instance CHedger
	 */
	void setKdtree(CHedger *tree);
	
	/**
	 * Précision des axes
	 * @param x définition de la précision de l'axe distance
	 * @param y définition de la précision de l'axe angle
	 */
	void setPrecision(double x, double y);
	
	/**
	 * définition des limites de l'exportation
	 * @param minx définition du minimum de l'axe distance
	 * @param miny définition du minimum de l'axe angle
	 * @param maxx définition du maximum de l'axe distance
	 * @param maxy définition du maximum de l'axe angle
	 */
	void setLimits(double minx, double miny, double maxx, double maxy);
	
	/**
	 * Normalisation de l'axe z
	 * @param new_prec_z 1 par défaut
	 */
	void ScaleZ(double new_prec_z);
	
	/**
	 * Libère la mémoire
	 */
	void Reset(void);
	
	/**
	 * Alloue la mémoire
	 */
	void Allocate(void);
	
	/**
	 * Alloue la mémoire destinée à générer des images
	 */
	void initValuesImage(void);

	/**
	 * Génère l'interpolation et stocke le résultat dans un fichier
	 * @param file nom de fichier de sortie
	 * @param act action de l'interpolation
	 */
	void Interpol(char *file, double act);
	
	/**
	 * Génère l'interpolation
	 * @param act action choisie pour l'interpolation
	 */
	void Interpol(double act);
	
	/**
	 * Génère une image affichant l'interpolation
	 * @param file nom du fichier image
	 * @param filestatus nom du fichier image contenant l'état de l'interpolation
	 * pour chaque point
	 * @param act action sélectionnée
	 */
	void InterpolImage(char *file, char *filestatus, double act);

	/**
	 * Exportation des valeurs du kdtree
	 * @param act action sélectionnée
	 */
	void Raw(double act);
	
	/**
	 * Exportation des valeurs du kdtree dans un fichier
	 * @param file nom du fichier de sortie
	 * @param act action sélectionnée
	 */
	void Raw(char *file, double act);  
	
	/**
	 * Exportation des valeurs du kdtree sous forme d'appel récursif
	 * @param n noeud de départ
	 * @param action action sélectionnée
	 */
	void RawLoop(KdNode *n, double action);

	float *x, *y, **z, **z_backup;
	double _max_z;
	int _TypeOfPrediction[PRED_NULL]; ///< compteur de prédiction
 private :
	KdVector *_limits;
	CHedger *_smart;
	KdVector *_sel;
	double _prec_x;
	double _prec_y;

	/**
	 * Initialise les axes angle, distance et qvalue
	 * @param toe type d'exportation
	 * @param action action sélectionnée
	 */
	void Build(TypeOfExport toe, double act);

	/**
	 * Sauvegarde un noeud dans un fichier
	 * @param pf handle du fichier
	 * @param n noeud à sauvergarder
	 * @param sel permet de sélectionner les valeurs à sauvegarder
	 */
	void Store(FILE *pf, KdNode *n, KdVector *sel);
	
	/**
	 * Sauvegarde un noeud dans un fichier
	 * @param pf handle du fichier
	 * @param b sac à sauvergarder
	 * @param sel permet de sélectionner les valeurs à sauvegarder
	 */
	void Store(FILE *pf, KdBag *b, KdVector *sel);
	
	/**
	 * Sauvegarde un noeud dans un fichier
	 * @param pf handle du fichier
	 * @param v vecteur à sauvegarder
	 * @param sel permet de sélectionner les valeurs à sauvegarder
	 */
	void Store(FILE *pf, KdVector *v, KdVector *sel);
	
	/**
	 * Sauvegarde un noeud dans un fichier
	 * @param pf handle du fichier
	 * @param l liste à sauvegarder
	 * @param sel permet de sélectionner les valeurs à sauvegarder
	 */
	void Store(FILE *pf, KdVectorList *l, KdVector *sel);  

	/**
	* Fonction qui retourne la q-valeur interpolée
	* du point distance, angle, action demandé
	* @param dist distance du point par rapport à la cible
	* @param angle angle par rapport à la cible
	* @param act action pour laquelle rechercher la q-valeur
	* @return la q-value interpolée
	*/
	double KdInterpol(double dist, double angle, double act);

	/**
	* Fonction qui retourne la q-valeur la plus proche
	* du point distance, angle, action demandé
	* @param dist distance du point par rapport à la cible
	* @param angle angle par rapport à la cible
	* @param act action pour laquelle rechercher la q-valeur
	* @return 0 si le point réel le plus proche est trop loin
	* @return la q-value la plus proche
	*/
	double KdRaw(double dist, double angle, double act);

	int indicex, indicey, indicez;
	int _mem_allow;
	unsigned char *_buf_image;
	unsigned char *_buf_image_status;
 
};

#endif



