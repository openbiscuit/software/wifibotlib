#include <iostream>
#include <unistd.h>
#include <memory.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdarg.h>
#include "CThread.h"
#include "CTcp.h"

CTcp::CTcp() : Run(true)
{
}

void CTcp::setSock(SOCKET s)
{
  m_socket = s;
}

CTcp::~CTcp()
{
}


bool CTcp::Connect(char *host, int sock)
{
  phost = gethostbyname(host);
  if(phost == NULL)
    return false;
  m_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (m_socket == -1) return false;
  memset(&serv_addr, 0x00, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons((uint16_t)sock);
  memcpy(&serv_addr.sin_addr, phost->h_addr, phost->h_length);
  if( connect (m_socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1 )
    {
      return false;
    } 
  return true;
}

bool CTcp::Sendf(const char *format, ...)
{
  char *s = NULL;
  bool ret = false;
  if (format)
    {
      char t[1];
      va_list pa;
      size_t size = 0;
      
      va_start (pa, format);
      size = vsnprintf (t, 1, format, pa);
      size++;
      s = new char[(sizeof (*s) * size)];
      if (s)
	{
	  vsnprintf (s, size, format, pa);
	}
    }
  ret = Send(s);
  if (s) delete s;
  return ret;
}


bool CTcp::Send(char *data)
{
  return Send(data, strlen(data));
}

bool CTcp::Send(char* data, int sz )
{
  int nb = 0;
  char *p = data;
  int tosend = sz;
 // printf("sended %s\n", data);fflush(stdout);
  while(tosend > 0)
    {
      nb = send(m_socket, p, (int)tosend, MSG_NOSIGNAL);
   //   printf("send %d\n", nb);fflush(stdout);
      if ( nb <= 0 )
	return false;
      p += nb;
      tosend -= nb;
    }
  return true;
}

int CTcp::Rec(char *data, int max)
{
  return recv(m_socket, data, max, 0);
}


void	CTcp::Receive(char *data, int &len, int max)
{
	len = recv(m_socket, data, max, 0);
	int received = 0;
	int ret;
	char *p = data;
//	char ctry = 3;
	do
	{
		ret = recv(m_socket, p, max-received, 0);
//		printf("recu %d", ret);
	//	if(ret < max-received)
		//  ctry--;
	//	if(ctry < 0)
		//  return ;
		if(ret <= 0)
		  return ;
		received += ret;
		p += ret;
		usleep(1);
	} while(received < max);
	len = received;
}

int CTcp::ReadLine(char *data, int max)
{
/*	int index = 0;
	char c;
	int rep = 0;
	
	rep = recv(m_socket, &c, 1, 0);
		
	while (rep!= -1 && index < max && c!='\n')
	{
		data[index++] = c;
		rep = recv(m_socket, &c, 1, 0);		
	} 
	return rep;*/
  int index = 0;
  char c;
  int rep = 0;
	
  rep = recv(m_socket, &c, 1, 0);
		
  while (rep!= -1 && index < max && c!='\n')
    {
      data[index++] = c;
      rep = recv(m_socket, &c, 1, 0);
    } 
  return rep;			
}


int CTcp::LoopReceive(char *data, int max)
{
  int received = 0;
  int ret;
  char *p = data;
  do
    {
      ret = recv(m_socket, p, max-received, 0);
      if(ret == -1) return -1;
      received += ret;
      p += ret;
    } while(received < max);
  return 1;
}


bool CTcp::waiting(int sock,  CThread *task)
{
	
	struct sockaddr_in ServerSock;
	int ServerSockSize = sizeof(struct sockaddr_in);
	SOCKET Client;
	struct sockaddr_in ClientSock;
	int ClientSockSize = sizeof(struct sockaddr_in);
	struct ClientInfos *Cli;
	int iClientId = 0;
	Run = false;
//	struct hostent* ClientInfos;

	SServer = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (!SServer) 
	{
		fprintf(stdout, "failure\ncreate socket ...");
		return false;
	}
	ServerSock.sin_family = AF_INET;
  	ServerSock.sin_port = htons(sock);
	ServerSock.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(SServer, (const struct sockaddr*) &ServerSock, ServerSockSize)) 
	{
		fprintf(stdout, "failure bind ...\n");
		return false;
	};

	if (listen(SServer, 0x800)) 
	{
 		fprintf(stdout, "failure listen ...\n");
    		return false;
 	}

	Run = true;
	while (Run) 
	{
	  Client = accept(SServer, (struct sockaddr*) &ClientSock, (socklen_t*) &ClientSockSize);
	  if (!Client) break;
	  Cli = new struct ClientInfos;
	  Cli->id = iClientId++;
	  Cli->HostName = NULL;	
	  sprintf(Cli->IP, "%s", inet_ntoa(ClientSock.sin_addr));
	  hostent* hostTmp = gethostbyaddr((char*) &(ClientSock.sin_addr.s_addr), 4, AF_INET);
	  if (hostTmp) 
	    {
	      Cli->HostName = new char[strlen(hostTmp->h_name)];
	      strcpy(Cli->HostName, hostTmp->h_name);
	    }
	  Cli->sock =  Client;
	  task->start(Cli);
	  usleep(1);
//	  pthread_create(&Thread, NULL, fct, Cli);
	}
	shutdown(SServer, 0);
	fprintf(stdout, "failure\nClosing clients sockets... ");
	close(SServer);
	Run = true;
	return true;
}

void CTcp::StopServer(void)
{
  if(Run == false) return ;
  Run = false;
  shutdown(SServer, SHUT_RDWR);
  close(SServer);
  //while(Run == false);
}

void CTcp::StopClient(void)
{
//	close(SServer);
  close(m_socket);
}
