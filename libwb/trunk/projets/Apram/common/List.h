/** 
 * \file
 * \brief liste doublement chaînée
 */
#ifndef LIST_H
#define LIST_H


typedef struct _tsbList tsbList;

/**
 * structure contenant un noeud de la chaine
 */
struct _tsbList
{
	void	*data; ///< données à stocker
	tsbList	*next; ///< pointeur vers l'élément suivant
	tsbList	*prev; ///< pointeur vers l'élément précédent
};

/**
 * classe CList : implémentation de la liste chainée
 */
class CList
{
public:
	CList(void); ///< constructeur
	~CList(void); ///< destructeur

	/**
	 * ajout un élément à la liste
	 * @param item donnée à stocker
	 */
	void Add(void *item) { Append(item); } ;
	

	/**
	 * ajoute un élément en fin de liste
	 * @param item donnée à stocker
	 */
	void Append(void *item);

	/**
	 * ajoute un élément en début de liste
	 * @param item donnée à stocker
	 */
	void Prepend(void *item);

	/**
	 * insére un élément dans la liste
	 * @param item donnée à stocker
	 * @param pos indice où stocker les données
	 */
	void Insert(void *item, int pos);

	// Suppression d'un element par une comparaison de sa valeur
	/**
	 * suppression d'un élément
	 * @item donnée à supprimer
	 */
	void Remove(void *item);

	/**
	 * suppression du premier élément de la liste
	 */
	void RemoveHead(void);

	/**
	 * suppression du dernier élément de la liste
	 */  
	void RemoveEnd(void);

	/**
	 * retourne un pointeur du premier élément de la liste
	 */
	void *GetHead(void);

	/**
	 * retourne un pointeur du dernier élément de la liste
	 */
	void *GetEnd(void);

	/**
	 * retourne la taille de la liste (le nombre d'élément)
	 */
	int GetLength();

	/**
	 * retourne l'indice d'un élément spécifié
	 * @param item élément rechercher
	 * @return indice de l'élément -1 si non trouvé
	 */
	int GetIndice(void *item);

	/**
	 * retourne un pointeur sur l'item spécifié
	 * @param indice de l'item désiré
	 * @return pointeur sur donnée
	 */
	void *Get(int indice);

	/**
	 * supprime une entrée de la liste
	 * @param indice indice de l'item recheché
	 */
	void Remove(int indice);

	// retourne une occurence de la liste en se basant sur la valeur de l'item
	/**
	 * retourne un pointeur de structure sur l'élément recherché
	 * @param item élément recherché
	 * @return pointeur de tsbList
	 */
	tsbList *Find(void *item);

	/**
	 * désallocation de la mémoire utilisé par la liste
	 */
	void	Free(void);
	
	/**
	 * unuse
	 */
	void	Lock(void);
private:
	tsbList	*head; ///< tête de la liste
	tsbList	*end; ///< queue de la liste
	int length; ///< longueur de la liste
	bool m_lock; ///< unuse
};


#endif

