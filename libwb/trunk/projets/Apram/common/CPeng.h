/**
 * \file
 *  \brief classe d'implémention d'Incremental Multi-Step Q-Learning
 */
#ifndef CPENG_H
#define CPENG_H

#include "List.h"


#define CPENG_DEFAULT_LAMBDA 0.5f ///< valeur par défaut lambda


/**
 * structure de stockage d'une trace
 */
typedef struct
{
  KdVector *v; ///< vecteur concerné par la trace
  double tr; ///< valeur de la trace
} CPeng_Trace;


class CPeng
{
 public:
  /**
   * constructeur
   */
  CPeng(CHedger *smart);
  /**
   * destructeur
   */
  ~CPeng(void);
  /**
   * réalise le calcul
   * @param xp etat de destination
   * @param qvalue q-valeur de passage de x vers xp
   * @param rewards récompense du passage
   * @param v etatAction concerné
   * @param weight attenuation trace (distance a l'état réel )
   */
  void compute(KdVector *xp, double qvalue, double rewards, KdVector *v, double weight);

  /**
   * vide la trace d'éligibilité
   */
  void reset(void);
  /**
   * récupère la q-valeur de qt+1
   * @return double la valeur q
   */
  double getQValue(void);
  double _lambda; ///< cf algo Peng
 private:
  /*
   * retourne q valeur de la meilleur action à l'état x
   * @param x état à évaluer
   * @return la q-valeur de la meilleure action
   */
  double bestActionFromState(KdVector *etat);
  double bestActionFromStateAction(KdVector *etat);
  double _e; ///< élégibilité du coup
  double _eMax; ///< élégibilité max du coup
  CHedger *_smart;

  CList _Tr; ///< liste des traces d'éligibilitées
};

#endif


