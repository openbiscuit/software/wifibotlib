#ifndef CTHREAD_H
#define CTHREAD_H

/**
 * \file
 * \brief gestion des threads au travers d'une classe sous linux
 * \author Béchu Jérôme
 */

/**
 * classe CThread
 */
class CThread
{
   public:
  /**
   * constructeur
   */
  CThread(void);
  /**
   * déstructeur
   */
  virtual ~CThread(void);
  /**
   * lance le thread
   * @param arg argument du thread
   * @return int retourner par  pthread_create
   */
  int start(void * arg);
  /**
   * stop le thread
   */
  void stop(void);
 protected:
  /**
   * permet de lancer le thread
   */
  void run(void);
  /**
   * point d'entrée du thread
   * @param pointeur sur une occurence CThread
   * @return NULL
   */
  static void * EntryPoint(void* pthis);

  /**
   * configuration avant le lancement de la tâche
   */
  virtual void setup(void);
  /**
   * coeur du thread
   */
  virtual void execute(void);
  void wait(void);
 protected:
  pthread_t _Thread; ///< handle du thread
  void * _Arg; ///< argument passé au thread
};

#endif

