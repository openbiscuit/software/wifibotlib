#include <iostream>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "Singleton.h"
#include "CKdtree.h"



CKdtree::CKdtree(void)
{
  init();
}

CKdtree::~CKdtree(void)
{
  FreeMem();
}

void CKdtree::init(void)
{
 _head = NewKdNode();
  _dim = 3;
  _limit_of_bag = 10;
//  _maxValues = NewKdVector(3);
  _verbosity = 0;
  _weight = NULL;
  _qmin = 0.0;
  _qmax = 0.0;
  _rmin = 0.0;
  _rmax = 0.0;
  /*for(int i=0;i<_maxValues->size;i++)
    {
      _maxValues->value[i] = 0.0;
    }*/
  info(3, "Creation du kdtree dim:%d, _limit_of_bag:%d", _dim, _limit_of_bag);
}

void CKdtree::SetWeight(KdVector *v)
{
  _weight = v;
}

int CKdtree::GetTotalSize(void)
{
  return GetSizeOfKdNode(_head);
}

void CKdtree::SaveTo(char *file)
{
  FILE *pf;
  pf = fopen(file, "w");
  if(pf == NULL) return ;
  info(3, "Save Kdtree to %s", file);
  fprintf(pf, "%4.4f %4.4f %4.4f %4.4f\n", _qmin,
	  _qmax, _rmin, _rmax);
  Store(pf, _head);
  fprintf(pf, "\n");
  fclose(pf);
}

void CKdtree::LoadFrom(char *file)
{
  FILE *pf;
  char line[255];
  double val1, val2, val3, val4;
  pf = fopen(file, "r");
  if(pf == NULL) return ;
  char *p;
  KdVector *v;
  int firstline = 1;

  while ((fscanf(pf, "%[^\n]", line)) != EOF)
    {
      fgetc(pf);    // Reads in '\n' character and moves file
                     // stream past delimiting character

      if(strlen(line) > 0)
	{
	  p = line;
	  val1 = val2 = val3 = val4 = 0;
    
	  val1 = atof(p);while(*p++ != ' ' && p);
	  if(!p) break;
	  val2 = atof(p);while(*p++ != ' ' && p);
	  if(!p) break;
	  val3 = atof(p);while(*p++ != ' ' && p);
	  if(!p) break;
	  val4 = atof(p);

	  if(firstline == 1)
	    {
	      _qmin = val1;
	      _qmax = val2;
	      _rmin = val3;
	      _rmax = val4;
	      firstline = 0;
	    }
	  else
	    {
	      v = NewKdVector(3);
	      v->value[0] = val1;
	      v->value[1] = val2;
	      v->value[2] = val3;
	      //	      v->value[3] = val4;
	      v->key = val4;
	      AddVector(v);
	    }
	  
//	  printf("%f %f %f %f\n", val1, val2, val3, val4);
	}
      memset(line, 0, 255);
    }

  fflush(stdout);
  fclose(pf);
}


/////////// PUBLIC KDTREE HANDLING ///////////////
int  CKdtree::AddVector(KdVector *v)
{
  KdNode *CurrentNode = _head;

  // if the size of vector is not equal that the global size
  // -> return
  if(v->size != _dim)
    return -1;

  // through the tree, until the node is a leaf
  while(CurrentNode->leaf == 0)
    {
      if(v->value[CurrentNode->index_dim] <= CurrentNode->value)
	CurrentNode = CurrentNode->ChildLeft;
      else
	CurrentNode = CurrentNode->ChildRight;
    }
  
  if(LookUpVector(CurrentNode->bag->list, v) == 0)
    {
      info(2, "Ajout d'un vecteur");
      AddVector(CurrentNode->bag->list, v);
      CurrentNode->bag->sizeofbag++;
    }
 
  if(CurrentNode->bag->sizeofbag >= _limit_of_bag)
    {
      info(2, "Découpage d'un vecteur");
      SplitNode(CurrentNode);
    }

  return 0;
}

void CKdtree::AddVector(KdVectorList *l, KdVector *v)
{
  KdVectorItemList *itemlist = NewKdVectorItemList(v, NULL, NULL);

  if(l->head == NULL)
    {
      l->head = itemlist;
      l->tail = itemlist;

      //for(int i=0;i<v->size;i++)
//	if(v->value[i] > _maxValues->value[i])
	//  _maxValues->value[i] = v->value[i];

    }
  else
    {
      l->tail->next = itemlist;
      l->tail = itemlist;

      // vérification si un vecteur à une valeur supérieur
      //for(int i=0;i<v->size;i++)
	//if(v->value[i] > _maxValues->value[i])
	 // _maxValues->value[i] = v->value[i];

    }
  l->size++;
}

void CKdtree::AddVectorSortByDist(KdVectorList *l, KdVector *v)
{
  KdVectorItemList *itemlist = NewKdVectorItemList(v, NULL, NULL);
  KdVectorItemList *cur = l->head;
  if(l->head == NULL)
    {
      l->head = itemlist;
      l->tail = itemlist;
    }
  else
    {
      while(cur->next != NULL && v->dist > cur->next->v->dist)
	cur = cur->next;

      itemlist->next = cur->next;
      cur->next = itemlist;
    }
  l->size++;
}

int CKdtree::CompareVector(KdVector *v1, KdVector *v2)
{
  int i;
  
  if(v1->size != v2->size)
    {
      return 0;
    }
  for(i=0;i<v1->size;i++)
    {
      if(v1->value[i] != v2->value[i])
	{
	  return 0;
	}
    }
  return 1;
}

double CKdtree::GetVectorDistance(KdVector *v1, KdVector *v2, KdVector *weight)
{
  double res = 0;
  int i = 0;
  if(v1->size != v2->size)
    return 0;
  
  if(weight != NULL)
    {
      if(weight->size == v1->size)
	{
	  for(i=0;i<v1->size;i++)
	    {
	      res += weight->value[i]*weight->value[i]*(v1->value[i]-v2->value[i])*(v1->value[i]-v2->value[i]);
	    }
	  res = sqrt(res);
	  return res;
	}
    }
  for(i=0;i<v1->size;i++)
    {
      res += (v1->value[i]-v2->value[i])*(v1->value[i]-v2->value[i]);
    }
  res = sqrt(res);
  return res;
}

void CKdtree::GrabValues(KdNode *n, KdVector *target, double h, KdVectorList *l)
{
  KdVectorItemList *itemlist;
  if(!n) return ;
  if(n->leaf)
    {
      itemlist = n->bag->list->head;
      while(itemlist != NULL)//&& l->size < nb_values)
	{
	  itemlist->v->dist = GetVectorDistance(itemlist->v, target, _weight);

	//  info(2, "GrabValues, %f %f",
	  //     itemlist->v->value[itemlist->v->size -1],
	    //   target->value[target->size -1]);

	  if((itemlist->v->value[itemlist->v->size -1] == target->value[target->size -1]))
	    {
	      if( exp(-(( itemlist->v->dist /h)*( itemlist->v->dist/h))) > 0.4)
		{
		  // add vector sort by dist
		  AddVectorSortByDist(l, itemlist->v);
		}
	    }
	  itemlist = itemlist->next;
	}
    }
  else
    {
      if( target->value[n->index_dim] > n->value )
	{
	  GrabValues( n->ChildRight, target, h, l);
	//  if(l->size < nb_values)
	    GrabValues( n->ChildLeft, target, h, l);
	}
      else
	{
	  GrabValues( n->ChildLeft, target, h, l);
	  //if(l->size < nb_values)
	    GrabValues( n->ChildRight, target, h, l);
	}
    }
}

double CKdtree::KdFindClosestInTree( KdVector *target, double &qvalue, KdVector *w)
{
	int i;
	KdVector *hyperRectMin, *hyperRectMax;

	// set up limits of hyperRect
	hyperRectMin = NewKdVector( target->size );
	hyperRectMax = NewKdVector( target->size );
	for( i = 0; i< target->size; i++ ) 
	{
		hyperRectMin->value[i] = -1000000.0;
		hyperRectMax->value[i] =  1000000.0;
	}

	return KdFindClosestInNode( _head, target, qvalue,
				    hyperRectMin, hyperRectMax, 1000000.0 , w);  
}


///////// PUBLIC NEW MEMORY ALLOCATION ///////////////

KdVector *CKdtree::NewKdVector(int size)
{
  KdVector *vector = new KdVector;
  vector->value = new double[size];
  vector->size = size;
  vector->key = 0.0;
  return vector;
}

KdVectorList *CKdtree::NewKdVectorList(void)
{
  KdVectorList *list = new KdVectorList;
  list->head = NULL;
  list->tail = NULL;
  list->size = 0;
  return list;
}

//////////// PUBLIC PRINT METHODS ///////////////

void CKdtree::Display(void)
{
  Display(_head);
}

void  CKdtree::Display(KdNode *n)
{
  if(n->leaf)
    {
      printf("Feuille\n");
      Display(n->bag);
    }
  else
    {
      printf("Feuille Gauche\n");
      Display(n->ChildLeft);
      printf("Feuille Droite\n");
      Display(n->ChildRight);
    }
}

void  CKdtree::Display(KdBag *b)
{
  Display(b->list);
}

void  CKdtree::Display(KdVector *v)
{
  for(int i=0;i<v->size;i++)
    {
      printf("%f ", v->value[i]);
    }
  printf("key : %f (%6.4f/%6.4f)\n",v->key, v->dist, v->weight);
}

void  CKdtree::Display(KdVectorList *l)
{
  KdVectorItemList *itemlist = l->head;
  while(itemlist != NULL)
    {
      Display(itemlist->v);
      itemlist = itemlist->next;
    }
}

///////// PRIVATE FREE MEMORY ///////////////
void CKdtree::FreeMem(void)
{
  if(_weight != NULL)
    FreeMem(_weight);
//  FreeMem(_maxValues);
  FreeMem(_head);
}

void CKdtree::FreeMem(KdNode *n)
{
  if(n == NULL) return ;
  if(n->leaf)
    { // si c'est une feuille
    //  if(n->bag != NULL)
	{
	  FreeMem(n->bag->list->head);
	  delete n->bag->list;
	  delete n->bag;
	}
    }
  else
    {
      FreeMem(n->ChildLeft);
      FreeMem(n->ChildRight);
    }
  delete n;
}

void CKdtree::FreeMemWithoutVector(KdVectorItemList *il)
{
  if(il != NULL)
    {
      FreeMemWithoutVector(il->next);
      delete il;
    }
}

void CKdtree::FreeMem(KdVectorList *l)
{
  FreeMem(l->head);
}

void CKdtree::FreeMem(KdVectorItemList *il)
{
  if(il != NULL)
    {
      FreeMem(il->v);
      FreeMem(il->next);
      delete il;
    }
}

void CKdtree::FreeMem(KdVector *v)
{
  delete v;
}

///////// PRIVATE NEW MEMORY ALLOCATION ///////////////

KdNode *CKdtree::NewKdNode(void)
{
  KdNode *node = new KdNode;
  node->ChildLeft = NULL;
  node->ChildRight = NULL;
  node->index_dim = 0;
  node->value = 0;
  node->leaf = 1;
  node->bag = NewKdBag();
  return node;
}

KdBag *CKdtree::NewKdBag(void)
{
  KdBag *bag = new KdBag;
  bag->sizeofbag = 0;
  bag->list = NewKdVectorList();
  return bag;
}

KdVectorItemList *CKdtree::NewKdVectorItemList(KdVector *v, KdVectorItemList *next, KdVectorItemList *back)
{
  KdVectorItemList *itemlist = new KdVectorItemList;
  itemlist->v = v;
  itemlist->back = back;
  itemlist->next = next;
  return itemlist;
}


/////////// PRIVATE KDTREE HANDLING ///////////////

int CKdtree::LookUpVector(KdVectorList *l, KdVector *v)
{
  KdVectorItemList *itemlist = l->head;
  while(itemlist != NULL)
    {
      if(CompareVector(itemlist->v, v) == 1)
	return 1;
      itemlist = itemlist->next;
    }
  return 0;
}

void CKdtree::SplitNode(KdNode *n)
{
  KdVectorItemList *itemlist = n->bag->list->head;
  KdVectorItemList *p = NULL;

  n->index_dim = GetMoreRependDim(n->bag, _dim);
  n->value = moy_of_dim(n->bag, n->index_dim);
  n->ChildLeft = NewKdNode();
  n->ChildRight = NewKdNode();

  while(itemlist != NULL)
    {
      if(itemlist->v->value[n->index_dim] <= n->value)
	{
	  AddVector(n->ChildLeft->bag->list, itemlist->v);
	  n->ChildLeft->bag->sizeofbag++;
	}
      else
	{
	  AddVector(n->ChildRight->bag->list, itemlist->v);
	  n->ChildRight->bag->sizeofbag++;
	}
      p = itemlist;
      itemlist = itemlist->next;
      delete p;
    }
 
//  FreeMemWithoutVector(n->bag->list->head);
  delete n->bag->list;
  delete n->bag;
  n->bag = NULL;
  n->leaf = 0;
}

int CKdtree::GetMoreRependDim(KdBag *b, int nbdim)
{
  int i, res;
  double interval_dim[_dim];
  i = res = 0;
  while(i < _dim)
    {
      if(_weight == NULL)
	interval_dim[i] = ecar_of_dim(b, i, 1.0);
      else
	{
	  interval_dim[i] = ecar_of_dim(b, i, _weight->value[i]);
	}

      if(interval_dim[i] > interval_dim[res])
	res = i;
      i++;
    }
//delete interval_dim;
  return res;
}

double CKdtree::min_of_dim(KdBag *b, int dim)
{
  double res;
  KdVectorItemList *itemlist = b->list->head;

  res = itemlist->v->value[dim];
  do
    {
      if(itemlist->v->value[dim] < res)
	res = itemlist->v->value[dim];
      itemlist = itemlist->next;
    } while(itemlist != NULL);
  return res;
}

double CKdtree::max_of_dim(KdBag *b,int dim)
{
  double res;
  KdVectorItemList *itemlist = b->list->head;

  res = itemlist->v->value[dim];
  do
    {
      if(itemlist->v->value[dim] > res)
	res = itemlist->v->value[dim];
      itemlist = itemlist->next;
    } while(itemlist != NULL);
  return res;
}

double CKdtree::moy_of_dim(KdBag *b,int dim)
{
  double res=0;
  KdVectorItemList *itemlist = b->list->head;

  res = itemlist->v->value[dim];
  do
    {
      res += itemlist->v->value[dim];
      itemlist = itemlist->next;
    } while(itemlist != NULL);
  
  return (res/b->sizeofbag);
}

double CKdtree::ecar_of_dim(KdBag *b,int dim,double w)
{
  double res=0;
  double m;
  KdVectorItemList *itemlist = b->list->head;
  res = (itemlist->v->value[dim])*(itemlist->v->value[dim]);
  itemlist = itemlist->next;
  while(itemlist != NULL)
    {
       res = (itemlist->v->value[dim])*(itemlist->v->value[dim]);
       itemlist = itemlist->next;
    }
  m = moy_of_dim(b, dim);
  res = sqrt(w*w*(((res)/b->sizeofbag)-m*m));
  return res;
}

int CKdtree::GetSizeOfKdNode(KdNode *n)
{
  if(n->leaf)
    return n->bag->list->size;
  else
    return GetSizeOfKdNode(n->ChildLeft) + GetSizeOfKdNode(n->ChildRight);
}

double CKdtree::KdFindClosestInNode(KdNode *node, KdVector *target, double &qvalue,
		       KdVector *hyperRectMin, KdVector *hyperRectMax,
				    double maxDist, KdVector *w)
{
  double thisMin, thisMax;
  double otherMin, otherMax;
  KdNode *other;
  double distance = 0, tmpDistance, distSplit;
  KdVector *pivot;
  int i;
  KdVectorItemList *listV;
  
  if( node->leaf ) 
    {
      
      distance = maxDist;
      // look through all the points
      listV = node->bag->list->head;
      while(listV != NULL ) 
	{
	  tmpDistance = GetVectorDistance( target, listV->v, w);
	  if( tmpDistance < distance ) 
	    {
	      distance = tmpDistance;
	      // JBE				closest = listV->v;
	      //printf("toto  %f ", listV->v->key);
	      qvalue = listV->v->key;
	    }
	  listV = listV->next;
	}
      return distance;
    }
  else 
    {
      // store current hyperRect
      thisMin = hyperRectMin->value[node->index_dim];
      thisMax = hyperRectMax->value[node->index_dim];
      
      // in LeftChild
      if( target->value[node->index_dim] <= node->value )
		{
		  other = node->ChildRight;
		  // store values of this and other hyperRect for the dimension of split
		  
		  otherMin = node->value;
			otherMax = hyperRectMax->value[node->index_dim];
			// recursive call
			hyperRectMax->value[node->index_dim] = node->value;
			distance = KdFindClosestInNode( node->ChildLeft, target, qvalue,
							hyperRectMin, hyperRectMax, maxDist, w );
		}
      else
	{ // in RightChild
	  other = node->ChildLeft;
	  // store values of this and other hyperRect for the dimension of split
	  otherMin = hyperRectMin->value[node->index_dim];
	  otherMax = node->value;
	  thisMin = node->value;
	  thisMax = hyperRectMax->value[node->index_dim];
	  // recursive call
	  hyperRectMin->value[node->index_dim] = node->value;
	  distance = KdFindClosestInNode( node->ChildRight, target, qvalue,
					  hyperRectMin, hyperRectMax, maxDist, w );
	}
      // check if other hyperRect could contain a better point
      // i.e. some parts of hyperRect lies closer of closest.
      //
      // new hyperRect
      hyperRectMin->value[node->index_dim] = otherMin;
      hyperRectMax->value[node->index_dim] = otherMax;
      // closest point on split line
      pivot = NewKdVector( target->size );
      for( i = 0; i < pivot->size; i++ ) 
		{
		  if( target->value[i] <= hyperRectMin->value[i] ) 
		    {
		      pivot->value[i] = hyperRectMin->value[i];
		 	}
		  else if( target->value[i] < hyperRectMax->value[i] ) 
		    {
		      pivot->value[i] = target->value[i];
		    }
		  else 
		    {
		      pivot->value[i] = hyperRectMax->value[i];
		    }
		}
      
      // split is close enough ?
      distSplit = GetVectorDistance( target, pivot, w);
      if( distSplit < distance ) 
		{
		  distance = KdFindClosestInNode( other, target, qvalue,
						  hyperRectMin, hyperRectMax, distance, w);
		}
      
      // restore hyperRect
      hyperRectMin->value[node->index_dim] = otherMin;
      hyperRectMax->value[node->index_dim] = otherMax;
      
      // clean up
		FreeMem( pivot );
		
		return distance;  
    }
  return 0;
}


///////// PRIVATE STORE METHOD ///////////////
void CKdtree::Store(FILE *pf, KdNode *n)
{
  if(n->leaf)
    {
      Store(pf, n->bag);
    }
  else
    {
      Store(pf, n->ChildLeft);
      Store(pf, n->ChildRight);
    }
}

void CKdtree::Store(FILE *pf, KdBag *b)
{
	 Store(pf, b->list);
}

void CKdtree::Store(FILE *pf, KdVector *v)
{
  for(int i=0;i<3;i++)
    {
      fprintf(pf, "%4.4f ", v->value[i]);
    }
  fprintf(pf, "%4.4f", v->key);
  fprintf(pf, "\n");
}

void CKdtree::Store(FILE *pf, KdVectorList *l)
{
  KdVectorItemList *itemlist = l->head;
  while(itemlist != NULL)
    {
      Store(pf, itemlist->v);
      itemlist = itemlist->next;
    }
}

void CKdtree::info(char v, const char *format, ...)
{
  if(v <= _verbosity)
    {
      if (format)
	{
	  va_list pa;
	  
	  va_start (pa, format);
	  vprintf (format, pa);
	  printf("\n");
	  fflush(stdout);
	}
    }
}

