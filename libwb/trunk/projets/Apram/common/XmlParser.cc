#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "XmlParser.h"

using namespace std;

CXmlParser::CXmlParser()
{

}

CXmlParser::~CXmlParser()
{
	for(unsigned int i=0;i<m_XmlDocument.size();i++)
		free(m_XmlDocument[i]);
}

/**
	\brief	Affiche un message d'erreur
	\param	pcMessage buffer d'affichage, fonctionne comme un printf
	exemple : Error("Test valeur : %d", iValue);
*/
void CXmlParser::Error(const char *pcMessage, ...)
{
	char	pcBuffer[150];
	va_list argptr;
	va_start(argptr, pcMessage);
	vsprintf(pcBuffer, pcMessage, argptr);
	cout<<pcBuffer<<endl;
}

/**
	\brief	Ajoute un tag au tag parent
	\param	parent tag parent
	\param	pcName nom du tag
	\param	pcValue valeur du tag
	\return le tag cr?r

	le tag root est NULL
*/
xmlTag *CXmlParser::AddTag(xmlTag *parent, char *pcName, char *pcValue)
{
	// initialize memory
	LPxmlTag node = new xmlTag;

	// set name
	node->pcName = (char *)malloc(sizeof(char) * strlen(pcName)+1);
	memset(node->pcName, 0, strlen(pcName)+1);
	strncpy(node->pcName, pcName, strlen(pcName));

	// set value
	node->pcValue = (char *)malloc(sizeof(char) * strlen(pcValue)+1);
	memset(node->pcValue, 0, strlen(pcValue)+1);
	strncpy(node->pcValue, pcValue, strlen(pcValue));

	// set parent tag
	node->parent = parent;

	// add tag in the tree memory
	m_XmlDocument.push_back( node );

	// return this entry
	return m_XmlDocument[m_XmlDocument.size()-1];
}

/**
	\brief modification d'un tag existant
	\param	indice indice du tag ?traiter
	\param parent tag parent
	\param	pcName nouveau nom du  tag
	\param	pcValue nouvelle valeur du tag
*/
xmlTag *CXmlParser::SetTag(int indice, xmlTag *parent, char *pcName, char *pcValue)
{
	// initialize memory
	//LPxmlTag node = new xmlTag; // Unused Variable

	// set name
	free(m_XmlDocument[indice]->pcName);
	m_XmlDocument[indice]->pcName = (char *)malloc(sizeof(char) * strlen(pcName)+1);
	memset(m_XmlDocument[indice]->pcName, 0, strlen(pcName)+1);
	strncpy(m_XmlDocument[indice]->pcName, pcName, strlen(pcName));

	// set value
	free(m_XmlDocument[indice]->pcValue);
	m_XmlDocument[indice]->pcValue = (char *)malloc(sizeof(char) * strlen(pcValue)+1);
	memset(m_XmlDocument[indice]->pcValue, 0, strlen(pcValue)+1);
	strncpy(m_XmlDocument[indice]->pcValue, pcValue, strlen(pcValue));

	// set parent tag
	m_XmlDocument[indice]->parent = parent;

	// return this entry
	return m_XmlDocument[indice];
}

/**
	\brief	supression d'un tag
	\param	lpData donn? brut ascii
	\param	lpTagStart d?ut de tag
	\param	lpTagEnd	fin de tag
*/
void CXmlParser::RemoveTag(char * lpData, const char * lpTagStart, const char * lpTagEnd)
{
	char *	string_found;
	long	start_tag, end_tag;
	while(true)
	{
		// search start tag
		string_found = strstr(lpData, lpTagStart);
		if(string_found == NULL)
			return ;
		start_tag = string_found - lpData;

		// search end tag
		string_found = strstr(lpData, lpTagEnd);
		if(string_found == NULL)
			return ;
		end_tag = string_found - lpData + strlen(lpTagEnd);

		// remove string
		if(end_tag > 0)
		{
			if(start_tag < end_tag)
				for(int i=start_tag;i<end_tag;i++)
					lpData[i] = ' ';
		}
	}
}

/**
	\brief	ajout d'un attribut ?un tag
	\param	tag tag concern?
	\param	pcName nom de l'attribut ?ajouter
	\param	pcValue valeur de l'attribut ?ajouter
*/
xmlAttr * CXmlParser::AddAttr(xmlTag *tag, char *pcName, char *pcValue)
{
	// initialize memory
	LPxmlAttr attr = new xmlAttr;

	// set name
	attr->pcName = (char *)malloc(sizeof(char) * strlen(pcName)+1);
	memset(attr->pcName, 0, strlen(pcName)+1);
	strncpy(attr->pcName, pcName, strlen(pcName));

	// set value
	attr->pcValue = (char *)malloc(sizeof(char) * strlen(pcValue)+1);
	memset(attr->pcValue, 0, strlen(pcValue)+1);
	strncpy(attr->pcValue, pcValue, strlen(pcValue));

	// add to list
	tag->Attr.push_back( attr );

	return tag->Attr[tag->Attr.size()-1];
}

/**
	\brief ?rit le tag de d?ut du tag concern?
	\param pf	fichier ??rire
	\param TagSel tag selectionn?
	\param sp nombre d'espace ?ajouter avant le tag (lisibilit?du fichier)
*/
void CXmlParser::printStartTag(FILE *pf, xmlTag *TagSel)
{
  char		tcLine[100];
  sprintf(tcLine, "\n<%s", TagSel->pcName);
  fwrite(tcLine, sizeof(char), strlen(tcLine), pf);
  for(unsigned int i=0;i<TagSel->Attr.size();i++)
    {
      sprintf(tcLine, " %s=\"%s\"", TagSel->Attr[i]->pcName, TagSel->Attr[i]->pcValue);
      fwrite(tcLine, sizeof(char), strlen(tcLine), pf);
    }
  sprintf(tcLine, ">"); //, TagSel->pcName); <-- End of Tags: no name to write
  fwrite(tcLine, sizeof(char), strlen(tcLine), pf);
}

/**
	\brief	?rit le tag de fin du tag s?ectionner
	\param pf fichier dans le lequel on va ?rire
	\param TagSel tag s?ectionner
	\param sp	espace ?ajouter avant le tag
*/
void CXmlParser::printEndTag(FILE *pf, xmlTag *TagSel)
{
  char		tcLine[200];
  memset(tcLine, 0, 200);
  if(strlen(TagSel->pcValue) > 0)
    {
      sprintf(tcLine, "%s</%s>", TagSel->pcValue, TagSel->pcName);
    }
  else
    sprintf(tcLine, "\n</%s>", TagSel->pcName);
  fwrite(tcLine, sizeof(char), strlen(tcLine), pf);
}


void CXmlParser::WriteToAutoCall(FILE *pf, xmlTag *parent)
{
	for(unsigned int i=0;i<m_XmlDocument.size();i++)
	{
		if(m_XmlDocument[i]->parent == parent)
		{
			printStartTag(pf, m_XmlDocument[i]);
			WriteToAutoCall(pf, m_XmlDocument[i]);
			printEndTag(pf, m_XmlDocument[i]);
		}
	}
}

/**
	\brief	?rit le fichier xml
	\param pcFileName nom du fichier ou l'on va ?rire les donn?s xml
	\warining fonction ?r?crire ne g?e que 5 niveaux de xml
*/
void CXmlParser::WriteTo(char *pcFileName)
{
	char		tcLine[100];
	FILE		*pf;
	//xmlTag		*parentTag = NULL;
	//xmlTag		*child1 = NULL;
	//xmlTag		*child2 = NULL;
	//xmlTag		*child3 = NULL;
	//xmlTag		*temp = NULL;

	pf = fopen(pcFileName, "w");
	if(pf != NULL)
	{
		sprintf(tcLine, "<?xml version=\"1.0\"?>");
		fwrite(tcLine, sizeof(char), strlen(tcLine), pf);

		WriteToAutoCall(pf, NULL);

		fclose(pf);
	}
}

/**
	\brief	va rechercher le prochain tag xml
	\param	child tag de d?art
*/
xmlTag *CXmlParser::NextChild(xmlTag *child)
{
	bool b = false;
	if(child == NULL) return NULL;
	// on r?up?e le tag parent
	xmlTag *parent = child->parent;


	for(unsigned int i=0;i<m_XmlDocument.size();i++)
	{ // parcours de tous les tags
		if(m_XmlDocument[i]->parent != NULL)
		{ // si le tag existe
			if(m_XmlDocument[i]->parent == parent)
			{ // si le tag parent correspond ?notre tag parent
				if(b == true)
				{ // si le tag passer en param?re ???trouv? alors c'est le bon tag
					m_TagSelected = m_XmlDocument[i];
					return m_XmlDocument[i];
				}
				if(m_XmlDocument[i] == child)
				{ // si le tag correspond au tag du param?re
					b = true;
				}
			}
		}
	}

	// on a rien trouv?
	return NULL;
}

/**
	\brief	r?up? le premier tag enfant
	\param	parent tag parent
*/
xmlTag *CXmlParser::GetChild(xmlTag *parent)
{
	// test param?re
	if(parent==NULL)
		parent = m_TagSelected;

	for(unsigned int i=0;i<m_XmlDocument.size();i++)
	{ // parcours de la structure
		if(m_XmlDocument[i]->parent != NULL)
		{ // tag non null
			if(m_XmlDocument[i]->parent == parent)
			{ // tag parent trouv?: ok !
				m_TagSelected = m_XmlDocument[i];
				return m_XmlDocument[i];
			}
		}
	}
	// on a rien trouv?
	return NULL;
}

/**
	\brief	recherche d'un noeud xml
	\param	pcAway chemin du tag ?trouv?
*/
xmlTag *CXmlParser::Search(char *pcAway)
{
	for(unsigned int i=0;i<m_XmlDocument.size();i++)
	{
		if(strcmp(m_XmlDocument[i]->pcName, pcAway) == 0)
			return m_XmlDocument[i];
	}
	return NULL;
}


/**
	This function read the xml structure
	@param pcFileName name of xml file
*/
bool CXmlParser::Initialize(char *pcFileName)
{
	xmlTag				*currentTag;
	xmlTag				*parentTag;
	xmlTag				pXml;	// current xml tag
	xmlAttr				pAttribut; // current attributs
	FILE				*hFile;	// handle xml file
	unsigned long		ulLengthFile; // size of xml file
	char *				pcXmlData;
	char				tcDummy[MAX_LEN];
	int					i;
	bool				bCloseTag = false;


	///////////////////////////////////////////////////////////////
	//
	// Récupération du contenu du fichier ascii passer en paramêtre
	//
	//////////////////////////////////////////////////////////////
	// open file
	hFile = fopen(pcFileName, "r");
	if(!hFile) {return false;}
	fseek(hFile, 0, SEEK_END);
	ulLengthFile = ftell(hFile);
	fseek(hFile, 0, SEEK_SET);
	pcXmlData = new char[sizeof(char)*(ulLengthFile+1)];
	memset(pcXmlData, 0, ulLengthFile+1);
	fread(pcXmlData, sizeof(char), ulLengthFile, hFile);
	fclose(hFile);

	///////////////////////////////////////////////////////////////
	//
	// Suppression des tags non g??: commentaires et infos xml
	//
	//////////////////////////////////////////////////////////////
	// remove xml info
	this->RemoveTag(pcXmlData, "<?", "?>");
	// remove comments
	this->RemoveTag(pcXmlData, "<!--", "-->");

	///////////////////////////////////////////////////////////////
	//
	// tant qu'il y a des donnÃ©es Ã  traiter
	//
	//////////////////////////////////////////////////////////////
	currentTag = NULL;
	while(pcXmlData && *pcXmlData)
	{
		memset(tcDummy, 0, MAX_LEN);

		// on d?lace le curseur jusqu'?la prochaine cha?e
		SKIP_SPACE(pcXmlData);
		if(*pcXmlData == '<')
		{ // a tag

			////////////////////////////
			//
			//	TAG NAME AND ATTRIBUTS
			//
			////////////////////////////

			pcXmlData++;

			if(*pcXmlData == '/')
			{ // close tag
				pcXmlData++;
				COPY_STRING(pcXmlData, tcDummy);
				for(int l=m_XmlDocument.size()-1;l>=0;l--)
				{
					if(strcmp(tcDummy, m_XmlDocument[l]->pcName) == 0)
					{
						m_XmlDocument[l]->bTagCompleted = true;
						break;
					}
				}
			}
			else
			{
				// get tag name and save ...
				COPY_STRING(pcXmlData, tcDummy);
				pXml.pcName = (char *)malloc(sizeof(char)*(strlen(tcDummy)+1));
				memset(pXml.pcName, 0, strlen(tcDummy)+1);
				strcpy(pXml.pcName, tcDummy);

				for(int k=m_XmlDocument.size()-1;k>=0;k--)
				{
					if(m_XmlDocument[k]->bTagCompleted == false)
					{
						currentTag = m_XmlDocument[k];
						break;
					}
				}

				// ajout du tag
				parentTag = AddTag(currentTag, pXml.pcName, (char *)"");
				// fin de tag non trouv?
				parentTag->bTagCompleted = false;


				SKIP_SPACE(pcXmlData);
				if(*pcXmlData == '/')
				{
					// fin de tag trouv?
					parentTag->bTagCompleted = true;
					pcXmlData++;
				}

				if(*pcXmlData != '>')
				{ // attention attribut dans le tag !
					bCloseTag = false;
					// read attributs of tag
					while(*pcXmlData != '>' && *pcXmlData && pcXmlData && !bCloseTag)
					{
						SKIP_SPACE(pcXmlData);
						// attribut ...
						memset(tcDummy, 0, MAX_LEN);
						COPY_STRING(pcXmlData, tcDummy);
						pAttribut.pcName = (char *)malloc(sizeof(char)*(strlen(tcDummy)+1));
						memset(pAttribut.pcName, 0, strlen(tcDummy)+1);
						strcpy(pAttribut.pcName, tcDummy);

						SKIP_SPACE(pcXmlData);
						if(*pcXmlData == '=')
						{
							// value of attribut
							pcXmlData++;
							if(*pcXmlData == '\"')
							{
								pcXmlData++;
								memset(tcDummy, 0, MAX_LEN);
								SKIP_SPACE(pcXmlData);
								COPY_STRING2(pcXmlData, tcDummy);
								pAttribut.pcValue = (char *)malloc(sizeof(char)*(strlen(tcDummy)+1));
								memset(pAttribut.pcValue, 0, strlen(tcDummy)+1);
								strcpy(pAttribut.pcValue, tcDummy);
								if(*pcXmlData == '\"')
								{
									pcXmlData++;
									// ajout d'un attribut
									AddAttr(parentTag, pAttribut.pcName, pAttribut.pcValue);
								}
								else
								{
									this->Error("(1) Error format tag<%s> attribut<%s> -> \"%c\" ",
										pXml.pcName,
										pAttribut.pcName,
										*pcXmlData);
									//return false;
								}
							}
							else
							{
								this->Error("(2) Error format tag<%s> attribut<%s=?> -> \"%c\" ",
									pXml.pcName,
									pAttribut.pcName,
									*pcXmlData);
								//return false;
							}
						}
						else
						{
							this->Error("(3) Error format tag<%s> attribut<%s> -> \"%c\" ",
									pXml.pcName,
									pAttribut.pcName,
									*pcXmlData);
							//return false;
						}

					}
				}
				pcXmlData++;


				////////////////////////////
				//
				//	TAG VALUE
				//
				////////////////////////////
				memset(tcDummy, 0, MAX_LEN);
				SKIP_SPACE_VALUE(pcXmlData);
				if(*pcXmlData != '<' && *pcXmlData != 0xA && *pcXmlData != 0x0D)
				{ // value
					COPY_STRING_VALUE(pcXmlData, tcDummy);
					pXml.pcValue = (char *)malloc(sizeof(char)*(strlen(tcDummy)+1));
					memset(pXml.pcValue, 0, strlen(tcDummy)+1);
					strcpy(pXml.pcValue, tcDummy);
					parentTag = SetTag(m_XmlDocument.size()-1, currentTag, pXml.pcName, pXml.pcValue);
					if(currentTag == NULL)
						currentTag = parentTag;
				}
			}
		}
		else
			pcXmlData++;
	}

	return true;
}


void	CXmlParser::Clear(void)
{
	m_XmlDocument.clear();
}
