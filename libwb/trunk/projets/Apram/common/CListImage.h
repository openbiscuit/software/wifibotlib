/** 
 * \file
 * \brief stack d'image
 */
#ifndef CLISTIMAGE_H
#define CLISTIMAGE_H

#include "Singleton.h"
#include "List.h"

typedef struct
{
  unsigned char *data;
  unsigned int width;
  unsigned int height;
  unsigned int size;
} CListImage_picture;

class CListImage : public CList, public Singleton<CListImage>
{
 public:
  /**
   * constructeur
   */
  CListImage(void);
  /**
   * déstructeur
   */
  ~CListImage(void);
  
  /**
   * ajoute une image à la liste en allouant la mémoire
   * @param img donnée de l'image
   * @param w largeur de l'image
   * @param h hauteur de l'image
   */
  void add_picture(unsigned char *img, unsigned int w, unsigned int h);
  
  /**
   * ajoute une image à la liste en allouant la mémoire
   * @param img donnée de l'image
   * @param size taille de l'image
   */
  void add_picture(unsigned char *img, unsigned int size);
};


#endif
