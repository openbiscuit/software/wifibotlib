#ifndef cimage_h
#define cimage_h

/**
 * \file
 * @brief toolbox pour traitement des images dans le projet wifibot
 * @author Bechu Jerome
 */

#include <jpeglib.h>

#define OUTBUFFER_SIZE 0xC350


/**
 * type de fichier supporté
 */
typedef enum
  {
    CIT_BMP,
    CIT_PPM,
    CIT_JPG
  } CImage_Type;

/**
 * structure d'une bitmap (image header)
 */
typedef struct
{
    unsigned long header_size;
    long width;
    long height;
    unsigned short r1;
    unsigned short depth;
    unsigned long r2;
    unsigned long size;
    long r3,r4;
    unsigned long r5,r6;
} CImage_BMP_ImageHeader;
 

/**
 * Header du fichier bitmap
 */
typedef struct tagpejaxx_BmpFileHeader
{
    unsigned short type;
    unsigned long size;
    unsigned short r1, r2;
    unsigned long data_offset;
} CImage_BMP_FileHeader;


/**
 * structure pour la convertion d'un jpeg en bmp
 */
typedef struct {
  struct jpeg_source_mgr pub; /* public fields */
  JOCTET * buffer;	/* Start of buffer */
  int nb_bytes;
  boolean start_of_file;
} tab_source_mgr;

typedef tab_source_mgr * tab_src_ptr;

class CImage
{
 public:
  /**
   * constructeur
   */
  CImage(void);
  /*
   * dstructeur
   */
  ~CImage(void);
  /**
   * affecte la dimension de l'image
   * @param w largeur de l'image
   * @param h hauteur de l'image
   */
  void set_size(int w, int h);
  
  /**
   * Définit la longueur des données
   * @param l longueur
   */
  void set_length(unsigned int l);
  
  /**
   * Libère la mémoire
   */
  void del_buffer(void);
  
  /**
   * affecte un buffer
   * @param buffer donnes  stocker
   * @param type type d'image renseign
   */
  void set_buffer(unsigned char *buffer, CImage_Type type);

  /**
   * affecte la qualit de sortie (jpeg)
   * @param q qualit de l'image
   */
  void set_quality(int q);
  
  /**
   * converti l'image au format spcifi (unuse)
   * @param type nouveau type d'image
   * @return true si la convertion est ok
   */
  bool convert_to(CImage_Type type);
  
  /**
   * enregistre l'image
   * @param filename chemin de l'image
   */
  bool save(char *filename);

  /**
   * ouvre une image
   * @param filename chemin de l'image
   * @param type type d'image
   */
  bool open(char *filename, CImage_Type type);
  
  /**
   * Récupère le buffer
   * @return buffer
   */
  unsigned char *get_buffer(void);
  
  /**
   * Récupère la taille du buffer
   * @return taille du buffer
   */
  unsigned int get_length(void);
  
  unsigned int _width; ///< largeur de l'image
  unsigned int _height; ///< hauteur de l'image
  unsigned char *_buffer; ///< buffer de l'image
 private:
  static void file_init_destination(j_compress_ptr cinfo);
  static boolean file_empty_output_buffer(j_compress_ptr cinfo);
  static void file_term_destination(j_compress_ptr cinfo) ;
  static void init_source(j_decompress_ptr cinfo);
  static boolean fill_input_buffer(j_decompress_ptr cinfo);
  static void skip_input_data(j_decompress_ptr cinfo, long num_bytes);
  static void jpeg_tab_src(j_decompress_ptr cinfo, JOCTET *buffer, int nbytes);
  static void term_source(j_decompress_ptr cinfo);
  
  ///< Convertion des pixels bgr en rgb
  bool bgr_to_rgb(void);
  
  ///< Convertion d'une image ppm en jpeg
  bool ppm_to_jpg(void);
  
  ///< Convertion d'une jpeg en ppm
  bool jpg_to_ppm(void);
  
  ///< sauvegarde un fichier de type ppm
  bool save_ppm(void);
  
  ///< ouverture d'un fichier de type ppm
  bool open_ppm(void);
  
  ///< sauvegarde d'un fichier de type bmp
  bool save_bmp(void);
  
  ///< ouverture d'un fichier de type bmp
  bool open_bmp(void);
  
  ///< sauvegarde d'un fichier de type jpeg
  bool save_jpg(void);
  
  ///< ouverture d'un fichier de type jpeg
  bool open_jpg(void);

  char *_filename; ///< nom du fichier
  CImage_Type _type; ///< type d'image
  int _quality; ///< qualité de l'image

  static FILE* fi; ///< handle de fichier
  static JOCTET * _jpeg_buf; ///< buffer jpeg
  static unsigned int _index_jpeg_buf; ///< taille du buffer jpeg
};

#endif
