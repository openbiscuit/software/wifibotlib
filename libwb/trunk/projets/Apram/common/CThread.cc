#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <types.h>  


#include "CThread.h"


CThread::CThread() 
{
}

CThread::~CThread(void)
{
}


int CThread::start(void * arg)
{
   _Arg = arg; // store user data
   int code = pthread_create(&_Thread, NULL, CThread::EntryPoint, this);
   return code;
}

void CThread::stop(void)
{
  pthread_cancel(_Thread);
}

void CThread::run(void)
{
  setup();
  execute();
}

void * CThread::EntryPoint(void * pthis)
{
  CThread * pt = (CThread*)pthis;
  pt->run();
  return NULL;
}

void CThread::setup(void)
{
}

void CThread::execute(void)
{
}

void CThread::wait(void)
{
  pthread_join (_Thread, NULL);
}
