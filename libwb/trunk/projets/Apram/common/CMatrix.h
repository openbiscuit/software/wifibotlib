/**
 * \file
 * \brief wrapper d'une matrice GSL
 */
#ifndef matrix_h
#define matrix_h

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

class CMatrix
{
public:
	/**
	 * création d'une matrice
	 * @param row nombre de ligne
	 * @param col nombre de colonne
	 */
  	CMatrix(int row, int col);
  	
  	/**
  	 * construction d'une matrice par copie
  	 * @param A matrice à copier
  	 */
	CMatrix(CMatrix *A);
	
	/**
	 * déstructeur
	 */
	~CMatrix(void);
	
	/**
	 * @return la matrice transposée
	 */
	CMatrix *Transpose(void);
	
	/**
	 * @return la matrice pseudo inverse
	 */
	CMatrix *PseudoInverse(void);
	
	/**
	 * multiplication de matrice
	 * @param A matrice à multiplier
	 * @return résultat de la multiplication
	 */
	CMatrix *Dot(CMatrix *A);
	
	/**
	 * normalise une dimension d'une matrice
	 * @param dimension spécifier quelle dimension
	 */
	void Normalize(int dimension);

	/**
	 * @return la valeur maximale de la diagonale
	 */
	double getMaxDiag(void);
	
	/**
	 * @return true si la matrice est vide
	 * @return false dans le cas contraire
	 */
	bool IsEmpty(void);
	
	/**
	 * affecte une valeur à un élément de la matrice
	 * @param r quelle ligne
	 * @param c quelle colonne 
	 * @param value valeur à affecter
	 */
	void Set(int r, int c, double value);
	
	/**
	 * récupère la valeur d'un élément
	 * @param r ligne
	 * @param c colonne
	 * @return double valeur de l'élément
	 */
	double Get(int r, int c);
	
	/**
	 * affiche la matrice sur la sortie standard 
	 */
	void Print(void);


	///< matrice gsl
	gsl_matrix *_M;
	
	///< nombre de lignes
	int _row;
	
	///< nombre de colonnes
	int _col;

private:
	/**
	 * multiplication d'une matrice par une autre
	 * @param A première matrice
	 * @param B seconde matrice
	 * @return le résultat de la multiplication
	 */
	gsl_matrix *Dot(gsl_matrix *A, gsl_matrix *B);
};

#endif
