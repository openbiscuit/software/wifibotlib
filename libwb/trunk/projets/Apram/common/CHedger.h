
#ifndef HEDGER_H
#define HEDGER_H

#define HEDGER_GAMMA_DEFAULT_VALUE 0.8f ///< valeur par dÃ©faut gamma
#define HEDGER_ALPHA_DEFAULT_VALUE 0.9f ///< valeur par dÃ©faut alpha
#define HEDGER_BANDWITH_DEFAULT_VALUE 1.0f ///< valeur bande passante par dÃ©faut (h)
#define HEDGER_NB_POINTS_FEUILLE 10 ///< nb de points dans une feuille de l'arbre
#define HEDGER_NB_POINTS 4 ///< nombre de points pris en compte par dÃ©faut (k)
#define HEDGER_NB_POINTS_MAX 10 ///< nombre de points maximum pour le calcul LWR
#define HEDGER_THRESHOLD 0.1f

/**
 * liste des actions possible de l'agent
 */
typedef enum
  {
    HEDGER_ACTION_NOTHING = 0, ///< ne rien faire
    HEDGER_ACTION_FORWARD, ///< avancer
    HEDGER_ACTION_BACKWARD, ///< reculer
    HEDGER_ACTION_TURNONLEFT, ///< tourner Ã  gauche
    HEDGER_ACTION_TURNONRIGHT ///< tourner Ã  droite
  } CHedger_Action ;

#define QDEF 0.0 ///< valeur qdefault pour l'algo d'aprentissage

/**
 * type de prÃ©diction
 */
enum
  {
    PRED_NORMAL = 0,
    PRED_EXACT, ///< point exact
    PRED_NOT_ENOUGH, ///< pas assez de points
    PRED_IVH, ///< pas dans la zone IVH
    PRED_BOUNDS, ///< en dehors des bornes
    PRED_EXT, ///< points extÃ©rieurs
    PRED_NULL ///< non utilisÃ©, juste pour le fin enum
  };

class CPeng;
/**
 * \file
   \brief algorithme d'apprentissage par renforcement
   \author Nicolas Beaufort
*/
class CHedger // : public Singleton<CHedger>
{
 public:
  /**
   * constructeur
   */
  CHedger(CKdtree *kdtree);
  ~CHedger(void);
  /**
   * Fonction "Hedger_Training".
   * 
   * Dans l'Ã©tat 'x', on vient de faire l'action 'ac' et de recevoir la rÃ©compense 'rewards' avant d'arriver dans l'Ã©tat 'xp'.
   * On va mettre ÃÂ  jour Q(xp,ac) et Q(voisins,ac) en utilisant une rÃ©gression
   * linÃ©aire pour trouver les valeurs de Q.
   * 
   * @param ext_states une liste d'etat externes "symboliques"
   * @param x vecteur de l'Ã©tat S(t)
   * @param xp vecteur de l'Ã©tat S(t+1)
   * @param rewards rÃ©compense au passage entre les 2 Ã©tats
   * @param ac l'action faite a l'Ã©tat S
   *
   * @return index de l'action la meilleurs dans l'Ã©tat s(t+1)
   *
   */
  int Training(KdVector *x,
	       KdVector *xp,
	       double ac);

  int BestAction(KdVector *s);

  /**
   * fonction qui retourne la rÃ©compense en fonction de distance et ange
   * @return la rÃ©compense
   */
  int Rewards(void);
  
  /**
   * Fonction "Hedger_Prediction" permettant de faire une interpolation pour connaÃÂ®tre la valeur Q d'un point proche d'autres points.
   *
   * On cherche d'abord dans la liste ext_state pour savoir si on a affaire a un etat
   * externe qui est alors traite a part.
   * Dans l'arbre contenant les points connus, on cherche la feuille contenant le points exemple. 
   * Si cette feuille ne contient pas assez de points -> renvoie QDEF.
   * Si il y a assez de points mais xq n'est pas dans l'enveloppe convexe -> QDEF
   *    sinon calcule l'approximation (LWR_PREDICTION).
   * 'status' est mis a jour et contient une etiquette precisant la condition de 
   * terminaison de la methode.
   *
   * @param S l'arbre contenant les donnÃ©es
   * @param ext_states une liste d'etat externes "symboliques"
   * @param xq vecteur contenant les coordonnÃÂ©es du point dont on veut connaÃÂ®tre la valeur q
   * @param l liste dans laquelle on va stoquer les points utilisÃÂ©s afin de les rÃÂ©cupÃÂ©rer ailleurs
   * @param mindist on va stocker la distance du point le plus proche de xq
   * @param status explique comment s'est passÃ© la prÃ©diction
   *
   * @return la valeur de l'approximation ou QDEF
   */
  double Prediction(KdVector *xq,
		    KdVectorList *l,
		    double *mindist,
		    int *status);

  /**
   * Fonction permettant de faire l'approximation.
   * A partir d'une liste 'l' de points dont on connait les qValeurs, on essaie
   * de faire une regression linÃ©aire pondÃ©rÃ©es par la distance, la fonction distance
   * Ã©tant elle mÃªme pondÃ©rÃ©e par 'w'.
   *
   * @param l liste contenant les points dont on va se servir
   * @param xq le point dont on veut approximer la valeur
   * @param w vecteur contenant les poids des dimensions
   * @param mindist on va stocker la distance du point le plus proche de xq
   *
   * @return la valeur de l'approximation
   */
  double LWR_PREDICTION(KdVectorList *l,
			KdVector *xq,
			KdVector *w,
			double *mindist);

  
  void GrabValues(KdNode *n, KdVector *target, KdVectorList *l);

  bool readTraces(char *path);

  /**
   * affecte la distance
   * @param dist nouvelle distance
   */
  void setDistance(double dist);

  /**
   * affecte l'angle
   * @param angle le nouvel angle
   */
  void setAngle(double angle);

  /**
   *
   */
  double kernel(double d, double h);

  /**
   * fonction qui retourne la chaine correspondant Ã  une action
   * @param act action Ã  convertir
   * @param txt chaine d'affectation de la valeur
   */
  static void ActionToString(double act, char *txt);


  float _alpha; ///< paramÃ¨tre alpha
  float _gamma; ///< paramÃ¨tre gamma
  int _nb_points; ///< nombre de point pris en compte
  int _nb_points_max; ///< nombre de point max pour la LWR
  int _rewards;
      
  KdVectorList *_extern_states;
      
  float _bandwith; ///< bande passante
  double seuil; ///< seuil de l'algo
  unsigned char _verbosity;
  CKdtree *S; ///< le Kd-tree
  CPeng *_peng; ///< pointeur vers la classe peng

 private:
   double _angle; ///< angle de l'agent
   double _distance; ///< distance de l'agent
   double _old_distance; ///< ancienne distance de l'agent

   void info(int v, const char *format, ...);
   KdVector *possible_actions; ///< contient la liste des actions possibles
};

#endif


