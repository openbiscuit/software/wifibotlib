import scipy
import scipy.linalg
import pylab
import matplotlib.patches
import math
from numpy import *

### INPUT ###	
pointsEntrainements = scipy.array(([98.0, 475.0, 1.0],
								[118.0, 406.0, 1.0],
								[104.0, 397.0, 1.0]))
pointsQvalues = scipy.array(([0.0],[9.0],[0.0]))
nbPoints = 3

#poids des dimensions
weights = scipy.array(([1.0],[1.0],[100.0]))

querypoint = scipy.array(([135.0],[575.0],[1.0]))

bandwith = 90
### END INPUT ###

def getVectorDistance(v1, v2, weight):
	s = v1.size
	res = 0
	for i in range(s):
		res += weight[i] * weight[i] * (v1[i]-v2[i]) * (v1[i]-v2[i])
	return math.sqrt(res)

def kernel(dist, h):
	return math.exp(-((dist/h)*(dist/h)))

distances = zeros((nbPoints))

weightsOfPoints = zeros((nbPoints))

# calcul distance
s = distances.size
for i in range(s):
	distances[i] = getVectorDistance(pointsEntrainements[i], querypoint, weights)

# calcul poids du point
s = weightsOfPoints.size
for i in range(s):
	weightsOfPoints[i] = kernel(distances[i], bandwith)

# construction matrice A
A = zeros((nbPoints, nbPoints+1))
for i in range(nbPoints):
	for j in range(nbPoints):
		A[i][j] = pointsEntrainements[i][j] * weightsOfPoints[i]
		A[j][nbPoints] = 1.0
 
Zinv = scipy.linalg.pinv2( A )

X = scipy.dot(Zinv, pointsQvalues)

s = X.size
res = 0
for i in range(querypoint.size):
	res += X[i] * querypoint[i]
res += X[i+1][0]

print "Q-value du point", querypoint[0], querypoint[1], querypoint[2], ":", res	

