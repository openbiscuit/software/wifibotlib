import scipy
import scipy.linalg
import pylab
import matplotlib.patches
import math
matP = scipy.array(([24.080135, 116.714941, 0.24517, 1.0],
                    [3.349986, 11.526224, 0.028390, 1.0],
                    [1.848074, 7.054667, 0.01770, 1.0]))
matY = scipy.array(([0],
                    [9],
                    [0]))

matX1 = scipy.array(([135.0],
				[575.0],
				[1.0]))

matZinv = scipy.linalg.pinv2( matP )
print matZinv
matX = scipy.dot(matZinv, matY)

#print matX
res = matX[0] * 135.0 + matX[1] * 575.0 + matX[2] * 1.0
res = res + matX[3]
#print res

