/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "font.h"
#include "axis.h"

// ============================================================================
Axis::Axis (void) : Object()
{
    set_label ("X axis");
    set_range (Range(-.5, .5, 5, 25));
    set_visible (true);
    set_position (-.5);
    set_size (1);
    set_fg_color (0,0,0,1);
    set_bg_color (1,1,1,.5);
    set_br_color (0,0,0,1);
    alpha_ = 1.0f;
}

// ============================================================================
Axis::~Axis (void)
{}

// ============================================================================
void
Axis::render (void)
{
    if (!get_visible())
        return;
    Object::compute_visibility();

    Font *font = Font::Font24();
    float font_scale = 1/(get_range().major*font->glyph_size().x*8);
    float d1 = 0.065;  // major tick size
    float d2 = 0.025; // minor tick size

    if (get_size().x < 0) {
        d1 = -d1;
        d2 = -d2;
    }

    glColor4f (get_fg_color().r, get_fg_color().g, get_fg_color().b, get_fg_color().a*alpha_);

    // Axis
    glLineWidth (2.0f);
    glBegin (GL_LINES);
    glVertex3f (get_position().x,          0, 0);
    glVertex3f (get_position().x+get_size().x, 0, 0);
    glEnd();

    // Major ticks
    glLineWidth (1.0f);
    glBegin (GL_LINES);
    for (int i=0; i<=get_range().major; i++) {
        float v = i/get_range().major*get_size().x;
        glVertex3f (get_position().x + v, 0, 0);
        glVertex3f (get_position().x + v, d1, 0);
    }
    glEnd();

    // Minor ticks
    glLineWidth (0.5f);
    glBegin (GL_LINES);
    for (int i=0; i<=get_range().minor; i++) {
        float v = i/get_range().minor*get_size().x;
        glVertex3f (get_position().x + v, 0, 0);
        glVertex3f (get_position().x + v, d2, 0);
    }
    glEnd();

    // Major tick labels
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glEnable (GL_TEXTURE_2D);
    char text[16];
    for (int i=0; i<=get_range().major; i++) {
        snprintf (text, 15, "%+.2f",
                  get_range().min + (get_range().max-get_range().min)*(1-i/get_range().major));
        Size s = font->size (text);
        glPushMatrix();
        if (get_size().x > 0)
            glRotatef (180,0,0,1);
        glTranslatef (get_position().x + i/get_range().major*get_size().x - s.x*font_scale/2,
                      -s.y*font_scale - fabs(d1), 0);
        glScalef (font_scale,font_scale,font_scale);
        font->render (text);
        glPopMatrix();
    }

    // Label
    font_scale = .0025;
    glPushMatrix();
    Size s = font->size (label_);
    if (get_size().x > 0)
        glRotatef (180,0,0,1);
    glTranslatef (get_position().x + get_size().x/2 - s.x*font_scale/2,
                  -2*s.y*font_scale - fabs(d1), 0);
    glScalef (font_scale,font_scale,font_scale);
    font->render (label_);
    glPopMatrix();

    glDisable (GL_TEXTURE_2D);
    
}

// ============================================================================
void
Axis::set_position (float x)
{
    position_ = Position (x, 0, 0, 0);
}


// ============================================================================
void
Axis::set_size (float x)
{
    size_ = Size (x, 0, 0, 0);
}

// ============================================================================
void
Axis::set_label (std::string label)
{
    label_ = label;
}

std::string
Axis::get_label (void)
{
    return label_;
}

// ============================================================================
void
Axis::set_range (Range range)
{
    range_ = range;
}

void
Axis::set_range (float min, float max)
{
  range_ = Range (min, max, 5, 10);
}

Range
Axis:: get_range (void)
{
    return range_;
}
