/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif
#include "object.h"

Object::Object (void)
{
    set_visible (true);
    set_size (1,1,1);
    set_position (0,0,0);
    set_fg_color (0,0,0,1);
    set_br_color (0,0,0,1);
    set_bg_color (1,1,1,1);
    alpha_ = 1.0f;
    fade_in_delay_ = 0;
    fade_out_delay_ = 0;
}

Object::~Object (void)
{}

void
Object::compute_visibility (void)
{
    // Update visibility and transparency according to current time
    struct timeval now;

    // Fade out
    if (fade_out_delay_) {
        gettimeofday (&now, NULL);
        float delay = ((now.tv_sec - fade_time_.tv_sec)*1000.0 +
                       (now.tv_usec - fade_time_.tv_usec)/1000.0);
        alpha_ = 1.0f - delay/fade_out_delay_;
        if (alpha_ < 0.0f) {
            fade_out_delay_ = 0;
            visible_ = false;
            alpha_ = 0.0f;
        }
    // Fade in
    } else if (fade_in_delay_) {
        gettimeofday (&now, NULL);
        float delay = ((now.tv_sec - fade_time_.tv_sec)*1000.0 +
                       (now.tv_usec - fade_time_.tv_usec)/1000.0);
        alpha_ = delay/fade_in_delay_;
        if (alpha_ > 1.0f) {
            fade_in_delay_ = 0;
            alpha_ = 1.0f;
        }
    }
}

void
Object::build (void)
{}

void
Object::update (void)
{}

void
Object::render (void)
{
    compute_visibility();
    if (not get_visible()) return;

    typedef struct face {
        float vertices[4*3];
        float normal[3];
        float z;
        static int compare (const void *a, const void *b) {
            const face *da = (const face *) a;
            const face *db = (const face *) b;
            return ((*da).z > (*db).z) - ((*da).z < (*db).z);
        }
    };
    face faces[6] = {
        {{-1,-1, 1,  1,-1, 1,  1, 1, 1, -1, 1, 1}, { 0, 0, 1}, 0},
        {{-1,-1,-1, -1, 1,-1,  1, 1,-1,  1,-1,-1}, { 0, 0,-1}, 0},
        {{-1, 1,-1, -1, 1, 1,  1, 1, 1,  1, 1,-1}, { 0, 1, 0}, 0},
        {{-1,-1,-1,  1,-1,-1,  1,-1, 1, -1,-1, 1}, { 0,-1, 0}, 0},
        {{ 1,-1,-1,  1, 1,-1,  1, 1, 1,  1,-1, 1}, { 1, 0, 0}, 0},
        {{-1,-1,-1, -1,-1, 1, -1, 1, 1, -1, 1,-1}, {-1, 0, 0}, 0}
    };

    /* Cube faces are z sorted for correct translucency */
    /* z sorting is done on faces center */
	float m[16];
	glGetFloatv (GL_MODELVIEW_MATRIX, m);
    for (int i=0; i<6; i++)
        faces[i].z = faces[i].normal[0]*m[2]
                  +  faces[i].normal[1]*m[6]
                  +  faces[i].normal[2]*m[10]
                  + m[14]; 
    qsort (faces, 6, sizeof (face), face::compare);
    
    glPushAttrib (GL_ENABLE_BIT);
    glDisable (GL_TEXTURE_2D);
    glDisable (GL_LIGHTING);
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);

    glPushMatrix();
    glScalef (get_size().x, get_size().y, get_size().z);
    glTranslatef (get_position().x, get_position().y, get_position().z);

    for (int i=0; i<6; i++) {
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glColor4fv (get_bg_color().data);
        glPolygonOffset (1, 1);
        glEnable (GL_POLYGON_OFFSET_FILL);
        glBegin (GL_QUADS); {
            glNormal3f (-faces[i].normal[0], -faces[i].normal[1], -faces[i].normal[2]);
            glVertex3f (faces[i].vertices[0], faces[i].vertices[1], faces[i].vertices[2]);
            glVertex3f (faces[i].vertices[3], faces[i].vertices[4], faces[i].vertices[5]);
            glVertex3f (faces[i].vertices[6], faces[i].vertices[7], faces[i].vertices[8]);
            glVertex3f (faces[i].vertices[9], faces[i].vertices[10], faces[i].vertices[11]);
        } glEnd();

        glColor4fv (get_br_color().data);
        glDisable (GL_POLYGON_OFFSET_FILL);
        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth (0.5);
        glDepthMask (GL_FALSE);
        glBegin (GL_QUADS);
            glNormal3f (-faces[i].normal[0], -faces[i].normal[1], -faces[i].normal[2]);
            glVertex3f (faces[i].vertices[0], faces[i].vertices[1], faces[i].vertices[2]);
            glVertex3f (faces[i].vertices[3], faces[i].vertices[4], faces[i].vertices[5]);
            glVertex3f (faces[i].vertices[6], faces[i].vertices[7], faces[i].vertices[8]);
            glVertex3f (faces[i].vertices[9], faces[i].vertices[10], faces[i].vertices[11]);
        glEnd();
        glDepthMask (GL_TRUE);
    }
    glPopMatrix();
    glPopAttrib();
}

bool
Object::select (int x, int y)
{
    return false;
}

bool
Object::key_press (char key, int modifier)
{
    return false;
}

bool
Object::key_release (char key, int modifier)
{
    return false;
}

bool
Object::button_press (int button, int x, int y, int modifier)
{
    return false;
}

bool
Object::button_release (int button, int x, int y, int modifier)
{
    return false;
}

bool
Object::mouse_motion (int button, int x, int y)
{
    return false;
}

void
Object::set_name (std::string name)
{
    name_ = name;
}

void
Object::rename (std::string name)
{
    set_name (name);
}

std::string
Object::get_name (void) const
{
    return name_;
}

void
Object::set_visible (bool visible)
{
    visible_ = visible;
}

bool
Object::get_visible (void) const
{
    return visible_;
}

void
Object::hide (float timeout)
{
    if (timeout) {
        gettimeofday (&fade_time_, NULL);
        fade_out_delay_ = timeout;
        fade_in_delay_ = 0;
        alpha_ = 1.0f;
    } else {
        set_visible (false);
    }
}

void
Object::show (float timeout)
{
    set_visible (true);
    alpha_ = 1.0f;
    if (timeout) {
        gettimeofday (&fade_time_, NULL);
        fade_out_delay_ = 0;
        fade_in_delay_ = timeout;
        alpha_ = 0.0f;
    }
}

void
Object::set_position (Position position)
{
    position_ = Position (position);
}

void
Object::set_position (float x, float y, float z)
{
    set_position (Position (x,y,z));
}

void
Object::reposition (Position position)
{
    set_position (position);
}

void
Object::reposition (float x, float y, float z)
{
    set_position (Position (x,y,z));
}

void
Object::move (Position position)
{
    set_position (position);
}

void
Object::move (float x, float y, float z)
{
    set_position (Position (x,y,z));
}

Position
Object::get_position (void) const
{
    return position_;
}

void
Object::set_size (Size size)
{
    size_ = Size (size);
}

void
Object::set_size (float x, float y, float z)
{
    set_size (Size (x,y,z));
}

void
Object::resize (Size size)
{
    set_size (size);
}

void
Object::resize (float x, float y, float z)
{
    set_size (Size (x,y,z));
}

Size
Object::get_size (void) const
{
    return size_;
}

void
Object::set_fg_color (Color color)
{
    fg_color_ = color;
}

void
Object::set_fg_color (float r, float g, float b, float a)
{
    set_fg_color (Color (r,g,b,a));
}

Color
Object::get_fg_color (void) const
{
    return fg_color_;
}

void
Object::set_bg_color (Color color)
{
    bg_color_ = color;
}

void
Object::set_bg_color (float r, float g, float b, float a)
{
    set_bg_color (Color (r,g,b,a));
}

Color
Object::get_bg_color (void) const
{
    return bg_color_;
}

void
Object::set_br_color (Color color)
{
    br_color_ = color;
}

void
Object::set_br_color (float r, float g, float b, float a)
{
    set_br_color (Color (r,g,b,a));
}

Color
Object::get_br_color (void) const
{
    return br_color_;
}
