/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "smooth-surface.h"

// ============================================================================
SmoothSurface::SmoothSurface (void) : Object ()
{
  data_ = 0;
  vertices_ = 0;
  normals_ = 0;
  nvertices_ = 0;
  colors_ = 0;
  indices_ = 0;
  nindices_ = 0;
  set_size (1.0f, 1.0f, 0.5f);
  set_position (0.0f, 0.0f, 0.0f);
  set_fg_color (0.0f, 0.0f, 0.0f, 1.0f);
  set_bg_color (1.0f, 1.0f, 1.0f, 1.0f);
  cmap_ = Colormap::Hot();

  set_surf_draw (true);
  set_grid_draw (true);
}

// ============================================================================
SmoothSurface::~SmoothSurface (void)
{
	delete [] vertices_;
	delete [] normals_;
	delete [] colors_;
	delete [] indices_;
}

void
SmoothSurface::reset(void)
{
  delete [] vertices_;
  delete [] normals_;
  delete [] colors_;
  delete [] indices_;
  data_ = 0;
  vertices_ = 0;
  normals_ = 0;
  nvertices_ = 0;
  colors_ = 0;
  indices_ = 0;
  nindices_ = 0;
  set_size (1.0f, 1.0f, 0.5f);
  set_position (0.0f, 0.0f, 0.0f);
  set_fg_color (0.0f, 0.0f, 0.0f, 1.0f);
  set_bg_color (1.0f, 1.0f, 1.0f, 1.0f);
  cmap_ = Colormap::Hot();

  set_surf_draw (true);
  set_grid_draw (true);
}

// ============================================================================
void
SmoothSurface::render (void)
{
    if (not data_)  return;
    if (not vertices_) build();
    if (not vertices_) return;

    if (!get_visible()) return;
    Object::compute_visibility();

    glEnable (GL_LIGHTING);
    glDisable (GL_TEXTURE_2D);


    glPushMatrix();
    glTranslatef (get_position().x, get_position().y, get_position().z);
    glScalef (get_size().x, get_size().y, get_size().z);

    glVertexPointer (3, GL_FLOAT, sizeof(Vec4f), &vertices_[0].x);
    glColorPointer (4, GL_FLOAT, sizeof(Color), &colors_[0].r);
    glNormalPointer (GL_FLOAT, sizeof(Vec4f), &normals_[0].x);

    // Surface
    if (get_surf_draw()) {
      glPolygonOffset (1.0f, 1.0f);
      glEnable (GL_POLYGON_OFFSET_FILL);
      glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
      glEnableClientState (GL_COLOR_ARRAY);
      glEnableClientState (GL_NORMAL_ARRAY);
      glEnableClientState (GL_VERTEX_ARRAY);
      glDrawElements(GL_QUADS, nindices_, GL_UNSIGNED_INT, indices_);
      glDisableClientState (GL_NORMAL_ARRAY);
      glDisableClientState (GL_COLOR_ARRAY);
      glDisableClientState (GL_VERTEX_ARRAY);

      glDisable (GL_POLYGON_OFFSET_FILL);
    }

    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    glDisable (GL_LIGHTING);

    // Grid
    if (get_grid_draw()) {
      glDepthMask (GL_FALSE);
      glLineWidth (0.5f);
      glColor4f (0.0f, 0.0f, 0.0f, 0.25f);
      glEnableClientState (GL_VERTEX_ARRAY);
      glDrawElements (GL_QUADS, nindices_, GL_UNSIGNED_INT, indices_);
      glDisableClientState (GL_VERTEX_ARRAY);
      glDepthMask (GL_TRUE);
    }
    
    // Border
    glColor4fv (get_br_color().data);
    glLineWidth (2.0f);
    glBegin (GL_QUADS);
    glVertex3f (-0.5f, -0.5f, 0.0f);
    glVertex3f ( 0.5f, -0.5f, 0.0f);
    glVertex3f ( 0.5f,  0.5f, 0.0f);
    glVertex3f (-0.5f,  0.5f, 0.0f);
    glEnd ();

    glPopMatrix();
}

// ============================================================================
void
SmoothSurface::update (void)
{
    if (not data_)  return;
    if (not vertices_) build();
    if (not vertices_) return;

    if (data_->shape().z == 4) {
        for (int y=0; y < data_->shape().y; y++) {
            for (int x=0; x < data_->shape().x; x++) {
                int i = int (y*data_->shape().x + x);
                colors_[i] = Color (data_->data()[i*4],
                                    data_->data()[i*4+1],
                                    data_->data()[i*4+2],
                                    data_->data()[i*4+3]);
                vertices_[i].dz = (data_->data()[i*4] + data_->data()[i*4+1] +data_->data()[i*4+2])/3;
            }
        }
    } else if (data_->shape().z == 3) {
        for (int y=0; y < data_->shape().y; y++) {
            for (int x=0; x < data_->shape().x; x++) {
                int i = int (y*data_->shape().x + x);
                colors_[i] = Color(data_->data()[i*3], data_->data()[i*3+1], data_->data()[i*3+2]);
                vertices_[i].dz = (data_->data()[i*3] + data_->data()[i*3+1] +data_->data()[i*3+2])/3;
            }
        }
    } else {
        for (int y=0; y < data_->shape().y; y++) {
            for (int x=0; x < data_->shape().x; x++) {
                int i = int (y*data_->shape().x + x);
                float value = data_->data()[i];
                colors_[i] = (*cmap_)(value);
                vertices_[i].dz = value;
            }
        }
    }
    
    // zeroify normals
    for (int i = 0; i<nvertices_; i++) 
        normals_[i] = Vec4f (0,0,0,0);

    for(int y = 0; y < (data_->shape().y -1); y++){
        for(int x = 0; x < (data_->shape().x -1); x++){
            Vec4f p1, p2, p3, v1, v2, tmp;
            int v[4];
            // calculate the positions int the vertex buffer of the adjacent vertices
            // v[0] is current vertex
            v[0] = int(y*data_->shape().x+x);
            v[1] = int(y*data_->shape().y+x+1);
            v[2] = int((y+1)*data_->shape().x+x);
            v[3] = int((y+1)*data_->shape().x+x+1);
            // store the positions of first triangle into 3 usable vertices
            p1 = vertices_[v[0]];
            p2 = vertices_[v[3]];
            p3 = vertices_[v[2]];
            // calculate the triangle sides
            v1 = p2 - p1;
            v2 = p3 - p1;
            // calculate normal
            tmp = v1.cross(v2);

            // add normal to the normal of all the vertices in the triangle
            normals_[v[0]] = normals_[v[0]] + tmp;
            normals_[v[3]] = normals_[v[3]] + tmp;
            normals_[v[2]] = normals_[v[2]] + tmp;
            // store the positions of second triangle into 3 usable vertices
            p1 = vertices_[v[1]];
            p2 = vertices_[v[3]];
            p3 = vertices_[v[0]];
            // calculate the triangle sides
            v1 = p2 - p1;
            v2 = p3 - p1;
            // calculate normal
            tmp = v1.cross(v2);            
            // add normal to the normal of all the vertices in the triangle
            normals_[v[1]] = normals_[v[1]] + tmp;
            normals_[v[3]] = normals_[v[3]] + tmp;
            normals_[v[0]] = normals_[v[0]] + tmp;
        }
    }

    // normalize normals
    for (int i = 0; i < nvertices_; i++)
        normals_[i].normalize();
}

// ============================================================================
void
SmoothSurface::build (void)
{
    if (not data_)         return;
    if (not data_->data()) return;

	delete [] vertices_;
	delete [] normals_;
	delete [] colors_;
	delete [] indices_;
	nvertices_ = int (data_->shape().x*data_->shape().y);
	vertices_ = new Vec4f[nvertices_];
    normals_ = new Vec4f[nvertices_];
	colors_ = new Color[nvertices_];
    nindices_ = int((data_->shape().x-1)*(data_->shape().y-1))*4;
	indices_ = new int[nindices_];
    int j = 0;
	for (int y=0; y < (data_->shape().y-1); y++) {
        for (int x=0; x < (data_->shape().x-1); x++) {
            int i = int (y*data_->shape().x + x);
            indices_[j++] = i;
            indices_[j++] = i+1;
            indices_[j++] = i+int(data_->shape().x+1);
            indices_[j++] = i+int(data_->shape().x);
        }
    }

	for (int y=0; y < data_->shape().y; y++) {
        for (int x=0; x < data_->shape().x; x++) {
            int index = int (x+y*data_->shape().x);
            vertices_[index].x  = (x/(data_->shape().x-1)) - .5;
            vertices_[index].y  = (y/(data_->shape().y-1)) - .5;
        }
    }
}

// ============================================================================
void
SmoothSurface::set_data (Data *data)
{
    data_ = data;
}

// ============================================================================
void
SmoothSurface::set_cmap (Colormap *cmap)
{
    cmap_ = cmap;
}

Colormap *
SmoothSurface::get_cmap (void)
{
    return cmap_;
}

Colormap *
SmoothSurface::cmap (void)
{
    return get_cmap();
}

// ============================================================================
void
SmoothSurface::set_surf_draw (bool visible)
{
    surf_draw_ = visible;
}

bool
SmoothSurface::get_surf_draw (void) const
{
    return surf_draw_;
}

void
SmoothSurface::set_grid_draw (bool visible)
{
    grid_draw_ = visible;
}

bool
SmoothSurface::get_grid_draw (void) const
{
    return grid_draw_;
}

