/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __VIEWPORT_H__
#define __VIEWPORT_H__

#include "widget.h"

/**
 * Indepedent scene viewport.
 *
 */
class Viewport : public Widget {
public:
    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor.
     */
    Viewport (void);

    /**
     * Destructor
     */
    virtual ~Viewport (void);
    //@}



    /**
     * @name Rendering
     */
    /**
     * Setup viewport.
     *
     */
    virtual void build (void);

    /**
     * Update viewport.
     *
     */
    virtual void update (void);

    /**
     * Render viewport.
     *
     */
    virtual void render (void);
    //@}



protected:

};

#endif
