/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FONT_H__
#define __FONT_H__
#include <string>
#include <string.h>
#include "vec4f.h"
#include "font_12.h"
#include "font_16.h"
#include "font_24.h"
#include "font_32.h"


/**
 * Font rendering.
 *
 * The font object allows to display ascii text in monotype font at different
 * size (12, 16, 24 or 32). It works by building glyphs as texture mapped quads
 * and caching them in display lists. Displaying a text is then
 * straightforward.
 *
 * <table border="0"><tr>
 * <td>@image html font.png</td>
 * <td>@image html fonts.png</td>
 * </td></table>
 */
class Font {
public:

    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor
     */
    Font (void);

    /**
     * Destructor
     */
    virtual ~Font (void);
    //@}

    /**
     * @name Rendering
     */
    /**
     * Render text.
     *
     * @param text ascii text to render
     * @param ansi whether to interpret ansi sequences (slow)
     * @param alpha Overall transparency
     */
    virtual void render (std::string text, bool ansi=false, float alpha=1.0);
    //@}

    /**
     * @name Size
     */
    /**
     * Get size of text.
     *
     * @param text ascii text to render
     * @return size of text (GL units).
     */
    virtual Size size (std::string text) const;

    /**
     * Get glyph size.
     *
     * @return glyph size (GL units).
     */
    virtual Size get_glyph_size (void) const;

    /**
     * Get glyph size.
     *
     * @return glyph size (GL units).
     */
    virtual Size glyph_size (void) const;
    //@}


    /**
     *  @name Available fonts.
     */
    /**
     * Bitstream Vera Sans Mono 12
     *
     * @return Bitstream Vera Sans Mono 12 font
     */
    static Font * Font12 (void);

    /**
     * Bitstream Vera Sans Mono 16
     *
     * @return Bitstream Vera Sans Mono 16 font
     */
    static Font * Font16 (void);

    /**
     * Bitstream Vera Sans Mono 24
     *
     * @return Bitstream Vera Sans Mono 24 font
     */
    static Font * Font24 (void);

    /**
     * Bitstream Vera Sans Mono 32
     *
     * @return Bitstream Vera Sans Mono 32 font
     */
    static Font * Font32 (void);
    //@}

protected:
    /**
     * Build the texture for the font.
     *
     * @param data image data as alpha channel
     * @param width image width
     * @param height height image height
     */
    void build (const unsigned char *data=0, int width=0, int height=0);

protected:
    /**
     * Display list base
     */
    unsigned int base_;

    /**
     * Font texture (all glyphs at once)
     */
    unsigned int texture_;

    /**
     * Glyph size
     * 
     * Fonts are fixed width, all glyphs have the same size.
     */
    Size glyph_size_;
};

#endif
