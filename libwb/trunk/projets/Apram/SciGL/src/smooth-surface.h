/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMOOTH_SURFACE_H__
#define __SMOOTH_SURFACE_H__
#include "object.h"
#include "data.h"
#include "colormap.h"

/**
 * Represententation of a data source as a smooth surface.
 *
 * <table border="0"><tr>
 * <td>@image html smooth-surface.png</td>
 * <td>@image html smooth-surface-2.png</td>
 * </td></table>
 */
class SmoothSurface : public Object {
public:

    /**
     * @name Creation/Destruction
     */
    /**
     * Default constructor
     */
    SmoothSurface (void);

    /**
     * Destructor.
     */
    virtual ~SmoothSurface (void);
    //@}


    void reset(void);

    /**
     *  @name Rendering
     */
    /**
     * Build the surface.
     */
    void build (void);

    /**
     * Update the surface data.
     */
    void update (void);

    /**
     * Render the surface.
     */
    void render	(void);
    //@}


    /**
     *  @name Data
     */
    /**
     * Set data source.
     */
    virtual void set_data (Data *data);
    //@}


    /**
     *  @name Colormap
     */
    /**
     * Set surface colormap.
     */
    virtual void set_cmap (Colormap *cmap);

    /**
     * Get surface colormap.
     */
    virtual Colormap *get_cmap (void);

    /**
     * Get surface colormap.
     */
    virtual Colormap *cmap (void);
    //@}

    /**
     * @name Draw_element
     */
    /**
     * Set surface draw.
     *
     * @param  Visibility
     */
    virtual void set_surf_draw (bool visible);

    /**
     * Get surface draw.
     *
     * @return visibility
     */
    virtual bool get_surf_draw (void) const;

    /**
     * Set grid draw.
     *
     * @param  Visibility
     */
    virtual void set_grid_draw (bool visible);

    /**
     * Get grid draw.
     *
     * @return visibility
     */
    virtual bool get_grid_draw (void) const;


protected:
    /**
     * Represented data.
     */
    Data * data_;

    /**
     * Colormap of single values data (depth = 1).
     */
    Colormap * cmap_;

    /**
     * Surface vertices.
     */
    Vec4f * vertices_;

    /**
     * Surface normals
     */
    Vec4f * normals_;    

    /**
     * Number of vertices.
     */
    int nvertices_;	

    /**
     * Color at each vertex.
     */
    Color * colors_;

    /**
     * Quad indices.
     */
    int * indices_;	

    /**
     * number of quad indices.
     */
    int nindices_;

    /**
     * Whether surf is draw or not.
     */
    bool surf_draw_;   

    /**
     * Whether grid is draw or not.
     */
    bool grid_draw_;
};

#endif
