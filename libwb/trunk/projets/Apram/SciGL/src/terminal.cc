/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "terminal.h"

// ============================================================================
Terminal::Terminal (void) : Widget ()
{
    set_prompt ("\033[01m> \033[00m");
    input_ = "";
    set_margin    (5,5,5,5);
    set_radius    (0);
    set_size      (Size (1, 1));
    set_position  (0,0,-1);
    set_gravity   (1,1);
    set_fg_color  (0,0,0,1);
    set_bg_color  (1,1,.9,1);
    set_br_color  (0,0,0,0);
    alpha_ = 1.0f;
    font_base_ = 0;
    font_texture_ = 0;
    buffer_.push_back(new Line(""));
    cursor_ = 0;
    history_size_ = 256;
    history_index_ = 0;
    history_.push_back ("");
    event_handler_ = 0;
}

// ============================================================================
Terminal::~Terminal (void)
{
    if (font_texture_)
        glDeleteTextures (1, &font_texture_);
    if (font_base_)
        glDeleteLists (font_base_, 256);
}

// ============================================================================
void
Terminal::connect (void (*func)(Terminal *, std::string))
{
    event_handler_ = func;
}

// ============================================================================
void
Terminal::makefont (void)
{
    unsigned int width = font_12.width;
    unsigned int height = font_12.height;
    const unsigned char *data = font_12.data;

    glGenTextures (1, &font_texture_);
    glBindTexture (GL_TEXTURE_2D, font_texture_);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0,
                  GL_ALPHA, GL_UNSIGNED_BYTE, data);
    font_base_ = glGenLists (256);
    glyph_size_ = Size (width/16.0, height/16.0);
    float dx = glyph_size_.x / float (width);
    float dy = glyph_size_.y / float (height);
    for (int y=0; y<16; y++) {
        for (int x=0; x<16; x++) {
            glNewList (font_base_ + y*16+x, GL_COMPILE);
            if (((y%8)*16+x) == '\n') {
                glPopMatrix ();
                glTranslatef (0, -glyph_size_.y, 0);
                glPushMatrix ();
            } else if (((y%8)*16+x) == '\t') {
                glTranslatef (4*(glyph_size_.x-1), 0, 0);
            } else {
                glBegin(GL_QUADS);
                glTexCoord2f ((x  )*dx,      (y  )*dy);
                glVertex2f   ( 0,            glyph_size_.y);
                glTexCoord2f ((x  )*dx,      (y+1)*dy);
                glVertex2f   (0,             0);
                glTexCoord2f ((x+1)*dx,      (y+1)*dy);
                glVertex2f   (glyph_size_.x, 0);
                glTexCoord2f ((x+1)*dx,      (y  )*dy);
                glVertex2f   (glyph_size_.x, glyph_size_.y);
                glEnd();
                if (((y%8)*16+x) != '\0')
                    glTranslatef (glyph_size_.x-1, 0, 0);
            }
            glEndList();            
        }
    }
}

// ============================================================================
void
Terminal::print (std::string text)
{
    std::string::const_iterator s = text.begin();
    while (true) {
        std::string::const_iterator begin = s;
        while (*s != '\n' && s != text.end())
            ++s;
        std::string old = buffer_[buffer_.size()-1]->text;
        std::string line = std::string(begin, s);
        if (*s == '\n')
            line += '\n';
        if (old[old.size()-1] == '\n') {
            buffer_.push_back (new Line (line));
        } else {
            delete buffer_[buffer_.size()-1];
            buffer_[buffer_.size()-1] = new Line (old+line);
        }
        if (*s == '\n')
            buffer_.push_back (new Line (""));
	    if (s == text.end())
            break;
        if (++s == text.end())
            break;
    }
}

// ============================================================================
void
Terminal::render (std::string text, bool prompt)
{
    if (prompt)
        raw_prompt_ = "";

    glBindTexture (GL_TEXTURE_2D, font_texture_);

    int base = font_base_;
    Color default_color = fg_color_;
    Color fg_color = fg_color_;
    Color bg_color = bg_color_;
    bg_color.alpha = 0.0;
    bool inverse = false;
    Color sfg, sbg;
    const char *line = text.c_str();

    glPushMatrix();
    int i=0;
    int c=0;
    while (line[i] != 0) {
        if (strstr ((line+i), "\033[") == (char *)(line+i)) {
            int sequence = 1;
            i+=2;
            while (sequence) {
                /* Reset terminal attributes */
                if (strstr ((line+i), "00") == (char *)(line +i)) {
                    base = font_base_;
                    fg_color = default_color;
                    fg_color.alpha = 1;
                    bg_color = Color (1,1,1,0);
                    i+=2;
                /* Bold on */
                } else if (strstr ((line+i), "01") == (char *)(line +i)) {
                    base = font_base_ + 128;
                    i+=2;
                /* Bold off */
                } else if (strstr ((line+i), "22") == (char *)(line +i)) {
                    base = font_base_;
                    i+=2;
                /* Italic on */
                } else if (strstr ((line+i), "03") == (char *)(line +i)) {
                    fg_color.alpha = .5;
                    i+=2;
                /* Italic off */    
                } else if (strstr ((line+i), "23") == (char *)(line +i)) {
                    fg_color.alpha = 1;
                    i+=2;
                /* Underline on */
                } else if (strstr ((line+i), "04") == (char *)(line +i)) {
                    i+=2;
                /* Inverse on */
                } else if (strstr ((line+i), "07") == (char *)(line +i)) {
                    inverse = true;
                    sfg = fg_color;
                    sbg = bg_color;
                    fg_color = sbg;
                    fg_color.alpha = 1;
                    bg_color = sfg;
                    i+=2;
                /* Inverse off */
                } else if (strstr ((line+i), "27") == (char *)(line +i)) {
                    if (inverse) {
                        fg_color = sfg;
                        bg_color = sbg;
                        inverse = false;
                    }
                    i+=2;
                /* Underline off */
                } else if (strstr ((line+i), "24") == (char *)(line +i)) {
                    i+=2;
                /* Black foreground */
                } else if (strstr ((line+i), "30") == (char *)(line +i)) {
                    fg_color = Color (0,0,0,fg_color.alpha);
                    i+=2;    
                /* Red foreground */
                } else if (strstr ((line+i), "31") == (char *)(line +i)) {
                    fg_color = Color (1,0,0,fg_color.alpha);
                    i+=2;
                /* Green foreground */
                } else if (strstr ((line+i), "32") == (char *)(line +i)) {
                    fg_color = Color (0,1,0,fg_color.alpha);
                    i+=2;    
                /* Yellow foreground */
                } else if (strstr ((line+i), "33") == (char *)(line +i)) {
                    fg_color = Color (1,1,0,fg_color.alpha);
                    i+=2;    
                /* Blue foreground */
                } else if (strstr ((line+i), "34") == (char *)(line +i)) {
                    fg_color = Color (0,0,1,fg_color.alpha);
                    i+=2;    
                /* Magenta foreground */
                } else if (strstr ((line+i), "35") == (char *)(line +i)) {
                    fg_color = Color (1,0,1,fg_color.alpha);
                    i+=2;
                /* Cyan foreground */
                } else if (strstr ((line+i), "36") == (char *)(line +i)) {
                    fg_color = Color (0,1,1,fg_color.alpha);
                    i+=2;
                /* White foreground */
                } else if (strstr ((line+i), "37") == (char *)(line +i)) {
                    fg_color = Color (1,1,1,fg_color.alpha);
                    i+=2;
                /* Default foreground */
                } else if (strstr ((line+i), "39") == (char *)(line +i)) {
                    fg_color = default_color;
                    i+=2;    
                /* Black background */
                } else if (strstr ((line+i), "40") == (char *)(line +i)) {
                    bg_color = Color (0,0,0,1);
                    i+=2;
                /* Red background */
                } else if (strstr ((line+i), "41") == (char *)(line +i)) {
                    bg_color = Color (1,0,0,1);
                    i+=2;    
                /* Green background */
                } else if (strstr ((line+i), "42") == (char *)(line +i)) {
                    bg_color = Color (0,1,0,1);
                    i+=2;    
                /* Yellow background */
                } else if (strstr ((line+i), "43") == (char *)(line +i)) {
                    bg_color = Color (1,1,0,1);
                    i+=2;
                /* Blue background */
                } else if (strstr ((line+i), "44") == (char *)(line +i)) {
                    bg_color = Color (0,0,1,1);
                    i+=2;
                /* Magenta background */
                } else if (strstr ((line+i), "45") == (char *)(line +i)) {
                    bg_color = Color (1,0,1,1);
                    i+=2;    
                /* Cyan background */
                } else if (strstr ((line+i), "46") == (char *)(line +i)) {
                    bg_color = Color (0,1,1,1);
                    i+=2;    
                /* White background */
                } else if (strstr ((line+i), "47") == (char *)(line +i)) {
                    bg_color = Color (1,1,1,1);
                    i+=2;
                /* Default background */
                } else if (strstr ((line+i), "49") == (char *)(line +i)) {
                    bg_color = Color (1,1,1,0);
                    i+=2;
                /* Reset terminal attributes */
                } else if (strstr ((line+i), "0") == (char *)(line +i)) {
                    base = font_base_;
                    fg_color = default_color;
                    bg_color = Color (1,1,1,0);
                    glColor4fv (fg_color.data);
                    i+=1;
                /* Bold on */
                } else if (strstr ((line+i), "1") == (char *)(line +i)) {
                    base = font_base_+128;
                    i+=1;
                /* Italic on */
                } else if (strstr ((line+i), "3") == (char *)(line +i)) {
                    fg_color.alpha = .5;
                    glColor4fv (fg_color.data);                    
                    i+=1;
                /* Underline on */
                } else if (strstr ((line+i), "4") == (char *)(line +i)) {
                    i+=1;
                /* Inverse on */
                } else if (strstr ((line+i), "7") == (char *)(line +i)) {
                    Color color = fg_color;
                    fg_color = bg_color;
                    bg_color = color;
                    i+=2;
                }
                /* End of sequence */
                if (line[i++] == ';')
                    sequence = 1;
                else 
                    sequence = 0;
            }
            if (line[i] == 0)
                break;
        }
        else {
            if (bg_color.a*alpha_ > 0) {
                glColor4f (bg_color.r,bg_color.g, bg_color.b, bg_color.a*alpha_);
                glCallList (font_base_);
            }
            glColor4f (fg_color.r,fg_color.g, fg_color.b, fg_color.a*alpha_);
            glCallList (base+text[i]);
            if (prompt)
                raw_prompt_ += text[i];
            i++;
        }
        c++;
    }
    glPopMatrix();
}

// ============================================================================
void
Terminal::raw_render (std::string text)
{
    glPushMatrix();
    for (unsigned int i=0; i < text.size(); i++) {
        glColor4f (fg_color_.r,fg_color_.g, fg_color_.b, fg_color_.a*alpha_);
        glCallList (font_base_+text[i]);
    }
    glPopMatrix();

    glTranslatef (cursor_*(glyph_size_.x-1), 0, 0);
    glDisable (GL_TEXTURE_2D);
    glColor4f (fg_color_.r,fg_color_.g, fg_color_.b, fg_color_.a*alpha_);
    glBegin (GL_LINES);
    glVertex3f (1, 1, 0);
    glVertex3f (1, glyph_size_.y+1, 0);
    glEnd();
    glEnable (GL_TEXTURE_2D);
}

// ============================================================================
void
Terminal::render (void)
{
    if (!get_visible())  return;
    if (!font_base_) makefont();
    if (!font_base_) return;

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    // float width  = viewport[2];
    float height = viewport[3];

    render_start ();

    glTranslatef (.375,.375,0);
    if (get_br_color().a*alpha_ > 0) {
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glColor4f (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a*alpha_);
        glBegin(GL_POLYGON);
        round_rectangle (get_position().x, height-1-get_position().y,
                         get_size().x-1, get_size().y-1, radius_);
        glEnd();

        glTranslatef (0,0,1);
        glDepthMask (GL_FALSE);
        glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
        glEnable (GL_LINE_SMOOTH);
        glBegin (GL_LINE_LOOP);
        round_rectangle (get_position().x, height-1-get_position().y,
                         get_size().x-1, get_size().y-1, radius_);
        glEnd ();
    } else {
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glColor4f (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a*alpha_);
        glBegin(GL_POLYGON);
        round_rectangle (get_position().x, height-get_position().y,
                         get_size().x, get_size().y, radius_);
        glEnd();        
    }

    glColor4fv (get_fg_color().data);
    glLineWidth (1);
    glEnable (GL_SCISSOR_TEST);
    glScissor (int(get_position().x + get_margin().left),
               int(height-1-get_position().y - get_size().y + get_margin().down),
               int(get_size().x - get_margin().right - get_margin().left),
               int(get_size().y - get_margin().up    - get_margin().down));

    // Reset line smooth shift
    glTranslatef (-.375,-.375, 0);
    glTranslatef (0,0,1);
    glTranslatef (get_position().x + get_margin().left,
                  height-1-get_position().y - get_margin().up,
                  0);
    glEnable (GL_TEXTURE_2D);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glDisable (GL_LINE_SMOOTH);

    // When not there is not enough lines, text is aligned against top border,
    //  else, it is aligned against bottom border
    unsigned int lines =
        int(size_.y-get_margin().up-get_margin().down)/int(glyph_size_.y);

    unsigned l1, l2;
    if (buffer_.size() <= (lines)) {
        l1 = 0;
        l2 = buffer_.size();
    }  else {
        float dy = int(size_.y-get_margin().up-get_margin().down - lines*(glyph_size_.y));
        glTranslatef (0, glyph_size_.y-dy, 0);
        l1 = buffer_.size()-lines-1;
        l2 = buffer_.size();
    }

    glTranslatef (0, -glyph_size_.y, 0);
    // Actual render of lines using display list
    for (unsigned int i=l1; i<l2; i++) {
        // When fade_in/fade_out we draw directly to
        //  manage global alpha properly
        if ((fade_in_delay_) || (fade_out_delay_)) {
            render (buffer_[i]->text);
        } else if (alpha_ > 0.0f) {
            if (buffer_[i]->list == 0) {
                buffer_[i]->list = glGenLists (1);
                alpha_ = 1.0;
                glNewList (buffer_[i]->list, GL_COMPILE);
                render (buffer_[i]->text);
                glEndList();
            }
            glCallList (buffer_[i]->list);
        }
    }
    // Input line (not cached)
    render (prompt_, true);
    glTranslatef (raw_prompt_.size()*(glyph_size_.x-1), -0*glyph_size_.y, 0);
    raw_render (input_);
    glDepthMask (GL_TRUE);
    
    render_finish();
}

// ============================================================================
// Event processing
// ============================================================================
bool
Terminal::key_press (char key, int modifier)
{
    if (not get_visible()) return false;

    if ((key > 31) && (key < 127)) {
        std::string::iterator i = input_.begin() + cursor_;
        input_.insert(i, key);
        cursor_++;
        return true;
    } else {
        if (key  == ('a'-'a'+1)) {   // Control-a : home
            cursor_ = 0;
            return true;
        } else if (key == ('b'-'a'+1)) {   // Control-b : backward
            if (cursor_ > 0)
                cursor_--;
            return true;
        } else if (key == ('c'-'a'+1)) {   // Control-c : cancel
        } else if ((key == ('d'-'a'+1))    // Control-d
                   || (key == 127)) {      //  or delete
            if (cursor_ < input_.size()) {
                std::string::iterator i = input_.begin() + cursor_;
                input_.erase (i);
            }
            return true;
        } else if (key == ('e'-'a'+1)) {   // Control-e : end
            cursor_ = input_.size();
            return true;
        } else if (key == ('f'-'a'+1)) {   // Control-f : forward
            if (cursor_ < input_.size())
                cursor_++;
            return true;
        } else if (key == ('h'-'a'+1)) {   // Control-h : backspace
            if (cursor_ > 0) {
                std::string::iterator i = input_.begin() + cursor_ - 1;
                input_.erase (i);
                cursor_--;
            }
            return true;     
        } else if (key == ('i'-'a'+1)) {   // Control-i : up
            if (history_index_ < (history_.size()-1)) {
                if (history_index_ == 0)
                    history_[0] = input_;
                history_index_++;
                input_ = history_[history_index_];
                cursor_ = input_.size();
            }
            return true;
        } else if (key == ('j'-'a'+1)) {   // Control-j : down
            if (history_index_ > 0) {
                history_index_--;
                input_ = history_[history_index_];
                cursor_ = input_.size();
            }
            return true;
        } else if (key == ('k'-'a'+1)) {   // Control-k : kill from cursor
            if (cursor_ < input_.size()) {
                kill_buffer_ = input_.substr (cursor_);
                input_ = input_.substr (0, cursor_);
            }
            return true;
        } else if (key == ('l'-'a'+1)) {   // Control-l : clear
            buffer_.clear();
            buffer_.push_back (new Line(""));
            return true;
        } else if (key == ('m'-'a'+1)) {   // Control-n : return
            history_.insert (history_.begin()+1, input_);
            print (prompt_+input_+std::string("\n"));
            if (event_handler_)
                (*event_handler_)(this, input_);
            input_ = "";
            cursor_ = 0;
            history_index_ = 0;
            return true;
        } else if (key == ('y'-'a'+1)) {   // Control-y : yank
            input_.insert (cursor_, kill_buffer_);
            cursor_ += kill_buffer_.size();
            return true;
        }
    }
    return false;
}

// ============================================================================
void
Terminal::set_prompt (std::string prompt)
{
    prompt_ = prompt;
}

std::string
Terminal::get_prompt (void)
{
    return prompt_;
}

std::string
Terminal::prompt (void)
{
    return get_prompt ();
}

// ============================================================================
unsigned int
Terminal::get_font_base (void)
{
    if (!font_base_) makefont();
    return font_base_;
}
unsigned int
Terminal::font_base (void)
{
    return get_font_base();
}

unsigned int  
Terminal::get_font_texture (void)
{
    if (!font_base_) makefont();
    if (!font_base_) return 0;
    return font_texture_;
}
unsigned int
Terminal::font_texture (void)
{
    return get_font_texture ();
}
