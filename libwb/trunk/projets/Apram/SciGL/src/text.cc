/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "font.h"
#include "text.h"

Text::Text (void) : Widget ()
{
    autosize_ = true;
    //    lines_.push_back ( Line("Emergence of attention within a neural population", 24, 0));
    //    lines_.push_back ( Line("Neural Networks, N.P.Rougier & J.Vitay, volume 19, number 5, pp 573-581, June 2006.",  12, 0));
    set_fg_color (0,0,0,1);
    set_bg_color (1,1,1,0);
    set_br_color (0,0,0,0);
}

Text::~Text (void)
{}


void
Text::append (std::string text, int size, int justification)
{
    lines_.push_back (Line(text,size,justification));
}

void
Text::clear()
{
    lines_.clear();
}



void
Text::render (void)
{
    if (not get_visible()) return;
    if (not lines_.size()) return;

    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float height = viewport[3];

    render_start ();

    glTranslatef (.375,.375,0);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glPolygonOffset (1, 1);
    glEnable (GL_POLYGON_OFFSET_FILL);
    glColor4f (get_bg_color().r, get_bg_color().g, get_bg_color().b, get_bg_color().a*alpha_);
    glBegin (GL_POLYGON);
    round_rectangle (get_position().x, height-1-get_position().y, get_size().x-1, get_size().y-1, radius_);
    glEnd();

    glTranslatef (0,0,1);
    glDepthMask (GL_FALSE);
    glColor4f (get_br_color().r, get_br_color().g, get_br_color().b, get_br_color().a*alpha_);
    glEnable (GL_LINE_SMOOTH);
    glBegin (GL_LINE_LOOP);
    round_rectangle (get_position().x, height-1-get_position().y, get_size().x-1, get_size().y-1, radius_);
    glEnd ();
    glDepthMask (GL_TRUE);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glEnable (GL_BLEND);
    glEnable (GL_TEXTURE_2D);

    glTranslatef (-.375,-.375,0);

    glEnable (GL_SCISSOR_TEST);
    glScissor (int(get_position().x + get_margin().left),
               int(height-1-get_position().y - get_size().y + get_margin().down),
               int(get_size().x - get_margin().right - get_margin().left),
               int(get_size().y - get_margin().up    - get_margin().down));

    glPushMatrix ();
    glTranslatef (get_position().x+get_margin().left,
                  height-1-get_position().y-get_margin().up,
                  1);

    Size s(0,0);
    glColor4f (get_fg_color().r, get_fg_color().g, get_fg_color().b, get_fg_color().a*alpha_);
    Font *font = Font::Font12();
    for (unsigned int i=0; i<lines_.size(); i++) {
        switch (lines_[i].size) {
        case 16:
            font = Font::Font16();
            break;
        case 24:
            font = Font::Font24();
            break;
        case 32:
            font = Font::Font32();
            break;
        default:
            font = Font::Font12();
            break;
        }

        Size textsize = font->size (lines_[i].text);
        glPushMatrix ();
        if (lines_[i].justification == 0)
            glTranslatef (int((get_size().x-get_margin().left-get_margin().right - textsize.x)/2), 0, 0);
        else if (lines_[i].justification == +1)
            glTranslatef (int((get_size().x-get_margin().left-get_margin().right - textsize.x)), 0, 0);
        if (textsize.x > s.x)
            s.x = textsize.x;
        s.y += textsize.y;
        glTranslatef (0, -font->glyph_size().y,0);
        font->render (lines_[i].text, true);
        glPopMatrix ();
        glTranslatef (0, -font->glyph_size().y,0);
    }
    glPopMatrix ();

    size_request_.x = s.x + get_margin().left + get_margin().right;
    size_request_.y = s.y + get_margin().up + get_margin().down;
    glDisable (GL_SCISSOR_TEST);
    render_finish();
}

void
Text::set_size (Size size)
{
    if (autosize_)
        size_.z = size.z;
    else
        Widget::set_size (size);
}

void
Text::set_size (float x, float y, float z)
{
    if (autosize_)
        size_.z = z;
    else
        Widget::set_size (x,y,z);
}
