/**
 *
 * @example glut-object.cc
 *   @image html object.png
 *   Show object default rendering.
 *
 * @example glut-widget.cc
 *   @image html widget.png
 *   Show widget default rendering.
 *
 * @example glut-colorbar.cc 
 *   @image html colorbar.png
 *   Show colorbar default rendering.
 *
 * @example glut-colorbars.cc 
 *   @image html colorbars.png
 *   Show standard default colorbars.
 *
 * @example glut-text.cc 
 *   @image html text.png
 *   Show text widget.
 *
 * @example glut-font.cc 
 *   @image html font.png
 *   Show font default rendering.
 *
 * @example glut-fonts.cc 
 *   @image html fonts.png
 *   Show standard default fonts.
 *
 * @example glut-smooth-surface.cc
 *   @image html smooth-surface.png
 *   Show smooth surface object using a function as data.
 *
 * @example glut-heightmap.cc
 *   @image html smooth-surface-2.png
 *   Show smooth surface object using an image as data.
 *
 * @example glut-flat-surface.cc
 *   @image html flat-surface.png
 *   Show flat surface object using a function as data.
 *
 * @example glut-image.cc
 *   @image html image.png
 *   Show flat surface object using an image as data & colormap
 *
 * @example glut-terminal.cc
 *   @image html terminal.png
 *   Show interactive terminal and how to get input from terminal.
 */

/**

@mainpage SciGL - OpenGL Scientific Visualization

@section about About

SciGL (OpenGL Scientific Visualization) aims at facilitating the development of
scientific visualization by providing a set of classes for rapid prototyping
of scientific visualization software. It has not been designed as a library
since the goal of SciGL is to try to offer a minimal set of objects without the
need for any kind of installation. A large number of examples is provided to
show how one can use parts of SciGL components to suit its own needs.

Current dependencies are:

- OpenGL, the Open Graphics Library
  <br> see http://www.opengl.org

- GLUT, the OpenGL Utility Toolkit
  <br> see http://www.opengl.org/resources/libraries/glut/

- libpng, the Portable Network Graphics library
<br> see http://www.libpng.org

- libjpeg, the Joint Photographic Experts Group library
<br> see http://www.ijg.org
 
@section features Features

 - Font display as texture mapped quads
 - Terminal widget for interactive use


@section hierarchy Object hierarchy

- Colormap
- Data
- Font
- Object
    - Axis 
    - Frame
    - CubicSurface
    - SmoothSurface
    - FlatSurface
    - Scene
    - Widget
        - Colorbar
        - Text
        - Terminal
- Vec4f (or Size, Position, Orientation, Margin, Gravity, Range, Shape)


@section examples Examples

- Basic
    - @ref glut-object.cc
      <br>Show object default rendering.
    - @ref glut-widget.cc
      <br>Show widget default rendering.
    - @ref glut-colorbar.cc
      <br>Show colorbar default rendering.
    - @ref glut-colorbars.cc 
      <br>Show standard default colorbars.
    - @ref glut-font.cc 
      <br>Show font default rendering.      
    - @ref glut-fonts.cc 
      <br>Show standard default fonts.
    - @ref glut-text.cc 
      <br>Show text wiget.
    - @ref glut-smooth-surface.cc
      <br>Show smooth surface object using a function as data.
    - @ref glut-heightmap.cc
      <br>Show smooth surface object using an image as data.
    - @ref glut-flat-surface.cc
      <br>Show flat surface object using a function as data.
    - @ref glut-image.cc
      <br>Show flat surface object using an image as data & colormap.
    - @ref glut-terminal.cc
      <br>Show flat surface object using an image as data & colormap.

- Advanced


@section license License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/
