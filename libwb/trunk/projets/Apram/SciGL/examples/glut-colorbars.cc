/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <Glut/glut.h>
#else
    #include <GL/glut.h>
#endif
#include "colorbar.h" 

std::vector<Colorbar *> colorbars;

void display (void) {
    glClearColor (1,1,1,1);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    for (unsigned int i=0; i<colorbars.size(); i++)
        colorbars[i]->render();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    glViewport (0, 0, width, height);
}
int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("Colorbars");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutReshapeWindow (768,256);

    Colorbar *colorbar;
    float x  = 0.075;
    float dx = (1-x)/6.0;

    colorbar = new Colorbar();
    colorbar->set_title ("Ice");
    colorbar->set_gravity (1,0);
    colorbar->set_position (x+0*dx,0);
    colorbar->set_size (20,.75);
    colorbar->set_cmap (Colormap::Ice());
    colorbars.push_back (colorbar);

    colorbar = new Colorbar();
    colorbar->set_title ("Fire");
    colorbar->set_gravity (1,0);
    colorbar->set_position (x+1*dx,0);
    colorbar->set_size (20,.75);
    colorbar->set_cmap (Colormap::Fire());
    colorbars.push_back (colorbar);

    colorbar = new Colorbar();
    colorbar->set_title ("IceAndFire");
    colorbar->set_gravity (1,0);
    colorbar->set_position (x+2*dx,0);
    colorbar->set_size (20,.75);
    colorbar->set_cmap (Colormap::IceAndFire());
    colorbars.push_back (colorbar);

    colorbar = new Colorbar();
    colorbar->set_title ("Hot");
    colorbar->set_gravity (1,0);
    colorbar->set_position (x+3*dx,0);
    colorbar->set_size (20,.75);
    colorbar->set_cmap (Colormap::Hot());
    colorbars.push_back (colorbar);

    colorbar = new Colorbar();
    colorbar->set_title ("Grey");
    colorbar->set_gravity (1,0);
    colorbar->set_position (x+4*dx,0);
    colorbar->set_size (20,.75);
    colorbar->set_cmap (Colormap::Grey());
    colorbars.push_back (colorbar);

    colorbar = new Colorbar();
    colorbar->set_title ("User defined");
    colorbar->set_gravity (1,0);
    colorbar->set_position (x+5*dx,0);
    colorbar->set_size (20,.75);
    Colormap *cmap = new Colormap();
    cmap->append (0.0, Color(1,1,1,1));
    cmap->append (1.0, Color(1,0,1,1));
    colorbar->set_cmap (cmap);
    colorbars.push_back (colorbar);

    glutMainLoop();
    return 0;
}

