/**
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include "font.h" 

void print (Font *font, std::string text) {
    glPushMatrix ();
    glTranslatef (0, -font->glyph_size().y,0);
    font->render (text);
    glPopMatrix ();
    glTranslatef (0, -font->glyph_size().y,0);
}

void display (void) {
    glClearColor (1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_TEXTURE_2D);
    glEnable (GL_BLEND);
    GLint viewport[4]; 
    glGetIntegerv (GL_VIEWPORT, viewport);
    float width = viewport[2];
    float height = viewport[3];
    glColor4f (0,0,0,1);
    for (int i=0; i<10; i++) {
        glPushMatrix ();
        Size s =  Font::Font12()->size ("    Hello world !");
        glTranslatef (int(width/2), int(height/2), 0);
        glRotatef (36*i,0,0,1);
        glTranslatef (0, -int(s.y/2), 0);
        glColor4f (0,0,0,1);
        Font::Font12()->render ("    Hello world !");
        glPopMatrix ();
    }
    glutSwapBuffers();
}
void reshape (int width, int height) {
    glViewport (0, 0, width, height);
    glMatrixMode (GL_PROJECTION);
    glPushMatrix ();
    glLoadIdentity ();
    glOrtho (0, width, 0, height, -1000, 1000);
    glMatrixMode (GL_MODELVIEW);
    glPushMatrix ();
    glLoadIdentity();
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("Font");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutReshapeWindow (256,256);
    glutMainLoop();
    return 0;
}

