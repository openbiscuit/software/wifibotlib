/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include "objects.h" 


struct Network {
    int   size;
    float *units;
    float *afferents;
    float *laterals_p;
    float *laterals_m;
    float af_i;			// afferent default input weight (afferents)
    float gp_w, gp_i;	// Parameter for positive gaussian (laterals)
    float gm_w, gm_i;	// Paremeter for negative gaussian (laterals)
	float alpha;
	float dt;
    float baseline;
    int seed;
    unsigned long epochs;
    unsigned long age;
    float bubble_x, bubble_y;
    float sum_dist;
    float sum_dist2;
};

struct Stimulus {
    float x, y;
    float dx, dy;
    float dx_, dy_;
};
struct Environment {
    int size;
    float *units;
    float noise_level;
    int	  stim_n;
    float stim_w, stim_s;
    Stimulus *stims;
    int refresh_rate;
};  
Environment env;
Network net;


static Scene scene;
static Terminal *terminal = 0;
static Colorbar *colorbar = 0;
static unsigned int button = 0;
static Size window_size;
static Position window_position;
static bool fullscreen = false;

void switch_fullscreen (void) {
    if (fullscreen) {
        if (not window_size.width)
            window_size = Size (1000,800);
        glutReshapeWindow (int(window_size.width), int(window_size.height));
        glutPositionWindow (int(window_position.x), int(window_position.y));
        fullscreen = false;
    } else {
        window_position = Position (glutGet(GLUT_WINDOW_X), glutGet(GLUT_WINDOW_Y));
        window_size = Size (glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
        glutFullScreen();
        fullscreen = true;
    }
}

void on_key_press (unsigned char key, int x, int y) {
    if (key == 27)
        if (terminal->visible())
            terminal->hide (250);
        else
            terminal->show (250);
    else
        scene.key_press (key);
}

void on_special_key_press (int key, int x, int y) {
    unsigned char k = 0;
    switch (key) {
    case GLUT_KEY_F1:
        switch_fullscreen();
        break;
    case GLUT_KEY_F2:
        if (colorbar->visible()) colorbar->hide (250);
        else                     colorbar->show (250);        
        break;
    case GLUT_KEY_UP:    k = 'i'-'a' + 1; break; // Control-i
    case GLUT_KEY_DOWN:  k = 'j'-'a' + 1; break; // Control-j
    case GLUT_KEY_LEFT:  k = 'b'-'a' + 1; break; // Control-b
    case GLUT_KEY_RIGHT: k = 'f'-'a' + 1; break; // Control-f
    case GLUT_KEY_HOME:  k = 'a'-'a' + 1; break; // Control-a
    case GLUT_KEY_END:   k = 'e'-'a' + 1; break; // Control-e
    default:             k = 0;           break;
    }
    scene.key_press (k);
}

void on_mouse_button (int b, int state, int x, int y) {
    switch (b) {
    case GLUT_LEFT_BUTTON:   button = 1;  break;
    case GLUT_MIDDLE_BUTTON: button = 2;  break;
    case GLUT_RIGHT_BUTTON:  button = 3;  break;
    default:                 button = 0;  break;
    }
    switch (state) {
    case GLUT_UP:
        scene.button_release (button, x, y);
        break;
    case GLUT_DOWN:
        scene.button_press (button, x, y);
        break;
    }
}

void on_mouse_move (int x, int y) {
    scene.mouse_motion (button, x, y);
}

void display (void) {
    scene.render();
    glutSwapBuffers();
}

void reshape (int width, int height) {
    scene.resize (width, height);
}

void terminal_event (Terminal *terminal, std::string input) {
    terminal->print (std::string("Unknown command\n"));
}

float gaussian (float center_x, float A, float x)
{
	return exp (-((center_x-x)*(center_x-x)/(A*A)));
}

float gaussian (float center_x, float center_y, float A, float x, float y)
{
	return exp (-((center_x-x)*(center_x-x)/(A*A) + (center_y-y)*(center_y-y)/(A*A)));
}

void update (Environment *env)
{
    for (int i=0; i< (env->size*env->size); i++)
        env->units[i] = 0;

    for (int k=0; k<env->stim_n; k++) {
        float center_x = env->stims[k].x * env->size;
        float center_y = env->stims[k].y * env->size;
        float size     = env->stim_w * env->size/2.0f;
        for (int j = int(center_y-3*size); j < int(center_y+3*size); j++) {
            for (int i = int(center_x-3*size); i < int(center_x+3*size); i++) {
                if ((j>=0) && (i>=0) && (j<env->size) && (i<env->size)) {
                    env->units[j*env->size+i] += gaussian (center_x, center_y, size, i, j);
                }
            }
        }
    }
    for (int i=0; i< (env->size*env->size); i++) {
        env->units[i] += (2*(random()/float(RAND_MAX))-1)*env->noise_level;
        if (env->units[i] > 1)
            env->units[i] = 1;
    }

    for (int i = 0; i<env->stim_n; i++) {
        env->stims[i].x += env->stims[i].dx+env->stims[i].dx_;
        env->stims[i].y += env->stims[i].dy+env->stims[i].dy_;
        if (env->stims[i].x < env->stim_w) {
            env->stims[i].x = env->stim_w;
            env->stims[i].dx *= -1;
        }
        else if (env->stims[i].x > (1.0f-env->stim_w)) {
            env->stims[i].x = 1-env->stim_w;
            env->stims[i].dx *= -1;
        }
        if (env->stims[i].y < env->stim_w) {
            env->stims[i].y = env->stim_w;
            env->stims[i].dy *= -1;
        }
        else if (env->stims[i].y > (1-env->stim_w)) {
            env->stims[i].y = 1-env->stim_w;
            env->stims[i].dy *= -1;
        }
    }
    for (int i = 0; i<env->stim_n; i++) {
        env->stims[i].dx_ = 0;
        env->stims[i].dy_ = 0;
        for (int j=0; j<env->stim_n; j++) {
            if (i != j) {
                float dx = (env->stims[i].x - env->stims[j].x)/(3.0*env->stim_w);
                float dy = (env->stims[i].y - env->stims[j].y)/(3.0*env->stim_w);
                float d = dx*dx+dy*dy;
                if (d > 0) {
                    d = sqrt(d);
                    //                    float r = 0.0005/(d*d*d*d);
                    float r = 0.00005/(d*d*d*d);
                    env->stims[i].dx_ += r*dx;
                    env->stims[i].dy_ += r*dy;
                }
            }
        }
    }
}

void update (Network *net)
{
    static float *buffer  = 0;
    static float *buffer1 = 0;
    static float *buffer2 = 0;
    if (buffer == 0) {
        buffer  = new float [net->size*net->size];
        buffer1 = new float [net->size*net->size];
        buffer2 = new float [net->size*net->size];
    }
    
    memset (buffer, 0, net->size*net->size*sizeof(float));
    memset (buffer1, 0, net->size*net->size*sizeof(float));
    int index = 0;
    for (int y=0; y<net->size; y++) {
        for (int x=0; x<net->size; x++) {
            for (int i=0; i<net->size; i++) {
                buffer[index] += net->units[y*net->size+i]*net->laterals_p[net->size+(x-i)];
            }
            index++;
        }
    }

    index = 0;
    for (int y=0; y<net->size; y++) {
        for (int x=0; x<net->size; x++) {
            for (int i=0; i<net->size; i++) {
                buffer1[index] += buffer[i*net->size+x]*net->laterals_p[net->size+(y-i)];
            }
            index++;
        }
    }

    memset (buffer, 0, net->size*net->size*sizeof(float));
    memset (buffer2, 0, net->size*net->size*sizeof(float));
    index = 0;
    for (int y=0; y<net->size; y++) {
        for (int x=0; x<net->size; x++) {

            for (int i=0; i<net->size; i++) {
                buffer[index] += net->units[y*net->size+i]*net->laterals_m[net->size+(x-i)];
            }
            index++;
        }
    }

    index = 0;
    for (int y=0; y<net->size; y++) {
        for (int x=0; x<net->size; x++) {
            for (int i=0; i<net->size; i++) {
                buffer2[index] += buffer[i*net->size+x]*net->laterals_m[net->size+(y-i)];
            }
            index++;
        }
    }

    float x = net->bubble_x;
    float y = net->bubble_y;
    net->bubble_x = 0;
    net->bubble_y = 0;
    float sum = 0;
    for (int k=0; k<net->size*net->size; k++) {
        int x = rand() % net->size;
        int y = rand() % net->size;
        
        x = k%net->size;
        y = k/net->size;

        index = y*net->size+x;

        float u = net->units[index];
        float v = net->afferents[index]*env.units[index] + buffer1[index] - buffer2[index];
        float dv = (-net->units[index] + net->baseline + (1.0f/net->alpha)*v)*net->dt;
        net->units[index] += dv;
        if (net->units[index] > 1.0f)		net->units[index] = 1.0f;
        else if (net->units[index] < 0.0f)	net->units[index] = 0.0f;
        
        dv = u - net->units[index];
        
        sum += net->units[index];
        net->bubble_x += net->units[index] * x/float(net->size);
        net->bubble_y += net->units[index] * y/float(net->size);

    }
    
    if (sum != 0) {
        net->bubble_x /= sum;
        net->bubble_y /= sum;
        x = net->bubble_x;
        y = net->bubble_y;
    }
}

void timeout (int value) {
    update (&env);
    update (&net);
    scene.update();
    display ();
    glutTimerFunc (int(1000.0/scene.fps()), timeout, 1);
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("CNFT - Full");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutMouseFunc (on_mouse_button);
    glutMotionFunc (on_mouse_move);
    glutKeyboardFunc (on_key_press);
    glutSpecialFunc (on_special_key_press);
    glutReshapeWindow (1000,600);
    glutTimerFunc (int(1000.0/scene.fps()), timeout, 1);


    // Argument parsing
    for (int i=1; i<argc; i+=2) {
        if (strcmp (argv[i], "-size") == 0)
            sscanf (argv[i+1], "%d", &net.size);
        else if (strcmp (argv[i], "-epochs") == 0)
            sscanf (argv[i+1], "%ld", &net.epochs);
        else if (strcmp (argv[i], "-alpha") == 0)
            sscanf (argv[i+1], "%f", &net.alpha);
        else if (strcmp (argv[i], "-dt") == 0)
            sscanf (argv[i+1], "%f", &net.dt);
        else if (strcmp (argv[i], "-baseline") == 0)
            sscanf (argv[i+1], "%f", &net.baseline);
        else if (strcmp (argv[i], "-af_i") == 0)
            sscanf (argv[i+1], "%f", &net.af_i);
        else if (strcmp (argv[i], "-gp_i") == 0)
            sscanf (argv[i+1], "%f", &net.gp_i);
        else if (strcmp (argv[i], "-gp_w") == 0)
            sscanf (argv[i+1], "%f", &net.gp_w);
        else if (strcmp (argv[i], "-gm_w") == 0)
            sscanf (argv[i+1], "%f", &net.gm_w);
        else if (strcmp (argv[i], "-gm_i") == 0)
            sscanf (argv[i+1], "%f", &net.gm_i);
        else if (strcmp (argv[i], "-seed") == 0)
            sscanf (argv[i+1], "%d", &net.seed);
        else if (strcmp (argv[i], "-stim_n") == 0)
            sscanf (argv[i+1], "%d", &env.stim_n);
        else if (strcmp (argv[i], "-stim_w") == 0)
            sscanf (argv[i+1], "%f", &env.stim_w);
        else if (strcmp (argv[i], "-stim_s") == 0)
            sscanf (argv[i+1], "%f", &env.stim_s);
    }


    // Environment setup
    // =================
    env.size	= 75;
    env.units 	   = new float[env.size * env.size];
    env.noise_level = 0.0;
    env.stim_n	= 5;
    env.stim_w	= 0.1f;
    env.stim_s	= 0.00025;
    env.stims   = new Stimulus[env.stim_n];

    // Network setup
    // ==============
    net.size	= env.size;
    net.epochs	= 0;
    net.alpha	= 11.0f;
    net.dt		= 0.1;
    net.baseline= 0.0f;
    net.af_i	= 1.5;
    net.gp_w	= 0.015f;
    net.gp_i	= 1.3f;
    net.gm_w	= 0.8f;
    net.gm_i	= 0.15f;
    net.seed	= 12;
    net.units = new float[net.size * net.size];

    for (int i=0; i<(net.size*net.size); i++)
        net.units[i] = 0;
    net.afferents	= new float[net.size * net.size];
    net.laterals_p	= new float[2*net.size+1];
    net.laterals_m	= new float[2*net.size+1];
    for (int i=0; i<(2*net.size+1); i++)
        net.laterals_p[i] = sqrt(net.gp_i)*gaussian (0, net.gp_w, (i-net.size)/float(2*net.size+1));
    for (int i=0; i<(2*net.size+1); i++)
        net.laterals_m[i] = sqrt(net.gm_i)*gaussian (0, net.gm_w, (i-net.size)/float(2*net.size+1));
    for (int i=0; i<net.size*net.size; i++)
        net.afferents[i] = net.af_i;

    net.age = 0;
    net.sum_dist  = 0;
    net.sum_dist2 = 0;
    srand (net.seed);
    for (int i=0; i<env.stim_n; i++) {
        env.stims[i].x = env.stim_w/2.0f + (rand()/float(RAND_MAX)) * (1.0f-env.stim_w);
        env.stims[i].y = env.stim_w/2.0f + (rand()/float(RAND_MAX)) * (1.0f-env.stim_w);
        float theta = (rand()/float(RAND_MAX)) * 2.0f * M_PI;
        env.stims[i].dx = cos(theta) * env.stim_s;
        env.stims[i].dy = sin(theta) * env.stim_s;
    }

    Colormap *cmap = Colormap::Hot();
    cmap->scale (-.1,1);

    colorbar = new Colorbar();
    colorbar->set_cmap (cmap);
    colorbar->set_gravity (0,-1);
    colorbar->set_position (0,48,1);
    colorbar->set_title ("");
    colorbar->set_fg_color (1,1,1,1);
    colorbar->set_br_color (1,1,1,1);
    scene.add (colorbar);

    Text *text = new Text();
    text->set_bg_color (1,1,1,0);
    text->set_fg_color (1,1,1,1);
    text->set_gravity (0,1);
    text->set_position (0,24);
    text->append ("Emergence of Attention within a Neural Population", 24, 0);
    text->append (" N.P. Rougier and J. Vitay, Neural Networks, volume 19, number 5, pp 573-581, June 2006.", 12, 0);
    scene.add (text);


    terminal = new Terminal();
    terminal->set_radius (8);
    terminal->set_size (.75, .5);
    terminal->set_position (10,10,1);
    terminal->set_gravity (1,-1);
    terminal->set_fg_color (1,1,1,1);
    terminal->set_bg_color (.5,.5,.5,.5);
    terminal->set_br_color (1,1,1,1);
    terminal->set_prompt ("Command > ");
    terminal->connect (terminal_event);
    scene.add (terminal);

    SmoothSurface *input = new SmoothSurface();
    //Image *input = new Image ();
    Data *data = new Data();
    data->set_data  (env.units, Shape (env.size, env.size, 1));
    input->set_data (data);
    input->set_cmap (cmap);
    input->set_size (.49,.49,.1);
    input->set_position (-.25, 0);
    input->set_br_color (1,1,1,1);
    scene.add (input);

    SmoothSurface *focus = new SmoothSurface();
    //Image *focus = new Image ();
    data = new Data();
    data->set_data  (net.units, Shape (net.size, net.size, 1));
    focus->set_data (data);
    focus->set_cmap (cmap);
    focus->set_size (.49,.49,.1);
    focus->set_position (.25, 0);
    focus->set_br_color (1,1,1,1);
    scene.add (focus);

    scene.set_orientation (35, 0);
    scene.set_bg_color (0, 0, 0, 1);
    scene.set_zoom (3.75);
    scene.build ();
    
    terminal->hide();
    update(&env);
    update(&net);

    glutMainLoop();
    return 0;
}

