/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include "object.h" 

static Object object;

void display (void) {
    glClearColor (1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    glTranslatef (0.0, 0, -8.0f);
    glRotatef (25,0,1,0);
    glRotatef (5,0,0,1);
    object.render();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    glViewport (0, 0, width, height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    float aspect = 1.0f;
    if (width > height) 
        aspect = width / float(height);
    else
        aspect = height / float(width);
    float aperture = 25.0f;
    float near = 1.0f;
    float far = 100.0f;
    float top = tan(aperture*3.14159/360.0) * near;
    float bottom = -top;
    float left = aspect * bottom;
    float right = aspect * top;
    glFrustum (left, right, bottom, top, near, far);
    glMatrixMode (GL_MODELVIEW);
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("Object");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutReshapeWindow (256,256);

    glEnable (GL_DEPTH_TEST);
    glClearDepth (1.0f); 
    glDepthFunc (GL_LEQUAL);
    glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glShadeModel (GL_SMOOTH);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);

    object.set_bg_color (1,1,1,.75);
    object.set_br_color (0,0,0,1);

    glutMainLoop();
    return 0;
}

