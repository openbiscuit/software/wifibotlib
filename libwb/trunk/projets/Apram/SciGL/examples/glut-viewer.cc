/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include <cstdlib>
#include "fparser.h"
#include "scene.h" 
#include "terminal.h" 
#include "colorbar.h" 
#include "smooth-surface.h"
#include "frame.h"
#include "data.h"

static Scene scene;
static Terminal *terminal = 0;
static Colorbar *colorbar = 0;
static Frame *frame = 0;
static unsigned int button = 0;
static unsigned int fps = 40;
static Data *data;
SmoothSurface *surf;

void on_key_press (unsigned char key, int x, int y) {
    if (key == 27)
        if (terminal->get_visible())
            terminal->hide (250);
        else
            terminal->show (250);
    else
        scene.key_press (key);
}
void on_special_key_press (int key, int x, int y) {
    unsigned char k = 0;
    switch (key) {
    case GLUT_KEY_F1:
        if (frame->get_visible()) frame->hide (250);
        else                      frame->show (250);        
        break;
    case GLUT_KEY_F2:
        if (colorbar->get_visible()) colorbar->hide (250);
        else                         colorbar->show (250);        
        break;
    case GLUT_KEY_UP:    k = 'i'-'a' + 1; break; // Control-i
    case GLUT_KEY_DOWN:  k = 'j'-'a' + 1; break; // Control-j
    case GLUT_KEY_LEFT:  k = 'b'-'a' + 1; break; // Control-b
    case GLUT_KEY_RIGHT: k = 'f'-'a' + 1; break; // Control-f
    case GLUT_KEY_HOME:  k = 'a'-'a' + 1; break; // Control-a
    case GLUT_KEY_END:   k = 'e'-'a' + 1; break; // Control-e
    default:             k = 0;           break;
    }
    scene.key_press (k);
}
void on_mouse_button (int b, int state, int x, int y) {
    switch (b) {
    case GLUT_LEFT_BUTTON:   button = 1;  break;
    case GLUT_MIDDLE_BUTTON: button = 2;  break;
    case GLUT_RIGHT_BUTTON:  button = 3;  break;
    default:                 button = 0;  break;
    }
    switch (state) {
    case GLUT_UP:
        scene.button_release (button, x, y);
        break;
    case GLUT_DOWN:
        scene.button_press (button, x, y);
        break;
    }
}
void on_mouse_move (int x, int y) {
    scene.mouse_motion (button, x, y);
}
void display (void) {
    scene.render ();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    scene.resize (width, height);
}
void timeout (int value) {
    display ();
    glutTimerFunc (int(1000.0/fps), timeout, 1);
}
float my_function (float x, float y) {
    return (1-x/2+pow(x,5)+pow(y,3))*exp(-x*x-y*y);
}
void apply_function (std::string function) {
    FunctionParser fparser;
    int res = fparser.Parse(function, "x,y");
    if (res >= 0) {
        terminal->print (std::string(fparser.ErrorMsg()) + "\n\n");
        return;
    }
    double vals[2];
    float min = 0;
    float max = 0;
    for (int y=0; y<100; y++) {
        for (int x=0; x<100; x++) {
            vals[0] = -1.0+ x/100.0*2.0f;
            vals[1] = -1.0+ y/100.0*2.0f;
            float value = fparser.Eval(vals);
            *(data->get_data(x,y)) = value;
            if (value > max) max = value;
            if (value < min) min = value;
        }
    }
    surf->cmap()->scale  (min, max);
    if (max > min) {
        surf->set_size     (1,1, 1.0/(max-min));
        surf->set_position (0,0, -min/(max-min)-.5);
    } else {
        surf->set_size     (1,1,1);
        surf->set_position (0,0,0);
    }
    frame->set_z_range (Range (max, min, 4, 20));
    surf->update();   
}
void
terminal_event (Terminal *terminal, std::string input) {
    apply_function (input);
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("OpenGL viewer");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutMouseFunc (on_mouse_button);
    glutMotionFunc (on_mouse_move);
    glutKeyboardFunc (on_key_press);
    glutSpecialFunc (on_special_key_press);
    glutReshapeWindow (800,800);
    glutTimerFunc (int(1000.0/fps), timeout, 1);

    terminal = new Terminal();
    terminal->set_prompt ("f(x,y) = ");
    terminal->connect (terminal_event);
    terminal->hide();
    scene.add (terminal);

    colorbar = new Colorbar();
    colorbar->set_cmap (Colormap::IceAndFire());
    Colormap::IceAndFire()->scale(-1,1);
    scene.add (colorbar);

    surf = new SmoothSurface ();
    surf->set_br_color (0,0,0,0);
    data = new Data();
    data->set_data (my_function, -1,1, -1,1, 100);
    surf->set_data (data);
    surf->set_cmap (Colormap::IceAndFire());
    surf->set_size (1, 1,.25);
    surf->set_position (0,0,0);
    surf->update();
    scene.add (surf);    

    frame = new Frame();
    scene.add (frame);

    scene.set_zoom (1.25);
    scene.build();

    glutMainLoop();
    return 0;
}

