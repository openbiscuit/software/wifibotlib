/*
 * Copyright (C) 2008 Nicolas P. Rougier
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <Glut/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include "scene.h" 
#include "widget.h" 

static Scene scene;
static int button = 0;

void on_mouse_button (int b, int state, int x, int y) {
    int modifier = glutGetModifiers();

    switch (b) {
    case GLUT_LEFT_BUTTON:   button = 1;  break;
    case GLUT_MIDDLE_BUTTON: button = 2;  break;
    case GLUT_RIGHT_BUTTON:  button = 3;  break;
    default:                 button = 0;  break;
    }
    switch (state) {
    case GLUT_UP:
        scene.button_release (button, x, y, modifier);
        break;
    case GLUT_DOWN:
        scene.button_press (button, x, y, modifier);
        break;
    }
}
void on_mouse_move (int x, int y) {
    scene.mouse_motion (button, x, y);
}
void display (void) {
    scene.render ();
    glutSwapBuffers();
}
void reshape (int width, int height) {
    scene.resize (width, height);
}
void timeout (int value) {
    display ();
    glutTimerFunc (int(1000.0/30), timeout, 1);
}
int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow ("Widget default rendering");
    glutReshapeFunc (reshape);
    glutDisplayFunc (display);
    glutMouseFunc (on_mouse_button);
    glutMotionFunc (on_mouse_move);
    glutReshapeWindow (256,256);
    glutTimerFunc (int(1000.0/30), timeout, 1);

    glEnable (GL_DEPTH_TEST);
    glClearDepth (1.0f); 
    glDepthFunc (GL_LEQUAL);
    glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glShadeModel (GL_SMOOTH);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);

    Widget widget1;
    widget1.set_name("Widget1");
    widget1.set_bg_color (.5,.5,.5,.25);
    widget1.set_br_color (0,0,0,1);
    widget1.set_gravity (1,1);
    widget1.set_position (20,20,1);
    widget1.set_size (.5,.5);
    widget1.set_radius (12);

    Widget widget2;
    widget2.set_name("Widget2");
    widget2.set_bg_color (.5,.5,.5,.25);
    widget2.set_br_color (0,0,0,1);
    widget2.set_gravity (1,1);
    widget2.set_position (50,50,2);
    widget2.set_size (.5,.5);
    widget2.set_radius (12);

    Widget widget3;
    widget3.set_name("Widget3");
    widget3.set_bg_color (.5,.5,.5,.25);
    widget3.set_br_color (0,0,0,1);
    widget3.set_gravity (1,1);
    widget3.set_position (80,80,3);
    widget3.set_size (.5,.5);
    widget3.set_radius (12);

    scene.add (&widget1);
    scene.add (&widget2);
    scene.add (&widget3);

    glutMainLoop();
    return 0;
}

