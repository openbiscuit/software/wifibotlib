# Copyright (C) 2008 Nicolas P. Rougier
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

PLATFORM	= $(shell uname)
CC			= g++
CXXFLAGS	= -Wall -O3 -I../src
LIBS		= -lGL -lglut -lpng -ljpeg
ifeq ($(PLATFORM), Darwin)
	CXXFLAGS=	-Wall -O3  -I../src -I/opt/local/include
	LIBS	=	-framework OpenGL -framework GLUT  -L/opt/local/lib -lpng -ljpeg -lz
endif

.PHONY: all
all: examples

get = $(addprefix ../src/, $(addsuffix .o, $1))

# Function that define a target
# target is   $(1)
# sources are $(1).o and  ../src/{$(2)}.o
define EXAMPLE
$(1): $(1).o $(addsuffix .o, $2) $(call get, $(3))
	$(CC) -o $1 $(1).o $(addsuffix .o, $2) $(call get, $(3)) $(LIBS)
OBJS += $(1).o
EXAMPLES += $(1)
endef

$(eval $(call EXAMPLE, glut-object, ,        object))
$(eval $(call EXAMPLE, glut-widget, ,        object widget))
$(eval $(call EXAMPLE, glut-widgets, ,       object widget scene trackball))
$(eval $(call EXAMPLE, glut-colorbar, ,      object widget font colormap colorbar))
$(eval $(call EXAMPLE, glut-colorbars, ,     object widget font colormap colorbar))
$(eval $(call EXAMPLE, glut-text, ,          object widget text font))
$(eval $(call EXAMPLE, glut-font, ,          object font))
$(eval $(call EXAMPLE, glut-fonts, ,         object font))
$(eval $(call EXAMPLE, glut-viewport, ,      object widget viewport))
$(eval $(call EXAMPLE, glut-terminal, ,      object widget font terminal))
$(eval $(call EXAMPLE, glut-main, ,object widget font colormap colorbar data scene trackball terminal frame axis smooth-surface))
$(eval $(call EXAMPLE, glut-smooth-surface, ,object widget font colormap colorbar data scene trackball smooth-surface))
$(eval $(call EXAMPLE, glut-heightmap, ,     object widget font colormap colorbar data scene trackball smooth-surface))
$(eval $(call EXAMPLE, glut-flat-surface, ,  object widget font colormap colorbar data scene trackball flat-surface))
$(eval $(call EXAMPLE, glut-image, ,         object widget font colormap colorbar data scene trackball flat-surface))
$(eval $(call EXAMPLE, glut-viewer, \
	                   fparser fpoptimizer, \
	                   object widget font colormap colorbar data scene trackball terminal frame axis smooth-surface))

examples: $(EXAMPLES)

%.o : %.cc
	$(CC) -c $(CXXFLAGS) $< -o $@

clean:
	@-rm -f $(EXAMPLES)
	@-rm -f $(OBJS)
distclean: clean
	@-rm -f *~

