#include <iostream>
#include <memory.h>
#include "CThread.h"
#include "Singleton.h"
#include "List.h"
#include "CTcp.h"
#include "CImage.h"
#include "CCamDriverImage.h"
#include "CListImage.h"
#include "XmlParser.h"
#include "CConfig.h"

using namespace std;

CCamDriverImage::CCamDriverImage(void)
{
  _bKnonwSize = false;
  _buffer = new char[CDI_MAX_BUFFER];
  _bRun = true;
}

CCamDriverImage::~CCamDriverImage(void)
{
  delete _buffer;
}

void CCamDriverImage::clear(void)
{
  _bRun = false;
  StopClient();
}

void CCamDriverImage::setup(void)
{

}

void CCamDriverImage::execute(void)
{
  int ret;
  char *p;
  
  int len;

  if(Connect(CConfig::getInstance()->cam_address, CConfig::getInstance()->cam_sock) == false)
    {
      printf("Impossible de se connecter Image !\n");
      fflush(stdout);
      return ;
    }
  if(Send((char *)"GET /mjpeg.cgi HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n") == false)
    return ;
  while(_bRun == true)
    {
      memset(_buffer, 0, CDI_MAX_BUFFER);
      ret = ReadLine(_buffer, CDI_MAX_BUFFER);
      //	printf("%s\n", _buffer);
      if(ret > 0)
	{
	  p = strstr(_buffer, "Content-length: ");
	  if (p != NULL)
	    {
	      p +=  16;
	      _size = atoi(p);
	    }
	  p = strstr(_buffer, "Content-type: image/jpeg");
	  if(p != NULL)
	    {
	      memset(_buffer, 0, CDI_MAX_BUFFER);
	      ret = ReadLine(_buffer, CDI_MAX_BUFFER);
	      
	      memset(_buffer, 0, CDI_MAX_BUFFER);
	      len = 0;
	      LoopReceive(_buffer, _size);
	      if(_bKnonwSize == false)
		{
		  _bKnonwSize = true;
		  CImage getSize;
		  getSize.set_buffer((unsigned char *)_buffer, CIT_JPG);
		  if(getSize.convert_to(CIT_PPM) == true)
		    {
		      CConfig::getInstance()->_width = getSize._width;
		      CConfig::getInstance()->_height = getSize._height;
		      CConfig::getInstance()->_init = true;
		    }
		}
	      else
		if( CListImage::getInstance()->GetLength() < 10 )
			      CListImage::getInstance()->add_picture((unsigned char *)_buffer, _size);
	    }
	}
      usleep(1);
    }
  _bRun = true;
}
