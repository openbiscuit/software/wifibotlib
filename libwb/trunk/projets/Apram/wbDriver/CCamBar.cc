#include <iostream>
#include <gtkmm.h>

#include "CThread.h"
#include "Singleton.h"
#include "CCam.h"

#include "CCamDriver.h"

#include "CCamBar.h"


CCamBar::CCamBar(void)
{
  _offset = 5;
  set_default_size(300, 100);
  set_title("Camera toolbar");
  add(m_VBox);
    
  m_Entry.set_max_length(5);
  m_Entry.set_text("5");
  m_VBox.pack_start(m_Entry);
  m_VBox.add(m_ButtonBox);
  _bHome.add_pixlabel("icons/camera.png", "");
  _bUpd.add_pixlabel("icons/up.png", "");
  _bDown.add_pixlabel("icons/kdevelop_down.png", "");
  _bRight.add_pixlabel("icons/next.png", "");
  _bLeft.add_pixlabel("icons/previous.png", "");


  m_ButtonBox.pack_start(_bHome, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bUpd, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bDown, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bRight, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bLeft, Gtk::PACK_SHRINK);

  m_ButtonBox.set_border_width(5);

  _bUpd.signal_clicked().connect(sigc::mem_fun(*this,
	       &CCamBar::on_button_up) );
  _bDown.signal_clicked().connect(sigc::mem_fun(*this,
             &CCamBar::on_button_down) );
  _bRight.signal_clicked().connect(sigc::mem_fun(*this,
             &CCamBar::on_button_right) );
  _bLeft.signal_clicked().connect(sigc::mem_fun(*this,
             &CCamBar::on_button_left) );
  _bHome.signal_clicked().connect(sigc::mem_fun(*this,
             &CCamBar::on_button_home) );
  show_all_children();
}

CCamBar::~CCamBar(void)
{

}

void CCamBar::getOffset(void)
{
  _offset = atoi((char *)m_Entry.get_text().c_str());
}

void CCamBar::on_button_up()
{
  getOffset();
  CCamDriver::getInstance()->_camMovement.up(_offset);
}

void CCamBar::on_button_down()
{
  CCamDriver::getInstance()->_camMovement.down(_offset);
}

void CCamBar::on_button_left()
{
  getOffset();
  CCamDriver::getInstance()->_camMovement.left(_offset);
}

void CCamBar::on_button_right()
{
  getOffset();
  CCamDriver::getInstance()->_camMovement.right(_offset);
}

void CCamBar::on_button_home()
{
  getOffset();
  CCamDriver::getInstance()->_camMovement.home();
}
