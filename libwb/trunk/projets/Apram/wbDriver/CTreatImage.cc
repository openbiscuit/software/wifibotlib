#include <iostream>

#include "CTreatImage.h"
#include "Singleton.h"
#include "XmlParser.h"
#include "CConfig.h"

CTreatImage::CTreatImage(void)
{
  _hsl = new CHsl(CConfig::getInstance()->_width,
  	 CConfig::getInstance()->_height);
  _width = CConfig::getInstance()->_width;
  _height = CConfig::getInstance()->_height;
}

CTreatImage::~CTreatImage(void)
{
  delete _hsl;
}

void CTreatImage::setHslPic(CImage *pic)
{
  _img = pic;
}


void CTreatImage::init_surface(CTI_Surface *surf)
{
  surf->left_up.x = surf->left_down.x = surf->right_up.x = surf->right_down.x = 0;
  surf->left_up.y = surf->left_down.y = surf->right_up.y = surf->right_down.y = 0;
}

bool CTreatImage::FindTheTarget(void)
{
  int i = 0, j = 0;
  int lineok = 0;

  int indice_targ;
  int indice = -1;
  unsigned char old_color = 0;
  int maxSize = 0;
  int selLine = -1;
  int last_line = -1;
  init_surface(&_red_circle);
  init_surface(&_yellow_circle);
  _hsl->convert(_img);
  //  _img->save("2.bmp");
  // init des trois séquence de couleur que l'on cherche
  for(int k=0;k<3;k++)
    {
      target[k].color = 0;
      target[k].start = 0;
      target[k].stop = 0;

      tmp_target[k].color = 0;
      tmp_target[k].start = 0;
      tmp_target[k].stop = 0;
    }
  indice_targ = 0;
  
  // parcours de l'image
  for(j=0;j<_hsl->_height;j++)
    {
      indice = 0;
      old_color = 0;
      // ** ETAPE 1 **
      // séquence chaque couleur dans un tableau sequence
      // avec couleur pixel de début et pixel de fin
      for(i=0;i<_hsl->_width;i++)
	{
	  if(!_hsl->teinte[i][j].iswhite && !_hsl->teinte[i][j].isblack)
	    {
	      if(_hsl->teinte[i][j].valeur != old_color)
		{
		  old_color = _hsl->teinte[i][j].valeur;
		  _hsl->teinte[i][j].on = 1;
		  sequence[indice].color = _hsl->teinte[i][j].valeur;
		  sequence[indice].start = i;
		  old_color = _hsl->teinte[i][j].valeur;
		  
		  // si dans la séquence 3 pixels sont de différentes couleurs
		  // on considère que c'est la même séquence
		  int gap = 0;
		  while(i < _hsl->_width)
		    {
		      if(_hsl->teinte[i][j].valeur != old_color 
			 || _hsl->teinte[i][j].iswhite || _hsl->teinte[i][j].isblack )
			gap++;
		      if(gap >= 2)
			break;
		      i++;
		    }
		  sequence[indice].stop = i;
		  
		  indice ++;
		}  
	    }
	  else
	    old_color = 255;
	}

      // ** ETAPE 2 **
      // sélection de la bonne séquence depuis
      // l'analyse faite précedemment
      for(int k=0;k<indice;k++)
	{
	  // il faut au moins CTI_ACCURENCY pixels pour que l'on considère qu'il s'agit
	  // d'une séquence
	  if((sequence[k].stop-sequence[k].start) <  CTI_ACCURENCY ) // filtrage pour de px
	    continue ;

	  if(sequence[k].color == COLOR_OF_THE_THIRD_CIRCLE)
	    {
	      // _hsl->teinte[sequence[k].start][j].on = 1;
	      
	      tmp_target[indice_targ].color = COLOR_OF_THE_THIRD_CIRCLE;
	      tmp_target[indice_targ].start = sequence[k].start;
	      tmp_target[indice_targ].stop = sequence[k].stop;
	      
	      if(indice_targ == 0)
		indice_targ++;
	      if(indice_targ == 2)
		{
		  // vérification ...
		  int size1 = tmp_target[0].stop - tmp_target[0].start;
		  int size3 = tmp_target[2].stop - tmp_target[2].start;
		  if(abs(size1-size3) < CTI_ACCURENCY 
		     && (tmp_target[1].start-tmp_target[0].stop) < size1+ CTI_ACCURENCY
		     && (tmp_target[2].start-tmp_target[1].stop) < size3+ CTI_ACCURENCY
		     && (tmp_target[1].start-tmp_target[0].stop) >  CTI_ACCURENCY 
		     &&  (tmp_target[2].start-tmp_target[1].stop) >   CTI_ACCURENCY)
		    {
		      //  printf("> %d %d %d\n", tmp_target[0].start, (tmp_target[2].start-tmp_target[1].stop),  size3);
		      k = indice;
		      break;
		    }
		  else
		    indice_targ = 0;
		}
	    }
	  
	  if(sequence[k].color == COLOR_OF_THE_FIRST_CIRCLE)
	    {
	      _hsl->teinte[sequence[k].start][j].on = 1;
	      if(indice_targ == 1)
		{
		  
		  tmp_target[indice_targ].color = COLOR_OF_THE_FIRST_CIRCLE;
		  tmp_target[indice_targ].start = sequence[k].start;
		  tmp_target[indice_targ].stop = sequence[k].stop;
		  indice_targ++;
		}
	    }
	}
      if(indice_targ == 2 
	 &&  (tmp_target[2].stop - tmp_target[0].start)> CTI_SIZE_TARGET_MIN ) // j'ai les trois teinte
	{
	  //  printf(" start %d %d\n",  tmp_target[0].start, j);
	  int size =  tmp_target[2].stop - tmp_target[0].start;
	  last_line = j;
	  lineok++;
	  //    _hsl->teinte[tmp_target[0].start][j].on = 1;
	  if(size > maxSize && lineok >  CTI_ACCURENCY)
	    {
	      for(int u=0;u<3;u++)
		{
		  target[u].start = tmp_target[u].start;
		  target[u].stop = tmp_target[u].stop;
		  target[u].color = tmp_target[u].color;
		}
	      maxSize = size;
	      selLine = j;
	    }
	}
      else
	{
	  if(j - last_line >  CTI_ACCURENCY)
	    lineok = 0;
	}
    }

  // ** ETAPE 3 **
  // cible trouvée on met à jour les structures
  if(selLine != -1)
    {
      _red_circle.left_up.x = target[0].start;
      _red_circle.left_up.y = selLine-5;
      _red_circle.right_down.x = target[2].stop;
      _red_circle.right_down.y = selLine+5;
      
      _yellow_circle.left_up.x = target[1].start;
      _yellow_circle.left_up.y = selLine-10;
      _yellow_circle.right_down.x = target[1].stop;
      _yellow_circle.right_down.y = selLine+10;
      return true;
    }

   _hsl->build();

  return false;
}
