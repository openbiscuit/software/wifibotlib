#ifndef CCAMDISPLAY_H
#define CCAMDISPLAY_H

#include "CThread.h"
#include "CCam.h"

#include <gtkmm.h>

class CCamDisplay : public CThread, public Gtk::Window
{
 public:
  CCamDisplay(void);
  ~CCamDisplay(void);
  ///< rendu de la caméra
  CCam camA;
  ///< flag pour savoir si la cible a été trouvée ou non
  bool _bFoundTarget;
  ///< dernière largeur connu de la cible dans l'image
  int _largeur;
  ///< position de la cible dans l'image
  int _centre;
 private:
  ///< hook des touches
  int on_keyb(Gtk::Widget *,GdkEventKey *);

  /**
   * affichage de la caméra en hsl
   */
  virtual void on_button_hsl();
  /**
   * affichage de la caméra en bitmap
   */
  virtual void on_button_bmp();
  /**
   * unuse
   */
  virtual void on_button_red();
  /**
   * ununse
   */
  virtual void on_button_yellow();
 protected:
  void setup(void); ///<setup du thread
  void execute(void); ///< exécution du thread

  ///< conteneur de l'affichage
  Gtk::VBox m_VBox;
  ///< menu de la caméra
  Gtk::MenuBar menuBar;
  ///< sous menu de la caméra
  Gtk::Menu mAff;
  ///< type d'affichage
  char type_aff;
};

#endif

