#include <iostream>
#include <gtkmm.h>
#include <string.h>

#include "List.h"
#include "Singleton.h"
#include "CThread.h"
#include "CTcp.h"
#include "CImage.h"
#include "CCam.h"
#include "CCamDriverImage.h"
#include "CCamDriverMovement.h"
#include "CCamDriver.h"
#include "CCamDisplay.h"
#include "CCamBar.h"
#include "CHsl.h"
#include "CTreatImage.h"
#include "XmlParser.h"
#include "CConfig.h"
#include "CWifibotDriver.h"

#include "CWBBar.h"
#include "CReport.h"

#include "CConfig.h"



CWBBar::CWBBar(void) : m_Button_Quit("Quit")
{
  set_default_size(300, 100);
  set_title("WifiBot bar");
  add(m_VBox);

  m_VBox.pack_start(m_ButtonBox);
  m_VBox.add(m_ButtonBox2);

  _bPause.add_pixlabel("icons/player_stop.png", "stop");
   _bQuery.add_pixlabel("icons/agt_business.png", "query");
  _bPlay.add_pixlabel("icons/player_play.png", "play");
  _bRecord.add_pixlabel("icons/gear.png", "learn");
  _bQuit.add_pixlabel("icons/exit.png", "quit");

  _bUpd.add_pixlabel("icons/up.png", "");
  _bDown.add_pixlabel("icons/kdevelop_down.png", "");
  _bRight.add_pixlabel("icons/next.png", "");
  _bLeft.add_pixlabel("icons/previous.png", "");
  _bStop.add_pixlabel("icons/stop.png", "");

  m_ButtonBox2.pack_start(_bUpd, Gtk::PACK_SHRINK);
  m_ButtonBox2.pack_start(_bDown, Gtk::PACK_SHRINK);
  m_ButtonBox2.pack_start(_bRight, Gtk::PACK_SHRINK);
  m_ButtonBox2.pack_start(_bLeft, Gtk::PACK_SHRINK);
  m_ButtonBox2.pack_start(_bStop, Gtk::PACK_SHRINK);

  m_ButtonBox.pack_start(_bPause, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bPlay, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bQuery, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bRecord, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(_bQuit, Gtk::PACK_SHRINK);

  _bPlay.signal_clicked().connect(sigc::mem_fun(*this,
              &CWBBar::on_button_play) );
  _bQuery.signal_clicked().connect(sigc::mem_fun(*this,
              &CWBBar::on_button_query) );
  _bRecord.signal_clicked().connect(sigc::mem_fun(*this,
              &CWBBar::on_button_learn) );
  _bPause.signal_clicked().connect(sigc::mem_fun(*this,
              &CWBBar::on_button_stop) );
  _bQuit.signal_clicked().connect(sigc::mem_fun(*this,
              &CWBBar::on_button_quit) );

  menuBar.items().push_back( Gtk::Menu_Helpers::MenuElem("_File", fileMenu) );

  fileMenu.items().push_back( Gtk::Menu_Helpers::MenuElem("save" ,
		   sigc::mem_fun(*this, &CWBBar::on_button_save)));
  fileMenu.items().push_back( Gtk::Menu_Helpers::MenuElem("load" ,
		   sigc::mem_fun(*this, &CWBBar::on_button_load)));

  fileMenu.items().push_back( Gtk::Menu_Helpers::MenuElem("export raw" ,
		   sigc::mem_fun(*this, &CWBBar::on_button_export_raw)));
  fileMenu.items().push_back( Gtk::Menu_Helpers::MenuElem("export interpolation" ,
		   sigc::mem_fun(*this, &CWBBar::on_button_export_interpol)));


  menuBar.items().push_back( Gtk::Menu_Helpers::MenuElem("Fenetres", mWindow) );
  mWindow.items().push_back( Gtk::Menu_Helpers::MenuElem("Camera" , 
		    sigc::mem_fun(*this, &CWBBar::on_button_camera) ));
  mWindow.items().push_back( Gtk::Menu_Helpers::MenuElem("Camera toolbar" , 
		    sigc::mem_fun(*this, &CWBBar::on_button_cameratb) ));

  m_VBox.pack_start(menuBar);

  _bUpd.signal_clicked().connect(sigc::mem_fun(*this,
	       &CWBBar::on_button_fd) );
  _bDown.signal_clicked().connect(sigc::mem_fun(*this,
             &CWBBar::on_button_bd) );
  _bRight.signal_clicked().connect(sigc::mem_fun(*this,
             &CWBBar::on_button_right) );
  _bLeft.signal_clicked().connect(sigc::mem_fun(*this,
             &CWBBar::on_button_left) );
  _bStop.signal_clicked().connect(sigc::mem_fun(*this,
             &CWBBar::on_button_wb_stop) );
  _state =  CWWB_state_stop;

  distance = -1;
  angle = -1;

  dis = new CCamDisplay();
  dis->start(NULL);
  cam_bar = new CCamBar();

  dis->show();

  peng = NULL;
  kdtree = new CKdtree();
  smart = new CHedger(kdtree);

  CConfig *cfg = CConfig::getInstance();
  int i = 0;
  while(i < cfg->_list_params.GetLength())
    {
      CConfig_param *p = (CConfig_param *)cfg->_list_params.Get(i);
      i++;
      if(p->value < 0) continue;
      if(strstr(p->name, "bandwith"))
	 smart->_bandwith = p->value;
      if(strstr(p->name, "nb_points_min"))
	 smart->_nb_points = p->value;
      if(strstr(p->name, "nb_points_max"))
	 smart->_nb_points_max = p->value;
      if(strstr(p->name, "alpha"))
	 smart->_alpha = p->value;
      if(strstr(p->name, "gamma"))
	 smart->_gamma = p->value;
      if(strstr(p->name, "lambda"))
	{
	  peng = new CPeng(smart);
	  peng->_lambda = p->value;
	}
      if(strstr(p->name, "threshold"))
	 smart->seuil = p->value;
    }

  printf("Configuration des algos ...\n");
  printf("Hedger h(%f) pts min(%d) pts max(%d) alpha(%f) gamma(%f) threshold(%f)\n",
	 smart->_bandwith, smart->_nb_points, smart->_nb_points_max,
	 smart->_alpha, smart->_gamma, smart->seuil);
  if(peng != NULL)
    printf("Peng lambda(%f)\n", peng->_lambda);
  fflush(stdout);

  printf("Lecture du fichier de traces ...\n");
  if(smart->readTraces((char *)"./traces.txt") == false)
    printf("Impossible de lire le fichier !\n");

  show_all_children();
}

CWBBar::~CWBBar(void)
{
  delete dis;
  delete cam_bar;

  delete kdtree;
  delete smart;
  if(peng) delete peng;
}

void CWBBar::setup(void)
{
}

void CWBBar::execute(void)
{
  distance = angle = -2;
  algo_step();
}

void CWBBar::algo_step(void)
{
  char action_str[30];
  KdVector *x;
  KdVector *xp;
  double val;
  KdVectorList *list = NULL;
  double mindist = 0;
  int sts = 0;
  if(_state != CWWB_state_learn)
    {
      // quelle est la meilleure action ?
      sleep(5); // on attend la fin de l'action
      dis->_bFoundTarget = false;
      distance = angle = -2;
      sleep(2);
      
      if(dis->_bFoundTarget == true)
	{
	  // normalisation des entrées
	  distance = (double)(dis->_largeur - CWBBAR_DIST_MIN) / CWBBAR_DIST_SIZE; 
	  angle = (double)(dis->_centre - CWBBAR_ANGLE_MIN) / CWBBAR_ANGLE_SIZE;
	}
      else
	{
	  distance = angle = -2;
	}
      
      xp = CKdtree::NewKdVector(3);
      xp->value[0] = distance;
      xp->value[1] = angle;
    //  smart->_verbosity = 3;
      printf("Etat [%f:%f]\n",  distance, angle);
      for(int i=0;i<=HEDGER_ACTION_TURNONRIGHT;i++)
	{
	  xp->value[2] = i;
	  list =  CKdtree::NewKdVectorList();
	  //smart->_verbosity = 3;
	  //smart->S->_verbosity = 3;
	  val = smart->Prediction(xp, list, &mindist, &sts);
	  
	  smart->ActionToString(i, action_str);
	  printf("Action %s Q-value %f status %d\n",  action_str, val, sts);
	  smart->S->FreeMemWithoutVector(list->head);
	  delete list;

	 } 
       smart->S->FreeMem(xp);
      return ;
    }

  static double old_distance = distance;
  static double old_angle = angle;

  sleep(5); // on attend la fin de l'action
  dis->_bFoundTarget = false;
  distance = angle = -2;
  sleep(2);

  if(dis->_bFoundTarget == true)
    {
      // normalisation des entrées
      distance = (double)(dis->_largeur - CWBBAR_DIST_MIN) / CWBBAR_DIST_SIZE; 
      angle = (double)(dis->_centre - CWBBAR_ANGLE_MIN) / CWBBAR_ANGLE_SIZE;
    }
  else
    {
      distance = angle = -2;
    }

  smart->_rewards = 0;
  // mettre une distance maximale
  if(distance > 0.6)
    {
      smart->_rewards = 10;
    }

  /*  if(distance > 200)
    {
      smart_peng->_rewards = -10;
      smart->_rewards = -10;
      }*/
  
  smart->ActionToString(_act, action_str);
  printf("apprentissage [%s] dist : %f angle : %f\n", 
	 action_str, distance, angle);
      

  x = CKdtree::NewKdVector(2);
  x->value[0] = old_distance;
  x->value[1] = old_angle;

  xp = CKdtree::NewKdVector(2);
  xp->value[0] = distance;
  xp->value[1] = angle;
  
  int best_action;

  best_action = smart->Training(x, xp, _act);

  smart->ActionToString(best_action, action_str);
  printf("best act : %s nb pts : %d rewards : %d\n",
	 action_str, smart->S->GetTotalSize(), smart->_rewards);
  
  CReport::getInstance()->AddTrace("%f\t%f\t%d\t%f\t%f\t%d",
				   old_distance, old_angle,
				   _act,
				   distance, angle,
				   smart->_rewards);

  old_distance = distance;
  old_angle = angle;
  fflush(stdout); 
  dis->_bFoundTarget = false;
}


void CWBBar::on_button_quit()
{
  Gtk::Main::quit();
}

void CWBBar::on_button_fd()
{
  CWifibotDriver::getInstance()->forward(25, 1);
  
  _act =  HEDGER_ACTION_FORWARD;
  this->start(NULL);
}

void CWBBar::on_button_bd()
{
  CWifibotDriver::getInstance()->backward(25, 1);
  
  _act =  HEDGER_ACTION_BACKWARD;
  this->start(NULL);
}

void CWBBar::on_button_wb_stop()
{
  CWifibotDriver::getInstance()->stop();
  
  _act =  HEDGER_ACTION_NOTHING;
  this->start(NULL);
}

void CWBBar::on_button_left()
{
  CWifibotDriver::getInstance()->turn_on_left(25, 1);
  
  _act =  HEDGER_ACTION_TURNONLEFT;
  this->start(NULL);
}

void CWBBar::on_button_right()
{
  CWifibotDriver::getInstance()->turn_on_right(25, 1);
  
  _act =  HEDGER_ACTION_TURNONRIGHT;
  this->start(NULL);
}

void CWBBar::on_button_camera()
{
  if(dis->is_visible())
    dis->hide();
  else
    dis->show();
}

void CWBBar::on_button_cameratb()
{
  if(cam_bar->is_visible())
    cam_bar->hide();
  else
    cam_bar->show();
}

void CWBBar::on_button_save()
{
  kdtree->SaveTo((char *)"./kdtree.txt");
}

void CWBBar::on_button_load()
{
  kdtree->LoadFrom((char *)"./kdtree.txt");
}

void CWBBar::on_button_learn()
{
  _state =  CWWB_state_learn;
}

void CWBBar::on_button_stop()
{
  distance = -2;
  angle = -2;
  kdtree->SaveTo((char *)"./kdtree.txt");
  _state =  CWWB_state_stop;
}

void CWBBar::on_button_play()
{
  _state =  CWWB_state_play;
}

void CWBBar::on_button_query()
{ 
  KdVector *x;
  char action_str[30];
  if(dis->_bFoundTarget == true)
    {
      // normalisation des entrées
      distance = (double)(dis->_largeur - CWBBAR_DIST_MIN) / CWBBAR_DIST_SIZE; 
      angle = (double)(dis->_centre - CWBBAR_ANGLE_MIN) / CWBBAR_ANGLE_SIZE;
    }
  else
    {
      distance = angle = -2;
    }
  x = CKdtree::NewKdVector(3);
  x->value[0] = distance;
  x->value[1] = angle;
  x->value[2] = 0.0;

  int best_action = smart->BestAction(x);
  smart->ActionToString(best_action, action_str);
  printf("Meilleure action : %s\n", action_str);
  
  CKdtree::FreeMem(x);
}

void CWBBar::on_button_export_raw()
{
  CKdtreeExport *spe = new CKdtreeExport();
  spe->setLimits(40, 40, 240, 440);
  spe->setPrecision(2, 4);
  spe->setKdtree(smart);
  spe->Raw((char *)"./raw.txt", HEDGER_ACTION_FORWARD);
}

void CWBBar::on_button_export_interpol()
{
  CKdtreeExport *spe = new CKdtreeExport();
  spe->setLimits(40, 40, 240, 440);
  spe->setPrecision(2, 4);
  spe->setKdtree(smart);
  spe->Interpol((char *)"./interpol.txt", HEDGER_ACTION_FORWARD);
}
