#ifndef CWIFIBOTDRIVER_H
#define CWIFIBOTDRIVER_H

#include "CThread.h"
#include "Singleton.h"
#include "List.h"
#include "CTcp.h"
#include "CImage.h"
#include "CListImage.h"
#include "XmlParser.h"
#include "CConfig.h"


#define CWD_LEN_TAB_CMD 7 ///< nombre d'octets des commandes

#define NEW_PROT_LIBWB 1 ///< pour activer la nouvelle librairie wifibot

class CWifibotDriver : public CThread, public CTcp, public Singleton<CWifibotDriver>
{
 public:
  CWifibotDriver(void);
  ~CWifibotDriver(void);
  /**
   * envoi la commande avancer au wifibot
   * @param speed vitesse
   * @param timeout temps d'application de la commande
   */
  bool forward(int speed, int timeout);

  /**
   * envoi la commande reculer au wifibot
   * @param speed vitesse
   * @param timeout temps d'application de la commande
   */
  bool backward(int speed, int timeout);

  /**
   * envoi la commande tourner à droite
   * @param speed vitesse
   * @param timeout temps d'application de la commande
   */
  bool turn_on_right(int speed, int timeout);

  /**
   * envoi la commande tourner à gauche
   * @param speed vitesse
   * @param timeout temps d'application de la commande
   */
  bool turn_on_left(int speed, int timeout);

  /**
   * envoi la commande stopper les moteurs
   */
  bool stop(void);
 protected:
  /**
   * unuse
   */
  void setup(void);

  /**
   * envoi de la commande au wifibot
   */
  void execute(void);
 private:
  bool _WillSend; ///< flag d'envoi d'une commande
  char _command[CWD_LEN_TAB_CMD];
};

#endif

