#ifndef HSL_H
#define HSL_H

#include "CImage.h"

///< teinte maximum
#define TMAX 255
///< teinte à partir de laquelle on considère du blanc
#define seuil_blanc 180
///< teinte en dessus de laquelle on considère du noir
#define seuil_noir 32

/**
 * définition d'un pixel
 */
typedef struct Tpx
{
  unsigned char valeur; ///< valeur du pixel
  char traite; ///< traité ou non
  unsigned char iswhite; ///< pixel blanc
  unsigned char isblack; ///< pixel noir
  unsigned char on; ///< pour debug
} Tpx;

class CHsl
{
 public:
  CHsl(int w, int h);
  ~CHsl(void);
  /**
   * convertit l'image en hsl
   * @param img l'image à traitée
   */
  void convert(CImage *img);
  /**
   * pour le debug construit une image des séquences
   */
  void build(void);

  int _width; ///< largeur de l'image
  int _height; ///< hauteur de l'image
  Tpx **teinte; ///< tableau des teintes
 
 private:
  /**
   * convertion rvt vers hsl
   */
  void rvb_to_hsl(void);
  /**
   * convertion hsl vers rvb
   */
  void hsl_to_rvb(void);

  /**
   * La teinte sera codée par intervales de longueur 'div'
   * (0, div, 2*div, 3*div, ...).
   */
  void discretiser_teinte(int div);
		
  /**
   * Crée des pixels de taille 'sizeofpixel' dont la teinte est la moyenne
   * des teintes des pixels remplacés.
   */
  void pixeliser_teinte(int sizeofpixel);
  
  CImage *_img_base; ///< l'image en elle même
};

#endif
