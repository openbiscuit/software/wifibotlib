#include <iostream>
#include <memory.h>

#include "CWifibotDriver.h"

using namespace std;

CWifibotDriver::CWifibotDriver(void)
{
  _WillSend = false;
  memset(_command, 0, CWD_LEN_TAB_CMD);
}

CWifibotDriver::~CWifibotDriver(void)
{

}

void CWifibotDriver::setup(void)
{

}

void CWifibotDriver::execute(void)
{
  if(Connect(CConfig::getInstance()->wb_address, CConfig::getInstance()->wb_sock) == false)
    {
      printf("Impossible de se connecter wifibot !\n");
      fflush(stdout);
      return ;
    }
  //  while(1)
  //{
  //     while( _WillSend == false ) usleep(100);
      
  //     _WillSend = false;
      
  Send(_command, CWD_LEN_TAB_CMD);

  printf("cwb %c ",_command[0]);
  for(int i=1;i<7;i++)
    printf("%d ", _command[i]);
  printf("\n");
  fflush(stdout);
      memset(_command, 0, CWD_LEN_TAB_CMD);
       LoopReceive(_command,  CWD_LEN_TAB_CMD);
      memset(_command, 0, CWD_LEN_TAB_CMD);
      
        usleep(1);
	// }
    StopClient();
}

bool CWifibotDriver::forward(int speed, int timeout)
{
  memset(_command, 0, CWD_LEN_TAB_CMD);

  _command[0] = 'F';
  _command[1] = (unsigned char)speed;
  _command[2] = (unsigned char)speed;
  _command[3] = (unsigned char)timeout;

  _WillSend = true;
    execute();
  return true;
}

bool CWifibotDriver::backward(int speed, int timeout)
{
  memset(_command, 0, CWD_LEN_TAB_CMD);

  _command[0] = 'R';
  _command[1] = (unsigned char)speed;
  _command[2] = (unsigned char)speed;
  _command[3] = (unsigned char)timeout;

  _WillSend = true;
   execute();
  return true;
}

bool CWifibotDriver::turn_on_right(int speed, int timeout)
{
  memset(_command, 0, CWD_LEN_TAB_CMD);

  _command[0] = 'T';
  _command[1] = 1;
  _command[2] = (unsigned char)speed;
  _command[3] = (unsigned char)timeout;

  _WillSend = true;
    execute();
  return true;
}

bool CWifibotDriver::turn_on_left(int speed, int timeout)
{
  memset(_command, 0, CWD_LEN_TAB_CMD);

  _command[0] = 'T';
  _command[1] = 0;
  _command[2] = (unsigned char)speed;
  _command[3] = (unsigned char)timeout;

  _WillSend = true;
    execute();
  return true;
}

bool CWifibotDriver::stop(void)
{
  memset(_command, 0, CWD_LEN_TAB_CMD);

  _command[0] = 'K';

  _WillSend = true;
   execute();
  return true;
}
