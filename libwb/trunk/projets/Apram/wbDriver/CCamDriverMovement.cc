#include <iostream>
#include <memory.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <jpeglib.h>
#include <semaphore.h>

#include "Singleton.h"
#include "List.h"
#include "CThread.h"
#include "CTcp.h"
#include "CCamDriverMovement.h"
#include "CListImage.h"
#include "XmlParser.h"
#include "CConfig.h"

using namespace std;

CCamDriverMovement::CCamDriverMovement(void)
{ 
}


CCamDriverMovement::~CCamDriverMovement(void)
{

}

bool CCamDriverMovement::move(int pan, int tilt, CDM_Direction dir)
{
  bool ret;
  char buffer[CDM_SIZE_BUFFER];
  int size = 0;
  if(Connect(CConfig::getInstance()->cam_address, CConfig::getInstance()->cam_sock) == false)
    {
      printf("Impossible de se connecter !\n");
      fflush(stdout);
      return false;
    }
  size = sprintf(buffer, "PanSingleMoveDegree=%d&TiltSingleMoveDegree=%d&PanTiltSingleMove=%d", pan, tilt, dir);
  ret = Send((char *)"POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\n" \
	     "Authorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\n\0");
  ret += Sendf("Content-Length: %d\r\n\r\n\0", size);
  ret += Send(buffer, size);
  ret += Send((char *)"\r\n\r\n");
  memset(buffer, 0, CDM_SIZE_BUFFER);
  if(ret && LoopReceive(buffer, CDM_SIZE_BUFFER) > 0)
    {
      StopClient();
      return true;
    }
  StopClient();
  return false;
}

bool CCamDriverMovement::up(int offset)
{
  return move(0, offset, CDM_Top);
}

bool CCamDriverMovement::down(int offset)
{
  return move(0, offset, CDM_Bottom);
}

bool CCamDriverMovement::right(int offset)
{
  return move(offset, 0, CDM_Right);
}

bool CCamDriverMovement::left(int offset)
{
  return move(offset, 0, CDM_Left);
}

bool CCamDriverMovement::home(void)
{
  return move(0, 0, CDM_Home);
}
