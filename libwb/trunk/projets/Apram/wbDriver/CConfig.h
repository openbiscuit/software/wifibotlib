#ifndef CCONFIG_H
#define CCONFIG_H

#include "Singleton.h"
#include "XmlParser.h"
#include "List.h"

#define CCONFIG_FILENAME "config.xml"

/**
 * structure d'un paramètre de l'algo
 */
typedef struct
{
  char *name; ///< nom du paramètre
  float value; ///< value
} CConfig_param;

class CConfig : public Singleton<CConfig>, public CXmlParser
{
 public:
  CConfig(void);
  ~CConfig(void);

  /**
   * charge la configuration depuis le fichier xml
   */
  bool Load(void);
  
  char *cam_address; ///< adresse ip de la caméra
  int cam_sock; ///< socket de connexion
  char *wb_address; ///< adresse ip du wifibot
  int wb_sock; ///< socket de connexion du wifibot
  
  bool _init; ///< variable qui indique quand la config est chargée
  int _width;
  int _height; 
  CList _list_params; ///< liste des paramètres de l'algorithme

 private:
  /**
   * cette fonction lit un paramètre de l'algorithme
   * @param tag valeurs du paramètres
   */
  void read_once_param(xmlTag *tag);
};

#endif

