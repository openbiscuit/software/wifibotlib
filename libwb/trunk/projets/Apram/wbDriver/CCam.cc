#include <gtkmm.h>
#include <iostream>
#include <memory.h>

#include "CImage.h"
#include "CCam.h"

CCam::CCam()
{
  _l = _b = _w = _h = 0;
}

void CCam::setRectangle(int l, int b, int w, int h)
{
  _l = l;
  _b = b;
  _w = w;
  _h = h;
}

void CCam::setSize(int w, int h)
{
  _width = w;
  _height = h;
  set_size_request(_width, _height);
  b = new char[_width*_height*3];
  memset(b, 0, _width*_height*3);

  Glib::signal_timeout().connect(
                sigc::mem_fun(*this, &CCam::onSecondElapsed), 25);
}


CCam::~CCam()
{
  delete b;
}


bool CCam::on_expose_event(GdkEventExpose* event)
{
  Glib::RefPtr<Gdk::GC> gc = Gdk::GC::create(get_window());
  Glib::RefPtr<Gdk::Drawable> drawable = get_window();
  drawable->draw_rgb_image(gc, 
			   0,
			   0,
			   _width,
			   _height,
			   Gdk::RGB_DITHER_NONE,
			   (guchar *)b,
			   _width*3);


  drawable->draw_rectangle(this->get_style()->get_white_gc(), false, _l, 
			   _b, _w, _h);

  return true;
}

void CCam::setPic(CImage *tmpImg)
{
 for(int i=0;i<_width*_height*3;i++)
   b[i] = tmpImg->_buffer[i];
}



bool CCam::onSecondElapsed(void)
{
    // force our program to redraw the entire clock.
    Glib::RefPtr<Gdk::Window> win = get_window();
    if (win)
    {
        Gdk::Rectangle r(0, 0, get_allocation().get_width(),
                get_allocation().get_height());
        win->invalidate_rect(r, false);
    }
    return true;
}


