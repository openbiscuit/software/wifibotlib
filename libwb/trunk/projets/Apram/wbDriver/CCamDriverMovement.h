#ifndef CCAMDRIVERMOVEMENT_H
#define CCAMDRIVERMOVEMENT_H

#include "CTcp.h"

/**
 * liste des mouvements possible de la caméra
 */
typedef enum {
	CDM_TopLeft,
	CDM_Top,
	CDM_TopRight,
	CDM_Left,
	CDM_Home,
	CDM_Right,
	CDM_BottomLeft,
	CDM_Bottom,
	CDM_BottomRight
} CDM_Direction;

#define CDM_SIZE_BUFFER	100 ///< taille du buffer pour envoi des données

class CCamDriverMovement : public CTcp
{
 public:
 /**
  * constructeur
  */
  CCamDriverMovement(void);
  
/**
 * desctructeur
 */
  ~CCamDriverMovement(void);
/**
 * action vers le haut
 * @param offset nombre de pas Ã  rÃ©aliser
 * @return true envoyer correctement
 * @return false erreur lors de l'envoi
 */
	bool up(int offset);
/**
 * action vers le bas
 * @param offset nombre de pas Ã  rÃ©aliser
 * @return true envoyer correctement
 * @return false erreur lors de l'envoi
 */
	bool down(int offset);
/**
 * action vers la droite
 * @param offset nombre de pas Ã  rÃ©aliser
 * @return true envoyer correctement
 * @return false erreur lors de l'envoi
 */
	bool right(int offset);
/**
 * action vers la gauche
 * @param offset nombre de pas Ã  rÃ©aliser
 * @return true envoyer correctement
 * @return false erreur lors de l'envoi
 */
	bool left(int offset);
/**
 * action retour position initiale
 * @return true envoyer correctement
 * @return false erreur lors de l'envoi
 */
	bool home(void);
 
 private:
/**
 * réaliser un mouvement
 * @param pan pas Ã  rÃ©aliser en pan
 * @param tilt pas Ã  rÃ©aliser en tilt
 * @param dir mouvement Ã  rÃ©aliser
 * @return true envoyer correctement
 * @return false erreur lors de l'envoi
 */
 	bool move(int pan, int tilt, CDM_Direction dir);
 private:
	bool _bRun;
};


#endif

