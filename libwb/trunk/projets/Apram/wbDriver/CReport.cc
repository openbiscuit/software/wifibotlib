#include <iostream>
#include <gtkmm.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <jpeglib.h>
#include <semaphore.h>
#include <string.h>

#include "List.h"
#include "Singleton.h"
#include "CReport.h"


CReport::CReport(void)
{

}

CReport::~CReport(void)
{
  _variables.Free();
}

void CReport::AddVariable(const char *name, int value)
{
  CR_Variable *var = new CR_Variable;
  strncpy(var->name, name, 50);
  var->typ_val = ENTIER;
  var->u.i = value;
  _variables.Add(var);
}

void CReport::AddVariable(const char *name, float value)
{
  CR_Variable *var = new CR_Variable;
  strncpy(var->name, name, 50);
  var->typ_val = FLOTTANT;
  var->u.f = value;
  _variables.Add(var);
}

void CReport::AddTrace(const char *format, ...)
{
  char *s = NULL;
  FILE *pf = fopen(CREPORT_TRAC, "a");
  if(pf == NULL)
    return ;
  if (format)
    {
      char t[1];
      va_list pa;
      size_t size = 0;
      
      va_start (pa, format);
      size = vsnprintf (t, 1, format, pa);
      size++;
      s = new char[(sizeof (*s) * size)];
      if (s)
	{
	  vsnprintf (s, size, format, pa);
	}
    }
  fprintf(pf, "%s\n", s);
  fclose(pf);
  if(s)
    delete s;
}

