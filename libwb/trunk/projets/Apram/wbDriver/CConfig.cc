#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CConfig.h"

CConfig::CConfig(void)
{
  _width = 0;
  _height = 0;	
  _init = false;
  cam_address = wb_address = NULL;
  cam_sock = wb_sock = -1;
}

CConfig::~CConfig(void)
{
  delete cam_address;
  delete wb_address;
}

bool CConfig::Load(void)
{
  xmlTag *search;
  xmlTag *Cam;
  xmlTag *Wifibot;
  xmlTag *params;

  if(Initialize((char *)CCONFIG_FILENAME) == false)
    return false;

  search = Search((char *)"wbDriver");
  if(search == NULL)
    return false; 


  Cam = GetChild(search);
  if(Cam == NULL)
    return false;
  if(Cam->Attr.size() != 2)
    return false;
  cam_address = new char[strlen( Cam->Attr[0]->pcValue )];
  strcpy( cam_address, Cam->Attr[0]->pcValue );
  cam_sock = atoi( Cam->Attr[1]->pcValue );

  Wifibot = NextChild(Cam);
  if(Wifibot == NULL)
    return false;
  if(Wifibot->Attr.size() != 2)
    return false;
  wb_address = new char[strlen( Wifibot->Attr[0]->pcValue )];
  strcpy( wb_address, Wifibot->Attr[0]->pcValue );
  wb_sock = atoi( Wifibot->Attr[1]->pcValue );

  search = Search((char *)"config");
  if(search == NULL)
    return true; 
  
  params = GetChild(search);
  while(params != NULL)
    {
      read_once_param(params);
      params = NextChild(params);
    }
  return true;
}

void CConfig::read_once_param(xmlTag *tag)
{
  CConfig_param *p;
  if(tag->Attr.size() != 2)
    return ;
  
  p = new CConfig_param;

  p->name = new char[strlen( tag->Attr[0]->pcValue )];
  strcpy( p->name, tag->Attr[0]->pcValue );
  p->value = atof( tag->Attr[1]->pcValue );
  if(p->value >= 0)
    _list_params.Add(p);
}

