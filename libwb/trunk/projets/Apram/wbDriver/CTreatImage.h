#ifndef CTREATIMAGE_H
#define CTREATIMAGE_H

#include "CImage.h"
#include "CHsl.h"

///< définition d'un point
typedef struct point
{
  int x;
  int y;
}point;

///< définition d'une surface
typedef struct
{
  point left_up, left_down;
  point right_up, right_down;
  unsigned char color;
} CTI_Surface;

///< définition d'une séquence
typedef struct
{
  unsigned char color; ///< couleur de la séquence
  int start; ///< pixel de départ
  int stop; ///< pixel d'arrivé
} CTI_Seq;

///< tolérance en nombre de pixels lors de la recherche
#define CTI_ACCURENCY 5
///< taille minimal de la cible dans l'image
#define CTI_SIZE_TARGET_MIN 40

///< couleur du cercle le plus à l'extérieur
#define COLOR_OF_THE_THIRD_CIRCLE 224
///< couleur du cercle du milieu
#define COLOR_OF_THE_SECOND_CIRCLE 128
///< couleur du cercle intérieur
#define COLOR_OF_THE_FIRST_CIRCLE 32

class CTreatImage
{
 public:
  CTreatImage(void);
  ~CTreatImage(void);
  /**
   * affecte l'image à la classe
   * @param pic image à traiter
   */
  void setHslPic(CImage *pic);
  /**
   * fonction de recherche de la cible
   * @return true si la cible a été trouvée
   * @return false dans le cas contraire
   */
  bool FindTheTarget(void);

  ///< surface du cercle extérieure détecté
  CTI_Surface _red_circle;
  ///< surface du cecle intérieure détecté
  CTI_Surface _yellow_circle;
 private:
  /**
   * initialisation de la surface
   * @param surf surface à traiter
   */
  void init_surface(CTI_Surface *surf);

  ///< séquences d'une ligne
  CTI_Seq sequence[640];
  ///< variable temporaire utilisé lors de la recherche
  CTI_Seq tmp_target[3];
  ///< les trois séquences finales de la recherche
  CTI_Seq target[3];
  ///< image sur lequelle se base la recherche
  CImage *_img;
  ///< classe permettant la convertion en teintes
  CHsl *_hsl;
  ///< hauteur et largeur de l'image
  int _width, _height;
};

#endif

