#ifndef ccamdriver_image_h
#define ccamdriver_image_h

#include "CThread.h"
#include "CTcp.h"

#define CDI_MAX_IMAGE_WIDTH	640
#define CDI_MAX_IMAGE_HEIGHT	480
#define CDI_MAX_NB_BUFFER	3
#define CDI_MAX_BUFFER	(CDI_MAX_IMAGE_WIDTH * CDI_MAX_IMAGE_HEIGHT * CDI_MAX_NB_BUFFER)

class CCamDriverImage : public CThread, public CTcp
{
 public:
  CCamDriverImage();
  ~CCamDriverImage(void);
  /**
   * stop le client tcp
   */
  void clear(void);
  /**
   * buffer de réception messages tcp
   */
  char *_buffer;
  /**
   * taille des données reçues
   */
  unsigned int _size;
 protected:
  void setup(void);
  void execute(void);
 private:
  bool _bKnonwSize;
  bool _bRun;
};

#endif 
