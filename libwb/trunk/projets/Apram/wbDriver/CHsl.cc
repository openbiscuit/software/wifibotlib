#include <iostream>

#include "CHsl.h"


CHsl::CHsl(int w, int h)
{
  _width = w;
  _height = h;
  _img_base = NULL;
  teinte = new Tpx * [_width];
  for(int i=0;i<_width;i++)
    teinte[i] = new Tpx[_height];
}

CHsl::~CHsl(void)
{
  for(int i=0;i<_width;i++)
    delete teinte[i];
  delete teinte;
}

void CHsl::build(void)
{
  unsigned char *image = _img_base->get_buffer();
  for(int i=0;i<_width;i++)
    {
      for(int j=0;j<_height;j++)
	{
	//  printf("%f", teinte[i][j].luminance);
	  if( teinte[i][j].on == 1) 
	    {
	      continue;
	    }
	  else
	    {
	      for(int k=0;k<3;k++)
		image[(j*_width+i)*3+k] = teinte[i][j].on;
	    }
	}
    }
}

void CHsl::hsl_to_rvb(void)
{
  int i=0;
  int j=0;
  unsigned int p,q,tr,tv,tb,r,v,h,b;
   unsigned char *image = _img_base->get_buffer();
  q=255;
  p=0;

  for(i=0;i<_width;i++)
    {
      for(j=0;j<_height;j++)
	{
	  h = teinte[i][j].valeur ;	
	  tr = h + 85;
	  tv = h;
	  tb = h-85;
	  if(tr<0)
	    tr = tr+255;
	  if(tr>255)
	    tr = tr-255;
	
	  if(tb<0)
	    tb = tb+255;
	  if(tb>255)
	    tb = tb-255;
				
	  if(tr<=42)
	    {
					
	      r = p + (q-p) * (tr/42);
	    }
	  else
	    {
	      if(tr<128)
		{
		  r = q;
		}
	      else
		{
		  if(tr<170)
		    {
		      r= ((170*q - 128*p)/42)  - ((q-p)/42)*tr;
		    }
		  else
		    {
		      r=p;
		    }
		}
					
	    }
	  if(tv<=42)
	    {
					
	      v = p + (q-p) * (tr/42);
	    }
	  else
	    {
	      if(tv<128)
		{
		  v = q;
		}
	      else
		{
		  if(tv<170)
		    {
		      v= ((170*q - 128*p)/42)  - ((q-p)/42)*tv;
		    }
		  else
		    {
		      v=p;
		    }
		}
					
	    }
	  if(tb<=42)
	    {
					
	      b = p + (q-p) * (tr/42);
	    }
	  else
	    {
	      if(tb<128)
		{
		  b = q;
		}
	      else
		{
		  if(tb<170)
		    {
		      b= ((170*q - 128*p)/42)  - ((q-p)/42)*tb;
		    }
		  else
		    {
		      b=p;
		    }
		}
					
	    }
	  //	printf("h : %d r : %d v : %d b :%d\n",h,r,v,b);
//	  printf(" %d", (i*width+j)*3);
	  if(teinte[i][j].isblack)
	    {
	      image[(j*_width+i)*3] = (unsigned char)  0;
		image[(j*_width+i)*3+1] = (unsigned char)  0;
		image[(j*_width+i)*3+2] = (unsigned char)  0;
	    }
	  else
	    if(teinte[i][j].iswhite)
	      {
		image[(j*_width+i)*3] = (unsigned char)  255;
		image[(j*_width+i)*3+1] = (unsigned char)  255;
		image[(j*_width+i)*3+2] = (unsigned char)  255;
	      }
	    else
	      {
		image[(j*_width+i)*3] = (unsigned char)  (r);
		image[(j*_width+i)*3+1] = (unsigned char)  (v);
		image[(j*_width+i)*3+2] = (unsigned char)  (b);
	      }
	}
				
    }
  //	printf("width : %d, height %d\n",width,height);
}

void CHsl::rvb_to_hsl(void)
{

   unsigned char *image = _img_base->get_buffer();
  //pour le cacul de la teinte
  int T0, delta; // delta=max(r,v,b)-min(r,v,b)
  unsigned char cst=TMAX; // pour la formule du calcul de la teinte

  unsigned char r, v, b; // couleurs
  int i=0, j; //parcours du tableau hsl

  //on traite l'image pixel par pixel
  while(i<_width){
    j=0;
    //if(i%10 == 0) printf("ligne %d\n", i);
    while(j<_height){
      //recuperation des composantes rvb
      r=image[(j*_width+i)*3];
      v=image[(j*_width+i)*3+1];
      b=image[(j*_width+i)*3+2];
     // temp += 3*(INC-1);

      if( r == v && v == b ) 
	{
	// cas special
	T0 = 0;
      }
      else if(r >= v && r >= b) {
	delta = (v > b)? r-b : r-v;
	T0 = cst*(v-b)/ (6*delta);
      } else if(v >= b && v >= r) {
	delta = (b > r)? v-r : v-b;
	T0 = cst*((b-r)+2*delta)/ (6*delta);
      } else {
	delta = (r > v)? b-v : b-r;
	T0 = cst*((r-v)+4*delta)/ (6*delta);
      }
			   
      //T = ((T0)*cst); // cst = TMAX/6 avec TMAX = 255
      //T = (unsigned char)(T%TMAX);
			
      //remplir le tableau hsl avec seulement la composante h
      if(r> seuil_blanc && v > seuil_blanc && b > seuil_blanc)
	{
	  teinte[i][j].iswhite =1;
	}
      else
	{
	  teinte[i][j].iswhite = 0;
	}
      if(r< 60 && v < 60 && b < 60)
	{
	  teinte[i][j].isblack =1;
	}
      else
	{
	  teinte[i][j].isblack = 0;
	}
      teinte[i][j].valeur=T0;
      teinte[i][j].traite=0;
      teinte[i][j].on = 0;
      // on avance dans la ligne
      j++;  
    } // while(width)
    // on change de ligne
    i ++ ;
  }// while(height)
}// rvb2hsl()

/**
 * La teinte sera codée par intervales de longueur 'div'
 * (0, div, 2*div, 3*div, ...).
 */
void CHsl::discretiser_teinte(int div)
{
  int i,j;
  for(i=0;i<_width;i++)
    {
      for(j=0;j<_height;j++)
	{
	  teinte[i][j].valeur = teinte[i][j].valeur / div;
			
	  teinte[i][j].valeur*=div;	
	}
    }
}

		
/**
 * Crée des pixels de taille 'sizeofpixel' dont la teinte est la moyenne
 * des teintes des pixels remplacés.
 */		
void CHsl::pixeliser_teinte(int sizeofpixel)
{
  int i,j,x,z;
  int moy=0;
  for(i=0;i<_width;i+=sizeofpixel)
    {
      for(j=0;j<_height;j+=sizeofpixel)
	{
	  moy=0;
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  moy+=teinte[x][z].valeur;
					
		}
	    }
	  moy/=(sizeofpixel * sizeofpixel);
			
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  teinte[x][z].valeur = moy;
		}
	    }
	}	
    }
}

void CHsl::convert(CImage *img)
{
  _img_base = img;
  
  rvb_to_hsl();

  discretiser_teinte(32);
  pixeliser_teinte(1);

  hsl_to_rvb();
}



