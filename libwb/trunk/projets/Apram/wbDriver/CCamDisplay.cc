#include <iostream>
#include <gtkmm.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>

#include "Singleton.h"
#include "List.h"

#include "CListImage.h"

#include "CThread.h"
#include "CImage.h"
#include "CCam.h"
#include "CCamDisplay.h"
#include "CTreatImage.h"
#include "XmlParser.h"
#include "CConfig.h"



CCamDisplay::CCamDisplay(void) : type_aff(0x01)
{
  set_title("Wifibot cam");
  set_default_size(CConfig::getInstance()->_width,
  	 CConfig::getInstance()->_height);
  add(m_VBox);
  

  menuBar.items().push_back( Gtk::Menu_Helpers::MenuElem("Affichage", mAff) );
  mAff.items().push_back( Gtk::Menu_Helpers::MenuElem("hsl" , 
	   sigc::mem_fun(*this, &CCamDisplay::on_button_hsl) ));
  mAff.items().push_back( Gtk::Menu_Helpers::MenuElem("rvb" , 
	   sigc::mem_fun(*this, &CCamDisplay::on_button_bmp) ));
  mAff.items().push_back( Gtk::Menu_Helpers::MenuElem("Rect sur cercle rouge" , 
	   sigc::mem_fun(*this, &CCamDisplay::on_button_red) ));
  mAff.items().push_back( Gtk::Menu_Helpers::MenuElem("Rect sur cercle jaune" , 
	   sigc::mem_fun(*this, &CCamDisplay::on_button_yellow) ));
  m_VBox.pack_start(menuBar);

  camA.setSize(CConfig::getInstance()->_width,
  	 CConfig::getInstance()->_height);

  Gtk::Main::signal_key_snooper().connect(sigc::mem_fun(*this,&CCamDisplay::on_keyb));
  m_VBox.pack_start(camA);
  _bFoundTarget = false;
  _largeur = 0;
  _centre = 0;
  show_all_children();
}

CCamDisplay::~CCamDisplay(void)
{

}

void CCamDisplay::setup(void)
{
  
}

void CCamDisplay::execute(void)
{
  CListImage *imgs = CListImage::getInstance();
  CImage *img = new CImage();

  CTreatImage treatment;
  while(1)
    {
       if(imgs->GetLength() > 2)
	{
	  CListImage_picture *pic =(CListImage_picture *) imgs->GetHead();

	  img->set_length(pic->size);
	  img->set_buffer(pic->data, CIT_JPG);
	  if(img->convert_to(CIT_PPM) == true)
	    {
	      //	      img->save("1.ppm");
	      if(type_aff & 0x01)
		camA.setPic(img);

	      treatment.setHslPic(img);
	      
	      if( treatment.FindTheTarget() == true )
		{
		  _bFoundTarget = true;
		  _largeur =  treatment._red_circle.right_down.x-treatment._red_circle.left_up.x;
		  _centre = _largeur / 2 + treatment._red_circle.left_up.x;

		}
	      
	      if(!(type_aff & 0x01))
		camA.setPic(img);
	      
	      if(type_aff & 0x02)
		camA.setRectangle(treatment._yellow_circle.left_up.x,
				  treatment._yellow_circle.left_up.y,
				  treatment._yellow_circle.right_down.x-treatment._yellow_circle.left_up.x,
				  treatment._yellow_circle.right_down.y-treatment._yellow_circle.left_up.y);
	      
	      if(!(type_aff & 0x02))
		camA.setRectangle(treatment._red_circle.left_up.x,
				  treatment._red_circle.left_up.y,
				  treatment._red_circle.right_down.x-treatment._red_circle.left_up.x,
				  treatment._red_circle.right_down.y-treatment._red_circle.left_up.y);
	    }

	  delete pic->data;
	  imgs->RemoveHead();
	  }
     usleep(1);
//     sleep(1);
    }
}

void CCamDisplay::on_button_hsl()
{
  if(type_aff & 0x01)
    type_aff ^= 0x01;
}


void CCamDisplay::on_button_bmp()
{
  if(!(type_aff & 0x01))
    type_aff |= 0x01;

}

void CCamDisplay::on_button_red()
{
  if(type_aff & 0x02)
    type_aff ^= 0x02;
}


void CCamDisplay::on_button_yellow()
{
  if(!(type_aff & 0x02))
    type_aff  |= 0x02;
}



int CCamDisplay::on_keyb(Gtk::Widget * widget, GdkEventKey *event)
{
  if(event->type != GDK_KEY_RELEASE)
    return TRUE;

  if(event->keyval == GDK_q)
   {
     Gtk::Main::quit();
   }

 return TRUE;
}

