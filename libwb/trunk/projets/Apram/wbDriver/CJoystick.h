#ifndef CJOY_H
#define CJOY_H

#include "CThread.h"

#define JOY_DEV "/dev/input/js0"

class CJoystick : public CThread
{
 public:
  CJoystick(void);
  ~CJoystick(void);
 protected:
  void setup(void);
  void execute(void);
 private:
  ///< handle du joystick
  int joy_fd;
  ///< pointeur vers la fenêtre CWBBar
  CWBBar *bar;
};


#endif
