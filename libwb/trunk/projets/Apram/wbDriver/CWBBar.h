#ifndef CWBBAR_H
#define CWBBAR_H

#include <gtkmm.h>

#include "CThread.h"
#include "CCamDisplay.h"
#include "CCamBar.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"
#include "CPeng.h"

/**
 * exploitation/exploration/mouvement
 */
typedef enum
  {
    CWWB_state_stop,
    CWWB_state_learn,
    CWWB_state_play
  } CWWB_state;

#define CWBBAR_DIST_MAX 200
#define CWBBAR_DIST_MIN 40
#define CWBBAR_DIST_SIZE (CWBBAR_DIST_MAX - CWBBAR_DIST_MIN)

#define CWBBAR_ANGLE_MAX 600
#define CWBBAR_ANGLE_MIN 0
#define CWBBAR_ANGLE_SIZE (CWBBAR_ANGLE_MAX - CWBBAR_ANGLE_MIN)

class CWBBar : public Gtk::Window, public CThread
{
 public:
  CWBBar(void);
  ~CWBBar(void);

  /**
   * quitte le programme
   */
  virtual void on_button_quit();
  /**
   * affiche le gui de la caméra (affichage)
   */
  virtual void on_button_camera();
  /**
   * affiche le gui déplacement de la caméra
   */
  virtual void on_button_cameratb();
  /**
   * faire avancer le wifibot
   */
  virtual void on_button_fd();
  /**
   * faire reculer le wifibot
   */
  virtual void on_button_bd();
  /**
   * wifibot vers la gauche
   */
  virtual void on_button_left();
  /**
   * stopper le wifibot
   */
  virtual void on_button_wb_stop();
  /**
   * wifibot vers la droite
   */
  virtual void on_button_right();
  /**
   * sauvegarder le kdtree
   */
  virtual void on_button_save();
  /**
   * charger le kdtree
   */
  virtual void on_button_load();
  /**
   * mode apprentissage
   */
  virtual void on_button_learn();
  /**
   * mode mouvement
   */
  virtual void on_button_stop();
  /**
   * mode exploitation
   */
  virtual void on_button_play();
  /**
   * interrogation de smart
   */
  void on_button_query();
  /**
   * exporter les données raw
   */
  virtual void on_button_export_raw();
  /**
   * exporter les donnée interpolées
   */
  virtual void on_button_export_interpol();
 private:
  /**
   * réaliser un apprentissage
   */
  void algo_step(void);

  void setup(void);
  void execute(void);

  Gtk::Button _bUpd, _bDown, _bRight, _bLeft;
  Gtk::Button _bPause, _bPlay, _bQuery, _bRecord, _bQuit, _bStop;
  Gtk::Toolbar _Toolbar;
  Gtk::Button m_Button_Quit;
  Gtk::VBox m_VBox;
  Gtk::HButtonBox m_ButtonBox;
  Gtk::HButtonBox m_ButtonBox2;

  Gtk::MenuBar menuBar;
  Gtk::Menu mWindow;
  Gtk::Menu   fileMenu;
  Gtk::Menu   fileMenu2;

  ///< affichage de la caméra
  CCamDisplay *dis;
  ///< gui de déplacement de la caméra
  CCamBar *cam_bar;

  ///< action a réalisée
  int _act;
  ///< état en cours (exploration/exploitation)
  CWWB_state _state;

  ///< distance de la cible
  double distance; 
  ///< angle de la cible
  double angle;

  ///< kdtree utilisé
  CKdtree *kdtree;
  ///< algo hedger
  CHedger *smart;
  ///< traces d'élligibilitées
  CPeng *peng;
};

#endif
