#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/joystick.h>

#include "CWBBar.h"

#include "CJoystick.h"

CJoystick::CJoystick(void)
{

}

CJoystick::~CJoystick(void)
{
  close( joy_fd );
}

void CJoystick::setup(void)
{
  bar = (CWBBar *)_Arg;
}

void CJoystick::execute(void)
{
  int *axis=NULL, num_of_axis=0, num_of_buttons=0;
  unsigned char *button=NULL, name_of_joystick[80];
  struct js_event js;
  
  if( ( joy_fd = open( JOY_DEV , O_RDONLY)) == -1 )
    {
      printf( "Couldn't open joystick\n" );
      return ;
    }

  ioctl( joy_fd, JSIOCGAXES, &num_of_axis );
  ioctl( joy_fd, JSIOCGBUTTONS, &num_of_buttons );
  ioctl( joy_fd, JSIOCGNAME(80), &name_of_joystick );
  
  axis = new int[num_of_axis];
  button = new unsigned char[num_of_buttons];

  printf("Joystick detected: %s\n\t%d axis\n\t%d buttons\n\n"
	 , name_of_joystick
	 , num_of_axis
	 , num_of_buttons );

  fcntl( joy_fd, F_SETFL, O_NONBLOCK );	/* use non-blocking mode */
  while(1)
    {
      /* read the joystick state */
      read(joy_fd, &js, sizeof(struct js_event));
      
      /* see what to do with the event */
      switch (js.type & ~JS_EVENT_INIT)
	{
	case JS_EVENT_AXIS:
	  axis   [ js.number ] = js.value;
	  break;
	case JS_EVENT_BUTTON:
	  button [ js.number ] = js.value;
	  break;
	}

      if(num_of_buttons>=7)
	{
	  if(button[3] == 1)
	    {
	      bar->on_button_fd();
	      sleep(1);
	    }
	  if(button[1] == 1)
	    {
	      bar->on_button_bd();
	      sleep(1);
	    }
	  if(button[0] == 1)
	    {
	      bar->on_button_left();
	      sleep(1);
	    }
	  if(button[2] == 1)
	    {
	      bar->on_button_right();
	      sleep(1);
	    }
	  if(button[6] == 1)
	    {
	      bar->on_button_stop(); sleep(1);
	    }
	  if(button[7] == 1)
	    {
	      bar->on_button_learn(); sleep(1);
	    }
	  if(button[4] == 1)
	    {
	      bar->on_button_save(); sleep(1);
	    }
	  if( button[5] == 1)
	    {
	      bar->on_button_wb_stop();
	      sleep(1);
	    }
	}
 
      usleep(100);
    }
}
