#ifndef CCAMDRIVER_H
#define CCAMDRIVER_H

#include "CThread.h"
#include "Singleton.h"

#include "CCamDriverImage.h"
#include "CCamDriverMovement.h"

class CCamDriver : public CThread, public Singleton<CCamDriver>
{
 public:
  CCamDriver(void);
  ~CCamDriver(void);
  CCamDriverMovement _camMovement; ///< driver mouvement
  CCamDriverImage _camImage; ///< driver récupération des images
 protected:
  void setup(void);
  void execute(void);
 private:

};

#endif
