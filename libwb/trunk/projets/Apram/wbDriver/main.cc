#include <iostream>
#include <gtkmm.h>

#include "List.h"
#include "Singleton.h"
#include "CThread.h"
#include "CTcp.h"
#include "CImage.h"
#include "CCam.h"
#include "CCamDriverImage.h"
#include "CCamDriverMovement.h"
#include "CCamDriver.h"
#include "CCamDisplay.h"

#include "CHsl.h"
#include "CTreatImage.h"
#include "XmlParser.h"
#include "CConfig.h"
#include "CCamBar.h"
#include "CWifibotDriver.h"
#include "CReport.h"
#include "CWBBar.h"


#include "CJoystick.h"

/**
 * sequence de boot :
 *  verifier la connection vers le wifibot
 *  verifier la connection vers la webcam
 *  recuperer la resolution des images
 *  lancer l'interface graphique !
 */
int main(int argc, char **argv)
{ 
  CWifibotDriver wbD;
  CCamDriver camD;

  if(CConfig::getInstance()->Load() == false)
    {
      printf("Erreur lecture du fichier de config !\n");
      fflush(stdout);
      return 0;
    }
  
  camD._camImage.start(NULL);
  wbD.start(NULL);
	
  while(!CConfig::getInstance()->_init) usleep(100);
	
  printf("** wbDriver **\n");
  printf("** Taille de l'image : %dx%d **\n", 
	 CConfig::getInstance()->_width,
	 CConfig::getInstance()->_height);
  fflush(stdout);
  	
  Gtk::Main kit(argc, argv);
  CJoystick pad;
  
  CWBBar bar;
  pad.start(&bar);

  Gtk::Main::run(bar);

  return 0;
}
