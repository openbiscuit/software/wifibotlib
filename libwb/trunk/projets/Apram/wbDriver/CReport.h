
#define CREPORT_VARS "variables.txt"
#define CREPORT_TRAC "traces.txt"

typedef enum 
{
  ENTIER,
  FLOTTANT
} CR_Type;

typedef struct
{
  char name[50];
  CR_Type typ_val;
  union
  {
    int i;
    float f;
  } u;
} CR_Variable;


class CReport : public Singleton<CReport>
{
 public:
	CReport(void);
	~CReport(void);

	/**
	 * Ajoute une variable au fichier de trace
	 * @param name nom de la variable
	 * @param value valeur de la variable
	 */
	void AddVariable(const char *name, int value);
	
	/**
	 * Ajoute une variable au fichier de trace
	 * @param name nom de la variable
	 * @param value valeur de la variable
	 */
	void AddVariable(const char *name, float value);
 	 ///< ajoute une trace
 	 void AddTrace(const char *format, ...);
 private:
	CList _variables; ///< liste des variables
};

