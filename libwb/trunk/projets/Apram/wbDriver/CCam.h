#ifndef ccam_h
#define ccam_h

#include <gtkmm.h>

#include "CImage.h"

class CCam : public Gtk::DrawingArea
{
public:
  CCam();
  ~CCam();
  /**
   * affecte la taille de la fenêtre
   * @param w largeur de la fenêtre
   * @param h hauteur de la fenêtre
   */
  void setSize(int w, int h);
  /**
   * affectation de l'image
   * @param tmpImg image à afficher
   */
  void setPic(CImage *tmpImg);
  /**
   * affectation de l'emplacement du rectangle à afficher
   * @param l left
   * @param b bottom
   * @param w width
   * @param h height
   */
  void setRectangle(int l, int b, int w, int h);
  ///< image à afficher
  char *b;
  
protected:
  int _width;
  int _height;
  virtual bool on_expose_event(GdkEventExpose* event);
  bool onSecondElapsed(void);
  int _l;
  int _b;
  int _w;
  int _h;
};

#endif

