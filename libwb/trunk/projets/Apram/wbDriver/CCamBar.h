#ifndef CCAMBAR_H
#define CCAMBAR_H

#include <gtkmm.h>

class CCamBar : public Gtk::Window
{
 public:
  CCamBar(void);
  ~CCamBar(void);
 private:
  ///< faire monter la caméra
  virtual void on_button_up();
  ///< faire descendre la caméra
  virtual void on_button_down();
  ///< caméra vers la gauche
  virtual void on_button_left();
  ///< caméra vers la droite
  virtual void on_button_right();
  ///< caméra en position initiale
  virtual void on_button_home();
  ///< offset de déplacement (de combien la caméra doit se déplacer)
  void getOffset(void);
  
  int _offset; ///< pas de déplacement
  ///< handle des boutons
  Gtk::Button _bUpd, _bDown, _bRight, _bLeft, _bHome;
  ///< hanlde du layout
  Gtk::HBox m_VBox;
  ///< conteneur des boutons
  Gtk::HButtonBox m_ButtonBox;
  ///< entrée de saisie pour _offset
  Gtk::Entry m_Entry;
};

#endif

