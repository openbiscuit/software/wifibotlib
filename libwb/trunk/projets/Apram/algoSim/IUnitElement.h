#ifndef IUNITELEMENT_H
#define IUNITELEMENT_H

class IUnitElement
{
public:
	IUnitElement(void);
	
	/**
	 * Définit le contexte de dessin
	 */
	void setCairo(cairo_t *cr);
	
	/**
	 * Définit la position de l'élément
	 * @param x coordonnée x
	 * @param y coordonnée y
	 */
	void setPos(int x, int y);
	
	/**
	 * Dessine la position
	 */
	void doDraw(void);
	
	void beginDraw(void);
	void endDraw(void);
	void setColor(float r, float v, float b);
	void Text(int x, int y, char *data, int size);
	void Rectangle(int x1, int y1, int x2, int y2);
	void Circle(int radius, char fill=1);

	void Circle(int x1, int x2, int radius, char fill=1);

	void Cone(int x1, int x2, int angle, int radius);
	void Triangle(int size, char fill=1);
	void Line(int x, int y, int width);
	void Arc(int x1, int y1, int x, int y, int angle, int radius);

	int posx, posy;
	double _t_x;
	double _t_y;
	float _r;
private:	
	cairo_t *_cr;
	cairo_matrix_t _matrix;
};



#endif

