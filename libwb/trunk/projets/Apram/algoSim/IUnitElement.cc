#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include "IUnitElement.h"


IUnitElement::IUnitElement(void)
{
	posx = posy = 0;
	_cr = NULL;
	_t_x = _t_y = 0;
	_r = 0;
}

void IUnitElement::setCairo(cairo_t *cr)
{
	_cr = cr;
}

void IUnitElement::doDraw(void)
{
	Cone(posx, posy, 5, 10);
}

void IUnitElement::setColor(float r, float v, float b)
{
	cairo_set_source_rgb(_cr, r, v, b);
}

void IUnitElement::setPos(int x, int y)
{
	posx = x;
	posy = y;
}

void IUnitElement::Rectangle(int x1, int y1, int x2, int y2)
{
  cairo_rectangle(_cr, x1, y1, x2, y2);
  cairo_fill(_cr);
}

void IUnitElement::Circle(int radius, char fill)
{
  cairo_arc(_cr, posx, posy, radius, 0, 2 * M_PI);
  if(fill == 1)
    cairo_fill(_cr);
  else
    cairo_stroke(_cr);
}

void IUnitElement::Cone(int x1, int x2, int angle, int radius)
{
	cairo_move_to(_cr, 0, 0);
	cairo_arc(_cr, x1, x2, radius, -angle/2 , angle/2);
	cairo_line_to(_cr, 0, 0);
	cairo_stroke(_cr);
}


void IUnitElement::Triangle(int size, char fill)
{
	cairo_move_to(_cr, posx, posy);
	cairo_rel_line_to(_cr, -3*size/2, -size/2);
	cairo_rel_line_to(_cr, 0, size);
	cairo_rel_line_to(_cr, 3*size/2, -size/2);
	if(fill == 1)
	   cairo_fill(_cr);
	else
	  cairo_stroke(_cr);
}

void IUnitElement::Circle(int x1, int x2, int radius, char fill)
{
  cairo_matrix_t _m;
  cairo_get_matrix(_cr, &_m);
//  cairo_translate(_cr, x1, y1);
  cairo_arc(_cr, x1, x2, radius, 0, 2 * M_PI);
  if(fill == 1)
    cairo_fill(_cr);
  else
    cairo_stroke(_cr);
  cairo_set_matrix(_cr, &_m);
}

void IUnitElement::Arc(int x1, int y1, int x, int y, int angle, int radius)
{
  cairo_matrix_t _m;
  //cairo_move_to(_cr, x1, y1);
  //cairo_stroke(_cr);
  cairo_get_matrix(_cr, &_m);

  cairo_translate(_cr, x1, y1);

  cairo_arc_negative(_cr, x, y, radius, -angle/2 , angle/2);
  cairo_stroke(_cr);
  cairo_set_matrix(_cr, &_m);
}

void IUnitElement::Line(int x, int y, int width)
{
  cairo_move_to(_cr, posx, posy);
  cairo_set_line_width (_cr, width);
  cairo_rel_line_to(_cr, x, y);
  cairo_stroke(_cr);
}

// example : Text(50, 100, "test", 20);
void  IUnitElement::Text(int x, int y, char *data, int size)
{
    cairo_set_font_size (_cr, size);
    cairo_move_to (_cr, x, y);
    cairo_show_text (_cr, data);
}

void IUnitElement::beginDraw(void)
{
  cairo_get_matrix(_cr, &_matrix);
  cairo_translate(_cr, _t_x, _t_y);
  cairo_rotate(_cr, _r );
}

void IUnitElement::endDraw(void)
{
  cairo_set_matrix(_cr, &_matrix);
}
	
