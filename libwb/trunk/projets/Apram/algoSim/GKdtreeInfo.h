/** \file
 * \brief interface d'informations sur le kdtree
 */

///< largeur du widget
#define GDKTREEINFO_WITH 220

///< hauteur du widget
#define GDKTREEINFO_HEIGHT 300

enum
{
   TITLE_COLUMN,
   VALUE_INT,
   N_COLUMNS
};

typedef struct
{
  char name[20];
  char value[20];
  GtkTreeIter iter;
} item_list;

class GKdtreeInfo
{
public:
  /**
     * constructeur du wiget informations sur le kd-tree
     */
  GKdtreeInfo(void);
  
  /**
    * instancie le widget et l'affiche
    */
  void Show(void);

	///< ajoute un point (compteur de point)
 	void AddPoint(void);
 	
 	/**
 	 * 	affecte le nombre de points
 	 * @param nb nombre de points
 	 */
	void SetPoint(int nb);
	
	/**
	 * Définit la meilleure action
	 * @param act action
	 */
	void SetBestAction(double act);
	
	/**
	 * Ajoute une colonne
	 * @param name nom de la colonne
	 * @param value valeur de la colonne
	 */
	void AddColumn(char *name, int value);
	
	/**
	 * Affecte une valeur à la colone
	 * @param name nom de la colonne
	 * @param value nouvelle valeur
	 */
	void SetColumn(char *name, int value);
private:
	///< handle du widget	
	GtkWidget *_window;
  GtkWidget *_labelNbPoints;
  GtkWidget *_labelBestAction;
  GtkWidget *pVBox;
  GtkListStore *_list_pred_status;

   GtkTreeStore *store;
   GtkWidget *tree;
   GtkTreeViewColumn *column;
   GtkCellRenderer *renderer;
  int _nb_points;
  item_list items[20];
  int index_item;
};
