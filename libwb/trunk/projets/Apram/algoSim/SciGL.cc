#include <stdlib.h>
#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <iostream>
#include <gdk/gdkkeysyms.h>


using namespace std;

#include "SciGL.h"


// initialisation des objets du graphe
Frame *SciGL::frame = NULL;
Scene *SciGL::scene = new Scene();
float *SciGL::value_data = NULL;
Data *SciGL::data_graph = new Data();
SmoothSurface *SciGL::surf = new SmoothSurface ();
SciGL *SciGL::graph = new SciGL();

// initialisation des variables pour gtk
//GtkObject *SciGL::Adjust = NULL;
//GtkWidget *SciGL::pScrollbar = NULL;
//int SciGL::_iScrollbarValue = 1;
guint SciGL::idle_id = 0;

// initialisation des variables pour les algos
CKdtree *SciGL::tree = NULL;
CHedger *SciGL::smart = NULL;
CKdtreeExport *SciGL::out = new CKdtreeExport();
CPeng *SciGL::peng = NULL;


void SciGL::init(GtkWidget *widget, gpointer   data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
    return;
  Colormap::IceAndFire()->scale(-1,1);

  surf->set_data (data_graph);
  surf->set_cmap (Colormap::IceAndFire());
  surf->set_size (1, 1, 1);
  surf->set_position (0, 0, 0);
  scene->add (surf);
  frame = new Frame();
  scene->add (frame);
  set_frame_size(1, 1, 1);
  scene->set_zoom (1.25);

  set_axis_label("distance", "angle", "q-value");

  scene->update();
  scene->build();

  gdk_gl_drawable_gl_end (gldrawable);
}

void SciGL::compute_algo(CHedger *smart, bool raw)
{

  out->Reset();

  smart->_verbosity = 0;

  float dist_min = 0.0;
  float dist_max  = 1.0;
  float angle_min = 0.0;
  float angle_max = 1.0;
  float dist_pre = 0.01;
  float angle_pre = 0.01;

  out->setLimits(dist_min, angle_min, dist_max, angle_max);
  out->setPrecision(dist_pre, angle_pre);
  out->setKdtree(smart);
  out->Allocate();


  smart->S->_limit_of_bag = 10;
  smart->_bandwith = 0.4;
  smart->_nb_points = 15;
  smart->_nb_points_max = 15;
//  smart->_alpha = alpha;
  //smart->_gamma = gamma;

  if(raw == true)
    out->Raw(HEDGER_ACTION_FORWARD);
  else
    out->Interpol(HEDGER_ACTION_FORWARD);
 
  data_graph->reset();
  surf->reset();

  float dist_size = (dist_max-dist_min)/dist_pre;
  float angle_size = (angle_max-angle_min)/angle_pre;


  if(value_data) delete value_data;
  value_data = new float[(int)(dist_size * angle_size)];
  for(int i=0;i<dist_size;i++)
    {
      for(int j=0;j<angle_size;j++)
	{
	  value_data[j*(int)dist_size+i] = 0;
	}
    }
  g_resolution_x = dist_size;
  g_resolution_y = angle_size;

  data_graph->set_data(value_data,
		       Shape(g_resolution_x, g_resolution_y, 1));

  frame->set_frame_size(dist_size/100, 
			angle_size/100,
			1);
  surf->set_size (dist_size/100, 
			angle_size/100,
			1);

  set_x_range(dist_min, dist_max, dist_size/100);
  set_y_range(angle_min, angle_max, angle_size/100);
  set_z_range(0, (int)out->_max_z);
  upd(out->z);

  // set_size(200, 200);
  surf->set_data (data_graph);
  surf->set_cmap (Colormap::IceAndFire());
 
  surf->set_position (0,0,0);
  surf->update();

  //tree->SaveTo("kdtree.txt");

 
}

gboolean SciGL::idle (GtkWidget *widget)
{

  gdk_window_invalidate_rect (widget->window, &widget->allocation, FALSE);

  return TRUE;
}



void SciGL::idle_add (GtkWidget *widget)
{
  if (idle_id == 0)
    {
      idle_id = g_idle_add_full (GDK_PRIORITY_REDRAW,
                                 (GSourceFunc) idle,
                                 widget,
                                 NULL);
    }
}

void SciGL::idle_remove (GtkWidget *widget)
{
  if (idle_id != 0)
    {
      g_source_remove (idle_id);
      idle_id = 0;
    }
}

gboolean SciGL::map (GtkWidget   *widget,
     GdkEventAny *event,
     gpointer     data)
{
  idle_add (widget);

  return TRUE;
}

gboolean SciGL::unmap (GtkWidget   *widget,
       GdkEventAny *event,
       gpointer     data)
{
  idle_remove (widget);

  return TRUE;
}

gboolean SciGL::visible (GtkWidget          *widget,
	 GdkEventVisibility *event,
	 gpointer            data)
{
  if (event->state == GDK_VISIBILITY_FULLY_OBSCURED)
    idle_remove (widget);
  else
    idle_add (widget);

  return TRUE;
}

/* change view angle, exit upon ESC */
gboolean SciGL::key (GtkWidget   *widget,
     GdkEventKey *event,
     gpointer     data)
{

  switch (event->keyval)
    {
    case GDK_z:
//      view_rotz += 5.0;
      break;
    case GDK_Z:
  //    view_rotz -= 5.0;
      break;
    case GDK_Up:
    //  view_rotx += 5.0;
      break;
    case GDK_Down:
      //view_rotx -= 5.0;
      //mouse_y += 5;
  //    mouse_x += 5;
//      scene.mouse_motion (1, mouse_x, mouse_y);
      break;
    case GDK_Left:
      //view_roty += 5.0;
      break;
    case GDK_Right:
     // view_roty -= 5.0;
      break;
    case GDK_Escape:
      gtk_main_quit ();
      break;
    case GDK_s:
      scene->save("graph.ppm");
      break;
    default:
      return FALSE;
    }

  gdk_window_invalidate_rect (widget->window, &widget->allocation, FALSE);

  return TRUE;
}


gboolean SciGL::button_press_event (GtkWidget      *widget,
		    GdkEventButton *event,
		    gpointer        data)
{
  int x = event->x;
  int y = event->y;

// if (event->state & GDK_BUTTON1_MASK)
   {
     scene->button_press (1, x, y);
   }
 // begin_x = event->x;
  //begin_y = event->y;

  return FALSE;
}

gint SciGL::motion_notify_event( GtkWidget *widget, GdkEventMotion *event, gpointer obj)
{
  int x = event->x;
  int y = event->y;


  if (event->state & GDK_BUTTON1_MASK)
    {
      scene->mouse_motion (1, x, y);
    }

  if (event->state & GDK_BUTTON2_MASK)
    {
      scene->mouse_motion (2, x, y);
    }

  return TRUE;
}

void SciGL::OnScrollbarChange(GtkWidget *pWidget, gpointer data)
{
//   _iScrollbarValue = gtk_range_get_value(GTK_RANGE(pWidget));
//   CGraph3D::getInstance()->ScaleZValues();
 //  out->ScaleZ( graph->_iScrollbarValue );
//   this->upd(out->z);
  // this->set_z_range(0, graph->_iScrollbarValue );
}

SciGL::SciGL(void)
{

}

SciGL::~SciGL(void)
{
  delete value_data;
  
  delete scene;
  delete frame;
  delete data_graph;
  delete surf;

//  gtk_widget_destroy(window);
}

void SciGL::upd(int i, int j, float v)
{
  // i est l'axe distance
  // j est l'axe angle
  value_data[j*g_resolution_x+i] = v;
  surf->update();
}

void SciGL::upd(float **tab)
{
 
  for(int i=0;i<g_resolution_x;i++)
    {
      for(int j=0;j<g_resolution_y;j++)
	{
	  value_data[j*g_resolution_x+i] = tab[i][j];
	}
    }
  surf->update();

}


void SciGL::set_axis_label(const char *x, const char *y, const char *z)
{
  frame->set_x_label(x);
  frame->set_y_label(y);
  frame->set_z_label(z);
}

void SciGL::set_frame_size(float x, float y, float z)
{

}

void SciGL::set_x_range(float min, float max, float size)
{
  frame->set_x_range(Range (min, max, 4, 20));
  frame->set_x_size(size);
  frame->set_x_position(-1*((float)size/2));
}

void SciGL::set_y_range(float min, float max, float size)
{
  frame->set_y_range(Range (min, max, 4, 20));
  frame->set_y_size(size*-1);
  frame->set_y_position((float)size/2);
}

void SciGL::set_z_range(float min, float max, float size)
{
  frame->set_z_range(Range (max, min, 4, 20));
  frame->set_z_size(-1*size);
  frame->set_z_position((float)size);
  Colormap::IceAndFire()->scale((float)size*-1, (float)size);
}



int SciGL::create (void)
{
  GdkGLConfig *glconfig;
  
  GtkWidget *vbox;
  GtkWidget *drawing_area;
  GtkWidget *button;

  gtk_gl_init (NULL, NULL);

  glconfig = gdk_gl_config_new_by_mode (GdkGLConfigMode(GDK_GL_MODE_RGB    |
					GDK_GL_MODE_DEPTH  |
							GDK_GL_MODE_DOUBLE));
  if (glconfig == NULL)
    {
      g_print ("*** Cannot find the double-buffered visual.\n");
      g_print ("*** Trying single-buffered visual.\n");

      /* Try single-buffered visual */
      glconfig = gdk_gl_config_new_by_mode (GdkGLConfigMode(GDK_GL_MODE_RGB   |
							    GDK_GL_MODE_DEPTH));
      if (glconfig == NULL)
	{
	  g_print ("*** No appropriate OpenGL-capable visual found.\n");
	  exit (1);
	}
    }

  /*
   * Top-level window.
   */

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
//   gtk_window_move((GtkWindow *)window, 650, 200);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_title (GTK_WINDOW (window), "** SciGL **");

  /* Get automatically redrawn if any of their children changed allocation. */
  gtk_container_set_reallocate_redraws (GTK_CONTAINER (window), TRUE);

  g_signal_connect (G_OBJECT (window), "delete_event",
		    G_CALLBACK (gtk_main_quit), NULL);

  /*
   * VBox.
   */

  vbox = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_widget_show (vbox);

  /*
   * Drawing area for drawing OpenGL scene.
   */

  drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 600, 600);

  /* Set OpenGL-capability to the widget. */
  gtk_widget_set_gl_capability (drawing_area,
				glconfig,
				NULL,
				TRUE,
				GDK_GL_RGBA_TYPE);

  gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK
    | GDK_LEAVE_NOTIFY_MASK
    | GDK_BUTTON_PRESS_MASK
    | GDK_POINTER_MOTION_MASK
    | GDK_POINTER_MOTION_HINT_MASK);

  gtk_widget_add_events (drawing_area,
			 GDK_VISIBILITY_NOTIFY_MASK);

  g_signal_connect_after (G_OBJECT (drawing_area), "realize",
                          G_CALLBACK (SciGL::init), NULL);

  g_signal_connect (G_OBJECT (drawing_area), "configure_event",
		    G_CALLBACK (SciGL::reshape), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "expose_event",
		    G_CALLBACK (SciGL::draw), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "map_event",
		    G_CALLBACK (SciGL::map), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "unmap_event",
		    G_CALLBACK (SciGL::unmap), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "visibility_notify_event",
		    G_CALLBACK (SciGL::visible), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "motion_notify_event",
		    G_CALLBACK (SciGL::motion_notify_event), this);
  g_signal_connect (G_OBJECT (drawing_area), "button_press_event",
		    G_CALLBACK (SciGL::button_press_event), this);

  g_signal_connect_swapped (G_OBJECT (window), "key_press_event",
			    G_CALLBACK (SciGL::key), drawing_area);


  gtk_box_pack_start (GTK_BOX (vbox), drawing_area, TRUE, TRUE, 0);

  gtk_widget_show (drawing_area);


  button = gtk_button_new_with_label ("Quit");

  g_signal_connect (G_OBJECT (button), "clicked",
		    G_CALLBACK (gtk_main_quit), NULL);

//  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  //gtk_widget_show (button);

//  Adjust = gtk_adjustment_new(1, 1, 10, 1, 10, 1);
//  pScrollbar = gtk_vscrollbar_new(GTK_ADJUSTMENT(Adjust));
///  g_signal_connect(G_OBJECT(pScrollbar), "value-changed",
//		    G_CALLBACK(SciGL::OnScrollbarChange), NULL);
  //gtk_box_pack_start(GTK_BOX(vbox), pScrollbar, TRUE, TRUE, 0);

 
  gtk_widget_show_all(window); 

  return 0;
}


gboolean SciGL::draw (GtkWidget      *widget,
      GdkEventExpose *event,
      gpointer        data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

  /*** OpenGL BEGIN ***/
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
    return FALSE;


  scene->render ();

  if (gdk_gl_drawable_is_double_buffered (gldrawable))
    gdk_gl_drawable_swap_buffers (gldrawable);
  else
    glFlush ();

  gdk_gl_drawable_gl_end (gldrawable);
  /*** OpenGL END ***/

  return TRUE;
}

/* new window size or exposure */
gboolean SciGL::reshape (GtkWidget         *widget,
	 GdkEventConfigure *event,
	 gpointer           data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

  /*** OpenGL BEGIN ***/
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
    return FALSE;

  scene->resize (widget->allocation.width, widget->allocation.height);

  gdk_gl_drawable_gl_end (gldrawable);
  /*** OpenGL END ***/

  return TRUE;
}

