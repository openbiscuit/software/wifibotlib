#ifndef IPREDATOR_H
#define IPREDATOR_H

/**
 * \file
 * classe pour le dessin du prédateur
 */
class IPredator : public IUnitElement
{
public:
	IPredator(void);
	void resetPosition(void);
	void doDraw(void);
	void advance(int offset);
	void turn(float offset);
private:	
};

#endif

