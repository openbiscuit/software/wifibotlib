#include <gtk/gtk.h>
#include <cairo.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include "IUnitElement.h"
#include "IPredator.h"

IPredator::IPredator(void)
{
	
}


void IPredator::doDraw(void)
{
  beginDraw();
  
  setColor(0, .2, .5);
  
  Circle(20);
  
  Line(20, 30, 5);
  Arc(20, 20, 10, 10, 5, 10);
  
  Line(20, -30, 5);
  Arc(20, -40, 10, 10, 5, 10);
  
  setColor(1, 1, 1);
  Circle(5, 5, 5, 1);
  Circle(5, -5, 5, 1);
  
  setColor(0, 0, 0);
  Circle(5, 5, 2, 1);
  Circle(5, -5, 2, 1);	
  
  endDraw();
}

void IPredator::resetPosition(void)
{
  posx = posy = 0;
  _t_x =  40; //rand()/(((double)RAND_MAX + 1) / 400);
  _t_y =  250; //rand()/(((double)RAND_MAX + 1) / 300);
  _r =  4.85; //rand()/((double)RAND_MAX) ;
}

void IPredator::advance(int offset)
{
  _t_x = (_t_x + offset * cos(_r));
  _t_y = (_t_y + offset * sin(_r));
}

void IPredator::turn(float offset)
{
  _r = _r + offset;
}


