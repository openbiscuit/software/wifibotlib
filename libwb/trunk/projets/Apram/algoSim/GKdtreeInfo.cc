#include <gtk/gtk.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <time.h>
#include <cairo.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>

// algo
#include "Singleton.h"
#include "List.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"

#include "GKdtreeInfo.h"



GKdtreeInfo::GKdtreeInfo(void) : _nb_points(0)
{

}

void GKdtreeInfo::Show(void)
{
  _window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(_window), "Kd-tree view");
  gtk_widget_set_size_request (_window, GDKTREEINFO_WITH,  GDKTREEINFO_HEIGHT);
  gtk_window_move((GtkWindow *)_window, 200, 600);

  pVBox = gtk_vbox_new(TRUE, 0);
  gtk_container_add(GTK_CONTAINER(_window), pVBox);

  _labelNbPoints = gtk_label_new("nb points");
  _labelBestAction = gtk_label_new("conseil");
  
   store = gtk_tree_store_new (N_COLUMNS,
                               G_TYPE_STRING,
                               G_TYPE_STRING);
   tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));

   g_object_unref (G_OBJECT (store));

   renderer = gtk_cell_renderer_text_new ();
   g_object_set (G_OBJECT (renderer),
                 "foreground", "red",
                 NULL);

   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("Variable",
                                                      renderer,
                                                      "text", TITLE_COLUMN,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_toggle_new ();
   column = gtk_tree_view_column_new_with_attributes ("Value",
                                                      gtk_cell_renderer_text_new(),
                                                      "text", VALUE_INT,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   gtk_box_pack_start(GTK_BOX(pVBox), tree, TRUE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(pVBox), _labelNbPoints, TRUE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(pVBox), _labelBestAction, TRUE, FALSE, 0);
  

  gtk_widget_show_all(_window);
  index_item = 0;
  AddColumn((char *)"PRED_NORM", 0);
  AddColumn((char *)"PRED_EXACT", 0);
  AddColumn((char *)"PRED_NOT_ENOUGH", 0);
  AddColumn((char *)"PRED_BOUNDS", 0);
  AddColumn((char *)"PRED_EXT", 0);
}

void GKdtreeInfo::AddColumn(char *name, int value)
{
  strcpy(items[index_item].name, name);
  sprintf(items[index_item].value, "%d", value);
  gtk_tree_store_append (store, &items[index_item].iter, NULL);

  gtk_tree_store_set (store, &items[index_item].iter,
                    TITLE_COLUMN, items[index_item].name,
                    VALUE_INT, items[index_item].value,
                    -1);
  index_item++;
}

void GKdtreeInfo::SetColumn(char *name, int value)
{
  int i = 0;
  for(i=0;i<index_item;i++)
    {
      if(strstr(name, items[i].name))
	break;
    }
  if(i == index_item || index_item == 0)
    {
      return ;
    }

  memset(items[i].value, 0, 10);
  sprintf(items[i].value, "%d", value);
//  gtk_tree_store_append (store, &items[index_item].iter, NULL);

  gtk_tree_store_set (store, &items[i].iter,
                    TITLE_COLUMN, items[i].name,
                    VALUE_INT, items[i].value,
                    -1);
}


void GKdtreeInfo::AddPoint(void)
{
  char _txtPointsLabels[10];
  _nb_points++;
  sprintf(_txtPointsLabels, "%d", _nb_points);
  gtk_label_set_text((GtkLabel *)_labelNbPoints, _txtPointsLabels);
}


void GKdtreeInfo::SetPoint(int nb)
{
  char _txtPointsLabels[10];
  _nb_points = nb;
  sprintf(_txtPointsLabels, "%d", _nb_points);
  gtk_label_set_text((GtkLabel *)_labelNbPoints, _txtPointsLabels);
}

void GKdtreeInfo::SetBestAction(double act)
{  
  char _txt[20];
  memset(_txt, 0, 20);
  CHedger::ActionToString(act, _txt);
  gtk_label_set_text((GtkLabel *)_labelBestAction, _txt);
}
