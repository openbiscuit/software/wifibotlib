#ifndef SCI_GL_H
#define SCI_GL_H

// algo
#include "Singleton.h"
#include "List.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"
#include "CPeng.h"

#include "scene.h"
#include "terminal.h"
#include "colorbar.h" 
#include "smooth-surface.h"
#include "frame.h"
#include "data.h"


class SciGL
{
 public:
  SciGL(void);
  ~SciGL(void);
  /**
   * création de la fenêtre opengl
   */
  int create(void);
  /**
   * mise à jour des infos d'affichage
   */
  void compute_algo(CHedger *smart, bool raw);
  static CKdtreeExport *out;
 private:

  static void set_axis_label(const char *x, const char *y, const char *z);
  static void set_x_range(float min, float max, float size=1);
  static void set_y_range(float min, float max, float size=1);
  static void set_z_range(float min, float max, float size=1);
  static void set_frame_size(float x, float y, float z);
  void upd(float **tab); 
  void upd(int i, int j, float v);
  
  
 


  static gboolean draw (GtkWidget      *widget,
			GdkEventExpose *event,
			gpointer        data);
  static gboolean reshape (GtkWidget         *widget,
			   GdkEventConfigure *event,
			   gpointer           data);
  static void init(GtkWidget *widget, gpointer   data);
  static gboolean  idle (GtkWidget *widget);
  static void  idle_add (GtkWidget *widget);
  static void idle_remove (GtkWidget *widget);
  static gboolean    map (GtkWidget   *widget,
	 GdkEventAny *event,
	 gpointer     data);
  static gboolean unmap (GtkWidget   *widget,
	   GdkEventAny *event,
	   gpointer     data);
  static gboolean visible (GtkWidget          *widget,
	     GdkEventVisibility *event,
	     gpointer            data);
  static gboolean key (GtkWidget   *widget,
	 GdkEventKey *event,
	 gpointer     data);
  static gboolean button_press_event (GtkWidget      *widget,
			GdkEventButton *event,
			gpointer        data);

  static gint motion_notify_event( GtkWidget *widget, GdkEventMotion *event, gpointer obj);
  static void OnScrollbarChange(GtkWidget *pWidget, gpointer data);

  ////// ATTRIBUTS
//  static int _iScrollbarValue;
  static Scene *scene;
  static Frame *frame;
  static Data *data_graph;
  static  SmoothSurface *surf;
  static float *value_data ;

  GtkWidget *window;
//  static GtkObject *Adjust;
 // static GtkWidget *pScrollbar;
  static guint idle_id;

  static CKdtree *tree;
  static CHedger *smart;

  static SciGL *graph;
  static CList list_graph;
  static CPeng *peng;
  int g_resolution_x, g_resolution_y;


};

#endif

