#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <cairo.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>

// cairo
#include "IUnitElement.h"
#include "ITarget.h"
#include "IPredator.h"

// algo
#include "Singleton.h"
#include "List.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"
#include "CPeng.h"

// gtk
#include "GKdtreeInfo.h"
#include "scene.h" 
#include "terminal.h" 
#include "colorbar.h" 
#include "smooth-surface.h"
#include "frame.h"
#include "data.h"
#include "SciGL.h"

#define DEFAULT_WIDTH  400
#define DEFAULT_HEIGHT 300


// global, à modifier
IPredator predateur;
ITarget cible;
//CKdtree tree;
SciGL graph;

GKdtreeInfo tree_infos;
	int best_action = 0;
//the global pixmap that will serve as our buffer
static GdkPixmap *pixmap = NULL;

CKdtree *tree = new CKdtree();
CHedger *smart = new CHedger(tree);
CPeng *peng = NULL;

gboolean on_window_configure_event(GtkWidget * da, GdkEventConfigure * event, gpointer user_data){
    static int oldw = 0;
    static int oldh = 0;
    //make our selves a properly sized pixmap if our window has been resized
    if (oldw != event->width || oldh != event->height){
        //create our new pixmap with the correct size.
        GdkPixmap *tmppixmap = gdk_pixmap_new(da->window, event->width,  event->height, -1);
        //copy the contents of the old pixmap to the new pixmap.  This keeps ugly uninitialized
        //pixmaps from being painted upon resize
        int minw = oldw, minh = oldh;
        if( event->width < minw ){ minw =  event->width; }
        if( event->height < minh ){ minh =  event->height; }
        gdk_draw_drawable(tmppixmap, da->style->fg_gc[GTK_WIDGET_STATE(da)], pixmap, 0, 0, 0, 0, minw, minh);
        //we're done with our old pixmap, so we can get rid of it and replace it with our properly-sized one.
        g_object_unref(pixmap); 
        pixmap = tmppixmap;
    }
    oldw = event->width;
    oldh = event->height;
    return TRUE;
}

gboolean on_window_expose_event(GtkWidget * da, GdkEventExpose * event, gpointer user_data){
    gdk_draw_drawable(da->window,
        da->style->fg_gc[GTK_WIDGET_STATE(da)], pixmap,
        // Only copy the area that was exposed.
        event->area.x, event->area.y,
        event->area.x, event->area.y,
        event->area.width, event->area.height);
    return TRUE;
}


static int currently_drawing = 0;
//do_draw will be executed in a separate thread whenever we would like to update
//our animation
void *do_draw(void *ptr){

    currently_drawing = 1;
//	ITarget cible;

    int width, height;
    gdk_threads_enter();
    gdk_drawable_get_size(pixmap, &width, &height);
    gdk_threads_leave();

    //create a gtk-independant surface to draw on
    cairo_surface_t *cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cairo_t *cr = cairo_create(cst);

    //do some time-consuming drawing
    static int i = 0;
    ++i; i = i % 300;   //give a little movement to our animation

    
    cairo_paint (cr);
    cairo_set_source_rgb(cr, 0.6, 0.6, 0.6);
    cairo_rectangle(cr, 0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    cairo_fill(cr);

    cible.setCairo(cr);
    cible.setPos(DEFAULT_WIDTH/2, 30);
    cible.doDraw();
	
    predateur.setCairo(cr);
	//predateur.setPos(i, i);
    predateur.doDraw();
	
    cairo_destroy(cr);


    //When dealing with gdkPixmap's, we need to make sure not to
    //access them from outside gtk_main().
    gdk_threads_enter();

    cairo_t *cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);

    gdk_threads_leave();

    cairo_surface_destroy(cst);

    currently_drawing = 0;

    return NULL;
}

gboolean timer_exe(GtkWidget * window){

    static gboolean first_execution = TRUE;

    //use a safe function to get the value of currently_drawing so
    //we don't run into the usual multithreading issues
    int drawing_status = g_atomic_int_get(&currently_drawing);

    //if we are not currently drawing anything, launch a thread to 
    //update our pixmap
    if(drawing_status == 0){
        static pthread_t thread_info;
        int  iret;
        if(first_execution != TRUE){
            pthread_join(thread_info, NULL);
        }
        iret = pthread_create( &thread_info, NULL, do_draw, NULL);
    }

    //tell our window it is time to draw our animation.
    int width, height;
    gdk_drawable_get_size(pixmap, &width, &height);
    gtk_widget_queue_draw_area(window, 0, 0, width, height);

    first_execution = FALSE;

    return TRUE;

}

void trace_append(const char *format, ...)
{
  char *s = NULL;
  FILE *pf = fopen("./traces.txt", "a");
  if(pf == NULL)
    return ;
  if (format)
    {
      char t[1];
      va_list pa;
      size_t size = 0;
      
      va_start (pa, format);
      size = vsnprintf (t, 1, format, pa);
      size++;
      s = new char[(sizeof (*s) * size)];
      if (s)
	{
	  vsnprintf (s, size, format, pa);
	}
    }
  fprintf(pf, "%s\n", s);
  fclose(pf);
  if(s)
    delete s;
}

gboolean key_press (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	KdVector *x;
	KdVector *xp;

	CHedger_Action act = HEDGER_ACTION_NOTHING;
	CKdtreeExport *spe = new CKdtreeExport();
	if(event->keyval == GDK_Up)
	  {
	    predateur.advance(20);
	    act = HEDGER_ACTION_FORWARD;
	  }
	if(event->keyval == GDK_Left)
	  {
	    predateur.turn(-.3);
	    act =  HEDGER_ACTION_TURNONLEFT;
	  }
	if(event->keyval == GDK_Right)
	  {
	    predateur.turn(.3);
	    act = HEDGER_ACTION_TURNONRIGHT;
	  }
	if(event->keyval == GDK_Down)
	  {
	    predateur.advance(-20);
	    act = HEDGER_ACTION_BACKWARD;
	  }
	double xrel = cible.posx - (predateur._t_x);
	double yrel = cible.posy - (predateur._t_y);
	double distance = (int)sqrt(xrel*xrel+yrel*yrel);
	double angle = (int)(fabs(atan2(yrel, xrel) - predateur._r)*100);
	// distance min = 5, distance max = 300
	distance = (distance - 5) / 295;
	// angle min = 400, angle max = 800
	angle = (angle - 400) / 800;
	static double old_distance = distance;
	static double old_angle = angle;

	if(act !=  HEDGER_ACTION_NOTHING)
	  {
	    smart->setDistance(distance);
	    smart->setAngle(angle);

	    if(distance < 0.2)
	      smart->_rewards = 10;
	    else
	      smart->_rewards = 0;
	    
	    xp = CKdtree::NewKdVector(2);
	    xp->value[0] = distance;
	    xp->value[1] = angle;

	    x = CKdtree::NewKdVector(2);
	    x->value[0] = old_distance;
	    x->value[1] = old_angle;
	    
	    best_action = smart->Training(x, xp, act);

	    trace_append("%f\t%f\t%d\t%f\t%f\t%d",
			 old_distance, old_angle,
				   act,
			 distance, angle,
				   smart->_rewards);

	    tree_infos.SetPoint(smart->S->GetTotalSize());
	    tree_infos.SetBestAction(best_action);
	

	    printf("distance : %f ", distance);
	    printf("angle : %f ", angle);
	    printf("recompense : %d", smart->Rewards());
	    printf(" best action : %d\n", best_action);
	    
	    old_distance = distance;
	    old_angle = angle;
	     //graph.compute_algo(smart, false);
	  }

	if(event->keyval == GDK_r)
	{ // reset
	  if(peng != NULL)
	    smart->_peng->reset();
	  predateur.resetPosition();
	  old_distance = -2;
	  old_angle = -2;
	}
	if(event->keyval == GDK_s)
	{ // save
	  smart->S->SaveTo((char *)"./data.txt");
	  printf((char *)"save\n");
	}

	if(event->keyval == GDK_i)
	{ // save
	  //CKdtreeExport *spe = new CKdtreeExport();
	  spe->setLimits(0, -250, 500, 250);
	  spe->setPrecision(5, 5);
	  spe->setKdtree(smart);
	  spe->Interpol((char *)"interpol.txt", HEDGER_ACTION_FORWARD);
	}

	if(event->keyval == GDK_j)
	{ // save
	  graph.compute_algo(smart, false);
	  tree_infos.SetColumn((char *)"PRED_NORM", graph.out->_TypeOfPrediction[PRED_NORMAL]);
	  tree_infos.SetColumn((char *)"PRED_EXACT", graph.out->_TypeOfPrediction[PRED_EXACT]);
	  tree_infos.SetColumn((char *)"PRED_NOT_ENOUGH", graph.out->_TypeOfPrediction[PRED_NOT_ENOUGH]);
	  tree_infos.SetColumn((char *)"PRED_BOUNDS", graph.out->_TypeOfPrediction[PRED_BOUNDS]);
	  tree_infos.SetColumn((char *)"PRED_EXT", graph.out->_TypeOfPrediction[PRED_EXT]);
	}

	if(event->keyval == GDK_k)
	{ // save
	   graph.compute_algo(smart, true);
	}

	if(event->keyval == GDK_p)
	  {
	//    peng = new CPeng(smart);
	//    smart->_bPeng = !smart->_bPeng ;
//k	    printf(" CHedger::getInstance()->_bPeng %d",  smart->_bPeng);
	  }

	if(event->keyval == GDK_w)
	{ // save
	  //CKdtreeExport *spe = new CKdtreeExport();
	  spe->setLimits(0, -150, 400, 150);
	  spe->setPrecision(1, 1);
	  spe->setKdtree(smart);
	  spe->Raw((char *)"raw.txt", HEDGER_ACTION_FORWARD);
	  printf("export raw ok\n");
	}


/*
	if(event->keyval == GDK_l)
	{ // save
	  int xx, yy;
	  GdkModifierType state;
	  gdk_window_get_pointer (event->window, &xx, &yy, &state);
	  double xxrel = cible.posx - (xx);
	  double yyrel = cible.posy - (yy);
	  double ddistance = (int)sqrt(xxrel*xxrel+yyrel*yyrel);
	  KdVector *xp = CKdtree::NewKdVector(3);
	  xp->value[0] = ddistance;
	  xp->value[1] = 0;
	  xp->value[2] = HEDGER_ACTION_FORWARD;
	  double mindist = 0;
	  int status = 0;
	  KdVectorList *list = CKdtree::NewKdVectorList();
  
	  double v = smart->Prediction(NULL, xp, list, &mindist, &status);
	  printf("xx : %d, yy : %d %f value = %f\n", xx, yy, ddistance, v);
	}
*/
	if(event->keyval == GDK_q)
	{ // quit
	  gtk_main_quit ();
	}



	return TRUE;
}


int main (int argc, char *argv[])
{
    //we need to initialize all these functions so that gtk knows
    //to be thread-aware
    if (!g_thread_supported ()){ g_thread_init(NULL); }
    gdk_threads_init();
    gdk_threads_enter();

    srand (time (NULL));

    gtk_init(&argc, &argv);

    GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_move((GtkWindow *)window, 200, 200);
    predateur.resetPosition();

    
    //tree->LoadFrom((char *)"./data.txt");
   // tree->SaveTo("./dup.txt");
    smart->readTraces((char *)"./traces.txt");
    
    //return 0;
    gtk_widget_set_size_request (window, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(G_OBJECT(window), "expose_event", G_CALLBACK(on_window_expose_event), NULL);
    g_signal_connect(G_OBJECT(window), "configure_event", G_CALLBACK(on_window_configure_event), NULL);
    g_signal_connect(G_OBJECT(window), "key-release-event", G_CALLBACK(key_press), NULL);
	
    //this must be done before we define our pixmap so that it can reference
    //the colour depth and such
    tree_infos.Show();
   // SciGL graph;
    graph.create();
//
    //start_graph();
    gtk_widget_show_all(window);

    //tree_infos.Show();
//    smart->_bandwith = 90;
    smart->_verbosity = 3;
    peng = new CPeng(smart);
    //   CHedger::getInstance()->_bPeng = true;
    tree_infos.SetPoint(smart->S->GetTotalSize());
    
   
    //set up our pixmap so it is ready for drawing
    pixmap = gdk_pixmap_new(window->window, DEFAULT_WIDTH, DEFAULT_HEIGHT, -1);
    //because we will be painting our pixmap manually during expose events
    //we can turn off gtk's automatic painting and double buffering routines.
    gtk_widget_set_app_paintable(window, TRUE);
    gtk_widget_set_double_buffered(window, FALSE);

    (void)g_timeout_add(33, (GSourceFunc)timer_exe, window);
  //  g_timeout_add(50, (GSourceFunc)ano, window);

    gtk_main();
    gdk_threads_leave();

    return 0;
}


