#ifndef ITARGET_H
#define ITARGET_H

/**
 * \brief dessine la cible
 */
class ITarget : public IUnitElement
{
public:
	ITarget(void);
	
	void doDraw(void);
private:	
};

#endif

