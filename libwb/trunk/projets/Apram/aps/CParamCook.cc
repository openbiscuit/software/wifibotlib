#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "CParamCook.h"
#include "CParam.h"

CParamCook::CParamCook(void)
{
  _list_params = new CList();
  _factory = new CAlgoFactory();
  _current_param = -1;
  _nb_config = 1;
  _current_config = 1;
}


CParamCook::~CParamCook(void)
{
  delete _list_params;
  delete _factory;
  fclose( _config );
  fclose( _result );
}

bool CParamCook::load(char *filename)
{
  xmlTag *search;
  xmlTag *p;

  if(_factory->load() == false)
    return false;

  if(Initialize(filename) == false)
    return false;
 
  search = Search((char *)"aps");
  if(search == NULL)
    return false;
  p = GetChild(search);
  while(p != NULL)
    {
      read_param(p);
      p = NextChild(p);
    }
  FILE *pf = fopen("result.txt", "w");
  fclose(pf);
  pf = fopen("features.txt", "w");
  fclose(pf);
  pf = fopen("config.txt", "w");
  for(int i=0;i<_list_params->GetLength();i++)
    {
      CParam *p = (CParam *)_list_params->Get(i);
      fprintf(pf, "#%s ", p->_name);
    }
  fprintf(pf, "\n");
  fclose(pf);
  _config = fopen("config.txt", "a");
  _result = fopen("result.txt", "a");
//  printf("%d\n", _nb_config);
  return true;
}


void CParamCook::cook(void)
{
  compute(0);
}

void CParamCook::compute(int id_param)
{
 
  CParam *p = (CParam *)_list_params->Get(id_param);

  p->_current = p->_init_value;

  if(id_param == _list_params->GetLength()-1)
    {
      while( p->_current <= p->_end_value )
	{
	//  sleep(2);
	  snapshot();
//	  while(1)sleep(2);
	  p->_current += p->_step;
	}
//      snapshot();
    }

  while( p->_current <= p->_end_value )
    {
	compute(id_param+1);
	p->_current += p->_step;
    } 
}
 
void CParamCook::snapshot(void)
{
  char temp[40];
  static int j = 0;
//  CAlgoFactory *f = new CAlgoFactory();
//  f->_tr = _factory->_tr;

  
  fprintf(_config, "%d ", j);
  for(int i=0;i<_list_params->GetLength();i++)
    {
      CParam *p = (CParam *)_list_params->Get(i);
      _factory->set_parameter(p->_name,
			      p->_current);
      printf("%s %f ", p->_name, p->_current);
      fprintf(_config, "%f ", p->_current);
    }
  fprintf(_config, "\n");
  printf("\n");

  _factory->run();
  sprintf(temp, "convert ii.ppm %d.bmp", j);
  system(temp);
  sprintf(temp, "convert status.ppm %ds.bmp", j);
  system(temp);
  fprintf(_result, "%d ", j);
  for(int k=0;k<PRED_NULL;k++)
    {
      fprintf(_result, "%d ", _factory->out->_TypeOfPrediction[k]);
    }
  fprintf(_result, "\n");
 // delete f;
  //_factory->kill();
  _current_config ++;
  j++;
}

void CParamCook::read_param(xmlTag *tag)
{
  CParam *param = NULL;
  if(tag->Attr.size() != 5)
    {
      printf("erreur chargement param");
      return ;
    }
  if(strcmp(tag->Attr[1]->pcValue, "off") == 0)
    return ;
  param = new CParam();
  param->set_name( tag->Attr[0]->pcValue );
  param->_init_value = atof(tag->Attr[2]->pcValue );
  param->_end_value = atof(tag->Attr[3]->pcValue );
  param->_step = atof( tag->Attr[4]->pcValue );
  param->_current = param->_init_value;

  int nb = (param->_end_value - param->_init_value) / param->_step;
  if(nb != 0)
    _nb_config *= nb;
  _list_params->Add(param);
}
