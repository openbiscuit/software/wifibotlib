/**
 * \file
 * \brief Utilise les algorithmes avec des valeurs de paramètres définis
 */
#ifndef algo_factory_h
#define algo_factory_h

#include <stdio.h>
// algo
#include "List.h"
#include "CKdtree.h"
#include "CHedger.h"
#include "CKdtreeExport.h"
#include "CPeng.h"

#define TRACE_FILE "./traces.txt"

typedef struct
{
  double old_dist;
  double old_angle;
  int action;
  double dist;
  double angle;
  float reward;
} item_list;

class CAlgoFactory
{
 public:
  CAlgoFactory(void);
  ~CAlgoFactory(void);
  
  ///< charge le fichier de traces
  bool load(void);
  
  /**
   * Définit la valeur d'un paramètre
   * @param name nom du paramètre
   * @param value valeur du paramètre
   */
  void set_parameter(char *name, float value);
  
  ///< test l'algorithme
  void run(void);

  CList *_tr; ///< traces
  CKdtree *tree; ///< instance kdtree
  CHedger *smart; ///< instance hedger
  CKdtreeExport *out; ///< exportation
  CPeng *peng; ///< instance peng
};


#endif
