#ifndef param_cook_h
#define param_cook_h

#include "List.h"
#include "XmlParser.h"
#include "CAlgoFactory.h"

class CParamCook : public CXmlParser
{
 public:
  CParamCook(void);
  ~CParamCook(void);
  /**
   * Charge le fichier xml de configuration
   * @param filename nom du fichier xml
   * @return true si le chargement est ok
   * @return false dans le cas inverse
   */
  bool load(char *filename);
  ///< lance la série d'apprentissage
  void cook(void);
 private:
 	/**
 	 * Lit un paramètre du fichier xml
 	 * @param tag pointeur sur le tag à lire
 	 */
	void read_param(xmlTag *tag);
	/**
	 * Lance une simulation
	 * @param id_param 0 = première configuration d'instance
	 */
	void compute(int id_param);
	///< Réalise l'apprentissage
	void snapshot(void);
	///< paramère courant
	int _current_param;
	///< instance pour réaliser la simulation
	CAlgoFactory   *_factory;
	///< liste des paramètres
	CList          *_list_params;
	///< handle du fichier configuration
	FILE *_config;
	///< handle du fichier de résultat
	FILE *_result;
	///< nombre de configuration
	int _nb_config;
	///< configuration courante
	int _current_config;
};

#endif
