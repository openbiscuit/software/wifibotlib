#include <stdio.h>

#include "CParamCook.h"

// automatic parameters selection
int main(void)
{
  CParamCook *Chief = new CParamCook();
  
  if(Chief->load((char *)"params.xml") == false)
    {
      //erreur 
      return 0;
    }
  Chief->cook();
  
  return 0;
}
