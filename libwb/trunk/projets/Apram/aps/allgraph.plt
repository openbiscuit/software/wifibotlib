set terminal jpeg transparent interlace medium size 500, 300
#set multiplot
#set grid
#set mxtics 2
#set mytics 2
#set grid xtics ytics mxtics mytics

set output "allgraph.jpg"

set title "Type de prediction"

set xlabel "configurations"
set ylabel "quantite"

plot "result.txt" using 1:2 smooth bezier title "normal" with linespoints lt 2 pt -1 lw 2., "result.txt" using 1:3 smooth bezier title "exact" with linespoints lt 3 pt -1 lw 2, "result.txt" using 1:4 smooth bezier title "pas assez de points" with linespoints lt 4 pt -1 lw 2, "result.txt" using 1:5 smooth bezier title "hors ivh" with linespoints lt 5 pt -1 lw 2, "result.txt" using 1:6 smooth bezier title "en dehors des bornes" with linespoints lt 6 pt -1 lw 2, "result.txt" using 1:7 smooth bezier title "etats externes" with linespoints lt 7 pt -1 lw 2


