#include <string.h>

#include "CAlgoFactory.h"

//CList CAlgoFactory::_tr = new CList();

CAlgoFactory::CAlgoFactory(void)
{
  _tr = new CList();
  tree = new CKdtree();
  smart = new CHedger(tree);
  out = new CKdtreeExport();
  peng = NULL;
  out->setLimits(0.0, 0.0, 1.0, 1.0);
 // out->setLimits(60, 40, 160, 140);
  out->setPrecision(0.01, 0.01);
  out->Allocate();
  out->initValuesImage();
  out->setKdtree(smart);
}

CAlgoFactory::~CAlgoFactory(void)
{
  delete tree;
  delete smart;
  delete out;
}


void CAlgoFactory::set_parameter(char *name, float value)
{
  if(value < 0) return ;
  if(strcmp(name, "bandwith") == 0)
     smart->_bandwith = value;
  if(strcmp(name, "alpha") == 0)
     smart->_alpha = value;
  if(strcmp(name, "gamma") == 0)
     smart->_gamma = value;
  if(strcmp(name, "lambda") == 0)
    {
      if(peng == NULL) peng = new CPeng(smart);
      peng->_lambda = value;
    }
  if(strcmp(name, "points") == 0)
    {
      smart->_nb_points = (int)value;
    }
  if(strcmp(name, "pointsmax") == 0)
    {
      smart->_nb_points_max = (int)value;
    }
  if(strcmp(name, "pointsfeuilles") == 0)
    {
      smart->S->_limit_of_bag = (int)value;
    }
}

void CAlgoFactory::run(void)
{
  KdVector *x;
  KdVector *xp;
  for(int i=0;i<_tr->GetLength();i++)
    {
      item_list *il = (item_list *)_tr->Get(i);
      x = CKdtree::NewKdVector(2);
      x->value[0] = il->old_dist;
      x->value[1] = il->old_angle;
      
      xp = CKdtree::NewKdVector(2);
      xp->value[0] = il->dist;
      xp->value[1] = il->angle;

      smart->_rewards = il->reward;
      
      smart->Training(x, xp, il->action);
      smart->S->FreeMem(x);
      smart->S->FreeMem(xp);
    }
  
  out->InterpolImage( (char *)"ii.ppm", (char *)"status.ppm", HEDGER_ACTION_FORWARD );
//  out->InterpolImage( (char *)"ii2.ppm", (char *)"status2.ppm", HEDGER_ACTION_TURNONRIGHT );
  smart->S->FreeMem();
  smart->S->init();
  delete peng;
  peng = NULL;
  smart->_peng = NULL;

 // tree->FreeMem();
}

bool CAlgoFactory::load(void)
{
  float odi, oan, di, an;
  int act;
  float dist_min, dist_max, angle_min, angle_max;
  FILE * pf = fopen("traces.txt", "r");
  if(pf == NULL)
    {
      printf("pas de fichier traces.txt !\n");
      return false;
    }

  dist_max = angle_max = 0;
  dist_min = angle_min = 0xFFFF;
  float r;
  char *p;
  char line[255];
  memset(line, 0, 255);

  while ((fscanf(pf, "%[^\n]", line)) != EOF)
    {
      fgetc(pf);
    if(strlen(line) > 0)
	{
	  p = line;
	  odi = oan = act = di = an = 0;
	  r = 0.0;

	  for(unsigned int k=0;k<strlen(line);k++)
	    {
	      if(line[k] == ',')
		line[k] = '.';
	    }
	  sscanf(line, "%f\t%f\t%d\t%f\t%f\t%f",
		 &odi, &oan, &act, &di, &an, &r);
	  
	  if(di < dist_min) dist_min = di;
	  if(di > dist_max) dist_max = di;
	  if(an < angle_min) angle_min = an;
	  if(an > angle_max) angle_max = an;

	  
	  item_list *il = new item_list;
	  il->old_dist = odi;
	  il->old_angle = oan;
	  il->action = act;
	  il->dist = di;
	  il->angle = an;
	  il->reward = r;

	  _tr->Add(il);
	}
    memset(line, 0, 255);
    }
  fclose(pf);
  return true;
}
