
myplot()
{
	echo sh graph.sh $1 \"$2\" \>\> temp.temp >> a.temp
}

myplot 2 "normal"
myplot 3 "exact"
myplot 4 "pas assez de points"
myplot 5 "hors ivh"
myplot 6 "en dehors des bornes"
myplot 7 "etats externes"


sh a.temp
gnuplot temp.temp
rm a.temp
rm temp.temp

