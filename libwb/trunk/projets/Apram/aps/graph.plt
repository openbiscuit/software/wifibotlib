#set term post eps enhanced color solid "Times-Roman" 18
set terminal jpeg transparent interlace medium size 800, 600

set output "graph.jpg"

set title "Type de prediction"

set xlabel "configurations"
set ylabel "quantite"
#set xrange [1500:1600]

#plot  
#plot "result.txt" using 1:2 title "normal" with lines, "result.txt" using 1:3 title "exact" with lines, "result.txt" using 1:4 title "pas assez de points" with lines, "result.txt" using 1:5 title "dans ivh" with lines, "result.txt" using 1:6 title "en dehors des bornes" with lines, "result.txt" using 1:7 title "etats externes" with lines
#plot "result.txt" using 1:2 with lines 
plot "result.txt" using 1:4 title "pas assez de points" with lines

