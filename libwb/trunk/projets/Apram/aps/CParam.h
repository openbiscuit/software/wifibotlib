/**
 * \file
 * \brief une instance = un paramètre
 */
#ifndef param_h
#define param_h

class CParam
{
 public:
  CParam(void);
  ~CParam(void);
  /**
   * Définit un nom de paramètre
   * @param name nom
   */
  void set_name(char *name);
  
  ///< nom du paramètre
  char _name[20];
  
  ///< valeur initiale
  float _init_value;
  
  ///< valeur de fin
  float _end_value;
  
  ///< valeur de déplacement (curseur)
  float _step;
  
  ///< valeur courante
  float _current;
};

#endif
