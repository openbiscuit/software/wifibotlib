#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CParam.h"


CParam::CParam(void)
{
  memset(_name, 0, 20);
  _init_value = 0;
  _end_value = 0;
  _step = 0;
  _current = 0;
}

CParam::~CParam(void)
{
}

void CParam::set_name(char *name)
{
  strcpy(_name, name);
}

