/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**                         Maia, Loria.
**
** Original authors: C�dric Bernier, Julien Le Guen.
**  
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/
/*
 * Ensemble de fonctions facilitant la manipulation d'images
 *  - Acquisition depuis la cam
 *  - d�compression � la vol�e
 *  - �criture en ppm (pour tests)
 *
 * Routines utilis�es par le traitement d'image (recherche de balles, ...)
 *
 */

#ifndef H_WB_IMAGE
#define H_WB_IMAGE


/*
 *	R�cup�re une image de la cam�ra via la socket sk.
 *	L'image est plac�e dans le buffer, sa taille est renvoy�e par la fonction.
 *	Attention � fournir un buffer suffisamment grand (au moins 150ko, selon les applications)
 */
int wbGetImage(unsigned char *buffer);


/* 
 * Enregistre une image au format ppm
 *  - fichier 	: chaine de caractere contenant le nom du fichier
 *  - img		: pointeur vers le buffer contenant l'image d�compress�e
 *  - taille	: taille de l'image en octets
 *  - width, height : dimensions de l'image
 * Renvoie 0 en cas de succes.
 */
int wbWriteImage(char *fichier, unsigned char *img, int taille, int width, int height);

/* 
 * D�compresse une image contenue dans un buffer.
 *  - image		: double pointeur sur un buffer qui contiendra l'image d�compress�e (non allou�)
 *  - buffer	: buffer contenant l'image compress�e
 *  - taille	: taille en octets de l'image compress�e
 * Renvoie la taille de l'image d�compress�e (en octets)
 * Met � jour : width et height.
 */
int wbDecompressImage(unsigned char *image, unsigned char *buffer, int taille,
		     int *width, int *height);

#endif
