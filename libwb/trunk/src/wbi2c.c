/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen, Alain Dutech
**                         Maia, Loria.
**
** Original authors: C�dric Bernier, Julien Le Guen.
**  
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/* Fonctions I2C "bas niveau" pour le wifibot 4G
 * 
 * Ces fonctions permettent de contr�ler et de r�cup�rer
 * les information du robot via le bus I2C de la carte
 * mere, qui joue le r�le du maitre du bus.
 * Plusieurs esclaves sont pr�sents sur le bus :
 *  - une carte qui contr�le le niveau de la batterie
 *    que l'on peut interroger
 *  - une carte par moiti� du robot (gauche et droite)
 *    qui contr�le les deux moteurs, et r�cup�re les
 *    informations de vitesse des moteurs ainsi que la
 *    valeur renvoy�e par le capteur de distance IR.
 *
 * Il faut initialiser le bus I2C au d�but du programme,
 * avec la fonction initI2C. Le num�ro du bus est d�fini
 * dans le header.
 *
 * Ensuite on peut modifier la vitesse de chaque cot�
 * avec la fonction setI2CSpeed(adresse, vitesse).
 * L'adresse doit �tre une adresse de carte valide
 * (ADR_LEFT ou ADR_RIGHT) et vitesse doit �tre construit
 * de cette fa�on :
 * - bit 7:	Speed Motor Control
 *  		Permet d'activer le PID des roues
 *  		Asservissement de la vitesse des roues
 *  		par rapport � la consigne.
 *  		1: ON
 *  		0: OFF (par d�faut)
 * - bit 6: Sens des roues
 *   		0: Backward
 *   		1: Forward
 * - bits 5 � 0: Vitesse des roues
 *   		De 0 � 60 en mode "libre"
 *   		De 0 � 40 en mode PID
 *
 * Pour contr�ler le robot, on peut lire la vitesse de
 * chaque roue. Pour cela il faut utiliser la fonction
 * getI2CInfo(adresse). Adresse doit �tre une adresse
 * valide. La fonction met � jour les champs
 * correspondant au c�t� du robot dans la structure
 * winfo de type WifibotInfo. Elle r�cup�re :
 *  - vitesse de la roue avant
 *  - vitesse de la roue arri�re
 *  - valeur du capteur de distance associ� au cot�
 *
 * On peut �galement contr�ler le niveau de la batterie
 * du robot grace � la fonction getI2CBattery().
 * Elle met � jour le champ battery de la structure
 * winfo.
 *
 * Il est recommand� d'utiliser le mode PID pour le
 * controle de la vitesse, car le robot v�rifie en
 * permanence la vitesse des roues et r�agit bien mieux
 * aux changements de consigne.
 */

/**
 * @file   wbi2c.c
 *
 * @brief Functions using I2C to set speed and gather sensor values.
 *
 * @author C�dric Barnier
 * @author Alain Dutech
 * @author Julien LeGuen
 * @date   Fri Apr 20 15:11:50 2007
 * 
 * <b>French comments can be found in the source file</b>.<br>
 *
 * These functions use the I2C bus to control the speed of the robot
 * and gather sensor values. The motherboard is the master of the bus,
 * and several slaves do exists:
 *  - card for the battery, can ask for batteru level.
 *  - one card for each lateral side of the robot (right and left). Can 
 * set wheel speed and gather IR sensor value.
 *
 * The I2C bus must be initialized with wbIniI2C(). I2C bus number is defined
 * in the header.
 *
 * wbSetI2CSpeed( address, speed) set the speed. 'address' must be a valid
 * one (either WB_ADR_LEFT ou WB_ADR_RIGHT) and speed is build as follow:
 *  - bit 7: speed motor control. Activate the PID of the wheel with regulation
 *    - 1: 0N
 *    - 0: OFF (by default)
 *  - bit 6: rotation direction
 *    - 0: backward
 *    - 1: forward
 *  - bits 5 to 0: speed of the wheel
 *    - [0,60] in free mode
 *    - [0,40] in PID mode
 *
 * It is also possible to gather info from the wifibot using wbGetI2CBattery()
 * and wbGetI2CInfo() that update the global 'WBInfo info' variable. 
 * 
 */


#include <stdio.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include "libwb/wbi2c.h"


/*
 * Defines 
 */

/* 
 * Encore un peu crade ces globales...
 * TODO modifier la lib pour renvoyer des fp et tout...
 */
WBInfo winfo;
/** Private: pointer to the I2C bus */
int file;	/* "pointeur" vers le bus I2C */


/*
 * Initialise le bus I2C num_bus en lecture/ecriture
 */
/** 
 * Initialize the I2C bus 'num_bus' for reading/wrinting.
 * 
 * @param num_bus 
 * 
 * @return WB_I2C_OK or WB_I2C_OPEN_ERROR
 */
int wbInitI2C(int num_bus)
{
	char buf[255];
	 /*
	  * Version powerpc : i2c-%d
	  * Version MIPS : i2c/%d
	  */
	sprintf(buf, "/dev/i2c-%d", num_bus);
	/* Impossible d'ouvrir le bus I2C */
	if((file = open(buf, O_RDWR)) < 0) 
		return WB_I2C_OPEN_ERROR;
	
	/* Tout va bien */
	return WB_I2C_OK;
}


/*
 * Envoie la commande de vitesse au robot.
 * adr est ADR_LEFT ou ADR_RIGHT
 */
/** 
 * Sent speed command to one side of the wifibot.
 * 
 * @param adr is either WB_ADR_LEFT or WB_ADR_RIGHT
 * @param speed 
 * 
 * @return WB_I2C_ADR_ERROR when adr is not valid
 * @return WB_I2C_IO_ERROR when connectin can't be opened.
 * @return WB_I2C_WRITE_ERROR if can not send speed on the bus.
 * @return WB_I2C_OK is everything went allright.
 */
int wbSetI2CSpeed(unsigned char adr, unsigned char speed)
{
	char buf[2];
	buf[0] = speed;
	buf[1] = speed;

	/* On teste ocazou */
	if(adr != WB_ADR_LEFT && adr != WB_ADR_RIGHT)
		return WB_I2C_ADR_ERROR;
	
	/* On ouvre la connexion vers l'esclave */
	if(ioctl(file, I2C_SLAVE, adr) < 0)
		return WB_I2C_IO_ERROR;

	/* On �crit la vitesse */
	if(write(file, buf, 2) != 2)
		return WB_I2C_WRITE_ERROR;

	return WB_I2C_OK;
}


/*
 * R�cup�re les informations de vitesse et distance
 * du robot. Les informations se retrouvent dans la
 * structure globale winfo.
 */
/** 
 * Get speed and distance info from one side of the wifibot.
 *
 * Information are in the global structure 'winfo'.
 * 
 * @param adr is either WB_ADR_LEFT or WB_ADR_RIGHT
 * 
 * @return WB_I2C_ADR_ERROR when adr is not valid
 * @return WB_I2C_IO_ERROR when connectin can't be opened.
 * @return WB_I2C_READ_ERROR if can not read info on the bus.
 * @return WB_I2C_OK is everything went allright.
 *
 * @todo remove global struct winfo.
 */
int wbGetI2CInfo(unsigned char adr)
{
	char buf[3];
	
	/* on teste ocazou */
	if(adr != WB_ADR_LEFT && adr != WB_ADR_RIGHT)
		return WB_I2C_ADR_ERROR;

	/* On ouvre la connexion vers l'esclave */
	if(ioctl(file, I2C_SLAVE, adr) < 0)
		return WB_I2C_IO_ERROR;

	/* on lit les informations de vitesse */
	if(read(file, buf, 3) != 3)
		return WB_I2C_READ_ERROR;

	/* On met � jour les donn�es */
	if(adr == WB_ADR_LEFT)
	{
		winfo.frontLeft = buf[0];
		winfo.rearLeft = buf[1];
		winfo.irLeft = buf[2];
	} else
	{
		winfo.frontRight = buf[1];
		winfo.rearRight = buf[0];
		winfo.irRight = buf[2];
	}

	return WB_I2C_OK;
}


/*
 * R�cup�re l'information de la batterie
 * Met � jour la structure globale winfo
 */
/** 
 * Get battery info from the wifibot.
 *
 * Information are put in the global structure 'winfo'.
 * 
 * @return WB_I2C_ADR_ERROR when adr is not valid
 * @return WB_I2C_IO_ERROR when connectin can't be opened.
 * @return WB_I2C_READ_ERROR if can not read info on the bus.
 * @return WB_I2C_OK is everything went allright.
 *
 * @todo remove global struct winfo.
 */
int wbGetI2CBattery()
{
	char buf[2];
	char buf2;

	/* On ouvre la connexion vers l'esclave */
	if(ioctl(file, I2C_SLAVE, WB_ADR_BAT) < 0)
		return WB_I2C_IO_ERROR;

	/* On lui dit qu'on veut les infos de la batterie */
	buf[0] = 0x40;
	buf[1] = 250;	/* DAC - cf source d'exemple */
	if(write(file, buf, 2) != 2)
		return WB_I2C_WRITE_ERROR;

	/* On r�cup�re l'information de niveau */
	if(read(file, &buf2, 1) != 1)
		return WB_I2C_READ_ERROR;

	winfo.battery = buf2;

	return WB_I2C_OK;
}
