/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 C�dric Bernier, Julien Le Guen,Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: C�dric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/*
 * Ensemble de fonctions facilitant la manipulation d'images
 *  - Acquisition depuis la cam
 *  - d�compression � la vol�e
 *  - �criture en ppm (pour tests)
 *
 * Routines utilis�es par le traitement d'image (recherche de balles, ...)
 *
 */
/**
 * @file   wbimage.c
 * 
 * @brief Functions for easy image manipulation with wifibot.
 *
 * @author C�dric Barnier
 * @author Alain Dutech
 * @author Julien LeGuen
 * @date   Fri Apr 20 12:05:56 2007
 *
 * <b>French comments can be found in the source file</b>.<br>
 * Available functions to:
 *  - get an image from camera.
 *  - decompress jpeg image 'on the fly'.
 *  - write image as a '.ppm' in a file.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/* JPEGLIB is a locally needed library */
#include "jpeglib.h"
#include "libwb/jpeg_tab_src.h"
//#include "../include/jpeglib.h"

#include <string.h>
#include <sys/types.h>	/* pour jouer avec les sockets */
#include <sys/socket.h>
#include <netinet/in.h>
#include "libwb/wbsocket.h"

// c'est crade mais n�cessaire :(
//unsigned int height, width;

/** size of the receiving buffer */
#define SIZE	5000


/** 
 * Get an image from the camera (via internal socket).
 *
 * Uses socket sk defined by wbInitialiseSocket().
 * The image is stored in buffer.
 * <br><b>WARNING<b> : buffer must be allocated and large enough (can need up
 * to 150ko).
 * 
 * @param buffer an allocated buffer that will hold the image.
 * 
 * @return size of the image in bytes.
 */

int wbGetImage(unsigned char *buffer)
{
	unsigned char buf_rec[SIZE];
	int img_size = 0;
	int retval, i;
	int sk;

	sk = wbInitialiseSocket();
	/* On demande le flux JPG */
	send(sk, "GET /IMAGE.JPG HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n", 128, 0);

	/* On commence � r�cup�rer l'image */
	retval = recv(sk, buf_rec, SIZE, 0);
	while(retval != 0)
	{
		/* On copie les donn�es du buffer dans le buffer final */
		memcpy(buffer+img_size, buf_rec, retval);
		img_size += retval;
		retval = recv(sk, buf_rec, SIZE, 0);
	}

	/* Le d�but du buffer est pollu� par les headers HTTP */
	for(i=0; i<img_size-1; i++)
	{
		// une image JPEG commence par 0xFFD8
		if(buffer[i] == 0xFF && buffer[i+1] == 0xD8)
		{
			img_size -= i;
			memmove(buffer, buffer+i, img_size);
			break;
		}
	}

	close(sk);
	/* L'image est enti�rement dans buffer, et fait img_size octets */
	return img_size;
}



/* 
 * Enregistre une image au format ppm
 *  - fichier 	: chaine de caractere contenant le nom du fichier
 *  - img		: pointeur vers le buffer contenant l'image d�compress�e
 *  - taille	: taille de l'image en octets
 *  - width, height : dimensions de l'image
 * Renvoie 0 en cas de succes.
 */
/** 
 * Write the image in a file using .ppm format.
 * 
 * @param fichier name of the file
 * @param img pointer to the decompressed buffer image
 * @param taille size if the image in bytes
 * @param width of the image (pixels)
 * @param height of the image (pixels)
 * 
 * @return 0 if success
 */
int wbWriteImage(char *fichier, unsigned char *img, int taille, int width, int height)
{
	FILE *fp;
	int i;

	fp = fopen(fichier, "w");
	if(fp == NULL)
	{
		fprintf(stderr, "Erreur : impossible d'ouvrir le fichier %s\n", fichier);
		return(1);
	}
	// on �crit les entetes ppm (24 bits RVB)
	fprintf(fp, "P6 %d %d 255 ", width, height);
	// on copie le tableau dans le fichier
	for(i=0; i<taille; i++)
	{
		fprintf(fp, "%c", img[i]);
	}
	
	return fclose(fp);
} /* ecrireImage() */



/* 
 * D�compresse une image contenue dans un buffer.
 *  - image		: double pointeur sur un buffer qui contiendra l'image d�compress�e (non allou�)
 *  - buffer	: buffer contenant l'image compress�e
 *  - taille	: taille en octets de l'image compress�e
 * Renvoie la taille de l'image d�compres�e (en octets)
 * Met � jour : width et height.
 */
/** 
 * Decompress an jpeg image pointed at by buffer.
 * 
 * @param image will contain the decompressed image (will be allocated)
 * @param buffer points to the compressed image
 * @param taille of the compressed image
 * @param width of the image (pixels)
 * @param height of the image (pixels)
 * 
 * @return size of the decompressed image (bytes)
 */
int wbDecompressImage(unsigned char *image, unsigned char *buffer, int taille,
		     int *width, int *height)
{
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	unsigned char *ligne;
	
	/* Initialisation de la structure d'info */
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	/* On lui dit "d�compresse ce qu'il y a dans le buffer l�" */
	jpeg_tab_src(&cinfo, buffer, taille);

	/* On r�cup�re les infos de l'image et on d�compresse */
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	*height = cinfo.output_height;
	*width = cinfo.output_width;

	/* Buffer o� l'image d�compress�e sera plac�e */
	//printf("Allocation m�moire image (%d)\n", width*height*3*sizeof(char));
	//*image = malloc(height*width*3*sizeof(unsigned char));
	ligne = image;

	/* Tant qu'il y a des scanlines, on les place dans le buffer */
	while(cinfo.output_scanline < (*height))
	{
		ligne = image + 3 * (*width) * cinfo.output_scanline;
		jpeg_read_scanlines(&cinfo, &ligne, 1);
	}

	/* On termine proprement */
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	return 3* (*height) * (*width);
} /* decompresseImage() */

