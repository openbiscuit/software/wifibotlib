#ifndef H_WB_CMD
#define H_WB_CMD

#define WB_PROT_FORWARD     'F'
#define WB_PROT_BACKWARD    'R'
#define WB_PROT_TURN        'T'
#define WB_PROT_STOP        'K'
#define WB_PROT_GET_BATTERY 'B'
#define WB_PROT_CONTROL      'A'
#define WB_PROT_GET_SENSORS 'S'

#endif
