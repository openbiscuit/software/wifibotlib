#include <memory.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "wb_socket.h"

int g_socket_client = -1;
int g_socket_server = -1;

SOCKET_STATE wb_socket_receive(int socket, char *data, int max_len)
{
  int received = 0;
  int ret;
  char *p = data;
  do
    {
      ret = recv(socket, p, max_len-received, 0);
      if(ret <= 0) return SOCKET_RCV_ERROR;
      received += ret;
      p += ret;
    } while(received < max_len);
  return SOCKET_OK;
}

SOCKET_STATE wb_socket_send(int socket, char *data, int len)
{
  int sended = send(socket, data, len, 0);
  if(sended <= 0)
    return SOCKET_SND_ERROR;
  return SOCKET_OK;
}

SOCKET_STATE wb_socket_start_server(int port, PtrFunc f)
{
  struct sockaddr_in ServerSock;
  int ServerSockSize = sizeof(struct sockaddr_in);

  struct sockaddr_in CustomerSock;
  int CustomerSockSize = sizeof(struct sockaddr_in);
  char receive[7];
  char send[7];

  g_socket_server = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (!g_socket_server) 
    return SOCKET_INIT_ERROR;
  int on=1;
  setsockopt(g_socket_server, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

  ServerSock.sin_family = AF_INET;
  ServerSock.sin_port = htons(port);
  ServerSock.sin_addr.s_addr = htonl(INADDR_ANY);
 
  if (bind(g_socket_server, (const struct sockaddr*) &ServerSock, ServerSockSize)) 
    return SOCKET_INIT_ERROR;

  if (listen(g_socket_server, 0x800)) 
    return SOCKET_INIT_ERROR;

  while (1) 
    {
      g_socket_client = accept(g_socket_server, (struct sockaddr*) &CustomerSock, (socklen_t*) &CustomerSockSize);
      if (!g_socket_client) continue;
      while(1)
	{
	  memset(receive, 0, 7);
	  memset(send, 0, 7);
	  if(wb_socket_receive(g_socket_client, receive, 7) != SOCKET_OK)
	    break;
	  f(receive, send);
	  if(wb_socket_send(g_socket_client, send, 7) != SOCKET_OK)
	    break;
	}
      close(g_socket_client);
    }
  wb_socket_stop(0);
  return SOCKET_OK;
}

SOCKET_STATE wb_socket_connect(const char *address, int port, int *handle)
{
  struct sockaddr_in serv_addr;
  struct hostent *phost;
  phost = gethostbyname(address);
  if(phost == NULL)
    return  SOCKET_CNX_ERROR;
  *handle = socket(AF_INET, SOCK_STREAM, 0);
  if (*handle == -1) return SOCKET_CNX_ERROR;
  memset(&serv_addr, 0x00, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons((uint16_t)port);
  memcpy(&serv_addr.sin_addr, phost->h_addr, phost->h_length);
  if( connect (*handle, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1 )
    {
      return SOCKET_CNX_ERROR;
    } 
  return SOCKET_OK;
}

void wb_socket_stop(int *handle)
{
  if(handle == 0)
    {
      close(g_socket_server);
      close(g_socket_client);
    }
  else
    {
      close(*handle);
    }
}
