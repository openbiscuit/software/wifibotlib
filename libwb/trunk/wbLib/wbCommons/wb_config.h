#ifndef H_WB_CONF
#define H_WB_CONF

#define WB_CONFIG_FILE "libwb.conf"

typedef enum
  {
    CS_ERROR,
    CS_OK
  } CONFIG_STATE;

/**
 * cette fonction verifie la presence du fichier de configuration
 * @return erreur ou ok
 */
CONFIG_STATE wb_config_init(void);

/**
 * Lit un element du fichier de configuration
 * @param item nom de l'element � lire
 * @param result doit pointeur sur un char * recuperere la valeur de l'element
 * @return erreur ou ok
 */
CONFIG_STATE wb_config_read(const char *item, char *result, int max_size);

/**
 * ferme le handle du fichier
 */
void wb_config_close(void);

#endif

