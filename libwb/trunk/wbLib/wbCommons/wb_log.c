#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "wb_log.h"

int g_level = 0;

void wb_log_verbosity_set_level(int level)
{
  g_level = level;
}

void wb_log_print(char level_msg, const char *format, ...)
{
  if(level_msg <= g_level)
    {
      if (format)
	{
	  va_list pa;
	  
	  va_start (pa, format);
	  vprintf (format, pa);
	}
    }
}
