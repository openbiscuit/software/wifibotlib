#include <stdio.h>
#include <memory.h>
#include "wb_config.h"

FILE *pf = NULL;

CONFIG_STATE wb_config_init(void)
{
  pf = fopen(WB_CONFIG_FILE, "r");
  if(pf == NULL)
    return CS_ERROR;
  return CS_OK;
}

CONFIG_STATE wb_config_read(const char *item, char *result, int max_size)
{
  char *p;
  char line[50];
  if(pf == NULL) 
    return CS_ERROR;

  memset(result, 0, max_size);
  while(fgets(line, sizeof line, pf))
    {
      line[strlen(line)-1] = 0;
      if(line[0] != '#')
	{ // rechercher de l'item dans la ligne
	  if(strstr(line, item))
	    { // recherche du = dans la ligne
	      p = strstr(line, "=");
	      if(p)
		{
		  if(strlen(p) > 1) p++;
		  if((int)strlen(p) > max_size)
		    strncpy(result, p, max_size);
		  else
		    strncpy(result, p, strlen(p));

		  fseek (pf , 0 , SEEK_END);
		  rewind (pf);
		  return CS_OK;
		}
	    }
	}
    }
  fseek (pf , 0 , SEEK_END);
  rewind (pf);
  return CS_ERROR;
}

void wb_config_close(void)
{
  if(pf != NULL)
    fclose(pf);
}

