#ifndef H_WB_LOG
#define H_WB_LOG

/**
 * d�finit le niveau d'affichage des logs
 * @param niveau d'importance du log
 */
void wb_log_verbosity_set_level(int level);


/**
 * ajout un log au syst�me via syslog et sur sortie erreur
 * @param level_msg niveau de sortie du message
 * @param format, m�me utilisation que le printf (%d %f ...)
 */
void wb_log_print(char level_msg, const char *format, ...);

#endif

