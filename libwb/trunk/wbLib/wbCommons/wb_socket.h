#ifndef H_WB_SOCKET
#define H_WB_SOCKET

///< d�finition de pointeur de fonction pour le serveur
typedef int (*PtrFunc)(char*, char*);

/**
 * �num�ration des �tats de retour des fonctions 
 */
typedef enum
  {
    SOCKET_OK,
    SOCKET_INIT_ERROR,
    SOCKET_SND_ERROR,
    SOCKET_RCV_ERROR,
    SOCKET_CNX_ERROR
  } SOCKET_STATE;

/**
 * r�ception de donn�e sur la socket
 * @param sock port ouvert
 * @param data pointeur o� seront stock�s les donn�es lues
 * @param max_len longueur maximum qui peut �tre stock� dans le buffer
 * @return retourne l'�tat de la commande
 */
SOCKET_STATE wb_socket_receive(int socket, char *data, int max_len);

/**
 * envoi de donn�es sur la socket 
 * @param sock port ouvert
 * @param data donn�e � envoyer
 * @param len longueur de donn�es � �crire
 */
SOCKET_STATE wb_socket_send(int socket, char *data, int len);

/**
 * lance un serveur sur un port
 * @param port � ouvrir
 * @param f pointeur de fonction � lancer lorsqu'un client se connecte
 */
SOCKET_STATE wb_socket_start_server(int port, PtrFunc f);

/**
 * se connecte � un serveur tcp
 * @param address adresse du serveur
 * @param socket port du serveur
 * @return �tat de la connection
 */
SOCKET_STATE wb_socket_connect(const char *address, int port, int *handle);

/**
 * stop le client ou le serveur
 * @param handle en cas de connection handle client
 */
void wb_socket_stop(int *handle);

#endif

