#include <stdio.h>
#include "wb_core.h"
#include "wb_i2c.h"
#include "wb_log.h"

int wb_core_init_hardware(char * device)
{
#ifdef FAKEHW
  wb_log_print(0, "*** Warning: fake hardware !\n");
  return 1;
#else
  // init du bus i2c
  if(wb_init_i2c(device) != I2C_OK)
    {
      wb_log_print(0, "Erreur initialisation du bus i2c!\n");
      return 0;
    }
  return 1;
#endif
}

I2C_STATE wb_core_apply(wb_command wb)
{
  unsigned char buf_speed[2];

  wb.control_mode = 0; // TODO XXX Gniii ?
 #ifndef FAKEHW
  int i, j;
  for(i=LEFT;i<=RIGHT;i++)
    {
      for(j=FRONT;j<=REAR;j++)
	{ 
	  buf_speed[j] = 0;
	   if(wb.wheel_sens[i][j] == FORWARD)
	     {
	       if(wb.control_mode == 1)
	        buf_speed[j] =  wb.wheel_speed[i][j] | I2C_MOTOR_FORWARD | I2C_MOTOR_CTRL_ON;
	       else
		 buf_speed[j] =  wb.wheel_speed[i][j] | I2C_MOTOR_FORWARD | I2C_MOTOR_CTRL_OFF;
	     }
	   else
	     {
	       if(wb.control_mode == 1)
	        buf_speed[j] =  wb.wheel_speed[i][j] | I2C_MOTOR_BACKWARD | I2C_MOTOR_CTRL_ON;
	       else
		 buf_speed[j] =  wb.wheel_speed[i][j] | I2C_MOTOR_BACKWARD | I2C_MOTOR_CTRL_OFF;
	     }
	}
      if(i == LEFT)
	{
	  if(wb_write_i2c(I2C_ADR_MOTOR_LEFT, buf_speed, 2) != I2C_OK)
	    return I2C_WRITE_ERROR;
	}
      else
	{
	  if(wb_write_i2c(I2C_ADR_MOTOR_RIGHT, buf_speed, 2) != I2C_OK)
	    return I2C_WRITE_ERROR;
	}
    }
#endif
  return I2C_OK;
}


I2C_STATE wb_core_get_infos(wb_info *wb)
{  
  unsigned char buf[3] = {0,0,0};
  I2C_STATE state;
  unsigned char buf_bat_request[2] = {0x40, 250};

#ifdef FAKEHW
  wb->wheel_speed[LEFT][FRONT]=0;
  wb->wheel_sens[LEFT][FRONT] = FORWARD;
  wb->wheel_speed[LEFT][REAR]=0;
  wb->wheel_sens[LEFT][REAR] = FORWARD;
  wb->wheel_speed[RIGHT][FRONT]=0;
  wb->wheel_sens[RIGHT][FRONT] = FORWARD;
  wb->wheel_speed[RIGHT][REAR]=0;
  wb->wheel_sens[RIGHT][REAR] = FORWARD;
  wb->control_mode=1;
  wb->ir_state[0]=17;
  wb->ir_state[1]=27;
  wb->battery_level=42;
  return I2C_OK;
#else

  // left side
  if(wb_read_i2c(I2C_ADR_MOTOR_LEFT, buf, 3) != I2C_OK)
    return  I2C_READ_ERROR;  

  // roue avant gauche
  wb->wheel_speed[LEFT][FRONT] = buf[0];
  if(buf[0] & I2C_MOTOR_FORWARD)
    wb->wheel_sens[LEFT][FRONT] = FORWARD;
  else
    wb->wheel_sens[LEFT][FRONT] = BACKWARD;

  // pour le test d'asservissement on ne le fait qu'une fois
  // puisqu'il est appliqu� � toute les roues
  if(buf[0] & I2C_MOTOR_CTRL_ON)
    wb->control_mode = 1;
  else
    wb->control_mode = 0;

  // roue arri�re gauche
  wb->wheel_speed[LEFT][REAR] = buf[1];
  if(buf[0] & I2C_MOTOR_FORWARD)
    wb->wheel_sens[LEFT][REAR] = FORWARD;
  else
    wb->wheel_sens[LEFT][REAR] = BACKWARD;

  wb->ir_state[LEFT] = buf[2];

  // right side
  if((state = wb_read_i2c(I2C_ADR_MOTOR_RIGHT, buf, 3)) != I2C_OK)
    return state;  

  // roue avant droite
  wb->wheel_speed[RIGHT][FRONT] = buf[0];
  if(buf[0] & I2C_MOTOR_FORWARD)
    wb->wheel_sens[RIGHT][FRONT] = FORWARD;
  else
    wb->wheel_sens[RIGHT][FRONT] = BACKWARD;

  // roue avant gauche
  wb->wheel_speed[RIGHT][REAR] = buf[1];
  if(buf[0] & I2C_MOTOR_FORWARD)
    wb->wheel_sens[RIGHT][REAR] = FORWARD;
  else
    wb->wheel_sens[RIGHT][REAR] = BACKWARD;
  wb->ir_state[RIGHT] = buf[2];

  // battery request
  state = wb_write_then_read_i2c(I2C_ADR_BATTERY, buf_bat_request, 2, &wb->battery_level, 1);
  if(state != I2C_OK)
    return state; 

  return I2C_OK;
#endif
}
