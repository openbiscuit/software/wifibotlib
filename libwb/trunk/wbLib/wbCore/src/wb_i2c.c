#include <stdio.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <sys/ioctl.h>

#include "wb_i2c.h"

int g_i2c_handle; ///< handle i2c

void bin( char temp )
{
  if(temp & 0x80) printf("1");
  else printf("0");
  if(temp & 0x40) printf("1");
  else printf("0");
  if(temp & 0x20) printf("1");
  else printf("0");
  if(temp & 0x10) printf("1");
  else printf("0");
  if(temp & 0x08) printf("1");
  else printf("0");
  if(temp & 0x04) printf("1");
  else printf("0");
  if(temp & 0x02) printf("1");
  else printf("0");
  if(temp & 0x01) printf("1");
  else printf("0");
  printf(" ");
}


I2C_STATE wb_init_i2c(char *bus_path)
{
  g_i2c_handle = open(bus_path, O_RDWR);
  if(g_i2c_handle < 0)
    return  I2C_INIT_ERROR;

  return I2C_OK;
}

I2C_STATE wb_write_i2c(unsigned char adr, unsigned char *value, char len)
{
  int i=0;
  for(i=0;i<len;i++)
    bin(value[i]);
  printf("\n");
  if(ioctl(g_i2c_handle, I2C_SLAVE, adr) < 0)
    return I2C_IO_ERROR;

  if(write(g_i2c_handle, value, len) != len)
    return I2C_WRITE_ERROR;

  return I2C_OK;
}

I2C_STATE wb_read_i2c(unsigned char adr, unsigned char *value, char len)
{
 if(ioctl(g_i2c_handle, I2C_SLAVE, adr) < 0)
    return I2C_IO_ERROR;

  if(read(g_i2c_handle, value, len) != len)
    return I2C_READ_ERROR;

  return I2C_OK;
}

I2C_STATE wb_write_then_read_i2c(unsigned char adr, unsigned char *str_write, char lenwrite,
				 unsigned char *str_read, char lenread)
{  
  if(ioctl(g_i2c_handle, I2C_SLAVE, adr) < 0)
    return I2C_IO_ERROR;

  if(write(g_i2c_handle, str_write, lenwrite) != lenwrite)
    return I2C_WRITE_ERROR;

  if(read(g_i2c_handle, str_read, lenread) != lenread)
    return I2C_READ_ERROR;

  return I2C_OK;
}
