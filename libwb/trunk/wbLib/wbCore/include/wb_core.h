#ifndef H_WB_CORE
#define H_WB_CORE

#include "wb_i2c.h"

typedef enum
  {
    LEFT,
    RIGHT
  } wb_wheel_sens;

typedef enum
  {
    FRONT,
    REAR
  } wb_wheel_pos;

typedef enum
  {
    FORWARD,
    BACKWARD
  } wb_sens;

#define MOVE_MAX_SPEED 63 ///< vitesse maximum des moteurs autorisés

typedef struct
{
  unsigned char battery_level; ///< niveau de la batterie
  unsigned char wheel_speed[2][2]; ///< vitesse des quatres roues
  unsigned char wheel_sens[2][2]; ///< sens des roues 
  unsigned char ir_state[2]; ///< �tat des infrarouges
  unsigned char control_mode;///< 1 mode activ�, 0 d�sactiv�
} wb_info;


typedef struct
{
  unsigned char wheel_speed[2][2]; ///< vitesse des quatres roues
  unsigned char wheel_sens[2][2]; ///< sens des roues (I2C_MOTOR_FORWARD ou I2C_MOTOR_BACKWARD)
  unsigned char control_mode;
} wb_command;

/**
 * initialise la wifibot : le bus i2c
 * @return 0 si tout s'est bien pass�
 * @return 1 en cas de probl�me
 */
int wb_core_init_hardware(char * i2c_device);

/**
 * affecte les vitesse et l'asservissement au wifibot
 * @param wb contient les informations de vitesse
 * @return un �tat i2c
 */
I2C_STATE wb_core_apply(wb_command wb);

/**
 * r�cup�re les informations sur le wifibot
 * @param wb structure pour r�cup�rer les infors
 * @return un �tat i2c
 */
I2C_STATE wb_core_get_infos(wb_info *wb);

#endif

