#ifndef H_WB_I2C
#define H_WB_I2C

/**
 * �num�ration des �tats de retours possible des fonctions i2c
 */
typedef enum 
  {
    I2C_OK,
    I2C_INIT_ERROR,
    I2C_ADR_ERROR,
    I2C_IO_ERROR,
    I2C_WRITE_ERROR,
    I2C_READ_ERROR
  } I2C_STATE;

#define	I2C_ADR_MOTOR_LEFT  0x51 ///< adresse des moteurs � gauche
#define	I2C_ADR_MOTOR_RIGHT 0x52 ///< adresse des moteurs � droite
#define	I2C_ADR_BATTERY     0x4e ///< adresse de la batterie


#define I2C_MOTOR_CTRL_ON   0x80 ///< asservissement on
#define I2C_MOTOR_CTRL_OFF  0x00 ///< asservissement off
#define I2C_MOTOR_FORWARD   0x40 ///< en avant
#define I2C_MOTOR_BACKWARD  0x00 ///< marche arriere

/**
 * Initialise le bus i2c
 * @param bus_path chemin du bus i2c
 * @return un �tat i2c
 */
I2C_STATE wb_init_i2c(char *bus_path);

/**
 * �crit des octets sur le bus i2c � destination d'un adresse
 * @param adr adresse de destination
 * @param value valeur des octets � �crire
 * @param len longueur de la cha�ne � �crire
 * @return un �tat i2c
 */
I2C_STATE wb_write_i2c(unsigned char adr, unsigned char *value, char len);

/**
 * Lit des octets sur le bus i2c
 * @param adr adresse de lecture de l'octet
 * @param value sera affecter de la valeur lue sur le bus
 * @param len longueur � lire
 * @return un �tat i2c
 */
I2C_STATE wb_read_i2c(unsigned char adr, unsigned char *value, char len);

/**
 * �crit des octets sur le bus puis lit les octets de reponses
 * @param adr adresse d'ecriture
 * @param towrite valeur a ecrire
 * @param lenwrite nombre d'octet a ecrire
 * @param toread pointeur sur la valeur lue
 * @param lenread nombre d'octet a lire
 * @return un �tat i2c
 */
I2C_STATE wb_write_then_read_i2c(unsigned char adr,  unsigned char *str_write, char lenwrite,
				 unsigned char *str_read, char lenread);

#endif

