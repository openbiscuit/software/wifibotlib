#ifndef H_WB_COMMAND
#define H_WB_COMMAND

typedef enum
{
  WB_COMMAND_OK,
  WB_COMMAND_ERROR
} WB_COMMAND;


////////////////
// HIGHT LEVEL API
///////////////

/**
 * commande avancer
 * @param speed_left vitesse cot� gauche
 * @param speed_right vitesse cot� droit
 * @return �tat d'ex�cution de la commande
 */
WB_COMMAND wb_command_forward(char speed_left,
			      char speed_right);
/**
 * commande reculer
 * @param speed_left vitesse cot� gauche
 * @param speed_right vitesse cot� droit
 * @return �tat d'ex�cution de la commande
 */
WB_COMMAND wb_command_reverse(char speed_left,
			      char speed_right);

/**
 * commande tourner
 * @param sens 0 tourner � gauche, 1 tourner � droite
 * @param speed_left vitesse cot� gauche
 * @param speed_right vitesse cot� droit
 * @return �tat d'ex�cution de la commande
 */
WB_COMMAND wb_command_turn(char sens,
			   char speed_left,
			   char speed_right);

/**
 * stop tous les mouvements du wifibot
 * @return �tat d'ex�cution de la commande
 */
WB_COMMAND wb_command_stop(void);

/**
 * retourne l'�tat de charge de la batterie
 * @param value valeur de retour de l'�tat de charge
 * @return �tat d'ex�cution de la commande
 */
WB_COMMAND wb_command_get_battery_level(char *value);

/**
 * retourne l'�tat d'un capteur du wifibot
 * @param side quel cot� 0 pour la gauche, 1 pour la droite
 * @param value valeur lue sur le capteur
 * @return �tat d'�xecution de la commande
 */
WB_COMMAND wb_command_get_status_sensor(char side,
					char *value);

/**
 * d�finit le type d'asservissement
 * @param enable 1 pour activer l'asservissement 0 pour l'inverse
 * @return �tat d'�xecution de la commande
 */
WB_COMMAND wb_command_set_control_mode(char enable);

#endif

