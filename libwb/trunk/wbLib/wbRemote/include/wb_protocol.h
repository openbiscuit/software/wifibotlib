#ifndef WB_PROTOCOL_H
#define WB_PROTOCOL_H

/**
 * recoit une ccommande, envoi cette commande � wbCore et retourne la r�ponse
 * @param rcv contenu de la commande
 * @param snd r�ponse de l'execution de la commande
 * @return 0 si tout s'est bien pass�
 * @return 1 en cas d'erreur
 */
int wb_protocol_apply_command(char *rcv, char *snd);

#endif
