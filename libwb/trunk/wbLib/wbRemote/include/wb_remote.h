#ifndef H_WB_REMOTE
#define H_WB_REMOTE


/**
 * Initialise la partie remote
 * @return 0 si l'initialisation s'est bien pass�
 * @return 1 si un probl�me est rencontr�
 */
int wb_remote_init(void);

/**
 * serveur tcp, transmet les commandes vers wb core
 */
void wb_remote_run(void);

#endif
