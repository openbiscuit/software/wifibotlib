#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "wb_log.h"
#include "wb_command.h"
#include "wb_core.h"

// we need to keep this one for control_mode (and possibly speed*)
// TODO: initialize !
static wb_command cmd;

WB_COMMAND wb_command_wheel(char dir_l,char speed_lf,char speed_lr,char dir_r, char speed_rf, char speed_rr)
{
    if (dir_l==0)
	{
	  cmd.wheel_sens[LEFT][FRONT] = FORWARD;
	  cmd.wheel_sens[LEFT][REAR] = FORWARD;
	}
      else
	{
	  cmd.wheel_sens[LEFT][FRONT] = BACKWARD;
	  cmd.wheel_sens[LEFT][REAR] = BACKWARD;
	}

      cmd.wheel_speed[LEFT][FRONT]=speed_lf;
      cmd.wheel_speed[LEFT][REAR] =speed_lr;

      if(dir_r == 0)
	{
	  cmd.wheel_sens[RIGHT][FRONT] = FORWARD;
	  cmd.wheel_sens[RIGHT][REAR] = FORWARD;
	}
      else
	{
	  cmd.wheel_sens[RIGHT][FRONT] = BACKWARD;
	  cmd.wheel_sens[RIGHT][REAR] = BACKWARD;
	}
      cmd.wheel_speed[RIGHT][FRONT] = speed_rf;
      cmd.wheel_speed[RIGHT][REAR] = speed_rr;

      if(wb_core_apply(cmd) == I2C_OK) 
        return WB_COMMAND_OK;
      else
	return WB_COMMAND_ERROR;
}

WB_COMMAND wb_command_forward(char speed_left,char speed_right)
{
  return wb_command_wheel(0,speed_left,speed_left,0,speed_right,speed_right);
}

WB_COMMAND wb_command_reverse(char speed_left,char speed_right)
{
  return wb_command_wheel(1,speed_left,speed_left,1,speed_right,speed_right);
}

WB_COMMAND wb_command_turn(char sens,
			   char speed_left,
			   char speed_right)
{
  if(sens == 0)
    return wb_command_wheel(1,speed_left,speed_left,0,speed_right,speed_right);
  else
    return wb_command_wheel(0,speed_left,speed_left,1,speed_right,speed_right);
}

WB_COMMAND wb_command_stop(void)
{
    cmd.wheel_speed[LEFT][FRONT] = 0;
    cmd.wheel_speed[LEFT][REAR] = 0;
    cmd.wheel_speed[RIGHT][FRONT] = 0;
    cmd.wheel_speed[RIGHT][REAR] = 0;
    wb_core_apply(cmd);
    if(wb_core_apply(cmd) == I2C_OK)
        return WB_COMMAND_OK;
    else
        return WB_COMMAND_ERROR;
}

WB_COMMAND wb_command_get_battery_level(char *value)
{
  // TODO: should we check for wb_core_get_infos() success ?
  wb_info info;
  wb_core_get_infos(&info);
  *value=info.battery_level;
  return WB_COMMAND_OK;
}

WB_COMMAND wb_command_get_status_sensor(char side,char *value)
{
  wb_info info;
  wb_core_get_infos(&info);
  *value=info.ir_state[side];
  return WB_COMMAND_OK;
}

WB_COMMAND wb_command_set_control_mode(char enable)
{
  cmd.control_mode = enable;
  return WB_COMMAND_OK;
}
