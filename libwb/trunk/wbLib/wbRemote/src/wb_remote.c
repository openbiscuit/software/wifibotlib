#include <stdlib.h>
#include <string.h>
#include "wb_config.h"
#include "wb_log.h"
#include "wb_remote.h"
#include "wb_socket.h"
#include "wb_protocol.h"
#include "wb_core.h"

int g_server_port = -1;

int wb_remote_init(void)
{
  char sock[5];
  memset(sock, 0, 5);

  if(wb_config_init() == CS_ERROR)
    {
      wb_log_print(0, "Erreur pas de fichier de configuration !\n");
      return 0;
    }

  char i2c_path[40];
  memset(i2c_path, 0, 40);
  if(wb_config_read("i2c_path", i2c_path, 40) == CS_ERROR)
    {
      wb_log_print(0, "Erreur i2c fichier de configuration !\n");
      return 0;
    }

  if(wb_core_init_hardware(i2c_path) == 0)
    {
      wb_log_print(0, "Erreur init core hardware !\n");
      return 0;
    }

  if(wb_config_read("server_port", sock, 5) == CS_ERROR)
    {
      wb_log_print(0, "Erreur sock fichier de configuration !\n");
      return 0;
    }
  g_server_port = atoi(sock);
  wb_config_close();
  return 1;
}

void wb_remote_run(void)
{
  if(g_server_port == -1)
    return ;
  wb_socket_start_server(g_server_port, &wb_protocol_apply_command);
}
