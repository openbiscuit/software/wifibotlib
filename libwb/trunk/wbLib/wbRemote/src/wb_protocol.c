#include <unistd.h>
#include <stdio.h>
#include "wb_log.h"
#include "wb_command.h"
#include "wb_protocol.h"
#include "wb_cmd.h"

int wb_protocol_apply_command(char *rcv, char *snd)
{
  char value = 0;
  char timeout = 0;
  int i;

  printf("recu %c ", rcv[0]);
  for(i=1;i<7;i++)
    printf("%d ", rcv[i]);
  printf("\n");
  fflush(stdout);

  snd[0] = rcv[0];

  switch(rcv[0])
    {
    case WB_PROT_FORWARD:
      snd[1] = (char)wb_command_forward(rcv[1], rcv[2]);
      timeout = rcv[3];
      break;
    case WB_PROT_BACKWARD:
      snd[1] = (char)wb_command_reverse(rcv[1], rcv[2]);
      timeout = rcv[3];
      break;
    case WB_PROT_TURN:
      snd[1] = (char)wb_command_turn(rcv[1], rcv[2], rcv[2]);
      timeout = rcv[3];
      break;
    case WB_PROT_STOP:
      snd[1] = (char)wb_command_stop();
      break;
    case WB_PROT_GET_BATTERY:
      snd[1] = (char)wb_command_get_battery_level(&value);
      snd[2] = value;
      break;
    case WB_PROT_CONTROL:
      snd[1] = (char)wb_command_set_control_mode(rcv[1]);
      break;
    case WB_PROT_GET_SENSORS:
      snd[1] = (char)wb_command_get_status_sensor(rcv[1], &value);
      snd[2] = value;
      break;
    default:
      return 1;
    }
  printf("SEND %c ", snd[0]);
  for(i=1;i<7;i++)
    printf("%d ", snd[i]);
  printf("\n");
  fflush(stdout);

  if(timeout != 0)
    {
      sleep(timeout);
      wb_command_stop();
    }
  return 0;
}

