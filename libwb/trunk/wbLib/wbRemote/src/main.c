#include <stdlib.h>
#include "wb_log.h"
#include "wb_remote.h"
#include "wb_command.h"
#include "wb_protocol.h"


int main(void)
{
   if(wb_remote_init() == 0)
     {
       wb_log_print(0, "Erreur initialisation de wb remote !\n");
       exit(0);
     }
 
   wb_remote_run();
  return 0;
}
