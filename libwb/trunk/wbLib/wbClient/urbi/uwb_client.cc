#include <urbi/uobject.hh>
#include <wb_client.h>

class uWbClient: public urbi::UObject
{
  public:
    // Urbi API
    uWbClient(const std::string &name) : urbi::UObject(name) { UBindFunction(uWbClient,init);};
  protected:
    int init(const std::string &host,int port);
    urbi::UVar irL;
    urbi::UVar irR;
    urbi::UVar battery;
    urbi::UVar control_mode;
    int forward(int,int);
    int backward(int,int);
    int stop(void);
    int turn(int,int);
    WbClient wb;
    int control_mode_changed(urbi::UVar &);    
    int ir_accessed(urbi::UVar &);
    int battery_accessed(urbi::UVar &);
};

UStart(uWbClient);

int uWbClient::init(const std::string &host,int port)
{
    if (!wb.connect(host.c_str(),port))
        return 1;

    UBindVar(uWbClient, irL);
    UNotifyAccess(irL,&uWbClient::ir_accessed);
    UBindVar(uWbClient, irR);
    UNotifyAccess(irR,&uWbClient::ir_accessed);
    UBindVar(uWbClient, battery);
    UNotifyAccess(battery,&uWbClient::battery_accessed);

    UBindVar(uWbClient, control_mode);
    UNotifyChange(control_mode,&uWbClient::control_mode_changed);

    UBindFunction(uWbClient,forward);
    UBindFunction(uWbClient,backward);
    UBindFunction(uWbClient,stop);
    UBindFunction(uWbClient,turn);
    return 0;
}

int uWbClient::forward(int speedleft,int speedright)
{
    return wb.forward(speedleft, speedright,0);
}

int uWbClient::backward(int speedleft,int speedright)
{
    return wb.backward(speedleft, speedright,0);
}

int uWbClient::stop(void)
{
    return wb.stop();
}

int uWbClient::turn(int d,int speed)
{
    return wb.turn(d,speed,0);
}

int uWbClient::control_mode_changed(urbi::UVar &)
{
    return wb.set_control_mode(control_mode);
}

int uWbClient::ir_accessed(urbi::UVar &)
{
    int left,right;
    int r=wb.get_sensors(&left,&right);
    if (!r)
        return 0;
    irL=left;
    irR=right;
    return 1;
}

int uWbClient::battery_accessed(urbi::UVar &)
{
    int bat;
    int r=wb.get_battery(&bat);
    if (!r)
        return 0;
    battery=bat;
    return 1;
}
