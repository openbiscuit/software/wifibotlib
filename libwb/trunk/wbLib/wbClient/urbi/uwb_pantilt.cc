#include <urbi/uobject.hh>
#include <wb_pantilt.h>

class uWbPanTilt: public urbi::UObject
{
  public:
    // Urbi API
    uWbPanTilt(const std::string &name) : urbi::UObject(name) { UBindFunction(uWbPanTilt,init);};
  protected:
    int init(const std::string &host,int port);
    urbi::UVar image;
    WbPanTilt wb;
    int image_accessed(urbi::UVar &);
};

UStart(uWbPanTilt);

int uWbPanTilt::init(const std::string &host,int port)
{
    if (!wb.connect(host.c_str(),port))
        return 1;

    UBindVar(uWbPanTilt, image);
    UNotifyAccess(image,&uWbPanTilt::image_accessed);
    return 0;
}

int uWbPanTilt::image_accessed(urbi::UVar &)
{
    urbi::UBinary b;
    b.type=urbi::BINARY_IMAGE;
    b.image.imageFormat= urbi::IMAGE_JPEG;
    b.image.width= 0;
    b.image.height= 0;
    b.image.data=new unsigned char[640*480*3];
    b.image.size=wb.get_image(b.image.data);
    // TODO: set width & height (use jpeg lib ?)
    image=b;
    return 1;
}
