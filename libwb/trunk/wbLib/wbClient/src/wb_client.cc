#include <unistd.h>
#include <stdio.h>
#include <algorithm>

#include "wb_socket.h"
#include "wb_cmd.h"
#include "wb_client.h"

WbClient::WbClient(): sock(-1)
{
    std::fill_n(snd,7,0); 
    std::fill_n(rcv,7,0); 
}

int WbClient::connect(const char * ip,int port)
{
    if(wb_socket_connect(ip, port, &sock) != SOCKET_OK)
    {
      printf("Impossible de se connecter !\n");
      return 0;
    }
    if(sock == -1)
    {
      printf("Erreur de connexion au serveur, verifier la configuration !\n");
      return 0;
    }
    return 1;
}

int WbClient::send_and_receive(void)
{
  wb_socket_send(sock,snd,7);
  wb_socket_receive(sock,rcv,7);
  return 1;
}

int WbClient::forward(int left, int right, int timeout)
{
  snd[0]=WB_PROT_FORWARD;
  snd[1]=left;
  snd[2]=right;
  snd[3]=timeout;
  return send_and_receive();
}

int WbClient::backward(int left, int right, int timeout)
{
  snd[0]=WB_PROT_BACKWARD;
  snd[1]=left;
  snd[2]=right;
  snd[3]=timeout;
  return send_and_receive();
}

int WbClient::turn(int direction, int speed, int timeout)
{
  snd[0]=WB_PROT_TURN;
  snd[1]=direction;
  snd[2]=speed;
  snd[3]=timeout;
  return send_and_receive();
}

int WbClient::stop(void)
{
    snd[0]=WB_PROT_STOP;
    return send_and_receive();
}
    
int WbClient::get_battery(int *bat)
{
    snd[0]=WB_PROT_GET_BATTERY;
    if (send_and_receive())
    {
        *bat=(unsigned char)rcv[2];
    }
    else return 0;
    return 1;
}

int WbClient::set_control_mode(int mode)
{
    snd[0]=WB_PROT_CONTROL;
    return send_and_receive();
}

int WbClient::get_sensors(int *ir_left,int *ir_right)
{
// TODO: get_sensors should return BOTH sensors
// add a get_sensor if needed...
    snd[0]=WB_PROT_GET_SENSORS;
    snd[1]=0;
    if (send_and_receive())
    {
        *ir_left=(unsigned char)rcv[2];
    }
    else return 0;
    snd[0]=WB_PROT_GET_SENSORS;
    snd[1]=1;
    if (send_and_receive())
    {
        *ir_right=(unsigned char)rcv[2];
    }
    else return 0;
    return 1;
}
