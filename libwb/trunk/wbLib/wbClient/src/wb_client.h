#ifndef WBCLIENT_H
#define WBClIENT_H

class WbClient
{
  public:
    WbClient();
    /* connect to a wbRemote server at ip:port */
    int connect(const char * ip,int port);
    /* move forward, possibly with different left and right speed.
       timeout=0: move forever; timeout>0: move until timeout, then stop. */
    int forward(int left, int right, int timeout);
    /* same as forward, but backward :-)  */
    int backward(int left, int right, int timeout);
    /* turn in place. Timeout: see above. */
    int turn(int direction, int speed, int timeout);

    /* stop both motors */
    int stop(void);
    /* set control mode: 0=open loop (no control), 1=use PID*/
    int set_control_mode(int mode);

    /* get various sensors */
    int get_battery(int *bat);
    int get_sensors(int *ir_left,int *ir_right);
  protected:
    int send_and_receive(void);
    int sock;    // -1: uninitialized; otherwise: socket fd
    char snd[7]; // send and receive buffers
    char rcv[7];
};

#endif
