#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "wb_client.h"

int main(int argc, char ** argv)
{
    if (argc!=3)
    {
        printf(" ***  usage: test_client1 ip port\n");
        exit(EXIT_FAILURE);
    }
    WbClient wb;
    wb.connect(argv[1],atoi(argv[2]));
    wb.forward(10,10,1);
    wb.backward(10,10,1);
    wb.stop();
    int bat;
    wb.get_battery(&bat);
    printf("Battery: %d\n",bat);
    int irl,irr;
    wb.get_sensors(&irl,&irr);
    printf("IR Left: %d\n",irl);
    printf("IR Right: %d\n",irr);
}
