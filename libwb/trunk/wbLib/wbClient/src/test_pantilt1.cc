#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include "wb_pantilt.h"
using namespace std;

int main(int argc, char ** argv)
{
    if (argc!=3)
    {
        printf(" ***  usage: test_pantilt1 ip port\n");
        exit(EXIT_FAILURE);
    }
    WbPanTilt wb;
    wb.connect(argv[1],atoi(argv[2]));
    wb.move_camera_to(40,100);
    sleep(1);
    wb.move_camera_to(46,120);
    sleep(1);
    unsigned char buffer[640*480*3];
    int s=wb.get_image(buffer);
    cout << "got image, size " << s << endl;
    return EXIT_SUCCESS;
}
