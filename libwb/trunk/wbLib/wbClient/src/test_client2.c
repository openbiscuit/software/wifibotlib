#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>	
#include <ctype.h>
#include <memory.h>
#include "wb_config.h"
#include "wb_log.h"
#include "wb_socket.h"
#include "wb_cmd.h"

typedef struct
{
  char txt[40];
  int min;
  int max;
} arg_def;

typedef struct
{
  int id;
  char cmd;
  int nb_arg;
  arg_def args[6];
} command_def;

#define NB_CMD 9
command_def cmds[] = {\
  {1, WB_PROT_FORWARD, 3, {{"Vitesse gauche ? [1:66]", 1, 66}, {"Vitesse droite [1:66] ?", 1, 66}, {"Temps s ?", 0, 10}}}, \
  {2, WB_PROT_BACKWARD, 3, {{"Vitesse gauche ? [1:66]", 1, 66}, {"Vitesse droite [1:66] ?", 1, 66}, {"Temps s ?", 0, 10}}}, \
  {3, WB_PROT_TURN, 3, {{"Gauche(0) ou Droite(1) ?", 0, 1}, {"Vitesse ? [1:66]", 1, 66}, {"Temps s ?", 0, 10}}}, \
  {4, WB_PROT_STOP, 0, {{"",0,0}}},		\
  {5, WB_PROT_GET_BATTERY, 0, {{"",0,0}}},	\
  {6, WB_PROT_CONTROL, 1, {{"Active asservissement (1:oui, 0:non)", 0, 1}}}, \
  {7, WB_PROT_GET_SENSORS,  1, {{"Gauche(0) ou Droite(1) ?", 0, 1}}}
};


int menu(void);
void execute(int m, int sock);
int input(int min, int max);

int main()
{
  char ip[15];
  char sock[6];
  int sock_cli = -1;
  unsigned char menu_item = -1;

  if(wb_config_init() == CS_ERROR)
    {
      wb_log_print(0, "Erreur pas de fichier de configuration !\n");
      return 0;
    }
  if(wb_config_read("wifibot_ip", ip, 15) == CS_ERROR)
    {
      wb_log_print(0, "Erreur i2c fichier de configuration !\n");
      return 0;
    }
  if(wb_config_read("wifibot_port", sock, 6) == CS_ERROR)
    {
      wb_log_print(0, "Erreur sock fichier de configuration !\n");
      return 0;
    }
  wb_log_print(0, "Connexion au serveur %s, %s !\n", ip, sock);

  if(wb_socket_connect(ip, atoi(sock), &sock_cli) != SOCKET_OK)
    {
      wb_log_print(0, "Impossible de se connecter !\n");
      return 0;      
    }
  if(sock_cli == -1)
    {
       wb_log_print(0, "Erreur de connexion au serveur, verifier la configuration !");
      return 0;
    }
  do 
    {
      menu_item = menu();

      if(menu_item > 1) execute(menu_item, sock_cli);
      usleep(1);
    } while(menu_item > 1);

  wb_socket_stop(&sock_cli);
  return 0;
}

void execute(int m, int sock)
{
  char send[7];
  char rcv[7];
  int i = 0;

  if(m < 2 || m > NB_CMD+2) return ;
  // recupere la structure associee � la commande
  command_def *c = &cmds[m-2];
  memset(send, 0, 7);
  memset(rcv, 0, 7);

  // affiche le nom de la commande
  printf("Commande : %c\n", c->cmd);
  send[i] = c->cmd;
  // recupere les parametres
  while(i < c->nb_arg)
    {
      printf("%d %s\n", i, c->args[i].txt);
      send[i+1] = input(c->args[i].min, c->args[i].max);
      i++;
    }
  // affiche le tableau qui va etre envoye
  printf("%c", send[0]);
  for(i=1;i<7;i++)
    printf(" %d ", send[i]);
  printf("\n");
	
  wb_socket_send(sock, send, 7);
  wb_socket_receive(sock, rcv, 7);

  // affiche la reponse
  printf("recu %c ", rcv[0]);
  for(i=1;i<7;i++)
    printf("%d ", rcv[i]);
  printf("\n");
}

int menu(void)
{
  const char *menu = {"[1] = quitter\n"			\
		      "[2] = avancer\n"			\
		      "[3] = reculer\n"			\
		      "[4] = tourner\n"			\
		      "[5] = stop\n"			\
		      "[6] = batterie\n"		\
		      "[7] = asservissement\n"		\
		      "[8] = capteurs\n"		\
		      "\n"				\
		      "Quelle action ?\n" };
  printf("%s", menu);
  return input(1, NB_CMD);
}

int input(int min, int max)
{
  char enter[10];
  int indice = 0;
  char c;
  int rep = 0;
  do
    {
      memset(enter, 0, 10);
      indice = 0;
      while ((c = getchar ()) != '\n')
	{
	  if(c >= '0' && c <= '9' && indice < 10)
	    {
	      enter[indice++] = c;
	    }
	}
    
      if(indice > 0) rep = atoi(enter);

    } while(rep < min || rep > max);

  return rep;
}
