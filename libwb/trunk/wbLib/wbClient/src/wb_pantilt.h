/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Cédric Bernier, Julien Le Guen, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Cédric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*/

#ifndef WB_PANTILT_H
#define WB_PANTILT_H
#include <iostream>
/* Enumeration des directions du mouvement de la camera*/
typedef enum {
  TopLeft,
  Top,
  TopRight,
  Left,
  Home,
  Right,
  BottomLeft,
  Bottom,
  BottomRight,
  WBSens_Unknown
} WBSens;

class WbPanTilt
{
protected:
    char * host;
    int port;
    int init_sock();
public:
    /* Procédure permettant de contrôler la caméra.
    * pan: le mouvement vertical 
    * tilt: le mouvement horizontal
    * sens: la direction du mouvement 
    */
    WbPanTilt() {}
    int connect(const char *host, int ip);
    int move_camera(int pan, int tilt, WBSens sens);
    int move_camera_no_sleep(int pan, int tilt, WBSens sens);

    /**
    * @param angle_horiz wanted horizontal angle of the camera (home is 158)
    * @param angle_ver wanted vertical angle of the camera 
    * (home is 46 (120 with unmodified coordinate, don't ask me...)) 
    */
    int move_camera_to(int angle_horiz, int angle_vert);

    int get_image(unsigned char *buffer);
};

#endif
