/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Cédric Bernier, Julien Le Guen, Alain Dutech
**			Maia Team, LORIA.
**
**               2008 Jerome Bechu, Olivier Rochel
** Original authors: Cédric Bernier, Julien Le Guen
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/
/*
 * Routines pour controler la caméra du wifibot
 *
 * La caméra du robot embarque un serveur Web exécutant des scripts Java et CGI,
 * mettant à disposition un flux vidéo et Jpeg.
 * Pour piloter la caméra, on dispose d'un applet java. On peut aussi utiliser les routines
 * ci-dessous pour la contrôler en embarqué, depuis le robot.
 *
 * Pour la contrôler il faut envoyer une requete POST au script /PANTILTCONTROL.CGI
 * de ce type: 
 *	POST /PANTILTCONTROL.CGI HTTP/1.0
 *	User-Agent: user
 *	Authorization: Basic
 *	Content-Type: application/x-www-form-urlencoded
 *	Content-Length: 66
 *
 *	PanSingleMoveDegree=10&TiltSingleMoveDegree=10&PanTiltSingleMove=6
 *
 * Cela demande à la caméra de bouger de 10 degrés en PAN et TILT, selon la direction 6.
 * Tableau des directions :
 * 0 1 2
 * 3 4 5
 * 6 7 8
 * La direction 4 repositionne la caméra en position initiale.
 *
 * Pour faire bouger la caméra il suffit alors d'ouvrir une socket sur le port 80 de la caméra,
 * de construire une requete POST correcte et d'envoyer tout ça au script CGI.
 * 
 * 
 */

/**
 * @file   wb_pantilt.c
 *
 * @brief Functions for controling the movements of the camera.
 *
 * @author Cédric Barnier
 * @author Alain Dutech
 * @author Julien LeGuen
 * @date   Fri Apr 20 12:05:56 2007
 *
 * <b>French comments can be found in the source file</b>.<br>
 * inside the robot's camera there is a web server that executes JAVA and
 * CGI scripts. A video flow and JPEG images are also available.
 *
 * To control the camera, a POST request must be send to the /PANTILTCONTROL.CGI cgi script, with the following info
 *  - POST  /PANTILTCONTROL.CGI HTTP/1.0
 *  - User-Agent: user
 *  - Authorization: Basic
 *  - Content-Type: application/x-www-form-urlencoded
 *  - Content-Length: 65
 *
 *  - PanSingleoveDegree=10&TiltSingleMoveDegree=10&PanTiltSingleMove=6
 *
 * This asks the camera to move 10 degrees (PAN and TILT) in the direction number 6.
 * Directions are:
 *  - 0 1 2
 *  - 3 4 5
 *  - 6 7 8
 * (These info are hidden in the WBSens enum type).
 */  

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "wb_socket.h"
#include "wb_pantilt.h"

#define BUFFERSIZE 512

int WbPanTilt::connect(const char * host,int port)
{
// TODO: check if already connected, destructor
    this->host=new char[strlen(host)+1];
    memcpy(this->host,host,strlen(host)+1);
    this->port=port;      
    printf("H %s %d %d\n",host,port,strlen(host));
    printf("H %s %d\n",this->host,this->port);
    return 1;
}

int WbPanTilt::init_sock(void)
{
    int sock;
    if(wb_socket_connect(host, port, &sock) != SOCKET_OK)
    {
      printf("Impossible de se connecter ! %s %d\n",host,port);
      return -1;
    }
    if(sock == -1)
    {
      printf("Erreur de connexion à la caméra, verifier la configuration !\n");
      return -1;
    }
    return sock;
}

/* Procédure permettant de contrôler la caméra.
 * pan: le mouvement vertical 
 * tilt: le mouvement horizontal
 * sens: la direction du mouvement 
 */
/**
 * Moves the camera, long.
 *
 * Opens a socket, sends order, waits and closes the socket after the move.
 *
 * @param pan vertical movement
 * @param tilt horizontal movement
 * @param sens direction of the movement (type WBSens)
 *
 */
int WbPanTilt::move_camera(int pan, int tilt, WBSens sens)
{
	char buffer[BUFFERSIZE];
	char buf[100];
	int taille, nb;
        int sock=init_sock();
	
	taille = snprintf(buf, 100,"PanSingleMoveDegree=%d&TiltSingleMoveDegree=%d&PanTiltSingleMove=%d", pan, tilt, sens);

	/* on construit la requete POST */
	nb = snprintf(buffer, BUFFERSIZE, "POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n", taille, buf);

	/* on envoie notre requete */
	send(sock, buffer, nb, 0);
	/* ON ATTEND QUE LE MESSAGE SOIT ENVOYE
	 * ET QUE LA CAM AIT REAGI */
	usleep(500000);
        // TODO: error checking, taille and nb checking,...
        close(sock);
        return 1;
}

/* Procédure permettant de contrôler la caméra.
 * pan: le mouvement vertical 
 * tilt: le mouvement horizontal
 * sens: la direction du mouvement 
 * N'EFFECTUE PAS L'ATTENTE APRES LE SEND
 * A N'UTILISER QUE SI LES TRAITEMENTS ENTRE DEUX APPELS
 * SONT SUFFISAMMENT LONGS
 * Retourne le numero de socket a fermer dans la routine appelante
 */
/**
 * Moves the camera, but does not wait until movement is done...
 * 
 * Opens a socket, send moves <b>BUT</b> does not wait afterwards.
 * The socket returned will have to be closed.
 *
 * @warning Should be used only if the time between two successive 
 * call is long enough.
 * 
 * @param pan vertical movement
 * @param tilt horizontal movement
 * @param sens direction of the movement (type WBSens)
 *
 * @return ?
 */
int WbPanTilt::move_camera_no_sleep(int pan, int tilt, WBSens sens)
{
	char buffer[BUFFERSIZE];
	char buf[100];
	int taille, nb;
	int sock=init_sock();
	taille = snprintf(buf,100,"PanSingleMoveDegree=%d&TiltSingleMoveDegree=%d&PanTiltSingleMove=%d", pan, tilt, sens);

	/* on construit la requete POST */
	nb = snprintf(buffer,BUFFERSIZE,"POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n", taille, buf);

	/* on envoie notre requete */
	send(sock, buffer, nb, 0);
// TODO: error checking, close sock ?
        return 1;
}

/**
 * Using a formula found in trunk/robosoft/SimpleGUI/video-client/Main.c line 91
 * @param angle_horiz wanted horizontal angle of the camera (home is 158)
 * @param angle_ver wanted vertical angle of the camera (home is 46 (120 with unmodified coordinate, don't ask me...)) 
 */
int WbPanTilt::move_camera_to(int angle_horiz, int angle_vert)
{
	int nb, taille;
	char buf[100];
	char buffer[BUFFERSIZE];
        int sock=init_sock();
	taille = snprintf(buf, 100,
                    "PanTiltHorizontal=%d&PanTiltVertical=%d&PanTiltPositionMove=true",
                    angle_horiz,
                    (int)((angle_vert*-0.75)+135));

	nb = snprintf(buffer, BUFFERSIZE,
                    "POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n",
                    taille,
                    buf);
	/* on envoie notre requete */
	send(sock, buffer, nb, 0);
	/* ON ATTEND QUE LE MESSAGE SOIT ENVOYE
	 * ET QUE LA CAM AIT REAGI */
	usleep(500000);
        // TODO: error checking
        close(sock);
        return 1;
}

/** 
 * Get an image from the camera (via internal socket).
 *
 * Uses socket sk defined by wbInitialiseSocket().
 * The image is stored in buffer.
 * <br><b>WARNING<b> : buffer must be allocated and large enough (can need up
 * to 150ko).
 * 
 * @param buffer an allocated buffer that will hold the image.
 * 
 * @return size of the image in bytes.
 */

#define SIZE 1024*1024

int WbPanTilt::get_image(unsigned char *buffer)
{
	unsigned char buf_rec[SIZE];
	int img_size = 0;
	int retval, i;
	const char * req="GET /IMAGE.JPG HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n";
	int sock=init_sock();
	/* On demande le flux JPG */
	send(sock,req,strlen(req),0);

	/* On commence à récupérer l'image */
	retval = recv(sock, buf_rec, SIZE, 0);
	while(retval != 0)
	{
		/* On copie les données du buffer dans le buffer final */
		memcpy(buffer+img_size, buf_rec, retval);
		img_size += retval;
		retval = recv(sock, buf_rec, SIZE, 0);
	}

	/* Le début du buffer est pollué par les headers HTTP */
	for(i=0; i<img_size-1; i++)
	{
		// une image JPEG commence par 0xFFD8
		if(buffer[i] == 0xFF && buffer[i+1] == 0xD8)
		{
			img_size -= i;
			memmove(buffer, buffer+i, img_size);
			break;
		}
	}

	close(sock);
	/* L'image est entièrement dans buffer, et fait img_size octets */
	return img_size;
}
