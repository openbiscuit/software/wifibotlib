/**
 * This program display a chosen image and give the hue value (in stdout) of clicked pixel
 * (right click).
 * Input will be ask regarding the type of image (only jpeg and ppm), then on the file name
 * along with the path to reach the file if needed. 
 */

#include "LibRemote.h"
#include "stdio.h"
#include "string.h"
#include "../include/libwb/wbcamera.h"
//#include <iostream>

typedef enum image_type
{
	jpeg = 1,
	ppm = 2,
	from_camera = 3
} image_type;

//using namespace std;


void colorChoser(unsigned char * image_rgb, int width, int height)
{
	int x, y;
	hsl_image * image_hsl;
	unsigned char * to_display;
	roi * p_roi = initRoi();
	boolean draw_roi = FALSE;
	int roi_x, roi_y;
	
	if(image_rgb == NULL)
		return;
	image_hsl = rgb2hsl(image_rgb, width, height);
	to_display = (unsigned char *)malloc(width * height * 3 * sizeof(unsigned char));
	memcpy(to_display, image_rgb, width * height * 3);
	
	while(1)
	{
		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_MOUSEBUTTONDOWN:
					x = event.button.x;
					y = event.button.y;
					if(event.button.button == SDL_BUTTON_RIGHT)
					{
						printf("hue = %d, lightness = %d, saturation = %d", image_hsl->pixels[y][x].hue, image_hsl->pixels[y][x].lightness, image_hsl->pixels[y][x].saturation);
						if(image_hsl->pixels[y][x].isblack == 1)
							printf("pixel is black");
						if(image_hsl->pixels[y][x].iswhite == 1)
							printf("pixel is white");
						if(image_hsl->pixels[y][x].hue == UNDEFINED)
							printf("pixel is achromatic");
						printf("\n");
					}		

					else if(event.button.button == SDL_BUTTON_LEFT)
					{
						if(draw_roi == FALSE)
						{
							draw_roi = TRUE;
							roi_x = event.button.x;
							roi_y = event.button.y;
							printf("button 3\n");
						}
						else
						{
							draw_roi = FALSE;
							displayRGBBuffer(image_rgb, width, height);
						}
					}
					
					break;

				case SDL_MOUSEMOTION:
					if(draw_roi)
					{
						printf("mouse moved\n");
						reinitRoi(p_roi);
						addPixelToROI(roi_y, roi_x, p_roi);
						addPixelToROI(event.motion.y, event.motion.x, p_roi);
						drawROIOnImage(to_display, p_roi, width, height);
						displayRGBBuffer(to_display, width, height);
						memcpy(to_display, image_rgb, width * height * 3);
					}
					break;
					
				case SDL_KEYUP:
				// If escape is pressed, return (and thus, quit)
        			if (event.key.keysym.sym == SDLK_ESCAPE)
        			{
        				free(to_display);
        				freeHslImage(image_hsl);
          				return;
        			}
        			break;
      			case SDL_QUIT:
       				free(to_display);
       				freeHslImage(image_hsl);
       				free(image_rgb);
        			exit(0);
			}
		}
	}
	free(to_display);
	freeHslImage(image_hsl);
}

void commandCamera(char * ip)
{
	robot * bot;
	unsigned char image_jpeg[150000];
	unsigned char * image_rgb;
	int size, width, height, cam_x, cam_y;

	bot = initCameraWitoutMoving(ip);
	cam_x = bot->cam_x;
	cam_y = bot->cam_y;

	if(RemoteGetImage(bot, image_jpeg, &size) != 0)
	{
		printf("error while retrieveing image\n");
		return;
	}
	displayJPGBuffer(image_jpeg, size);
	while(1)
	{
		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			if(event.type == SDL_KEYDOWN)
			{
				switch(event.key.keysym.sym)
				{
					case( SDLK_UP):
						RemoteTournerWebcam(bot, 0, 5, Top);
						sleep(1);
						if(RemoteGetImage(bot, image_jpeg, &size) != 0)
						{
							printf("error while retrieveing image\n");
							return;
						}
						displayJPGBuffer(image_jpeg, size);
						break;
					case(SDLK_DOWN):
						RemoteTournerWebcam(bot, 0, 5, Bottom);
						sleep(1);
						if(RemoteGetImage(bot, image_jpeg, &size) != 0)
						{
							printf("error while retrieveing image\n");
							return;
						}
						displayJPGBuffer(image_jpeg, size);
						break;
					case(SDLK_LEFT):
						RemoteTournerWebcam(bot, 5, 0, Left);
						sleep(1);
						if(RemoteGetImage(bot, image_jpeg, &size) != 0)
						{
							printf("error while retrieveing image\n");
							return;
						}
						displayJPGBuffer(image_jpeg, size);
						break;
					case(SDLK_RIGHT):
						RemoteTournerWebcam(bot, 5, 0, Right);
						sleep(1);
						if(RemoteGetImage(bot, image_jpeg, &size) != 0)
						{
							printf("error while retrieveing image\n");
							return;
						}
						displayJPGBuffer(image_jpeg, size);
						break;
					case(SDLK_RETURN):
						if(RemoteGetImage(bot, image_jpeg, &size) != 0)
						{
							printf("error while retrieveing image\n");
							return;
						}
						image_rgb = jpegBufferDecompress(image_jpeg, size, &width, &height);
						colorChoser(image_rgb, width, height);
						free(image_rgb);
						break;
					case SDLK_ESCAPE:
						return;
					default:
						break;
				}
			}
		}
	}
}


void menu()
{
	int image_type, width, height;
	char file[100];
	char ip[20];
	unsigned char * image_rgb;
	
	printf("choisissez un type d'image\n");
	printf("1:jpeg\n");
	printf("2:ppm\n");
	printf("3:from_camera\n");
	scanf("%d", &image_type);
	if(image_type == 3)
	{
		printf("ip of camera\n");
		scanf("%s", ip);
		commandCamera(ip);
	}
	else
	{
		printf("nom de l'image (avec chemin)\n");
		scanf("%s", file);
		
			switch(image_type)
			{
				case jpeg:
	    			displayJPGFile(file);
	  				image_rgb = jpegFileDecompress(file, &width, &height);
	  				break;
	  				
	  			case ppm:
					displayPPMFile(file);
					image_rgb = ppmFileDecompress(file, &width, &height);
	  				break;
	
	  			default:
					image_rgb = NULL;
					break;
			}
	}
	colorChoser(image_rgb, width, height);
	free(image_rgb);
}

int main()
{
	initSDL();
	
	menu();
	
	return 1;
}
