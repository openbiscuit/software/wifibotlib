#
# Pour tester les notions de IVH dans [Smart00]
#
import scipy
import scipy.linalg
import pylab
import matplotlib.patches
import math

# Calcule l'IVH
# @param m_examples matrice of examples, one by line
# @param m_point column matrice of the point seeked
# @param coef by which confident value us multiplied (max_Vii * coef)
def compute_ivh( m_examples, m_point, coef):
    """ Calcule si le pt m_src est dans l'ivh des points m_pt
    """
    fig = pylab.figure()
    ax = fig.add_subplot(111)
    (max_Vii,Kinv,V) = compute_matrices(m_examples)
    (a,b,c,d,f,g) = determine_ellipsoid(Kinv, max_Vii*coef, term=1.0)
    add_ellipse( ax, a, b, c, d, f, g, verbose=True) 
    # affiche les points avec des croix rouges
    ax.plot(m_examples[:,0],m_examples[:,1],'r+')
    # affiche la src avec un rond vert
    ax.plot(m_point[0,:], m_point[1,:],'go')
    xmin = min(m_point[0,:], min(m_examples[:,0]))-0.1
    xmax = max(m_point[0,:], max(m_examples[:,0]))+0.1
    ymin = min(m_point[1,:], min(m_examples[:,1]))-0.1
    ymax = max(m_point[1,:], max(m_examples[:,1]))+0.1
    ax.set_xlim(xmin,xmax)
    ax.set_ylim(ymin,ymax)
    pylab.show()
    return (max_Vii,Kinv,V)

# Check Ellipse
# Given by a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
# @return True if the parameters define an ellipse
# See http://mathworld.wolfram.com/Ellipse.html
def check_ellipse( a=0, b=0, c=0, d=0, f=0, g=0, verbose=False):
    """ Verifie que les parametres sont compatibles avec une ellipse.
        a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
    """
    matDelta = scipy.array(([a,b,d],[b,c,f],[d,f,g]))
    matJ = scipy.array(([a,b],[b,c]))
    delta = scipy.linalg.det( matDelta )
    J = scipy.linalg.det( matJ )
    I = a + c
    if (verbose) :
        print "--- check_eclipse\n"
        print "    delta=",delta,"  J=",J,"  I=",I,"\n"
    if (delta != 0) and (J > 0) and (delta/I < 0) :
        return True

    return False

# Compute Ellipse parameters
# Given by a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
# @return (centerXY,lengthHor,lengthVer,angle)
# See http://mathworld.wolfram.com/Ellipse.html
def get_ellipse_parameters( a=0, b=0, c=0, d=0, f=0, g=0, verbose=False):
    """ Calcule les parametres d'une ellipse.
        a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
    """
    centerX = (c*d - b*f)/(b*b - a*c)
    centerY = (a*f - b*d)/(b*b - a*c)
    angle = 0.5 * math.atan2( 2*b,(c-a)) #arccot(z) = arctan(1/z)
    num_length = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
    det_lengthH = (b*b-a*c)*((c-a)*math.sqrt(1+(4*b*b/((a-c)*(a-c))))-(c+a))
    det_lengthV = (b*b-a*c)*((a-c)*math.sqrt(1+(4*b*b/((a-c)*(a-c))))-(c+a))
    length1 = 2*math.sqrt(num_length/det_lengthH)
    length2 = 2*math.sqrt(num_length/det_lengthV)
    length_H = max(length1,length2)
    length_V = min(length1,length2)
    if (verbose):
        print "--- get_ellipse_parameters\n"
        print "    center=(",centerX,",",centerY,")  angle=",angle,"  lengthH=",length_H,"  lengthV=",length_V,"\n"

    return (scipy.array([centerX,centerY]),length_H,length_V,angle)

# Get an ellipse patch for matplotlib
# Given by a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
# @return matplotlib.patches.Ellipse
def get_ellipse_patch_cart( a=0, b=0, c=0, d=0, f=0, g=0, verbose=False):
    """ Construi un 'patch' d'ellipse avec pylab en coord. cartesiennes.
        a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
    """
    if (check_ellipse(a,b,c,d,f,g,verbose) == False) :
        return
    cXY,lH,lV,ang = get_ellipse_parameters(a,b,c,d,f,g,verbose)
    ang = (ang*180/math.pi)
    ellipse = matplotlib.patches.Ellipse(xy=cXY,width=lH,height=lV,angle=ang)
    return ellipse

# Add a matplotlib.patches.Ellipse to a subfigure (axes)
# @param ax (as given by figure.add_subplot for example)
def add_ellipse( ax, a=0, b=0, c=0, d=0, f=0, g=0, verbose=False):
    """ Ajoute une ellipse a une (sous)-figure.
        a x^2 + 2b xy + c y^2 + 2d x + 2f y + g = 0
    """
    ellipse = get_ellipse_patch_cart(a,b,c,d,f,g,verbose)
    ax.add_artist(ellipse)
    ellipse.set_clip_box(ax.bbox)
    ellipse.set_alpha(0.1)
    
                                         
def draw_ellipse( a=0, b=0, c=0, d=0, f=0, g=0, verbose=False):
    fig = pylab.figure()
    ax = fig.add_subplot(111)
    ellipse = get_ellipse_patch_cart(a,b,c,d,f,g,verbose)
    ax.add_artist(ellipse)
    ellipse.set_clip_box(ax.bbox)
    ellipse.set_alpha(0.1)
    ax.set_xlim(-10,10)
    ax.set_ylim(-10,10)
    pylab.show()

def plot_ellipse( center, w, h, ang):
    fig = pylab.figure()
    ax = fig.add_subplot(111)
    ellipse = matplotlib.patches.Ellipse(xy=center,width=w,height=h,angle=ang)
    ax.add_artist(ellipse)
    ellipse.set_clip_box(ax.bbox)
    ellipse.set_alpha(0.1)
    ax.set_xlim(-2,2)
    ax.set_ylim(-2,2)
    pylab.show()

# Add a constant term to each line of a matrix.
# @param mat matrice
def add_term_line( mat ):
    """ Ajoute un term constant egal a 1 a chaque ligne d'une matrice.
    """
    n,m = mat.shape
    result = scipy.concatenate((mat,scipy.ones((n,1))),1)
    return result

# Add a constant term to each col of a matrix.
# @param mat matrice
def add_term_col( mat ):
    """ Ajoute un term constant egal a 1 a chaque colonne d'une matrice.
    """
    n,m = mat.shape
    result = scipy.concatenate((mat,scipy.ones((1,m))),0)
    return result

# Compute matrices for hull computation
# @param examples one line for each x
# @return (max_Vii, Kinv, V)
def compute_matrices(examples):
    tmpK = scipy.dot(examples.transpose(), examples)
    tmpKinv = scipy.linalg.pinv2( tmpK )
    tmpV = scipy.dot(examples, scipy.dot(tmpKinv, examples.transpose()))
    max_Vii = max(scipy.diag(tmpV))
    return (max_Vii, tmpKinv, tmpV) 

# Compute the parameters of the ellipsoid
# @param Kinv matrice
# @param conf confidence factor (max_Vii)
# @param val of the constant term (default=1.0)
def determine_ellipsoid(Kinv, conf, term=1.0):
    a = Kinv[0,0]
    b = Kinv[0,1]
    c = Kinv[1,1]
    if( Kinv.shape == (3,3)) : 
        d = Kinv[0,2]
        f = Kinv[1,2]
        g = Kinv[2,2]-conf
    else:
        d = 0
        f = 0
        g = -conf
    return (a,b,c,d,f,g)

# Read from keyboard a matrix
# @return 2D scipy.array
def input_array():
    matrice = []
    line = raw_input("[line]=")
    nbRow = 0
    nbCol = 0
    while( line != "--"):
        nbRow += 1
        # separe chaque nombre (avec espace entre eux)
        data = map(float, line.split())
        nbCol = len(data)
        print "lu "+str(len(data))+" nombres"
        for number in data:
            matrice.append(number)
        line = raw_input("[line]=")

    print "matrice "+str(nbRow)+" x "+str(nbCol)
    M = scipy.array(matrice)
    return M.reshape((nbRow,nbCol))
    
    

matP = scipy.array(([0.025,3.48],
                    [0.027,3.18],
                    [0.030,2.88],
                    [0.034,2.58],
                    [0.038,2.28],
                    [0.044,1.98],
                    [0.052,1.68],
                    [0.063,1.38],
                    [0.080,1.08],
                    [0.018,4.68]))

matY = scipy.array(([0],
                    [0],
                    [0],
                    [0],
                    [0],
                    [0.0005],
                    [0.0747],
                    [1.4925],
                    [4.5],
                    [0]))

matX1 = scipy.array(([0.179],[0.49]))
matX2 = scipy.array(([0.111],[0.79]))

matK = scipy.dot(matP.transpose(), matP)
matKinv = scipy.linalg.pinv2( matK )
matV = scipy.dot(matP, scipy.dot(matKinv, matP.transpose()))
max_vi = max(scipy.diag(matV))

matH = scipy.dot(matX1.transpose(), scipy.dot(matKinv, matX1 ))


fig = pylab.figure()
ax = fig.add_subplot(111)
ax.set_xlim(-10,10)
ax.set_ylim(-10,10)
add_ellipse( ax, a=matKinv[0,0], b=(matKinv[0,1]+matKinv[1,0])/2, c=matKinv[1,1],g=(-max_vi), verbose=True) 
# affiche les points avec des croix rouges
ax.plot(matP[:,0],matP[:,1],'r+')
# affiche la src avec un rond vert
ax.plot(matX1[0,:],matX1[1,:],'go')
ax.set_xlim(-10,10)
ax.set_ylim(-10,10)
pylab.show()

# faut afficher les matrices ainsi produites par le programme en C
