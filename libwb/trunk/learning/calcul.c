/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   calcul.h
 * @brief  Matrix calculus (pseudo inverse and singular value decomposition)
 *
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * <br><b>French comments can be found in the header file</b>.
 * Used by:
 */

#include "calcul.h"
#include <stdlib.h>
#include <stdio.h>
#include "svdlib.h"
//#include "svdutil.h"


void init_matrix(double def_value,DMat m)
{
  int i,j;
  for(i=0;i<m->rows;i++)
    {
      for(j=0;j<m->cols;j++)
	{
	  matrix_set_value(m,i,j,def_value);
	}
    }
}
int is_empty_matrix(DMat m)
{
  int i,j;
  for(i=0;i<m->rows;i++)
    {
      for(j=0;j<m->cols;j++)
	{
	  if(m->value[i][j] != 0.0)
	    return 0;
	}

    }
  return 1;

}

double matrix_max_diag(DMat m)
{
  int i;
  double res;
  int z;
  if(m->rows < m->cols)
    z = m->rows;
  else
    z = m->cols;
	
  if(z==0)
    {
      return 0.0;
    }
  res=m->value[0][0];
  for(i=0;i<z;i++)
    {
      if(m->value[i][i] > res)
	res = m->value[i][i];
		
    }
  //printf("res : %f\n",res);


  return res;
}
DMat matrix_transp(DMat m)
{
  int i,j;
  DMat res;
  res = svdNewDMat(m->cols,m->rows);
  for(i=0;i<m->rows;i++)
    {
      for(j=0;j<m->cols;j++)
	{
	  matrix_set_value(res,j,i,matrix_get_value(m,i,j));
	}
	
    }
  return res;
}

DMat matrix_mult(DMat m1,DMat m2)
{
  int i,j,x;
  double f;
  DMat res;


  if(m1->cols > m2->rows)
    {	printf("erreur: multiplication de matrice avec dimension incompatible : m1 : %ld %ld , m2 : %ld %ld\n",m1->cols,m1->rows,m2->cols,m2->rows);	
    return res;
    }

	
  res = svdNewDMat(m1->rows,m2->cols);
  for(i=0;i<res->rows;i++)
    {
      for(j=0;j<res->cols;j++)
	{
	  f=0;
	  for(x=0;x<m1->cols && x<m2->rows;x++)
	    {
	      f+=matrix_get_value(m1,i,x)*matrix_get_value(m2,x,j);
	    }
	  matrix_set_value(res,i,j,f);
	}
		
    }
  return res;
}
DMat matrix_redim(DMat m,int height,int width)
{
  int i,j;
  DMat res;
  if((height< m->rows)||(width < m->cols))
    {return m;}
  res = svdNewDMat(height,width);
  init_matrix(0.0,res);
  for(i=0;i<m->rows;i++)
    {
      for(j=0;j<m->cols;j++)
	{
	  matrix_set_value(res,i,j,(matrix_get_value(m,i,j)));
	}
		
    }
  return res;
	
}
DMat vector2diagmatrixinv(double *v,int d,int height,int width)
{
	
  int i;

  DMat mat;
  mat=svdNewDMat(height,width);
  //mat=svdNewDMat(d,d);
  init_matrix(0.0,mat);

  for(i=0;i<d;i++)
    {
      if(v[i]!=0)
	{
	  matrix_set_value(mat,i,i,1.0/v[i]);
	}
      else
	{
	  matrix_set_value(mat,i,i,0.0);	
	}
    }
  return mat;	
}
void printmatrix(DMat m)
{
  int i,j;

  for(i=0;i<m->rows;i++)
    {
      for(j=0;j<m->cols;j++)
	{
	  printf("%f\t",m->value[i][j]);
	}
      printf("\n");
    }

}

void printMatrixClean(DMat m)
{
  int i,j;

  for(i=0;i<m->rows;i++)
    {
      for(j=0;j<m->cols;j++)
	{
	  printf("%6.3f\t",m->value[i][j]);
	}
      printf("\n");
    }

}

DMat matrix_inv(DMat m)
{
  //int i,j;
  DMat res;
  DMat ep;
  DMat m1;
  DMat U;
  DMat V;
  SMat temp;
  SVDRec restemp;
	
  temp=svdConvertDtoS(m);
  restemp = svdLAS2A(temp,0);

  ep = vector2diagmatrixinv(restemp->S,restemp->d,restemp->Ut->rows,restemp->Vt->cols);
  V = matrix_transp(restemp->Vt);
	
  U = matrix_redim(restemp->Ut,ep->cols,restemp->Ut->cols);
  m1 = matrix_mult(ep,U);
  res = matrix_mult(V,m1);
  if(U!=restemp->Ut){
    svdFreeDMat(U);}
  svdFreeSVDRec(restemp);
  svdFreeSMat(temp);
  svdFreeDMat(m1);
  svdFreeDMat(ep);
  svdFreeDMat(V);
	
  //svdFreeDMat(U);
	
  return res;	
	
}
void matrix_set_value(DMat m,int row, int col,double value)
{
  m->value[row][col] = value;
}

double matrix_get_value(DMat m, int row, int col)
{
	
  return m->value[row][col];
}

void set_verbosity( long verb )
{
  SVDVerbosity = verb;
}
