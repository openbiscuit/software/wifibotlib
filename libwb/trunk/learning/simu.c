/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   simu.c
 * @brief  Interface pour effectuer l'apprentissage sur le wifibot.
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * Mise en oeuvre de l'apprentissage, la boucle principale étant :<br>
 * <li> màj de old/new_distance/angle -> x</li>
 * <li> màj de old_action (qui est en fait l'action que l'on va effectuer)</li>
 * <li> action effectuée </li>
 * <li> targ = chercher_cible() et màj distance/angle -> xp</li>
 * <li> apprentissage Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action)</li>
 *
 * Il existe deux modes de fonctionnement.
 * Le premier mode est interactif, on guide le robot pas à pas (flèches).
 * Le deuxième mode (d) fait faire 100 actions+apprentissage au robot.
 *
 * Enfin, le programme a besoin de la présence d'un fichier 'data' dans
 * le répertoire. Attention, tous les nouveaux points sont ajoutés dans ce fichier,
 * ce qui peut dégrader les peformances du robot.
 *
 * Il faut aussi que le programme 'remote.wb' tourne sur le wifibot. 
 *
 * Des structures ont été mises en place pour, éventuellement, gérer des sortes
 * de traces d'éligibilité (_mem).

 * Enfin, pour rappel, les fonctions de l'interface.
 * <li> q : quit</li>
 * <li> i : ??</li>
 * <li> fleche_bas   : ACTION_RECULER + rech. cible + Hedger_training</li>
 * <li> fleche_haut  : ACTION_AVANCER + rech. cible + Hedger_training</li>
 * <li> fleche_droite: ACTION_TOURNERD + rech. cible + Hedger_training</li>
 * <li> fleche_gauche: ACTION_TOURNERG + rech. cible + Hedger_training</li>
 * <li> s : afficher l'arbre</li>
 * <li> d : pendant 100 pas de temps, faire apprentissage + action,
 * avec epsilon-greedy (epsilon=10%)</li>
 * <li> l ou m (disable) : apprentissage dans simulateur ? </li> 
 */

#include "SDL.h"
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL_rotozoom.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include "learn.h"
#include "traitement_image.h"
#include "LibRemote.h"
#define SDL_VIDEO_FLAGS (SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_ANYFORMAT)
#define ACTION_AVANCER 1
#define ACTION_RECULER 2
#define ACTION_RIEN 0
#define ACTION_TOURNERG 3
#define ACTION_TOURNERD 4
#define H 10
#define K 10
#define ALPHA 0.9
#define GAMA 0.6

KdTree *S;
int affichage;
int discretiseur;
SDL_Surface * screen;
double old_rewards;
double old_action;
double old_distance;
double new_distance;
double new_angle;
double ac_efec;
double old_angle;
FILE *fp;
SDL_Surface * black;
double pi = 3.1415927;
target targ;
typedef struct memoire{double distance;double angle;double action;KdVectorList *l;int LWRUSE;}memoire;
typedef struct sprite{int x;int y;double angle;int sensx;int sensy;SDL_Surface *im;}sprite;
int w,h;
sprite s1,s2;
memoire mem[50];
int nb_mem;
int current_ind;
void init_mem()
{
  int i;
  for(i=0;i<50;i++)
    {
      mem[i].distance = 0;
      mem[i].angle = 0;
      mem[i].action = 0;
      mem[i].l = NULL;
      mem[i].LWRUSE = 0;

    }
  nb_mem = 0;
  current_ind = 0;

}
void maj_mem(double alpha,double gama,KdTree *t)
{
  double qnew;
  double qt;
  double qtp;
  int index,index2,borne;
  int i=0;
  KdVector *x;
  KdVector *x2;
  KdVector *x3;
  //printf("nb elem: %d\n",nb_mem);
  if(nb_mem>2){
    x = KdNewVector(4);
    //printf("mise a jour de la memoire\n");
    index = current_ind-1;
    if(index<0)
      index = 49;
    borne = current_ind;
    if(borne ==50)
      borne = 0;

    if(nb_mem<50)
      borne = 1;

    //printf("borne : %d, index : %d\n",borne,index);

    while(index != borne)
      {

	i++;
	index2 = index -1;
	if(index2 < 0)
	  index2 = 49;

	x->value[0] = mem[index].distance;
	x->value[1] = mem[index].angle;
	x->value[2] = mem[index].action;
	x->value[3] = 1;

	KdGetVect(t->tete,x,&x2);

	if(x2==NULL)
	  printf("NUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUL\n");
	qtp = x2->key;
	x->value[0] = mem[index2].distance;
	x->value[1] = mem[index2].angle;
	x->value[2] = mem[index2].action;
	x->value[3] = 1;
	
	KdGetVect(t->tete,x,&x3);
	if(x3==NULL)
	  printf("NUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUL\n");
	qt = x3->key;



	qnew = (alpha)*(0+gama*qtp - qt) + qt;



	x3->key = qnew;

	index = index-1;
	if(index < 0)
	  index = 49;



      }

    KdFreeVector(x);
  }

  init_mem();
}
void add_to_mem(double dist,double angl,double ac,int LWRUSE)
{

  mem[current_ind].distance = dist;
  mem[current_ind].angle = angl;
  mem[current_ind].action = ac;
  mem[current_ind].l = NULL;
  mem[current_ind].LWRUSE= LWRUSE;
  current_ind++;

  if(nb_mem<50)
    nb_mem ++;

  if(current_ind == 50)
    current_ind = 0;


}
void init_sim(int width,int height)
{
  w = width;
  h = height;
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) == -1)
    {
      fprintf(stderr, "Erreur lors de l'initialisation de SDL: %s\n",
	      SDL_GetError());

    }
  // on ouvre le joystick
  SDL_JoystickUpdate();

  screen = SDL_SetVideoMode(w, h, 8, SDL_VIDEO_FLAGS);
}
double view_center()
{

  return targ.center;

}
double view_dist_cible()
{
  double a;
  //if(targ.hauteur !=100000000)
  return targ.hauteur;
  /*else
    {
    a = rand()%20;
    a = a/20;
    return targ.hauteur +a;
    }*/
}
void affiche_sprite(sprite sp)
{
  SDL_Rect p;
  SDL_Surface *s;
  if(affichage)
    {
      s= rotozoomSurface(sp.im, sp.angle, 1,0);
      p.x = sp.x;
      p.y = sp.y;
      SDL_BlitSurface(s, NULL, screen, &p);
      SDL_Flip(screen);
      SDL_FreeSurface(s);
    }
}
void desaffiche_sprite(sprite sp)
{
  if(affichage)
    {
      SDL_Rect p;
      p.x = sp.x;
      p.y = sp.y;
      SDL_BlitSurface(black, NULL, screen, &p);
      SDL_Flip(screen);
    }
}
void desaffiche_sprite2(sprite sp)
{
  if(affichage)
    {
      SDL_Rect p;
      p.x = sp.x;
      p.y = sp.y;
      SDL_BlitSurface(black, NULL, screen, &p);
      //SDL_Flip(screen);
    }
}
int avancer_sprite(sprite *sp,int norme)
{
  desaffiche_sprite2(*sp);
  int x;
  int y;
  int newx,newy;
  int res =0;
  x = norme*(sin(sp->angle*(pi/180)));
  y = norme*(cos(sp->angle*(pi/180)));
  //printf("angle : %f x %d y %d \n",sp->angle,x,y);
  newx = sp->x + x;
  newy = sp->y + y;
  if((newx< 40)||(newy<40)||(newx > w - 40)||(newy > h-40))
    {
      res = -1;
      if(newx <40)
	newx = w-40;
      if(newy <40)
	newy = h-40;
      if(newx > w-40)
	newx = 40;
      if(newy>h-40)
	newy = 40;
      sp->x = newx;
      sp->y = newy;
    }
  else
    {
      sp->x = newx;
      sp->y = newy;
    }
  affiche_sprite(*sp);

  return res;
}

double distance(sprite sp1,sprite sp2)
{
  int x,y;
  x = sp2.x - sp1.x;
  y = sp2.y - sp1.y;
  return sqrt(x*x + y*y);
	
}
double angle(sprite sp1,sprite sp2)
{
  double x,y;
  double angle1,angle2,angle3;
  x = sp2.x - sp1.x;
  y = sp2.y - sp1.y;
  angle1 = acos(y/distance(sp1,sp2))*(180/pi);
  angle3 = asin(x/distance(sp1,sp2))*(180/pi);
  if(angle3<0)
    angle1=360-angle1;
  if(angle1<0)
    angle1+=360;
  angle2 = sp1.angle - angle1;
  if(angle2>360)
    angle2 = angle2-360;
  if( angle2< 0)
    angle2 = angle2 + 360;
  return angle2;
}


double distance_to_mur()
{



  return (h -s1.y);


}
double view_angle()
{
  double a,b;
  b = rand()%100;
  b = b/10000;

  a = angle(s1,s2);
  if((a<90)||(a>270))
    {
	
      /*	discretiseur = a*100;

      a = discretiseur;*/
      return a+b;
    }
  else
    return -300+b;
	
}
double view_distance()
{
  double a,b;
  b = rand()%100;
  b = b/10000;

  //return 0.0;
  a = angle(s1,s2);
  if((a<90)||(a>270))
    {
	
      return distance(s1,s2)+b;
    }
  else
    return -300+b;


	
}	
double view_distancer()
{
  double a;

  a = angle(s1,s2);
  if((a<90)||(a>270))
    {
	
      return distance(s1,s2);
    }
  else
    return -5000;
	
}
/**
 * Recompense pour le wifibot.
 * <li> -100 : si avance et perd la cible (new_distance == 100000000)</li>
 * <li>  300 : si distance in ]100; 150[</li>
 * <li> -300 : si distance > 150 mais pas infini (100000000)</li>
 * <li>et quatre facteurs additifs:
 *     <li> +150 si angle in ]250,450[</li>
 *     <li> +150 si vu alors que avant pas vu</li>
 *     <li> -100 si distance augmante</li>
 *     <li> +100 si se rapproche</li>
 * </li>
 */
double rewards()
{
  double d,a;
  //printf("reconpense ? \n");
  double res = 0;
  if(old_action ==ACTION_AVANCER && new_distance == 100000000)
    return -100;
  if(new_distance>100 && new_distance<150){


    return 300;

  }

  if(new_distance>150 && new_distance!=100000000)
    {

      return -300;}
  if(new_angle >250 && new_angle< 450)
    res+= 150;
  if(old_angle == -1 && new_angle !=-1)
    res+= 150;
  if(old_distance-new_distance >0)
    {
      res-= 100;
    }
  else{
    if(old_distance-new_distance!=0)
      res+= 100;}
  return res;
  /*	d=view_distancer();
	if(d<50 && d!=0)
	return 0;
	if(d<80 && d > 50)
	{
	//printf("on a une récompense\n");

	return 300;

		
	}
	a = view_angle();
	if((a>357||(a <4 && a >=0)))
	{if(old_action ==1)return 300;else return 0;}
	else
	{
	if(old_action ==1)return 0;
	else
	return 50;
	}


	/*if(old_distance - d >0)
	return 50;*/

	
	
  return 0;
	
}	

void tourner_sprite(sprite *sp,double angle)
{
  desaffiche_sprite(*sp);
  sp->angle += angle;
  if(sp->angle<0)
    sp->angle+= 360;
  if(sp->angle>360)
    sp->angle-=360;
  affiche_sprite(*sp);

}
void handler(int sig)
{
  char buf[255];
  KdWriteTree(S->tete,S->f);
  close(fp);
  exit(0);

}
int main(int argc,char *argv[])
{
  int i,j;
  long int t;
  int action;
  char *buffer;
  size_t len = 0;
  int sock;
  double f1,f2,f3,f4;

  KdVector *we;
  t=1;
  SDL_Event event;
  KdVector *ac;
  KdVector *x;
  KdVector *xp;
  KdVector *temp;
	
  KdVectorList *li;
  KdVectorListp *po;
  li = KdNewVectorList();
  target tar;
  struct sigaction nvt, old;


  affichage = 1;
  /* initialise SDL */
  init_sim(400,400);

  /* recupere les points d'apprentissage */
  /* et les stockes dans 'li' */
  fp = fopen("data","r");
  getline(&buffer,&len,fp);
  while(getline(&buffer,&len,fp)!=EOF)
    {

      sscanf(buffer,"%lf %lf %lf %lf\n",&f1,&f2,&f3,&f4);
      if(f3 != 4.0 && f3 !=2.0)
	{

	  temp = KdNewVector(4);
	  temp->value[0] = f1;
	  temp->value[1] = f2;
	  temp->value[2] = f3;
	  temp->value[3] = 1;
	  temp->key = f4;
	  printf("%f %f %f %f\n",f1,f2,f3,f4);
	  KdAddVector(li,temp);

	}

      //free(buffer);
    }

  /*for(i=0;i<500;i++)
    {
    f1 = rand()%140 + 30;
    f2 = rand()%610 + 30;
    f3 = 3;
    if(f1 < 150)
    f4 = -100 ;
    else
    f4 = 100;

    temp = KdNewVector(3);
    temp->value[0] = f1;
    temp->value[1] = f2;
    temp->value[2] = f3;
    temp->key = f4;
    printf("%f %f %f %f\n",f1,f2,f3,f4);
    KdAddVector(li,temp);
    }*/
  printf("ici\n");
  close(fp);
  fp = fopen("data","w");
  S = KdNewTree(4,10);
  fprintf(fp,"%d\n",S->dim);
  KdSetFile(S,fp);
  nvt.sa_handler = handler;
  sigaction(SIGTSTP, &nvt, &old);

  /* preparation des poids pour la distance */
  we = KdNewVector(4);
  we->value[0] = 1; //poid de la distance :la distance a très peut d'importance
  we->value[1] = 1; //poid de l'angle : importance normal
  we->value[2] = 100;//l'action a une importance maximal
  we->value[3] = 1;
  KdSetWeight(S,we);
  /* ajout des points precedents a l'arbre */
  po = li->tete;
  while(po!=NULL)
    {
      KdAddVectorToTree(S,po->v);
      po = po->next;
    }
  /* vecteur état actuel */
  x = KdNewVector(2);
  /* vecteur état suivant */
  xp = KdNewVector(2);
  /* vecteur liste des actions possibles */
  ac = KdNewVector(5);
  ac->value[0] = ACTION_RIEN;
  ac->value[1] = ACTION_AVANCER;
  ac->value[2] = ACTION_RECULER;
  ac->value[3] = ACTION_TOURNERG;
  ac->value[4] = ACTION_TOURNERD;
	
  s1.im = SDL_LoadBMP("sprite1.bmp");
  black = SDL_LoadBMP("black.bmp");
  srand((int)time(NULL));
  s2.im = SDL_LoadBMP("sprite2.bmp");
  desaffiche_sprite(s1);
  i=40 + rand()%(w-80);
  j = 40 + rand()%(h-80);
  s1.angle = rand()%260;
  s1.x = i;
  s1.y = j;
  affiche_sprite(s1);
  desaffiche_sprite(s2);
  i=40 + rand()%(w-80);
  j = 40 + rand()%(h-80);
  s2.angle = rand()%260;
  s2.x = i;
  s2.y = j;
  affiche_sprite(s2);
  SVDVerbosity=0;
  printf("là\n");

  /* connexion au wifibot */
  RemoteConnec();/*
		   printf("on essaye d'avancer\n");
		   RemoteAvancerCourbe(10,10);
		   sleep(1);
		   RemoteStop();*/
  printf("on est ici :d\n");

  /* recherche de la cible */
  targ = chercher_cible();
  new_distance = view_dist_cible();
  old_distance = new_distance;
  new_angle = view_center();
  old_angle =new_angle;
  while(1){
    // if(rewards())
    // maj_mem(ALPHA,GAMA,S);
    if(rewards()==300)
      {
	/*	desaffiche_sprite(s1);
		i=40 + rand()%(w-80);
		j = 40 + rand()%(h-80);
		s1.angle = rand()%260;
		s1.x = i;
		s1.y = j;
		affiche_sprite(s1);
		desaffiche_sprite(s2);
		i=40 + rand()%(w-80);
		j = 40 + rand()%(h-80);
		s2.angle = rand()%260;
		s2.x = i;
		s2.y = j;
		affiche_sprite(s2);*/
	printf("Gagné\n");
      }
    while (SDL_PollEvent(&event))
      {
	/* interface clavier ou joystick
	 * <li> q : quit</li>
	 * <li> i : ??</li>
	 * <li> fleche_bas   : ACTION_RECULER + rech. cible + Hedger_training</li>
	 * <li> fleche_haut  : ACTION_AVANCER + rech. cible + Hedger_training</li>
	 * <li> fleche_droite: ACTION_TOURNERD + rech. cible + Hedger_training</li>
	 * <li> fleche_gauche: ACTION_TOURNERG + rech. cible + Hedger_training</li>
	 * <li> s : afficher l'arbre</li>
	 * <li> d : pendant 100 pas de temps, faire apprentissage + action,
	 * avec epsilon-greedy (epsilon=10%)</li>
	 * <li> l ou m (disable) : apprentissage dans simulateur ? </li> 
	 */
	switch (event.type)
	  {
	  case SDL_KEYDOWN:
	    switch (event.key.keysym.sym)
	      {
	      case SDLK_q:
		KdWriteTree(S->tete,S->f);
		KdFreeTree(S);
		SDL_FreeSurface(s1.im);
		SDL_FreeSurface(s2.im);
		SDL_FreeSurface(black);
		KdFreeVector(x);
		KdFreeVector(xp);
		KdFreeVector(ac);
		SDL_Quit();
							
		fclose(fp);
		exit(0);
		break;
	      case SDLK_i:
		desaffiche_sprite(s1);
		i=40 + rand()%(w-80);
		j = 40 + rand()%(h-80);
		s1.angle = rand()%260;
		s1.x = i;
		s1.y = j;
		affiche_sprite(s1);
		desaffiche_sprite(s2);
		i=40 + rand()%(w-80);
		j = 40 + rand()%(h-80);
		s2.angle = rand()%260;
		s2.x = i;
		s2.y = j;
		affiche_sprite(s2);
		break;
		/*	case SDLK_DOWN:
			tourner_sprite(&s1,180);
			break;
		*/	
	      case SDLK_DOWN:
		old_angle = new_angle;
		old_distance = new_distance;
		old_action = ACTION_RECULER;
		x->value[0] = old_distance;
		//	x->value[0] = 0;
		x->value[1] = old_angle;
		RemoteReculerCourbe(15,15);
		usleep(500000);
		RemoteStop();
		targ = chercher_cible();
		new_distance = view_dist_cible();
		new_angle = view_center();
		xp->value[0] = new_distance;
		//xp->value[0] =0;
		xp->value[1] = new_angle;
		printf("angle : %f, distance : %f\n",view_angle(),view_distance());
		printf("best action : %d\n\n",Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action));
		
		printf("position : %f\n",new_distance);
		break;
	      case SDLK_UP:
		old_distance = new_distance;
		old_angle = new_angle;
		old_action = ACTION_AVANCER;
		x->value[0] = old_distance;
		//	x->value[0] = 0;
		x->value[1] = old_angle;
		RemoteAvancerCourbe(15,15);
		usleep(500000);
		RemoteStop();
		targ = chercher_cible();
		new_distance = view_dist_cible();
		new_angle = view_center();
		xp->value[0] = new_distance;
		//xp->value[0] =0;
		xp->value[1] = new_angle;
		//printf("angle : %f, distance : %f\n",view_angle(),view_distance());
		printf("best action : %d\n\n",Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action));
								
		printf("position : %f\n",new_distance);
						
		break;
	      case SDLK_RIGHT:
		old_distance = new_distance;
		old_angle = new_angle;
		old_action = ACTION_TOURNERD;
		x->value[0] = old_distance;
		//	x->value[0] = 0;
		x->value[1] = old_angle;;
		RemoteTournerHoraire(8,8);
		usleep(500000);
		RemoteStop();
		targ = chercher_cible();
		new_distance = view_dist_cible(); // ~~ target->hauteur
		new_angle = view_center();        // ~~ target->center
		xp->value[0] = new_distance;
		//xp->value[0] =0;
		xp->value[1] = new_angle;
		//printf("angle : %f, distance : %f\n",view_angle(),view_distance());
		printf("best action : %d\n\n",Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action));
								
		printf("position : %f\n",new_distance);
						
		break;
	      case SDLK_LEFT:
		old_distance = new_distance;
		old_angle = new_angle;
		old_action = ACTION_TOURNERG;
		x->value[0] = old_distance;
		//	x->value[0] = 0;
		x->value[1] = old_angle;;
		RemoteTournerAntiHoraire(8,8);
		usleep(500000);
		RemoteStop();
		targ = chercher_cible();
		new_distance = view_dist_cible();
		new_angle = view_center();
		xp->value[0] = new_distance;
		//xp->value[0] =0;
		xp->value[1] = new_angle;
		//printf("angle : %f, distance : %f\n",view_angle(),view_distance());
		printf("best action : %d\n\n",Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action));
								
		printf("position : %f\n",new_distance);
						
		break;
	      case SDLK_s:
		printNode(S->tete);
		break;
		/*	case SDLK_UP:
			old_distance = view_distance();
			old_angle = view_angle();
			old_action = ACTION_AVANCER;
			x->value[0] = old_distance;
			//x->value[0] = 0;
			x->value[1] = old_angle;

			// add_to_mem(old_distance,old_angle,old_action,0);

			avancer_sprite(&s1,10);
			xp->value[0] = view_distance();
			//xp->value[0] =0;
			xp->value[1] = view_angle();
			printf("angle : %f, distance : %f\n",view_angle(),view_distance());
			printf("best action : %d\n\n",	Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action));
							
			break;*/
	      case SDLK_d: // ----- mode "BATCH" -----
		desaffiche_sprite(s1);
		desaffiche_sprite(s2);
		affichage = 0;
		t=1;
		while(t<100)
		  {
		    old_distance = new_distance;
		    old_angle = new_angle;
		    if(!(t%1000))
		      printf("%d\n",t);
		    // if(rewards())
		    // maj_mem(ALPHA,GAMA,S);
		    /*		if(rewards()==300)
				{
				t=100;
				}*/
		    x->value[0] = old_distance;
		    //x->value[0] = 0;
		    x->value[1] = old_angle;

		    /* epsilon greedy */
		    if((rand()%10)>9)
		      {old_action = 1 + rand()%4;
		      //printf("action %f\n",old_action);
		      switch((int)old_action)
			{
			case 0:
										
			  break;
			case 1:
			  RemoteAvancerCourbe(15,15);
			  usleep(500000);
			  RemoteStop();
			  break;
			case 2:
			  RemoteReculerCourbe(15,15);
			  usleep(500000);
			  RemoteStop();
			  break;
			case 3:
			  RemoteTournerAntiHoraire(8,8);
			  usleep(500000);
			  RemoteStop();
			  break;
			case 4:
			  RemoteTournerAntiHoraire(8,8);
			  usleep(500000);
			  RemoteStop();
			  break;
									
			}
		      // add_to_mem(old_distance,old_angle,old_action,0);
		      targ = chercher_cible();
		      new_distance = view_dist_cible();
		      new_angle = view_center();
		      xp->value[0] = new_distance;
		      //xp->value[0] =0;
		      xp->value[1] = new_angle;
		      // add_to_mem(old_distance,old_angle,old_action,0);
		
		      old_action = Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action);
		      while((int)old_action==0){

			old_action = 1 + rand()%4;


		      }
		      }
		    else
		      {
			switch((int)old_action)
			  {
			  case 0:
										
			    break;
			  case 1:
			    RemoteAvancerCourbe(15,15);
			    usleep(500000);
			    RemoteStop();
			    break;
			  case 2:
			    RemoteReculerCourbe(15,15);
			    usleep(500000);
			    RemoteStop();
			    break;
			  case 3:
			    RemoteTournerAntiHoraire(8,8);
			    usleep(500000);
			    RemoteStop();
			    break;
			  case 4:
			    RemoteTournerAntiHoraire(8,8);
			    usleep(500000);
			    RemoteStop();
			    break;
									
			  }
			targ = chercher_cible();
			new_distance = view_dist_cible();
			new_angle = view_center();
			xp->value[0] = new_distance;
			//xp->value[0] =0;
			xp->value[1] = new_angle;
			// add_to_mem(old_distance,old_angle,old_action,0);
			old_action = Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action);



			printf("best action : %f\n\n",old_action);
			while((int)old_action==0){

			  old_action = 1 + rand()%4;


			}
		      }

		    t++;
		    //printf("%d\n",t);
		  }
		affichage = 1;

						
		break;
		/*	case SDLK_l:
			t=1;
			affichage = 1;
			while(t<5000)
			{
			// 					if(rewards())
			// 					maj_mem(ALPHA,GAMA,S);
			if(rewards()==300)
			{
			desaffiche_sprite(s1);
			i=40 + rand()%(w-80);
			j = 40 + rand()%(h-80);
			s1.angle = rand()%260;
			s1.x = i;
			s1.y = j;
			affiche_sprite(s1);
			desaffiche_sprite(s2);
			i=40 + rand()%(w-80);
			j = 40 + rand()%(h-80);
			s2.angle = rand()%260;
			s2.x = i;
			s2.y = j;
			affiche_sprite(s2);
			}
			old_distance = view_distance();
			old_angle = view_angle();
			x->value[0] = old_distance;
			//x->value[0] = 0;
			x->value[1] = old_angle;


			if((rand()%10)>8)
			{old_action = 1+ rand()%2;
			//printf("action %f,%f\n",old_action);
			switch((int)old_action)
			{
			case 0:
										
			break;
			case 1:
			avancer_sprite(&s1,10);
			break;
			case 2:
			tourner_sprite(&s1,-5);
			break;
			case 3:
			tourner_sprite(&s1,5);
			break;
									
			}	
			// add_to_mem(old_distance,old_angle,old_action,0);
			old_action = Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action);
			printf("best action : %f\n\n",old_action);
			while((int)old_action==0){

			old_action = 1 + rand()%2;


			}
			}
			else
			{


			//printf("old action : %f\n",old_action);
			switch((int)old_action)
			{
			case 0:
										
			break;
			case 1:
			avancer_sprite(&s1,10);
			break;
			case 2:
			tourner_sprite(&s1,-5);
			break;
			case 3:
			tourner_sprite(&s1,5);
			break;
									
			}
			xp->value[0] = view_distance();
			//xp->value[0] =0;
			xp->value[1] = view_angle();
			// add_to_mem(old_distance,old_angle,old_action,0);
			old_action=Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action);
			printf("best action : %f\n\n",old_action);
			while((int)old_action==0){

			old_action = 1 + rand()%2;


			}
			}

			t++;
			}

			break;*/
		/*	case SDLK_m:
			t=1;
			affichage = 1;
			while(t<5000)
			{
			// 					if(rewards())
			// 					maj_mem(ALPHA,GAMA,S);
			if(rewards()==300)
			{
			desaffiche_sprite(s1);
			i=40 + rand()%(w-80);
			j = 40 + rand()%(h-80);
			s1.angle = rand()%260;
			s1.x = i;
			s1.y = j;
			affiche_sprite(s1);
			desaffiche_sprite(s2);
			i=40 + rand()%(w-80);
			j = 40 + rand()%(h-80);
			s2.angle = rand()%260;
			s2.x = i;
			s2.y = j;
			affiche_sprite(s2);
			}
			old_distance = view_distance();
			old_angle = view_angle();
			x->value[0] = old_distance;
			//x->value[0] = 0;
			x->value[1] = old_angle;


			if((rand()%10)>10)
			{old_action = 1+ rand()%2;
			//printf("action %f,%f\n",old_action);
			switch((int)old_action)
			{
			case 0:
										
			break;
			case 1:
			avancer_sprite(&s1,10);
			break;
			case 2:
			tourner_sprite(&s1,-5);
			break;
			case 3:
			tourner_sprite(&s1,5);
			break;
									
			}	
			// add_to_mem(old_distance,old_angle,old_action,0);
			old_action = Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action);
			printf("best action : %f\n\n",old_action);
			while((int)old_action==0){

			old_action = 1 + rand()%2;


			}
			}
			else
			{


			//printf("old action : %f\n",old_action);
			switch((int)old_action)
			{
			case 0:
										
			break;
			case 1:
			avancer_sprite(&s1,10);
			break;
			case 2:
			tourner_sprite(&s1,-5);
			break;
			case 3:
			tourner_sprite(&s1,5);
			break;
									
			}
			xp->value[0] = view_distance();
			//xp->value[0] =0;
			xp->value[1] = view_angle();
			// add_to_mem(old_distance,old_angle,old_action,0);
			old_action=Hedger_training(S,x,xp,rewards(),K,H,ALPHA,GAMA,ac,old_action);
			printf("best action : %f\n\n",old_action);
			while((int)old_action==0){

			old_action = 1 + rand()%2;


			}
			}

			t++;
			}

			break;*/
							
	      default:
						
		break;

	      }
	    //printNode(S->tete);

	  }
	//	printf("angle : %f, distande : %f,recompense : %f\n",view_angle(),view_distance(),rewards());
      }
	
  }
  desaffiche_sprite(s1);
  fclose(fp);
  SDL_Quit();
  return 0;

}
