/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   LibRemote.c
 * @brief  Méthodes pour dirigier le wifibot à distance
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * Suppose qu'un serveur de type 'remote.wb' tourne sur le robot.
 */

typedef enum direction {stop,droite0,droite1,droite2,gauche0,gauche1,gauche2,haut0,haut1,haut2,bas0,bas1,bas2} direction ;
typedef enum action {a_stop,a_avance,a_camera,a_recule,a_senshoraire,a_sensantihoraire}action;
typedef enum {
  TopLeft, Top, TopRight, Left, Home, Right, BottomLeft, Bottom, BottomRight, WBSens_Unknown
} WBSens;


int RemoteAvancerCourbe(int v1,int v2);
int RemoteReculerCourbe(int v1,int v2);
int RemoteTournerHoraire(int v1,int v2);
int RemoteTournerAntiHoraire(int v1,int v2);
int RemoteTournerWebcam(int pan,int tilt,WBSens s);
int RemoteStop();
int RemoteConnec();
