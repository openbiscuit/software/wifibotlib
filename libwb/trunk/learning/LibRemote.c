/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   LibRemote.c
 * @brief  Méthodes pour dirigier le wifibot à distance
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * Suppose qu'un serveur de type 'remote.wb' tourne sur le robot.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"

#define CLCK_PER_MILISEC (CLOCKS_PER_SEC/1000)
#define DEFAULT_PORT 6080
#define DEFAULT_IP "192.168.1.115"
#define STDIN 0
#define SIZEIMG 150000
#define SIZE	150000
typedef enum direction {stop,droite0,droite1,droite2,gauche0,gauche1,gauche2,haut0,haut1,haut2,bas0,bas1,bas2} direction ;
typedef enum action {a_stop,a_avance,a_camera,a_recule,a_senshoraire,a_sensantihoraire}action;
typedef enum {
  TopLeft, Top, TopRight, Left, Home, Right, BottomLeft, Bottom, BottomRight, WBSens_Unknown
} WBSens;

int thread_exit;
int sem1,sem2;
pthread_mutex_t mut;
int size_IMG;
unsigned char bufferImg[SIZEIMG];
unsigned char buffer[SIZEIMG];
int socks;
unsigned char img[SIZEIMG];
direction dir=stop;
int isconnect=0;



int send_info(int sk,action ac,int v1,int v2,int sens)
{
  char buffer[10];
	
  sprintf(buffer,"%d\0",ac);
  if(v1<10)
    {
      sprintf(buffer,"%s0%d\0",buffer,v1);
		
    }
  else
    {
      sprintf(buffer,"%s%d\0",buffer,v1);
    }
  if(v2<10)
    {
      sprintf(buffer,"%s0%d\0",buffer,v2);
		
    }
  else
    {
      sprintf(buffer,"%s%d\0",buffer,v2);
    }
  if(sens<10)
    {
      sprintf(buffer,"%s0%d\0",buffer,sens);
		
    }
  else
    {
      sprintf(buffer,"%s%d\0",buffer,sens);
    }
  printf("packet envoyer : %s\n",buffer);
  send(socks,buffer,strlen(buffer),0);
  return 0;
	
}

int RemoteSocket_connect(int port,char *adresse,struct sockaddr_in *serv_addr)
{
  int res;
  struct  hostent         *server;
  if (! (server=gethostbyname(adresse)))
    {printf("erreur DNS\n"); return -1;}
  //  printf("on a l'ip....\n");
  serv_addr->sin_addr.s_addr = inet_addr(adresse);
  serv_addr->sin_port = htons(port);
  serv_addr->sin_family = AF_INET;
  //printf("on a tout connecté\n");
  if((res=socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {perror("socket"); return -1;}
  if (connect(res, (struct sockaddr*)serv_addr, sizeof(*serv_addr)) < 0)
    {perror("connect"); return -1;}
  return res;
}

int RemoteAvancerCourbe(int v1,int v2)
{

  send_info(socks,a_avance,v1,v2,0);
}
int RemoteReculerCourbe(int v1,int v2)
{

  send_info(socks,a_recule,v1,v2,0);
}
int RemoteTournerHoraire(int v1,int v2)
{
  send_info(socks,a_senshoraire,v1,v2,0);
}

int RemoteTournerAntiHoraire(int v1,int v2)
{

  send_info(socks,a_sensantihoraire,v1,v2,0);
}

int RemoteTournerWebcam(int pan,int tilt,WBSens s)
{
  send_info(socks,a_camera,pan,tilt,(int)s);
}
int RemoteStop()
{
  send_info(socks,a_stop,0,0,0);
}
int RemoteConnec()
{
  struct sockaddr_in addr;
  socks = RemoteSocket_connect(DEFAULT_PORT,DEFAULT_IP,&addr);
  isconnect = 1;
}
