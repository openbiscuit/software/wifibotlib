/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   kdtree.c
 * @brief  Manage Kd-Trees.
 *
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * <br><b>French comments can be found in the header file</b>.
 * Used by:
 */


#include "kdtree.h"
#include <stdlib.h>
#include <stdio.h>
#include "math.h"
void KdWriteTree(KdNode *n,FILE *f)
{
KdVectorListp *p;

if(n->isaLeaf)
{
p = n->bag->list->tete;
while(p!=NULL)
{
printVectorIn(f,p->v);
p = p->next;

}

}
else
{
if(n->ChildLeft != NULL)
KdWriteTree(n->ChildLeft,f);

if(n->ChildRight != NULL)
KdWriteTree(n->ChildRight,f);
}


}
void KdSetWeight(KdTree *t,KdVector *v)
{
t->weight = v;

}
void KdSetFile(KdTree *t,FILE *f)
 {
t->f = f;

}
int KdCompareVector(KdVector *v1,KdVector *v2)
{
	int i;

	if(v1->size != v2->size)
	{	

		return 0;
	}
	for(i=0;i<v1->size;i++)
	{if(v1->value[i] != v2->value[i])
	{

		return 0;}
	}

	return 1;


}
void *KdGetVect(KdNode *n,KdVector *x,KdVector **res)
{

	KdVectorListp *p;
	if(n->isaLeaf)
	{
		p=n->bag->list->tete;

		
		while(p!=NULL)
		{
				if(KdCompareVector(p->v,x))
			{
				
			//	printVector(p->v);
		*res = p->v;
				return;
			}


				p = p->next;
		}
		*res =  NULL;
		return;

	}
	else
	{

		if( x->value[n->index_dim] > n->value)
		{KdGetVect(n->ChildRight,x,res);
		if(*res ==NULL)
			KdGetVect(n->ChildLeft,x,res);	
		}

		else
		{KdGetVect(n->ChildLeft,x,res);
		if(*res ==NULL)
			KdGetVect(n->ChildRight,x,res);	
		}
	}

}

int KdLookUp(KdNode *n,KdVector *xq,int k,KdVectorList *l)
{
  return KdLookUpWeighted( n, xq, k, l, NULL);
} 
int KdLookUpWeighted(KdNode *n,KdVector *xq,int k,KdVectorList *l, KdVector *weight)
{
  printf( "On cherche ");
  printVector( xq );
  printf( " -> ");
	KdVectorListp *p;
	if(n->isaLeaf)
	{
	  printf( "Dans feuille de taille %d\n",n->bag->sizeofbag);
		p=n->bag->list->tete;
		while(p!=NULL && l->size <k)
		{
			p->v->dist = KdVectorDistance(p->v,xq,weight);
			// doivent avoir la même action...
			if((p->v->value[p->v->size -1] == xq->value[xq->size -1]))
			  {
			    printf ("\t\t Add vector to list: ");
			    printVector( p->v );
			    printf (" (dist=%f)\n",p->v->dist);
			    KdAddVectorSortByDist(l,p->v);
			  }
			p=p->next;

		}

	}
	else
	{
		if( xq->value[n->index_dim] > n->value)
		  {
		    printf( "Arbre de droite\n");
		    KdLookUpWeighted(n->ChildRight,xq,k,l,weight);
		    if(l->size < k)
		      KdLookUpWeighted(n->ChildLeft,xq,k,l,weight);	
		  }

		else
		  {
		    printf( "Arbre de gauche\n");
		    KdLookUpWeighted(n->ChildLeft,xq,k,l,weight);
		    if(l->size < k)
		      KdLookUpWeighted(n->ChildRight,xq,k,l,weight);
		  }
	}
	return 0;
}
double KdKernel(double d,double h)
{
  double result = exp(-((d/h)*(d/h)));
  printf( "Compute weight (%6.4f,%6.4f) => %6.4f\n", d, h, result);
	
	  return result;
}
double KdVectorDistance(KdVector *v1,KdVector *v2,KdVector *weight)
{
	int i;
	double res=0;
	if(v1->size !=v2->size)
		return 0.0;

	if((weight==NULL)||(weight->size != v1->size))
	{
		for(i=0;i<v1->size;i++)
		{
			res+=(v1->value[i]-v2->value[i])*(v1->value[i]-v2->value[i]);

		}
		res =sqrt(res);
	}
	else
	{
		for(i=0;i<v1->size;i++)
		{
			res+=weight->value[i]*weight->value[i]*(v1->value[i]-v2->value[i])*(v1->value[i]-v2->value[i]);

		}
		res =sqrt(res);
	}

if(res <0)
printf("res : %f\n", res);
	return res;
}
void KdFreeTree(KdTree *t)
{
if(t->weight != NULL)
KdFreeVector(t->weight);
	KdFreeNode(t->tete);
	free(t);

	
}
void KdFreeNode(KdNode *n)
{
	if(n->isaLeaf)
	{
		KdFreeVectorList(n->bag->list->tete);
		free(n->bag->list);
		free(n->bag);
		
	}
	else
	{
		KdFreeNode(n->ChildLeft);
		KdFreeNode(n->ChildRight);
		
	}
	free(n);
	
}
void KdSplitNode(KdNode *n,int nbdim,KdVector *w)
{
	
	KdVectorListp *p;
	n->index_dim = KdMoreRependDim(n->bag,nbdim,w);
//printf("on split sur la dim %d\n",n->index_dim);
	n->value = moy_of_dim(n->bag,n->index_dim);
//printf("avec la valeur : %d\n",n->value);
	n->ChildLeft = KdNewNode();
	n->ChildRight = KdNewNode();
	p = n->bag->list->tete;
	
	while(p!=NULL)
	{
		if(p->v->value[n->index_dim] <= n->value)
		{KdAddVector(n->ChildLeft->bag->list,p->v);
		(n->ChildLeft->bag->sizeofbag)++;
		}
		else
		{KdAddVector(n->ChildRight->bag->list,p->v);
		(n->ChildRight->bag->sizeofbag)++;
		}
		p = p->next;

	}
	KdFreeVectorListWithoutVector(n->bag->list->tete);
	free(n->bag->list);
	free(n->bag);
	n->bag = NULL;
	n->isaLeaf = 0;

}
int KdMoreRependDim(KdBag *b,int nbdim,KdVector *w)
{
	int i,res;
	double *interval_dim=malloc(sizeof(double)*nbdim);

	for(i=0;i<nbdim;i++)
	{
	
		if(w!=NULL)
		interval_dim[i] = ecar_of_dim(b,i,w->value[i]);
		else
		interval_dim[i] = ecar_of_dim(b,i,1.0);
	//	printf("%d int : %f\n",i,interval_dim[i]);
		if(i==0)
		{res = i;
		}
		else
		{
			if(interval_dim[i]>interval_dim[res])
			{

				res = i;
			
			}

		}
	}
	free(interval_dim);

	return res;
}
double min_of_dim(KdBag *b,int dim)
{
	double res;
	KdVectorListp *p = b->list->tete;
	res = p->v->value[dim];
	p = p->next;
	while(p!=NULL)
	{
		if(p->v->value[dim]<res)
			res = p->v->value[dim];
		p = p->next;
	}
	
	
	return res;
}
double ecar_of_dim(KdBag *b,int dim,double w)
{
	double res=0;
	double m;
	KdVectorListp *p = b->list->tete;
	res = (p->v->value[dim])*(p->v->value[dim]);
	p = p->next;
	while(p!=NULL)
	{

			res += (p->v->value[dim] )*(p->v->value[dim]);
		p = p->next;
	}
	
//printf("%f    %f\n",res,m*m);
	m = moy_of_dim(b,dim);
	res = sqrt(w*w*(((res)/b->sizeofbag)-m*m));

	return res;
}
double moy_of_dim(KdBag *b,int dim)
{
	double res=0;
	KdVectorListp *p = b->list->tete;
	res = p->v->value[dim];
	p = p->next;
	while(p!=NULL)
	{
		res += p->v->value[dim];
		p = p->next;
	}
	res = res/(b->sizeofbag);
	return res;
}

double max_of_dim(KdBag *b,int dim)
{
	double res;
	KdVectorListp *p = b->list->tete;
	res = p->v->value[dim];
	p = p->next;
	while(p!=NULL)
	{
		if(p->v->value[dim]>res)
			res = p->v->value[dim];
		p = p->next;
	}
	return res;
}

KdNode *KdNewNode(){
	KdNode *res = malloc(sizeof(KdNode));
	res->ChildLeft = NULL;
	res->ChildRight = NULL;
	res->index_dim = 0;
	res->value = 0;
	res->isaLeaf = 1;
	res->bag = malloc(sizeof(KdBag));
	res->bag->sizeofbag = 0;
	res->bag->list = KdNewVectorList();
	return res;
}

int KdAddVectorToTree(KdTree *t,KdVector *v)
{
	KdNode *CurrentNode = t->tete;
	if(v->size != t->dim)
		return -1;


	while(!(CurrentNode->isaLeaf))
	{
		if(v->value[CurrentNode->index_dim] <= CurrentNode->value)
			CurrentNode = CurrentNode->ChildLeft;
		else
			CurrentNode = CurrentNode->ChildRight;
	}
	
	if(KdAddVectorOrMaj(CurrentNode->bag->list,v))
		{(CurrentNode->bag->sizeofbag)++ ;
/*		if(t->f!=NULL)
	printVectorIn(t->f,v);
*/
}

	if(CurrentNode->bag->sizeofbag >= t->limit_of_bag)
	{
		//printf("on le split lui:\n");
	//	printBag(CurrentNode->bag);

		KdSplitNode(CurrentNode,t->dim,t->weight);

	}
	return 0;
}


KdTree *KdNewTree(int dim,int limit_of_bag)
{
	KdTree *res = malloc(sizeof(KdTree));
	res->dim = dim;
	res->limit_of_bag = limit_of_bag;
	res->tete = KdNewNode();
	res->f = NULL;
	res->weight = NULL;
	return res;

}
KdVector *KdNewVector(int size)
{
	KdVector *v= malloc(sizeof(KdVector));
	v->value = malloc(sizeof(double)*size);
	v->size = size;
	v->key = 0.0;
	v->dist = 0.0;
	v->weight = 0.0;
	return v;
}
void printVector(KdVector *v)
{
	int i;
	for(i=0;i<v->size;i++)
	{
		printf("%f  ",v->value[i]);
	}
	printf("key : %f (%6.4f/%6.4f)",v->key, v->dist, v->weight);
}
void printVectorIn(FILE *f,KdVector *v)
{
	int i;
	for(i=0;i<v->size;i++)
	{
		fprintf(f,"%f  ",v->value[i]);
	}
	fprintf(f,"%f  (%6.4f/%6.4f)\n",v->key, v->dist, v->weight);
}
void printListVector(KdVectorList *v)
{

	KdVectorListp *p;
	p = v->tete;
	while(p!=NULL)
	{
		printVector(p->v);
		printf("\n");
		p=p->next;

	}

}
KdVectorListp *KdNewVectorListp(KdVector *v,KdVectorListp *next,KdVectorListp *prec)
{
	KdVectorListp *kvl=malloc(sizeof(KdVectorListp));
	kvl->v = v;
	kvl->next=next;
	kvl->prec = prec;
	return kvl;
}


void KdSetNext(KdVectorListp *kvl,KdVectorListp *next)
{
	kvl->next = next;
}

void KdSetPrec(KdVectorListp *kvl,KdVectorListp *prec)
{
	kvl->prec = prec;
}

void KdFreeVector(KdVector *v)
{
	free(v->value);
	free(v);

}
void KdFreeVectorList(KdVectorListp *l)
{
	if(l!=NULL)
	{
		KdFreeVector(l->v);
		KdFreeVectorList(l->next);
		free(l);
	}
}
void KdFreeVectorListWithoutVector(KdVectorListp *l)
{
	if(l!=NULL)
	{
//KdFreeVector(l->v);
		KdFreeVectorListWithoutVector(l->next);
		free(l);
	}
}
int KdAddVectorOrMaj(KdVectorList *l,KdVector  *v)
{
	int trouve=0;
	KdVectorListp *p;
	KdVectorListp *p2 = l->tete;
	p = KdNewVectorListp(v,NULL,NULL);
	if((l->tete) == NULL)
	{
	
		l->tete = p;
		l->queue = p;


	}
	else
	{
		p2 = l->tete;
		while(p2!=NULL)
		{
			if(KdCompareVector(p2->v,v))
			{
				p2->v->key = v->key;
				p2=NULL;
				trouve=1;
			}
			if(p2!=NULL)
			{
				p2=p2->next;
			}


		}

		if(trouve==0)
		{
			l->queue->next = p;
			l->queue = p;
		}
	}
	if(!trouve)
	{	l->size++; return 1;}
	else
	{return 0;}
	

}
void KdAddVector(KdVectorList *l,KdVector  *v)
{
	KdVectorListp *p;
	p = KdNewVectorListp(v,NULL,NULL);
	if((l->tete) == NULL)
	{
	
		l->tete = p;
		l->queue = p;


	}
	else
	{

		l->queue->next = p;
		l->queue = p;

	}
	l->size ++;

}

void KdAddVectorSortByDist(KdVectorList *l,KdVector  *v)
{
	KdVectorListp *kdvlp;
	KdVectorListp *p = l->tete;
	kdvlp = KdNewVectorListp(v,NULL,NULL);
	if((l->tete) == NULL)
	{
	
		l->tete = kdvlp;
		l->queue = kdvlp;


	}
	else
	{
		while(p->next !=NULL && v->dist > p->next->v->dist)
		{
			p = p->next;
		}
		kdvlp->next = p->next;
		p->next = kdvlp;
		
	}
	l->size ++;

}
KdVectorList *KdNewVectorList()
{
	KdVectorList *res = malloc(sizeof(KdVectorList));
	res->tete = NULL;
	res->size=0;
	res->queue = NULL;

	return res;

}

void printBag(KdBag *b){
	printListVector(b->list);
	
}

void printNode(KdNode *n)
{
	if(n->isaLeaf)
	{
		printf("feuille : \n");
		printBag(n->bag);
		
	}
	else
	{
		printf("\t%f\n",n->value);
		printf("\t\t");
		printNode(n->ChildLeft);
		printf("\t\t");
		printNode(n->ChildRight);
		
	}
}
/* Ajouté par Alain */
int getTotalSize(KdNode *n)
{
  if(n->isaLeaf)
    {
      return (n->bag->list->size);
    }
  else
    {
      return (getTotalSize(n->ChildLeft) + getTotalSize(n->ChildRight));
    }
}
int debugGetData(KdNode *n, int index, double *xdata, double *ydata, int *indexLeaf)
{
  int i = index;
  KdVectorListp *vp;
  KdBag *b;

  if(n->isaLeaf)
    {
      //printf( "Adding a Bag...\n");
      b = n->bag;
      // add vectors in bag to data
      vp = b->list->tete;
      while(vp != NULL )
	{
	  //printf( "Add vector at %d : ",i);
	  //printVector( vp->v );
	  //printf ("\n" );
	  xdata[i] = vp->v->value[0];
	  ydata[i] = vp->v->key;
	  indexLeaf[i]=n->value;
	  i++;
	  vp = vp->next;
	}
      return i;
    }
  else
    {
      //printf( "Visiting ChildLeft\n");
      i = debugGetData(n->ChildLeft, index, xdata, ydata, indexLeaf);
      //printf( "Visiting ChildRight\n");
      i = debugGetData(n->ChildRight, i, xdata, ydata, indexLeaf);
      
      return i;
    }
}
double KdFindClosestInTree( KdTree *tree, KdVector *target, KdVector **closest )
{
  int i;
  KdVector *hyperRectMin, *hyperRectMax;

  // set up limits of hyperRect
  hyperRectMin = KdNewVector( target->size );
  hyperRectMax = KdNewVector( target->size );
  for( i = 0; i< target->size; i++ ) {
    hyperRectMin->value[i] = -1000000.0;
    hyperRectMax->value[i] =  1000000.0;
  }

  return KdFindClosestInNode( tree->tete, target, closest,
			    hyperRectMin, hyperRectMax, 1000000.0 );  
}
// Inspired by [Moore91]
double KdFindClosestInNode(KdNode *node, KdVector *target, KdVector **closest,
		       KdVector *hyperRectMin, KdVector *hyperRectMax,
		       double maxDist )
{
  double thisMin, thisMax;
  double otherMin, otherMax;
  KdNode *other;
  double distance, tmpDistance, distSplit;
  KdVector *pivot;
  int i;
  KdVectorListp *listV;

  if( node->isaLeaf ) {
    distance = maxDist;
    // look through all the points
    listV = node->bag->list->tete;
    while(listV != NULL ) {
      tmpDistance = KdVectorDistance( target, listV->v, NULL /* No Weight */);
      if( tmpDistance < distance ) {
	distance = tmpDistance;
	*closest = listV->v;
      }
      listV = listV->next;
    }
    return distance;
  }
  else {
    // store current hyperRect
    thisMin = hyperRectMin->value[node->index_dim];
    thisMax = hyperRectMax->value[node->index_dim];
    
    // in LeftChild
    if( target->value[node->index_dim] <= node->value ) {
      other = node->ChildRight;
      // store values of this and other hyperRect for the dimension of split
      
      otherMin = node->value;
      otherMax = hyperRectMax->value[node->index_dim];
      // recursive call
      hyperRectMax->value[node->index_dim] = node->value;
      distance = KdFindClosestInNode( node->ChildLeft, target, closest,
				    hyperRectMin, hyperRectMax, maxDist );
    }
    else { // in RightChild
      other = node->ChildLeft;
      // store values of this and other hyperRect for the dimension of split
      otherMin = hyperRectMin->value[node->index_dim];
      otherMax = node->value;
      thisMin = node->value;
      thisMax = hyperRectMax->value[node->index_dim];
      // recursive call
      hyperRectMin->value[node->index_dim] = node->value;
      distance = KdFindClosestInNode( node->ChildRight, target, closest,
				    hyperRectMin, hyperRectMax, maxDist );
    }
    
    
    // check if other hyperRect could contain a better point
    // i.e. some parts of hyperRect lies closer of closest.
    //
    // new hyperRect
    hyperRectMin->value[node->index_dim] = otherMin;
    hyperRectMax->value[node->index_dim] = otherMax;
    // closest point on split line
    pivot = KdNewVector( target->size );
    for( i = 0; i < pivot->size; i++ ) {
      if( target->value[i] <= hyperRectMin->value[i] ) {
	pivot->value[i] = hyperRectMin->value[i];
      }
      else if( target->value[i] < hyperRectMax->value[i] ) {
	pivot->value[i] = target->value[i];
      }
      else {
	pivot->value[i] = hyperRectMax->value[i];
      }
    }
    
    // split is close enough ?
    distSplit = KdVectorDistance( target, pivot, NULL /* No Weight */);
    if( distSplit < distance ) {
      distance = KdFindClosestInNode( other, target, closest,
				       hyperRectMin, hyperRectMax, distance );
    }

    // restore hyperRect
    hyperRectMin->value[node->index_dim] = otherMin;
    hyperRectMax->value[node->index_dim] = otherMax;

    // clean up
    KdFreeVector( pivot );
    
    return distance;  
  }
}

