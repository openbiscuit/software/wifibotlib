##
## libwb - A library for low and medium level control of a WifiBot.
##
## Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
##			Maia Team, LORIA.
##
## Original authors: Nicolas Beaufort
##
## Mail: Alain.Dutech@loria.fr
## Web: http://maia.loria.fr
##  
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##  
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##  
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
##  


## @file   kdtree.py
## @brief  Interface with 'python' for kdtree
##
## @author Nicolas Beaufor
## @author Alain Dutech
## @date   Thu Apr 19 13:24:32 2007
## 


#
# Some 'python' functions and declaration for using and debugging KdTree
# as coded in kdtree.c and kdtree.h
# 
# kdtree must be compiled as a shared library
#   gcc -c kdtree.c
#   gcc -shared -Wl,-soname,libkdtree.so.1 -o libkdtree.so.1 kdtree.o -lm
#
# Under 'ipython', I just use
# >>> run kdtree.py
# >>> tree = libkdtree.KdNewTree(2, 10)
# >>> fillKdTree( tree, 15, listV )
# >>> plotTreeG( tree, 0)
#

# ctypes is important for binding C to PYTHON
import ctypes
from ctypes import *

# mathematical libs
import random
import math

# for displaying graphs
import numpy
import pylab
import matplotlib.axes3d as plot3

# for exception
import sys

# IMPORTANT : name of the library ----------------------<<<< IMPORTANT >>>>
libkdtree = ctypes.CDLL("libkdtree.so.1")
#libkdtree = ctypes.CDLL("libkdtree")

# -----------------------------------------------------------
# Definition des structures utilisees dans [kdtree.h]
#
class KdVector(Structure):
	_fields_ = [("size", c_int),
	            ("value", POINTER(c_double)),
		    ("key", c_double),
                    ("dist", c_double),
                    ("weight", c_double)]

class KdVectorListp(Structure):
	pass

KdVectorListp._fields_ = [("v", POINTER(KdVector)), ("next", POINTER(KdVectorListp)), ("prec", POINTER(KdVectorListp))]

class KdVectorList(Structure):
	_fields_ = [("tete", POINTER(KdVectorListp)), ("queue", POINTER(KdVectorListp)), ("size", c_int)]

class KdBag(Structure):
	_fields_ = [("sizeofbag", c_int), ("list", POINTER(KdVectorList))]

class KdNode(Structure):
	pass

KdNode._fields_ = [("ChildLeft", POINTER(KdNode)), ("ChildRight", POINTER(KdNode)), ("index_dim", c_int), ("value", c_double), ("isaLeaf", c_int), ("bag", POINTER(KdBag))]

class KdTree(Structure):
	_fields_ = [("tete",POINTER(KdNode)), ("dim", c_int), ("limit_of_bag", c_int), ("qmin", c_double), ("qmax", c_double), ("rmin", c_double), ("rmax", c_double), ("weigth", POINTER(KdVector)), ("f", c_void_p)]

# special function signature
libkdtree.KdNewTree.restype = POINTER(KdTree)
libkdtree.KdNewVectorList.restype = POINTER(KdVectorList)
libkdtree.KdFindClosestInTree.restype = c_double
libkdtree.KdFindClosestInTree.argtypes = [POINTER(KdTree), POINTER(KdVector),
					  POINTER(POINTER(KdVector))]
libkdtree.KdKernel.restype = c_double
libkdtree.KdNewVector.restype = POINTER(KdVector)
#libkdtree.printVector.argtypes = [POINTER(KdVector)]
# -----------------------------------------------------------


def newKdVectorRand( dim ):
	""" Generate a random KdVector with 'dim+1' coeficients.
	action (last coef) is set to 0.
	"""
	tmval = (c_double * (dim+1))()
	for i in range(len(tmval)) :
		tmval[i] = c_double(random.uniform(-2,2))
	tmval[dim] = c_double(0.0)
	tmkey = math.cos(tmval[0]*3) * math.sin(tmval[0])
	return KdVector( c_int(dim+1), tmval, c_double(tmkey), c_double(0), c_double(1))

def newKdVectorTuple( data ):
	""" Generate a KdVector from the given tuple data.
	"""
	dim = len(data)
	tmval = (c_double * dim)()
	for i in range(dim) :
		tmval[i] = c_double(data[i])
	return KdVector( c_int(dim), tmval, c_double(0.0), c_double(0.0), c_double(1.0) )


def fillKdTree( tree, nb, listVec ) :
	""" Add 'nb' random KdVector to KdTree 'tree'.
	'listVec' is essential for locally generated KdVector to exist
	outside the function (Python has a garbage collector).
	"""
	for i in range(0,nb):
		v = newKdVectorRand(2)
		#libkdtree.printVector( byref(v))
		#print '\n'
		libkdtree.KdAddVectorToTree( tree, pointer(v))
		#libkdtree.printNode( tree.contents.tete)
		#print '***** Added vector %d' % i
		listVec.append(v)

def printTree( tree) :
	""" Print the elements of a KdTree."""
	print "KdTree de dimension %d" % tree.contents.dim
	libkdtree.printNode( tree.contents.tete )

# Graphical
def plotKernel( limits=(-2,2), precision=0.05, h=0.5):
	""" Display (graph) the weight (KdKernel) of kdlearn.
	"""
	tabx = numpy.arange( limits[0], limits[1]+precision, precision)
	taby = []
	for x in tabx:
		taby.append( libkdtree.KdKernel( c_double(x), c_double(h)))
	pylab.plot( tabx, taby )
	pylab.show()
	
	
def plotTree( tree ) :
	""" Display (graph) the elements of a KdTree.
	This uses internal functions 'getTotalSize()' and 'debugGetData()'.
	"""
	treeSize = libkdtree.getTotalSize( tree.contents.tete )
	#xdata = ( (c_double * tree.contents.dim) * treeSize)()
	xdata = (c_double * treeSize)()
	ydata = (c_double * treeSize)()
	indexLeaf = (c_int * treeSize)()
	libkdtree.debugGetData( tree.contents.tete, 0, xdata, ydata, indexLeaf )

	x = []
	for e in range(len(xdata)) :
		x.append(xdata[e])
	y = []
	for e in range(len(ydata)) :
		y.append(ydata[e])

	pylab.plot( x, y, '+')
	pylab.show()


def plotNodeG( node, dim=0 ) :
	""" Display (graphic) a KdNode recursively (key=f(value[dim])).
	"""
	if node.contents.isaLeaf == 1:
		# plot Bag
		listVecP = node.contents.bag.contents.list.contents.tete	
		print "*** Feuille\n"
		x = []
		y = []
		# teste si le vecteur pointe sur NULL
		while( bool(listVecP) ):
			#print listVecP
			#print "\n"
			libkdtree.printVector( listVecP.contents.v )
			print "\n"

			val = listVecP.contents.v.contents.value
			#print "Valeur desiree %f\n" % val[dim]
			
			x.append( listVecP.contents.v.contents.value[dim] )
			y.append( listVecP.contents.v.contents.key )

			listVecP = listVecP.contents.next
		pylab.plot( x, y , '+')
		
	else:
		plotNodeG( node.contents.ChildLeft, dim )
		plotNodeG( node.contents.ChildRight, dim )


def plotTreeG( tree, dim=0 ) :
	""" Display (grpahic) a KdTree (key=f(value[dim]))."""
	pylab.ioff()
	plotNodeG( tree.contents.tete, dim )
	pylab.show()
	pylab.ion()

def plotNode2DG( node, dim1=0, dim2=1) :
	""" Display (graphic) 2 dimensions of elements of KdNode recursively.
	y=value[dim2], x=value[dim1].
        """
	if node.contents.isaLeaf == 1:
		# plot Bag
		listVecP = node.contents.bag.contents.list.contents.tete	
		print "*** Feuille\n"
		x = []
		y = []
		# teste si le vecteur pointe sur NULL
		while( bool(listVecP) ):
			#print listVecP
			#print "\n"
			libkdtree.printVector( listVecP.contents.v )
			print "\n"

			val = listVecP.contents.v.contents.value
			#print "Valeur desiree %f\n" % val[dim]
			
			x.append( listVecP.contents.v.contents.value[dim1] )
			y.append( listVecP.contents.v.contents.value[dim2] )

			listVecP = listVecP.contents.next
		pylab.plot( x, y , '+')
		
	else:
		plotNode2DG( node.contents.ChildLeft, dim1, dim2 )
		plotNode2DG( node.contents.ChildRight, dim1, dim2 )

def plotTree2DG( tree, dim1=0, dim2=1 ) :
	""" Display (grpahic) a KdTree (y=value[dim2], x=value[dim1]).
	"""
	pylab.ioff()
	plotNode2DG( tree.contents.tete, dim1, dim2 )
	pylab.show()
	pylab.ion()

def plotNodeSplit2DG( node, dimx=0, dimy=1, limits=(-1,1,-1,1)) :
	""" Draw the line split for the node within 'limits' (xmin,xmax,ymin,ymax).
        """
	if node.contents.isaLeaf == 1:
		# Bag -> do Nothing
		pass
	else:
		xmin,xmax,ymin,ymax = limits
		if node.contents.index_dim == dimx :
			pylab.plot( [node.contents.value, node.contents.value],
				    [ymin, ymax],
				    'k-')
			plotNodeSplit2DG( node.contents.ChildLeft, dimx, dimy,
					  (xmin,node.contents.value,ymin,ymax))
			plotNodeSplit2DG( node.contents.ChildRight, dimx, dimy,
					  (node.contents.value,xmax,ymin,ymax))
		elif node.contents.index_dim == dimy :
			pylab.plot( [xmin, xmax],
				    [node.contents.value, node.contents.value],
				    'k-')
			plotNodeSplit2DG( node.contents.ChildLeft, dimx, dimy,
					  (xmin,xmax,ymin,node.contents.value))
			plotNodeSplit2DG( node.contents.ChildRight, dimx, dimy,
					  (xmin,xmax,node.contents.value,ymax))
		else :
			
			plotNodeSplit2DG( node.contents.ChildLeft, dimx, dimy,
					  limits)
			plotNodeSplit2DG( node.contents.ChildRight, dimx, dimy,
					  limits)
			
def plotTreeSplit2DG( tree, dimx=0, dimy=1, limits=(-1,1,-1,1) ) :
	""" Draw the line split for the node within 'limits' (xmin,xmax,ymin,ymax).
	"""
	pylab.ioff()
	plotNodeSplit2DG( tree.contents.tete, dimx, dimy, limits )
	pylab.show()
	pylab.ion()			

def plotTree3DG( tree, dimx=0, dimy=1, limits=(-1,1,-1,1), precision=(0.1,0.1) ) :
	""" Draw the tree within 'limits' (xmin,xmax,ymin,ymax),
	with 'precision' (precX,precY).
	"""
	xx = numpy.arange(limits[0], limits[1]+precision[0], precision[0])
	yy = numpy.arange(limits[2], limits[3]+precision[1], precision[1])
			  
	x,y = numpy.meshgrid(xx,yy)
	z = numpy.zeros((len(yy), len(xx)), 'Float32')
	addPointsFromNode( tree.contents.tete, dimx, dimy, limits, precision, z)
	fig = pylab.figure()
	ax = plot3.Axes3D(fig)
	try:
		ax.plot_wireframe(x, y, z)
	except IndexError:
		print x,y,z
	#ax.plot_surface(x,y,z)
	#ax.contour3D(x,y,z)
	ax.set_xlabel("dim=%d" % dimx )
	ax.set_ylabel("dim=%d" % dimy )
	ax.set_zlabel("QVal")
	#pylab.title( "X=dim[%d], Y=dim[%d], Z=Qval" % dimx,dimy )
	pylab.show()

def addPointsFromNode( node, dimx, dimy, limits, precision, tabZ ) :
	if node.contents.isaLeaf == 1:
		# Bag -> add points
		# plot Bag
		listVecP = node.contents.bag.contents.list.contents.tete	
		print "*** Feuille\n"
		x = []
		y = []
		# teste si le vecteur pointe sur NULL
		while( bool(listVecP) ):
			#print listVecP
			#print "\n"
			libkdtree.printVector( listVecP.contents.v )
			print "\n"

			#val = listVecP.contents.v.contents.value
			#print "Valeur desiree %f\n" % val[dim]
			
			x = listVecP.contents.v.contents.value[dimx]
			y = listVecP.contents.v.contents.value[dimy]
			if inLimits(x, y, limits) :
				tabZ[(y - limits[2])/precision[1],
				     (x - limits[0])/precision[0]] = listVecP.contents.v.contents.key

			listVecP = listVecP.contents.next
	else :
		addPointsFromNode( node.contents.ChildLeft, dimx, dimy, limits, precision, tabZ)
		addPointsFromNode( node.contents.ChildRight, dimx, dimy, limits, precision, tabZ)

def plotTree3DGFun( tree, fun, limits=(-1,1,-1,1), precision=(0.1,0.1), titleStr="Qval") :
	""" Draw the tree within 'limits' (xmin,xmax,ymin,ymax),
	with 'precision' (precX,precY).
	"""
	xx = numpy.arange(limits[0], limits[1]+precision[0], precision[0])
	yy = numpy.arange(limits[2], limits[3]+precision[1], precision[1])
			  
	x,y = numpy.meshgrid(xx,yy)
	z = numpy.zeros((len(yy), len(xx)), 'Float32')
	addPointsFromNodeFun( tree.contents.tete, limits, precision, z, fun)
	fig = pylab.figure()
	ax = plot3.Axes3D(fig)
	try:
		ax.plot_wireframe(x, y, z)
		#ax.plot_surface(x,y,z)
		
	except:
		print "Unexpected error:", sys.exc_info()[:2]
		print "x=",x.shape,"\n",x
		print "y=",y.shape,"\n",y
		print "z=",z.shape,"\n",z
		raise
	#
	#ax.contour3D(x,y,z)
	ax.set_xlabel("X")
	ax.set_ylabel("Y")
	ax.set_zlabel("QVal")
	pylab.title( titleStr)
	pylab.show()


def addPointsFromNodeFun( node, limits, precision, tabZ, fun):
	if node.contents.isaLeaf == 1:
		# Bag -> add points
		# plot Bag
		listVecP = node.contents.bag.contents.list.contents.tete	
		print "*** Feuille\n"
		# teste si le vecteur pointe sur NULL
		while( bool(listVecP) ):
			#print listVecP
			#print "\n"
			libkdtree.printVector( listVecP.contents.v )
			print "\n"

			#val = listVecP.contents.v.contents.value
			#print "Valeur desiree %f\n" % val[dim]
			fun( listVecP.contents.v, tabZ, limits, precision )
			listVecP = listVecP.contents.next
	else :
		addPointsFromNodeFun( node.contents.ChildLeft, limits, precision, tabZ, fun)
		addPointsFromNodeFun( node.contents.ChildRight, limits, precision, tabZ, fun)

def plotTree3DGVal( tree, fun, limits=(-1,1,-1,1), precision=(0.1,0.1), titleStr="interpol Qval") :
	""" Draw QVal for each point within the limits.
	"""
	xx = numpy.arange(limits[0], limits[1]+precision[0], precision[0])
	yy = numpy.arange(limits[2], limits[3]+precision[1], precision[1])
			  
	x,y = numpy.meshgrid(xx,yy)
	z = numpy.zeros((len(yy), len(xx)), 'Float32')

	dimi,dimj = z.shape
	for i in range(0,dimi):
		for j in range(0,dimj):
			z[i,j] = fun(x[i,j],y[i,j])
	
	fig = pylab.figure()
	ax = plot3.Axes3D(fig)
	try:
		ax.plot_wireframe(x, y, z)
	except:
		print "Unexpected error:", sys.exc_info()[:2]
		print x
		print y
		print z
		raise
	#ax.plot_surface(x,y,z)
	#ax.contour3D(x,y,z)
	ax.set_xlabel("X")
	ax.set_ylabel("Y")
	ax.set_zlabel("QVal")
	pylab.title( titleStr)
	pylab.show()
	

def inLimits(x, y, limits):
	""" True if x,y within limits.
	"""
	if x > limits[0] and x < limits[1] and y > limits[2] and y < limits[3] :
		return True
	else :
		return False

# --------------------------------------------------------
# Can be used for init
if __name__ == "__main__":
	listV = []
	tree = libkdtree.KdNewTree(3, 10)
	fillKdTree( tree, 200, listV )

	xdata = ( (c_double * 2) * 20)()
	ydata = (c_double * 20)()
	indexLeaf = (c_int * 20)()
# --------------------------------------------------------



