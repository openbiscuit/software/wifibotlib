/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   traitement_image.c
 * @brief  Méthodes pour trouver la cible dans l'image.
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * La function principale est 'chercher_cible()' qui renvoie une structure
 * de type 'target'.
 */
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "jinclude.h"
#include "traitement_image.h"
#include "jpeglib.h"
#include "jerror.h"
#define SIZE	5000
#define TMAX 255
#define INC 1
#define color_red 224
#define color_blue 128
#define seuil_blanc 180
#define seuil_noir 32
#define color_yellow 32
#define profondeur_max 30000
/* Expanded data source object for tab input */

typedef struct point{
  int x;int y;}point;

typedef struct trapez {
  point lu;point ld; point ru; point rd;
  point luint;point rdint;
  unsigned char color;}trapez;
typedef struct Tpx{
  unsigned char valeur;
  char traite;
  unsigned char iswhite;
  unsigned char isblack;}Tpx;
	
typedef struct {
  struct jpeg_source_mgr pub; /* public fields */
  JOCTET * buffer;	/* Start of buffer */
  int nb_bytes;
  boolean start_of_file;
} tab_source_mgr;

typedef tab_source_mgr * tab_src_ptr;


/*
 * Initialise source --- called by jpeg_read_header
 * before any data is actually read
 */
METHODDEF(void) init_source(j_decompress_ptr cinfo)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src; 
  /* We reset the empty-input-file flag for each image,
   * but we don't clear the input buffer.
   * This is correct behavior for reading a series of images from one source.
   */
  src->start_of_file = TRUE;
}




METHODDEF(boolean) fill_input_buffer(j_decompress_ptr cinfo)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src;
  src->pub.next_input_byte = src->buffer;
  src->pub.bytes_in_buffer = src->nb_bytes;
  src->start_of_file = FALSE;

  return TRUE;
}



METHODDEF(void) skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src;
  if(num_bytes > 0)
    {
      src->pub.next_input_byte += (size_t) num_bytes;
      src->pub.bytes_in_buffer -= (size_t) num_bytes;
    }
}


METHODDEF(void) term_source(j_decompress_ptr cinfo)
{
  /* void */
}


/*
 * Source manager à partir d'un tableau déjà chargé en mémoire.
 * Il suffit d'indiquer l'adresse du tableau, ainsi que sa taille,
 * pour ensuite pouvoir décompresser l'image stockée dans ce tableau.
 * Une fois l'image décompressée on peut travailler dessus.
 */
GLOBAL(void) jpeg_tab_src(j_decompress_ptr cinfo, JOCTET *buffer, int nbytes)
{
  tab_src_ptr src;
  if(cinfo->src == NULL)
    {
      cinfo->src = (struct jpeg_source_mgr *)
	(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
				    SIZEOF(tab_source_mgr));
      src = (tab_src_ptr) cinfo->src;
    }

  src = (tab_src_ptr) cinfo->src;
  src->pub.init_source = init_source;
  src->pub.fill_input_buffer = fill_input_buffer;
  src->pub.skip_input_data = skip_input_data;
  src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src->pub.term_source = term_source;
  src->nb_bytes = nbytes;
  src->buffer = buffer;
  src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
  src->pub.next_input_byte = NULL; /* until buffer loaded */

}

int socket_connect(int port,char *adresse,struct sockaddr_in *serv_addr)
{
  int res;
  struct  hostent         *server;
  if (! (server=gethostbyname(adresse)))
    {printf("erreur DNS\n"); return -1;}
  //  printf("on a l'ip....\n");
  serv_addr->sin_addr.s_addr = inet_addr(adresse);
  serv_addr->sin_port = htons(port);
  serv_addr->sin_family = AF_INET;
  //printf("on a tout connecté\n");
  if((res=socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {perror("socket"); return -1;}
  if (connect(res, (struct sockaddr*)serv_addr, sizeof(*serv_addr)) < 0)
    {perror("connect"); return -1;}
  return res;
}



int GetImage(unsigned char *buffer)
{
  unsigned char buf_rec[SIZE];
  int img_size = 0;
  int retval, i,fin;
  int sk;
  struct sockaddr_in addr;
  sk = socket_connect(80,"192.168.1.115",&addr);
  printf("on est conecté et on demande l'image\n");
  send(sk, "GET /IMAGE.JPG HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n", 128, 0);

  /* On commence � r�cup�rer l'image */
  retval = recv(sk, buf_rec, SIZE, 0);
  fin = 0;
  while(retval != 0 && !fin)
    {
      /* On copie les donn�es du buffer dans le buffer final */
      memcpy(buffer+img_size, buf_rec, retval);
      img_size += retval;
				
      for(i=img_size-retval-1;i<img_size-1;i++)
	{
	  if(buffer[i] == 0xFF && buffer[i+1] == 0xD9)
	    {
	      printf("on chope la fin de l'image\n");
	      fin =1;
	    }
	}
      if(!fin)
	retval = recv(sk, buf_rec, SIZE, 0);
    }

  /* Le d�but du buffer est pollu� par les headers HTTP */
  for(i=0; i<img_size-1; i++)
    {
      // une image JPEG commence par 0xFFD8
      if(buffer[i] == 0xFF && buffer[i+1] == 0xD8)
	{
	  img_size -= i;
	  memmove(buffer, buffer+i, img_size);
	  break;
	}
    }

  close(sk);
  /* L'image est enti�rement dans buffer, et fait img_size octets */
  return img_size;
}



int WriteImage(char *fichier, unsigned char *img, int taille, int width, int height)
{
  FILE *fp;
  int i;

  fp = fopen(fichier, "w");
  if(fp == NULL)
    {
      fprintf(stderr, "Erreur : impossible d'ouvrir le fichier %s\n", fichier);
      return(1);
    }
  // on �crit les entetes ppm (24 bits RVB)
  fprintf(fp, "P6 %d %d 255 ", width, height);
  // on copie le tableau dans le fichier
  for(i=0; i<taille; i++)
    {
      fprintf(fp, "%c", img[i]);
    }
	
  return fclose(fp);
} /* ecrireImage() */



/* 
 * D�compresse une image contenue dans un buffer.
 *  - image		: double pointeur sur un buffer qui contiendra l'image d�compress�e (non allou�)
 *  - buffer	: buffer contenant l'image compress�e
 *  - taille	: taille en octets de l'image compress�e
 * Renvoie la taille de l'image d�compres�e (en octets)
 * Met � jour : width et height.
 */

int DecompressImage(unsigned char *image, unsigned char *buffer, int taille,
		    int *width, int *height)
{
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
  unsigned char *ligne;
	
  /* Initialisation de la structure d'info */
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);

  /* On lui dit "d�compresse ce qu'il y a dans le buffer l�" */
  jpeg_tab_src(&cinfo, buffer, taille);

  /* On r�cup�re les infos de l'image et on d�compresse */
  jpeg_read_header(&cinfo, TRUE);
  jpeg_start_decompress(&cinfo);

  *height = cinfo.output_height;
  *width = cinfo.output_width;

  /* Buffer o� l'image d�compress�e sera plac�e */
  //printf("Allocation m�moire image (%d)\n", width*height*3*sizeof(char));
  //*image = malloc(height*width*3*sizeof(unsigned char));
  ligne = image;

  /* Tant qu'il y a des scanlines, on les place dans le buffer */
  while(cinfo.output_scanline < (*height))
    {
      ligne = image + 3 * (*width) * cinfo.output_scanline;
      jpeg_read_scanlines(&cinfo, &ligne, 1);
    }

  /* On termine proprement */
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  return 3* (*height) * (*width);
} /* decompresseImage() */


int pixeliser(unsigned char *entre,int width,int height,unsigned char *res,int startwidth,int endwidth,int startheight,int endheight,int sizeofpixel,int center_inf,int center_sup,int sizeofpixelcenter)
{
  int i,j,x,z;
  int i2=0,j2=0;
  int moyr,moyg,moyb;
  printf("width : %d height: %d\n",endwidth,endheight);
  for(i=startwidth;i<center_inf;i+=sizeofpixel)
    {
      j2=0;
      //printf("i : %d\n",i);
      for(j=startheight;j<endheight;j+=sizeofpixel)
	{
	  //printf("j : %d\n",j);
	  moyr = 0;
	  moyg = 0;
	  moyb = 0;
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
						
		  moyr += entre[z*width*3+x*3];	
		  moyg += entre[z*width*3+x*3+1];	
		  moyb += entre[z*width*3+x*3+2];	
						
		}

	    }	
	  //	printf("%d %d %d %d\n",i,j,x,z);
	  moyr = moyr/(sizeofpixel * sizeofpixel);
	  moyg = moyg/(sizeofpixel * sizeofpixel);
	  moyb = moyb/(sizeofpixel * sizeofpixel);
	  /*res[j2][i2][0] = (unsigned char)moyr;
	    res[j2][i2][1] = (unsigned char)moyg;
	    res[j2][i2][2] = (unsigned char)moyb;
	  */
				
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  res[z*width*3+x*3] = moyr;
		  res[z*width*3+x*3+1] = moyg;
		  res[z*width*3+x*3+2] = moyb;
		}
	    }
				







	  j2++;
	}
      i2++;



    }
			
  for(i=center_inf;i<center_sup;i+=sizeofpixelcenter)
    {
      j2=0;
      //printf("i : %d\n",i);
      for(j=startheight;j<endheight;j+=sizeofpixelcenter)
	{
	  //printf("j : %d\n",j);
	  moyr = 0;
	  moyg = 0;
	  moyb = 0;
	  for(x=i;x<i+sizeofpixelcenter;x++)
	    {
	      for(z=j;z<j+sizeofpixelcenter;z++)
		{
						
		  moyr += entre[z*width*3+x*3];	
		  moyg += entre[z*width*3+x*3+1];	
		  moyb += entre[z*width*3+x*3+2];	
						
		}

	    }	
	  //	printf("%d %d %d %d\n",i,j,x,z);
	  moyr = moyr/(sizeofpixelcenter * sizeofpixelcenter);
	  moyg = moyg/(sizeofpixelcenter * sizeofpixelcenter);
	  moyb = moyb/(sizeofpixelcenter * sizeofpixelcenter);
	  /*res[j2][i2][0] = (unsigned char)moyr;
	    res[j2][i2][1] = (unsigned char)moyg;
	    res[j2][i2][2] = (unsigned char)moyb;
	  */
				
	  for(x=i;x<i+sizeofpixelcenter;x++)
	    {
	      for(z=j;z<j+sizeofpixelcenter;z++)
		{
		  res[z*width*3+x*3] = moyr;
		  res[z*width*3+x*3+1] = moyg;
		  res[z*width*3+x*3+2] = moyb;
		}
	    }
				







	  j2++;
	}
      i2++;



    }
  for(i=center_sup;i<endwidth;i+=sizeofpixel)
    {
      j2=0;
      //printf("i : %d\n",i);
      for(j=startheight;j<endheight;j+=sizeofpixel)
	{
	  //printf("j : %d\n",j);
	  moyr = 0;
	  moyg = 0;
	  moyb = 0;
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
						
		  moyr += entre[z*width*3+x*3];	
		  moyg += entre[z*width*3+x*3+1];	
		  moyb += entre[z*width*3+x*3+2];	
						
		}

	    }	
	  //	printf("%d %d %d %d\n",i,j,x,z);
	  moyr = moyr/(sizeofpixel * sizeofpixel);
	  moyg = moyg/(sizeofpixel * sizeofpixel);
	  moyb = moyb/(sizeofpixel * sizeofpixel);
	  /*res[j2][i2][0] = (unsigned char)moyr;
	    res[j2][i2][1] = (unsigned char)moyg;
	    res[j2][i2][2] = (unsigned char)moyb;
	  */
				
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  res[z*width*3+x*3] = moyr;
		  res[z*width*3+x*3+1] = moyg;
		  res[z*width*3+x*3+2] = moyb;
		}
	    }
				







	  j2++;
	}
      i2++;



    }


}

		
void hsl2rvb(Tpx entre[480][640],unsigned char *sortie,int width,int height)
{
  int i=0;
  int j=0;
  unsigned char c;
  unsigned int p,q,tr,tv,tb,r,v,h,b;
  q=255;
  p=0;
		
  for(i=0;i<width;i++)
    {
      for(j=0;j<height;j++)
	{
	  h = entre[j][i].valeur ;	
	  tr = h + 85;
	  tv = h;
	  tb = h-85;
	  if(tr<0)
	    tr = tr+255;
	  if(tr>255)
	    tr = tr-255;
	
	  if(tb<0)
	    tb = tb+255;
	  if(tb>255)
	    tb = tb-255;
				
	  if(tr<=42)
	    {
					
	      r = p + (q-p) * (tr/42);
	    }
	  else
	    {
	      if(tr<128)
		{
		  r = q;
		}
	      else
		{
		  if(tr<170)
		    {
		      r= ((170*q - 128*p)/42)  - ((q-p)/42)*tr;
		    }
		  else
		    {
		      r=p;
		    }
		}
					
	    }
				
				
	  if(tv<=42)
	    {
					
	      v = p + (q-p) * (tr/42);
	    }
	  else
	    {
	      if(tv<128)
		{
		  v = q;
		}
	      else
		{
		  if(tv<170)
		    {
		      v= ((170*q - 128*p)/42)  - ((q-p)/42)*tv;
		    }
		  else
		    {
		      v=p;
		    }
		}
					
	    }
	  if(tb<=42)
	    {
					
	      b = p + (q-p) * (tr/42);
	    }
	  else
	    {
	      if(tb<128)
		{
		  b = q;
		}
	      else
		{
		  if(tb<170)
		    {
		      b= ((170*q - 128*p)/42)  - ((q-p)/42)*tb;
		    }
		  else
		    {
		      b=p;
		    }
		}
					
	    }
	  //	printf("h : %d r : %d v : %d b :%d\n",h,r,v,b);
	  sortie[j*width*3 +i*3] = (unsigned char)  (r);
	  sortie[j*width*3 +i*3+1] = (unsigned char)  (v);
	  sortie[j*width*3 +i*3+2] = (unsigned char)  (b);
	  if(entre[j][i].isblack)
	    {
	      sortie[j*width*3 +i*3] = 0;
	      sortie[j*width*3 +i*3+1] = 0;
	      sortie[j*width*3 +i*3+2] = 0;	
	    }
	  if(entre[j][i].iswhite)
	    {
	      sortie[j*width*3 +i*3] = 255;
	      sortie[j*width*3 +i*3+1] = 255;
	      sortie[j*width*3 +i*3+2] = 255;	
	    }
	}
				
    }
  //	printf("width : %d, height %d\n",width,height);
}
void rvb2hsl(unsigned char *image, Tpx teinte[480][640], int width, int height){

  //pour le cacul de la teinte
  int T0, delta; // delta=max(r,v,b)-min(r,v,b)
  unsigned char cst=TMAX; // pour la formule du calcul de la teinte

  unsigned char r, v, b; // couleurs
  int i=0, j; //parcours du tableau hsl
  int temp=0; //parcours du tableau image

  //on traite l'image pixel par pixel
  while(i<height){
    j=0;
    //if(i%10 == 0) printf("ligne %d\n", i);
    while(j<width){
      //recuperation des composantes rvb
      r=image[temp++];
      v=image[temp++];
      b=image[temp++];
      temp += 3*(INC-1);

      if( r == v && v == b ) {
	// cas special
	T0 = 0;
      }
      else if(r >= v && r >= b) {
	delta = (v > b)? r-b : r-v;
	T0 = cst*(v-b)/ (6*delta);
      } else if(v >= b && v >= r) {
	delta = (b > r)? v-r : v-b;
	T0 = cst*((b-r)+2*delta)/ (6*delta);
      } else {
	delta = (r > v)? b-v : b-r;
	T0 = cst*((r-v)+4*delta)/ (6*delta);
      }
			   
      //T = ((T0)*cst); // cst = TMAX/6 avec TMAX = 255
      //T = (unsigned char)(T%TMAX);
			
      //remplir le tableau hsl avec seulement la composante h
      if(r> seuil_blanc && v > seuil_blanc && b > seuil_blanc)
	{
	  teinte[i][j].iswhite =1;
	}
      else
	{
	  teinte[i][j].iswhite = 0;
	}
      if(r< 60 && v < 60 && b < 60)
	{
	  teinte[i][j].isblack =1;
	}
      else
	{
	  teinte[i][j].isblack = 0;
	}
      teinte[i][j].valeur=T0;
      teinte[i][j].traite=0;
      // on avance dans la ligne
      j++;  
    } // while(width)
    // on change de ligne
    i += INC;
  }// while(height)
}// rvb2hsl()
		
		
/**
 * Crée des pixels de taille 'sizeofpixel' dont la teinte est la moyenne
 * des teintes des pixels remplacés.
 */		
void pixeliser_teinte(Tpx entre[480][640], Tpx sortie[480][640],int width,int height,int sizeofpixel)
{
  int i,j,x,z;
  int moy=0;
  for(i=0;i<width;i+=sizeofpixel)
    {
      for(j=0;j<width;j+=sizeofpixel)
	{
	  moy=0;
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  moy+=entre[z][x].valeur;
					
		}
	    }
	  moy/=(sizeofpixel * sizeofpixel);
			
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  sortie[z][x].valeur = moy;
					
		}
	    }
			
			
	}
		
    }
	
}
/**
 * La teinte sera codée par intervales de longueur 'div'
 * (0, div, 2*div, 3*div, ...).
 */
void discretiser_teinte(Tpx entre[480][640], Tpx sortie[480][640],int width,int height,int div)
{
  int i,j,x,z;
  int moy=0;
  for(i=0;i<width;i++)
    {
      for(j=0;j<width;j++)
	{
	  moy=0;
	  sortie[j][i].valeur = entre[j][i].valeur / div;
			
	  sortie[j][i].valeur*=div;	
	}
		
    }
	
}

void discretiser(unsigned char *entre,unsigned char *sortie,int width,int height,int div)
{
  int i,j;
	
	
  for(i=0;i<width;i++)
    {
      for(j=0;j<height;j++)
	{
	  sortie[j*width*3 + i*3] = entre[j*width*3 + i*3]/div;	
	  sortie[j*width*3 + i*3 +1] = entre[j*width*3 + i*3+1]/div;	
	  sortie[j*width*3 + i*3 +2] = entre[j*width*3 + i*3+2]/div;	
	  sortie[j*width*3 + i*3]*=div;
	  sortie[j*width*3 + i*3+1]*=div;
	  sortie[j*width*3 + i*3+2]*=div;
		
	}
		
    }
	
	
	
}
/**
 * Fait grossir la surface 't' en partant du point actuel et en recommançant
 * la recherche avec ses 8 voisin.
 *
 * La recherche s'arrête si la profondeur atteint 'profondeur_max'.
 * La recherche s'arrête si on est trop près du bord.
 * La recherche s'arrête si pas bonne couleur ou blanc ou noir.
 *
 * @param t la surface qui grossi progressivement.
 * @param prof Profondeur de recherche.
 *
 * @return nb de pixels trouvés.
 */
int search_surface(unsigned char color,int startx,int starty,Tpx teinte[480][640],trapez *t,int width,int height,int prof)
{
  int i,j;
  //printf("color : %d %d %d %d\n",color,startx,starty,prof);
  /*if(teinte[starty][startx].traite ==1)
    return 0;*/
  if(prof>profondeur_max)
    {return 0;}
  /* trop près du bord */
  if(startx>width-2 || starty>height-2 || startx<2 || starty<2)
    {	
				
      //	teinte[starty][startx].traite = 1;
      return 0;
	
    }
  /* pas bonne couleur, ou blanc, ou noir */
  if(teinte[starty][startx].traite || teinte[starty][startx].valeur != color ||  teinte[starty][startx].iswhite  || teinte[starty][startx].isblack){
    teinte[starty][startx].traite = 1;
    return 0;
  }
  else
    /* ajoute point courant au trapèze 't' */
    {

      teinte[starty][startx].traite = 1;
      //	teinte[starty][startx].valeur = 0;
		
      if(t->lu.x > startx)
	{t->lu.x = startx;
	}
      if(t->lu.y >starty)
	{
	  t->lu.y = starty;
	}
      if(t->rd.x <startx)
	{
	  t->rd.x = startx;
	}
      if(t->rd.y < starty)
	{
	  t->rd.y = starty;
	}
	
      /* renvoie le nb de pixel trouvé dans courant et ses 8 voisins */
      return 1
	+ search_surface(color,startx + 1,starty+1,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx + 1,starty-1,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx - 1,starty-1,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx - 1,starty+1,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx + 1,starty+0,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx + 0,starty+1,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx - 1,starty+0,teinte,t,width,height,prof+1) 
	+ search_surface(color,startx + 0,starty-1,teinte,t,width,height,prof+1);		
    }
	
	
}
/**
 * A partir d'un point, cherche une surface minimum de la couleur indiquée.
 * On indique la zone dans laquelle on cherche parmis les points "non-traités".
 *
 * @return t trapez mis a jour
 * @return nb de pixels dans la surface
 */
int trouve_trapez(int surf_min,unsigned char color,Tpx teinte[480][640],int width,int height,int startx,int starty,int stopx,int stopy,trapez *t)
{
	
  int i,j;
  int n;
  for(j=starty;j<stopy-1;j++)
    {
      for(i=startx;i<stopx-1;i++)
	{
		

	  t->lu.x = i;
	  t->lu.y = j;
	  t->rd.x = i;
	  t->rd.y = j;
	  t->luint.x = i;
	  t->luint.y = j;
	  t->rdint.x = i;
	  t->rdint.y = j;
	  if(!teinte[j][i].traite){
	    if((n=search_surface(color,i,j,teinte,t,width,height,0))>surf_min)
	      {
		//	printf("on a trouve le truc, les coordoné des point : lu %d %d,rd %d %d   sa surface : %d\n",t->lu.x,t->lu.y,t->rd.x,t->rd.y,n);
		t->color = color;
		teinte[j][i].traite=1;
		return n;
	      }
	  }
	}
		
    }
  return 0;
	
}

/**
 * Le champs '.traite' de chaque pixel est remis à 0.
 */
void reinit_teinte(Tpx teinte[480][640],int width,int height)
{
  int i,j;
  for(i=0;i<width;i++)
    {
      for(j=0;j<height;j++)
	{
	  teinte[j][i].traite = 0;
	}
		
    }
	
}

	
/**
 * La cible est composée d'une surface rouge dans laquelle se trouve une surface
 * jaune. 
 *
 * @return target cible trouvée ou pas (tout à -1 ou 100000000).
 */
target chercher_cible()
{
  int taille;
  int i,j;
  int sr,sb,sj; //surface rouge bleu jaune..
  unsigned char image[480][640][3];
  unsigned char buffer[100000];
  int width,height;
  Tpx teinte[480][640];
  Tpx teinte2[480][640];
  unsigned char res[480][640];
  unsigned char res2[480][640][3];
  unsigned char res3[480][640][3];
  target ret;
  trapez tr,tb,tj;
  printf("getImage\n");
  taille = GetImage(buffer);
  //	printf("ici%d\n",taille);
  //printf("decompresseImage\n");
  taille = DecompressImage(image, buffer, taille, &width, &height);
  //		printf("la\n");
  //pixeliser(image,width,0,res2,0,width,0,height,2,120,200,2);
  //discretiser(image,res2,width,height,128);
  //	remplir_teinte(teinte);
  //	pixeliser(image,width,0,res2,0,width,0,height,3,120,200,3);
  rvb2hsl(image, teinte, width, height);
  //printf("la\n");
  //fflush(stdout);
  discretiser_teinte(teinte,teinte2,width,height,32);
  pixeliser_teinte(teinte2,teinte,width,height,1);
			

				
			
  /* repasse en rvb pour sauver l'image dans fichier "teste.ppm" */
  hsl2rvb(teinte,res2,width,height);
  //pixeliser(image,width,0,res2,0,width,0,height,3,120,200,3);
  //	discretiser(res3,res2,width,height,64);


  WriteImage("teste.ppm", res2, width*height*3, width, height);
  reinit_teinte(teinte,640,480);

  for(j=1;j<height-1;j++)
    {
      for(i=1;i<width-1;i++)
	{
				
			
	  //	printf("%d,%d\n",i,j);
	  //printf("red- : %d\n",teinte[j][i].traite);
	  
	  /* cherche un point rouge (pas blanc/noir) non traité */
	  if(teinte[j][i].valeur == color_red && !(teinte[j][i].iswhite) && !(teinte[j][i].isblack) && !(teinte[j][i].traite))
	    {
	      //printf("red : %d\n",teinte[j][i].traite);

	      /* cherche une surface rouge dans l'image entière */
	      sr=trouve_trapez(400,color_red,teinte,width,height,i,j,width,height,&tr);
					
	      /* si on a trouvé qqchose */
	      if(sr)
		{
		  //	printf("%d\n",teinte[310][310].valeur);
		  //reinit_teinte(teinte,width,height);
		  
		  /* cherche une surface jaune DANS les limites de la surface rouge */
		  sj=trouve_trapez(sr/5,color_yellow,teinte,width,height,tr.lu.x,tr.lu.y,tr.rd.x,tr.rd.y,&tj);
		  //printf("trouvey \n%d %d %d %d\n%d %d %d %d\nsr: %d sb : %d sj : %d\n",tr.lu.x,tr.lu.y,tr.rd.x,tr.rd.y,tj.lu.x,tj.lu.y,tj.rd.x,tj.rd.y,sr,sb,sj);
		  
		  /* si on a aussi trouvé le jaune */
		  if(sj)
		    {
		      if(tj.lu.x > tr.lu.x && tj.lu.y > tr.lu.y && tj.rd.x< tr.rd.x && tj.rd.y < tr.rd.y)
			{ 
			  /* prépare les coordonnées de la cible trouvée */
			  ret.xpos = tr.lu.x;
			  ret.ypos = tr.lu.y;
			  ret.hauteur = tr.rd.y - tr.lu.y;
			  ret.largeur = tr.rd.x - tr.lu.x;
			  ret.center = ret.largeur/2 + tr.lu.x;
			  printf("la target : centre : %d , largeur : %d, hauteur : %d posx %d posy : %d\n",ret.center,ret.largeur,ret.hauteur,ret.xpos,ret.ypos); 
			  return ret;
			}
		    }
		}
	      //	exit(0);
		
	    }
	}
		
    }

  /* prépare les coordonnées de la cible PAS trouvée */
  ret.xpos = -1;
  ret.ypos = -1;
  ret.hauteur = 100000000;
  ret.largeur = -1;
  ret.center = -1;
  printf("pas de target\n");
  return ret;
}
