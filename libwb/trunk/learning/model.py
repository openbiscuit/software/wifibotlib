#! /usr/bin/env python
#
# usually lauched by >> xterm -bg white -fg black -e ipython -gthread first.py &
#
import pygtk
#pygtk.require('2.0')

import gtk, gobject, cairo
import math

playground = (-1.0, -10.0, 10.0, 10.0)
#playground = (-5.0, -5.0, 5.0, 5.0)

# Create a GTK+ widget on which we will draw using Cairo
class Screen(gtk.DrawingArea):

    model = False
    idleWork = -1

    # Draw in response to an expose-event
    #__gsignals__ = { "expose-event": "override" }

    def init(self):
        """ Connect for various event.
        """
        self.connect("expose_event", self.draw_callback)
        self.setIdle( True )
        # Un model
        #self.model = Model()

    def setIdle(self, flag):
        """ True pour dessiner en idle, False sinon.
        """
        if( flag ) :
            if( self.idleWork == -1) :
                self.idleWork = gobject.idle_add( self.idle_callback )
        else :
            if( self.idleWork != -1) :
                gobject.source_remove( self.idleWork )
                self.idleWork = -1

    # DrawCallback
    def draw_callback(self, widget, event):
        
        # Create the cairo context
        cr = self.window.cairo_create()

        # Restrict Cairo to the exposed area; avoid extra work
        cr.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        cr.clip()

        self.draw(cr, *self.window.get_size())
        return True

    # IdleCallback
    def idle_callback(self):
        #print "Idle"
        # Create the cairo context
        cr = self.window.cairo_create()

        self.draw(cr, *self.window.get_size())
        return True

    # Handle the expose-event by drawing
    def do_expose_event(self, event):

        # Create the cairo context
        cr = self.window.cairo_create()

        # Restrict Cairo to the exposed area; avoid extra work
        cr.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        cr.clip()

        self.draw(cr, *self.window.get_size())

    def do_draw(self, event=0):
        """ Draw event.
        """
        # Create the cairo context
        cr = self.window.cairo_create()
        self.draw(cr, *self.window.get_size())

    def do_draw_traj(self, traj, fgPrey=True, col=(0.0, 0.0, 0.8)):
        # Create the cairo context
        cr = self.window.cairo_create()
        width,height = self.window.get_size()
        # Fill the background with white
        cr.set_source_rgb(1, 1, 1)
        cr.rectangle(0, 0, width, height)
        cr.fill()
        # set up a transform so that playground
        # maps to (0,0)x(width, height)
        cr.scale(width / (playground[2]-playground[0]),
                 -height / (playground[3] - playground[1]))
        #cr.translate( (playground[2]-playground[0])/2, -(playground[3] - playground[1])/2)
        cr.translate( -playground[0], -playground[3] )
        cr.set_line_width(0.01)
        
        self.drawAxes(cr)
        self.drawTraj( cr, traj, fgPrey, col)
        
    def draw(self, cr, width, height):
        # Fill the background with white
        cr.set_source_rgb(1, 1, 1)
        cr.rectangle(0, 0, width, height)
        cr.fill()
        # set up a transform so that playground
        # maps to (0,0)x(width, height)
        cr.scale(width / (playground[2]-playground[0]),
                 -height / (playground[3] - playground[1]))
        #cr.translate( (playground[2]-playground[0])/2, -(playground[3] - playground[1])/2)
        cr.translate( -playground[0], -playground[3] )
        cr.set_line_width(0.01)
        # draw
        if self.model == False :
            #print "default"
            self.drawAxes(cr)
            self.drawTriangle(cr, 0.3 )
            self.drawCircle(cr, 0.2)
            # store context
            context = cr.get_matrix()
            cr.translate(1,1)
            #draw
            self.drawCircle( cr, 0.2)
            # restore context
            cr.set_matrix(context)
            # store context
            context = cr.get_matrix()
            cr.translate(1,0)
            #draw
            self.drawCircle( cr, 0.2)
            # restore context
            cr.set_matrix(context)
            
        else :
            self.drawAxes(cr)
            self.drawPredator(cr)
            self.drawPrey(cr)

    def drawAxes(self, cr):
        """ Draw axes in Grey.
        """
        # en gris
        cr.set_source_rgb(0.3, 0.3, 0.3)
        cr.move_to( playground[0],0 )
        cr.line_to( playground[2],0 )
        cr.move_to( 0, playground[1] )
        cr.line_to( 0,playground[3] )
        cr.stroke()
        
    def drawPlots( self, cr):
        """ Draw the plots in Orange.
        """
        # store context
        context = cr.get_matrix()
        #en orange
        cr.set_source_rgb(0.95, 0.5, 0.2)
        for pl in self.model.plots:
            x,y,size = pl
            cr.arc(x,y,size,0,2*math.pi)
        cr.fill()
            
    def drawPrey(self, cr):
        """ Utilise le modele pour afficher la proie.
        """
        x,y = self.model.prey

        # store context
        context = cr.get_matrix()
        cr.translate(x,y)
        #draw
        self.drawCircle( cr, 0.2)
        # restore context
        cr.set_matrix(context)
        
    def drawPredator(self, cr):
        """ Utilise le modele pour afficher le predateur.
        """
        
        x,y,rot = self.model.predator
        
        # store context
        context = cr.get_matrix()
        cr.translate(x,y)
        cr.rotate( rot )
        #draw
        self.drawTriangle(cr, 0.3)
        angV,radV = self.model.view
        self.drawCone(cr, angV,radV)
        if self.model.percep_predator() != False:
                angP,distP = self.model.percep_predator()
                self.drawHeading(cr,angP,distP)
        # restore context
        cr.set_matrix(context)

    def drawTriangle(self, cr, size, col=(0.8, 0.0, 0.0)):
        """ Dessine un triangle.
        """
        # en rouge
        cr.set_source_rgb(col[0], col[1], col[2])
        cr.move_to(size, 0)
        cr.rel_line_to(-3*size/2, -size/2)
        cr.rel_line_to(0, size)
        cr.rel_line_to(3*size/2, -size/2)
        cr.stroke()

    def drawCircle(self, cr, radius):
        """ Dessine un cercle.
        """
        #en vert
        cr.set_source_rgb(0.0, 0.8, 0.0)
        #draw
        cr.arc( 0, 0, radius, 0, 2 * math.pi)
        cr.stroke()


    def drawCone(self, cr, angle, radius):
        """ Dessine un cone bleu.
        """
        #en bleu
        cr.set_source_rgb(0.0, 0.0, 0.8)
        #draw
        #cr.line(0,0, radius * math.cos(angle/2), radius * math.sin(angle/2))
        cr.move_to(0,0)
        cr.arc( 0, 0, radius, -angle/2 , angle/2)
        cr.line_to(0,0)
        cr.stroke()


    def drawHeading(self, cr, ang, dist):
        """ Dessine une ligne pointillee bleue.
        """
        #en bleu
        cr.set_source_rgb(0.0, 0.0, 0.8)
        #draw
        #turn dash on
        cr.set_dash((0.1,),0)
        cr.move_to(0,0)
        cr.line_to( dist * math.cos(ang), dist * math.sin(ang))
        cr.stroke()
        #turn tash off
        cr.set_dash((1,0),0)

    def drawTraj(self, cr, traj, fgPrey=True, col=(0.0, 0.0, 0.8)):
        """ Dessine une trajectoire de Prey.
        """
        # store context
        context = cr.get_matrix()
        # couleur
        cr.set_source_rgb(col[0], col[1], col[2])

        pos = traj[0]
        cr.move_to(pos[0],pos[1])
        for pos in traj:
            cr.line_to(pos[0],pos[1])
        cr.stroke()

        if fgPrey:
            for pos in traj:
                local = cr.get_matrix()
                cr.translate(pos[0],pos[1])
                cr.rotate( pos[2] )
                #draw
                self.drawTriangle(cr, 0.3)
                cr.set_matrix(local)
                
    
        # restore context
        cr.set_matrix(context)
                
class Model:
    """ Consist in a 'prey' and a 'predator'.
    """
    predator = (5,5,0) #x,y,rot
    view = (math.pi/2,5) #angle, dist
    prey = (5,0) #x,y

    def advance_predator(self, dist):
        """ Advance from a distance 'dist'.
        """
        x,y,r = self.predator
        #print "Avant x=",x," y=",y," r=",r
        #x = ( x + dist * math.cos(r)) % playground[0]
        #y = ( y + dist * math.sin(r)) % playground[1]
        #x,y = self.clipTore( x + dist * math.cos(r), y + dist * math.sin(r))
        x = ( x + dist * math.cos(r))
        y = ( y + dist * math.sin(r))
        #print "Apres x=",x," y=",y," r=",r
        self.predator = (x,y,r)

    def turn_predator(self, rot):
        """ Turn clockwise.
        """
        x,y,r = self.predator
        r += rot
        # clipping
        r = r % (2 * math.pi)
        if( r > math.pi) : r -= 2 * math.pi
        self.predator = (x,y,r)

    def percep_predator(self):
        """ Distance and angle to prey or (-1,-1)
        """
        xp,yp,rp = self.predator
        xt,yt = self.prey
        xrel = xt - xp
        yrel = yt - yp
        dist = math.sqrt(xrel*xrel+yrel*yrel)
        ang = math.atan2(yrel,xrel) - rp 

        angref,distref = self.view
        #print "### Model.percep_predator : angle = ",ang," dist=",dist
        "print "###                         lim = [",-angref/2,", ", angref/2,"]"
        #print "###                         xrel =",xrel," , yrel =",yrel
        if( (math.fabs(ang) < angref/2) and (dist < distref)):
            return (ang,dist)
        else :
            return False

    def clipTore(self, x, y):
        """ Make sure that coordinate stay in playground.
        """
        deltaX = playground[2] - playground[0]
        x = x - playground[0]
        x = x % deltaX
        x = x + playground[0]
        deltaY = playground[3] - playground[1]
        y = y - playground[1]
        y = y % deltaY
        y = y + playground[1]
        return (x,y)

def redraw():
    # Create the cairo context
    cr = widget.window.cairo_create()
    
    widget.draw(cr, *widget.window.get_size())

def display(aModel):
    """ Create a screen for displaying a model.
    """
    global widget
    window = gtk.Window()
    window.resize(300,600)
    window.connect("delete-event", gtk.main_quit)
    widget = Screen()
    widget.init()
    widget.show()
    widget.model = aModel
    window.add(widget)
    window.present()
    gtk.main()

# GTK mumbo-jumbo to show the widget in a window and quit when it's closed
def run(Widget):
    global widget
    window = gtk.Window()
    window.resize(300,300)
    window.connect("delete-event", gtk.main_quit)
    widget = Widget()
    widget.init()
    widget.show()
    window.add(widget)
    window.present()
    gtk.main()

if __name__ == "__main__":
    run(Screen)
    
