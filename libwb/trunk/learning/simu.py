#
# En train de mettre en place des trajectoires (memorisees) mais pour quoi faire?
# Comme y'a des problemes entre la gestion des "inputs" et les "idle" (en gros
# on n'est pas capable de faire de l'affichage pendant qu'on attend une entree),
# je propose de deconnecter IdleEvent et de rajouter une fonciton do_draw dans model.#
# AFAIRE : afficher lae KTree apres une trajectoire pour voir
#
# xterm -e ipython -gthread
# 
# * charger KdTree et creer
# * action_poss
#   * creer
#   * lien avec actions du modele
# * model
# - learn
# - format des Kdvector (avec action)
# - turn off enveloppe convexe
# - affichage KdTree "brut"
#   * findClosestInTree
# - QDEF = -1

import kdtree
import model
import learn

# ctypes is important for binding C to PYTHON
import ctypes

import random
import math
import scipy

class Simu:
    # variables
    nbDim = 2
    nbElemLeaf = 10
    nbPoints = 10
    # param
    bandwith = 0.3
    alpha = 0.9
    gamma = 0.8
    r_goal = 5.0
    rec_dist_min = 0.5
    rec_dist_max = 1.0
    rec_ang_max = 0.4
    # un modele pour la simulation
    model = False
    # un kdtree
    tree = False
    # liste actions possibles
    dic_actions = { 0.0 : lambda s: s.model.advance_predator( 0.3 ),
                    1.0 : lambda s: s.model.turn_predator( math.pi/6 ),
                    2.0 : lambda s: s.model.turn_predator( -math.pi/6 ) }
    nbAct = len(dic_actions)
    action_poss = False
    verbeux = 0

    def init(self):
        """ Create tree,
        """
        self.tree = kdtree.libkdtree.KdNewTree(self.nbDim+1, self.nbElemLeaf)
        # preparation des poids pour la distance
        we = kdtree.libkdtree.KdNewVector(4);
        we.contents.value[0] = 1; #poids de la distance
        we.contents.value[1] = 1; # poid de l'angle : importance normal
        we.contents.value[2] = 100; # l'action a une importance maximal
        we.contents.value[3] = 1;
        #kdtree.libkdtree.KdSetWeight(self.tree, ctypes.pointer(we));
        kdtree.libkdtree.KdSetWeight(self.tree, we);
        self.verbeux = 10
        # action_poss
        tmval = (ctypes.c_double * self.nbAct)()
        intAct = self.dic_actions.keys()
	for i in range(len(tmval)) :
		tmval[i] = intAct[i]
        self.action_poss = kdtree.KdVector( self.nbAct, tmval, 0, 0, 1)

        # model
        self.model = model.Model()
        

    def initPos(self):
        """ Reset the Prey position.
        """
        r = random.random() * math.pi * 2
        x = random.random() * 2;
        y = (random.random() - 0.5) * 10
        self.model.predator = (x,y,r)
        

    def oneStep(self, action):
        """ One step of simulation, return (state, reward).
            'action' is one from the key of dic_action (0, 1, etc).
             BEWARE : state can be (ang,dist) or False
        """
        # action : utilise une des methodes de 'model' stockee dans le dictionnaire
        self.dic_actions[action](self)
        # state
        state = self.model.percep_predator()
        # reward
        reward = self.computeReward()
        #
        if (self.verbeux > 5) :
            print action," ==> ",state,"(",reward,")"
        return (state, reward)

    def oneLearn(self, state, action, nextState, reward):
        """ One step of learning.
        """
        global listExtendedStates
        #states
        xVec = kdtree.newKdVectorTuple( state )
        xNextVect = kdtree.newKdVectorTuple( nextState )
        #action
        ctypes.c_double(action)
        # learn
        learn.liblearn.Hedger_training( self.tree,
                                        #ctypes.pointer(listExtendedStates),
                                        listExtendedStates,
                                        ctypes.pointer(xVec),
                                        ctypes.pointer(xNextVect),
                                        ctypes.c_double(reward),
                                        ctypes.c_int(self.nbPoints),
                                        ctypes.c_double(self.bandwith),
                                        ctypes.c_double(self.alpha),
                                        ctypes.c_double(self.gamma),
                                        ctypes.pointer(self.action_poss),
                                        ctypes.c_double(action)
                                        )

    def computeReward(self):
        """ Depends on the relative distance between prey and predator.
            And if the predator look toward the prey.
        """
        xr,yr,rr = self.model.predator
        xp,yp = self.model.prey
        dist = math.sqrt((xp-xr)*(xp-xr)+(yp-yr)*(yp-yr))
        ang = math.fabs(math.atan2((yp-yr),(xp-xr)) - rr)
        if (dist> self.rec_dist_min) and (dist < self.rec_dist_max) and (ang < self.rec_ang_max):
            reward = self.r_goal
        else:
            reward = 0
        return reward
        
    def trajHint(self, fgLearn=False):
        """ Init Position, then ask for action until '-1'.
        """
        global listExtendedStates
        # real trajectory
        traj = []
        # state,action,r history
        hist = []
        
        self.initPos()
        traj.append(self.model.predator)
        oldState = self.model.percep_predator()
        if( oldState == False ):
            oldState = (-1,-1)
        hist.append(oldState)
        
        model.widget.do_draw()
        model.widget.do_draw()
        choix = 0
        while choix >= 0:
            choix = float(raw_input("Action : "))
            
            print "Choix = %d" % choix
            if choix >= 0.0:
                print "Applique action"
                s,r = self.oneStep(choix)
                traj.append(self.model.predator)
                hist.append(choix)
                hist.append(r)
                if( s == False ):
                    s = (-1,-1)
                hist.append( s )
                if( fgLearn ):
                    self.oneLearn( oldState, choix, s, r)
                
                print "Rec = %f" % r
                model.widget.do_draw()
                model.widget.do_draw()

                oldState = s
            # HACK pour afficher l'arbre
            if choix <= -2.0:
                print "Etat speciaux"
                kdtree.libkdtree.printListVector(listExtendedStates)
                kdtree.printTree( self.tree )
                choix = 0.0

        return hist,traj


    def KdInterpol(self, x1, x2, act=0.0):
        """ Uses kdtree pour generer une surface interpolee.
        Hard-coded pour KdVector de dimension 3.
        """
        global listPoint
        global mindist
        global pred_status
        # Point
        xp = kdtree.newKdVectorTuple((x1,x2,act))
        # Interpolation
        cleanListPoint()
        #listPoint = kdtree.libkdtree.KdNewVectorList()
        #mindist = ctypes.c_double(0.0)
        result = 0;
        result = learn.liblearn.Hedger_prediction( self.tree, ctypes.pointer(xp),
                                    ctypes.c_double(self.bandwith),
                                    ctypes.c_int(self.nbPoints),
                                    listPoint,
                                    ctypes.pointer(mindist),
                                    ctypes.pointer(pred_status) )
        print( result );
        return result
    
    def KdInterpolSliced(self, x1, x2):
        """ Uses kdtree pour generer une surface interpolee.
        Hard-coded pour KdVector de dimension 3.
        """
        global listPoint
        global mindist
        global pred_status
        global actionAdded
        # Point
        xp = kdtree.newKdVectorTuple((x1,x2,actionAdded))
        # Interpolation
        cleanListPoint()
        #listPoint = kdtree.libkdtree.KdNewVectorList()
        #mindist = ctypes.c_double(0.0)
        result = 0;
        result = learn.liblearn.Hedger_prediction( self.tree, ctypes.pointer(xp),
                                    ctypes.c_double(self.bandwith),
                                    ctypes.c_int(self.nbPoints),
                                    listPoint,
                                    ctypes.pointer(mindist),
                                    ctypes.pointer(pred_status) )
        #print( result );
        return result

    def KdRaw(self, x1, x2):
        """ Utilise kdtree pour generer une surface brute.
        Pseudo-dirac en chaque point.
        Hard-coded pour KdVector de dimension 3.
        """
        # Point
        xp = kdtree.newKdVectorTuple((x1,x2,0.0))
        close = ctypes.POINTER(kdtree.KdVector)()
        # Raw value
        distance = kdtree.libkdtree.KdFindClosestInTree( self.tree, ctypes.pointer(xp), ctypes.pointer(close))

    def plotTreeRawSliced(self, act, lim=(-1.5,1.5,-1.5,5)):
        """ Plot raw tree for action=act.
        """
        global checkActionWhenAdding
        global actionAdded
        global KdIsPointAdded
        checkActionWhenAdding = True
        actionAdded = act
        strTitre = str("Raw Qval act=%d"%act)
        kdtree.plotTree3DGFun( self.tree, fun=KdIsPointAdded, limits=lim, titleStr=strTitre)

    def plotTreeInterpolSliced(self, act, lim=(-1.5,1.5,-1.5,5)):
        """ Plot interpolled tree for action=act.
        """
        global actionAdded
        actionAdded = act
        strTitre = str("Inter Qval act=%d"%act)
        kdtree.plotTree3DGVal( self.tree, self.KdInterpolSliced, limits=lim, titleStr=strTitre)

def KdIsPointAdded( vec, tabZ, limits, precision):
    """ KdVector : vecteur issu du KdTree (etat,action).
    Hard-coded for the actual simulator.
    A point is added if is in the Limits and the action is ok.
    """
    global checkActionWhenAdding
    global actionAdded
    x = vec.contents.value[0]
    y = vec.contents.value[1]
    act = vec.contents.value[2]
    
    if (kdtree.inLimits(x, y, limits)):
        if (( checkActionWhenAdding == False) or (act == actionAdded)):
            tabZ[(y - limits[2])/precision[1],
                 (x - limits[0])/precision[0]] = vec.contents.key
            return True
        else:
            return False
    return False
    
    #TEMP dic_essai = { 0 : lambda s: s.printA(), 1 : lambda s: s.printB()}
    #TEMP 
    #TEMP def printA(self):
    #TEMP     print "A"
    #TEMP def printB(self):
    #TEMP     print "B"
    #TEMP def essai(self):
    #TEMP     for k,f in self.dic_essai.iteritems():
    #TEMP         f(self)

def cleanListPoint():
    global listPoint
    kdtree.libkdtree.KdFreeVectorListWithoutVector(listPoint.contents.tete)
    listPoint.contents.tete = None
    listPoint.contents.size = 0
    listPoint.contents.queue = None

def computeSurf( simulateur, x, y):
    """ x and y are 2D meshgrid (numpy).
    """
    #[x,y] = grid
    #result is an array
    z = scipy.zeros( x.shape )
    for i in range(len(x)) :
        for j in range(len(x[i])) :
            #print "(",x[i][j],", ",y[i][j],")"
            z[i][j] = simulateur.KdInterpol( x[i][j], y[i][j])
    return z

def startSim(fgGraphic = True):
    """ Start the simulation;
    """
    #global sim
    global listV
    global listPoint
    global mindist
    global pred_status
    global listExtendedStates

    # Simulator
    sim = Simu()
    sim.init()
    
    # global variables
    listExtendedStates = kdtree.libkdtree.KdNewVectorList()
    listV = []
    
    # y mettre l'etat (-1,-1)
    for action in sim.dic_actions:
        xp = kdtree.newKdVectorTuple((-1,-1,action))
        listV.append(xp)
        print "ajout de ",kdtree.libkdtree.printVector( ctypes.pointer(xp) )
        kdtree.libkdtree.KdAddVector( listExtendedStates, ctypes.pointer(xp) )
        print "LIST---\n"
        kdtree.libkdtree.printListVector( listExtendedStates )
    
    listPoint = kdtree.libkdtree.KdNewVectorList()
    mindist = ctypes.c_double(0.0)
    pred_status = ctypes.c_int(0)
    
    if fgGraphic:
        model.display(sim.model)
        model.widget.model = sim.model
        model.widget.setIdle(False)
    return sim

def fillTree(simulateur):
    global listV
    global listPoint
    global mindist
    global pred_status
    listV = []
    simulateur.tree = kdtree.libkdtree.KdNewTree(3, 10)
    kdtree.fillKdTree( simulateur.tree, 350, listV )
    listPoint = kdtree.libkdtree.KdNewVectorList();
    mindist = ctypes.c_double(0.0);
    pred_status = ctypes.c_int(0);
    
if __name__ == "__main__":
    #     simu = Simu()
    #     simu.init()
    #model.run(model.Screen)
    #model.widget.model = simu.model
    listV = []
    #     simu.tree = kdtree.libkdtree.KdNewTree(3, 10)
    #     kdtree.fillKdTree( simu.tree, 350, listV )
    #     simu.tree.contents.qmin = ctypes.c_double(-10.0)
    #     simu.tree.contents.qmax = ctypes.c_double(10.0)
    #kdtree.plotTree2DG( simu.tree )
    #kdtree.plotTreeSplit2DG( simu.tree, limits=(-2,2,-2,2))
    #     kdtree.plotTree3DG( simu.tree, dimx=0, dimy=1, limits=(-2,2,-2,2), precision=(0.1,0.1) )
    #     listPoint = kdtree.libkdtree.KdNewVectorList();
    #     mindist = ctypes.c_double(0.0);
    sim = startSim(True)
    #h,t = sim.trajHint(fgLearn=False)
    



