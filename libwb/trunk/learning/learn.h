/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   learn.h
 * @brief  Quick Reinforcement Learning (from [Smart02])
 *
 * W. Smart, "Making reinforcement learning work on real robots",
 * PhD from Department of Computer Science, Brown University,
 * 2002}
 *
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * <br><b>French comments can be found in the source file</b>.
 * Used by:
 */

/** fichier permettant l'aprentissage par renforcement dans le domaine continu , (cf Making Reinforcement Learning Work on Real Robots by Smart)
*/

#include "kdtree.h"
#include "calcul.h"
#include <stdio.h>
#include <stdlib.h>

#define QDEF 0.0 // valeur qdefault pour l'algo d'aprentissage
#define Seuil 0.001 // seuil par défaut :ceci permet de ne pas ajouter un point si l'arbre contient déja un point à une distance de ce point inférieur au seuil

/** Debug flag, entre 0 et 2 */
extern int db_learn;

/**
 * Niveau de verbosit pour debug.
 * 0 = verbosité nulle.
 * 1 = verbeux, 2 plus verbeux, etc...
 */
void set_debug( long matrixVerbosity, int learnVerbosity );


/**
 * Fonction "Hedger_Training".
 * 
 * Dans l'état 'x', on vient de faire l'action 'ac' et de recevoir la récompense 'rewards' avant d'arriver dans l'état 'xp'.
 * On va mettre à jour Q(xp,ac) et Q(voisins,ac) en utilisant une régression
 * linéaire pour trouver les valeurs de Q.
 * 
 * @param arbre l'arbre contenant les données
 * @param ext_states une liste d'etat externes "symboliques"
 * @param etat vecteur de l'état s(t)
 * @param etatSuiv vecteur de l'état s(t+1)
 * @param rewards récompense au passage entre les 2 états
 * @param nbPointsPourRegression nombres de points nécessaires pour l'interpolation
 * @param bdePassante bande passante (cf article de Smart)
 * @param alpha paramètre alpha
 * @param gamma paramètre gama
 * @param action_possible vecteur contenant toute les actions faisable par l'agent
 * @param action l'action faite a l'état s(t)
 *
 * @return index de l'action la meilleurs dans l'état s(t+1)
 *
 */
int Hedger_training(KdTree *arbre, KdVectorList *ext_states, KdVector *etat, KdVector *etatSuiv,double rewards,int nbPointsPourRegression, double bdePassante, double alpha, double gamma, KdVector *action_possible, double action);

/**
 * Fonction "Hedger_Prediction" permettant de faire une interpolation pour connaître la valeur Q d'un point proche d'autres points.
 *
 * On cherche d'abord dans la liste ext_state pour savoir si on a affaire a un etat
 * externe qui est alors traite a part.
 * Dans l'arbre contenant les points connus, on cherche la feuille contenant le points exemple. 
 * Si cette feuille ne contient pas assez de points -> renvoie QDEF.
 * Si il y a assez de points mais xq n'est pas dans l'enveloppe convexe -> QDEF
 *    sinon calcule l'approximation (LWR_PREDICTION).
 * 'status' est mis a jour et contient une etiquette precisant la condition de 
 * terminaison de la methode.
 *
 * @param arbre l'arbre contenant les données
 * @param ext_states une liste d'etat externes "symboliques"
 * @param pointCible vecteur contenant les coordonnées du point dont on veut connaître la valeur q
 * @param bdePassante bande passante (cf article de Smart)
 * @param nbPointsPourRegression nombres de points nécessaires pour l'interpolation
 * @param listVoisins liste dans laquelle on va stoquer les points utilisés afin de les récupérer ailleurs
 * @param mindist on va stocker la distance du point le plus proche de xq
 * @param status explique comment s'est passé la prédiction
 *
 * @return la valeur de l'approximation ou QDEF
 */
double Hedger_prediction(KdTree *arbre, KdVectorList *ext_states, KdVector *pointCible, double bdePassante, int nbPointsPourRegression, KdVectorList *listVoisins, double *mindist, int *status);

/**
 * Fonction permettant de faire l'approximation.
 * A partir d'une liste 'listVoisin' de points dont on connaît les qValeurs, on essaie
 * de faire une regression linéaire pondérées par la distance, la fonction distance
 * étant elle même pondérée par 'weight'.
 *
 * @param listVoisin  liste contenant les points dont on va se servir
 * @param pointCible le point dont on veut approximer la valeur
 * @param bdePassante bande passante (cf article de Smart)
 * @param nbPointsPourRegression nombre de points maximum pour l'approximation
 * @param weight vecteur contenant les poids des dimensions
 * @param mindist on va stocker la distance du point le plus proche de pointCible
 *
 * @return la valeur de l'approximation
 */
double LWR_PREDICTION(KdVectorList *listVoisin, KdVector *pointCible,
		      double bdePassante, int nbPointsPourRegression,
		      KdVector *weight, double *mindist);
