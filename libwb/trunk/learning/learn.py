# ctypes is important for binding C to PYTHON
import ctypes
from ctypes import *
import kdtree

# IMPORTANT : name of the library ----------------------<<<< IMPORTANT >>>>
liblearn = ctypes.CDLL("liblearn.so.1")

# special function signature
liblearn.set_debug.argtype = [c_long, c_int]
liblearn.Hedger_training.argtype = [POINTER(kdtree.KdTree),
                                    POINTER(kdtree.KdVectorList),
                                    POINTER(kdtree.KdVector),
                                    POINTER(kdtree.KdVector),
                                    c_double, c_int, c_double,
                                    c_double, c_double,
                                    POINTER(kdtree.KdVector),
                                    c_double]
liblearn.Hedger_training.restype = c_int
liblearn.Hedger_prediction.argtype = [POINTER(kdtree.KdTree),
                                       POINTER(kdtree.KdVectorList),
                                       POINTER(kdtree.KdVector),
                                       c_double, c_int,
                                       POINTER(kdtree.KdVectorList),
                                       POINTER(c_double),
                                       POINTER(c_int)]
liblearn.Hedger_prediction.restype = c_double
liblearn.LWR_PREDICTION.restype = c_double
