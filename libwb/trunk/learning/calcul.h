/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   calcul.h
 * @brief  Matrix calculus (pseudo inverse and singular value decomposition)
 *
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * <br><b>French comments can be found in the source file</b>.
 * Used by:
 */

/**
 * Série de fonction de calcul matriciel, se basant sur la bibliothèque svdlibC.
 * Voir : http://tedlab.mit.edu/~dr/SVDLIBC
 */
#define CALCUL_LIB

#include "svdlib.h"
//typedef struct matrix {long width;long height;double *point;}matrix;

/**
 * Renvoie le maximum de la  diagonale d'une matrice.
 *@param m la matrice 
 *@return valeur max de la diagonal de la matrice
*/
double matrix_max_diag(DMat m);

/**
 * Renvoie 1 si la matrice m est remplie de 0 , renvoie 0 sinon.
 * @param m la matrice
 * @return 1 si la matrice ne contient que des 0,0 sinon
 */
int is_empty_matrix(DMat m);

/**
 * Rempli une matrice par la valeur def_value.
 * @param def_value valeur par défault de la matrice
 * @param m matrice à remplir
 */
void init_matrix(double def_value, DMat m);

/**
 * Renvoie la transposée d'une matrice.
 * @param m matrice à transposer
 * @return matrice transposée
 */
DMat matrix_transp(DMat m);

/**
 * Multiplie 2 matrice.
 * @param m1 matrice 1
 * @param m2 matrice 2
 * @return m1 X m2
 */
DMat matrix_mult(DMat m1,DMat m2);

/**
 * Transforme un vecteur en matrice diagonal dont la diagonal est ce vecteur. 
 * @param v le vecteur
 * @param d taille du vecteur
 * @param height hauteur de la matrice
 * @param width largeur de la matrice
 */
DMat vector2diagmatrixinv(double *v,int d,int height,int width);

/**
 * Renvoie la pseudo inverse d'une matrice.
 * @param m matrice a inverser
 * @return pseudo inverse de m 
 */
DMat matrix_inv(DMat m);

/**
 * Change la valeur d'un élément de la matrice.
 *
 * @param m matrice
 * @param row numero de ligne
 * @param col numero de colonne
 * @param value nouvelle valeur
 */
void matrix_set_value(DMat m,int row, int col,double value);

/**
 * Récupère la valeur d'un élément de la matrice.
 *
 * @param m matrice
 * @param row numero de ligne
 * @param col numero de colonne
 * @return valeur m[row][col]
 */
double matrix_get_value(DMat m, int row, int col);

/**
 * Affiche la matrice.
 */
void printmatrix(DMat m);

/**
 * Affiche la matrice, formattee.
 */
void printMatrixClean(DMat m);

/**
 * Choisit le niveau de verbosité.
 */
void set_verbosity( long verb );
