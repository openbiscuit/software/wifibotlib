/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   kdtree.h
 * @brief  Manage Kd-Trees.
 *
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * <br><b>French comments can be found in the source file</b>.
 * Used by:
 */

/**fichier permetant la gestion d'un kdtree (cf [SmartXX] et wikipedia), certaine fonction et variable sont spécifiques à l'utilisation du module d'ia (apprentissage par renforcement) .Mais la structure générale peut être réutilisée pour n'importe quel kdtree.
Un kdtree contient des élements qui sont des KdVector dont la dimension est spécifiée quand on crée l'arbre. A chaque élément on associe une valeur, stockée dans le champ key de KdVector.
*/

#include <stdio.h>

// vecteur un peu amélioré: il contient des élement d'information autre que ses coordonnées.
typedef struct KdVector {int size;double *value;double key;double dist;double weight;}KdVector;

typedef struct KdVectorListp{KdVector *v;struct KdVectorListp *next;struct KdVectorListp *prec;}KdVectorListp;
typedef struct KdVectorList{KdVectorListp *tete;KdVectorListp *queue;int size;}KdVectorList;
typedef struct KdBag{int sizeofbag;KdVectorList *list;}KdBag;// représente une feuille de l'arbre
typedef struct KdNode{struct KdNode *ChildLeft;struct KdNode *ChildRight;int index_dim;double value;int isaLeaf;KdBag *bag;}KdNode;//représente un noeud de l'arbre, index_dim est l'index de la dimension sur laquelle on éffectue la coupure, value la valeur de coupure, bag est un pointeur vers une feuille, dans le cas où le noeud est une feuille.

typedef struct KdTree{KdNode *tete;int dim;int limit_of_bag;double qmin;double qmax;double rmin;double rmax;KdVector *weight;FILE *f;}KdTree;//représente un kdtree, dim est le nombre de dimension des Kdvector,limit of bag, le nombre max de vecteur par feuille,qmin,qmax,rmin,rmax servent pour les calcules d'ia, weight est un vecteur contenant le poid des différente dimension


/** permet de fixer les poids des dimension
* @param t l'arbre sur lequelle on veut apliquer les poids
* @param v vecteur contenant les poids de chacune des dimensions
**/
void KdSetWeight(KdTree *t,KdVector *v);

int KdCompareVector(KdVector *v1,KdVector *v2);

/** permet de recuperer k point proche du point xq (et non les k plus proche, cette fonction est encore "aproximative"
* @param n noeud de l'arbre où débute la recherche
* @param xq point dont on cherche les voisins
* @param k nombres de points désiré
* @param l liste dans laquelle les k point sont insérer
* @return si l'operation à réussi ou non
**/
int KdLookUp(KdNode *n,KdVector *xq,int k,KdVectorList *l);
/** permet de recuperer k point proche du point xq (et non les k plus proche, cette fonction est encore "aproximative"
* @param n noeud de l'arbre où débute la recherche
* @param xq point dont on cherche les voisins
* @param k nombres de points désiré
* @param l liste dans laquelle les k point sont insérer
* @param weight poids pour le calcul de la distance
* @return si l'operation à réussi ou non
**/
int KdLookUpWeighted(KdNode *n,KdVector *xq,int k,KdVectorList *l, KdVector *weight);
double KdKernel(double d,double h);

/** renvoie la distance entre 2 vecteur
* @param v1 vecteur 1
* @param v2 vecteur 2
* @param weight vecteur contenant le poids des dimensions
* @return distance entre les 2 vecteurs
**/
double KdVectorDistance(KdVector *v1,KdVector *v2,KdVector *weight);

/** libère completement un arbre avec les vecteur qu'ils contients
* @param t arbre
*/
void KdFreeTree(KdTree *t);

/**calcule la dimension la plus répendue dans une feuille
* @param b le "sac" de la feuille
* @param nbdim le nombre de dimension de la feuille
* @param w le vecteur de poids des dimensions
* @return index de la dimension la plus répendue
**/
int KdMoreRependDim(KdBag *b,int nbdim,KdVector *w);

/** permet de créer un arbre
* @param dim  nombre de dimension
* @param limit_of_bag nombre de point maximum par feuilles
* @return un pointeur vers l'arbre créer
**/
KdTree *KdNewTree(int dim,int limit_of_bag);

/**permet d'ajouter un vecteur a un arbre
* @param t l'arbre sur lequel on travail
* @param v le vecteur à ajouter
**/
int KdAddVectorToTree(KdTree *t,KdVector *v);

/**permet de créer un vecteur
* @param size taille du vecteur
* @return le vecteur créer
*/
KdVector *KdNewVector(int size);

/**permet de récupérer un vecteur précis
* @param n noeud de départ de la recherche
* @param x vecteur contenant les coordoné du point que l'on cherche
* @param res variable dans laquelle on stoque le résultat(un pointeur vers le vecteur que l'on cherche
**/void *KdGetVect(KdNode *n,KdVector *x,KdVector **res);

/** permet de créer une liste
*/
KdVectorList *KdNewVectorList();
double moy_of_dim(KdBag *b,int dim);
double ecar_of_dim(KdBag *b,int dim,double w);
double min_of_dim(KdBag *b,int dim);
double max_of_dim(KdBag *b,int dim);
void KdSplitNode(KdNode *n,int nbdim,KdVector *w);
void KdFreeNode(KdNode *n);
void KdSetFile(KdTree *t,FILE *f);

KdNode *KdNewNode();
void KdAddVector(KdVectorList *l,KdVector  *v);
int KdAddVectorOrMaj(KdVectorList *l,KdVector  *v);
void KdAddVectorSortByDist(KdVectorList *l,KdVector  *v);

void printVector(KdVector *v);
void printVectorIn(FILE *f,KdVector *v);
void printListVector(KdVectorList *v;);
void KdWriteTree(KdNode *n,FILE *f);

KdVectorListp *KdNewVectorListp(KdVector *v,KdVectorListp *next,KdVectorListp *prec);
void KdFreeVectorList(KdVectorListp *l);
void KdFreeVectorListWithoutVector(KdVectorListp *l);
void KdSetNext(KdVectorListp *kvl,KdVectorListp *next);
void KdSetPrec(KdVectorListp *kvl,KdVectorListp *prec);
void KdFreeVector(KdVector *v);
void printBag(KdBag *b);
void printNode(KdNode *n);
//
/* Ajouté par Alain */
int getTotalSize(KdNode *n);
int debugGetData(KdNode *n, int index, double *xdata, double *ydata, int *indexLeaf);
/**
 * Recherche le point le plus proche de 'target'.
 * Inspiré par [Moore91].
 *
 * @param node Le KdNode où on commence la recherche
 * @param targer le KdVector dont on cherche le plus proche voisin
 * @param closest retourne le point le plus proche
 *
 * @return la distance du meilleur point trouvé ou maxDist
 * @return closest, le meilleur point trouvé ou closest
 */
double KdFindClosestInTree( KdTree *tree, KdVector *target, KdVector **closest );
/**
 * Recherche le point le plus proche de 'target'.
 * Inspiré par [Moore91].
 *
 * @param node Le KdNode où on commence la recherche
 * @param targer le KdVector dont on cherche le plus proche voisin
 * @param closest retourne le point le plus proche
 * @param hyperRectMin les coordonnées mini de l'hyper rectangle actuel de recherche
 * @param hyperRectMax les coordonnées maxi de l'hyper rectangle actuel de recherche
 * @param maxDist la distance max de recherche
 *
 * @return la distance du meilleur point trouvé ou maxDist
 * @return closest, le meilleur point trouvé ou closest
 */
double KdFindClosestInNode(KdNode *node, KdVector *target, KdVector **closest,
		       KdVector *hyperRectMin, KdVector *hyperRectMax,
			 double maxDist );
