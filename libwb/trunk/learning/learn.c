#include <stdio.h>
#include "learn.h"

int db_learn = 0;

double seuil = Seuil;
#define PRED_NORMAL -9
#define PRED_EXACT -10
#define PRED_NOT_ENOUGH -11
#define PRED_IVH -12
#define PRED_BOUNDS -13
#define PRED_EXT -14


void set_debug( long matrixVerbosity, int learnVerbosity )
{
  db_learn = learnVerbosity;
  set_verbosity( matrixVerbosity );
}


int Hedger_training(KdTree *arbre, KdVectorList *ext_states, KdVector *etat, KdVector *etatSuiv,double rewards,int nbPointsPourRegression, double bdePassante, double alpha, double gamma, KdVector *action_possible, double action)
{
  int i;
  int maxind;
  int isinlist=0;
  double valEtatSuiv, qEtat, qEtatNew;
  double maxq, m;
  double mindist;
  int status;
  int result;
  KdVectorList *list_1;
  KdVectorListp *p;
  KdVector *vecEtat;
  KdVectorList *listVoisins = KdNewVectorList();
  list_1 = KdNewVectorList();
  printf("Hedger_training\n");
  
  // mise à jour de rmin et rmax
  if(arbre->rmin > rewards) {
    arbre->rmin = rewards;
    arbre->qmin = arbre->rmin/(1-gamma);
  }
  if(arbre->rmax < rewards) {
    arbre->rmax = rewards;
    arbre->qmax = arbre->rmax/(1-gamma);
  }
  

  if( db_learn > 1 ) {
    printf( "  maxQ pour état_(t+1)\n");
  }
  ///affecter  valEtatSuiv : maximum de q sur toutes les actions à l'état_t+1
  // vecEtat est le point (état+action) où on cherche une approximation
  // pour chaque action (vecEtat->value[vecEtat->size -1] = action_possible->value[i])
  // on calcule sa qValeur par Hedger_prediction(...)
  vecEtat = KdNewVector(etatSuiv->size +1);
  for(i=0;i<etatSuiv->size;i++) {
    vecEtat->value[i] = etatSuiv->value[i];
  }
  
  // pour l'action 0
  vecEtat->value[vecEtat->size -1] = action_possible->value[0]; /// pe,ser a remetre value[0] a la fin des test

  maxq = Hedger_prediction(arbre, ext_states, vecEtat, bdePassante, nbPointsPourRegression,list_1, &mindist, &status);
  maxind = 0;
  KdFreeVectorListWithoutVector(list_1->tete);
  list_1->tete = NULL;
  list_1->size=0;
  list_1->queue = NULL;
  if( db_learn > 1 ) {
    printf("  action %d: Q(s,a)= %f\n",0,maxq);
  }
  // pour les autres actions 
  for(i=1;i<action_possible->size;i++)
    {
      vecEtat->value[vecEtat->size -1] = action_possible->value[i];
      if((m=Hedger_prediction(arbre, ext_states, vecEtat, bdePassante, nbPointsPourRegression, list_1, &mindist, &status))>maxq) {
	maxq = m;
	maxind = i;
      }
      if( db_learn > 1 ) {
	printf("  action %d: Q(s,a)= %f\n",i,m);
      }
      KdFreeVectorListWithoutVector(list_1->tete);
      list_1->tete = NULL;
      list_1->size=0;
      list_1->queue = NULL;
    }
  free(list_1);
  // valEtatSuiv c'est max_a{q(s,a)}
  valEtatSuiv = maxq;
  
  

  ///affecter qt = Q(s,a)
  // On calcule la qValeur de (s,ac)
  for(i=0;i<etatSuiv->size;i++)
    {
      vecEtat->value[i] = etat->value[i];
    }
  vecEtat->value[vecEtat->size -1] = action;  
  //printf("on calcul le qt\n");
  mindist = 1000000000000000.0;
  qEtat = Hedger_prediction(arbre, ext_states, vecEtat, bdePassante, nbPointsPourRegression, listVoisins, &mindist, &status);
  if( db_learn > 1 ) {
    printf("  qEtat = %f\n", qEtat);
  }
  
  // qEtatNew nouvelle valeur de Q(s,ac)
  qEtatNew = alpha*( rewards + gamma* valEtatSuiv - qEtat) + qEtat;
  if( db_learn > 0 ) {

    printf("  rewards : %f\n",rewards);
    printf("  qEtatNew = %f, valEtatSuiv : %f, qEtat %f\n", qEtatNew, valEtatSuiv, qEtat );
  }
  p=listVoisins->tete;
  vecEtat->key = qEtatNew;

  // Mise à jour des valeurs des points de l'arbre
  //
  // si 'etatSuiv' est un etat extérieur, on ne remet à jour que sa valeur
  // cherche si etatSuiv est dans ext_states
  if( ext_states != NULL ) {
    p = ext_states->tete;
    while( p!=NULL ) {
      if( KdCompareVector(vecEtat, p->v) ) {
	// dans la liste des ext_states
	p->v->key = qEtatNew;
	if( db_learn > 0 ) {
	  printf("  newQ : pt dans la list_ext\n");
	  printf("renvoie best_action : %d\n",maxind);
	}
	return maxind;
      }
      p = p->next;
    }
  }
  
  // sinon (etatSuiv n'est pas dans la liste), on agit "normalement"
  if ( db_learn > 1) {
    printf("  newQ : pt_normal\n");
  }
  p=listVoisins->tete;
  while(p!=NULL && !isinlist ) {
    
    if( db_learn > 1 ) {
      printVector(p->v);
      
      printf("  Pt ");
      printf(" dans liste listVoisin ->");
    }
    
    if(KdCompareVector(p->v, vecEtat)) {
      // si le point etatSuiv est dans l'arbre, on change sa valeur
      if( db_learn > 1 ) {
	printf( " point cible DONC modifié!\n");
      }
      p->v->key = qEtatNew;
      isinlist = 1;
    }
    else {
      // on modifie aussi ses voisins
      if( db_learn > 1 ) {
	printf( " point voisin");
      }
      //if(qt!=QDEF)
      if ((status == PRED_NORMAL) || (status == PRED_EXACT)) 
	{
	  p->v->key = alpha *(p->v->weight)*(qEtatNew-p->v->key) + p->v->key;

	  if( db_learn > 1 ) {
	    printf( " modifié!");
	    printf(" p->v->weight %f ",p->v->weight);
	    printf( " new = %f ",p->v->key);
	  }
	}
      if( db_learn > 1 ) {
	printf( "\n");
      }
    }
    p=p->next;
    
  }
  
  // si le point etatSuiv est dans la liste (donc dans l'arbre)
  if(isinlist) {
    // on fait rien de plus
    if( db_learn > 0 ) {
      printf("  MAJ : rien car pt dans arbre\n");
    }
    KdFreeVector(vecEtat);
  }
  else {
    // sinon on l'ajoute (si pas trop près d'un autre)
    if( db_learn > 0 ) {
      printf( "  MAJ: Ajoute nouveau point dans arbre (%f)? ",mindist);
    }
    if(mindist > seuil) {
      result = KdAddVectorToTree(arbre, vecEtat);
      if( db_learn > 0 ) {
	printf( " OUI (%d)",result );
	printVector(vecEtat);
      }
      
    }
    if( db_learn > 0 ) {  
      printf("\n");
    }
    if( db_learn > 1 ) {
      printf("-----Arbre-----\n");
      printNode( arbre->tete );
    }
  }
  
  KdFreeVectorListWithoutVector(listVoisins->tete);
  free(listVoisins);
  
  if( db_learn > 0 ) {
    printf("best_action : %d\n",maxind);
  }
  return maxind;
  
}
/*
 * Modified so that '*status' explains what happens.
 */
double Hedger_prediction(KdTree *arbre, KdVectorList *ext_states, KdVector *pointCible, double bdePassante, int nbPointsPourRegression, KdVectorList *listVoisins, double *mindist, int *status)
{
  DMat K;
  DMat V;
  DMat KT;
  DMat KTK;
  DMat KTK_inv;
  DMat X;
  DMat XT;
  DMat H;
  DMat temp1;
  DMat temp2;
  KdVectorListp *p;
  int i=0,j;
  double maxvi;
  double res=QDEF;
  *status = PRED_NORMAL;
  //KdVector *vec=NULL;
  //on init K

  if( db_learn > 0 ) {
    printf( "\tHedger_prediction\n");
  }
  
  // cherche si pointCibles n'est pas dans ext_state
  if( ext_states != NULL ) {
    p = ext_states->tete;
    while( p!=NULL ) {
      if( KdCompareVector( pointCible, p->v) ) {
	// dans la liste des ext_states, renvoie valeur "fixee"
	if( db_learn > 0 ) {
	  printf("\t point externe\n");
	} 
	*status = PRED_EXT;
	return p->v->key;
      }
      p = p->next;
    }
  }

  // La feuille contenant pointCible.
  // Cela génère aussi la liste des points Voisins, mais les diverses dimensions
  // ne sont pas valuées dans le calcul de la distance
  // Question : pourquoi ne pas prendre les "weight" de l'arbre ???
  // Alain => modifié en ce sens.
  //KdLookUp(arbre->tete, pointCible, nbPointsPourRegression, listVoisins);
  KdLookUpWeighted(arbre->tete, pointCible, nbPointsPourRegression, listVoisins, arbre->weight);
  p = listVoisins->tete;
  while(p!=NULL)
    {
      if( db_learn > 1 ) {
	printf( "\t // cherche si point exact !\n");
      }
      if(KdCompareVector(pointCible,p->v))
	{
	  if( db_learn > 0 ) {
	    printf("\t point exact\n");
	  }
	  *status = PRED_EXACT; // rien
	  return p->v->key;
		
	}
      p = p->next;
	
    }
  
  if( db_learn > 1 ) {
    printf( "\t // assez de points ?\n");
  }
  if(listVoisins->size < nbPointsPourRegression)
    {
      if( db_learn > 0 ) {
	printf("\t  pas assez de point %d (%d)\n", nbPointsPourRegression, listVoisins->size);
      }
      *status = PRED_NOT_ENOUGH; //rien
      res = QDEF;
    }
  else
    {
      if( db_learn > 1 ) {
	printf( "\t  // cherche si xp est dans l'enveloppe convexe (ivh)\n");
      }
      K = svdNewDMat(listVoisins->size, listVoisins->tete->v->size+1);
      p = listVoisins->tete;
      while(p!=NULL)
	{
	  for(j=0;j<p->v->size;j++)
	    {
	      K->value[i][j] = p->v->value[j];
	    }
	  // Ajout d'une composante pour valeur à l'origine
	  K->value[i][j] = 1.0;
	  p=p->next;
	  i++;
	}
      if( db_learn > 1 ) {
	printf( "\t   //on init X\n");
      }
      X = svdNewDMat(pointCible->size+1,1);
      for(j=0;j<pointCible->size;j++)
	{
	  X->value[j][0] = pointCible->value[j];
	}
      // Ajout d'une composante pour valeur à l'origine
      X->value[j][0] = 1.0;
      if( db_learn > 1 ) {
	printf( "\t   //calcule de l'ivh\n");
      }
      KT = matrix_transp(K);
      KTK = matrix_mult(KT,K);
      KTK_inv = matrix_inv(KTK);
      temp1 = matrix_mult(KTK_inv,KT);
      V = matrix_mult(K,temp1);
      maxvi = matrix_max_diag(V);
      if( db_learn > 1 ) {
	printf("\t   maxvi : %f\n",maxvi);
      }

      XT = matrix_transp(X);
      temp2 = matrix_mult(KTK_inv,X);
      H = matrix_mult(XT,temp2);

      if( db_learn > 2 ) {
	printf( "-- matrice inv(KT.K).KT\n" );
	printMatrixClean(temp1);
	printf( "-- matrice V\n" );
	printMatrixClean(V);
	printf( "-- matrice X\n" );
	printMatrixClean(X);
	printf( "-- matrice H\n");
	printMatrixClean(H);
      }
	
      if(H->value[0][0] <= maxvi)
	{
	  // le point x est dans la zone ivh
	  if( db_learn > 1 ) {
	    printf("\t   on est dans l'ivh\n");
	  }
	  res = LWR_PREDICTION(listVoisins ,pointCible, bdePassante,  nbPointsPourRegression, arbre->weight, mindist);
	  if((res < arbre->qmin)||(res > 10*arbre->qmax))
	    {
	      if( db_learn > 0 ) {
		printf("\t   mais c'est en dehors des bornes (%f) <> [%f, %f]\n",res, arbre->qmin, 10*arbre->qmax);
	      }
	      *status = PRED_BOUNDS; //rien
	      res = QDEF;
	    }
	}
      else
	{
	  if( db_learn > 0 ) {
	    printf("\t   on est pas dans l'ivh");
	  }
	  *status = PRED_IVH; //rien
	  res = QDEF;

	}

      //on libère tout
      svdFreeDMat(K);
      svdFreeDMat(V);
      svdFreeDMat(KT);
      svdFreeDMat(KTK);
      svdFreeDMat(KTK_inv);
      svdFreeDMat(X);
      svdFreeDMat(XT);
      svdFreeDMat(H);
      svdFreeDMat(temp1);
      svdFreeDMat(temp2);

    }
  if( db_learn > 0 ) {
    printf("\tfin hedger predic\n");
  }
  return res;
}

double LWR_PREDICTION(KdVectorList *listVoisin, KdVector *pointCible, double bdePassante, int nbPointsPourRegression, KdVector *weight, double *mindist)
{

  int i=0,j=0;
  //double d;
  //double k;
  double res;
  //double min_dist;
  KdVectorListp *p;
  //KdVectorListp *p2;
  KdVectorList *list2;
  KdVectorList *list3;
  //printListVector(l);
  //printf("lwr predic\n");

  // matrix row x col
  DMat A = svdNewDMat(listVoisin->size, listVoisin->tete->v->size + 1);
  DMat B = svdNewDMat(listVoisin->size,1);
  DMat X;
  //DMat Z;
  DMat pinvA;
  DMat estB;
  //DMat AT;
  //DMat ATB;
  /*printf("LWR_PRED with h=%6.4f\n", h);*/


  // Trie les vecteurs de 'listVoisin' et calcule la distance minimale à xp.
  p = listVoisin->tete;
  list2 = KdNewVectorList();
  *mindist = KdVectorDistance(p->v, pointCible, weight);

  while(p!=NULL) {
    p->v->dist = KdVectorDistance(p->v, pointCible, weight);
    if(p->v->dist < *mindist) {
      *mindist = p->v->dist;
    }
    KdAddVectorSortByDist(list2,p->v);
    p = p->next;
  }

  // 'list3' est composée, au maximum, des 'nbPointsPourRegression' vecteurs les plus proches de pointCible.
  p=list2->tete;
  list3 = KdNewVectorList();
  while(p!=NULL && i<nbPointsPourRegression)
    {
      KdAddVector(list3,p->v);
      p=p->next;
      i++;
    }
  KdFreeVectorListWithoutVector(list2->tete);
  free(list2);

  // debug 2
  if( db_learn > 1 ) {
    printf("-- %d vecteurs les plus proches\n", nbPointsPourRegression);
    printListVector( list3 );
  }
  // end_debug 2

  // Prépare les matrices A et B
  // A composée des examples (pondérés par poids) et B de leur qValeur
  p=list3->tete;
  i=0;
  while(p!=NULL) {
    p->v->weight = KdKernel(p->v->dist, bdePassante);
    
    for(j=0;j<p->v->size;j++) {
      A->value[i][j] = p->v->value[j] * p->v->weight;
    }
    A->value[i][j] = 1;
    
    B->value[i][0] = p->v->key;
    
    i++;
    p = p->next;
  }
  KdFreeVectorListWithoutVector(list3->tete);
  free(list3);  

  // Calcul de la pseudo-inverse
  pinvA = matrix_inv(A);

  // debug 1
  if( db_learn > 0 ) {
    printf( "-- matrice A\n" );
    printMatrixClean(A);
    printf( "-- matrice B\n" );
    printMatrixClean(B);
    printf( "-- matrice pinvA\n");
    printMatrixClean(pinvA);
  }
  // end_debug 1
  
  if(!is_empty_matrix(pinvA)) {
      // Si la pseudo inverse est non-nulle.
      X = matrix_mult(pinvA, B);	

      // debug 1
      if( db_learn > 0 ) {
	estB = matrix_mult(A, X);
	printf( "-- estimation de B\n");
	printMatrixClean( estB );
	svdFreeDMat( estB );
      }

      // produit de X par pointCible
      res = 0;	
      for(i=0;i<pointCible->size;i++) {
	res+= X->value[i][0] * pointCible->value[i];
      }
      res+= X->value[i][0] * 1.0;

      svdFreeDMat(X);
    }
  else {
      // sinon on fait la moyenne des qValeurs des examples
      res=0;

      for(i=0; i<B->rows; i++) {
	  res+= B->value[i][0];		
	}
      res = res/(B->rows);

    }

  svdFreeDMat(A);
  svdFreeDMat(B);
  svdFreeDMat(pinvA);

  //printf("resultat de la regression : %f\n",res);
  return res;
}
