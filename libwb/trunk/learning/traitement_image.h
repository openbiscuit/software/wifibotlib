/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   traitement_image.h
 * @brief  Méthodes pour trouver la cible dans l'image.
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * La function principale est 'chercher_cible()' qui renvoie une structure
 * de type 'target'.
 */

typedef struct target
{
  int xpos;int ypos;
  int largeur;int hauteur;
  int center;
} target;

target chercher_cible();


