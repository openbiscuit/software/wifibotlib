#include "LibRemote.h"
#include <math.h>

#define GREEN "192.168.0.21"
#define BLUE "192.168.0.20"
#define YELLOW "192.168.1.100" 

#define OBSERVER1_IP GREEN
#define OBSERVER2_IP BLUE
//#define OBSERVER2_IP "192.168.1.115"
#define ACTOR_IP YELLOW

#define ROBOT_SIZE 250 //in pixel
#define PI 3.1416

robot * p_observer1;
robot * p_observer2;
robot * p_actor;


//all these are angle found by the observer. They are to be in degree !
int observer1_actor_init_angle;
int observer1_actor_end_angle;
int observer1_cones_angle;
int observer1_observer2_angle;
int observer2_actor_init_angle;
int observer2_actor_end_angle;
int observer2_cones_angle;
int observer2_observer1_angle;

//server as reference for the object searched by observer1
list_object * observer1_list;

//server as reference for the object searched by observer2
list_object * observer2_list;


double angleModulo(double angle)
{
	if(angle > 180)
		angle = angle - 360;
	if(angle < -180)
		angle = angle + 360;
	
	return angle;
}

/**
 * compute an angle (counterclockwise positiv), first one being the 'from' angle.
 * @param angle1 from angle (in degree)
 * @param angle2 to angle (in degree).
 * @return angle between both angle.
 */
int computeAngle(int angle1, int angle2)
{
	int result;
	result = angle1 - angle2;

	return angleModulo(result);
}

/**
 * compute in which cadran is the first angle, all angle are in radian.
 * @return real phi in radian.
 */
double computeRealPhi(double phi, double alpha1, double alpha2)
{
	if(alpha2 - alpha1 < 0)//neg
	{
		if(alpha1 > PI/2) //quad 1
		{
			if(phi < alpha1 - PI) 
				phi += PI;
		}
		else if(alpha1 < -PI/2)//quad 4
		{
			if(phi < alpha1 + PI)
				phi +=PI;
		}
		else if(alpha1 > 0)//quad 2
		{
			if(phi > alpha1)
				phi -= PI;
		}
		else//quad 3
		{
			if(phi > alpha1)
				phi -= PI;
		}
	}
	else//pos
	{
		if(alpha1 > PI/2)//quad 1
		{
			if(phi > alpha1 - PI)
				phi -= PI;
		}
		else if(alpha1 < -PI/2)//quad 4
		{
			if(phi > alpha1 + PI)
				phi -= PI;
		}
		else if(alpha1 > 0)//quad 2
		{
			if(phi < alpha1)
				phi += PI;
		}
		else//quad 3
		{
			if(phi < alpha1)
				phi += PI;
		}
	}
	return phi;
}


/**
 * angles should be a size 8 array, 4 first values should be measure from observer 1
 * four other from observer 2. Values are to be in this order : cone angle, initial actor
 * angle, final actor angle and observer angle.
 * @return angle value of the direction in radian.
 */
double computeDirection()
{
	//positif = sens trigo
	double alpha1, alpha2, beta1, beta2, tan_phi_num, tan_phi_den, phi;
	double gamma, tan_gamma_den, tan_gamma_num, direction;
	double obs1_cone_angle;
	double to_add;

	obs1_cone_angle = computeAngle(observer1_observer2_angle, observer1_cones_angle);
	//if angle obs2->obs1->cones is negativ, cones are on the right of observer1
	//right means on the right of the line starting from obs1 to obs2
	//if cones are on right for obs1 then cones are on the left for obs2 (left on the line starting from obs2 to obs1)
	//et vice-versa ...
	if(obs1_cone_angle < 0)
		to_add = 90;
	else
		to_add = -90;
		
	//computing direction actor's final position -> cone	
	alpha1 = computeAngle(observer1_observer2_angle, observer1_actor_end_angle);
	alpha1 += to_add; 
	alpha1 = angleModulo(alpha1);
	alpha1 *= PI/180.0;
	alpha2 = computeAngle(observer1_observer2_angle, observer1_cones_angle);
	alpha2 += to_add;
	alpha2 = angleModulo(alpha2);
	alpha2 *= PI/180.0;

	beta1 = computeAngle(observer2_observer1_angle, observer2_actor_end_angle);
	beta1 -= to_add;
	beta1 = angleModulo(beta1);
	beta1 *= PI/180.0;
	beta2 = computeAngle(observer2_observer1_angle, observer2_cones_angle);
	beta2 -= to_add;
	beta2 = angleModulo(beta2);
	beta2 *= PI/180.0;

	tan_gamma_num = ((tan(alpha2) + tan(beta2))/(tan(alpha2) - tan(beta2))) - ((tan(alpha1) + tan(beta1))/(tan(alpha1) - tan(beta1)));
	tan_gamma_den = (2/(tan(alpha2) - tan(beta2))) - (2/(tan(alpha1)-tan(beta1)));
	gamma = atan(tan_gamma_num/tan_gamma_den);
	gamma = computeRealPhi(gamma, alpha1, alpha2);


	//computing actor's direction (actor_init -> actor_end)
	alpha1 = computeAngle(observer1_observer2_angle, observer1_actor_init_angle);
	alpha1 += to_add;
	alpha1 = angleModulo(alpha1);
	alpha1 *= PI/180.0;
	alpha2 = computeAngle(observer1_observer2_angle, observer1_actor_end_angle);
	alpha2 += to_add;
	alpha2 = angleModulo(alpha2);
	alpha2 *= PI/180.0;
	
	beta1 = computeAngle(observer2_observer1_angle, observer2_actor_init_angle);
	beta1 -= to_add;
	beta1 = angleModulo(beta1);
	beta1 *= PI/180.0;
	beta2 = computeAngle(observer2_observer1_angle, observer2_actor_end_angle);
	beta2 -= to_add;
	beta2 = angleModulo(beta2);
	beta2 *= PI/180.0;
	//version bonne
	tan_phi_num = ((tan(alpha2) + tan(beta2))/(tan(alpha2) - tan(beta2))) - ((tan(alpha1) + tan(beta1))/(tan(alpha1) - tan(beta1)));
	tan_phi_den = (2/(tan(alpha2) - tan(beta2))) - (2/(tan(alpha1)-tan(beta1)));
	phi = atan(tan_phi_num/tan_phi_den);
	phi = computeRealPhi(phi, alpha1, alpha2);
	
//	direction = (PI - phi - gamma);
	direction = gamma - phi;
	while(direction < -(2*PI))
		direction += 2*PI;
	while(direction > (2*PI))
		direction -= 2*PI; 
	printf("acteur tourne :\ncap actuel %f, direction des plots %f, tourne de %f\n", phi, gamma, direction);
	return direction;
	/*
 	alpha1 = computeAngle(observer1_observer2_angle, observer1_actor_init_angle);
	alpha1 += (alpha1 < 0)? 90 : -90;
	alpha1 *= PI/180.0;
	alpha2 = computeAngle(observer1_observer2_angle, observer1_actor_end_angle);
	alpha2 += (alpha2 < 0)? 90 : -90;
	alpha2 *= PI/180.0;
	beta1 = computeAngle(observer2_observer1_angle, observer2_actor_init_angle);
	beta1 += (beta1 < 0)? -90 : 90;
	beta1 *= PI/180.0;
	beta2 = computeAngle(observer2_observer1_angle, observer2_actor_end_angle);
	beta2 += (beta2 < 0)? -90 : 90;
	beta2 *= PI/180.0;
	//version bonne
	tan_phi_num = ((tan(alpha2) + tan(beta2))/(tan(alpha2) - tan(beta2))) - ((tan(alpha1) + tan(beta1))/(tan(alpha1) - tan(beta1)));
	tan_phi_den = (2/(tan(alpha2) - tan(beta2))) - (2/(tan(alpha1)-tan(beta1)));
	phi = atan(tan_phi_num/tan_phi_den);
	phi = computeRealPhi(phi, alpha1, alpha2);
	
	//comuting direction cone -> actor final position
	alpha1 = computeAngle(observer1_observer2_angle, observer1_cones_angle);
	alpha1 += (alpha1 < 0)? 90 : -90;
	alpha1 *= PI/180.0;
	alpha2 = computeAngle(observer1_observer2_angle, observer1_actor_end_angle);
	alpha2 += (alpha2 < 0)? 90 : -90;
	alpha2 *= PI/180.0;
	beta1 = computeAngle(observer2_observer1_angle, observer2_cones_angle);
	beta1 += (beta1 < 0)? -90 : 90;
	beta1 *= PI/180.0;
	beta2 = computeAngle(observer2_observer1_angle, observer2_actor_end_angle);
	beta2 += (beta2 < 0)? -90 : 90;
	beta2 *= PI/180.0;

	tan_gamma_num = ((tan(alpha2) + tan(beta2))/(tan(alpha2) - tan(beta2))) - ((tan(alpha1) + tan(beta1))/(tan(alpha1) - tan(beta1)));
	tan_gamma_den = (2/(tan(alpha2) - tan(beta2))) - (2/(tan(alpha1)-tan(beta1)));
	gamma = atan(tan_gamma_num/tan_gamma_den);
	gamma = computeRealPhi(gamma, alpha1, alpha2);
	
//	direction = (PI - phi - gamma);
	direction = (- phi - gamma);
	while(direction < -(2*PI))
		direction += 2*PI;
	while(direction > (2*PI))
		direction -= 2*PI; 
	return direction;
	*/
	//omega1 = -180 + beta2 - (beta1 - alpha1) * (PI/180.0);
}

void initObserver1Object()
{
	object * actor, * observer2, * cone1;
	list_object * l_actor, * l_observer2, * l_cone1;
	
	actor = initObject("actor");
	
	observer2 = initObject("observer");
	
	cone1 = initObject("cone");	
		
	actor->hue = 50;
	actor->delta_color = 10;
	actor->allow_discontinuity = FALSE;
	actor->min_lightness = 160;
	actor->min_saturation = 80;
	l_actor = initListObject(actor);

	observer1_list = l_actor;	
	
	observer2->hue = 210;
	observer2->allow_discontinuity = FALSE;
	observer2->delta_color = 10;
	observer2->min_lightness = 90;
	//observer2->max_lightness = 300;
	observer2->min_saturation = 100;
	//observer2->max_saturation = 300;
	l_observer2 = initListObject(observer2);

	addObjectToList(l_observer2, &observer1_list);
	
	cone1->hue = 353;
	cone1->allow_discontinuity = FALSE;
	cone1->delta_color = 10;
//	cone1->max_lightness = 230;
	cone1->min_lightness = 120;
//	cone1->max_saturation = 220;
	cone1->min_saturation = 120;
	l_cone1 = initListObject(cone1);

	addObjectToList(l_cone1, &observer1_list);

}


void initObserver2Object()
{
	object * actor, * observer1, * cone1;
	list_object * l_actor, * l_observer1, * l_cone1;
	
	actor = initObject("actor");
	
	observer1 = initObject("observer");
	
	cone1 = initObject("cone");
		
	actor->hue = 60;
	actor->delta_color = 10;
	actor->min_lightness = 120;
	actor->min_saturation = 150;
	l_actor = initListObject(actor);

	observer2_list = l_actor;	
	
	observer1->hue = 90;
	observer1->delta_color = 10;
	observer1->min_lightness = 250;
	observer1->min_saturation = 210;
	l_observer1 = initListObject(observer1);

	addObjectToList(l_observer1, &observer2_list);
	
	cone1->hue = 355;
	cone1->allow_discontinuity = FALSE;
	cone1->delta_color = 10;
//	cone1->max_lightness = 230;
	cone1->min_lightness = 140;
//	cone1->max_saturation = 220;
	cone1->min_saturation = 130;
	l_cone1 = initListObject(cone1);
	l_cone1 = initListObject(cone1);

	addObjectToList(l_cone1, &observer2_list);
}

/**
 * Search given object and return a list with all found object with their value set accordingly.
 * Note that the reference list may change : first the x_position of each object will be updated. And
 * in case the hue, lightness and saturation values were changed, the reference list will also have this change.
 * @param observer this is the robot which will search the object.
 * @param reference_list list where the wanted aobject will be drawn from.
 * @param names of the object that we search, those name will be used to retrieve corresponding object in the
 * reference_list.
 * @param name_count size of the array 'names' (for example, an array with two value should have name_count set to 2).
 * @return a list with all the wanted object and with correct values.
 */
 list_object * searchObject(robot * observer, list_object * reference_list, char * names[], int name_count)
{
	object * tmp_object;
	int index;
	list_object * missing_object_list =  NULL, * found_object_list = NULL;
	list_object * final_object_list = NULL;
	list_object * tmp_list_object;
			
	//constructing list of object from reference list
	for(index = 0; index < name_count; index++)
	{
		tmp_object = findObject(names[index], reference_list);
		tmp_list_object = initListObject(copyObject(tmp_object));
		addObjectToList(tmp_list_object, &missing_object_list);
	}
	
	//while there is still object missing, we keep looking for them
	while(missing_object_list != NULL)
	{
		found_object_list = RemoteFindAndCenter(observer, &missing_object_list, TRUE);
		appendList(&final_object_list, found_object_list);
		//missing_object_list will hold the object missing so scanning through it
		tmp_list_object = missing_object_list;
		while(tmp_list_object != NULL)//for every object not found, we ask user to enter new value to find it
		{
			char * object_name = tmp_list_object->item->name;
			freeObject(tmp_list_object->item);
			//for every objects missing, we ask for changing their values
			printf("object %s can't be found, constructing it anew\n", object_name);
			tmp_list_object->item = constructObject(object_name);

			tmp_list_object = tmp_list_object->next;
		}
		//now ready for another turn, if there is still object in the missing_list, we continue to look for them		
	}//when this loop end, missing_object_list is empty!
	
	//we change the object in the reference list so that x position is set in the reference
	//also hue, lightness and saturation may have change
	tmp_list_object = final_object_list;
	
	while(tmp_list_object != NULL)
	{
		tmp_object = replaceObject(copyObject(tmp_list_object->item), reference_list);
		freeObject(tmp_object);
		tmp_list_object = tmp_list_object->next;
	}
	return final_object_list;
}

/**
 * This will search for cone and observer, for both observer.
 */
void searchStaticObject()
{
	char * object_names[] = {"observer", "cone"};
	list_object * new_list;
	object * tmp_object;
	int observer1_cones[2], observer2_cones[2];
	
	// ******************observer1
	new_list = searchObject(p_observer1, observer1_list, object_names, 2);
	freeList(new_list);
	
	//setting angle value from found objects
	tmp_object = findObject("observer", observer1_list);
	observer1_observer2_angle = tmp_object->x_position;

	tmp_object = findObject("cone", observer1_list);
	observer1_cones[0] = tmp_object->x_position;

	tmp_object = copyObject(findObject("cone", observer1_list));
	RemoteMoveCameraTo(p_observer1, tmp_object->x_position, p_observer1->cam_y);
	while(RemoteFindAndCenterOnSecondObject(p_observer1, tmp_object, TRUE, FALSE) == -1)
	{
		printf("cone 2 can't be found, constructing it anew\n");
		freeObject(tmp_object);
		tmp_object = constructObject("cone");
	}
	observer1_cones[1] = tmp_object->x_position;
	observer1_cones_angle = meanOnRoundRange(observer1_cones, 2, 360);
	
	//changing cone values in case they changed
	tmp_object = replaceObject(tmp_object, observer1_list);
	freeObject(tmp_object);
	
	
	// ******************observer2
	new_list = searchObject(p_observer2, observer2_list, object_names, 2);
	freeList(new_list);
	//setting angle value from found objects
	tmp_object = findObject("observer", observer2_list);
	observer2_observer1_angle = tmp_object->x_position;

	tmp_object = findObject("cone", observer2_list);
	observer2_cones[0] = tmp_object->x_position;	

	tmp_object = copyObject(findObject("cone", observer2_list));
	RemoteMoveCameraTo(p_observer2, tmp_object->x_position, p_observer2->cam_y);
	while(RemoteFindAndCenterOnSecondObject(p_observer2, tmp_object, TRUE, FALSE) == -1)
	{
		printf("cone 2 can't be found, constructing it anew\n");
		tmp_object = constructObject("cone");
	}
	observer2_cones[1] = tmp_object->x_position;
	observer2_cones_angle = meanOnRoundRange(observer2_cones, 2, 360);

	//changing cone values in case they changed
	tmp_object = replaceObject(tmp_object, observer2_list);
	freeObject(tmp_object);	
}

/**
 * This will search the actor once for both observer, then make the actor move, then search for it again.
 */
void searchActor(int iteration)
{
	object * tmp_object;
	char * names[] = {"actor"};
	list_object * tmp_list;
	double angle;

	// **************initial actor's position
	
	//making the actor move
	//if this is the first time we measure actor's positions then we'll search for its initial position
	if(iteration == 1)
	{
		tmp_list = searchObject(p_observer1, observer1_list, names, 1);
		freeList(tmp_list);	
		tmp_list = searchObject(p_observer2, observer2_list, names, 1);
		freeList(tmp_list);
			
		tmp_object = findObject("actor", observer1_list);
		observer1_actor_init_angle = tmp_object->x_position;
		tmp_object = findObject("actor", observer2_list);
		observer2_actor_init_angle = tmp_object->x_position;
	}
	else
	{
		//if a measure was already made, then the last actor's position will be initial position
		//for new measure 
		observer1_actor_init_angle = observer1_actor_end_angle;
		observer2_actor_init_angle = observer2_actor_end_angle;
	}
	
	RemoteAvancerCourbe(p_actor, 5, 5);
	sleep(2);
	RemoteStop(p_actor);
	
	// **************final actor's position
	tmp_list = searchObject(p_observer1, observer1_list, names, 1);
	freeList(tmp_list);	
	tmp_list = searchObject(p_observer2, observer2_list, names, 1);
	freeList(tmp_list);
	
	tmp_object = findObject("actor", observer1_list);
	observer1_actor_end_angle = tmp_object->x_position;
	tmp_object = findObject("actor", observer2_list);
	observer2_actor_end_angle = tmp_object->x_position;
		
	angle = computeDirection();
	//angle is computed in radian, RemoteTurnOf takes degree
	angle = angle * 180.0/PI;
	RemoteTurnOf(p_actor, angle * 1.2);
}

/**
 * This is the whole thing
 */
void protocole()
{
	int iteration = 1;
	
	initObserver1Object();
	initObserver2Object();
	
	searchStaticObject();
		
	do
	{
		searchActor(iteration);
/*		angle = computeDirection();
		angle = angle * 180.0/PI;
		RemoteTurnOf(p_actor, angle);
		RemoteAvancerCourbe(p_actor, 5, 5);
		sleep(4);
		RemoteStop(p_actor);*/
		iteration++;
	}
	while(iteration);
}


int main()
{
	/*
	* angles should be a size 8 array, 4 first values should be measure from observer 1
 	* four other from observer 2. Values are to be in this order : cone angle, initial actor
 	* angle, final actor angle and observer angle.
 	*/
	p_actor = initRobot(ACTOR_IP); 
	
	p_observer1 = initCameraOnly(OBSERVER1_IP);
//	testObserver1();
	
	//p_observer2 = initRobot(OBSERVER2_IP);
	p_observer2 = initCameraOnly(OBSERVER2_IP);
//	observer2_actor = observer2_cones = observer2_observer1 = p_observer2->cam_x;
	
//	testObserver2();

	protocole();
	freeList(observer2_list);
	freeList(observer1_list);
/*	freeObject(observer1_cone2);
	freeObject(observer2_cone2);
	freeList(observer1_actor_list);
	freeList(observer2_actor_list);*/
	RemoteDisconnect(p_actor);
	RemoteDisconnect(p_observer1);
	RemoteDisconnect(p_observer2);
	free(p_actor);
	free(p_observer1);
	free(p_observer2);

	return 1;
}
