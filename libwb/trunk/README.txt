** libwb **
***********

'libwb' is a low level library for using and controling a set of WifiBot.
Under 'robosoft' there are some examples applications found on the
ROBOSOFT cdrom or WIFIBOT website (www.wifibot.com).

10/08/2007

* Layout
--------

README.txt			this file
TODO.txt			what seems important to do, add.

+ doc				documentation
|  |
|  + tex			Latex source files
|
+ src				source file for libwb
|  Makefile			to compile libwb
|
|
+ include			include files for local library 
|  |				(like libjpeg)
|  |
|  + libwb			include files for libwb library
|
+ lib				library files
|				(external like libjpeg) and created
|
+ dist				needed to set up released packages
|
|
+ learning
|  kdtree.c/h/py		implements a KdTree, with Python interface
|  calcul.c/h	                matrix calcululs
|  learn.c/h                    Reinf. Learning on continous domain
|  LibRemote.c/h		connect with wifi and remote control
|  traitement_image.c/h		(work) detect target in image
|  simu.c			(work) interface for piloting learning
|  data_final			an example of good experience points for learning
|  data				file where experience is stored
|
| 
+ demo				various small demo programs
| testCamera			a simple test of the camera;
|  braitenberg			the wifibot moves like a Braitenberg Bot.
|  stop				the wifibot stops.
|  remote			a little server to remote the wifibot
|  client			the client to connect on remote
|  |
|  + suitBalle			the robot camera follows a pink ball.
|     simpleServer		example of a server on the robot side
|     simpleClient		example of associated client
|     suitBalleServer		onboard demo of ball following + server
|     viewer			associated viewer
|     debugSize			debug software for data compatibility test
|     debugTraitement		debug software for testing 'suitBalleServer'
|
|
+ robosoft			various simple client/server applications
   |				from ROBOSOFT / WIFIBOT
   |
   + SimpleServer		simple_server on the robot
   |
   |
   + SimpleGUI			some client examples
     |
     + Common			common TCPSocket functions
     |
     + robot-client		controls the robot via socket
     |
     + video-client		displays camera images via socket


* authors
------------------
- Nicolas Beaufort, summer 2007 in the MAIA team of Loria
- Julien Le Guen and Cedric Bernier, summer 2006 in the MAIA team of Loria. 
- Alain Dutech, research fellow, MAIA team of Loria.
- Someone at WIFIBOT for the robosoft software.
