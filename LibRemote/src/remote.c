/**
 * Programme permettant de téléguider le wifibot. 
 * S'utilise ave la LibRemote.
 * */
#include "remote.h"

extern int socket_decoute;

int socket_ecoute(int port,struct sockaddr_in *local)
{
	int res;
	
	if((res=socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("erreur socket");
		return -1;
	}
	bzero(local, sizeof(*local));
	local->sin_family = AF_INET;
	local->sin_port = htons(port);
	local->sin_addr.s_addr = htonl(INADDR_ANY);
	bzero(&(local->sin_zero), 8);

	if(bind(res, (struct sockaddr *)local, sizeof(struct sockaddr))== -1)
	{
		perror("bind");
		return -1;
	}
	printf("bindage réussi\n");
	
	if(listen(res, 6) == -1)
	{
		perror("listen");
		return -1;
	}
	printf("listen réussi sur le port: %d\n",port);

	return res;
}

void handler(int sig)
{
	printf("terminating\n");
//	if(terminate_thread)
//		exit(0);
	//else
	terminate_thread = true;
	wbForceStop();
	wbStop();
	wbStop();
	printf("Arrêt du robot\n");
	
	printf("Arrêt des threads\n");
	close(socket_connec);
	usleep(50000);
	close(socket_decoute);
	printf("Fermeture des sockets\n");
	
	exit(0);
}

//reading socket is done here.
void * thread_read(void * args)
{
	char instr;
	char buffer[10];
	char v1[3],v2[3],sens[3];
	char anglex[4], angley[4];
	int n;
	struct sockaddr_in addr,addr2;
	int size = sizeof(struct sockaddr_in);
	
	//fcntl(STDIN, F_SETFL, O_NONBLOCK) ;// rend stdin non bloquant
	socket_decoute = socket_ecoute(DEFAULT_PORT,&addr);

	if((socket_connec=accept(socket_decoute, (struct sockaddr *)&addr2, (socklen_t*)&size)) != -1)
	{
		while((n=read(socket_connec,buffer,7))!=0 && !terminate_thread)
		{
			buffer[n]='\0';
			if(strlen(buffer)==7)
			{
				instr=buffer[0];
				v1[0]=buffer[1];v1[1]=buffer[2];v1[2]='\0';
				v2[0]=buffer[3];v2[1]=buffer[4];v2[2]='\0';
			
				sens[0]=buffer[5];sens[1]=buffer[6];sens[2]='\0';
				printf("action %c v1 %s v2 %s sens %s \n", instr ,v1,v2,sens);
				wbGetI2CInfo(WB_ADR_LEFT);	/* Recupere les infos du robot */
				wbGetI2CInfo(WB_ADR_RIGHT);
				switch(instr)//instr must be one of enum action in LibRemote.h
				{
					case '0':
						wbForceStop();
						break;
					case '1':
						wbForceAvancerCourbe(atoi(v1),atoi(v2));
						break;
					case '2':
						wbPanTiltMoveCamera(atoi(v1),atoi(v2),(WBSens)atoi(sens));
						break;
					case '3':
						wbForceReculerCourbe(atoi(v1),atoi(v2));
						break;
					case '4':
						wbTournerHoraire(atoi(v1));
						break;
					case '5':
						wbTournerAntiHoraire(atoi(v1));
						break;
					case '6':
						wbGetI2CBattery();
						wbGetI2CInfo(WB_ADR_LEFT);
						wbGetI2CInfo(WB_ADR_RIGHT);
						
						unsigned char buff[7];
						buff[0] = winfo.battery;
						buff[1] = winfo.frontLeft;
						buff[2] = winfo.rearLeft;
						buff[3] = winfo.frontRight;
						buff[4] = winfo.rearRight;
						buff[5] = winfo.irLeft;
						buff[6] = winfo.irRight;
						
						send(socket_connec, buff, 7,0);
						break;
					case '7':
						if(atoi(v1) == ctrl_off) //'control off' case
							wbSetMode(WB_MOTOR_CTRL_OFF);
						else wbSetMode(WB_MOTOR_CTRL_ON);
						break;
					case '8':
						printf("on bouge la camera\n");
						anglex[0] = buffer[1];
						anglex[1] = buffer[2];
						anglex[2] = buffer[3];
						anglex[3] = '\0';
						angley[0] = buffer[4];
						angley[1] = buffer[5];
						angley[2] = buffer[6];
						angley[3] = '\0';
						wbMoveCameraTo(atoi(anglex), atoi(angley));
						break;
					default:
						printf("no associated action with %c\n", instr);
						break;
				}
			}
			else//if(strlen(buffer)==7)
				printf("\'%s\', has not the correct length\n", buffer);
		}//while((n=read(socket_connec,buffer,10))!=0)
		printf("not reading anymore\n");
		close(socket_connec);
		usleep(50000);
		close(socket_decoute);
		terminate_thread = true;
	}//if((socket_connec=accept(socket_decoute, (struct sockaddr *)&addr2, (socklen_t*)&size)) != -1)
	else
	{
		printf("erreur d'acceptation\n");
	}
	pthread_exit(NULL);
}

//basic avoidance : if an obstacle is detected, robot is stopped.
void * thread_obstacle_stop(void *)
{
	//only checking for right IR. For wifibot 115 its the only working
	while(!terminate_thread)
	{
		wbGetI2CInfo(WB_ADR_RIGHT);
		if(winfo.irRight > 80)
			wbStop();
		usleep(500);
	}
	pthread_exit(NULL);	
}


int main(int argc,char *argv)
{
	struct sigaction nvt, old;
	
	nvt.sa_handler = handler;
	sigaction(SIGINT, &nvt, &old);
	wbInitI2C(WB_NUM_BUS);//initialise le robot;
//	pthread_create(&stop_thread, NULL, thread_obstacle_stop, NULL);
	
	thread_read(NULL);
/*	pthread_create(&read_thread, NULL, thread_read, NULL);
	pthread_create(&stop_thread, NULL, thread_obstacle_stop, NULL);
	
	pthread_join(read_thread, NULL);
	pthread_join(stop_thread, NULL);
*/
	return 0;	
}