#include "TraitementImageStructure.h"

/******************************************ROI FUNCTION********************************/
/**
 * Set the roi's bounds to R 255, G 0, B 255 in the image. Make sure that the roi's bounds
 * are INSIDE the image lengths. 'image' is to be an RGB image (means a pixel is coded with 3 unsigned
 * char, one for each color). 'width' and 'height' are the size of the image in pixel.
 */
void drawROIOnImage(unsigned char * image, roi * box, int width, int height)
{
	int line, row, current_pixel;
	
	if(box == NULL)
		return;
	
	line = box->begin_y;
	for(row = box->begin_x; row < box->end_x; row ++)
	{
		//drawing upper horizontal bound
		current_pixel =(line * width * 3) + row * 3;
		if(current_pixel > (width * 3) * height || current_pixel < 0)
			break;
		image[current_pixel] = 255;
		image[current_pixel + 1] = 0;
		image[current_pixel + 2] = 255;
	}
	
	line = box->end_y;
	for(row = box->begin_x; row < box->end_x; row ++)
	{
		//drawing lower horizontal bound
		current_pixel =(line * width * 3) + row * 3; 
		if(current_pixel > (width * 3) * height || current_pixel < 0)
			break;
		image[current_pixel] = 255;
		image[current_pixel + 1] = 0;
		image[current_pixel + 2] = 255;
	}
	
	row = box->begin_x;
	for(line = box->begin_y; line < box->end_y; line++)
	{
		current_pixel =(line * width * 3) + row * 3; 
		if(current_pixel > (width * 3) * height || current_pixel < 0)
			break;
		image[current_pixel] = 255;
		image[current_pixel + 1] = 0;
		image[current_pixel + 2] = 255;
	}

	row = box->end_x;
	for(line = box->begin_y; line < box->end_y; line++)
	{
		current_pixel =(line * width * 3) + row * 3; 
		if(current_pixel > (width * 3) * height || current_pixel < 0)
			break;
		image[current_pixel] = 255;
		image[current_pixel + 1] = 0;
		image[current_pixel + 2] = 255;
	}
}

roi * initRoi()
{
	roi * result = (roi *) malloc(sizeof(roi));
	if(result == NULL)
		return NULL;
	result->object_pixel_in_roi = 0;
	//else
	result->begin_x = -1;
	result->begin_y = -1;
	result->end_x = -1;
	result->end_y = -1;
	
	return result;
}

void reinitRoi(roi * box)
{
	box->object_pixel_in_roi = 0;
	box->begin_x = -1;
	box->begin_y = -1;
	box->end_x = -1;
	box->end_y = -1;
	
}

/**
 * Add pixel with given coordinate to 'box'.
 */
void addPixelToROI(int line, int row, roi * box)
{
	if(box == NULL)
		return;
	//else
	//begin coordinate
	if(box->begin_x < 0)
		box->begin_x = row;
	else if(box->begin_x > row)
		box->begin_x = row;
		
	if(box->begin_y < 0)
		box->begin_y = line;
	else if(box->begin_y > line)
		box->begin_y = line; 

	//end coordinate
	if(box->end_x < 0)
		box->end_x = row;
	else if(box->end_x < row)
		box->end_x = row;
	
	if(box->end_y < 0)
		box->end_y = line;
	else if(box->end_y < line)
		box->end_y = line;
		
	box->object_pixel_in_roi++;
}

roi * copyRoi(roi * box)
{
	if(box == NULL)
		return NULL;
	roi * copy = initRoi();
	copy->begin_x = box->begin_x;
	copy->end_x = box->end_x;
	copy->begin_y = box->begin_y;
	copy->end_y = box->end_y;
	copy->object_pixel_in_roi = box->object_pixel_in_roi;
	
	return copy;
}

/**************************************OBJECT & LIST_OBJECT FUNCTION**************************/
object * initObject(char * name)
{
	object * result = (object *)malloc(sizeof(object));

	if(result == NULL)
		return NULL;

	result->name = name;
	result->object_roi = NULL;

	result->allow_discontinuity = TRUE;
	result->max_inadequate_adjacent_pixel = 2;

	result->delta_color = 5;
	result->max_saturation = HSLMAX;
	result->min_saturation = 0;
	result->max_lightness = HSLMAX;
	result->min_lightness = 0;

	result->minimum_size = 200;

	return result;
}

list_object * initBlankListObject()
{
	list_object * result;
	result = (list_object *) malloc(sizeof(list_object));
	result->item = NULL;
	result->next = NULL;
	result->found = FALSE;
	return result;
}

object * findObject(char * name, list_object * list)
{
	list_object * crt = list;
	
	while(crt != NULL)
	{
		if(strcmp(crt->item->name, name) == 0)
			return crt->item;
		crt = crt->next; 
	}
	
	return NULL;
}

list_object * initListObject(object * item)
{
	list_object * result;
	result = (list_object *) malloc(sizeof(list_object));
	result->item = item;
	result->next = NULL;
	result->found = FALSE;
	return result;
}

void reinitList(list_object * list)
{
	list_object * crt = list;
	while(crt != NULL)
	{
		crt->found = FALSE;
		reinitObject(crt->item);
		crt = crt->next;
	}
}

void reinitObject(object * item)
{
	item->object_roi = NULL;
	item->x_position = -1;
	item->y_position = -1;
}
/**
 * Remove an object from a list of object. The corresponding list object will be returned. If the returned object
 * is of no more use, free it !
 * @param name name of the object to remove.
 * @param l_object list of object where given object is to be removed.
 * @return NULL if object as not been found, the removed object in its list_object otherwise.
 */
list_object * removeObjectWithNameFromList(char * name, list_object ** list)
{
	list_object * last_object = *list;
	list_object * crt_object = (*list)->next;
	
	if(strcmp((*list)->item->name, name) == 0)//if wanted object is first one
	{
		last_object->next = NULL;
		*list = crt_object;
		return last_object;
	}
				
	while(crt_object != NULL)
	{
		if(strcmp(crt_object->item->name, name) == 0)
		{
			last_object->next = crt_object->next;
			return crt_object;
		}
		//else
		last_object = crt_object;
		crt_object = crt_object->next;
	}
	#ifdef DEBUG
	printf("RemoveObjectWithNameFromList(), object %s not found\n", name);
	#endif
	return NULL;
}

boolean removeObjectFromList(list_object * object_to_remove, list_object ** list)
{
	list_object * last_object = *list;
	list_object * crt_object = (*list)->next;
	
	if(*list == object_to_remove)//if wanted object is first one
	{
		object_to_remove->next = NULL;
		*list = crt_object;
		return TRUE;
	}

	while(crt_object != NULL)
	{
		if(object_to_remove == crt_object)
		{
			last_object->next = crt_object->next;

			crt_object->next = NULL;
			return TRUE;
		}
		last_object = crt_object;
		crt_object = crt_object->next;
	}
	
	return FALSE;
}

void addObjectToList(list_object * object_to_add, list_object ** list)
{
	list_object * crt_object = *list;
	
	if(*list == NULL)
	{
		*list = object_to_add;
		return;
	}
	
	while(crt_object->next != NULL)
		crt_object = crt_object->next;
	
	crt_object->next = object_to_add;
}

void freeList(list_object * list)
{
	list_object * crt_p, * next_p;
	
	crt_p = list;
	next_p = crt_p->next;
	
	while(next_p != NULL)
	{
		freeObject(crt_p->item);
		free(crt_p);
		crt_p = next_p;
		next_p = crt_p->next;
	}
	freeObject(crt_p->item);
	free(crt_p);
}

void freeObject(object * item)
{
	if(item->object_roi != NULL)
		free(item->object_roi);
	free(item);
}

list_object * copyListObject(list_object * l_to_copy)
{
	list_object * copy = initBlankListObject(l_to_copy->item->name);
	copy->found = l_to_copy->found;
	copy->item = copyObject(l_to_copy->item);
	
	return copy;
}

object * copyObject(object * object_to_copy)
{
	object * copy = initObject(object_to_copy->name);
	
	copy->object_roi = copyRoi(object_to_copy->object_roi);
	
	copy->hue = object_to_copy->hue;
	copy->delta_color = object_to_copy->delta_color;
	copy->min_lightness = object_to_copy->min_lightness;
	copy->max_lightness = object_to_copy->max_lightness;
	copy->min_saturation = object_to_copy->min_saturation;
	copy->max_saturation = object_to_copy->max_saturation;
	copy->minimum_size = object_to_copy->minimum_size;
	copy->allow_discontinuity = object_to_copy->allow_discontinuity;
	copy->max_inadequate_adjacent_pixel = object_to_copy->max_inadequate_adjacent_pixel;
	copy->x_position = object_to_copy->x_position;
	copy->y_position = object_to_copy->y_position;
	
	return copy;
}

list_object * copyList(list_object * list_to_copy)
{
	list_object * crt_pt = list_to_copy, * copy, * crt_pt_copy;
	
	crt_pt_copy = copy = copyListObject(crt_pt);
	crt_pt = crt_pt->next;
	
	while(crt_pt != NULL)
	{
		crt_pt_copy->next = copyListObject(crt_pt);
		crt_pt_copy = crt_pt_copy->next;
		crt_pt = crt_pt->next;
	}
	
	return copy;
}

int count(list_object * list)
{
	list_object * crt_obj = list;
	int nb = 0;
	
	while(crt_obj != NULL)
	{
		nb ++;
		crt_obj = crt_obj->next;
	}
	return nb;
}

object * constructObject(char * name)
{
	object * result = initObject(name);
	
	printf("constructing %s\n", name);
	printf("please, give hue (0-%d)\n",HSLMAX);
	scanf("%d",&(result->hue));
	printf("\nplease, give delta color\n");
	scanf("%d",&(result->delta_color));
	printf("\nplease, give min_lightness (min = 0)\n");
	scanf("%d",&(result->min_lightness));
	printf("\nplease, give max_lightness (max = %d)\n", HSLMAX);
	scanf("%d",&(result->max_lightness));
	printf("please, give min_saturation (min = 0)\n");
	scanf("%d",&(result->min_saturation));
	printf("\nplease, give max_saturation (max = %d)\n", HSLMAX);
	scanf("%d",&(result->max_saturation));
	printf("\nplease, give min size in pixel (good one is 100)\n");
	scanf("%d",&(result->minimum_size));
	
	return result;
}

void appendList(list_object ** base_list, list_object * to_append)
{
	list_object * crt_p;
	
	if(*base_list == NULL)
	{
		*base_list = to_append;
		return;
	}
	//else	
	crt_p = *base_list;
	
	while(crt_p->next != NULL)
		crt_p = crt_p->next;
	
	crt_p->next = to_append;	
}

object * replaceObject(object * object_to_replace, list_object * list)
{
	list_object * crt_p = list;
	object * to_return;
	
	while(crt_p != NULL && strcmp(crt_p->item->name, object_to_replace->name) != 0)
		crt_p = crt_p->next;
	
	if(crt_p == NULL)
		return NULL;
		
	to_return = crt_p->item;
	crt_p->item = object_to_replace;
	
	return to_return;
}
