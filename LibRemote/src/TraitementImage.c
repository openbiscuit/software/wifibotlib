/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   traitement_image.c
 * @brief  Méthodes pour trouver la cible dans l'image.
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 */

#include "TraitementImage.h"


/**
 * return a pointer to RGB component of an existing jpeg file named 'file_name'. width and height are set accordingly with
 * the size of the jpeg image. The overall length of the returned pointer is 3 * width * height. each pixel as a R, G, B component,
 * accessed in this order.
 */
unsigned char * jpegFileDecompress(char * file_name, int * width, int * height)
{
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	unsigned char * current_position, * result_buffer;
	FILE * infile;
	
	infile = fopen(file_name, "rb");
	if(infile == NULL)
	{
		fprintf(stderr, "jpegFileDecompress : couldn't open %s file", file_name);
		return NULL;
	}
	
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);
	jpeg_stdio_src(&cinfo, infile);
	
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);
	
	*height = cinfo.output_height;
	*width = cinfo.output_width;
	
	//3 * for R, G & B component.
	result_buffer = (unsigned char *) malloc( 3 * (*width) * (*height) * sizeof(unsigned char));
	current_position = result_buffer;
	
	
	while(cinfo.output_scanline < (*height))
	{
		jpeg_read_scanlines(&cinfo, &current_position, 1);
		current_position = result_buffer +(3 * (*width)) * cinfo.output_scanline;
	}
	
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	return result_buffer;
}

unsigned char * ppmFileDecompress(char * file_name, int * width, int * height)
{
	char string[10];
	int trash, taille, index, val;
	unsigned char * image_rgb;
	FILE * p_file;
	
	p_file = fopen(file_name, "rb");
	fscanf(p_file, "%s %d %d %d", string, width, height, &trash);
	taille = (*width) * (*height) * 3;
	image_rgb = (unsigned char *)malloc(taille * sizeof(unsigned char));
	for(index = 0; index < taille; index+=3)
	{
		val = fgetc(p_file);
		image_rgb[index+2] = val;
		val = fgetc(p_file);
		image_rgb[index] = val;
		val = fgetc(p_file);
		image_rgb[index+1] = val;
	}
	return image_rgb;
}

/**
 * Simply return a pointer to a struct hsl_image ready to be used.
 * Return NULL if a something went wrong in a malloc.
 */
hsl_image * initHslImage(int width, int height)
{
	int line, row, line_free;
	hsl_image * result = (hsl_image *)malloc(sizeof(hsl_image));
	if(result == NULL)
		return NULL;
	result->pixels = (Tpx**) malloc(width * height * sizeof(Tpx *));
	if(result->pixels == NULL)
	{
		free(result);
		return NULL;
	}
	for(line = 0; line < height; line++)
	{
		result->pixels[line] = (Tpx *) malloc(width * sizeof(Tpx));
		if(result->pixels[line] == NULL)
		{
			for(line_free = 0; line_free < line; line_free++)
				free(result->pixels[line]);
			free(result->pixels);
			free(result);
			return NULL;
		}
		for(row = 0; row < width; row ++)
			result->pixels[line][row].traite = 0;
	}
			
	
	result->width = width;
	result->height = height;
	return result;
}

/**
 * Call this to deallocate a hsl_image created the initHslImage() way.
 */
void freeHslImage(hsl_image * image)
{
	int line;
	
	if(image == NULL)
		return;
	
	for(line = 0; line < image->height; line++)
		free(image->pixels[line]);
	free(image->pixels);
	//Il y a un problème étrange, dans la fonction pour trouver le 2eme objet d'un type donné, si le 2 eme objet n'est pas 
	//présent en meme temps que le 1er sur l'image, il arrive que le passage du rgb au hsl donne une image_hsl corrompu.
	//La méthode mde passage rgb au hsl ne semble pas en cause, toujours est il que le pointeur que l'on a en sortie de la fonction
	//n'est pas le meme que le pointeur retourné. Ainsi on peut désallouer les structure interne à l'image mais pas l'image elle meme.
	//Perso j'y comprends rien, et j'ai pas le temps d'approfondir !
	if(image->width > 0)
		free(image);
	image = NULL;
}

void reinitHslImage(hsl_image * image)
{
	int i, j;
	
	for(i = 0; i < image->width; i++)
		for(j = 0; j < image->height; j++)
			image->pixels[j][i].traite = 0;
}

/*
 * 
 * Write hue value of the given image in a file named after the file_name parameter.
 * return FALSE if problem occured, RIGHT otherwise
 */
boolean hsl2File(char * file_name, hsl_image * image)
{
	FILE * file = fopen(file_name, "w");
	int line, row;
	
	if(file == NULL)
		return FALSE;
	//else
	for(line = 0; line < image->height; line++)
	{
		for(row = 0 ; row < image->width; row++)
		{
			if(image->pixels[line][row].hue > 9)
				{
					if(image->pixels[line][row].hue > 99)
						fprintf(file, "%d ", image->pixels[line][row].hue);
					else
						fprintf(file, "%d  ", image->pixels[line][row].hue);
				}
			else
				fprintf(file,"%d   ", (int)image->pixels[line][row].hue);
		}
		fprintf(file, "\n");
	}
	return TRUE;
}
/*
 * Initialise source --- called by jpeg_read_header
 * before any data is actually read
 */
METHODDEF(void) init_source(j_decompress_ptr cinfo)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src; 
  /* We reset the empty-input-file flag for each image,
   * but we don't clear the input buffer.
   * This is correct behavior for reading a series of images from one source.
   */
  src->start_of_file = TRUE;
}




METHODDEF(boolean) fill_input_buffer(j_decompress_ptr cinfo)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src;
  src->pub.next_input_byte = src->buffer;
  src->pub.bytes_in_buffer = src->nb_bytes;
  src->start_of_file = FALSE;

  return TRUE;
}



METHODDEF(void) skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
  tab_src_ptr src = (tab_src_ptr) cinfo->src;
  if(num_bytes > 0)
    {
      src->pub.next_input_byte += (size_t) num_bytes;
      src->pub.bytes_in_buffer -= (size_t) num_bytes;
    }
}


METHODDEF(void) term_source(j_decompress_ptr cinfo)
{
  /* void */
}


/*
 * Source manager à partir d'un tableau déjà chargé en mémoire.
 * Il suffit d'indiquer l'adresse du tableau, ainsi que sa taille,
 * pour ensuite pouvoir décompresser l'image stockée dans ce tableau.
 * Une fois l'image décompressée on peut travailler dessus.
 */
GLOBAL(void) jpeg_tab_src(j_decompress_ptr cinfo, JOCTET *buffer, int nbytes)
{
  tab_src_ptr src;
  if(cinfo->src == NULL)
    {
      cinfo->src = (struct jpeg_source_mgr *)(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT, SIZEOF(tab_source_mgr));
      src = (tab_src_ptr) cinfo->src;
    }

  src = (tab_src_ptr) cinfo->src;
  src->pub.init_source = init_source;
  src->pub.fill_input_buffer = fill_input_buffer;
  src->pub.skip_input_data = skip_input_data;
  src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src->pub.term_source = term_source;
  src->nb_bytes = nbytes;
  src->buffer = buffer;
  src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
  src->pub.next_input_byte = NULL; /* until buffer loaded */

}


/**
 * write to a file named after parameter 'fichier' . whats under 'img' pointer will be writed, it is supposed
 * to hold an RGB image with color in that order. 'width' & 'height' are to be accurate, the actual size of 
 * the pointer 'img' should be 'width' * 'height' * 3.
 * File is in .ppm format, add the extension to the file name as it won't be added.
 */
int writePPMImage(char *fichier, unsigned char *img, int width, int height)
{
  FILE *fp;
  int i, taille = width * height * 3;

  fp = fopen(fichier, "w");
  if(fp == NULL)
    {
      fprintf(stderr, "Erreur : impossible d'ouvrir le fichier %s\n", fichier);
      return(1);
    }
  // on �crit les entetes ppm (24 bits RVB)
  fprintf(fp, "P6 %d %d 255 ", width, height);
  // on copie le tableau dans le fichier
  for(i=0; i<taille; i++)
    {
      fprintf(fp, "%c", img[i]);
    }
	
  return fclose(fp);
} /* ecrireImage() */





void pixeliserRGB(unsigned char *entre,int width,int height,unsigned char *res,int startwidth,int endwidth,int startheight,int endheight,int sizeofpixel,int center_inf,int center_sup,int sizeofpixelcenter)
{
  int i,j,x,z;
  int i2=0,j2=0;
  int moyr,moyg,moyb;
  printf("width : %d height: %d\n",endwidth,endheight);
  for(i=startwidth;i<center_inf;i+=sizeofpixel)
    {
      j2=0;
      //printf("i : %d\n",i);
      for(j=startheight;j<endheight;j+=sizeofpixel)
	{
	  //printf("j : %d\n",j);
	  moyr = 0;
	  moyg = 0;
	  moyb = 0;
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
			{
		  moyr += entre[z*width*3+x*3];	
		  moyg += entre[z*width*3+x*3+1];	
		  moyb += entre[z*width*3+x*3+2];	
						
		}

	    }	
	  //	printf("%d %d %d %d\n",i,j,x,z);
	  moyr = moyr/(sizeofpixel * sizeofpixel);
	  moyg = moyg/(sizeofpixel * sizeofpixel);
	  moyb = moyb/(sizeofpixel * sizeofpixel);
	  /*res[j2][i2][0] = (unsigned char)moyr;
	    res[j2][i2][1] = (unsigned char)moyg;
	    res[j2][i2][2] = (unsigned char)moyb;
	  */
				
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  res[z*width*3+x*3] = moyr;
		  res[z*width*3+x*3+1] = moyg;
		  res[z*width*3+x*3+2] = moyb;
		}
	    }
	  j2++;
	}
      i2++;
    }
			
  for(i=center_inf;i<center_sup;i+=sizeofpixelcenter)
    {
      j2=0;
      //printf("i : %d\n",i);
      for(j=startheight;j<endheight;j+=sizeofpixelcenter)
	{
	  //printf("j : %d\n",j);
	  moyr = 0;
	  moyg = 0;
	  moyb = 0;
	  for(x=i;x<i+sizeofpixelcenter;x++)
	    {
	      for(z=j;z<j+sizeofpixelcenter;z++)
		{
						
		  moyr += entre[z*width*3+x*3];	
		  moyg += entre[z*width*3+x*3+1];	
		  moyb += entre[z*width*3+x*3+2];	
						
		}

	    }	
	  //	printf("%d %d %d %d\n",i,j,x,z);
	  moyr = moyr/(sizeofpixelcenter * sizeofpixelcenter);
	  moyg = moyg/(sizeofpixelcenter * sizeofpixelcenter);
	  moyb = moyb/(sizeofpixelcenter * sizeofpixelcenter);
	  /*res[j2][i2][0] = (unsigned char)moyr;
	    res[j2][i2][1] = (unsigned char)moyg;
	    res[j2][i2][2] = (unsigned char)moyb;
	  */
				
	  for(x=i;x<i+sizeofpixelcenter;x++)
	    {
	      for(z=j;z<j+sizeofpixelcenter;z++)
		{
		  res[z*width*3+x*3] = moyr;
		  res[z*width*3+x*3+1] = moyg;
		  res[z*width*3+x*3+2] = moyb;
		}
	    }
	  j2++;
	}
      i2++;
    }
  for(i=center_sup;i<endwidth;i+=sizeofpixel)
    {
      j2=0;
      //printf("i : %d\n",i);
      for(j=startheight;j<endheight;j+=sizeofpixel)
	{
	  //printf("j : %d\n",j);
	  moyr = 0;
	  moyg = 0;
	  moyb = 0;
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
						
		  moyr += entre[z*width*3+x*3];	
		  moyg += entre[z*width*3+x*3+1];	
		  moyb += entre[z*width*3+x*3+2];	
						
		}

	    }	
	  //	printf("%d %d %d %d\n",i,j,x,z);
	  moyr = moyr/(sizeofpixel * sizeofpixel);
	  moyg = moyg/(sizeofpixel * sizeofpixel);
	  moyb = moyb/(sizeofpixel * sizeofpixel);
	  /*res[j2][i2][0] = (unsigned char)moyr;
	    res[j2][i2][1] = (unsigned char)moyg;
	    res[j2][i2][2] = (unsigned char)moyb;
	  */
				
	  for(x=i;x<i+sizeofpixel;x++)
	    {
	      for(z=j;z<j+sizeofpixel;z++)
		{
		  res[z*width*3+x*3] = moyr;
		  res[z*width*3+x*3+1] = moyg;
		  res[z*width*3+x*3+2] = moyb;
		}
	    }
	  j2++;
	}
      i2++;
    }
}

/**
 * Convert a struct hsl_image to an array of unsigned char. The array is of length
 * image->width * image->length * 3. The array is the image in RGB format, each color
 * component is accessed in this order.
 */
unsigned char * hsl2rgb(hsl_image * image)
{
	int row, line, red, green, blue;
	unsigned char * c = (unsigned char *) malloc(image->width * image->height * 3 * sizeof(unsigned char));
	
	if(c == NULL)
	return NULL;
  
	for(line = 0; line < image->height; line++)
	  	for(row = 0; row < image->width; row++)
	  	{
	  		pixelHSLtoRGB(image->pixels[line][row], &red, &green, &blue);
	  		c[(line * (image->width * 3)) + (row * 3)] = red;
	  		c[(line * (image->width * 3)) + (row * 3) + 1] = green;
	  		c[(line * (image->width * 3)) + (row * 3) + 2] = blue;
	  	}
	return c;
}

hsl_image * rgb2hsl(unsigned char *image, int width, int height){

	//pour le cacul de la teinte
	int red, green, blue;
	int line, row, index; //parcours du tableau hsl
	hsl_image * result_image = initHslImage(width, height);
	if(result_image == NULL)
		return NULL;

	//on traite l'image pixel par pixel
	for(line = 0; line < height; line++)
	{
		//if(i%10 == 0) printf("ligne %d\n", i);
		for(row = 0; row < width; row++)
		{
			//recuperation des composantes rvb
			index = line * (width * 3) + (row * 3); 
			red=image[index];
			green=image[index+1];
			blue=image[index+2];
			pixelRGBtoHSL(red, green, blue, &(result_image->pixels[line][row]));
			// on avance dans la ligne
		} // for row
		// on change de ligne
	}// for line
	return result_image;
}// rvb2hsl()
		
void rvb2hsl(unsigned char *image, Tpx teinte[480][640], int width, int height){

  //pour le cacul de la teinte
  int T0, delta; // delta=max(r,v,b)-min(r,v,b)
  unsigned char cst=TMAX; // pour la formule du calcul de la teinte

  unsigned char r, v, b; // couleurs
  int i=0, j; //parcours du tableau hsl
  int temp=0; //parcours du tableau image

  //on traite l'image pixel par pixel
  while(i<height){
    j=0;
    //if(i%10 == 0) printf("ligne %d\n", i);
    while(j<width){
      //recuperation des composantes rvb
      r=image[temp++];
      v=image[temp++];
      b=image[temp++];
      temp += 3*(INC-1);

      if( r == v && v == b ) {
	// cas special
	T0 = 0;
      }
      else if(r >= v && r >= b) {
	delta = (v > b)? r-b : r-v;
	T0 = cst*(v-b)/ (6*delta);
      } else if(v >= b && v >= r) {
	delta = (b > r)? v-r : v-b;
	T0 = cst*((b-r)+2*delta)/ (6*delta);
      } else {
	delta = (r > v)? b-v : b-r;
	T0 = cst*((r-v)+4*delta)/ (6*delta);
      }
			   
      //T = ((T0)*cst); // cst = TMAX/6 avec TMAX = 255
      //T = (unsigned char)(T%TMAX);
			
      //remplir le tableau hsl avec seulement la composante h
      if(r> seuil_blanc && v > seuil_blanc && b > seuil_blanc)
	{
	  teinte[i][j].iswhite =1;
	}
      else
	{
	  teinte[i][j].iswhite = 0;
	}
      if(r< 60 && v < 60 && b < 60)
	{
	  teinte[i][j].isblack =1;
	}
      else
	{
	  teinte[i][j].isblack = 0;
	}
      teinte[i][j].hue=T0;
      teinte[i][j].traite=0;
      // on avance dans la ligne
      j++;  
    } // while(width)
    // on change de ligne
    i += INC;
  }// while(height)
}// rvb2hsl()
		
/**
 * Crée des pixels de taille 'sizeofpixel' dont la teinte est la moyenne
 * des teintes des pixels remplacés.
 */		
hsl_image * pixeliserHSL(hsl_image * image_to_process, int sizeofpixel)
{
	int i, j, x, z, pixel_traiter, width = image_to_process->width, height = image_to_process->height, values[sizeofpixel * sizeofpixel];
	hsl_image * result = initHslImage(width, height);
	int hue_moy, light_moy, sat_moy;
	  
	for(i = 0; i < width; i += sizeofpixel)
	{
		for(j = 0; j < height; j += sizeofpixel)
		{
			light_moy = sat_moy = hue_moy = 0;
			pixel_traiter = 0;
			
			for(x = i; x < i + sizeofpixel && x < width; x++)
			{
				for(z = j; z < j + sizeofpixel && z < height; z++)
				{
					values[pixel_traiter] = image_to_process->pixels[z][x].hue;
					pixel_traiter++;
					sat_moy += image_to_process->pixels[z][x].saturation;
					light_moy += image_to_process->pixels[z][x].lightness;
				}
			}
			hue_moy  = meanOnRoundRange(values, pixel_traiter, HSLMAX);
			light_moy /= pixel_traiter;
			sat_moy /= pixel_traiter;
			for(x = i; x < i + sizeofpixel && x < width; x++)
			{
				for(z = j; z < j + sizeofpixel && z < height; z++)
				{
					result->pixels[z][x].hue = hue_moy;
					result->pixels[z][x].lightness = light_moy;
					result->pixels[z][x].saturation = sat_moy;
				}
			}
		}
	}
	return result;
}

/**
 * La teinte sera codée par intervales de longueur 'div'
 * (0, div, 2*div, 3*div, ...).
 */
hsl_image * discretiserHSL(hsl_image * image_to_process,int div)
{
	int i,j;
	int moy = 0;
	hsl_image * result = initHslImage(image_to_process->width, image_to_process->height); 
	
	for(i = 0; i < image_to_process->width; i++)
	{
		for(j = 0; j < image_to_process->height; j++)
		{
			moy = 0;
			result->pixels[j][i].hue = image_to_process->pixels[j][i].hue / div;
			result->pixels[j][i].hue *= div;
			result->pixels[j][i].lightness = image_to_process->pixels[j][i].lightness / div;
			result->pixels[j][i].lightness *= div;
			result->pixels[j][i].saturation = image_to_process->pixels[j][i].saturation / div;
			result->pixels[j][i].saturation *= div;
		}
	}
	return result;
}

void discretiserRGB(unsigned char *entre,unsigned char *sortie,int width,int height,int div)
{
  int i,j;
	
	
  for(i=0;i<width;i++)
    {
      for(j=0;j<height;j++)
		{
		  sortie[j*width*3 + i*3] = entre[j*width*3 + i*3]/div;	
		  sortie[j*width*3 + i*3 +1] = entre[j*width*3 + i*3+1]/div;	
		  sortie[j*width*3 + i*3 +2] = entre[j*width*3 + i*3+2]/div;	
		  sortie[j*width*3 + i*3]*=div;
		  sortie[j*width*3 + i*3+1]*=div;
		  sortie[j*width*3 + i*3+2]*=div;
		}	
    }
}

int max(int a, int b)
{
	if(a > b)
		return a;
	else
		return b;
}

int min(int a, int b)
{
	if(a > b)
		return b;
	else
		return a; 
}

void pixelRGBtoHSL(int R, int G, int B, Tpx * hsl_pixel)
{
	int cMax,cMin;      /* max and min RGB values */ 
	int  Rdelta,Gdelta,Bdelta; /* intermediate value: % of spread from max */
	
	/* calculate lightness */ 
	cMax = max( max(R,G), B);
	cMin = min( min(R,G), B);

	(*hsl_pixel).lightness = ((cMax + cMin) * HSLMAX) / (2 * RGBMAX);
	
	if (cMax < cMin + DELTA_COLOR && cMax > cMin - DELTA_COLOR) {           /* r=g=b --> achromatic case */ 
		(*hsl_pixel).saturation = 0;                     /* saturation */ 
		(*hsl_pixel).hue = UNDEFINED;             /* hue */ 
	}
	else {                        /* chromatic case */ 
		/* saturation */ 
		if ((*hsl_pixel).lightness <= (HSLMAX/2))
			(*hsl_pixel).saturation = ((cMax - cMin) * HSLMAX) / (cMax + cMin);
		else
			(*hsl_pixel).saturation = ((cMax - cMin) * HSLMAX)/(2 * RGBMAX - cMax - cMin);
		
		/* hue */ 
		Rdelta = (( ((cMax - R) / 6) + ((cMax - cMin) / 2) ) * HSLMAX) / (cMax-cMin);
		Gdelta = (( ((cMax - G) / 6) + ((cMax - cMin) / 2) ) * HSLMAX) / (cMax-cMin);
		Bdelta = (( ((cMax - B) / 6) + ((cMax - cMin) / 2) ) * HSLMAX) / (cMax-cMin);
		
		if (R == cMax)
			(*hsl_pixel).hue = Bdelta - Gdelta;
		else if (G == cMax)
			(*hsl_pixel).hue = (HSLMAX/3) + Rdelta - Bdelta;
		else /* B == cMax */ 
			(*hsl_pixel).hue = ((2*HSLMAX)/3) + Gdelta - Rdelta;
		
		if ((*hsl_pixel).hue < 0)
			(*hsl_pixel).hue += HSLMAX;
		if ((*hsl_pixel).hue > HSLMAX)
			(*hsl_pixel).hue -= HSLMAX;
			//white and black pixel case
	    if(R> seuil_blanc && G > seuil_blanc && B > seuil_blanc)
			(*hsl_pixel).iswhite = 1;
		else
			(*hsl_pixel).iswhite = 0;

		if(R< 32 && G < 32 && B < 32)
	     	(*hsl_pixel).isblack = 1;
		else
			(*hsl_pixel).isblack = 0;
	}	
}

/**
 * Convert a single RGB pixel to an hsl one. Old computation, don't think its right !
 */
void PixelRGBtoHSL2(int R, int G, int B, Tpx * hsl_pixel)
{
	int cMax,cMin;      /* max and min RGB values */ 
	int  Rdelta,Gdelta,Bdelta; /* intermediate value: % of spread from max */
	
	/* calculate lightness */ 
	cMax = max( max(R,G), B);
	cMin = min( min(R,G), B);
	(*hsl_pixel).lightness = ( ((cMax+cMin)*HSLMAX) + RGBMAX )/(2*RGBMAX);
	
	if (cMax < cMin + DELTA_COLOR && cMax > cMin - DELTA_COLOR) {           /* r=g=b --> achromatic case */ 
		(*hsl_pixel).saturation = 0;                     /* saturation */ 
		(*hsl_pixel).hue = UNDEFINED;             /* hue */ 
	}
	else {                        /* chromatic case */ 
		/* saturation */ 
		if ((*hsl_pixel).lightness <= (HSLMAX/2))
			(*hsl_pixel).saturation = ( ((cMax-cMin)*HSLMAX) + ((cMax+cMin)/2) ) / (cMax+cMin);
		else
			(*hsl_pixel).saturation = ( ((cMax-cMin)*HSLMAX) + ((2*RGBMAX-cMax-cMin)/2) )/ (2*RGBMAX-cMax-cMin);
		
		/* hue */ 
		Rdelta = ( ((cMax-R)*(HSLMAX/6)) + ((cMax-cMin)/2) ) / (cMax-cMin);
		Gdelta = ( ((cMax-G)*(HSLMAX/6)) + ((cMax-cMin)/2) ) / (cMax-cMin);
		Bdelta = ( ((cMax-B)*(HSLMAX/6)) + ((cMax-cMin)/2) ) / (cMax-cMin);
		
		if (R == cMax)
			(*hsl_pixel).hue = Bdelta - Gdelta;
		else if (G == cMax)
			(*hsl_pixel).hue = (HSLMAX/3) + Rdelta - Bdelta;
		else /* B == cMax */ 
			(*hsl_pixel).hue = ((2*HSLMAX)/3) + Gdelta - Rdelta;
		
		if ((*hsl_pixel).hue < 0)
			(*hsl_pixel).hue += HSLMAX;
		if ((*hsl_pixel).hue > HSLMAX)
			(*hsl_pixel).hue -= HSLMAX;
			//white and black pixel case
	    if(R> seuil_blanc && G > seuil_blanc && B > seuil_blanc)
			(*hsl_pixel).iswhite = 1;
		else
			(*hsl_pixel).iswhite = 0;

		if(R< 32 && G < 32 && B < 32)
	     	(*hsl_pixel).isblack = 1;
		else
			(*hsl_pixel).isblack = 0;
	}
}
/* utility routine for HLStoRGB */ 
int hueToRGB(int magic1, int magic2, int hue)
{
	/* range check: note values passed add/subtract thirds of range */ 
	if (hue < 0)
		hue += HSLMAX;
	 
	if (hue > HSLMAX)
		hue -= HSLMAX;
	
	/* return r,g, or b value from this tridrant */ 
	if (hue < (HSLMAX/6))
		return magic1 + (((magic2 - magic1) * hue + (HSLMAX / 12)) / (HSLMAX / 6));
	if (hue < (HSLMAX / 2))
		return magic2;
	if (hue < ((HSLMAX * 2) / 3))
		return magic1 + (((magic2 - magic1) * (((HSLMAX * 2) / 3) - hue) + (HSLMAX / 12)) / (HSLMAX / 6));
	else
		return magic1;
}

/**
 * Convert a single hsl pixel given as parameter to RGB value
 */
void pixelHSLtoRGB(Tpx hsl_pixel, int * R, int * G, int * B)
{
	int Magic1,Magic2;       /* calculated magic numbers (really!) */ 
	
	if(hsl_pixel.saturation == 0) {            /* achromatic case */ 
		*R = *G = *B = (hsl_pixel.lightness * RGBMAX) / HSLMAX;
		if (hsl_pixel.hue != UNDEFINED) {
			/* ERROR */ 
		}
	}
	else  {                    /* chromatic case */ 
		/* set up magic numbers */ 
		if (hsl_pixel.lightness <= (HSLMAX/2))
			Magic2 = (hsl_pixel.lightness * (HSLMAX + hsl_pixel.saturation) + (HSLMAX / 2)) / HSLMAX;
		else
			Magic2 = hsl_pixel.lightness + hsl_pixel.saturation - ((hsl_pixel.lightness * hsl_pixel.saturation) + (HSLMAX / 2)) / HSLMAX;
		 
		Magic1 = 2 * hsl_pixel.lightness - Magic2;
		
		/* get RGB, change units from HLSMAX to RGBMAX */ 
		*R = (hueToRGB(Magic1, Magic2, hsl_pixel.hue + (HSLMAX / 3)) * RGBMAX + (HSLMAX / 2)) / HSLMAX;
		*G = (hueToRGB(Magic1, Magic2, hsl_pixel.hue) * RGBMAX + (HSLMAX / 2)) / HSLMAX;
		*B = (hueToRGB(Magic1, Magic2, hsl_pixel.hue - (HSLMAX / 3)) * RGBMAX + (HSLMAX / 2)) / HSLMAX;
	}
}

/**
 * Test if given 'pixel' as 'color' more or less DELTA_COLOR.
 */
boolean hslPixelAsColor(object * searched_object, Tpx pixel)
{
	int max_bound, min_bound;
	
	if(pixel.isblack || pixel.iswhite || pixel.hue == UNDEFINED)
		return FALSE;
	
	max_bound = (searched_object->hue + searched_object->delta_color)%HSLMAX;
	min_bound = searched_object->hue - searched_object->delta_color;
	
	if(min_bound < 0)
		min_bound += HSLMAX;
		
	if(max_bound > HSLMAX)
		max_bound -= HSLMAX;
	
	if(max_bound < min_bound)//case for example [350 - 10]
	{
		if((pixel.hue < max_bound || pixel.hue > min_bound)
			&& pixel.saturation >= searched_object->min_saturation && pixel.saturation <= searched_object->max_saturation
			&& pixel.lightness >= searched_object->min_lightness && pixel.lightness <= searched_object->max_lightness)
			return TRUE;
	}
	//normal case
	else if(pixel.hue < max_bound && pixel.hue > min_bound
			&& pixel.saturation >= searched_object->min_saturation && pixel.saturation <= searched_object->max_saturation
			&& pixel.lightness >= searched_object->min_lightness && pixel.lightness <= searched_object->max_lightness)
		return TRUE;
	//else
	return FALSE;
}

boolean giveObjectRoi(object * searched_object, hsl_image * image)
{
	int row, line;
	roi * result;
	
	if(searched_object == NULL)
	{
		#ifdef DEBUG
		printf("giveObjectroi => l'objet donné est un pointeur NULL\n");
		#endif
		return FALSE;
	}
	
	if(searched_object->object_roi != NULL)
	{
		free(searched_object->object_roi);
		searched_object->object_roi = NULL;
	}
	
	result = initRoi();
	
	if(result == NULL)
	{
		printf("error while initializing roi in giveObjectRoi\n");
		return FALSE;
	}
	
	for(row = 0; row < image->width; row++)
	{
		for(line = 0; line < image->height; line++)
		{
			if(image->pixels[line][row].traite == 1)
				continue;
			image->pixels[line][row].traite = 1;
			if(hslPixelAsColor(searched_object, image->pixels[line][row]))
			{
				addPixelToROI(line, row, result);
				searchNeighborPixel(image, row, line, searched_object, result, 0);
				if(result->object_pixel_in_roi >= searched_object->minimum_size)
				{
					searched_object->object_roi = result;
					return TRUE;
				}
				else
					reinitRoi(result);
			}
		}
	}
	free(result);
	
	return FALSE;
}

boolean giveObjectsRoi(list_object * list, hsl_image * image)
{
	boolean found = FALSE;
	list_object * crt_object = list;
	
	while(crt_object != NULL)
	{
		if(crt_object->found == FALSE)
		{
			if(giveObjectRoi(crt_object->item, image))
			{
				crt_object->found = TRUE;
				found = TRUE;
			}
			reinitHslImage(image);
		}
		crt_object = crt_object->next;
	}
	return found;
}

/**
 * search if neighbor pixel of pixel at 'current_x' and 'current_y' in 'image' as good 'color'.
 * If yes, recursively search for neighbor's neighbors. 
 */
void searchNeighborPixel(hsl_image * image, int current_x, int current_y, object * searched_object, roi * box, int crt_inadequate_pixel_nb)
{
	int dx[] = {-1, 0, 1, 0};
	int dy[] = {0, -1, 0, 1};
	int delta, new_y, new_x;
	
	for(delta = 0; delta < 4; delta++)
	{
		new_y = current_y + dy[delta];
		new_x = current_x + dx[delta];
		if(new_x < 0 || new_y < 0 || new_x >= image->width || new_y >= image->height)
			continue;
		//else
		if(image->pixels[new_y][new_x].traite == 1)
			continue;
		//else
		image->pixels[new_y][new_x].traite = 1;
		if(hslPixelAsColor(searched_object, image->pixels[new_y][new_x]))
		{
			addPixelToROI(new_y, new_x, box);
			searchNeighborPixel(image, new_x, new_y, searched_object, box, 0);
		}
		//now for pixel with wrong color, but if we allow it for a certain pixel distance
		else if(searched_object->allow_discontinuity)
		{
		 	if(crt_inadequate_pixel_nb < searched_object->max_inadequate_adjacent_pixel)//if distance in pixel is still under treshold
				searchNeighborPixel(image, new_x, new_y, searched_object, box, crt_inadequate_pixel_nb + 1);
		}
	}
}

/**
 * give the average of the 'size' first number in 'numbers'. The average is computed on a "round range", max range being HSLMAX.
 * This mean that number near HSLMAX and just after 0 are very close.
 * For example average of 355 and 3 should give 359 and not 179.
 */
int meanOnRoundRange(int numbers[], int size, int range)
{
	int max = -1, index, average = 0;
	
	for(index = 0; index < size; index++)
		if(numbers[index] > max)
			max = numbers[index];
	
	for(index = 0; index < size; index++)
	{
		if(numbers[index] < max - range/2)
			numbers[index] += range;
		average += numbers[index];
	}
	average /= size;
	return average;
}

/**
 * decompress a jpeg file contained in given 'buffer', the buffer making 'taille' length.
 * This will return a pointer to an unsigned char pointer, pointing on the beggining of a rgb image. The rgb image
 * is width * height * 3 long.
 */
unsigned char * jpegBufferDecompress(unsigned char *buffer, int taille,
		     int *width, int *height)
{
	unsigned char * current_position, * result_buffer;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	
	/* Initialisation de la structure d'info */
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	/* On lui dit "d�compresse ce qu'il y a dans le buffer l�" */
	jpeg_tab_src(&cinfo, buffer, taille);

	/* On r�cup�re les infos de l'image et on d�compresse */
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	*height = cinfo.output_height;
	*width = cinfo.output_width;

	/* Buffer o� l'image d�compress�e sera plac�e */
	//printf("Allocation m�moire image (%d)\n", width*height*3*sizeof(char));
	//*image = malloc(height*width*3*sizeof(unsigned char));
	result_buffer = (unsigned char *)malloc((*height) * (*width) * 3 * sizeof(unsigned char));
	current_position = result_buffer;
	/* Tant qu'il y a des scanlines, on les place dans le buffer */
	while(cinfo.output_scanline < (*height))
	{
		jpeg_read_scanlines(&cinfo, &current_position, 1);
		current_position = result_buffer + 3 * (*width) * cinfo.output_scanline;
	}

	/* On termine proprement */
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	return result_buffer;
} /* decompresseImage() */
