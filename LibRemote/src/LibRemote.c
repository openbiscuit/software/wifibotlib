/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   LibRemote.c
 * @brief  Méthodes pour dirigier le wifibot à distance
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * Suppose qu'un serveur de type 'remote.wb' tourne sur le robot.
 * L'utilisation normale suppose qu'un RemoteConnec soit fait avant toute autre appel de fonction de la librairie.
 * Par la suite, avant de finir le programme, un RemoteStop permet d'immobiliser le robot et RemoteDisconnect termine
 * proprement en déconnectant et laissant le socket libre.
 */

#include "LibRemote.h"


/**
 * Just check if first angle is between two others.
 */
boolean angleIsBetween(int angle_to_test, int low_angle, int high_angle)
{
	if(high_angle > low_angle)//normal case, example low_angle is 10 and high one 20
	{
		if(low_angle < angle_to_test && angle_to_test <= high_angle)
			return TRUE;
	}
	else//case where low angle is for example 350 and high angle 10
		if(low_angle < angle_to_test || angle_to_test < high_angle)
			return TRUE;
	return FALSE;
}

/**
 * Display an image (in a SDL frame) and ask the user if the object is good => The object as to be drawn first on the image!
 * Draw the roi on an image with drawRoi() function in TraitementImage.c.
 * The focus as to be in the SDL frame, then if this is the good object, the user as to push enter or escape.
 * The function will block until either enter or escape is pushed.
 * @return TRUE if enter was pressed, FALSE if it was escape.
 */
boolean askIfGoodObject(unsigned char * image_rgb, int width, int height)
{
	if(initSDL() != 0)
		printf("display problem\n");
	
	printf("is it the good object ? yes->enter, no->escape\n");
	
	while(1)
	{
		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			displayRGBBuffer(image_rgb, width, height);
			if(event.type == SDL_KEYDOWN)
			{
				switch(event.key.keysym.sym)
				{
					case(SDLK_RETURN):
						return TRUE;
					case (SDLK_ESCAPE):
						return FALSE;
					default:
						break;
				}
			}
		}
	}
}

/**
 * general method to send info to the robot. It suppose that a remote server is runing on the robot (see remote.c), it will then
 * know how to process the message.
 * @param v1 should be a positiv integer less than 100
 * @param v2 should be a positiv integer less than 100
 * @param sens should be a positiv integer less than 100 
 */

int send_info(robot * rob, action ac,int v1,int v2,int sens)
{
	if(rob->camera_only)
	{
		printf("no robot, only camera\n");
		return -1;
	}
	if(rob->is_connected == FALSE)
	{
		printf("erreur, robot non connecté\n");
		return 1;
	}
	
  	char buffer[10];
	
  	sprintf(buffer,"%d\0",ac);
  	
    if(ac == a_camerato)
    {
    	while(v1>999)
    		v1/=10;
    	while(v2>999)
    		v2/=10;
    		
    	if(v1<10)
    		sprintf(buffer,"%s00%d\0", buffer, v1);
    	else if(v1<100)
    		sprintf(buffer,"%s0%d\0", buffer, v1);
    	else
    		sprintf(buffer, "%s%d\0", buffer, v1);
    	if(v2<10)
    		sprintf(buffer, "%s00%d\0", buffer, v2);
    	else if(v2<100)
    		sprintf(buffer, "%s0%d\0", buffer, v2);
    	else
    		sprintf(buffer, "%s%d\0", buffer, v2);
    }
	else
	{
	  	if(v1<10)
	    {
	      sprintf(buffer,"%s0%d\0",buffer,v1);
			
	    }
	  	else
	    {
	      sprintf(buffer,"%s%d\0",buffer,v1);
	    }
	  	if(v2<10)
		{
	      sprintf(buffer,"%s0%d\0",buffer,v2);
			
	    }
	  	else
	    {
	      sprintf(buffer,"%s%d\0",buffer,v2);
	    }
	  	if(sens<10)
	    {
	      sprintf(buffer,"%s0%d\0",buffer,sens);			
	    }
	  	else
	    {
	      sprintf(buffer,"%s%d\0",buffer,sens);
	    }
	}

  	printf("packet envoyé : %s, longueur %d\n",buffer, strlen(buffer));
  	send(rob->socket, buffer, strlen(buffer), 0);
  	return 0;
}

/**
 * Ask for information of a wifibot. Fill a struct WBinfo accordingly (see libwb/include/wbi2c.h).
 * @param rob robot we want the information from.
 * @param info structure where all the information will be held.
 */
int RemoteGetInfo(robot * rob, WBInfo * info)
{
	unsigned char buff[7];
	int nb_ret;
	
	if(rob->camera_only)
	{
		printf("no robot, only camera\n");
		return -1;
	}
	
	if(rob->is_connected == FALSE)
	{
		printf("erreur, robot non connecté\n");
		return 1;
	}
	
	send_info(rob, a_getinfo, 1, 1, 1);
	nb_ret = recv(rob->socket, buff, 7, 0);
	if(nb_ret != 7)
		printf("RemoteGetInfo(), reçu moins que prévu\n");
	
	info->battery = buff[0];
	info->frontLeft = buff[1];
	info->rearLeft = buff[2];
	info->frontRight = buff[3];
	info->rearRight = buff[4];
	info->irLeft = buff[5];
	info->irRight = buff[6];

	return 0;
}

int RemoteAvancerCourbe(robot * bot, int v1, int v2)
{
	return send_info(bot, a_avance, v1, v2, 0);
}
int RemoteReculerCourbe(robot * bot, int v1, int v2)
{
	return send_info(bot, a_recule, v1, v2, 0);
}
int RemoteTournerHoraire(robot * bot, int v1, int v2)
{
	return send_info(bot, a_senshoraire, v1, v2, 0);
}

int RemoteTournerAntiHoraire(robot * bot, int v1, int v2)
{
	return send_info(bot, a_sensantihoraire, v1, v2, 0);
}

/**
 * move the camera of given robot to specified angles. base position is vertical 158 and horizontal 120.
 * Some time the camera won't move, nonetheless its coordinate in struct robot are changed as if it moved.<br>
 * WARNING Don't use RemoteTournerWebcam with RemoteMoveCameraOf and RemoteMoveCameraTo<br>
 * It seems that for vertical angle, valid value are between 0 and 135. For horizontal value, value have to be
 * between 0 and 340. Ok, now with testing, the real horizontal angle is between 0 and 315. 
 * NOTE : les déplacements de 1 degré semblent problèmatique, faire au moins 2 voir 3 degrés, 4 pas de problème
 * Malgré tout
 * @param angle_vert vertical wanted angle
 * @param angle_horiz horizontal wanted angle
 * @param bot robot we want the camera to move
 */
int RemoteMoveCameraTo(robot * bot, int angle_horiz, int angle_vert)
{
	int res;
	int nb, sk, taille;
	char buf[100];
	char buffer[BUFFERSIZE];
	struct sockaddr_in server_adress;

	if(angle_horiz < 0)
		angle_horiz = 0;
	if(angle_vert < 0)
		angle_vert = 0;
	if(angle_horiz > 315)
		angle_horiz = 315;
	if(angle_vert > 135)
		angle_horiz = 135;
	
	if(bot->camera_only)
	{
		taille = sprintf(buf, "PanTiltHorizontal=%d&PanTiltVertical=%d&PanTiltPositionMove=true", angle_horiz, (int)((angle_vert*-0.75)+135));
		nb = sprintf(buffer, "POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n", taille, buf);
		sk = RemoteSocket_connect(80, bot->ip, &server_adress);

		res = send(sk, buffer, nb, 0);
		
		if(res != -1)
			res = 0;
		/* ON ATTEND QUE LE MESSAGE SOIT ENVOYE
		 * ET QUE LA CAM AIT REAGI */
		usleep(500000);
		close(sk);		
	}
	else
		res = send_info(bot, a_camerato, angle_horiz, angle_vert, 0);

	if(res == 0)
	{
		bot->cam_x = angle_horiz;
		bot->cam_y = angle_vert;
	}
	//waiting for the camera to finish moving
	sleep(2);
		
	return res;
}

/**
 * Move the camera of given angles from current position.<br>
 * WARNING : first use of this function assume the bot is in base position (except if RemoteMoveCameraTo was previously
 * used). If not the move will be made from the base postion and not the real current one. Only for first use !
 * Don't use RemoteTournerWebcam with RemoteMoveCameraOf and RemoteMoveCameraTo<br>
 * NOTE : les déplacements de 1 degré semblent problèmatiques, faire au moins 2 voir 3 degrés, 4  pas de problème
 * @param bot robot we want the camera to move.
 * @param angle_horiz angle (degree) to turn on horizontal axis
 * @param angle_vert  angle(degree) to turn on vertical axis
 */
int RemoteMoveCameraOf(robot * bot, int angle_horiz, int angle_vert)
{
	int new_angle_horiz, new_angle_vert, change_side = 0, result;
	
	new_angle_horiz = bot->cam_x + angle_horiz;
	while(new_angle_horiz < 0)//computing same angle modulo pi
		new_angle_horiz = 360 + new_angle_horiz;
	if(new_angle_horiz > 360)
		new_angle_horiz %= 360;
	if(new_angle_horiz > 315) //valid value for camera are 0 <= value <= 315
	//so if we have a value upside 340 we consider that the user wan't to go on the opposite side
	//i.e. if camera was on the right bound (+- 0 degree), it will go on the leftmost bound(340 degree)
	//whereas if it was on left bound (+-340 degree, it will go on the rightmost bound (0 degree)
	{
		change_side = 2;
		if(bot->cam_x < 158)
			new_angle_horiz = 315;
		else
			new_angle_horiz = 0;
	}
	
	new_angle_vert = bot->cam_y + angle_vert;
	while(new_angle_vert < 0)
		new_angle_vert = 360 + new_angle_vert;
	if(new_angle_vert > 360)
		new_angle_vert %= 360;
	if(new_angle_vert > 140)
		new_angle_vert = 140;
	
	result =  RemoteMoveCameraTo(bot, new_angle_horiz, new_angle_vert);
	sleep(change_side);
	return result;
}

int RemoteTournerWebcam(robot * bot, int pan, int tilt, WBSens s)
{
	int res;
	if(bot->camera_only)
	{
		char buffer[BUFFERSIZE];
		char buf[100];
		int taille, nb, sk;
		struct sockaddr_in server_adress;
		
		taille = sprintf(buf, "PanSingleMoveDegree=%d&TiltSingleMoveDegree=%d&PanTiltSingleMove=%d", pan, tilt, s);
		nb = sprintf(buffer, "POST /PANTILTCONTROL.CGI HTTP/1.0\r\nUser-Agent: user\r\nAuthorization: Basic\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s\r\n\r\n", taille, buf);

		sk = RemoteSocket_connect(80, bot->ip, &server_adress);

		res = send(sk, buffer, nb, 0);
		
		if(res != -1)
			res = 0;
		/* ON ATTEND QUE LE MESSAGE SOIT ENVOYE
		 * ET QUE LA CAM AIT REAGI */
		usleep(500000);
		close(sk);		
	}
	else
		res = send_info(bot ,a_camera,pan,tilt,(int)s);	
	return res;
}


int RemoteSocket_connect(int port,char *adresse,struct sockaddr_in *serv_addr)
{
	int res;
	struct  hostent * server;
	if (! (server = gethostbyname(adresse)))
    {
    	printf("erreur DNS\n");
    	return -1;
    }
	//  printf("on a l'ip....\n");
	serv_addr->sin_addr.s_addr = inet_addr(adresse);
	serv_addr->sin_port = htons(port);
	serv_addr->sin_family = AF_INET;
	//printf("on a tout connecté\n");
	if((res = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("socket");
		return -1;
	}
	if(connect(res, (struct sockaddr*)serv_addr, sizeof(*serv_addr)) < 0)
    {
    	perror("connect");
    	return -1;
    }
  return res;
}

/**
 * Get a JPEG image from camera, return 0 if ok.
 * Image and size will be set to the right value, image must be allocated first (at least 150000 bytes, use SIZEIMG).
 */
int RemoteGetImage(robot * bot, unsigned char * image, int * size)
{
	unsigned char buf_rec[BUFFERSIZE];
//	char buf[193];
	int img_size = 0;
	int sk, retval, i;

//	image = (unsigned char *)malloc(SIZEIMG * sizeof(unsigned char));
	//sleeping in case other messages were sent to camera : need time to process them
	sk = RemoteConnectCamera(bot);
	if(sk == -1)
		return -1;
  
  	//else
	send(sk, "GET /IMAGE.JPG HTTP/1.0\r\nUser-Agent: \r\nAuthorization: Basic \r\n\r\n", 128, 0);

	/* On commence � r�cup�rer l'image */
	retval = recv(sk, buf_rec, BUFFERSIZE, 0);
	while(retval != 0)
	{
		/* On copie les donn�es du buffer dans le buffer final */
		memcpy(image+img_size, buf_rec, retval);
		img_size += retval;
		retval = recv(sk, buf_rec, BUFFERSIZE, 0);
	}


	/* Le d�but du buffer est pollu� par les headers HTTP */
	for(i=0; i<img_size-1; i++)
	{
		// une image JPEG commence par 0xFFD8
		if(image[i] == 0xFF && image[i+1] == 0xD8)
		{
			img_size -= i;
			memmove(image, image+i, img_size);
			break;
		}
	}

	/* L'image est enti�rement dans buffer, et fait img_size octets */
	close(sk);
	*size = img_size;
	return 0;	
}

int RemoteStop(robot * bot)
{
	return send_info(bot, a_stop, 0, 0, 0);
}

int RemoteConnec(robot * bot)
{
  struct sockaddr_in addr;
  bot->socket = RemoteSocket_connect(DEFAULT_PORT,bot->ip,&addr);
  if(bot->socket != -1)
  {
  	bot->is_connected = TRUE;
  	return 0;
  }
  else
  {
  	printf("erreur de connection à %s\n", bot->ip);
  	return -1;
  }
}

int RemoteConnecCamera(robot * bot)
{
  struct sockaddr_in addr;
  bot->socket = RemoteSocket_connect(CAM_PORT,bot->ip,&addr);
  if(bot->socket != -1)
  {
  	bot->is_connected = TRUE;
  	return 0;
  }
  else
  {
  	printf("erreur de connection à %s\n", bot->ip);
  	return -1;
  }	
}

int RemoteConnectCamera(robot * bot)
{
  	struct sockaddr_in addr;
	int sock = RemoteSocket_connect(80,bot->ip,&addr);
	if(sock == -1)
  	{
  		printf("erreur de connection à la caméra de %s\n", bot->ip);
  		return -1;
  	}
  	else
  		return sock;
}

int RemoteDisconnect(robot * bot)
{
	  close(bot->socket);
	  bot->is_connected = FALSE;
	  return 0;
}

int RemoteSetMotorMode(robot * bot, motor_mode a_mode)
{
	return send_info(bot, a_setmode, (int)a_mode, 1, 1);
}

/**
 * Turn the bot of given 'angle' (in degree), counterclockwise is positiv (sens trigonométrique).
 * Detail of the process is described in "rotation"
 * WARNING : implementation of this function has been made specifically for wifibot 100, other wifibot
 * surely need another angular_rate
 */
int RemoteTurnOf(robot * bot, int angle)
{
	float angular_rate = 73.33, time_to_turn;
	float speed = 5;
	boolean clockwise_turn = FALSE;
	
	if(angle < 0)
	{
		angle = -angle;
		clockwise_turn = TRUE;
	}
		
	angle %= 360;
	
	//for speed 5, angular rate is 73.33 °/s
	//float vit_tick_sec = (float)vitesse_moteur * (1000/41);
	//float vit_m_sec = vit_tick_sec * WHEEL_CIRCUMFERENCE / NB_TICKS_BY_WHEEL;
	//float vit_angulaire = vit_m_sec/CENTER_ROBOT_TO_CENTER_WHEEL;
	if(angle > 180)
	{
		clockwise_turn = !clockwise_turn;	
		angle = 360 - angle;
	}
	time_to_turn = angle / angular_rate;

	if(clockwise_turn)
		RemoteTournerHoraire(bot, speed, speed);
	else
		RemoteTournerAntiHoraire(bot, speed, speed);
	
	usleep(time_to_turn * 1000000.0);
	RemoteStop(bot);
	return 0;
}

/**
 * Go forward of given 'distance' in meter at fixed speed.
 */
int RemoteForward(robot * bot, float distance)
{
	int vitesse_moteur = 5;
	float vit_tick_sec = (float)vitesse_moteur * (1000/41);
	float vit_m_sec = vit_tick_sec * WHEEL_CIRCUMFERENCE / NB_TICKS_BY_WHEEL;
	float time_to_move = distance / vit_m_sec;
	RemoteAvancerCourbe(bot, vitesse_moteur, vitesse_moteur);
	usleep(time_to_move * 1000000.0);
	RemoteStop(bot);
	return 0;
}

robot * initRobot(char * ip)
{
	int sk;
	
	robot * bot = (robot *) malloc(sizeof(robot));
	bot->ip = ip;
	bot->cam_x = 158;
	bot->cam_y = 120;
	bot->is_connected = 0;
	bot->socket = -1;
	bot->camera_only = FALSE;
	
	sk = RemoteConnec(bot);
	
	RemoteMoveCameraTo(bot, 158, 120);
	return bot;
}

robot * initCameraOnly(char * ip)
{
	int sk;
	
	robot * bot = (robot *) malloc(sizeof(robot));
	bot->ip = ip;
	bot->cam_x = 158;
	bot->cam_y = 120;
	bot->is_connected = 0;
	bot->socket = -1;
	bot->camera_only = TRUE;
	
	sk = RemoteConnecCamera(bot);
	
	RemoteMoveCameraTo(bot, 158, 120);
	return bot;	
}

robot * initCameraWitoutMoving(char * ip)
{
	int sk;
	
	robot * bot = (robot *) malloc(sizeof(robot));
	bot->ip = ip;
	bot->cam_x = 158;
	bot->cam_y = 120;
	bot->is_connected = 0;
	bot->socket = -1;
	bot->camera_only = TRUE;
	
	sk = RemoteConnecCamera(bot);
	
	return bot;	
}

int RemoteCenterOnObject(robot * bot, object * object_to_center_on, boolean display, boolean vertical_centering)
{
	int image_size, width, height;
	int dist_x = 1000, dist_y = 1000, last_dist_x = 1500, last_dist_y = 1500;
	int roi_center_x, roi_center_y;
	int  needed_cam_move_x, needed_cam_move_y, move_increment = 3;
	boolean continue_centering = TRUE, ask_for_object = TRUE;
	unsigned char image_jpeg[SIZEIMG], * image_rgb;
	hsl_image * image_hsl;
//	roi * object_roi = NULL;
	#ifdef DEBUG
	printf("RemoteCenterOnObject() %s begin centering on object %s\n", bot->ip, object_to_center_on->name);
	#endif
	
	if(display)
		initSDL();
		
	do
	{
		if(object_to_center_on->object_roi != NULL)
		{
			free(object_to_center_on->object_roi);
			object_to_center_on->object_roi = NULL;
		}
		last_dist_x = dist_x;
		last_dist_y = dist_y;
		needed_cam_move_x = 0;
		needed_cam_move_y = 0;
		
		RemoteGetImage(bot, image_jpeg, &image_size);
		image_rgb = jpegBufferDecompress(image_jpeg, image_size, &width, &height);
		image_hsl = rgb2hsl(image_rgb, width, height);
		
		if(image_hsl == NULL)
		{
			printf("problem converting to hsl\n");
			continue;
		}
			
		RemoteGiveObjectRoi(bot, object_to_center_on, image_hsl);
		freeHslImage(image_hsl);
		
		if(object_to_center_on->object_roi == NULL)
		{
			#ifdef DEBUG
			printf("RemoteCenterObObject() %s lost, returning\n", object_to_center_on->name);
			#endif
			free(image_rgb);
			return -1;
		}
		if(ask_for_object)
		{
			drawROIOnImage(image_rgb, object_to_center_on->object_roi, width, height);
			if(!askIfGoodObject(image_rgb, width, height))
			{
				free(image_rgb);
				return -2;
			}
			ask_for_object = FALSE;
		}
		//else
		if(display)
		{
			drawCross(image_rgb, width, height);
			drawROIOnImage(image_rgb, object_to_center_on->object_roi, width, height);
			displayRGBBuffer(image_rgb, width, height);
		}
		free(image_rgb);
		
		roi_center_x = object_to_center_on->object_roi->begin_x + (object_to_center_on->object_roi->end_x - object_to_center_on->object_roi->begin_x)/2;
		dist_x = roi_center_x - width/2;
		//case when the camera hasn't actually moved : the roi distance between last and actual shot should
		//be very close. To have more precision, we reset camera's position to its old value.
		//This is because camera' position field are updated with each camera move order even if the order wasn't followed. 
/*		if(abs(dist_x - last_dist_x) <= 3 * move_increment)
		{//see RemoteMoveCameraOf() for more precision
			bot->cam_x -= needed_cam_move_x;
			continue;
		}*/
		//moving camera so that it goes on the object
		if(dist_x < 0)
			needed_cam_move_x = -move_increment;
		else if(dist_x > 0)
			needed_cam_move_x = move_increment;
		
		if(vertical_centering)
		{		
			dist_y = roi_center_y - height/2;
			roi_center_y = object_to_center_on->object_roi->begin_y + (object_to_center_on->object_roi->end_y - object_to_center_on->object_roi->begin_y)/2;
			if(dist_y < 0)
				needed_cam_move_y = -move_increment;
			else if(dist_y > 0)
				needed_cam_move_y = move_increment;
		}
		
/*		if(roi_center_x > width/2 && roi_center_x < (width *2)/3)
			needed_cam_move_x = 1;
		else if(roi_center_x < width/2 && roi_center_x > (width/3))
			needed_cam_move_x = -1;

		if(roi_center_y < height/2 && roi_center_y > (height/3))
			needed_cam_move_y = -1;
		else if(roi_center_y > height/2 && roi_center_y < (height*2)/3)
			needed_cam_move_y = 1;
*/			
		#ifdef DEBUG
		printf("object size :%d\nmoving x:%d y:%d\ndistance, old|new x:%d|%d y:%d|%d\n", object_to_center_on->object_roi->object_pixel_in_roi, needed_cam_move_x, needed_cam_move_y, last_dist_x, dist_x, last_dist_y, dist_y);
		printf("x position: %d\n************\n", bot->cam_x);
		#endif
		RemoteMoveCameraOf(bot, needed_cam_move_x, needed_cam_move_y);
		if(last_dist_x < 1000)
		{
			if((dist_x <= 0 && last_dist_x >= 0) || (dist_x >= 0 && last_dist_x <= 0))
			{
				if(vertical_centering)
				{
					if((dist_y <= 0 && last_dist_y >= 0) || (dist_y >= 0 && last_dist_y <= 0))
						continue_centering = FALSE;
				}
				else
					continue_centering = FALSE;
			}
		}
	}
	while(continue_centering);
	
	//if object was closer to the center before last move, going back.
	if(abs(last_dist_x) < abs(dist_x))
		object_to_center_on->x_position += needed_cam_move_x;
	if(vertical_centering && abs(last_dist_y) < abs(dist_y))
		object_to_center_on->y_position += needed_cam_move_y;
	#ifdef DEBUG
	printf("RemoteCenterOnObject() finished centering on %s\n", object_to_center_on->name);
	#endif
	return 0;
}

int RemoteCenterOnSecondObject(robot * bot, object * object_to_find, boolean display, boolean vertical_centering)
{
	int image_size, width, height, dist_x = 1000, dist_y = 1000, last_dist_x, last_dist_y;
	int roi_center_x, roi_center_y, needed_cam_move_x = 0, needed_cam_move_y = 0, move_increment = 3;
	boolean continue_centering = TRUE;
	unsigned char image_jpeg[SIZEIMG], * image_rgb;
	hsl_image * image_hsl;
	roi * object_roi = NULL;
	#ifdef DEBUG
	printf("RemoteCenterOnSecondObject() %s begin centering on second %s\n", bot->ip, object_to_find->name);
	#endif
	
	if(display)
		initSDL();
	do
	{
		last_dist_x = dist_x;
		last_dist_y = dist_y;
		RemoteGetImage(bot, image_jpeg, &image_size);
		image_rgb = jpegBufferDecompress(image_jpeg, image_size, &width, &height);
		image_hsl = rgb2hsl(image_rgb, width, height);
		if(image_hsl == NULL)
		{
			printf("RemoteCenterOnSecondObject() :problem converting to hsl\n");
			continue;
		}
		//if object is not found once, no need to continue
		if(!giveObjectRoi(object_to_find, image_hsl))
		{
			printf("RemoteCenterOnSecondObject() :%s hasn't been found!\n", object_to_find->name);
			free(image_rgb);
			//Il y a un problème étrange, dans la fonction pour trouver le 2eme objet d'un type donné, si le 2 eme objet n'est pas 
			//présent en meme temps que le 1er sur l'image, il arrive que le passage du rgb au hsl donne une image_hsl corrompu.
			//La méthode mde passage rgb au hsl ne semble pas en cause, toujours est il que le pointeur que l'on a en sortie de la fonction
			//n'est pas le meme que le pointeur retourné. Ainsi on peut désallouer les structure interne à l'image mais pas l'image elle meme.
			//Perso j'y comprends rien, et j'ai pas le temps d'approfondir !
			freeHslImage(image_hsl);
			return -2;
		}
		//else
		object_roi = copyRoi(object_to_find->object_roi);
		if(giveObjectRoi(object_to_find, image_hsl))
			object_roi = object_to_find->object_roi;
		else
			object_to_find->object_roi = object_roi;
		
		freeHslImage(image_hsl);
		
		if(object_roi == NULL)
		{
			printf("RemoteCenterOnSecondObject() :%s wasn't found twice\n", object_to_find->name);
			free(image_rgb);
			return -3;
		}
		
		if(display)
		{
			drawCross(image_rgb, width, height);
			drawROIOnImage(image_rgb, object_roi, width, height);
			displayRGBBuffer(image_rgb, width, height);
		}
		free(image_rgb);
		roi_center_x = object_roi->begin_x + (object_roi->end_x - object_roi->begin_x)/2;
		dist_x = roi_center_x - width/2;
		
		//case when the camera hasn't actually moved : the roi distance between last and actual shot should
		//be very close. To have more precision, we reset camera's position to its old value.
		//This is because camera' position field are updated with each camera move order even if the order wasn't followed. 
/*		if(abs(dist_x - last_dist_x) <= 3 * move_increment)
		{//see RemoteMoveCameraOf() for more precision DON'T WORK, anywhere
			bot->cam_x -= needed_cam_move_x;
			continue;
		}*/
		
		//moving camera so that it goes on the object
		if(dist_x < 0)
			needed_cam_move_x = -move_increment;
		else if(dist_x > 0)
			needed_cam_move_x = move_increment;
		
		if(vertical_centering)
		{		
			dist_y = roi_center_y - height/2;
			roi_center_y = object_roi->begin_y + (object_roi->end_y - object_roi->begin_y)/2;
			if(dist_y < 0)
				needed_cam_move_y = -move_increment;
			else if(dist_y > 0)
				needed_cam_move_y = move_increment;
		}
/*		if(roi_center_x > width/2 && roi_center_x < (width *2)/3)
			needed_cam_move_x = 1;
		else if(roi_center_x < width/2 && roi_center_x > (width/3))
			needed_cam_move_x = -1;

		if(roi_center_y < height/2 && roi_center_y > (height/3))
			needed_cam_move_y = -1;
		else if(roi_center_y > height/2 && roi_center_y < (height*2)/3)
			needed_cam_move_y = 1;
*/			
		#ifdef DEBUG
		printf("object size :%d\nmoving x:%d y:%d\ndistance, old|new x:%d|%d y:%d|%d\n", object_roi->object_pixel_in_roi, needed_cam_move_x, needed_cam_move_y, last_dist_x, dist_x, last_dist_y, dist_y);
		printf("x position: %d\n************\n", bot->cam_x);
		#endif
		RemoteMoveCameraOf(bot, needed_cam_move_x, needed_cam_move_y);
		if(last_dist_x < 1000)
		{
			if((dist_x <= 0 && last_dist_x >= 0) || (dist_x >= 0 && last_dist_x <= 0))
			{
				if(vertical_centering)
				{
					if((dist_y <= 0 && last_dist_y >= 0 )|| (dist_y >= 0 && last_dist_y <= 0))
						continue_centering = FALSE;
				}
				else
					continue_centering = FALSE;
			}
		}
	}
	while(continue_centering);
	
	//if object was more in the center before last move, going back.
	if(abs(last_dist_x) > abs(dist_x))
		RemoteMoveCameraOf(bot, -needed_cam_move_x, 0);
	if(vertical_centering && abs(last_dist_y) < abs(dist_y))
		RemoteMoveCameraOf(bot, 0, -needed_cam_move_y);
	#ifdef DEBUG
	printf("RemoteCenterOnSecondObject() :finished centering on %s\n", object_to_find->name);
	#endif
	return 0;
}

int RemoteFindSecondObject(robot * bot, object * object_to_find, boolean display)
{
	hsl_image * image_hsl;
	roi * object_roi;
	unsigned char image_jpeg[SIZEIMG], * image_rgb;
	int image_size, width, height;
	//cam increment shouldn't be too big else we can mistake second object for first one
	int move_x = 0, move_y = 0, cam_increment = 10;
	int first_object_distance = -1, crt_object_distance = 0, delta = 5;
	
	boolean ask_if_good_object = TRUE;

	int cam_init_position = bot->cam_x;
	int cam_crt_position = bot->cam_x;
	int cam_last_position = bot->cam_x;
	
	if(display)
		initSDL();
	#ifdef DEBUG
	printf("RemoteFindSecondObject() %s searching %s\n", bot->ip, object_to_find->name);
	#endif
	do
	{
		#ifdef DEBUG
		printf("%s not found , moving camera to %d\n", object_to_find->name, bot->cam_x);
		#endif
		RemoteMoveCameraOf(bot, move_x, move_y);
		RemoteGetImage(bot, image_jpeg, &image_size);
		image_rgb = jpegBufferDecompress(image_jpeg, image_size, &width, &height);
		image_hsl = rgb2hsl(image_rgb, width, height);
		if(image_hsl == NULL)
			continue;

		//object found
		if(RemoteGiveObjectRoi(bot, object_to_find, image_hsl))
		{
			object_roi = object_to_find->object_roi;
			//case this is the first object found
			if(first_object_distance == -1)
			{
				first_object_distance = object_roi->begin_x + (object_roi->end_x - object_roi->begin_x)/2; 
				//if an other object is present, then we have finish
				if(RemoteGiveObjectRoi(bot, object_to_find, image_hsl))
				{
					drawROIOnImage(image_rgb, object_to_find->object_roi, width, height);
					freeHslImage(image_hsl);
					if(ask_if_good_object && !askIfGoodObject(image_rgb, width, height))
					{
						free(image_rgb);
						continue;
					}
					free(image_rgb);
					#ifdef DEBUG
					printf("RemoteFindSecondObject() %s found!\n", object_to_find->name);
					#endif
					return 0;
				}
			}
			//case we already found an object before moving the camera.
			//the present object could be the old one. Because we move the camera from left to right,
			//if the current object position is further on the left than old object position, this is the old
			//object (new object should come from the right)
			else
			{
				crt_object_distance = object_roi->begin_x + (object_roi->end_x - object_roi->begin_x)/2;
				//this is the case where we are still on the first object (it is now more on the left of its old position)
				if(first_object_distance >= crt_object_distance + delta)
				{
					if(RemoteGiveObjectRoi(bot, object_to_find, image_hsl))
					{
						drawROIOnImage(image_rgb, object_to_find->object_roi, width, height);
						freeHslImage(image_hsl);
						if(ask_if_good_object && !askIfGoodObject(image_rgb, width, height))
						{
							free(image_rgb);
							continue;
						}
						//else
						#ifdef DEBUG
						printf("RemoteFindSecondObject() %s found!\n", object_to_find->name);
						#endif
						free(image_rgb);
						return 0;
					}
					first_object_distance = crt_object_distance;
				}
				//this case is when the object found is further on the right than position of the previously
				//found object. It means that the found object is the second one (or still the first one but 
				//seen after a complete turn).
				else if(crt_object_distance >= first_object_distance + delta)
				{
					drawROIOnImage(image_rgb, object_to_find->object_roi, width, height);
					freeHslImage(image_hsl);
					if(ask_if_good_object && !askIfGoodObject(image_rgb, width, height))
					{
						free(image_rgb);
						continue;
					}
					#ifdef DEBUG
					printf("RemoteFindSecondObject() %s found!\n", object_to_find->name);
					#endif
					free(image_rgb);
					return 0;
				}
			}
		}
		freeHslImage(image_hsl);
		if(display)
		{
			drawCross(image_rgb, width, height);
			displayRGBBuffer(image_rgb, width, height);
		}
		free(image_rgb);
		move_x = cam_increment;
		move_y = 0;//only searching on horizonal axis, move_y is here just in case...
		cam_last_position = cam_crt_position;
		cam_crt_position = bot->cam_x;
	}
	while(!angleIsBetween(cam_init_position, cam_last_position, cam_crt_position));
	

	#ifdef DEBUG
	printf("RemoteFindSecondObject() finished, %s not found\n", object_to_find->name);
	#endif
	return -1;
}

list_object * RemoteFindAndCenter(robot * bot, list_object ** list, boolean display)
{
	//for finding the object will keep track of initial camera position : if the camera ever come back to its
	//initial position, it has make a whole turn, thus its not necessary to continue searching.
	int cam_init_position = bot->cam_x;
	int cam_crt_position = bot->cam_x;
	int cam_last_position = bot->cam_x;
	
	int move_increment = 20;
	
	unsigned char image_jpeg[SIZEIMG], * image_rgb;
	hsl_image * image_hsl;
	int image_size, width, height;
	
	list_object * list_pointer, * next_pointer;
	list_object * list_of_found_object = NULL;
	
	if(display)
	{
		initSDL();
	}
	#ifdef DEBUG
	printf("RemoteFindAndCenter() beginning on %s, camera is at %d\n", bot->ip, bot->cam_x);
	#endif
		
	//tant qu'il reste des objets a trouvés et que l'on a pas fait un tour complet
	while(*list != NULL && !angleIsBetween(cam_init_position, cam_last_position, cam_crt_position))
	{
		RemoteGetImage(bot, image_jpeg, &image_size);
		image_rgb = jpegBufferDecompress(image_jpeg, image_size, &width, &height);
		image_hsl = rgb2hsl(image_rgb, width, height);
		
		if(display)
		{
			drawCross(image_rgb, width, height);
			displayRGBBuffer(image_rgb, width, height);
		}
		free(image_rgb);
		
		if(image_hsl == NULL)
		{
			printf("error getting HSL image in RemoteFindAndCenter\n");
			continue;
		}
		
		//si au moins un objet a été trouvé
		if(giveObjectsRoi(*list, image_hsl))
		{
			list_pointer = *list;
			//on cherche les objets trouvés
			while(list_pointer != NULL)
			{
				//un objet trouvé
				if(list_pointer->found == TRUE)
				{
					#ifdef DEBUG
					printf("RemoteFindAndCenter() %s found\n", list_pointer->item->name);
					#endif
					if(RemoteCenterOnObject(bot, list_pointer->item, display, FALSE) != 0)
					{//if object was found on the finding phase but not in the centering phase, then we go on and consider it as not found
						printf("RemoteFindAndCenter() %s lost\n", list_pointer->item->name);
						reinitObject(list_pointer->item);
						list_pointer->found = FALSE;
						list_pointer = list_pointer->next;//rajouter modif var position cam.
						continue;
					}	
					//on ajoute l'objet à la liste des objets trouvés
					next_pointer = list_pointer->next;

					removeObjectFromList(list_pointer, list);
					addObjectToList(list_pointer, &list_of_found_object);
					
					#ifdef DEBUG
					printf("RemoteFindAndCenter() finished centering on %s, going back to %d\n", list_pointer->item->name, cam_crt_position);
					#endif
					list_pointer = next_pointer;
					//on replace la caméra la ou les objets ont été trouvés (dans le cas ou plusieurs
					//objets ont été trouvés: leur centrage déplace la camera)
					RemoteMoveCameraTo(bot, cam_crt_position, bot->cam_y);
					continue;
				}
				else
					list_pointer = list_pointer->next;			
			}
		}
		freeHslImage(image_hsl);
		//on tourne la caméra
		cam_last_position = bot->cam_x;
		RemoteMoveCameraOf(bot, move_increment, 0);
		#ifdef DEBUG
		printf("moving camera to %d\n", bot->cam_x);
		#endif
		cam_crt_position = bot->cam_x;
	}
	#ifdef DEBUG
	printf("RemoteFindAndCenter() finished on %s\n", bot->ip);
	#endif
	return list_of_found_object;
}

/*
int RemoteFindAndCenter(robot * bot, list_object * list)
{
	int result = -1, try_nb = 0;
	list_object * p_list = list;
	
	if(RemoteFindObject(bot, object_hue, object_size, display) == -1)
		return -1;
	//else
	while(try_nb < 4 && result == -1)
	{
	 result = RemoteCenterOnObject(bot, object_hue, object_size, allow_horizontal, allow_vertical, display, vertical_centering);
	 try_nb++;
	}
	return result;
}
*/
int RemoteFindAndCenterOnSecondObject(robot * bot, object * object_to_find, boolean display, boolean vertical_centering)
{
	int result = -1, try_nb = 0;
	if(RemoteFindSecondObject(bot, object_to_find, display) != 0)
		return -1;
	//else
	while(try_nb < 4 && result != 0)
	{
		result =  RemoteCenterOnSecondObject(bot, object_to_find, display, vertical_centering);
		try_nb++;
	}
	return result;
}

boolean RemoteGiveObjectRoi(robot * bot, object * object_to_find, hsl_image * image_hsl)
{
	if(giveObjectRoi(object_to_find, image_hsl) == FALSE)
		return FALSE;
	//else
	object_to_find->x_position = bot->cam_x;
	object_to_find->y_position = bot->cam_y;
	
	return TRUE;
}
