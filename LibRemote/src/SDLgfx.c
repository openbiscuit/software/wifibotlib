#include "SDLgfx.h"

SDL_Surface * screen;

int init_sdl = 0;

void putPixel(int x, int y, int R, int G, int B)
{
  unsigned int *ptr = (unsigned int*)screen->pixels;
  int lineoffset = y * screen->w;//(screen->pitch / 4);
  //scaling RGB values uf needed
  if(R > 255)
  	R = 255;
  if(G > 255)
  	G = 255;
  if(B > 255)
  	B = 255;
  if(R < 1)
  	R = 1;
  if(G < 1)
  	G = 1;
  if(B < 1)
  	B = 1;
  	
  ptr[lineoffset + x] = (R <<16) | (G << 8) | B;
}

/**
 * Draw a red cross in the center of the image.
 */
void drawCross(unsigned char * image, int width, int height)
{
	int index;
	index = width*(height/2 -1) * 3;
	index += (width/2)*3;

	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;
	
	index += (width - 1)*3; 

	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;
		
	index += (width -3)*3;
	
	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;

	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;
	
	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;
	
	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;
	
	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;	
	
	index += (width -3)*3;
	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;	

	index += (width-1)*3;
	image[index++] = 200;
	image[index++] = 10;
	image[index++] = 20;

}

int initSDL()
{
	if(init_sdl)//allready initialized, this allow multiple call without problem
		return 0;
	
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) 
  	{
    	fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    	return -1;
  	}

  	// Register SDL_Quit to be called at exit; makes sure things are
  	// cleaned up when we quit.
  	atexit(SDL_Quit);
    
  	// Attempt to create a 640x480 window with 32bit pixels.
  	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);
  
  	// If we fail, return error.
  	if ( screen == NULL ) 
  	{
	    fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
    	return -1;
  	}
  	init_sdl = 1;
  	return 0;
}


int displayRGBBuffer(unsigned char * image_buffer, int width, int height)
{
	int line, row, index;
	
   	//screen = SDL_SetVideoMode(image->w, image->h, 8, SDL_SWSURFACE|SDL_ANYFORMAT);
    screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);
    if ( screen == NULL ) {
    	fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
	   	SDL_GetError());
		return -1;
    }
    
    for(line = 0; line < height; line++)
    	for(row = 0; row < width; row++)
    	{
    		index = line * (width * 3) + (row * 3); 
    		putPixel(row, line, image_buffer[index], image_buffer[index + 1], image_buffer[index + 2]);
    	}
    /*printf("Set 640x480 at %d bits-per-pixel mode\n",
      screen->format->BitsPerPixel);*/

    // Draws the image on the screen:
    /* Blit onto the screen surface */

    	
    SDL_UpdateRect(screen, 0, 0, width, height);

    return 0;
}

int displayPPMFile(char * file_name)
{
	unsigned char * image_rgb;
	char string[10];
	int trash, taille, index, val;
	int width, height;
	FILE * p_file;
	
	p_file = fopen(file_name, "rb");
	fscanf(p_file, "%s %d %d %d", string, &width, &height, &trash);
	taille = width * height * 3;
	image_rgb = (unsigned char *)malloc(taille * sizeof(unsigned char));
	for(index = 0; index < taille; index+=3)
	{
		val = fgetc(p_file);
		image_rgb[index+2] = val;
		val = fgetc(p_file);
		image_rgb[index] = val;
		val = fgetc(p_file);
		image_rgb[index+1] = val;
	}
	
	val = displayRGBBuffer(image_rgb, width, height);
	free(image_rgb);
	return val;
}

int displayJPGFile(char * file_name)
{
	SDL_RWops *rwop;
	SDL_Surface *image;
	
	rwop=SDL_RWFromFile(file_name, "rb");
	image=IMG_LoadJPG_RW(rwop);
	
	if(!image)
	{
	   	printf("IMG_LoadJPG_RW: %s\n", IMG_GetError());
	   	return -1;
	}
	screen = SDL_SetVideoMode(image->w, image->h, 8, SDL_SWSURFACE|SDL_ANYFORMAT);
	
	if ( screen == NULL )
	{
		fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
		SDL_GetError());
		return -1;
	}
	if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
	{
		fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
		return -1;
	}
	
	SDL_UpdateRect(screen, 0, 0, image->w, image->h);
	return 0;
}

/**
 * Display a jpeg image.
 * @param bufferImg pointer to the image.
 * @param sizeImage size of the buffer.
 */
int displayJPGBuffer(unsigned char * bufferImg, int sizeImage)
{
	SDL_RWops * rwop = SDL_RWFromMem(bufferImg, sizeImage);
	
  	SDL_Surface * image = IMG_LoadJPG_RW(rwop);
  	if(!image)
  	{
    	printf("display>IMG_LoadJPG_RW: %s\n", IMG_GetError());
    	return -1;
  	}
  	else 
  	{
    	printf("IMG size : %dx%d\n",image->w, image->h);
    	/* Initialize the best video mode */
    	/* Have a preference for 24-bit, but accept any depth */
    	screen = SDL_SetVideoMode(image->w, image->h, 8, SDL_SWSURFACE|SDL_ANYFORMAT);
    	if ( screen == NULL )
    	{
      		fprintf(stderr, "Couldn't set 640x480x24 video mode: %s\n",
	      	SDL_GetError());
			return -1;
    	}
    	/*printf("Set 640x480 at %d bits-per-pixel mode\n",
      	screen->format->BitsPerPixel);*/

    	// Draws the image on the screen:
    	/* Blit onto the screen surface */
    	if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
    	{
      		fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
      		return -1;
    	}
    	SDL_UpdateRect(screen, 0, 0, image->w, image->h);

    	return 0;
  	}
}