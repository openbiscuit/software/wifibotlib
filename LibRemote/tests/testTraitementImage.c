#include "LibRemote.h"

int main(int argc, char * argv[])
{
	object * obj1, * obj2, * obj3;
	list_object * l_obj1, * l_obj2, * l_obj3, * p_list;
	hsl_image * image_hsl;
	unsigned char * image_rgb;
	int width, height;
	
	if(argc != 2)
	{
		printf("usage : testTraitementImage filename.ppm\n");
		return -1;
	}
	printf("testing on image %s\n", argv[1]);
	
	obj1 = initObject("jaune");
	obj1->hue = 63;
	obj1->delta_color = 4;
	obj1->allow_discontinuity = TRUE;
	obj1->max_inadequate_adjacent_pixel = 2;
	l_obj1 = initListObject(obj1);
	
	obj2 = initObject("cone");
	obj2->hue = 0;
	obj2->delta_color = 5;
	obj2->allow_discontinuity = TRUE;
	obj2->max_inadequate_adjacent_pixel = 2;
	l_obj2 = initListObject(obj2);
	addObjectToList(l_obj2, &l_obj1);
	
	obj3 = initObject("obj3");
	obj3->hue = 191;
	obj3->delta_color = 7;
	obj3->allow_discontinuity = TRUE;
	obj3->max_inadequate_adjacent_pixel = 3;
	l_obj3 = initListObject(obj3);
	addObjectToList(l_obj3, &l_obj1);

	image_rgb = ppmFileDecompress(argv[1], &width, &height);
	image_hsl = rgb2hsl(image_rgb, width, height);
	
	giveObjectsRoi(l_obj1, image_hsl);
	free(image_hsl);
		
	p_list = l_obj1;
	
	while(p_list != NULL)
	{
		if(p_list->found == TRUE)
		{
			printf("trouvé object %s\n", p_list->item->name);
			drawROIOnImage(image_rgb, p_list->item->object_roi, width, height);
			p_list = p_list->next;
		}
	}

	freeList(l_obj1);

	initSDL();
	displayRGBBuffer(image_rgb, width, height);
	free(image_rgb);
	
	while(1)
	{
		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_KEYUP:
				// If escape is pressed, return (and thus, quit)
        			if (event.key.keysym.sym == SDLK_ESCAPE)
          				return 1;
        			break;
      			case SDL_QUIT:
        			exit(0);
			}
		}
	}
}