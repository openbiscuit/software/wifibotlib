#include "LibRemote.h"

int main(int argc, char * argv[])
{
	char * ip = "192.168.1.100";
	char string[20];
	char ordre = 'a';
	int int1, int2;
	robot * bot;
	
	bot = initRobot(ip);
	while(ordre != 'q')
	{
		printf("ordre nombre nombre\n");
		gets(string);
		sscanf(string, "%c %d %d", &ordre, &int1, &int2);
		switch(ordre)
		{
			case 'a':
				RemoteAvancerCourbe(bot, int1, int2);
				sleep(2);
				RemoteStop(bot);
				break;
			case 't':
				RemoteTurnOf(bot, int1);
				break;
			default:
				break;
		}
	}
	RemoteDisconnect(bot);
	free(bot);	
}