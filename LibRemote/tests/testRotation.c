#include "LibRemote.h"

int main(int argc, char * argv[])
{
	int vitesse, duree, res, size;
	unsigned char image[SIZEIMG];
	robot * p_bot;
	char * ip = "192.168.1.100"; 
	
	if(argc != 3 && argc != 4)
	{
		printf("testRotation vitesse duree(sec) (ip)\n");
		return 0;
	}
	vitesse = atoi(argv[1]);
	duree = atoi(argv[2]);
	
	if(argc == 4)
		ip = argv[3];
		
	//initSDL();
	p_bot = initRobot(ip);
	
	//res = RemoteGetImage(p_bot, image, &size);
	//displayJPGBuffer(image, size);
	
	
	RemoteTournerAntiHoraire(p_bot, vitesse, vitesse);
	sleep(duree);
	RemoteDisconnect(p_bot);
	free(p_bot);
}