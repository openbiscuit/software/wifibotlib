#include "LibRemote.h"

int main(int argc, char * argv[])
{
	robot * cam;
	int size, width, height;
	unsigned char image_jpeg[150000], * image_rgb;
	char * file;
	file = (char *) malloc(50 * sizeof(char));
	//cam = robot * initCameraWitoutMoving("192.168.0.20");
	cam = initCameraWitoutMoving("192.168.0.21");
	//RemoteMoveCameraTo(bot, 200, 120);
	if(argc > 1)
		file = argv[1];
	else
		file = "test.ppm";
		
	if(RemoteGetImage(cam, image_jpeg, &size) != 0)
	{
		printf("error while retrieveing image\n");
		return -1;
	}
	image_rgb = jpegBufferDecompress(image_jpeg, size, &width, &height);
	writePPMImage(file, image_rgb, width, height);
	return 0;
}
