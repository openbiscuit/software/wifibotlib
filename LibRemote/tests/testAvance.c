#include "LibRemote.h"
#include <stdio.h>

int main(int argc, char * argv[])
{
	char * ip= "192.168.1.100";
	char * liste_options= "a:t:hl:r:n:";
	int option, nb_it = 1, i;
	int time = 2;
	robot * bot;
	int motor_set = 0;
	int vg, vd;
	
	while ((option = getopt (argc, argv, liste_options)) != -1)
	{
		switch (option)
		{
			case 'a':
				ip = optarg;
				break;
			case 't':
				time = atoi(optarg);  
				break;
			case 'h':
				printf("usage : %s [-a robot_ip_address (192.168.1.100)] [-n nb_iteration (1)] [-t time_to_move_sec (2)] -l vitesse_gauche -r vitesse_droite\n", argv[0]);
				return 0;
			case 'l':
				vg = atoi(optarg);
				motor_set++;
				break;
			case 'r':
				vd = atoi(optarg);
				motor_set++;
				break;
			case 'n':
				nb_it = atoi(optarg);
			default:
				break;
		}
	}
	
	if(motor_set != 2)
	{
		printf("set both motor speed with -vd X and -vg Y\n");
		return -1;
	}
	
	int max = (vg>vd)? vg : vd;
	
	bot = initRobot(ip);
	for(i = 0; i < nb_it; i++)
	{
		RemoteAvancerCourbe(bot, vg, vd);
		sleep(time/nb_it);
		RemoteStop(bot);
	}
	RemoteDisconnect(bot);
	free(bot);
	
}