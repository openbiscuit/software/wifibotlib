#include "LibRemote.h"

int main(int argc, char * argv[])
{
	int angle;
	robot * p_bot;
	char * ip = "192.168.1.100"; 
	
	if(argc != 3 && argc != 2)
	{
		printf("testRotation2 angle (ip)\n");
		return 0;
	}
	
	angle = atoi(argv[1]);
	if(argc == 3)
		ip = argv[2];
		
	p_bot = initRobot(ip);
	
	RemoteTurnOf(p_bot, angle);
	RemoteDisconnect(p_bot);
	free(p_bot);
}