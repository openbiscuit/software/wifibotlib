/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/**
 * @file   LibRemote.c
 * @brief  Méthodes pour dirigier le wifibot à distance
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 * Suppose qu'un serveur de type 'remote.wb' tourne sur le robot.
 * L'utilisation normale suppose qu'un RemoteConnec soit fait avant toute autre appel de fonction de la librairie.
 * Par la suite, avant de finir le programme, un RemoteStop permet d'immobiliser le robot et RemoteDisconnect termine
 * proprement en déconnectant et laissant le socket libre.
 */
#ifndef LIBREMOTE_H_
#define LIBREMOTE_H_

/***************INCLUDE*************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"
#include "libwb/jpeg_tab_src.h"
#include "libwb/wb.h"
#include "TraitementImage.h"
#ifndef NOSDL
#include "SDLgfx.h"
#endif
/***************CONSTANTE************************/

#define NB_TICKS_BY_WHEEL 300//number of ticks on the wheel for odometry measure
#define WHEEL_CIRCUMFERENCE 0.371 // meter
#define CENTER_ROBOT_TO_CENTER_WHEEL 0.149 //meter
#define DEFAULT_PORT 6080//default port for socket opening
#define CAM_PORT 80//port to acces camera
#define SIZEIMG 150000//size of jpeg image get from the camera
#define BUFFERSIZE 512//size of buffer for communication with server on the robots

/***************STRUCTURE**************************/
/**
 * Possible action to send to robot
 */
typedef enum action
{
	a_stop,
	a_avance,
	a_camera,
	a_recule,
	a_senshoraire,
	a_sensantihoraire,
	a_getinfo,
	a_setmode,
	a_camerato
}action;

typedef enum motor_mode
{
	ctrl_off = 0,
	ctrl_on
}motor_mode;

/**
 * used to represent either a wifibot or a lone ipcamera.
 */
typedef struct robot_t
{
	char * ip;//ip of the robot or camera
	int cam_x;//x position of the camera (in degree)
	int cam_y;//y position of the camera (in degree)
	int socket;//store a socket (only in robot I think) for communication. Camera use temporary socket
	boolean is_connected;//Robot as to be connected to give it order.
	boolean camera_only;//Used when connected not to a robot but a sole camera (preferably one like the one on
	//the wifibot
} robot;

/***************GLOBAL****************************/

/***************FONCTION**************************/

/**
 * initialize communication with robot, program remote must have been previously lauchned on the bot.
 * Note that this function should be called on each robot prior to any other functions of the LibRemote.
 * @param ip ip adress of the robot we want to communicate with.
 */
robot * initRobot(char * ip);

/**
 * Init a connection to an ip camera directly connected to the computer. None of the robot specific functions
 * will work, only camera function like camera move or centering on object will work.
 */
robot * initCameraOnly(char * ip);

/**
 * This will do the same thing as initCameraOnly but won't set its postion to origin.
 * For this reason it SHOULDN'T BE USE in most case. Still, the returned robot structure will have
 * its position member set to origin, thus, call to RemoteMoveCameraOf or RemoteMoveCameraTo will refer to true origin. 
 */
robot * initCameraWitoutMoving(char * ip);

/**
 * put all the information of the sensor of the robot in a WBinfo struct (see trunk/include/libwb/wbi2c.h
 * for detail of the structure).
 * @param bot wifibot we want the information of.
 * @param info structure pointer in which the information will be set.
 */
int RemoteGetInfo(robot * bot, WBInfo * info);

/**
 * Specify two different speed to apply, one for left motor, the other for right motor. the speed are applied
 * to make the robot move forward.
 * @bot robot to set the speed off.
 * @vg forward speed of the left motor (in ticks/41ms).
 * @vd forward speed of right motor  (in ticks/41ms)
 */
int RemoteAvancerCourbe(robot * bot, int vg, int vd);

/**
 * Specify two different speed to apply, one for left motor, the other for right motor. the speed are applied
 * to make the robot move backward.
 * @bot robot to set the speed off.
 * @vg backward speed of the left motor (in ticks/41ms).
 * @vd backward speed of right motor  (in ticks/41ms)
 */
int RemoteReculerCourbe(robot * bot, int vg, int vd);

/**
 * Specify two different speed to apply, one for left motor, the other for right motor. Left motor will go forward,
 * right motor will go backward.
 * Note that usual call of this function will set same speed for both motor
 * @bot robot to set the speed off.
 * @vg forward speed of the left motor (in ticks/41ms).
 * @vd backward speed of right motor  (in ticks/41ms)
 */
int RemoteTournerHoraire(robot * bot, int vg, int vd);

/**
 * Specify two different speed to apply, one for left motor, the other for right motor. Left motor will go backward,
 * right motor will go forward.
 * Note that usual call of this function will set same speed for both motor
 * @bot robot to set the speed off.
 * @vg backward speed of the left motor (in ticks/41ms).
 * @vd forward speed of right motor  (in ticks/41ms)
 */
int RemoteTournerAntiHoraire(robot * bot, int vg, int vd);
/**
 * Turn webcam of given unit for pan and tilt in given direction.
 * Note those call won't change bot->cam_x and bot->cam_y since unit of this function is unknown
 * @param bot robot we want the camera to move.
 * @param pan pan movement in an unknow unit.
 * @param tilt tilt movement in an unknow unit.
 * @param s direction of movement (see enum WBSens in trunk/include/libwb/wbcamera.h)
 */
int RemoteTournerWebcam(robot * bot, int pan, int tilt, WBSens s);

/**
 * move the camera of given robot to specified angles. base position is vertical 158 and horizontal 120.
 * Some time the camera won't move, nonetheless its coordinate in struct robot are changed as if it moved.<br>
 * WARNING Don't use RemoteTournerWebcam with RemoteMoveCameraOf and RemoteMoveCameraTo<br>
 * It seems that for vertical angle, valid value are between 0 and 135. For horizontal value, value have to be
 * between 0 and 340. Ok, now with testing, the real horizontal angle is between 0 and 315.<br> 
 * NOTE : les déplacements de 1 degré semblent problèmatique, faire au moins 2 voir 3 degrés<br>
 * NOTE : 0,0 coordinate seems to be upperleft bound
 * @param angle_vert vertical wanted angle
 * @param angle_horiz horizontal wanted angle
 * @param bot robot we want the camera to move
 */
 int RemoteMoveCameraTo(robot * bot, int angle_horiz, int angle_vert);

/**
 * Move the camera of given angles from current position.<br>
 * WARNING : first use of this function assume the bot is in base position (except if RemoteMoveCameraTo was previously
 * used). If not the move will be made from the base postion and not the real current one. Only for first use !
 * Don't use RemoteTournerWebcam with RemoteMoveCameraOf and RemoteMoveCameraTo<br>
 * NOTE : les déplacements de 1 degré semblent problèmatiques, faire au moins 2 voir 3 degrés
 * @param bot robot we want the camera to move.
 * @param angle_horiz angle (degree) to turn on horizontal axis
 * @param angle_vert  angle(degree) to turn on vertical axis
 */
int RemoteMoveCameraOf(robot * bot, int angle_horiz, int angle_vert);

/**
 * image and size will be set to the right value, image must be allocated first (at least 150000 bytes).
 * Use SIZEIMG for size of the allocation.
 */
int RemoteGetImage(robot * bot, unsigned char * image, int * size);

/**
 * stop all wheel movement of given robot.
 * @param bot robot we wan't to stop.
 */
int RemoteStop(robot * bot);

/**
 * Open a connection with given robot, automatically called by initRobot.
 * @param bot robot we wan't to open connection with.
 */
int RemoteConnec(robot * bot);

int RemoteConnecCamera(robot * bot);
/**
 * Not much use, not sure if it works, should test it someday (if usefull).
 */
int RemoteConnectCamera(robot * bot);

/**
 * Set motor mode to either controled speed or not. Robot should be mostly used with control on which is
 * default control.
 * @param bot robot we want to set the control off.
 * @param a_mode see enum motor_mode in LibRemote.h for both control mode.
 */
int RemoteSetMotorMode(robot * bot, motor_mode a_mode);

/**
 * Try to connect to given ip addresse on given port. Used by RemoteConnec
 * @param port port of the connection
 * @adresse ip adress of the remote connection
 * @serv_addr structure holding connection information, should be allocated first, all information will be written in it
 * @return socket number of connection or -1 if something failed.
 */
int RemoteSocket_connect(int port,char *adresse,struct sockaddr_in *serv_addr);

/**
 * Close connection with given robot. Should be called at the end of a program for each robot
 * @param bot robot we want the communication with to end.
 */
int RemoteDisconnect(robot * bot);

/**
 * Turn the bot of given 'angle' (in degree), counterclockwise is positiv (sens trigonométrique).
 * Detail of the process is described in "wifibot_mesure.txt"
 * WARNING : implementation of this function has been made specifically for wifibot 100, other wifibot
 * surely need another angular_rate
 * @param bot robot which will turn.
 * @param angle angle to turn of (in degree), counterclockwise is positiv.
 */
int RemoteTurnOf(robot * bot, int angle);

/**
 * Go forward of given 'distance' in meter at fixed speed.
 */
int RemoteForward(robot * bot, float distance);

/**
 * Search for all object in the list. Whenever the camera encounter one of the object, it will center on it,
 * updating position of the object and then go on to search other object.
 * Note that search will be made from current position and go on from left to right until it made a whole turn.
 * Camera of wifibot have an "angle mort", see RemoteTurnCameraTo()<br>
 * NOTE : list will be changed as the object are found. Each object found will be removed from the list, but
 * they will then be in the returned list.
 * @param bot robot or camera that will search the object.
 * @param list list of object to find.
 * @param display flag that will display current view of the camera in a SDL frame if set to TRUE.
 * @return list of all the object that were found
 */
list_object * RemoteFindAndCenter(robot * bot, list_object ** list, boolean display);

/**
 * Center camera on an object that is ALREADY in sight of the camera. Object should be mostly 
 * composed of a unique color.
 * See giveObjectRoi in TraitementImage.c for more detail on how object are found.
 * @param bot robot we want the camera to center on the object.
 * @param object_hue hue value (in hsl formalism) of the object
 * @param object_size minimum size of the object (in pixel) this allow to ignore smaller objects of the same
 * color as the one searched.
 * @param allow_horizontal this flag should be set to TRUE if object as some other color along horizontal axis
 * see MAXIMUM_SEARCH_WO_COLOR in TraitementImage.c, normal use should always set it to true, MAXIMUM_SEARCH_WO_COLOR
 * allowing for 3 pixel with different color. It happen in much case that object with a unique color still have little
 * difference between pixel that even DELTA_COLOR in TraitementImage.c couldn't handle.  
 * @param allow_vertical The same than allow_horizontal but for vertical axis.
 * @param display open an SDL frame to show the current image of the camera along with roi of object and center
 * of camera.
 * @param vertical_centering set this flag to TRUE if you want the object not only to be centered on horizontal axis.
 */
int RemoteCenterOnObject(robot * bot, object * object_to_center_on, boolean display, boolean vertical_centering);

/**
 * Will turn the camera until an object (second one) of given color is in sight.
 * @parma bot robot to interact with.
 * @param object_hue hue value (in hsl formalism) of the wanted object.
 * @param display open an SDL frame showing the camera visualisation.
 */
int RemoteFindSecondObject(robot * bot, object * object_to_find, boolean display);
/**
 * Will center on a object if more than one can fit the requesite, will center on the second.
 */
int RemoteCenterOnSecondObject(robot * bot, object * object_to_find, boolean display, boolean vertical_centering);

/**
 * Merely a call to RemoteFindSecondObject() then to RemoteCenterOnSecondObject
 */
int RemoteFindAndCenterOnSecondObject(robot * bot, object * object_to_find, boolean display, boolean vertical_centering);

/**
 * Simple call to giveObjectRoi in TraitementImage.h but will fill 'object_to_find' accordingly with 'bot'
 * camera position. If object is found, object_to_find->object_roi will hold the corresponding roi.
 * @param bot this is the robot from which object are searched.
 * @param object_to_find searched object.
 * @param image_hsl image (drawn from the camera of 'bot' logicaly) to search the object in.
 * @return TRUE if object was found, false otherwise.
 */
boolean RemoteGiveObjectRoi(robot * bot, object * object_to_find, hsl_image * image_hsl);
#endif
