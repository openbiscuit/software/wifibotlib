#ifndef REMOTE_H_
#define REMOTE_H_


/***************INCLUDE*************************/
#define NOSDL//no need of the SDL function and lib included in LibRemote.h, this flag disable loading of SDL
#include "LibRemote.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"
#include "libwb/jpeg_tab_src.h"
#include "libwb/wb.h"

/***************CONSTANTE************************/
#define DEFAULT_PORT 6080
#define STDIN 0


/***************VARIABLE GLOBALE*******************/
int socket_decoute;
int socket_connec;
pthread_t stop_thread;
bool terminate_thread = false;


/***************FONCTION**************************/

int socket_ecoute(int port,struct sockaddr_in *local);

void handler(int sig);

//reading socket is done here.
void * thread_read(void * args);

//basic avoidance : if an obstacle is detected, robot is stopped.
void * thread_obstacle_stop(void *);


#endif /*REMOTE_H_*/
