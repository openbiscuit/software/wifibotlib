#ifndef TRAITEMENTIMAGESTRUCTURE_H_
#define TRAITEMENTIMAGESTRUCTURE_H_


/***************INCLUDE**************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/***************CONSTANTE************************/
#ifndef HSLMAX
#define HSLMAX   360   /* H,L, and S vary over 0-HLSMAX */ 
#endif
                /* HLSMAX BEST IF DIVISIBLE BY 6 */ 
                /* RGBMAX, HLSMAX must each fit in a byte. */ 

/***************STRUCTURE*****************************/
#ifndef HAVE_BOOLEAN
typedef int boolean;
#define HAVE_BOOLEAN
#endif
#ifndef FALSE			/* in case these macros already exist */
#define FALSE	0		/* values of boolean */
#endif
#ifndef TRUE
#define TRUE	1
#endif

typedef struct roi_t
{
	int begin_x; //pixel extrème haut gauche
	int begin_y; //pixel extrème haut gauche
	int end_x; //pixel extrème bas droite
	int end_y; //pixel extrème bas droite
	int object_pixel_in_roi; //nombre de pixel de l'objet compris dans cette roi (pas la surface totale de la roi qui serait base * hauteur ...)
} roi;

//will hold all the information needed to recognize an object from an image
typedef struct
{
	char * name;//name of the object, when used in a list, each object should have a different name

	roi * object_roi;//roi of the object

	int hue;//color of the object
	int delta_color;//when searching in image for the object, each pixel with hue value between object hue +- delta_color
					//will be considered as having searched hue.
	int min_lightness;//pixel with lightness less than this won't fit in the object
	int max_lightness;//pixel with lightness more than this won't fit in the object
	int min_saturation;//pixel with saturation less than this won't fit in the object
	int max_saturation;//pixel with saturation more than this won't fit in the object
	
	int minimum_size;//if a set of pixel is found (with correct hue, lightness and saturation) but not
					//having this minimum pixel, it won't fit

	boolean allow_discontinuity;//allow for pixel not fitting hue, lightness or saturation constraint

	int max_inadequate_adjacent_pixel;//Cette valeur et son flag associé (allow_discontinuity) permet de reconnaitre un objet
										//dont la couleur principale serait séparée par une couleur minoritaire
										//exemple : un cone de lubeck majoritairement rouge avec une bande blanche qui séparerait la partie
										//basse et haute du cone.
										//Les objets possédant des points de couleurs minoritaires mais ne "coupant pas" l'objet en deux
										//sont reconnus sans problème, pas besoin de cette valeur et de son flag.
									//when pixel with wrong value is allowed, this maximum pixel are allowed.
									//Note that it restrain only the number of wrong pixel that follow each other
									//not the total number of wrong pixel.
									//If max_wrong_adjacent_pixel is set to 2 and allow_discontinuity is TRUE, if a row contains
									//5 good pixels then 3 wrong pixels then 5 good pixels, the 
	
	int x_position;//used for the robot, this is the camera horizontal angle when it was centered on the object
	int y_position;//used for the robot, this is the camera vertical angle when it was centered on the object
} object;

//list structure allowing search of multiple object in whatever the order.
typedef struct t_list_object
{
	object * item;//object of this list object
	boolean found;//used with the robot, we can tell if an object has already been found or not.

	struct t_list_object * next;// next object of the list, set to NULL if it's the last object of the list
} list_object;

/************************OBJECT & LIST_OBJECT MANAGEMENT****************************/

/**
 * Create an object with base value except for name. Don't forget to free returned object when of no more use.
 * @param name name of the object.
 * @return created object.
 */
object * initObject(char * name);

/**
 * Create a list_object containing a NULL object. Don't forget to free returned object when of no more use.
 * @return created list_object.
 */
list_object * initBlankListObject();

/**
 * Create a list_object from given object. Don't forget to free returned object when of no more use.
 * @param item object contained in the created list_object.
 * @return a list_object.
 */
list_object * initListObject(object * item);

/*
 * Return object with name stored in the list.
 * @param name name of wanted object.
 * @param list list_object to retrieve object from.
 */
object * findObject(char * name, list_object * list);

/**
 * Reinit the whole list so that object in it can be searched again. All object will keep their value such as hue,
 * min_lightness, ...
 */
void reinitList(list_object * list);

/**
 * Reinit object so that it can be searched again.
 */
void reinitObject(object * item);

/**
 * Remove an object from a list of object. The corresponding list object will be returned. If the returned object
 * is of no more use, free it !
 * @param name name of the object to remove.
 * @param l_object first element of the list of object where given object is to be removed.
 * @return NULL if object as not been found, the removed list object otherwise.
 */
list_object * removeObjectWithNameFromList(char * name, list_object ** l_object);

/**
 * Remove given list_object from a list (test is based on pointer adress : object are the same if they point
 * to same adress). The resulting list will be returned.
 * @param object_to_remove list_object to remove from the list. If of no more use, delete it !
 * @param list list which the object is to be removed from.
 * @return the resulting list.
 */
boolean removeObjectFromList(list_object * object_to_remove, list_object ** list);
/**
 *Add given list_object to the end of the list.
 * @param object_to_add object to insert at the end of the list.
 * @param list first element of the list where the list_object will be inserted. if NULL, will be set to 'object_to_add'
 */ 
void addObjectToList(list_object * object_to_add, list_object ** list);

/**
 * Free all the element in the list including object of each list object.
 * @param list first element of the list to delete.
 */
void freeList(list_object * list);

/**
 * free an object along with its roi.
 * @param item object to delete.
 */
void freeObject(object * item);

/**
 * Copy a list_object without the next field.
 * @param l_to_copy list_object to copy.
 * @return copy of the wanted object, only the next field will be set to null.
 */
list_object * copyListObject(list_object * object_to_copy);

object * copyObject(object * object_to_copy);

/**
 * copy this list_object along with all other following list_object and their item.
 */
list_object * copyList(list_object * list_to_copy);

/**
 * give the number of object in the list.
 */
int count(list_object * list);

/**
 * construct an object, asking much of the value through console.
 * @param name name of the object to construct.
 * @return an object with value as needed.
 */
object * constructObject(char * name);

/**
 * append 'to_append' at the end of 'base_list'
 */
void appendList(list_object ** base_list, list_object * to_append);

/**
 * Replace an object in the given list, returning old object if found.
 * @param object_to_replace this object will take place of an object with same name in 'list' if found.
 * @param list list where the object should be replaced.
 * @return the old replaced object if found. free it if of no use. Can return NULL if object was not found.
 */
object * replaceObject(object * object_to_replace, list_object * list);

/******************************************ROI FUNCTION********************************/
/**
 * Set the roi's bounds to R 255, G 0, B 255 in the image. Make sure that the roi's bounds
 * are INSIDE the image lengths. 'image' is to be an RGB image (means a pixel is coded with 3 unsigned
 * char, one for each color). 'width' and 'height' are the size of the image in pixel.
 */
void drawROIOnImage(unsigned char * image, roi * box, int width, int height);
/**
 * give a ready to use Region Of Interest as a pointer
 */
roi * initRoi();

/**
 * Reset given roi value as if it was just created. 
 */
void reinitRoi(roi * box);

/**
 * Add pixel with given coordinate to 'box'.
 */
void addPixelToROI(int line, int row, roi * box);

/**
 * Copy given roi
 */
roi * copyRoi(roi * box);

#endif /*TRAITEMENTIMAGESTRUCTURE_H_*/
