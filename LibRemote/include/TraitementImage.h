/*
** libwb - A library for low and medium level control of a WifiBot.
**
** Copyright (C) 2006-2007 Nicolas Beaufort, Alain Dutech
**			Maia Team, LORIA.
**
** Original authors: Nicolas Beaufort
**
** Mail: Alain.Dutech@loria.fr
** Web: http://maia.loria.fr
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/
#ifndef TRAITEMENT_IMAGE_H
#define TRAITEMENT_IMAGE_H

/**
 * @file   traitement_image.h
 * @brief  Méthodes pour trouver la cible dans l'image.
 * @author Nicolas Beaufort
 * @author Alain Dutech
 * @date   Thu Apr 19 13:24:32 2007
 * 
 */
/***************INCLUDE**************************/
#include <stdlib.h>
#include <stdio.h>
#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"
#include "TraitementImageStructure.h"

/***************CONSTANTE************************/
#ifndef TMAX
#define TMAX 255
#endif
#ifndef seuil_blanc
#define seuil_blanc 223
#endif
#ifndef HSLMAX
#define HSLMAX   360   /* H,L, and S vary over 0-HLSMAX */ 
#endif
#ifndef RGBMAX
#define RGBMAX   255   /* R,G, and B vary over 1-RGBMAX */ 
#endif
                /* HLSMAX BEST IF DIVISIBLE BY 6 */ 
                /* RGBMAX, HLSMAX must each fit in a byte. */ 

/* Hue is undefined if Saturation is 0 (grey-scale) */ 
/* This value determines where the Hue scrollbar is */ 
/* initially set for achromatic colors */ 
#define UNDEFINED 10000
#define INC 2

/***************GLOBAL VARIABLE**********************/
#define DELTA_COLOR 4

/***************STRUCTURE*****************************/

//HSL pixel structure  
typedef struct Tpx
{
	int hue;
	int saturation;
	int lightness;
	int traite;//un pixel traite ne le sera plus sauf si on réinitialise cette valeur à 0
	int iswhite;
	int isblack;
} Tpx;

/**
 * Store a size 2 array of Tpx struct. So its not a true hsl image since only hue
 * component is kept.
 * Any hsl image must be freed manually or througn a call to freeHslImage().
 */
typedef struct hsl_image
{
	//height first, then width
	Tpx ** pixels;
	int width;
	int height;
} hsl_image;

typedef struct
{
	struct jpeg_source_mgr pub; /* public fields */
	JOCTET * buffer;	/* Start of buffer */
	int nb_bytes;
	boolean start_of_file;
} tab_source_mgr;

typedef tab_source_mgr * tab_src_ptr;

/*******************************IMAGE MANAGEMENT****************************/

/**
 * write to a file named after parameter 'fichie'r . whats under 'img' pointer will be writed, it is supposed
 * to hold an RGB image with color in that order. 'width' & 'height' are to be accurate, the actual size of 
 * the pointer 'img' should be 'width' * 'height' * 3.
 * File is in .ppm format, add the extension to the file name as it won't be added.
 */int writePPMImage(char *fichier, unsigned char *img, int width, int height);

/**
 * return a pointer to RGB component of an existing jpeg file named 'file_name'. width and height are set accordingly with
 * the size of the jpeg image. The overall length of the returned pointer is 3 * width * height. each pixel as a R, G, B component,
 * accessed in this order.
 * @param file_name name of the jpeg file to decompress (along with path).
 * @param width width of the decompressed rgb buffer.
 * @param height height of the resulting rgb buffer.
 * @return pointer to the decompressed image (as rgb). Free the pointer when no longer used.
 */
unsigned char * jpegFileDecompress(char * file_name, int * width, int * height);

/**
 * return a pointer to RGB component of an existing ppm file named 'file_name'. width and height are set accordingly with
 * the size of the ppm image. The overall length of the returned pointer is 3 * width * height. each pixel as a R, G, B component,
 * accessed in this order.
 * @param file_name name of the ppm file to decompress (along with path).
 * @param width width of the decompressed rgb buffer.
 * @param height height of the resulting rgb buffer.
 * @param return pointer to decompressed image (as rgb). Free the pointer when no longer used.
 */
unsigned char * ppmFileDecompress(char * file_name, int * width, int * height);

/**
 * Same as JpegFileDecompress but with a buffer.
 * @param buffer pointer to the beggining of the jpeg buffer.
 * @param taille size of the buffer
 * @param width width of the decompressed rgb buffer.
 * @param height height of the decompressed rgb buffer.
 */
unsigned char * jpegBufferDecompress(unsigned char *buffer, int taille,
		     int *width, int *height);

/* 
 * D�compresse une image contenue dans un buffer.
 *  - image		: double pointeur sur un buffer qui contiendra l'image d�compress�e (non allou�)
 *  - buffer	: buffer contenant l'image compress�e
 *  - taille	: taille en octets de l'image compress�e
 * Renvoie la taille de l'image d�compres�e (en octets)
 * Met � jour : width et height.
 */
/** 
 * Decompress an jpeg image pointed at by buffer.
 * 
 * @param image will contain the decompressed image (will be allocated)
 * @param buffer points to the compressed image
 * @param taille of the compressed image
 * @param width of the image (pixels)
 * @param height of the image (pixels)
 * 
 * @return size of the decompressed image (bytes)
 */
int decompressImage(unsigned char *image, unsigned char *buffer, int taille, int *width, int *height);


/**************************HSL FUNCTION******************************************/
/**
 * Convert a struct hsl_image to an array of unsigned char. The array is of length
 * image->width * image->length * 3. The array is the image in RGB format, each color
 * component is accessed in this order.
 * @param image image to convert to rgb
 * @return a pointer to an rgb image, free it when no longer needed.
 */
unsigned char * hsl2rgb(hsl_image * image);

/**
 * Simply return a pointer to a struct hsl_image ready to be used.
 * @param width wanted width of the image.
 * @param height wanted height of the image.
 * @return NULL if a something went wrong in a malloc. Else return a pointer to hsl_image that should be
 * eventually freed with freeHslImage preferably.
 */
hsl_image * initHslImage(int width, int height);

/**
 * Call this to deallocate a hsl_image created the initHslImage() way.
 */
void freeHslImage(hsl_image * image);

/*
 * Reinit each pixels' traite field of the image so that the image can be used for new object recognition
 * @param image image to reinit
 */
void reinitHslImage(hsl_image * image);

/**
 * Convertit l'image rvb pointée par image en hsl (voir la structure Tpx)
 * width et height étant la taille de l'image passer en pointeur.
 * @param image rgb image to translate in hsl
 * @param width width (in pixel) of the rgb image
 * @param height height (in pixel) of the rgb image
 * @return pointer to an hsl_image, free it with freeHslImage() when no longer needed.
 */
hsl_image * rgb2hsl(unsigned char *image, int width, int height);

/**
 * Original method for converting from rgb to hsl.
 * @param image image rgb to convert
 * @param teinte hsl pixel to put the hsl image in
 * @param width width in pixel of the image (should be 640).
 * @param height height in pixel of the image (should be 480).
 */
void rvb2hsl(unsigned char *image, Tpx teinte[480][640], int width, int height);

/**
 * write given hsl_image in a file with given name. return true if succesful, false otherwise.
 */
boolean hsl2File(char * file_name, hsl_image * a_image);

/************************OBJECT FINDING*********************************************/
/**
 * fill 'searched_object' fields if the object is found. Note that roi value will change (possibly set to NULL) even if not found.
 * @param searched_object object to search.
 * @param image image to search object in.
 * @return TRUE if object is found, false otherwise.
 */
boolean giveObjectRoi(object * searched_object, hsl_image * image);

/**
 * return true if at least an object of the list as been found in 'image'.
 * All object found will have their Roi value set accordingly, the corresponding list_object will also has
 * its 'found' field set to TRUE. Note that 'image' will have its 
 * 'traite' pixels' fields reset (set to 0) at the end of the function.
 * All object's roi will change in the process (possibly set to NULL), keep this in mind.
 * @param list_of_object The objects looked for.
 * @param image The image to look in for the objects.
 * @return TRUE if at least an object is found, FALSE otherwise.
 */
boolean giveObjectsRoi(list_object * list_of_object, hsl_image * image);

/************************************************************************/
/********************************MISC FUNCTION **************************/
/************************************************************************/

/**
 * Convert a single RGB pixel to an hsl one.
 * @param R red value of the pixel.
 * @param G green value of the pixel.
 * @param B blue value of the pixel
 * @param hsl_pixel Tpx pointer to hold the result in.
 */
void pixelRGBtoHSL(int R, int G, int B, Tpx * hsl_pixel);

/* utility routine for HLStoRGB */ 
int hueToRGB(int magic1, int magic2, int hue);

/**
 * Convert a single hsl pixel given as parameter to RGB value.
 * @param hsl_pixel pixel to convert
 * @param R computed red value.
 * @param G computed green value.
 * @param B computed blue value. 
 */
void pixelHSLtoRGB(Tpx hsl_pixel, int * R, int * G, int * B);

/**
 * Use a top notch algorithm taking advantage of recent quad-core optimization to compute the max of two numbers
 */
int max(int a, int b);

/**
 * This one I won't explain.
 */
int min(int a, int b);

/**
 * Crée des pixels de taille 'sizeofpixel' dont la teinte est la moyenne
 * des teintes des pixels remplacés.
 * @param image_to_process image we want to pixelise, note that this image won't change, the returned image
 * will be the pixelised one.
 * @param sizeofpixel number of pixel to replace by only one.
 * @return pixelised image, to be freed with freeHslImage() eventually.
 */		
hsl_image * pixeliserHSL(hsl_image * image_to_process, int sizeofpixel);

/**
 * La teinte sera codée par intervales de longueur 'div'
 * (0, div, 2*div, 3*div, ...).
 * @param image_to_process image à discrétiser
 * @param div division
 * @return hsl_image discrétisé, à libérer avec freeHslImage()
 */
hsl_image * discretiserHSL(hsl_image * image_to_process, int div);

/**
 * discrétise une image rgb ...
 */
void discretiserRGB(unsigned char *entre,unsigned char *sortie,int width,int height,int div);

/**
 * Used by findObject() to find begin coordinate of a roi
 */
void beginPixel(int line, int row, roi * box);

/**
 * Used by findObject() to find end coordinate of a roi
 */
void endPixel(int line, int row, roi * box);

/**
 * Test if given 'pixel' as the hue, lightness and saturation required to be in the object.
 * @param pixel pixel to test upon.
 * @param searched_object object looked for, will hold hue, saturation, lightness values from which we can tell
 * if pixel can be part of the object or not.
 * @return TRUE if the pixel could possibly be in the object, FALSE otherwise.
 */
boolean hslPixelAsColor(object * searched_object, Tpx pixel);

/**
 * give the average of the 'size' first number in 'numbers'. The average is computed on a "round range", max range being HSLMAX.
 * This mean that number near HSLMAX and just after 0 are very close.
 * For example average of 355 and 3 should give 359 and not 179.
 * @param numbers, array holding the values to mean.
 * @param size, size of numbers
 */
int meanOnRoundRange(int numbers[], int size, int range);

/**
 * search if neighbor pixel of pixel at 'current_x' and 'current_y' in 'image' could fit the 'searched_object' prerequisite.
 * If yes, recurively search for neighbor's neighbors.
 * @param image the current image to take pixels from.
 * @param current_x row number of pixel which neighbor we'll check.
 * @param current_y line number of pixel which neighbor we'll check.
 * @param searched_object struct object hold all the information which can tell wether a pixel is from an object or not.
 * @param box this roi is used to store information of pixel's position if it fit the object need. We don't use
 * the roi in the struct object since, even if pixel fit hue, saturation and lightness prerequisite, the final size of
 * the roi may be to small to be the object.
 * When method end, this parameter will hold the actual roi of all found contiguous pixel. We can then test if the roi is big enough
 * to be the object or not (@see giveObjectRoi())
 * @param pixel_wo_color_nb if 'searched_object' allow for some pixel with wrong value, this parameter will store the number
 * of contiguous pixel (i.e. in previous call of the recursiv call leading to actual call (...)) that were not good one already found.
 * for each contiguous pixel with wrong number, this number will be incremented (by one), if it ever goes beyond searched_object->max_inadequate_adjacent_pixel
 * then there won't be anymore recursiv call.
 * Note that even if wrong pixel are allowed, they won't count as pixel of the object (see roi->object_pixel_in_roi).
 * If one or more wrong pixel are found, then a good one is found, the value will be reset to 0 in next recursiv call.
 */
void searchNeighborPixel(hsl_image * image, int current_x, int current_y, object * searched_object, roi * box, int pixel_wo_color_nb);

#endif
