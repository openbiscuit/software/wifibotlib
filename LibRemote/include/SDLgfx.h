#ifndef SDLGFX_H_
#define SDLGFX_H_

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_gfxPrimitives.h"
#include <stdlib.h>

//Frame where all the image will be displayed
extern SDL_Surface * screen;

/**
 * Used by displayRGBBuffer. Shouldn't be used as is, or make sure that variable 
 * screen is correclty initialized.
 * @param x row of the pixel.
 * @param y line of the pixel.
 * @param R red value of the pixel.
 * @param G green value of the pixel.
 * @param B blue value of the pixel.
 */
void putPixel(int x, int y, int R, int G, int B);

/**
 * Call this function prior to all other function in this library.
 * It will initialize the SDL for correct display of futur image.
 * Note that multiple call are ok, they won't make anything.
 * @return 0 if okay, something else if something went wrong.
 */
int initSDL();

/**
 * open a frame displaying given image and of given size.
 * @param image_buffer RGB image to display, the pointer is to be width * height * 3 length
 * with R,G and B value as char in that order in the buffer
 * @param width width in pixel of the image
 * @param height in pixel of the image
 * @return 0 if something went wrong, 0 otherwise.
 */
int displayRGBBuffer(unsigned char * image_buffer, int width, int height);

/**
 * Display a jpeg image in a buffer.
 * @param bufferImg pointer to the image.
 * @param sizeImage size of the buffer.
 * @return 0 if something went wrong, 0 otherwise.
 */
int displayJPGBuffer(unsigned char * bufferImg, int sizeImage);

/**
 * Display a jpeg file.
 * @param file_name name of the file.
 * @return 0 if something went wrong, 0 otherwise.
 */
int displayJPGFile(char * file_name);

/**
 * Display a ppm file ...
 * @param file_name age of the captain.
 * @return 0 if something went wrong, 0 otherwise.
 */
int displayPPMFile(char * file_name);

/**
 * Draw a cross on the center of given 'image' with given size
 * @param RGB image with 'width' and 'height' pixels.
 * @param width width in pixel.
 * @param height in pixel.
 */
void drawCross(unsigned char * image, int width, int height);

#endif /*SDLGFX_H_*/
